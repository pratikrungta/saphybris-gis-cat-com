#!/bin/bash
# sh scriptname 1
OWN_NAME=build_Dev.sh


echo "Pulling the code from VSTS)"


cd /opt/tomcat/hybris/bin/platform
./hybrisserver.sh stop
echo "Hybris stopped"

cd /opt/tomcat/hybris/bin/

rm -rf custom

scp -r  /usr/vsts/myagent/_work/1/s/hybris/bin/custom  /opt/tomcat/hybris/bin

cd /opt/tomcat/hybris/config

rm local.properties
rm localextensions.xml

cp /usr/vsts/myagent/_work/1/s/hybris/config/localextensions.xml /opt/tomcat/hybris/config/localextensions.xml
cp /usr/vsts/myagent/_work/1/s/hybris/config/local_Dev.properties /opt/tomcat/hybris/config/local.properties

cd /opt/tomcat/hybris/bin/platform

. ./setantenv.sh

ant clean all

ant production

echo "Building the dev server for Hybris code"

cd /opt/tomcat/hybris/bin/
rm -rf custom
rm -Rf ext-*

cp /opt/tomcat/hybris/temp/hybris/hybrisServer/hybrisServer-AllExtensions.zip  /opt/tomcat/

cd /opt/tomcat/

unzip -o hybrisServer-AllExtensions.zip

cd /opt/tomcat/hybris/bin/platform

. ./setantenv.sh

ant clean all

./hybrisserver.sh start
