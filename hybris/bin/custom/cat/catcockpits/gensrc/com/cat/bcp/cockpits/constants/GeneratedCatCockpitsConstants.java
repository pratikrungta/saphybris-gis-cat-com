/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jun 30, 2017 4:29:12 PM                     ---
 * ----------------------------------------------------------------
 */
package com.cat.bcp.cockpits.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedCatCockpitsConstants
{
	public static final String EXTENSIONNAME = "catcockpits";
	
	protected GeneratedCatCockpitsConstants()
	{
		// private constructor
	}
	
	
}
