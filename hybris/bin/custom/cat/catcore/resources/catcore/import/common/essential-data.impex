# -----------------------------------------------------------------------
# [y] hybris Platform
#
# Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
#
# This software is the confidential and proprietary information of SAP
# ("Confidential Information"). You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the
# license agreement you entered into with SAP.
# -----------------------------------------------------------------------
# Import essential data for the Accelerator
#
# Includes:
# * Languages
# * Currencies
# * Titles
# * Vendors
# * Warehouses
# * Supported Credit/Debit cards
# * User Groups
# * DistanceUnits for Storelocator
# * MediaFolders
# * MediaFormats                               s
# * Tax & Tax Groups
# * Jobs
#% impex.setLocale(Locale.GERMAN);

$productCatalog=NAProductCatalog
$catalogVersion=catalogVersion(catalog(id[default='NAProductCatalog']),version[default='Staged'])[unique=true]
$supercategories=supercategories(code,$catalogVersion)
$approved=approvalstatus(code)[default='approved']
$baseProduct=baseProduct(code,$catalogVersion)
$syncJobCode=sync $productCatalog:Staged->Online



# Languages
INSERT_UPDATE Language;isocode[unique=true];fallbackLanguages(isocode);active[default=true]
;en;;;
;de;en;;
;ja;en;;
;zh;en;;

# Currencies
INSERT_UPDATE Currency;isocode[unique=true];conversion;digits;symbol
;GBP;1;2;£
;EUR;1;2;€
;USD;1,4;2;$
;JPY;120;0;¥

# Vendor
INSERT_UPDATE Vendor;code[unique=true];name
;default;Default Vendor

# Disable preview for email pages
UPDATE CMSPageType;code[unique=true];previewDisabled
;EmailPage;true

# Titles
INSERT_UPDATE Title;code[unique=true]
;mr
;mrs
;miss
;ms
;dr
;rev

# Media Folders
INSERT_UPDATE MediaFolder;qualifier[unique=true];path[unique=true]
;images;images
;email-body;email-body
;email-attachments;email-attachments

# Media formats
INSERT_UPDATE MediaFormat;qualifier[unique=true]
;1200Wx1200H
;800Wx600H
;515Wx515H
;365Wx246H
;300Wx300H
;96Wx96H
;65Wx65H
;30Wx30H
;mobile
;tablet
;desktop
;widescreen

# Tax & Tax Groups
INSERT_UPDATE UserTaxGroup;code[unique=true]
;de-taxes
;jp-taxes
;uk-taxes
;us-taxes
;ca-taxes

INSERT_UPDATE ProductTaxGroup;code[unique=true]
;jp-vat-full
;eu-vat-full
;eu-vat-half
;us-sales-tax-full
;ca-sales-tax-full

INSERT_UPDATE Tax;code[unique=true];value;currency(isocode)
;de-vat-full;19
;de-vat-half;7
;jp-vat-full;5
;uk-vat-full;20
;uk-vat-half;5
;us-sales-tax-full;0
;ca-sales-tax-full;0

INSERT_UPDATE ServicelayerJob;code[unique=true];springId[unique=true]
;cartRemovalJob;cartRemovalJob
;siteMapMediaJob;siteMapMediaJob
;quoteToExpireSoonJobPerformable;quoteToExpireSoonJobPerformable
;quoteExpiredJobPerformable;quoteExpiredJobPerformable

# Deactivate Frontend Restriction on category by default for perfomance purposes
UPDATE SearchRestriction;code[unique=true];active[default=false]
;Frontend_RestrictedCategory


# UTV Categories restriction on  IM user group
INSERT_UPDATE SearchRestriction;code[unique=true];name;principal(uid);query;restrictedType(code);active;generate
;UTV_CATEGORIES_VISIBILITY;UTV CATEGORIES VISIBILITY RESTRICTION;dealerInventoryManagerGrp;{productFamily} not in  ({{SELECT {pk} from {Category} where {code} in ('utv','accessory')}});OrderAdditionalInfo;true;true



# Insert Product Unit
INSERT_UPDATE Unit;unitType[unique=true];code[unique=true];name[lang=de];name[lang=en];conversion;;;
;each;ea;ea;ea;1          

INSERT_UPDATE Vendor;code[unique=true];name[lang=en]
;caterpillarVendor;Caterpillar Vendor    


INSERT_UPDATE CronJob; code[unique=true];job(code);singleExecutable;sessionLanguage(isocode)
;catSftpChannelJobPerformable;catSftpChannelJobPerformable;false;en
;catProductCPCMediaCronJob;catProductCPCMediaCronJob;false;en
;catQuoteDataToAWSJob;catQuoteDataToAWSJob;false;en
;catUpdateInventoryReservationFlagJob;catUpdateInventoryReservationFlagJob;false;en
;catOrderDirectorJob;catOrderDirectorJob;false;en
;fbcTestCronJob;fbcTestCronJob;false;en
;catCountyDataToAWSJob;catCountyDataToAWSJob;false;en
;catAccessoriesODCronJob;catAccessoriesODCronJob;false;en	
;catOrderCompatibilityCheckJob;catOrderCompatibilityCheckJob;false;en
;catOrderAcknowledgementCronjob;catOrderAcknowledgementCronjob;false;en

INSERT_UPDATE CompositeCronJob;code[unique=true];job(code);sessionLanguage(isocode)
;catCompatibilityAndIndexCompositeJob;compositeJobPerformable;en

INSERT_UPDATE CompositeEntry;code[unique=true];triggerableJob(code);compositecronjob(code);compositecronjobpos
;catOrderCompatibilityCheckJob;catOrderCompatibilityCheckJob;catCompatibilityAndIndexCompositeJob;1
;NAIndexFullCronJob;solrIndexerJob;catCompatibilityAndIndexCompositeJob;2
 


INSERT_UPDATE CompositeCronJob;code[unique=true];job(code);sessionLanguage(isocode)
;catOrderDirectorCompositeJob;compositeJobPerformable;en

INSERT_UPDATE CompositeEntry;code[unique=true];triggerableJob(code);compositecronjob(code);compositecronjobpos
;catOrderDirectorJob;catOrderDirectorJob;catOrderDirectorCompositeJob;1
;catAccessoriesODCronJob;catAccessoriesODCronJob;catOrderDirectorCompositeJob;2
;catOrderAcknowledgementCronjob;catOrderAcknowledgementCronjob;catOrderDirectorCompositeJob;3

INSERT_UPDATE CpcCronJob; code[unique=true];job(code);singleExecutable;sessionLanguage(isocode);active;productFamilies
;CatCPCImportCronJob;catCPCImportCronJob;false;en;0;UTILITY VEHICLE

INSERT_UPDATE CronJob; code[unique=true];job(code);singleExecutable;sessionLanguage(isocode);active
;catProductCPCFacetCronJob;catProductCPCFacetCronJob;false;en;0


INSERT_UPDATE Trigger;cronjob(code)[unique=true];cronExpression
;catSftpChannelJobPerformable; 	0 0 0/1 1/1 * ? *
;catProductCPCFacetCronJob;	0 15 7 ? * SUN *
;catProductCPCMediaCronJob;	0 0 7 ? * SUN *
;catQuoteDataToAWSJob;0 0 1 ? * SUN *
;catCountyDataToAWSJob;0 0 2 ? * SUN *
;catOrderDirectorCompositeJob;	0 0/15 * 1/1 * ? *
;catCompatibilityAndIndexCompositeJob;0 15 2 1/1 * ? * 


REMOVE Trigger;cronjob(code)[unique=true]
;catOrderDirectorJob
;catOrderAcknowledgementCronjob

REMOVE CronJob;code[unique=true]
;OrderDirectorCronJob


##############################################################################
#CreateUserPriceGroups
##############################################################################
INSERT_UPDATE UserPriceGroup;code[unique=true];name[lang=en];
;USPRICEGROUP;USPRICEGROUP;
;CAPRICEGROUP;CAPRICEGROUP;



#Sales model payment term & fbc service access impex
INSERT_UPDATE Product;code[unique=true];paymentterm;$catalogVersion;controlFBCProcess;externalName;freightChargeCode(code);freightTransportCode(code);freightRouteCode
;232D	;	7KA03	;;true;232D;N;T;MANAGED
;236D	;	7KA03	;;true;236D;N;T;MANAGED
;239D	;	7KA03	;;true;239D;N;T;MANAGED
;242D	;	7KA03	;;true;242D;N;T;MANAGED
;246D	;	7KA03	;;true;246D;N;T;MANAGED
;249D	;	7KA03	;;true;249D;N;T;MANAGED
;257D	;	7KA03	;;true;257D;N;T;MANAGED
;259D	;	7KA03	;;true;259D;N;T;MANAGED
;262D	;	7KA03	;;true;262D;N;T;MANAGED
;272D2	;	7KA03	;;true;272D2;N;T;MANAGED
;279D	;	7KA03	;;true;279D;N;T;MANAGED
;289D	;	7KA03	;;true;289D;N;T;MANAGED
;299D2	;	7KA03	;;true;299D2;N;T;MANAGED
;300.9D	;	7KA03	;;true;300.9D;N;T;MANAGED
;301.4C	;	7KA03	;;true;301.4C;N;T;MANAGED
;301.7D	;	7KA03	;;true;301.7D;N;T;MANAGED
;302.4D	;	7KA03	;;true;302.4D;N;T;MANAGED
;303.5E2CR;	R014	;;true;303.5E2;N;T;MANAGED
;304E2CR;	R014	;;true;304E2;N;T;MANAGED
;305.5E2CR;	R014	;;true;305.5E2;N;T;MANAGED
;305E2CR;	R014	;;true;305E2;N;T;MANAGED
;307E2	;	R014	;;true;307E2;N;T;MANAGED
;307E2CR;	R014	;;true;307E2;N;T;MANAGED
;308E2CR;	R014	;;true;308E2;N;T;MANAGED
;415F2	;	7KA03	;;true;415F2;N;T;MANAGED
;903C2	;	7KA03	;;false;903C2;N;T;MANAGED
;906M	;	7KA03	;;true;906M;N;T;MANAGED
;908M	;	7KA03	;;true;908M;N;T;MANAGED
;910M	;	7KA03	;;false;910M;N;T;MANAGED
;914M	;	7KA03	;;false;914M;N;T;MANAGED
;918M	;	7KA03	;;false;918M;N;T;MANAGED
;926M	;	7KA03	;;false;926M;N;T;MANAGED
;930M	;	7KA03	;;false;930M;N;T;MANAGED
;938M	;	7KA03	;;false;938M;N;T;MANAGED
;950M	;	R017	;;true;950M;N;T;MANAGED   
;TL642D     ;7KA03  ;;true;TL642D;N;T;MANAGED
;TL943      ;7KA03  ;;true;TL943;N;T;MANAGED
;TL1055D    ;7KA03  ;;true;TL1055D;N;T;MANAGED
;TL1255D    ;7KA03  ;;true;TL1255D;N;T;MANAGED
;TH255C     ;7KA03  ;;true;TH255C;N;T;MANAGED
;TH514D     ;7KA03  ;;true;TH514D;N;T;MANAGED     
;950GC      ; AZZW5 ;;true;950GC-T4;N;T;MANAGED
;313F       ; AZZW5 ;;true;313F;N;T;MANAGED
;313FGC     ; AZZW5 ;;true;313FGC;N;T;MANAGED
;315F       ; AZZW5 ;;true;315F;N;T;MANAGED
;316F       ; AZZW5 ;;true;316F;N;T;MANAGED
;320        ; AZZW5 ;;true;320;N;T;MANAGED
;320GC      ; AZZW5 ;;true;320GC;N;T;MANAGED
;323        ; AZZW5 ;;true;323;N;T;MANAGED
;318        ; AZZW5 ;;true;318F;N;T;MANAGED
;226D;    AZZW5 ;;true;226D     ;N;T;MANAGED                               
;272D2XHP;AZZW5 ;;true;272D2 XHP;N;T;MANAGED
;299D2XHP;AZZW5 ;;true;299D2 XHP;N;T;MANAGED
;301.7DCR;AZZW5 ;;true;301.7D CR;N;T;MANAGED
;302.7DCR;AZZW5 ;;true;302.7D CR;N;T;MANAGED
;303ECR;  AZZW5 ;;true;303E CR;N;T;MANAGED
;416F2HRC;AZZW5 ;;true;416F2 HRC;N;T;MANAGED
;420F2HRC;AZZW5 ;;true;420F2 HRC;N;T;MANAGED
;430F2HRC;AZZW5 ;;true;430F2 HRC;N;T;MANAGED
;D3K2HRC; AZZW5 ;;true;D3K2 HRC;N;T;MANAGED
;D5K2HRC; AZZW5 ;;true;D5K2 HRC;N;T;MANAGED
;D4K2HRC; AZZW5 ;;true;D4K2 HRC;N;T;MANAGED



#Added CompatibilityInfo as rootTypes to the synchronization
UPDATE CatalogVersionSyncJob;code[unique=true];roottypes(code)[mode=append];
;$syncJobCode;CompatibilityInfo;

