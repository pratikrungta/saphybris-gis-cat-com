/**
 *
 */
package com.cat.bcp.core.actions.mail;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.event.CatOrderConfirmationEmailEvent;
import com.cat.bcp.core.model.CatPlaceBCPReOrderProcessModel;
import com.cat.bcp.core.model.CatPlaceOrderProcessModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.event.EventService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.annotation.Resource;


/**
 * @author asomjal
 *
 */
public class CatAccessoriesOrderConfirmationEmailAction extends AbstractSimpleDecisionAction<BusinessProcessModel>
{
	private static final Logger LOG = Logger.getLogger(CatAccessoriesOrderConfirmationEmailAction.class);

	private static final String CAT_ACCESSORIES_ORDER_CONFIRMATION_TEMPLATE = "CatAccessoriesOrderConfirmationEmailTemplate";


	@Resource(name = "eventService")
	private EventService eventService;

	/**
	 * This method is used to send order Confirmation Email. {@inheritDoc}
	 */
	@Override
	public Transition executeAction(final BusinessProcessModel process) throws CalculationException
	{
		if (process != null)
		{
			LOG.debug("Inside CatOrderConfirmationEmailAction execute action - business process : " + process.getCode());
			OrderModel orderModel;
			String fronEndTemplateName;
			orderModel = (OrderModel) ((CatPlaceOrderProcessModel) process).getOrder();

				fronEndTemplateName = CAT_ACCESSORIES_ORDER_CONFIRMATION_TEMPLATE;


			eventService.publishEvent(new CatOrderConfirmationEmailEvent(orderModel, fronEndTemplateName, true));
			return Transition.OK;
		}
		return Transition.NOK;
	}
}
