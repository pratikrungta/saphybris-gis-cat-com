/**
 *
 */
package com.cat.bcp.core.actions.mail;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.acceleratorservices.process.email.actions.GenerateEmailAction;
import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.task.RetryLaterException;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


/**
 * The class is used to generate the email for the list of config IDs which failed
 * during the compatibility check.
 *
 */
public class CatGenerateCompatibilityCheckFailedEmailAction extends GenerateEmailAction
{
	private static final Logger LOG = Logger.getLogger(CatGenerateCompatibilityCheckFailedEmailAction.class);

	@Resource(name="catalogService")
	CatalogService catalogService;


	@Resource(name="baseSiteService")
	BaseSiteService baseSiteService;

	@Override
	public Transition executeAction(final BusinessProcessModel businessProcessModel) throws RetryLaterException {
		getContextResolutionStrategy().initializeContext(businessProcessModel);

		 CatalogVersionModel contentCatalogVersion = getContextResolutionStrategy()
				.getContentCatalogVersion(businessProcessModel);
		if (contentCatalogVersion.getVersion() ==null)
		{

				LOG.warn("Could not resolve the content catalog version, cannot generate email content");
				return Transition.NOK;
		}


		final EmailPageModel emailPageModel = getCmsEmailPageService().getEmailPageForFrontendTemplate(getFrontendTemplateName(),
				contentCatalogVersion);
		if (emailPageModel == null)
		{
			LOG.warn("Could not retrieve email page model for " + getFrontendTemplateName() + " and "
					+ contentCatalogVersion.getCatalog().getName() + ":" + contentCatalogVersion.getVersion()
					+ ", cannot generate email content");
			return Transition.NOK;
		}

		final EmailMessageModel emailMessageModel = getEmailGenerationService().generate(businessProcessModel, emailPageModel);
		if (emailMessageModel == null)
		{
			LOG.warn("Failed to generate email message");
			return Transition.NOK;
		}

		final List<EmailMessageModel> emails = new ArrayList<>();
		emails.addAll(businessProcessModel.getEmails());
		emails.add(emailMessageModel);
		businessProcessModel.setEmails(emails);

		getModelService().save(businessProcessModel);

		LOG.info("Email message generated");
		return Transition.OK;
	}
	}
