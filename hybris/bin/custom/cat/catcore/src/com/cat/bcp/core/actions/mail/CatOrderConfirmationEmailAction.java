/**
 *
 */
package com.cat.bcp.core.actions.mail;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.servicelayer.event.EventService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.event.CatOrderConfirmationEmailEvent;
import com.cat.bcp.core.model.CatPlaceBCPReOrderProcessModel;
import com.cat.bcp.core.model.CatPlaceOrderProcessModel;


/**
 * @author asomjal
 *
 */
public class CatOrderConfirmationEmailAction extends AbstractSimpleDecisionAction<BusinessProcessModel>
{
	private static final Logger LOG = Logger.getLogger(CatOrderConfirmationEmailAction.class);

	private static final String CAT_BCP_ORDER_CONFIRMATION_TEMPLATE = "CatBCPOrderConfirmationEmailTemplate";

	private static final String CAT_CUV_ORDER_CONFIRMATION_EMAIL_TEMPLATE = "CatCUVOrderConfirmationEmailTemplate";

	private static final String CAT_TRUCKLOAD_ORDER_CONFIRMATION_EMAIL_TEMPLATE = "CatTruckloadOrderConfirmationEmailTemplate";

	@Resource(name = "eventService")
	private EventService eventService;

	/**
	 * This method is used to send order Confirmation Email. {@inheritDoc}
	 */
	@Override
	public Transition executeAction(final BusinessProcessModel process) throws CalculationException
	{
		if (process != null)
		{
			LOG.debug("Inside CatOrderConfirmationEmailAction execute action - business process : " + process.getCode());
			OrderModel orderModel;
			String fronEndTemplateName;
			if (process instanceof CatPlaceBCPReOrderProcessModel)
			{
				LOG.debug("inside if loop CatOrderConfirmationEmailAction - " + CAT_BCP_ORDER_CONFIRMATION_TEMPLATE);
				orderModel = (OrderModel) ((CatPlaceBCPReOrderProcessModel) process).getOrder();
				fronEndTemplateName = CAT_BCP_ORDER_CONFIRMATION_TEMPLATE;
			}
			else
			{
				LOG.debug("inside else loop CatOrderConfirmationEmailAction -");
				orderModel = (OrderModel) ((CatPlaceOrderProcessModel) process).getOrder();
				final String truckId = (orderModel.getEntries().iterator().hasNext())
						? orderModel.getEntries().iterator().next().getTruckloadId() : StringUtils.EMPTY;
				if (StringUtils.isNotEmpty(truckId) && truckId.contains(CatCoreConstants.MIX_AND_MATCH_TRUCK))
				{
					fronEndTemplateName = CAT_TRUCKLOAD_ORDER_CONFIRMATION_EMAIL_TEMPLATE;
				}
				else
				{
					fronEndTemplateName = CAT_CUV_ORDER_CONFIRMATION_EMAIL_TEMPLATE;
				}
			}
			eventService.publishEvent(new CatOrderConfirmationEmailEvent(orderModel, fronEndTemplateName, true));
			return Transition.OK;
		}
		return Transition.NOK;
	}
}
