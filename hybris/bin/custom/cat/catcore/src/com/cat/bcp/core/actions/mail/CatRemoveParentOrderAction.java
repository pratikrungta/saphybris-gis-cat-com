/**
 *
 */
package com.cat.bcp.core.actions.mail;

import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;

import org.apache.log4j.Logger;

import com.cat.bcp.core.model.CatOrderConfirmationProcessModel;


/**
 * @author asomjal
 *
 */
public class CatRemoveParentOrderAction extends AbstractSimpleDecisionAction<CatOrderConfirmationProcessModel>
{

	private static final Logger LOG = Logger.getLogger(CatRemoveParentOrderAction.class);

	/**
	 * This method is used to remove parent Order. {@inheritDoc}
	 */
	@Override
	public Transition executeAction(final CatOrderConfirmationProcessModel process) throws CalculationException
	{
		LOG.info("******Inside CatRemoveParentOrderAction for Email******");
		if (process != null && process.getOrder() != null && process.getRemoveParentOrder() != null)
		{
			if (process.getRemoveParentOrder().booleanValue())
			{
				modelService.remove(process.getOrder());
			}
			return Transition.OK;
		}
		return Transition.NOK;
	}

}
