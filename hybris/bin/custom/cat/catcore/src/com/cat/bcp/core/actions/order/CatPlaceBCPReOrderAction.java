/**
 *
 */
package com.cat.bcp.core.actions.order;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.OrderService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.dao.CatCustomerDao;
import com.cat.bcp.core.model.CatPlaceBCPReOrderProcessModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.FBCPartsOrderInfoModel;
import com.cat.bcp.core.order.CatCommerceCheckoutService;
import com.cat.bcp.core.util.CatOrderUtils;


/**
 * This is an asynchronous process to split BCP order into multiple orders, with each order containing a single quantity
 * with a single order entry.
 *
 * @author vjagannadharaotel
 *
 */
public class CatPlaceBCPReOrderAction extends AbstractSimpleDecisionAction<CatPlaceBCPReOrderProcessModel>
{

	private static final Logger LOG = Logger.getLogger(CatPlaceBCPReOrderAction.class);

	@Resource(name = "orderService")
	private OrderService orderService;

	@Resource(name = "orderCodeGenerator")
	private KeyGenerator keyGenerator;

	@Resource(name = "calculationService")
	private CalculationService calculationService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "catalogVersionService")
	CatalogVersionService catalogVersionService;

	@Resource(name = "catCustomerDao")
	private CatCustomerDao catCustomerDao;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "catCommerceCheckoutService")
	private CatCommerceCheckoutService commerceCheckoutService;

	@Resource(name = "catOrderUtils")
	private CatOrderUtils catOrderUtils;

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/** The Constant DEFAULT_ORDER_QTY. */
	private static final String DEFAULT_ORDER_QTY = "1";

	private static final String CATALOG_NAME = "NAProductCatalog";

	private static final String CATALOG_VERSION = "Online";

	public KeyGenerator getKeyGenerator()
	{
		return keyGenerator;
	}

	/**
	 * This method is to split the BCP order into multiple orders with each order containing only one order entry of a
	 * single quantity.
	 *
	 * {@inheritDoc}
	 */
	@Override
	public Transition executeAction(final CatPlaceBCPReOrderProcessModel process) throws CalculationException
	{
		if (process != null && process.getOrder() != null)
		{
			LOG.debug("*** Entered executeAction of Split process  ***");
			final OrderModel parentOrder = (OrderModel) process.getOrder();
			final List<AbstractOrderEntryModel> orderEntriesList = new ArrayList<>();

			final List<OrderModel> orderList = new ArrayList<>();
			addOrderEntries(parentOrder, orderEntriesList);

			final List<Long> quantityList = new ArrayList<>();

			catOrderUtils.setOrderEntries(process.getOrder(), orderEntriesList, orderList, quantityList);

			createOrders(process, orderList, parentOrder);
			modelService.removeAll(orderList);
			LOG.debug("*** Exiting executeAction of Split process  ***");
			return Transition.OK;
		}
		return Transition.NOK;
	}



	/**
	 * Adds the order entries.
	 *
	 * @param parentOrder
	 *           the parent order
	 * @param orderEntriesList
	 *           the order entries list
	 */
	private void addOrderEntries(final OrderModel parentOrder, final List<AbstractOrderEntryModel> orderEntriesList)
	{
		if (CollectionUtils.isNotEmpty(parentOrder.getEntries()))
		{
			for (final AbstractOrderEntryModel entry : parentOrder.getEntries())
			{
				if (entry.getProduct() != null && (entry.getProduct() instanceof ConfigVariantProductModel))
				{
					orderEntriesList.add(entry);
				}
			}
		}
	}

	/**
	 * Creates the orders.
	 *
	 * @param process
	 *           the process
	 * @param orderList
	 *           the order list
	 * @param parentOrderId
	 *           the parent order id
	 * @throws CalculationException
	 *            the calculation exception
	 */
	private void createOrders(final CatPlaceBCPReOrderProcessModel process, final List<OrderModel> orderList,
			final OrderModel parentOrder) throws CalculationException
	{
		final Map<String, List<String>> poMap = process.getOrder().getPoNumberMap();
		LOG.debug("*** Entered createOrders method of Split process  ***");
		for (final OrderModel interimOrder : orderList)
		{
			final int entryNumber = interimOrder.getEntries().get(0).getEntryNumber();
			if (CollectionUtils.isNotEmpty(interimOrder.getEntries()))
			{
				final ConfigVariantProductModel configProduct = (ConfigVariantProductModel) interimOrder.getEntries().get(0)
						.getProduct();
				LOG.debug("*** Product Code : *** " + configProduct.getCode());
				for (int iterator = 0; iterator < interimOrder.getEntries().get(0).getQuantity().intValue(); iterator++)
				{
					createOrders(interimOrder, poMap, configProduct, iterator, parentOrder, entryNumber);
				}

			}
			modelService.save(process);
		}
	}


	/**
	 * Creates the orders.
	 *
	 * @param interimOrder
	 *           the interim order
	 * @param poMap
	 *           the po map
	 * @param configProduct
	 *           the config product
	 * @param iter
	 *           the iter
	 * @param entryNumber
	 * @param parentOrderId
	 *           the parent order id
	 * @throws CalculationException
	 *            the calculation exception
	 */
	private void createOrders(final OrderModel interimOrder, final Map<String, List<String>> poMap,
			final ConfigVariantProductModel configProduct, final int iter, final OrderModel parentOrderModel,
			final Integer entryNumber) throws CalculationException
	{
		final String clonedOrderCode = String.valueOf(getKeyGenerator().generate());
		final OrderModel localOrder = orderService.clone(null, null, interimOrder, clonedOrderCode);
		final String productCode = configProduct.getCode();
		if (CollectionUtils.isNotEmpty(poMap.get(productCode + "__" + entryNumber)))
		{
			final List<String> poList = new ArrayList<>(poMap.get(productCode + "__" + entryNumber));
			localOrder.setPurchaseOrderNumber(poList.get(iter));
		}

		localOrder.setLaneType(parentOrderModel.getLaneType());
		localOrder.setStatus(OrderStatus.PENDING_SUBMISSION);
		if (null != localOrder.getEntries().get(0).getConfigurable()
				&& BooleanUtils.isTrue(localOrder.getEntries().get(0).getConfigurable()))
		{
			localOrder.setOrderGroupId(parentOrderModel.getCode() + "__" + entryNumber);
			final FBCPartsOrderInfoModel fbcPartsInfoModel = new FBCPartsOrderInfoModel();
			fbcPartsInfoModel.setOrderId(parentOrderModel.getCode());
			fbcPartsInfoModel.setEntryId(entryNumber.toString());
			fbcPartsInfoModel.setConfigurationId(productCode);

			final List<FBCPartsOrderInfoModel> fbcModelList = flexibleSearchService.getModelsByExample(fbcPartsInfoModel);
			for (final FBCPartsOrderInfoModel fbcModel : fbcModelList)
			{
				fbcModel.setOrderId(parentOrderModel.getCode() + "__" + entryNumber);
				modelService.save(fbcModel);
			}
		}
		else
		{
			localOrder.setOrderGroupId(parentOrderModel.getCode());
		}
		modelService.save(localOrder);
		catalogVersionService.setSessionCatalogVersion(CATALOG_NAME, CATALOG_VERSION);
		if (CollectionUtils.isNotEmpty(localOrder.getEntries()))
		{
			for (final AbstractOrderEntryModel entry : localOrder.getEntries())
			{
				if (BooleanUtils.isFalse(entry.getConfigurable()))
				{
					entry.setQuantity(Long.valueOf(DEFAULT_ORDER_QTY));
					entry.setTotalPrice(entry.getBasePrice());
					localOrder.setTotalPrice(entry.getBasePrice());
				}
				else
				{
					entry.setQuantity(Long.valueOf(DEFAULT_ORDER_QTY));
				}
				modelService.save(entry);
			}
			modelService.save(localOrder);
		}
		commerceCheckoutService.createAdditionalInfo(localOrder, productCode);

	}

}
