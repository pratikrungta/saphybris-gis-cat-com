/**
 *
 */
package com.cat.bcp.core.actions.order;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.OrderService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.task.RetryLaterException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.dao.CatCustomerDao;
import com.cat.bcp.core.enums.LaneTypeEnum;
import com.cat.bcp.core.model.CatPlaceOrderProcessModel;
import com.cat.bcp.core.order.CatCommerceCheckoutService;
import com.cat.bcp.core.util.CatOrderUtils;


/**
 * @author sparkibanda
 *
 */
public class CatPlaceOrderAction extends AbstractSimpleDecisionAction<CatPlaceOrderProcessModel>
{
	private static final Logger LOG = Logger.getLogger(CatPlaceOrderAction.class);

	@Resource(name = "orderService")
	private OrderService orderService;

	@Resource(name = "orderCodeGenerator")
	private KeyGenerator keyGenerator;

	@Resource(name = "calculationService")
	private CalculationService calculationService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "catalogVersionService")
	CatalogVersionService catalogVersionService;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "catCustomerDao")
	private CatCustomerDao catCustomerDao;

	@Resource(name = "catCommerceCheckoutService")
	private CatCommerceCheckoutService commerceCheckoutService;

	@Resource(name = "catOrderUtils")
	private CatOrderUtils catOrderUtils;

	/** The Constant DEFAULT_ORDER_QTY. */
	private static final String DEFAULT_ORDER_QTY = "1";

	private static final String CATALOG_NAME = "NAProductCatalog";

	private static final String CATALOG_VERSION = "Online";

	public KeyGenerator getKeyGenerator()
	{
		return keyGenerator;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.processengine.action.AbstractSimpleDecisionAction#executeAction(de.hybris.platform.
	 * processengine .model.BusinessProcessModel)
	 */
	@Override
	public Transition executeAction(final CatPlaceOrderProcessModel process) throws RetryLaterException, Exception
	{
		LOG.info("inside CatPlaceOrderAction for CUV- start");

		if (process != null && process.getOrder() != null && CollectionUtils.isNotEmpty(process.getPOList()))
		{
			final OrderModel parentOrder = (OrderModel) process.getOrder();

			final List<AbstractOrderEntryModel> orderEntriesList = new ArrayList<>();

			final List<OrderModel> orderList = new ArrayList<>();
			if (CollectionUtils.isNotEmpty(parentOrder.getEntries()))
			{
				for (final AbstractOrderEntryModel entry : parentOrder.getEntries())
				{
					orderEntriesList.add(entry);
				}
			}

			final List<Long> quantityList = new ArrayList<>();

			catOrderUtils.setOrderEntries(process.getOrder(), orderEntriesList, orderList, quantityList);

			createOrders(process, orderList, quantityList);

			modelService.removeAll(orderList);
			LOG.info("CUV order splitting completed.  Call trigger email action.");
			return Transition.OK;
		}
		LOG.info("inside CatPlaceOrderAction for CUV- error. Transition NOK.");
		return Transition.NOK;
	}

	/**
	 * @param process
	 * @param orderList
	 * @param quantityList
	 * @throws CalculationException
	 */
	private void createOrders(final CatPlaceOrderProcessModel process, final List<OrderModel> orderList,
			final List<Long> quantityList) throws CalculationException
	{
		int orderIter = 0;
		int entryIter = 0;
		final String orderGroupId = process.getOrder().getCode();

		for (int poNumberIter = 0; poNumberIter < process.getPOList().size(); poNumberIter++)
		{
			final String clonedOrderCode = String.valueOf(getKeyGenerator().generate());
			final OrderModel localOrder = orderService.clone(null, null, orderList.get(orderIter), clonedOrderCode);
			localOrder.setPurchaseOrderNumber(process.getPOList().get(poNumberIter));
			entryIter++;
			if (entryIter == quantityList.get(orderIter).intValue())
			{
				orderIter++;
				entryIter = 0;
			}
			//CUV orders will always be LANE3
			localOrder
					.setLaneType((process.getOrder().getLaneType() != null) ? process.getOrder().getLaneType() : LaneTypeEnum.LANE3);

			localOrder.setStatus(OrderStatus.PENDING_SUBMISSION);
			String productCode = null;
			if (CollectionUtils.isNotEmpty(localOrder.getEntries()))
			{
				Date shipToAfterDate = null;
				String truckloadId = "";
				AddressModel salesAreaShippingAddress = null;
				for (final AbstractOrderEntryModel entry : localOrder.getEntries())
				{
					productCode = entry.getProduct().getCode();
					entry.setQuantity(Long.valueOf(DEFAULT_ORDER_QTY));
					entry.setTotalPrice(entry.getProduct().getPriceQuantity());
					salesAreaShippingAddress = entry.getShippingAddress();
					shipToAfterDate = entry.getShipToAfter();
					truckloadId = entry.getTruckloadId();
					modelService.save(entry);

				}
				if (salesAreaShippingAddress != null && salesAreaShippingAddress.getOwner() instanceof B2BUnitModel)
				{
					final B2BUnitModel salesAreaDealer = (B2BUnitModel) salesAreaShippingAddress.getOwner();
					salesAreaShippingAddress.setSapCustomerID(salesAreaDealer.getUid());
					localOrder.setDeliveryAddress(salesAreaShippingAddress);
				}
				localOrder.setShipToAfter(shipToAfterDate);
				localOrder.setTruckloadId(truckloadId);

			}

			localOrder.setOrderGroupId(orderGroupId);
			catalogVersionService.setSessionCatalogVersion(CATALOG_NAME, CATALOG_VERSION);
			calculationService.calculate(localOrder);

			modelService.save(localOrder);

			modelService.save(process);


			//Additional Data for order related to product
			commerceCheckoutService.createAdditionalInfo(localOrder, productCode);
		}
	}
}
