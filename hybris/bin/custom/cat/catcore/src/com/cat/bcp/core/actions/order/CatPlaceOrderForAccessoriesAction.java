/**
 *
 */
package com.cat.bcp.core.actions.order;

import de.hybris.platform.b2b.model.B2BCommentModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.processengine.action.AbstractSimpleDecisionAction;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.model.CatPlaceOrderProcessModel;
import com.cat.bcp.core.model.OrderAdditionalInfoModel;
import com.cat.bcp.core.order.CatCommerceCheckoutService;
import com.cat.bcp.core.servicelayer.CatCategoryService;


/**
 * This class is used for splitting Accessories Order as per the compatible Sales Model.
 *
 * @author asomjal
 *
 */
public class CatPlaceOrderForAccessoriesAction extends AbstractSimpleDecisionAction<CatPlaceOrderProcessModel>
{

	private static final Logger LOG = Logger.getLogger(CatPlaceOrderForAccessoriesAction.class);

	private static final String CATALOG_NAME = "NAProductCatalog";

	private static final String CATALOG_VERSION = "Online";

	private static final String ACCESSORIES_CATEGORY_CODE = "accessory";

	@Resource(name = "catalogVersionService")
	CatalogVersionService catalogVersionService;

	@Resource(name = "categoryService")
	private CatCategoryService categoryService;

	@Resource(name = "orderCodeGenerator")
	private KeyGenerator keyGenerator;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "calculationService")
	private CalculationService calculationService;

	@Resource(name = "catCommerceCheckoutService")
	private CatCommerceCheckoutService commerceCheckoutService;

	public KeyGenerator getKeyGenerator()
	{
		return keyGenerator;
	}

	/**
	 * This method is used to Split Accessories Order.
	 *
	 * @param process
	 *           CatPlaceOrderProcessModel Object
	 * @return Transition Object.
	 */
	@Override
	public Transition executeAction(final CatPlaceOrderProcessModel process) throws CalculationException
	{
		LOG.debug("******Starting business process for Accessories******");
		if (process != null && process.getOrder() != null)
		{
			final OrderModel parentOrder = (OrderModel) process.getOrder();
			if (CollectionUtils.isNotEmpty(parentOrder.getEntries()))
			{
				final Map<String, List<AbstractOrderEntryModel>> compatibleModelAbstractOrderEntryModelMap = parentOrder.getEntries()
						.stream().collect(Collectors.groupingBy(AbstractOrderEntryModel::getCompatibleModel));
				for (final Map.Entry<String, List<AbstractOrderEntryModel>> compatibleModelAbstractOrderEntryModelMapEntry : compatibleModelAbstractOrderEntryModelMap
						.entrySet())
				{
					final OrderModel localOrder = modelService.create(OrderModel.class);
					setBasicAttributes(parentOrder, localOrder);
					modelService.save(localOrder);
					modelService.refresh(localOrder);
					final List<AbstractOrderEntryModel> abstractOrderEntryModelList = compatibleModelAbstractOrderEntryModelMap
							.get(compatibleModelAbstractOrderEntryModelMapEntry.getKey());
					addProductEntriesForLocalOrder(localOrder, abstractOrderEntryModelList);
					localOrder.setStatus(OrderStatus.PENDING_SUBMISSION);
					localOrder.setOrderGroupId(parentOrder.getCode());
					catalogVersionService.setSessionCatalogVersion(CATALOG_NAME, CATALOG_VERSION);
					calculationService.calculate(localOrder);
					localOrder.setCompatibleModel(compatibleModelAbstractOrderEntryModelMapEntry.getKey());
					modelService.save(localOrder);
					//Additional Data for order related to product
					addAdditionalDataForOrder(localOrder, abstractOrderEntryModelList);
				}
			}
			modelService.save(process);

			return Transition.OK;
		}
		return Transition.NOK;
	}

	/**
	 * This method is used to add product entries for Local Order.
	 *
	 * @param localOrder
	 *           OrderModel Object
	 * @param abstractOrderEntryModelList
	 *           List of AbstractOrderEntryModel
	 */
	private void addProductEntriesForLocalOrder(final OrderModel localOrder,
			final List<AbstractOrderEntryModel> abstractOrderEntryModelList)
	{
		if (CollectionUtils.isNotEmpty(abstractOrderEntryModelList))
		{
			localOrder.setPurchaseOrderNumber(abstractOrderEntryModelList.iterator().next().getAccessoriesPONumber());
			for (final AbstractOrderEntryModel abstractOrderEntryModel : abstractOrderEntryModelList)
			{
				final AbstractOrderEntryModel clonedEntry = modelService.clone(abstractOrderEntryModel);
				clonedEntry.setOrder(localOrder);
				modelService.save(clonedEntry);
			}
		}
	}

	/**
	 * This method is used to add Additional Data for Order.
	 *
	 * @param localOrder
	 *           OrderModel Object
	 * @param abstractOrderEntryModelList
	 *           List of AbstractOrderEntryModel
	 */
	private void addAdditionalDataForOrder(final OrderModel localOrder,
			final List<AbstractOrderEntryModel> abstractOrderEntryModelList)
	{
		final Optional<AbstractOrderEntryModel> abstractOrderEntryModel = abstractOrderEntryModelList.stream().findFirst();
		if (abstractOrderEntryModel.isPresent())
		{
			final OrderAdditionalInfoModel orderAdditionalInfoModel = modelService.create(OrderAdditionalInfoModel.class);
			orderAdditionalInfoModel.setOrder(localOrder);
			orderAdditionalInfoModel.setProductFamily(categoryService.getCategoryForCode(ACCESSORIES_CATEGORY_CODE));
			modelService.save(orderAdditionalInfoModel);
		}
	}

	/**
	 * Sets the basic attributes.
	 *
	 * @param orderModel
	 *           the order model
	 * @param localOrder
	 *           the local order
	 */
	private void setBasicAttributes(final AbstractOrderModel orderModel, final OrderModel localOrder)
	{
		final String clonedOrderCode = String.valueOf(getKeyGenerator().generate());
		localOrder.setCode(clonedOrderCode);
		localOrder.setUser(orderModel.getUser());
		localOrder.setDate(new Date());
		localOrder.setUnit(orderModel.getUnit());
		localOrder.setStore(orderModel.getStore());
		localOrder.setSite(orderModel.getSite());
		localOrder.setCurrency(orderModel.getCurrency());
		if (baseStoreService.getCurrentBaseStore() != null)
		{
			localOrder.setStore(baseStoreService.getCurrentBaseStore());
		}
		if (baseSiteService.getCurrentBaseSite() != null)
		{
			localOrder.setSite(baseSiteService.getCurrentBaseSite());
		}
		localOrder.setDeliveryAddress(orderModel.getDeliveryAddress());
		localOrder.setPaymentAddress(orderModel.getPaymentAddress());
		setReorderComments(orderModel, localOrder);

	}

	/**
	 * Method to set re-order comment
	 *
	 * @param orderModel
	 *           orderModel Object
	 * @param localOrder
	 *           orderModel Object
	 */
	private void setReorderComments(final AbstractOrderModel orderModel, final OrderModel localOrder)
	{
		if (CollectionUtils.isNotEmpty(orderModel.getB2bcomments()))
		{
			final Optional<B2BCommentModel> parentOrderComment = orderModel.getB2bcomments().stream().findFirst();
			if (parentOrderComment.isPresent())
			{
				final B2BCommentModel reOdercommnt = modelService.create(B2BCommentModel.class);
				reOdercommnt.setComment(parentOrderComment.get().getComment());
				modelService.save(reOdercommnt);
				localOrder.setB2bcomments(Collections.singleton(reOdercommnt));
			}
		}
	}
}
