/**
 *
 */
package com.cat.bcp.core.component.container.impl;


import de.hybris.platform.acceleratorcms.component.container.CMSComponentContainerStrategy;
import de.hybris.platform.acceleratorcms.model.components.CMSTabParagraphContainerModel;
import de.hybris.platform.cms2.model.contents.components.AbstractCMSComponentModel;
import de.hybris.platform.cms2.model.contents.components.SimpleCMSComponentModel;
import de.hybris.platform.cms2.model.contents.containers.AbstractCMSComponentContainerModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.type.TypeService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Required;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.servicelayer.CatProductService;


/**
 * @author bidavda
 *
 */
public class CatDefaultCMSTabParagraphContainerStrategy implements CMSComponentContainerStrategy
{
	private TypeService typeService;
	private List<String> showContainerForTypes;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "catProductService")
	private CatProductService catProductService;

	private static final String SPECIFICATIONS_TAB = "ProductSpecificationsTabComponent";
	private static final String ACCESSORIES_TAB = "ProductAccessoriesTabComponent";
	private static final String PARTS_TAB = "ProductPartsTabComponent";
	private static final String INVENTORY_TAB = "ProductInventoryTabComponent";
	private static final String INTRANSIT_MACHINES_TAB = "ProductInTransitMachinesTabComponent";
	private static final String FEATURES_TAB = "ProductFeaturesTabComponent";
	private static final String INVENTORY_AGE_TAB = "ProductInventoryAgeTabComponent";
	private static final String SEARCH_PARAMETERS_TAB = "ProductSearchParamTabComponent";


	/**
	 * Get the list of components that should be displayed in place of the container. For some use cases it is necessary
	 * for the CMS Container to be replaced at render time with a list of child components. If the CMS Container itself
	 * should be rendered then it should return a list containing itself, see the
	 * {@link de.hybris.platform.acceleratorcms.component.container.impl.IdentityCMSComponentContainerStrategy}.
	 *
	 * @param container
	 *           The container
	 * @return The list of components to display in place of the container
	 */
	@Override
	public List<AbstractCMSComponentModel> getDisplayComponentsForContainer(final AbstractCMSComponentContainerModel container)
	{
		final ProductModel product = sessionService.getAttribute("ProductModel");

		final String productType = catProductService.getProductType(product);

		final List<AbstractCMSComponentModel> components = (List) container.getSimpleCMSComponents();

		if (container instanceof CMSTabParagraphContainerModel && needShowContainer(components))
		{
			final List<SimpleCMSComponentModel> productsTabs = new ArrayList<>();

			if (CatCoreConstants.UTV.equals(productType))
			{
				populateUTVProductTabs(productsTabs, components);
			}
			else if (CatCoreConstants.NEW.equals(productType))
			{
				populateNewProductTabs(productsTabs, components);
			}
			else if (CatCoreConstants.USED.equals(productType))
			{
				populateUsedProductTabs(productsTabs, components);
			}

			container.setSimpleCMSComponents(productsTabs);

			return Arrays.asList(container);
		}

		return components;

	}

	/**
	 * @param productsTabs
	 * @param components
	 */
	private void populateUTVProductTabs(final List<SimpleCMSComponentModel> productsTabs,
			final List<AbstractCMSComponentModel> components)
	{
		for (final AbstractCMSComponentModel component : components)
		{
			if (component.getUid().equalsIgnoreCase(SPECIFICATIONS_TAB) || component.getUid().equalsIgnoreCase(ACCESSORIES_TAB)
					|| component.getUid().equalsIgnoreCase(SEARCH_PARAMETERS_TAB))
			{
				productsTabs.add((SimpleCMSComponentModel) component);
			}
		}
	}

	/**
	 * @param productsTabs
	 * @param components
	 */
	private void populateNewProductTabs(final List<SimpleCMSComponentModel> productsTabs,
			final List<AbstractCMSComponentModel> components)
	{
		for (final AbstractCMSComponentModel component : components)
		{
			final boolean isNewProductTab = component.getUid().equalsIgnoreCase(INVENTORY_TAB)
					|| component.getUid().equalsIgnoreCase(INTRANSIT_MACHINES_TAB)
					|| component.getUid().equalsIgnoreCase(SEARCH_PARAMETERS_TAB);
			if (component.getUid().equalsIgnoreCase(PARTS_TAB) || isNewProductTab)
			{
				productsTabs.add((SimpleCMSComponentModel) component);
			}

			if (component.getUid().equalsIgnoreCase(INVENTORY_AGE_TAB))
			{
				productsTabs.add((SimpleCMSComponentModel) component);
			}
		}
	}

	/**
	 * @param productsTabs
	 * @param components
	 */
	private void populateUsedProductTabs(final List<SimpleCMSComponentModel> productsTabs,
			final List<AbstractCMSComponentModel> components)
	{
		for (final AbstractCMSComponentModel component : components)
		{
			if (component.getUid().equalsIgnoreCase(FEATURES_TAB) || component.getUid().equalsIgnoreCase(SEARCH_PARAMETERS_TAB))
			{
				productsTabs.add((SimpleCMSComponentModel) component);
			}
		}
	}

	/**
	 * check whether show the container in corresponding jsp
	 *
	 * @param components
	 *           the children components
	 * @return children components all match given types then return true otherwise false
	 */
	protected boolean needShowContainer(final List<AbstractCMSComponentModel> components)
	{
		return components.stream().allMatch(component -> getShowContainerForTypes()
				.contains(getTypeService().getComposedTypeForClass(component.getClass()).getCode()));
	}

	protected TypeService getTypeService()
	{
		return typeService;
	}

	@Required
	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}

	protected List<String> getShowContainerForTypes()
	{
		return showContainerForTypes;
	}

	@Required
	public void setShowContainerForTypes(final List<String> showContainerForTypes)
	{
		this.showContainerForTypes = showContainerForTypes;
	}

}
