/*
' * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.core.constants;

import com.cat.bcp.core.model.DeliveryInfoModel;


/**
 * Global class for all CatCore constants. You can add global constants for your extension into this class.
 */
public final class CatCoreConstants extends GeneratedCatCoreConstants
{
	public static final String EXTENSIONNAME = "catcore";


	// implement here constants used by this extension
	public static final String QUOTE_BUYER_PROCESS = "quote-buyer-process";
	public static final String QUOTE_SALES_REP_PROCESS = "quote-salesrep-process";
	public static final String QUOTE_USER_TYPE = "QUOTE_USER_TYPE";
	public static final String QUOTE_SELLER_APPROVER_PROCESS = "quote-seller-approval-process";
	public static final String QUOTE_TO_EXPIRE_SOON_EMAIL_PROCESS = "quote-to-expire-soon-email-process";
	public static final String QUOTE_EXPIRED_EMAIL_PROCESS = "quote-expired-email-process";
	public static final String QUOTE_POST_CANCELLATION_PROCESS = "quote-post-cancellation-process";

	public static final String ENGINE_MODEL = "ENGINE_MODEL";
	public static final String NET_POWER = "NET_POWER";
	public static final String OPERATING_WEIGHT = "OPERATING_WEIGHT";

	public static final String FUEL_TYPE = "FUEL_TYPE";
	public static final String MAX_SPEED = "MAX_SPEED";
	public static final String SEAT_CAPACITY = "SEAT_CAPACITY";

	public static final String PRIMARY_IMAGE_URL = "img-800Wx600H";
	public static final String AVAILABLE_WAREHOUSES = "availableInWarehouse";
	public static final String MIN_QTY = "minimumStockLevel";
	public static final String MAX_QTY = "maximumStockLevel";
	public static final String RECOMMENDED_QTY = "recommendedStockLevel";
	public static final String INVENTORY_STATUS = "inventoryStatus";
	public static final String PRIMARY_OFFERING_ID = "primaryOfferingId";
	public static final String PRIMARY_OFFERING_NAME = "primaryOfferingName";
	public static final String SALES_MODEL_ID = "salesModelId";
	public static final String SALES_MODEL_NAME = "salesModelName";
	public static final String CATEGORY = "category";
	public static final String CATEGORY_NAME = "categoryName_string_mv";

	public static final String BRANCH_LOCATION = "BRANCH_LOCATION";
	public static final String MANUFACTURING_YEAR = "MANUFACTURING_YEAR";
	public static final String HOURS_USED = "HOURS_USED";
	public static final String PRICE = "PRICE";
	public static final String DEALER_CERTIFIED = "DEALER_CERTIFIED";

	public static final String SORT = ":relevance";

	public static final String WAREHOUSE_PARAM = ":availableInWarehouse:";
	public static final String PDC_PARAM = ":inStockPDCFlag:";
	public static final String MFU_PARAM = ":inStockMFUFlag:";

	public static final String UTV = "UTV";
	public static final String IM = "IM";
	public static final String SP = "SP";

	public static final String INVENTORY_MANAGER = "Inventory Manager";
	public static final String INVENTORY_MANAGER_UTV = "Inventory Manager UTV";
	public static final String SALES_PERSON = "Sales Person";

	public static final String IS_INVENTORY_MANAGER = "isIM";

	public static final String CATEGORY_PARAM = ":category:";
	public static final String SALESMODEL_PARAM = ":salesModelId:";
	public static final String PRIMARY_OFFERING_PARAM = ":primaryOfferingId:";
	public static final String CATEGORY_NAME_PARAM = ":categoryName:";

	public static final String SALESMODEL_NAME_STRING = "salesModelName_string$";
	public static final String PRIMARY_OFFERING_NAME_STRING = "primaryOfferingName_string$";
	public static final String CATEGORY_NAME_STRING = "categoryName_string_mv$";

	public static final String LAST_LINK_CLASS = "active";
	public static final String MIDDLE_LINK_CLASS = "";
	public static final String CONFIGURATIONS = "Configurations";
	public static final String LINK_URL = "#";
	public static final String CONFIG_LINK_URL_NEW = "/search/view/?categoryCode=p1&salesModelId=p2&primaryOfferingId=p3&pdcFlag=p4&mfuFlag=p5&wsFlag=p6&page=p7&laneType=p8";
	public static final String CONFIG_LINK_URL_USED = "/search/view/?categoryCode=p1&salesModelId=p2&primaryOfferingId=p3&page=p7&branchLocation=p8&manufacturingYearMin=p9&manufacturingYearMax=p10&hoursUsedMin=p11&hoursUsedMax=p12&priceMin=p13&priceMax=p14&dealerCertified=p15&isUsed=true";

	public static final String PDC = "PDC";
	public static final String MFU = "MFU";

	public static final String SOLR_TO = " TO ";
	public static final String SOLR_HOURS_USED = ":HOURS_USED:";
	public static final String SOLR_PRICE = ":PRICE:";
	public static final String SOLR_MANUFACTURING_YEAR = ":MANUFACTURING_YEAR:";
	public static final String SOLR_BRANCH_LOCATION = ":BRANCH_LOCATION:";
	public static final String SOLR_DEALER_CERTIFIED = ":DEALER_CERTIFIED:";

	public static final String INVENTORY_MANAGER_GRP = "dealerInventoryManagerGrp";
	public static final String INVENTORY_MANAGER_UTV_GRP = "dealerUTVManagerGrp";
	public static final String SALES_PERSON_GRP = "dealerSalesPersonGrp";

	public static final String NONDISPLAYNAME = "nondisplayname";
	public static final String MARKETING_CONTENT = "marketing_content";
	public static final String CONTENT = "content";
	public static final String MEDIA = "media";
	public static final String TYPE = "type";
	public static final String ID = "id";
	public static final String URLFORMAT = "Cat.scene7.UrlFormat";
	public static final String SORTVALUE = "sort";
	public static final String LEVEL = "level";
	public static final String IMAGE = "image";
	public static final String MEDIA_FORMAT = "Cat.media.format";
	public static final String MEDIA_XML_URL = "Cat.scene7.media.xml.url";
	public static final String CAT_PRODUCT_CATALOG = "Cat.Product.Catalog";
	public static final String CAT_PRODUCT_CATALOG_VERSION = "Cat.Product.Catalog.version";
	public static final String CAT_CONTENT_CATALOG = "cat.content.catalog";
	public static final String CAT_CONTENT_CATALOG_VERSION = "cat.content.catalog.version";
	public static final String CAT_PRODUCT_MEDIA_XML_URL = "Cat.scene7.product.media.xml.url";
	public static final String CAT_PRODUCT_MEDIA_XML_SEPERATOR = "Cat.scene7.product.media.xml.seperator";
	public static final String CAT_PRODUCT_MEDIA_XML_EXTENSION = "Cat.scene7.product.media.xml.extension";
	public static final String TYPE_MEDIA_PDF = "Cat.scene7.media.type.pdf";
	public static final String TYPE_MEDIA_IMAGE = "Cat.scene7.media.type.image";
	public static final String TYPE_MEDIA_VIDEO = "Cat.scene7.media.type.video";
	public static final String MEDIA_QUERY = "Cat.scene7.media.flexibleQuery";
	public static final String PDF = "PDF";
	public static final String MODEL_SAVE_EXCEPTION = "exception occured while saving model : ";
	public static final String MODEL_NOTFOUND_EXCEPTION = "model not found exception: ";
	public static final String DOCUMENT = "document";
	public static final String SUFFIX = "Cat.scene7.suffix";
	public static final String COMMA = ",";
	public static final String HYPHEN = "-";
	public static final String PERIOD = ".";
	public static final String EXCEL = "xlsx";
	public static final String IS_REORDERABLE = "isReorderable";
	public static final String FACET = "facet";
	public static final String PRODUCT = "product";

	public static final String SLASH = "/";

	public static final String SEMICOLON = ";";
	public static final String KEY = "key";
	public static final String NAME = "name";
	public static final String UNDERSCORE = "_";
	public static final String METADATA = "metadata";
	public static final String LISTOFMETADATA = "listofmetadata";
	public static final String CSV_FILEPATH = "cat.csv.write.path";
	public static final String CSV_FILENAME = "cat.csv.writer.file";
	public static final String CSV_DELIMITER = "cat.csv.writer.delimiter";
	public static final String CSV_HEADER = "cat.csv.writer.header";
	public static final String CSV_ISSINGLEFILE = "cat.csv.issinglefile";


	public static final String CSV_FILENAME_PRODUCT = "cat.csv.writer.file.product";
	public static final String CSV_HEADER_PRODUCT = "cat.csv.writer.header.product";


	public static final String CSV_FILEPATH_CPC = "cat.csv.write.path.cpc";
	public static final String CSV_FILENAME_MEDIA = "cat.csv.writer.file.media";
	public static final String CSV_HEADER_MEDIA = "cat.csv.writer.header.media";
	public static final String CSV_FILE_FORMAT = "cat.csv.writer.file.format";
	public static final String CSV_FILE_VALUE = "-001";

	public static final String CAT_CATALOG = "NAProductCatalog";
	public static final String CAT_VERSION = "Online";




	public static final String POPULATE_BASE_MODELS_QUERY = "Select DISTINCT({base.pk}) "
			+ " From {Product! as sales}, {BaseVariantProduct! as base}, {ConfigVariantProduct as config} "
			+ " WHERE {sales.pk} = {base.baseProduct} AND {config.baseProduct} = {base.pk} " + "AND {sales.pk} = ?salesModel "
			+ " AND {sales.catalogVersion} IN ({{ SELECT {PK} FROM {CatalogVersion} WHERE {version}='Online' AND {catalog} IN ({{ Select {pk} from {Catalog} where {id} = 'NAProductCatalog'}})}})";

	public static final String POPULATE_SALES_MODELS_QUERY = "Select DISTINCT({sales.pk}) "
			+ "From {Product as sales},{BaseVariantProduct as base},{ConfigVariantProduct as config},{category as cat}, {CategoryProductRelation as rel} "
			+ "where {sales.pk} = {base.baseproduct} AND {config.baseProduct} = {base.pk} "
			+ " AND {cat.code} = ?Category AND  {sales.pk}={rel.target} AND {cat.pk}={rel.source}"
			+ " AND {sales.catalogVersion} IN ({{ SELECT {PK} FROM {CatalogVersion} WHERE {version}='Online' AND {catalog} IN ({{ Select {pk} from {Catalog} where {id} = 'NAProductCatalog'}})}})";

	public static final String POPULATE_SALES_MODELS_USED_QUERY = "select DISTINCT({sales.pk}) from {Product as sales},{BaseVariantProduct as base},{Category as cat},"
			+ "{CategoryProductRelation as rel} where {sales.pk}={base.baseProduct} and {base.productCondition} in ({{select {pk} from {ProductConditionEnum} where {code} in ('Used')}}) "
			+ "and {base.pk}={rel.target} AND {cat.pk}={rel.source} AND {cat.code}=?Category "
			+ "and {sales.catalogVersion} IN ({{ SELECT {PK} FROM {CatalogVersion} WHERE {version}='Online' AND {catalog} IN ({{ Select {pk} from {Catalog} where {id} = 'NAProductCatalog' }}) }})";

	public static final String PRODUCT_FROM_NAME_QUERY = "SELECT {PK} FROM {Product} WHERE {name} = ?name AND {catalogVersion} "
			+ " IN ({{ SELECT {PK} FROM {CatalogVersion} WHERE {version}='Online' AND {catalog} IN ({{ Select {pk} from {Catalog} where {id} = 'NAProductCatalog' }}) }})";

	public static final String CATEGORY_FROM_NAME_QUERY = "SELECT {PK} FROM {Category} WHERE UPPER({name}) = ?name AND {catalogVersion} "
			+ " IN ({{ SELECT {PK} FROM {CatalogVersion} WHERE {version}='Online' AND {catalog} IN ({{ Select {pk} from {Catalog} where {id} = 'NAProductCatalog' }}) }})";
	public static final String GET_RESTOCK_ALERT = "select {PK} from {RestockAlert} where {alertId}=?alertId";
	public static final String LOW_STOCK_QUERY = "Select DISTINCT {PK} from {RestockAlert as R JOIN Product as P on {P.pk} = {R.product} } "
			+ " WHERE {dealer} =?dealer AND ({P.isReorderable}  != ?isReorderable or {P.isReorderable} is null)";
	public static final String LOW_STOCK_SALESMODEL_FILTER_QUERY = "Select DISTINCT {PK}  from {RestockAlert as R} where {dealer} =?dealer and {product} = ?product";
	public static final String GET_PDC_MFU_QUERY = "Select {pos.pk} from {PointOfService as pos JOIN Address as adr on {adr.pk}={pos.address} } where {pos.name} = ?posId";
	public static final String GET_WAREHOUSE_BY_DEALERCODE_QUERY = "Select {pk} from {warehouse as w} where {w.code} = ?dealerCode";

	public static final String LOW_STOCK_CATEGORY_FILTER_QUERY = "Select DISTINCT {PK}  from {RestockAlert as R JOIN CategoryProductRelation as C ON {R.product} = {C.target} JOIN Product as P ON {R.product}={P.pk} } where {R.dealer} =?dealer and {C.source} = ?category AND ({P.isReorderable}  != ?isReorderable or {P.isReorderable} is null)";
	public static final String LOW_STOCK_CATEGORES_FILTER_QUERY = "Select DISTINCT {PK}  from {Category as C JOIN CategoryProductRelation as P ON {C.PK} = {P.source}} WHERE {P.target} IN ({{Select DISTINCT {product}  from {RestockAlert as R  JOIN Product as P on {P.pk} = {R.product} } where {dealer} =?dealer AND ({P.isReorderable}  != ?isReorderable or {P.isReorderable} is null)}})";
	public static final String GET_CONFIG_VARIANTS_QUERY = "Select DISTINCT {PK}  from {AlertProduct} where {alertId} =?alertId";
	public static final String GET_CONFIG_VARIANT_QUERY = "Select DISTINCT {PK}  from {AlertProduct} where {alertId} =?alertId and {product}=?product";

	public static final String OPEN_BRACKET = "[";
	public static final String CLOSE_BRACKET = "]";
	public static final String TRUE_FLAG = "true";
	public static final String BASE_VARIANT_PRODUCT = "BaseVariantProduct";
	public static final String CONFIG_VARIANT_PRODUCT = "ConfigVariantProduct";

	public static final String CATEGORY_DEALER_STRING = "dealerCode_string$";
	public static final String DEALER_CODE = "dealerCode";
	public static final String CODE_STRING = "code_string$";
	public static final String SERIAL_NUMBER = "SERIAL_NUMBER_string$";



	public static final String GET_ORDER_WINDOW = "Select {PK} from {OrderWindow as o} where {o.fromDate} <= ?currentDate and {o.toDate} >= ?currentDate";
	public static final String GET_SHIPTO_DATES = "Select {PK} from {ShipToAfterDate} where {orderWindow} =?orderWindow ORDER BY {shipToDate} ASC";

	public static final String CATALOG_ID = "catalogId";
	public static final String CATALOG_NAME = "NAProductCatalog";
	public static final String CATALOG_VERSION = "catalogVersion";
	public static final String CATALOG_VERSION_VALUE = "Online";

	public static final String CATEGORY_CODE = "categoryCode";


	public static final String CATEGORY_NAME_VAL = "categoryName";

	public static final String SPECCHECK_TIERID = "Tier_ID";
	public static final String SPECCHECK_MACHINEID = "Machine_ID";
	public static final String SPECCHECK_RANGEID = "Range_ID";
	public static final String SPECCHECK_PLPPAGE = "catSpeccheckPLP";
	public static final String SPECCHECK_COMPPAGE = "catSpeccheckCompare";
	public static final String SPECCHECK_REQUESTURI = "speccheck.handler.requestURI";
	public static final String SPECCHECK_PROXYPORT = "speccheck.proxy.port";
	public static final String SPECCHECK_PROXYURL = "speccheck.proxy.url";
	public static final String SPECCHECK_RANGESHANDLERURI = "ranges.ashx?";
	public static final String SPECCHECK_MACHINESHANDLERURI = "machines.ashx?";
	public static final String SPECCHECK_COMPAREHANDLERURI = "machinedata.ashx?";
	public static final String SPECCHECK_RANGESHANDLER = "Ranges";
	public static final String SPECCHECK_MACHINESHANDLER = "Machines";
	public static final String SPECCHECK_COMPAREHANDLER = "compare";
	public static final String SPECCHECK_QUESTION = "?";
	public static final String SPECCHECK_FID = "fid=";
	public static final String SPECCHECK_RID = "rid=";
	public static final String SPECCHECK_MIDS = "mids=";
	public static final String SPECCHECK_MID = "&mid=";
	public static final String SPECCHECK_MANIDS = "&manids=";
	public static final String SPECCHECK_LIMIT = "&limit=";
	public static final String SPECCHECK_TIDS = "&tids=";
	public static final String SPECCHECK_LIMITFORCATCOMP = "speccheck.competitor.cat.limit";
	public static final String SPECCHECK_LIMITFORALLCOMP = "speccheck.competitor.all.limit";
	public static final String SPECCHECK_ERRORMESSAGE = "Error while fetching response from speccheck service";
	public static final String SPECCHECK_EQUIPMENT = "equipment";
	public static final String SPECCHECK_UTV = "utv";
	public static final String SPECCHECK_MANUFACTURERID = "5";
	public static final String SPECCHECK_EMPLTYLIST = "[]";
	public static final String SPECCHECK_GETCOMPAREMEDIAENABLED = "speccheck.compare.media.enabled";
	public static final String SPECCHEKCOMPARE_MEDIA_PREFIX_KEY = "media_prefix_domain";
	public static final String SPECCHEKCOMPARE_MEDIA_PREFIX_VALUE = "speccheck.compare.media.prefix.value";
	public static final String SPECCHECK_MED = "&med=1";
	public static final String SPECCHECK_PROXYENABLED = "speccheck.proxy.enabled";
	public static final String SPECCHECK_UOM = "&uom=";
	public static final String SPECCHEKCOMPARE_PRODUCTDATA = "productData";
	public static final String SPECCHEKCOMPARE_SALESMODEL = "salesModel";
	public static final String SPECCHEKCOMPARE_IMAGEURL = "imageURL";
	public static final String SPECCHECK_RGIDS = "&rgids=";
	public static final String SPECCHECK_RGIDSFORCOMPARE = "speccheck.compare.rgids";
	public static final String SPECCHECK_MACHINEIDONLOAD = "machineId";
	public static final String SPECCHECK_PRODUCTLIST = "Product List";
	public static final String SPECCHECK_BREADCRUMBFORPLPURI = "/speccheck/viewSpeccheckPLP?categoryName=";
	public static final String SPECCHECK_COMPAREMODELS = "Compare Models";
	public static final String SPECCHECK_CATERPILLAR = "Caterpillar";

	public static final String FTP_CONNECTION_ERROR = "SFTP connection with MFT server is failed";
	public static final String FTP_CHANNEL_ERROR = "SFTP Channel Error";
	public static final String FTP_ATTRIBUTE_ERROR = "FTP missing attribut";
	public static final String FTP_FILE_PATH_ERROR = " IO Error in FTP Path";
	public static final String FTP_FILE_DOWNLOAD_ERROR = "Error while dowloading the file from MFT server";

	public static final String SFTP_USER_ID = "Cat.mft.server.userid";
	public static final String SFTP_HOST_ADDRESS = "Cat.mft.server.host";
	public static final String SFTP_KEY = "Cat.mft.server.password";
	public static final int SFTP_PORT = 22;
	public static final int FILE_BUFFER_SIZE = 1024;
	public static final String SFTP_HOST_KEY_CHECK = "StrictHostKeyChecking";
	public static final String KEY_REQUIRED = "no";
	public static final String FTP_TYPE = "Cat.mft.server.ftp.type";
	public static final String CHANGE_DIRECTORY_PATH = "Cat.mft.change.directory.path";
	public static final String LS_COMPLETE_SEARCH = "*.*";
	public static final String FTP_FILE_PATTERN = "Cat.mft.server.ftp.file.pattern";
	public static final String FTP_FILE_TARGET_LOCATION = "cat.hotfolder.import.folder";
	public static final String FTP_FILE_TARGET_SITE = "cat.hotfolder.import.site";

	public static final String NEW = "NEW";
	public static final String USED = "USED";
	public static final String ACCESSORIES = "ACCESSORIES";
	public static final String ORDER_BY = " ORDER BY";
	public static final String MTL = "mtl";
	public static final String SSL = "ssl";
	public static final String PARTS_CLASSIFICATION = "parts_classification";

	public static final String SERIAL_NUMBER_CLASSIFICATION = "SERIAL_NUMBER";

	public static final String POPULATE_COMPARE_SALES_MODELS_QUERY = "SELECT {sales.pk} "
			+ "FROM {Product as sales JOIN SpeccheckInfo as spec ON {spec:product} = {sales:pk}}, {category as cat}, {CategoryProductRelation as rel} "
			+ "WHERE {cat.code} = ?category AND  {sales.pk}={rel.target} AND {cat.pk}={rel.source} "
			+ "AND {sales.catalogVersion} = ({{ SELECT {PK} FROM {CatalogVersion} WHERE {version}=?catalogVersion AND {catalog} = ({{ Select {pk} from {Catalog} where {id} = ?catalogId }}) }})  ";

	public static final String FIND_UNITS_BY_UID = "Select DISTINCT {U.pk},{U.uid} from {B2BUnit2ShipTo as S JOIN B2BUnit as U ON {S.target} = {U.pk} } where {S.source} IN ({{Select {pk} from {B2BUnit} where {uid} LIKE ?uid}})";
	public static final String FIND_SHIPPINGADDRESS_WITH_PK = "Select {pk} from {address} where {pk}=?pk  ";

	public static final String FIND_DELIVERY_INFO_BY_MSO = "SELECT {" + DeliveryInfoModel.PK + "} FROM {"
			+ DeliveryInfoModel._TYPECODE + "} WHERE {" + DeliveryInfoModel.MSONUMBER + "} = ?msoNumber";

	public static final String FIND_PRODUCT_DELIVERY_INFO = "SELECT {D:PK} AS pk FROM {DeliveryInfo AS D} WHERE {D:PRODUCTCONFIGURATION} = ?product";

	public static final String CAT_CART_PAGE = "catCartPage";
	public static final String CAT_CART = "Cart";
	public static final String CAT_PDP_URI_PREFIX = "/p/";
	public static final String CAT_PRODUCT_DETAILS_PAGE = "Product Details Page";

	public static final int EMAIL_SUGGESTION_COUNT = 6;

	public static final String IS_CONFIGURATION_IN_TRANSIT = "isConfigurationInTransit";
	public static final String STOCK_IN_TRANSIT = "stockIntransit";

	public static final String IS_CONFIGURATION_IN_TRANSIT_STRING = ":isConfigurationInTransit:";


	public static final String DATE_FORMAT = "yyyy-MM-dd";

	public static final String CREATED_STATE = "Created";
	public static final String DRAFTED_STATE = "Seller_Draft";

	public static final String EXPORT_QUOTE_FILE = "exportQuoteDoc/QuoteWords.docx";
	public static final String UPDATED_EXPORT_QUOTE_FILE = "exportQuoteDoc/TestQuoteWord.docx";
	public static final String DATE_PLACE_HOLDER = "$date";
	public static final String QUOTE_PLACE_HOLDER = "quoteNo";
	public static final String DEALER_PLACE_HOLDER = "dealerNam";
	public static final String USER_PLACE_HOLDER = "$userName";
	public static final String CUST_NAME_HOLDER = "$custName";
	public static final String CUST_EMAIL_HOLDER = "$custEmail";
	public static final String CUST_ADDRESS_HOLDER = "$custAddress";
	public static final String CUST_BILLING_HOLDER = "&custBilling";
	public static final String OPEN_SANS = "Open sans";
	public static final String OPEN_SANS_SEMIBOLD = "Open Sans Semibold";
	public static final String HELVETICA_NEUE_MEDIUM = "Helvetica Neue Medium";
	public static final String HELVETICA_NEUE_LIGHT = "Helvetica Neue Light";
	public static final String HELVETICA_NEUE = "Helvetica Neue";
	public static final String IMAGE_TYPE = "png";
	public static final String PLACE_HOLDER = "$pF";
	public static final String DATE_PATTERN = "MM/dd/yyyy";
	public static final String DATE_EXPORT_PATTERN = "MMMM dd, yyyy";
	public static final String FONT_LIGHT = "Open Sans Light";
	public static final String QUOTE_EXPORT_NAME_PREFIX = "QUOTE-";
	public static final String QUOTE_EXPORT_FILE_EXT = ".docx";


	public static final String CART_PAGE_URI = "/cart/catCartPage";
	public static final String CAT_QUOTE = "Quote";
	public static final String CAT_EDIT_QUOTE = "Edit Quote";
	public static final String CAT_REPLICATE_QUOTE = "Replicate Quote";
	public static final String CAT_QUOTE_DETAILS = "Quote Details";
	public static final String CAT_QUOTE_HISTORY = "Quote History";
	public static final String CAT_QUOTE_HISTORY_URI = "/my-account/my-quotes/view";
	public static final String CAT_QUOTE_DETAILS_URI = "/my-account/my-quotes/";
	public static final String PRODUCT_FEATURES_DELIMITER_REGEX = "product.features.delimiter.regex";

	public static final String CAT_REORDER_CART_PAGE = "catReOrderCartPage";
	public static final String VIEW_MANAGEORDERS_LINK_URL = "/reorder/viewManageOrders";
	public static final String REVIEW_ORDER = "Review Order";
	public static final String REODER_CART_LINK_URL = "/cart/reOrderCart";
	public static final String REORDER_PRODUCTS = "Reorder Products";
	public static final String REORDER_PRODUCT_LIST = "Reorder Product List";
	public static final String REORDER_PO_LIST = "reorderPoList";


	public static final String ABSOLUTE_DISCOUNT_PREFIX = "$";
	public static final String PERCENT_DISCOUNT_PREFIX = "%";


	//Constants for CPC integration
	public static final String SPECIFICATIONS = "specifications";
	public static final char CPCSEPARATOR = '|';
	public static final String CLASSIFICATION_CLASS_HEADER_ARRAY = "code|name";
	public static final String PROPERTY = "property";
	public static final String PRODUCT_DESCRIPTION_HEADER = "code|description";
	public static final String CATEGORY_RELATION_HEADER = "supercategory|category";
	public static final String PRODUCT_VALUE_HEADER = "code|value";
	public static final String CLASSIFICATION_DATA_REMOVAL_HEADER = "code";
	public static final String UNCLASSIFIED_DATA_REMOVAL_HEADER = "qualifier|code";
	public static final String UTV_CATEGORY = "utv";
	public static final String UTV_TEXT = "utility";
	public static final String CPC_XML_URL = "https://cpc.cat.com/ws/xml/en/";

	public static final String EPPPRODUCT = "EPPProduct";
	public static final String CSAPRODUCT = "CSAProduct";

	public static final String GET_IM_ADD_TO_CART_CONFIG_VARIANT_QUERY = "Select DISTINCT {PK}  from {AlertProduct} where {alertId} = ({{Select {pk} from {RestockAlert} where {alertId} = ?alertId }}) and {product} = ?product";

	public static final String GET_RESTOCK_ALERT_QUERY = "Select DISTINCT {PK}  from {RestockAlert} where {alertId} = ?alertId";


	//Constants for Export Quote Word Document
	public static final String SALES_MODEL = "Sales Model";
	public static final String PRODUCT_FAMILY = "Product Family";
	public static final String YEARS = "Years: ";
	public static final String HOURS = "Hours: ";
	public static final String CSA_OPTION_APPLIED = "CSA Option";
	public static final String EPP_OPTION_APPLIED = "EPP Option";
	public static final String EPP_CSA_APPLIED = "Applied:";
	public static final String CAT_QUOTE_EXPORT_QUOTE_TERMS = "cat.quote.export.quote.terms";
	public static final String CAT_QUOTE_EXPORT_QUOTE_TRANSACTION = "cat.quote.export.quote.transaction";
	public static final String CAT_QUOTE_EXPORT_QUOTE_DISCOUNT = "cat.quote.export.quote.discount";
	public static final String CAT_QUOTE_EXPORT_QUOTE_PRICE = "cat.quote.export.quote.price";
	public static final String CAT_QUOTE_EXPORT_CART_TRANSACTION = "cat.quote.export.cart.transaction";
	public static final String CAT_QUOTE_EXPORT_CART_DISCOUNT = "cat.quote.export.cart.discount";
	public static final String CAT_QUOTE_EXPORT_CART_QTY = "cat.quote.export.cart.qty";
	public static final String CAT_QUOTE_EXPORT_CART_LIST_PRICE = "cat.quote.export.cart.list.price";
	public static final String CAT_QUOTE_EXPORT_CART_SALES_MODEL = "cat.quote.export.cart.sales.model";
	public static final String CAT_QUOTE_EXPORT_CART_CONFIG_ID = "cat.quote.export.cart.config.id";
	public static final String CAT_QUOTE_EXPORT_CART_PRODUCT_FAMILY = "cat.quote.export.cart.product.family";
	public static final String CAT_QUOTE_EXPORT_CART_DETAILS = "cat.quote.export.cart.details";
	public static final String CAT_QUOTE_EXPORT_MACHINE_ATTACHMENTS = "cat.quote.export.machine.attachments";
	public static final String CAT_QUOTE_EXPORT_MACHINE_CONFIG = "cat.quote.export.machine.config";
	public static final String CAT_QUOTE_EXPORT_PRODUCT_DETAILS = "cat.quote.export.product.details";
	public static final String CAT_QUOTE_EXPORT_LAST_DATE = "cat.quote.export.last.date";
	public static final String CAT_QUOTE_EXPORT_QUOTE_DESCRIPTION = "cat.quote.export.quote.description";
	public static final String CAT_QUOTE_EXPORT_QUOTE_COUNTRY = "cat.quote.export.quote.country";
	public static final String CREATED_BY = "CREATED BY";
	public static final String QUOTATION_DATE = "QUOTATION DATE";
	public static final String OWNER = "OWNER";
	public static final String QUOTE_NO = "QUOTE NO.";
	public static final String CAT_QUOTE_EXPORT_EPP_DETAILS = "cat.quote.export.epp.details";
	public static final String CAT_QUOTE_EXPORT_CSA_DETAILS = "cat.quote.export.csa.details";
	public static final String CAT_QUOTE_EXPORT_CUSTOMER_BILLING_ADDRESS = "cat.quote.export.customer.billing.address";
	public static final String CAT_QUOTE_EXPORT_CUSTOMER_SHIPPING_ADDRESS = "cat.quote.export.customer.shipping.address";
	public static final String CAT_QUOTE_EXPORT_CUSTOMER_EMAIL = "cat.quote.export.customer.email";
	public static final String CAT_QUOTE_EXPORT_CUSTOMER_NAME = "cat.quote.export.customer.name";
	public static final String CAT_QUOTE_EXPORT_CUSTOMER_DETAILS = "cat.quote.export.customer.details";
	public static final String CAT_QUOTE_EXPORT_QUOTE_DETAILS = "cat.quote.export.quote.details";
	public static final String SAMPLE_SCENE7_URL = "sample.scene7.url";
	public static final String DEFAULT_SCENE7_URL = "scene7";
	public static final String EXPORT_CART_TABLE_BG_COLOR = "E8E8E8";
	public static final String ABSOLUTE_DISCOUNT_TYPE = "ABSOLUTE";
	public static final String PERCENT_DISCOUNT_TYPE = "PERCENT";


	public static final String GET_AVAILABLE_COUNTIES_FOR_DEALER = "select {pk} from {CatCounty} where {mainDealer} in ({{select {pk} from {B2BUnit} where {uid}=?mainDealer}}) AND {isMapped} = ?isMapped";

	public static final String GET_TRUCKLOAD_COMBINATION_QUERY = "SELECT {PK} from {truckload}";
	public static final String FIVE_SEATER_QUERY = "SELECT {PK} FROM {TRUCKLOAD} WHERE {fiveSeaterCapacity} = ?fiveSeaterQty";
	public static final String TWO_SEATER_QUERY = "SELECT {PK} FROM {TRUCKLOAD} WHERE {twoSeaterCapacity} = ?twoSeaterQty";
	public static final String TWO_FIVE_SEATER_QUERY = "SELECT {PK} FROM {TRUCKLOAD} WHERE {twoSeaterCapacity} = ?twoSeaterQty AND {fiveSeaterCapacity} = ?fiveSeaterQty";
	public static final String SPACE_DELIMITER = " ";
	public static final String MIX_AND_MATCH_TRUCK = "mm_";

	public static final String COUNTY_MAPPING = "County Mapping";
	public static final String COUNTY_LIST = "County List";
	public static final String CAT_COUNTY_MAPPING_URL = "/my-account/getCountyMapping";


	public static final String ATTACHMENTS_LABEL = "ATTACHMENTS";

	public static final String ACCESSORY_PAGE = "Utility Vehicle Accessories";

	public static final String GET_PRODUCTREFERENCE_FROM_PRODUCTREFERENCES = "SELECT {ref.pk} FROM {ProductReference as ref JOIN Product as product on {product.pk}={ref.source} JOIN PartVariantProduct as part on {part.pk}={ref.target}}, {category as cat}, {CategoryProductRelation as rel} WHERE {cat.code} = 'utv' AND {cat.pk}={rel.source} AND {product.pk}={rel.target} AND {ref.referenceType} in ({{ select {pk} from {ProductReferenceTypeEnum} where {code}='ACCESSORIES' }}) AND {product.catalogversion} IN ({{ SELECT {PK} FROM {CatalogVersion} WHERE {version}='Online' AND {catalog} IN ({{ Select {pk} from {Catalog} where {id} = 'NAProductCatalog' }}) }}) ";

	public static final String GET_CUV_MODELS = "Select DISTINCT({sales.pk}) "
			+ "From {Product as sales},{BaseVariantProduct as base},{ConfigVariantProduct as config},{category as cat}, {CategoryProductRelation as rel} "
			+ "where {sales.pk} = {base.baseproduct} AND {config.baseProduct} = {base.pk} "
			+ " AND {cat.code} = 'utv' AND  {sales.pk}={rel.target} AND {cat.pk}={rel.source}"
			+ " AND {sales.catalogVersion} IN ({{ SELECT {PK} FROM {CatalogVersion} WHERE {version}='Online' AND {catalog} IN ({{ Select {pk} from {Catalog} where {id} = 'NAProductCatalog' }}) }}) ";

	public static final String ACCESSORIES_LINK_URL = "/accessories";

	public static final String ACCESSORIES_PAGE = "Accessories";


	// OIDC

	public static final String OPENID_ACCESS_TOKEN = "OIDAT";

	public static final String ANONYMOUS_USER = "anonymous";

	public static final String OIDC_ISSUER = "iss";

	public static final String OIDC_CLIENT_ID = "client_id";

	public static final String OIDC_EXPIRATION = "exp";

	public static final String OIDC_CUPID = "oauth2.cat.cupid";

	public static final String OIDC_LOGIN_ID = "oauth2.cat.loginid";

	public static final String DEFAULT_COOKIE_PATH = "/";

	public static final String REDIRECT_PREFIX = "redirect:";
	public static final String CAT_EDH_UNEXPECTED_MSG = "cat.edh.unexpected.msg";

	public static final String CAT_EDH_NORESULTS_MSG = "cat.edh.noresults.msg";

	public static final String IS_UTV_CART = "isUTVCart";
	public static final String CART_PRODUCT_TYPE = "cartProductType";
	public static final String CART_TYPE = "cartType";

	public static final String LANE_ORDER_WINDOW_QUERY = "select {pk} from {CatLaneOrderingWindow} where ({fromDate} >= ?currentDate or ({toDate} = ?currentDate)or(?currentDate BETWEEN {fromDate} and {todate})) order by {fromDate} asc";

	public static final String COMPATIBLE_CONFIG_PRODUCTS = "Select DISTINCT({config.pk}) From {Product! as sales}, {BaseVariantProduct! as base}, {ConfigVariantProduct as config JOIN CompatibilityInfo as compatibleInfo ON {compatibleInfo.configVariantProduct}={config.pk}} WHERE {sales.pk} = {base.baseProduct} AND {config.baseProduct} = {base.pk} AND {sales.code} =?salesModel AND {sales.catalogVersion} IN ({{ SELECT {PK} FROM {CatalogVersion} WHERE {version}='Online' AND {catalog} IN ({{ Select {pk} from {Catalog} where {id} = 'NAProductCatalog'}})}}) AND {compatibleInfo.compatibilityStatus} IN ({{select {pk} from {CompatibilityStatusEnum} where code ='COMPATIBLE'}}) AND {compatibleInfo.laneType} IN ({{select {pk} from {LaneTypeEnum} where code =?laneType}})";

	public static final String IS_CONFIG_COMPATIBLE = "select {config.pk} from {CompatibilityInfo as compatibleInfo JOIN ConfigVariantProduct as config ON {compatibleInfo.configVariantProduct}={config.pk}} where {config.catalogVersion} IN ({{ SELECT {PK} FROM {CatalogVersion} WHERE {version}='Online' AND {catalog} IN ({{ Select {pk} from {Catalog} where {id} = 'NAProductCatalog'}})}}) AND  {compatibleInfo.laneType} IN ({{select {pk} from {LaneTypeEnum} where {code} =?laneType}}) AND {config.code}=?configVariantId AND {compatibleInfo.compatibilityStatus} IN ({{ select {pk} from {CompatibilityStatusEnum} where {code} ='COMPATIBLE' }})";


	// VET quote
	public static final String VALUE_ANALYSIS = "Value Analysis";
	public static final String PRODUCT_COMPARED_VET = "Product compared with: ";
	public static final String VET_LINK = "View the full study: ";
	public static final String YOU_SAVE = "You could save ";
	public static final String WITH = " with ";
	public static final String COMPARED_TO = "compared to the operating costs of a ";
	public static final String WHEN = " when ";
	public static final String WAS = " was ";
	public static final String SMALL_A = "a ";
	public static final String BIG_A = "A ";
	public static final String CAT = "CAT ";
	public static final String VET_API = "https://vet.cat.com/vetAPI/lite/getCustomerStudyDetails?studyId=";


	public static final String OPEN = "open";
	public static final String CLOSED = "closed";
	public static final String LANE3 = "lane3";
	public static final String LANE2 = "lane2";
	public static final String LANE1 = "lane1";

	public static final String LANE1_NAME = "Lane 1";
	public static final String LANE2_NAME = "Lane 2";
	public static final String LANE3_NAME = "Lane 3";

	public static final String PRODUCT_LANE_INFO_FOR_ORDERED_QTY = "select DISTINCT {PL.PK} from {ProductLaneInfo as PL JOIN Product as P on {P.genericProductCode} = {PL.genericProductCode}} where {P.code}=?salesModelId and {PL.monthYear}= ?monthYear and {dealer} =?dealer and ({PL.maximumOrderableQuantity} - {PL.reorderedQuantity} >= ?orderedQuantity)";

	public static final String PRODUCT_LANE_INFO = "select DISTINCT {PL.PK} from {ProductLaneInfo as PL JOIN Product as P on {P.genericProductCode} = {PL.genericProductCode}} where {PL.monthYear}= ?monthYear and {dealer} =?dealer";

	public static final String PRODUCT_LANE_INFO_SALES = "select DISTINCT {PL.PK} from {ProductLaneInfo as PL JOIN Product as P on {P.genericProductCode} = {PL.genericProductCode}} where {P.code}=?salesModelId and {PL.monthYear}= ?monthYear and {dealer} =?dealer";

	public static final String PRODUCT_LANE_INFO_LANETYPE = "select DISTINCT {PL.pk} from {ProductLaneInfo as PL JOIN Product as P on {P.genericProductCode} = {PL.genericProductCode} JOIN LaneTypeEnum as LC on {LC.pk}={PL.laneInfo}} where {P.code}=?salesModelId and {PL.monthYear}=?monthYear and {LC.code}=?laneCode and {dealer} =?dealer";

	public static final String GET_PARTVARIANTS_FOR_BASEVARIANT = "SELECT {ref.target} FROM {ProductReference as ref JOIN BaseVariantProduct! as product on {product.pk}={ref.source} JOIN PartVariantProduct as part on {part.pk}={ref.target} } WHERE  {product.catalogversion} IN ({{ SELECT {PK} FROM {CatalogVersion} WHERE {version}='Online' AND {catalog} IN ({{ Select {pk} from {Catalog} where {id} = 'NAProductCatalog' }}) }}) AND {product.code}=?dcaCode";


	public static final String ADD_PARTS = "Add Parts";

	public static final String REORDER_PLP = "Reorder PLP";

	public static final String REORDER_PLP_URL = "/reorder/viewManageOrders";

	public static final String IS_PART_EXISTS = "select {pvp.code} from {Product as p join ProductReference as pr on ({p.pk}={pr.source}) join PartVariantProduct as pvp on ({pvp.pk}={pr.target}) join catalogVersion as cv on ({cv.pk}={p.catalogversion}) } where {pvp.code}=?partId and {p.code}=?productCode and {cv.version}='Online'";

	public static final String SITE_UID = "catSite";

	public static final String CART_LANE_TYPE = "cartLaneType";

	public static final String GET_FBC_PARTS_WITH_PARAMS = "SELECT {PK} from {FBCPartsOrderInfo} where {cartId} =?cartId and {configurationId} =?configId and {partId}= ?partId ";
	public static final String GET_FBC_PARTS_FOR_CART = "SELECT {PK} from {FBCPartsOrderInfo} where {cartId} =?cartId";

	public static final String EPPOPTION = "EPPOption";

	public static final String CSAOPTION = "CSAOption";

	public static final String CSAHOURS = "CSAHours";

	public static final String CSAYEAR = "CSAYear";


	public static final String ACKNOWLEDGEMENT_EMAIL_ATTACHMENT_PATH = "catcore/acknowledgement_email/Dealer_Acknowledgement.xlsx";

	public static final String DATE_FORMAT_EXCEL = "dd-MMM-yyyy h:mm";


	public static final String GET_ALERT_PRODUCT = "Select {pk} from {AlertProduct} where {product}=?configVariantProduct";


	public static final String ALL_LANES = "all";

	public static final String SP_USER = "dealerSalesPersonGroup";

	public static final String DOUBLEUNDERSCORE = "__";


	public static final String GET_RESTOCK_PRODUCT = "Select {pk} from {RestockAlert} where {product}=?product";


	public static final String LANE_TYPE = "catLaneType";


	public static final String LANE_TYPE_PARAM = ":catLaneType:";

	public static final String PRODUCT_CODE_EXCEL = "N";


	private CatCoreConstants()
	{
		//empty
	}
}
