/**
 *
 */
package com.cat.bcp.core.customer;

import de.hybris.platform.commerceservices.customer.CustomerAccountService;


/**
 * @author vjagannadharaotel
 *
 */
public interface CatCustomerAccountService extends CustomerAccountService
{

	/**
	 * This method is to create a new buyer customer for CAT
	 *
	 * @param name
	 *           Customer Name
	 * @param uid
	 *           Customer Email Id
	 */
	void createCustomer(final String name, final String uid);
}
