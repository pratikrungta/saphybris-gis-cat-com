/**
 *
 */
package com.cat.bcp.core.customer.impl;

import de.hybris.platform.commerceservices.customer.impl.DefaultCustomerAccountService;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.cat.bcp.core.customer.CatCustomerAccountService;


/**
 * @author vjagannadharaotel
 *
 */
public class CatCustomerAccountServiceImpl extends DefaultCustomerAccountService implements CatCustomerAccountService
{
	@Resource(name = "modelService")
	private ModelService modelService;

	private PersistentKeyGenerator catCustomerUidGenerator;

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public void createCustomer(final String name, final String emailId)
	{
		final CustomerModel customer = modelService.create(CustomerModel.class);
		if (StringUtils.isNotEmpty(emailId))
		{
			customer.setUid(String.valueOf(getCatCustomerUidGenerator().generate()));
			customer.setCatCustomerEmailId(emailId);
		}
		if (StringUtils.isNotEmpty(name))
		{
			customer.setName(name);
		}
		modelService.save(customer);
	}

	/**
	 * @return the catCustomerUidGenerator
	 */
	public PersistentKeyGenerator getCatCustomerUidGenerator()
	{
		return catCustomerUidGenerator;
	}

	/**
	 * @param catCustomerUidGenerator
	 *           the catCustomerUidGenerator to set
	 */
	public void setCatCustomerUidGenerator(final PersistentKeyGenerator catCustomerUidGenerator)
	{
		this.catCustomerUidGenerator = catCustomerUidGenerator;
	}
}
