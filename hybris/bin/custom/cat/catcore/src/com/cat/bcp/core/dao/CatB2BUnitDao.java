/**
 *
 */
package com.cat.bcp.core.dao;

import de.hybris.platform.b2b.dao.B2BUnitDao;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;

import java.util.List;

import com.cat.bcp.core.model.InventoryReservationModel;


/**
 * The Interface CatB2BUnitDao.
 *
 * @author manjam
 */
public interface CatB2BUnitDao extends B2BUnitDao
{

	/**
	 * This Method used to fetch B2B units from Sales Area Unit.
	 *
	 * @param uid
	 *           the uid
	 * @return List<B2BUnitModel>
	 */
	List<B2BUnitModel> findShipTosB2BUnits(final String uid, final String countryISO);

	/**
	 * This Method used to fetch AddressModel from SelectedShippingAddressPK.
	 *
	 * @param pk
	 *           the pk
	 * @return AddressModel
	 */
	AddressModel findAddressModel(String pk);

	/**
	 * Fetch all inventory reservation for product.
	 *
	 * @param productModel
	 *           the product model
	 * @param b2bUnitModel
	 *           the b 2 b unit model
	 * @return the list
	 */
	List<InventoryReservationModel> fetchAllInventoryReservationForProduct(final ProductModel productModel,
			final B2BUnitModel b2bUnitModel);
}