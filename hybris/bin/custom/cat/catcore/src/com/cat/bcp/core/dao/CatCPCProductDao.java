package com.cat.bcp.core.dao;

import com.cat.bcp.core.model.BaseVariantProductModel;

import java.util.List;

/**
 * The interface contains the method to fetch UTV specific information for the CPC integration .
 */
public interface CatCPCProductDao {

    /**
     *The method returns list of base models for the supplied {@param productFamily }
     * @param productFamily the product family
     * @return list of base models for the {@param productFamily }
     */
    List<BaseVariantProductModel> getProductforSpecificationRecords(String productFamily);


    /**
     * The method returns the list of super categories code belonging to the utv category
     * @return List of category code
     */
    List<String> getExistingCategoryForUTV();


    /***
     * The method fetches the list of the classification attributes attached to the {@param classificationClassList }
     * @param classificationClassList the list of the classification classes
     * @return the list of the classification attributes
     */
    List<String> getListOfClassificationAttribute(List<String> classificationClassList);


    /**
     * The method fetches the list unclassified attribute to be removed
     * @return the list of the un classfied attributes
     */
    List<List<String>> getListOfUnClassifiedAttribute(List<String> totalAttributeRemovalList );
}
