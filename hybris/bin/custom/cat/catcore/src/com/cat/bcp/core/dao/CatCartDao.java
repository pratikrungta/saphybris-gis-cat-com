/**
 *
 */
package com.cat.bcp.core.dao;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.servicelayer.internal.dao.Dao;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import com.cat.bcp.core.model.CatAdditionalInfoModel;
import com.cat.bcp.core.model.FBCPartsOrderInfoModel;
import com.cat.core.integration.fbc.request.Selection;
import com.cat.facades.order.PurchaseOrderCheck;


/**
 * @author sparkibanda
 *
 */
public interface CatCartDao extends Dao
{
	PurchaseOrderCheck checkPOList(List<String> purchaseOrderNumberList, B2BCustomerModel customerModel);

	/**
	 * This method is used to fetch Cat Additional Info(Used for EPP/CSA drop-down options)
	 *
	 * @param type
	 * @return List of CatAdditionalInfoModel
	 */
	List<CatAdditionalInfoModel> getCatAdditionalInfo(String type);

	/**
	 * This method adds to fbc custom table
	 *
	 * @param selectionList
	 *           - fbc selections
	 * @param qty
	 *           - qty
	 * @param configId
	 *           - config id
	 */
	void addFBCPartInfo(List<Selection> selectionList, String configId, long qty);

	/**
	 * update fbc model
	 *
	 * @return
	 *
	 */
	SearchResult<FBCPartsOrderInfoModel> updateCartForConfigurableProducts();
}
