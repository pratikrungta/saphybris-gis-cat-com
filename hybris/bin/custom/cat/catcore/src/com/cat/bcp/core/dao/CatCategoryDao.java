/**
 *
 */
package com.cat.bcp.core.dao;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.Collection;


/**
 * @author manjam
 *
 */
public interface CatCategoryDao extends Dao
{

    /**
     * Used to find low stock Products Categories.
     *
     * @return CategoryModel
     */
     Collection<CategoryModel> findLowStockProducts(final B2BUnitModel b2bUnit);
}
