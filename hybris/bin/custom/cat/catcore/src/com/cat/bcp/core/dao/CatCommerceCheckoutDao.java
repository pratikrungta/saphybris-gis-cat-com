/**
 *
 */
package com.cat.bcp.core.dao;

import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import com.cat.bcp.core.model.TruckloadModel;


/**
 * @author avaijapurkar
 *
 */
public interface CatCommerceCheckoutDao
{

	/**
	 * @return List of truckload data
	 */
	public List<TruckloadModel> getTruckloadCombinations();

	/**
	 * @param fiveSeaterQty
	 *           - total 2 seaters
	 * @param twoSeaterQty
	 *           - total 5 seaters
	 * @return validated truckload
	 */
	public boolean validateTruckload(int twoSeaterQty, int fiveSeaterQty);


	/**
	 * @param fiveSeaterQty
	 *           - 5 seaters
	 * @return
	 */
	public SearchResult<TruckloadModel> getFiveSeaterSuggestion(int fiveSeaterQty);

	/**
	 * @param twoSeaterQty
	 *           - 2 seaters
	 * @return
	 */
	public SearchResult<TruckloadModel> getTwoSeaterSuggestion(int twoSeaterQty);

}
