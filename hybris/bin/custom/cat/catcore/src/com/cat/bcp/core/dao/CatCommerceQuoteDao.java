/**
 *
 */
package com.cat.bcp.core.dao;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import com.cat.bcp.core.model.InventoryReservationModel;


/**
 * @author asomjal
 *
 */
public interface CatCommerceQuoteDao
{

	/**
	 * Gets the reservation quote data for update.
	 *
	 * @param serialNumbers
	 *           the serial numbers
	 * @param b2bUnitModel
	 *           the b 2 b unit model
	 * @return the reservation quote data for update
	 */
	List<InventoryReservationModel> getReservationQuoteDataForUpdate(final String serialNumbers, final B2BUnitModel b2bUnitModel);

	/**
	 * Gets the reservation quote data.
	 *
	 * @param productModel
	 *           the product model
	 * @param b2bUnitModel
	 *           the b 2 b unit model
	 * @return the reservation quote data
	 */
	List<InventoryReservationModel> getReservationQuoteData(final ProductModel productModel, final B2BUnitModel b2bUnitModel);

	/**
	 * Fetch the expired inventory
	 *
	 * @return List<InventoryReservationModel>
	 */
	List<InventoryReservationModel> fetchExpiredInventory();

	/**
	 * Gets the reserved serial number.
	 *
	 * @param productModel
	 *           the product model
	 * @param b2bUnitModel
	 *           b2b unit model
	 * @param quoteModel
	 *           the quote model
	 * @return @return the reserved serial number
	 */
	List<InventoryReservationModel> getReservedSerialNumbers(ProductModel productModel, B2BUnitModel b2bUnitModel,
			QuoteModel quoteModel);

	/**
	 * Previous reserved serial numbers.
	 *
	 * @param quoteModel
	 *           the quote model
	 * @return the list
	 */
	List<InventoryReservationModel> previousReservedSerialNumbers(QuoteModel quoteModel);


	/**
	 * Method to check if epp option is present in the additional info table
	 *
	 * @param configurationLabel
	 *           eppOption or csaOption
	 * @param configurationValue
	 *           value to be checked in database
	 * @return true if value is present in db.
	 */
	boolean checkIfOptionPresent(String configurationLabel, String configurationValue);

}
