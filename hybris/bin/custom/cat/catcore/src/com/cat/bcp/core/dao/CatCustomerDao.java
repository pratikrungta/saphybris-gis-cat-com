/**
 *
 */
package com.cat.bcp.core.dao;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;

import com.cat.bcp.core.model.CatCountyModel;
import com.cat.bcp.core.model.OrderAdditionalInfoModel;



/**
 * The Interface CatCustomerDao.
 *
 * @author sagdhingra
 */
public interface CatCustomerDao
{

	/**
	 * Gets the customer name suggestions.
	 *
	 * @param name
	 *           the name
	 * @return List<String>
	 */
	List<String> getCustomerNameSuggestions(String name);

	/**
	 * Gets the customer email suggestions.
	 *
	 * @param email
	 *           the email
	 * @return List<String>
	 */
	List<String> getCustomerEmailSuggestions(String email);

	/**
	 * Fetch existing customer details by email.
	 *
	 * @param email
	 *           the email
	 * @return CustomerModel
	 */
	CustomerModel fetchExistingCustomerDetailsByEmail(String email);

	/**
	 * Fetch existing customer details by name.
	 *
	 * @param name
	 *           the name
	 * @return CustomerModel
	 */
	CustomerModel fetchExistingCustomerDetailsByName(String name);

	/**
	 * Find user by customer email.
	 *
	 * @param customerEmail
	 *           the customer email
	 * @return the customer model
	 */
	CustomerModel findUserByCustomerEmail(String customerEmail);



	/**
	 * Find order additional info.
	 *
	 * @param customerModel
	 *           the customer model
	 * @return the list
	 */
	List<CategoryModel> findProductFamilies(CustomerModel customerModel);

	/**
	 * Gets the additional info for order.
	 *
	 * @param orderModel
	 *           the order model
	 * @return the additional info for order
	 */
	OrderAdditionalInfoModel getAdditionalInfoForOrder(OrderModel orderModel);

	/**
	 * Method to get the county data for a dealer based on the sort parameters
	 *
	 * @param dealerShipTo
	 *           the dealer ship to which is linked to county
	 * @param dealerCode
	 *           the dealer which is linked to county
	 * @param sortCode
	 *           the sort code i.e ASC or DESC
	 * @param sortParameter
	 *           the sort parameter i.e CountyName, country and city
	 * @return List<CatCountyModel> list of county model
	 */
	List<CatCountyModel> getCountyDataForDealer(String dealerShipTo, String dealerCode, String sortParameter, String sortCode);

	/**
	 * Method to get the available list of counties which can be linked to a ship to
	 *
	 * @param mainDealerCode
	 *           Logged in user's b2bUnit Code
	 * @return List<CatCountyModel>
	 */
	List<CatCountyModel> getAvailableCountyForShipTo(String mainDealerCode);
}
