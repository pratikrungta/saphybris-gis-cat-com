/**
 *
 */
package com.cat.bcp.core.dao;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;

import com.cat.bcp.core.model.DeliveryInfoModel;


/**
 * Interface to perform DAO functionalities for Delivery Info
 *
 * @author megverma
 *
 */
public interface CatDeliveryInfoDao extends Dao
{
    /**
     * Method to get Delivery Info for an Order by MSO number
     *
     * @param msoNumber
     * @return DeliveryInfoModel : An object of DeliveryInfo having matched msoNumber passed.
     */
     DeliveryInfoModel getDeliveryInfoByMSO(String msoNumber);

    /**
     * Method to get the Delivery Infos for a product configuration
     *
     * @param product
     * @return List of DeliveryInfoModel objects for the Product configuration passed.
     */
     List<DeliveryInfoModel> getProductDeliveryInfo(ProductModel product);

    /**
     * Method to check if there are any product configurations in transit for the Product Model.
     *
     * @param product
     * @return Boolean : A Boolean to indicate if there are any product configurations in transit for the Product Model.
     */
     Boolean isConfigurationInTransit(ProductModel product);

    /**
     * Method to get count of product configurations in transit for the Product Model.
     *
     * @param product
     * @return Integer : An Integer to get count of product configurations in transit for the Product Model.
     */
    Integer getInTransitConfigurationCount(ProductModel product);
}
