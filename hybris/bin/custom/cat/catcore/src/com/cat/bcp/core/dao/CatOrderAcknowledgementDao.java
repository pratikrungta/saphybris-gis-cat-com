package com.cat.bcp.core.dao;

import de.hybris.platform.core.model.order.OrderModel;

import java.util.List;


/**
 * Interface to fetch CatOrders List
 * 
 * @author ravjonnalagadda
 *
 */
public interface CatOrderAcknowledgementDao
{
	List<OrderModel> getOrdersList(String orderQuery);
}
