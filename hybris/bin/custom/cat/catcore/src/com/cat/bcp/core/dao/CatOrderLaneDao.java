/**
 *
 */
package com.cat.bcp.core.dao;

import de.hybris.platform.b2b.model.B2BUnitModel;

import java.util.Date;
import java.util.List;

import com.cat.bcp.core.model.CatLaneOrderingWindowModel;
import com.cat.bcp.core.model.ProductLaneInfoModel;


/**
 * Interface for CatLaneOrderDao
 *
 * @author sagdhingra
 *
 */
public interface CatOrderLaneDao
{

	/**
	 * Method to get the product lane info for lane
	 *
	 * @param b2bUnit
	 *           b2b unit model
	 * @param monthYear
	 *           month and year
	 * @return List<ProductLaneInfoModel>
	 */
	List<ProductLaneInfoModel> getProductLaneInfo(B2BUnitModel b2bUnit, String monthYear);

	/**
	 * Method to get the product lane info for sales model
	 *
	 * @param salesModelId
	 *           sales model id
	 * @param b2bUnit
	 *           the b2b unit model
	 * @param monthYear
	 *           month and year based on which it is to be fetched
	 * @return List<ProductLaneInfoModel>
	 */
	List<ProductLaneInfoModel> getProductLaneInfoForSalesModel(String salesModelId, B2BUnitModel b2bUnit, String monthYear);

	/**
	 * Method to get the product lane info for ordered qty
	 *
	 * @param salesModelId
	 *           sales model id
	 * @param b2bUnit
	 *           b2b unit model
	 * @param monthYear
	 *           month and year
	 * @param orderedQty
	 *           ordered Qty
	 * @return List<ProductLaneInfoModel>
	 */
	List<ProductLaneInfoModel> getProductLaneInfoForOrderedQuantity(String salesModelId, B2BUnitModel b2bUnit, String monthYear,
			int orderedQty);

	/**
	 * Method to get lane info for lane type
	 *
	 * @param laneCode
	 *           lane code
	 * @param salesModelId
	 *           sales model id
	 * @param b2bUnit
	 *           b2b unit model
	 * @param monthYear
	 *           month and year
	 * @return List<ProductLaneInfoModel>
	 */
	List<ProductLaneInfoModel> getLaneInfoForLaneType(String laneCode, String salesModelId, B2BUnitModel b2bUnit,
			String monthYear);

	/**
	 * Gets the order window for lanes.
	 *
	 * @param currentDate
	 *           the current date
	 * @return the order window for lanes
	 */
	List<CatLaneOrderingWindowModel> getOrderWindowForLanes(Date currentDate);

}
