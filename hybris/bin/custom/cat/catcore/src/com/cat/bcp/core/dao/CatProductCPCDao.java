package com.cat.bcp.core.dao;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;


/**
 * @author ravjonnalagadda
 * 
 *         Interface to fetch Product Model List
 *
 */
public interface CatProductCPCDao extends Dao
{
	List<ProductModel> getProductModelList();
}
