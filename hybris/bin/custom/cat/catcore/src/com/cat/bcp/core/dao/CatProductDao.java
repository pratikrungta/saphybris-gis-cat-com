/**
 *
 */
package com.cat.bcp.core.dao;

import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.daos.ProductDao;

import java.util.List;


/**
 * @author sagdhingra
 *
 */
public interface CatProductDao extends ProductDao
{

	/**
	 * Method to return the accessory data
	 *
	 * @return List<ProductReferenceModel>
	 */
	List<ProductReferenceModel> getAccessoryData();


	/**
	 * Method to return CUV Models
	 *
	 * @return List<ProductModel>
	 */
	List<ProductModel> getCUVModels();


}
