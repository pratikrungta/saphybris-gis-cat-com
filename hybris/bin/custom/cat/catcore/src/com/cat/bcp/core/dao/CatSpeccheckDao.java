/**
 *
 */
package com.cat.bcp.core.dao;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;


/**
 * @author sankale
 *
 */
public interface CatSpeccheckDao
{
	/**
	 * @param categoryCode
	 * @param catalogVersion
	 * @param catalogName
	 * @return
	 */
	List<ProductModel> getSalesModelForCompare(String categoryCode, String catalogName, String catalogVersion);
}
