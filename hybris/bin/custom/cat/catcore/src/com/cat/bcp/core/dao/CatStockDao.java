/**
 *
 */
package com.cat.bcp.core.dao;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import com.cat.bcp.core.model.OrderWindowModel;
import com.cat.bcp.core.model.RestockAlertModel;
import com.cat.bcp.core.model.ShipToAfterDateModel;


/**
 * @author avaijapurkar
 *
 */
public interface CatStockDao
{

	/**
	 * This method will update Alert Entry
	 *
	 * @param alertId
	 * @param configProduct
	 * @param orderedQty
	 */
	void updateAlertEntry(String alertId, String configProduct, String orderedQty);


	/**
	 * This method will get Stock For Sales Model
	 *
	 * @param alertId
	 * @return RestockAlertModel
	 */
	RestockAlertModel getStockForSalesModel(String alertId);


	/**
	 * Method to check if the associated config product exist based on alert id
	 *
	 * @param alertId
	 *           used to fetch the associated config product
	 * @return List<AlertProductModel> alert products model
	 */
	Boolean isIMAddToCartConfigProduct(final String alertId, final ProductModel productModel);


	/**
	 * This method return Ship after dates
	 * 
	 * @param orderWindow
	 *           - the order window
	 * @return Order window - model
	 */
	List<ShipToAfterDateModel> getShipToDates(OrderWindowModel orderWindow);


	/**
	 * This method return Order window for product
	 *
	 * @param prodList
	 *           - the product list
	 * @return Order window - model
	 */
	OrderWindowModel getOrderWindowAndShipAfterDates(List<ProductModel> prodList);

}
