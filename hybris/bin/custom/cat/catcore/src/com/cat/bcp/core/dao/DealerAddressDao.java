package com.cat.bcp.core.dao;
/**
 *
 */

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.List;


/**
 * @author bidavda
 *
 */
public interface DealerAddressDao
{
	public List<AddressModel> findByPosId(final String posId);

	public List<PointOfServiceModel> findMFUorPDCByPosId(final String posId);

	public List<WarehouseModel> findWareHouseBycode(final String dealerCode);

}
