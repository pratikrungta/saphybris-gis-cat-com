/**
 *
 */
package com.cat.bcp.core.dao;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import com.cat.bcp.core.model.AlertProductModel;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.RestockAlertModel;


/**
 * @author sankale
 *
 */
public interface DealerSearchDao
{
	/**
	 * Method to return Base Models for populating landing page drop down.
	 *
	 * @param productModel
	 *           sales model for which the base model needs to be fetched
	 * @return List<BaseVariantProductModel> base models
	 */
	public List<BaseVariantProductModel> getBaseModels(ProductModel productModel);

	/**
	 * Method to return Sales Models for populating landing page drop down.
	 *
	 * @param categoryCode
	 *           Fetch sales model on basis of category
	 * @param isUsed
	 *           firing different queries for used and new products
	 * @return List<ProductModel> sales models
	 */
	public List<ProductModel> getSalesModels(String categoryCode, boolean isUsed);


	/**
	 * Method to get low stock products
	 *
	 * @param b2bUnit
	 *           Fetch restock alerts model for dealer code
	 * @param salesModelId
	 *           Fetch restock alerts model for sales model id
	 * @param category
	 *           Fetch restock alerts model for category code
	 * @return List<RestockAlertModel> list of restock alerts.
	 */
	public List<RestockAlertModel> getLowStockProducts(B2BUnitModel b2bUnit, String category, String salesModelId);


	/**
	 * Method to get the associated config products based on alert id
	 *
	 * @param alertId
	 *           used to fetch the associated config products
	 * @return List<AlertProductModel> alert products model
	 */
	public List<AlertProductModel> getAssociatedConfigProducts(String alertId);

	/**
	 * Method to return a product based on the name
	 *
	 * @param productName
	 *           name of the product
	 * @return ProductModel
	 */
	public ProductModel getProductByName(String productName);


	/**
	 * Method to fetch the category based on the category name
	 *
	 * @param categoryName
	 * @return CategoryModel
	 */
	public CategoryModel getCategoryByName(String categoryName);


}
