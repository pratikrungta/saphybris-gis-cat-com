/**
 *
 */
package com.cat.bcp.core.dao.impl;

import de.hybris.platform.b2b.dao.impl.DefaultB2BUnitDao;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.CatB2BUnitDao;
import com.cat.bcp.core.model.InventoryReservationModel;
import com.cat.core.integration.model.SalesAreaLookupModel;


/**
 * @author manjam
 *
 */
public class CatB2BUnitDaoImpl extends DefaultB2BUnitDao implements CatB2BUnitDao
{
	@Resource(name = "flexibleSearchService")
	FlexibleSearchService flexibleSearchService;

	@Resource(name = "searchRestrictionService")
	SearchRestrictionService searchRestrictionService;

	private static final String INVENTORY_RESERVED_QUERY = "select {PK} from {InventoryReservation} where {configVariantProduct} = ?configVariantProduct AND {dealer} = ?dealer";
	private static final String DEALER = "dealer";
	private static final String CONFIG_VARIANT_PRODUCT = "configVariantProduct";
	private static final String SALESAREA_LOOKUP = "select {pk} from {SalesAreaLookup} where {country} like ?country";
	private static final String LIKE_CHARACTER = "%";
	private static final String UNDERSCO = "_";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<B2BUnitModel> findShipTosB2BUnits(final String uid, final String countryISO)
	{
		searchRestrictionService.disableSearchRestrictions();
		final FlexibleSearchQuery salesAreaQuery = new FlexibleSearchQuery(SALESAREA_LOOKUP);
		salesAreaQuery.addQueryParameter(SalesAreaLookupModel.COUNTRY, LIKE_CHARACTER + countryISO + LIKE_CHARACTER);
		final SearchResult<SalesAreaLookupModel> salesAreaSearchResults = flexibleSearchService.search(salesAreaQuery);
		final Set<B2BUnitModel> shipToUnits = new HashSet<>();
		if (CollectionUtils.isNotEmpty(salesAreaSearchResults.getResult()))
		{
			for (final SalesAreaLookupModel salesAreaLookupModel : salesAreaSearchResults.getResult())
			{
				fetchShipToUnits(uid, shipToUnits, salesAreaLookupModel);
			}
		}
		searchRestrictionService.enableSearchRestrictions();
		return shipToUnits.stream().distinct().collect(Collectors.toList());
	}



	/**
	 * This helper method is created to resolve sonar complexity issue
	 * 
	 * @param uid
	 * @param shipToUnits
	 * @param salesAreaLookupModel
	 */
	private void fetchShipToUnits(final String uid, final Set<B2BUnitModel> shipToUnits,
			final SalesAreaLookupModel salesAreaLookupModel)
	{
		if (null != salesAreaLookupModel)
		{
			final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.FIND_UNITS_BY_UID);
			query.addQueryParameter(B2BUnitModel.UID,
					LIKE_CHARACTER + uid + UNDERSCO + salesAreaLookupModel.getSalesOrg() + LIKE_CHARACTER);
			final SearchResult<B2BUnitModel> searchResults = flexibleSearchService.search(query);
			if (null != searchResults.getResult() && !searchResults.getResult().isEmpty())
			{
				shipToUnits.addAll(searchResults.getResult());
			}
		}
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public AddressModel findAddressModel(final String pk)
	{
		AddressModel address = new AddressModel();
		searchRestrictionService.disableSearchRestrictions();
		final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.FIND_SHIPPINGADDRESS_WITH_PK);
		query.addQueryParameter("pk", pk);
		final SearchResult<AddressModel> searchResults = flexibleSearchService.search(query);
		searchRestrictionService.enableSearchRestrictions();
		if (CollectionUtils.isNotEmpty(searchResults.getResult()))
		{
			address = searchResults.getResult().get(0);
		}
		return address;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<InventoryReservationModel> fetchAllInventoryReservationForProduct(final ProductModel productModel,
			final B2BUnitModel b2bUnitModel)
	{
		List<InventoryReservationModel> inventoryReservation = new ArrayList();
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(CONFIG_VARIANT_PRODUCT, productModel);
		queryParams.put(DEALER, b2bUnitModel);
		final SearchResult<InventoryReservationModel> result = getFlexibleSearchService().search(INVENTORY_RESERVED_QUERY,
				queryParams);
		if (null != result)
		{
			inventoryReservation = result.getResult();
		}
		return inventoryReservation;
	}

}
