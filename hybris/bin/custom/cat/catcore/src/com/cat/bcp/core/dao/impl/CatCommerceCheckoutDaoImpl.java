/**
 *
 */
package com.cat.bcp.core.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.CatCommerceCheckoutDao;
import com.cat.bcp.core.model.TruckloadModel;


/**
 * @author avaijapurkar
 *
 */
public class CatCommerceCheckoutDaoImpl implements CatCommerceCheckoutDao
{

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<TruckloadModel> getTruckloadCombinations()
	{
		final SearchResult<TruckloadModel> searchResult = flexibleSearchService
				.search(new FlexibleSearchQuery(CatCoreConstants.GET_TRUCKLOAD_COMBINATION_QUERY));
		return searchResult.getResult();


	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean validateTruckload(final int twoSeaterQty, final int fiveSeaterQty)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.TWO_FIVE_SEATER_QUERY);
		query.addQueryParameter("twoSeaterQty", twoSeaterQty);
		query.addQueryParameter("fiveSeaterQty", fiveSeaterQty);
		final SearchResult<TruckloadModel> searchResult = flexibleSearchService.search(query);

		return CollectionUtils.isNotEmpty(searchResult.getResult());

	}

	@Override
	public SearchResult<TruckloadModel> getFiveSeaterSuggestion(final int fiveSeaterQty)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.FIVE_SEATER_QUERY);
		query.addQueryParameter("fiveSeaterQty", fiveSeaterQty);
		return flexibleSearchService.search(query);
	}

	@Override
	public SearchResult<TruckloadModel> getTwoSeaterSuggestion(final int twoSeaterQty)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.TWO_SEATER_QUERY);
		query.addQueryParameter("twoSeaterQty", twoSeaterQty);
		return flexibleSearchService.search(query);
	}


}
