/**
 *
 */
package com.cat.bcp.core.dao.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commerceservices.order.dao.impl.DefaultCommerceQuoteDao;
import de.hybris.platform.commerceservices.search.flexiblesearch.data.SortQueryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.QuoteState;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.store.BaseStoreModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.cat.bcp.core.dao.CatCommerceQuoteDao;
import com.cat.bcp.core.model.InventoryReservationModel;


/**
 * @author ridjain
 *
 */
public class CatCommerceQuoteDaoImpl extends DefaultCommerceQuoteDao implements CatCommerceQuoteDao
{
	private static final String LIKE_CHARACTER = "%";
	private static final String ORDER_BY = " ORDER BY {q1:";
	private static final String DESC = "} DESC";
	private static final String ASC = "} ASC";
	private static final String BY_CODE = "byCode";
	private static final String QUERY_PARAMATER_PLACE_HOLDER = "?";
	private static final String FIND_QUOTES_BY_CUSTOMER_STORE_CODE_MAX_VERSION_QUERY = "SELECT {q1:" + QuoteModel.PK + "} FROM {"
			+ QuoteModel._TYPECODE + " as q1 JOIN " + QuoteState._TYPECODE + " as S ON {S:pk}= {q1:" + QuoteModel.STATE + "} JOIN "
			+ PrincipalModel._TYPECODE + " as U ON {U:pk} = { q1:" + QuoteModel.QUOTECUSTOMER + "}} WHERE {q1:" + QuoteModel.USER
			+ "} = " + QUERY_PARAMATER_PLACE_HOLDER + QuoteModel.USER + " AND {q1:" + QuoteModel.STORE + "} = "
			+ QUERY_PARAMATER_PLACE_HOLDER + QuoteModel.STORE + " AND {q1." + QuoteModel.STATE + "} != "
			+ QUERY_PARAMATER_PLACE_HOLDER + QuoteModel.STATE + " AND {q1:" + QuoteModel.VERSION + "} = ({{ SELECT MAX({"
			+ QuoteModel.VERSION + "}) FROM {" + QuoteModel._TYPECODE + "} WHERE {" + QuoteModel.CODE + "} = {q1:" + QuoteModel.CODE
			+ "} AND {" + QuoteModel.USER + "} = " + QUERY_PARAMATER_PLACE_HOLDER + QuoteModel.USER + " AND {" + QuoteModel.STORE
			+ "} =" + QUERY_PARAMATER_PLACE_HOLDER + QuoteModel.STORE + "}}) ";
	private static final String ORDER_BY_QUOTE_CODE_DESC = ORDER_BY + QuoteModel.CODE + DESC;
	private static final String ORDER_BY_QUOTE_MODIFIED_DATE_DESC = ORDER_BY + QuoteModel.MODIFIEDTIME + DESC;
	private static final String ORDER_BY_QUOTE_DATE_DESC = ORDER_BY + QuoteModel.QUOTECREATEDTIME + DESC;
	private static final String ORDER_BY_QUOTE_STATE_DESC = " ORDER BY {S:code} DESC";
	private static final String ORDER_BY_QUOTE_CODE_ASC = ORDER_BY + QuoteModel.CODE + ASC;
	private static final String ORDER_BY_QUOTE_MODIFIED_DATE_ASC = ORDER_BY + QuoteModel.MODIFIEDTIME + ASC;
	private static final String ORDER_BY_QUOTE_DATE_ASC = ORDER_BY + QuoteModel.QUOTECREATEDTIME + ASC;
	private static final String ORDER_BY_QUOTE_STATE_ASC = " ORDER BY {S:code} ASC";
	private static final String FILTER_QUOTE_BY_SEARCH_VALUE = "AND ( UPPER({" + QuoteModel.NAME + "}) LIKE ?" + QuoteModel.NAME
			+ " OR {" + QuoteModel.CODE + "} LIKE ?" + QuoteModel.CODE + " OR UPPER({U:name}) LIKE ?" + PrincipalModel.NAME + ")";
	private SearchRestrictionService searchRestrictionService;


	private static final String INVENTORY_RESERVATION_QUERY = "select {PK} from {InventoryReservation} where {configVariantProduct} = ?configVariantProduct AND {dealer} = ?dealer AND {reserved} = ?reserved AND {updateFlag} = ?updateFlag";

	private static final String INVENTORY_RESERVED_QUERY = "select {PK} from {InventoryReservation} where {configVariantProduct} = ?configVariantProduct AND {dealer} = ?dealer AND {reserved} = ?reserved AND {updateFlag} = ?updateFlag  AND {quote} = ?quote";

	private static final String INVENTORY_RESERVED_QUOTE_QUERY = "select {PK} from {InventoryReservation} where {reserved} = ?reserved AND {updateFlag} = ?updateFlag  AND {quote} = ?quote";


	private static final String INVENTORY_RESERVATIONFORUPDATE_QUERY = "select {PK} from {InventoryReservation} where {serialNumber} IN (?serialNumbers) AND {dealer} = ?dealer AND {reserved} = ?reserved AND {updateFlag} = ?updateFlag";
	private static final String FETCH_EXPIRED_INVENTORY = "select {PK} from {InventoryReservation} where {reservationEndDate} < (?currentDate)";

	private static final String DEALER = "dealer";
	private static final String RESERVED = "reserved";
	private static final String UPDATE_FLAG = "updateFlag";
	private static final String QUOTE = "quote";


	private static final String FETCH_CSA_EPP_OPTION = "select {pk} from {CatAdditionalInfo} where {type} IN ({{ select {pk} from {AdditionalInfoType} where {code}=?type }}) AND {additionalInfoOption}=?option";


	/**
	 * {@inheritDoc}
	 */
	@Override
	public SearchPageData<QuoteModel> findQuotesByCustomerAndStore(final CustomerModel customerModel, final BaseStoreModel store,
			final PageableData pageableData, final Set<QuoteState> quoteStates)
	{
		validateUserAndStoreAndStates(store, customerModel, quoteStates);
		final Map<String, Object> queryParams = populateBasicQueryParams(store, customerModel, quoteStates);
		List<SortQueryData> sortQueries;
		SearchPageData<QuoteModel> results;
		if (StringUtils.isNotBlank(pageableData.getSearch()))
		{
			queryParams.put(QuoteModel.NAME, LIKE_CHARACTER + pageableData.getSearch().toUpperCase() + LIKE_CHARACTER);
			queryParams.put(QuoteModel.CODE, LIKE_CHARACTER + pageableData.getSearch() + LIKE_CHARACTER);
			queryParams.put(PrincipalModel.NAME, LIKE_CHARACTER + pageableData.getSearch().toUpperCase() + LIKE_CHARACTER);

			sortQueries = Arrays.asList(
					createSortQueryData("byDate",
							createQuery(FIND_QUOTES_BY_CUSTOMER_STORE_CODE_MAX_VERSION_QUERY, FILTER_QUOTE_BY_SEARCH_VALUE,
									ORDER_BY_QUOTE_DATE_DESC)),
					createSortQueryData(BY_CODE,
							createQuery(FIND_QUOTES_BY_CUSTOMER_STORE_CODE_MAX_VERSION_QUERY, FILTER_QUOTE_BY_SEARCH_VALUE,
									ORDER_BY_QUOTE_CODE_DESC)),
					createSortQueryData("byModifiedDate",
							createQuery(FIND_QUOTES_BY_CUSTOMER_STORE_CODE_MAX_VERSION_QUERY, FILTER_QUOTE_BY_SEARCH_VALUE,
									ORDER_BY_QUOTE_MODIFIED_DATE_DESC)),
					createSortQueryData("byState",
							createQuery(FIND_QUOTES_BY_CUSTOMER_STORE_CODE_MAX_VERSION_QUERY, FILTER_QUOTE_BY_SEARCH_VALUE,
									ORDER_BY_QUOTE_STATE_DESC)),
					createSortQueryData("byDateAsc",
							createQuery(FIND_QUOTES_BY_CUSTOMER_STORE_CODE_MAX_VERSION_QUERY, FILTER_QUOTE_BY_SEARCH_VALUE,
									ORDER_BY_QUOTE_DATE_ASC)),
					createSortQueryData("byCodeAsc",
							createQuery(FIND_QUOTES_BY_CUSTOMER_STORE_CODE_MAX_VERSION_QUERY, FILTER_QUOTE_BY_SEARCH_VALUE,
									ORDER_BY_QUOTE_CODE_ASC)),
					createSortQueryData("byModifiedDateAsc",
							createQuery(FIND_QUOTES_BY_CUSTOMER_STORE_CODE_MAX_VERSION_QUERY, FILTER_QUOTE_BY_SEARCH_VALUE,
									ORDER_BY_QUOTE_MODIFIED_DATE_ASC)),
					createSortQueryData("byStateAsc", createQuery(FIND_QUOTES_BY_CUSTOMER_STORE_CODE_MAX_VERSION_QUERY,
							FILTER_QUOTE_BY_SEARCH_VALUE, ORDER_BY_QUOTE_STATE_ASC)));
		}
		else
		{
			sortQueries = Arrays.asList(
					createSortQueryData("byDate",
							createQuery(FIND_QUOTES_BY_CUSTOMER_STORE_CODE_MAX_VERSION_QUERY, ORDER_BY_QUOTE_DATE_DESC)),
					createSortQueryData(BY_CODE,
							createQuery(FIND_QUOTES_BY_CUSTOMER_STORE_CODE_MAX_VERSION_QUERY, ORDER_BY_QUOTE_CODE_DESC)),
					createSortQueryData("byModifiedDate",
							createQuery(FIND_QUOTES_BY_CUSTOMER_STORE_CODE_MAX_VERSION_QUERY, ORDER_BY_QUOTE_MODIFIED_DATE_DESC)),
					createSortQueryData("byState",
							createQuery(FIND_QUOTES_BY_CUSTOMER_STORE_CODE_MAX_VERSION_QUERY, ORDER_BY_QUOTE_STATE_DESC)),
					createSortQueryData("byDateAsc",
							createQuery(FIND_QUOTES_BY_CUSTOMER_STORE_CODE_MAX_VERSION_QUERY, ORDER_BY_QUOTE_DATE_ASC)),
					createSortQueryData("byCodeAsc",
							createQuery(FIND_QUOTES_BY_CUSTOMER_STORE_CODE_MAX_VERSION_QUERY, ORDER_BY_QUOTE_CODE_ASC)),
					createSortQueryData("byModifiedDateAsc",
							createQuery(FIND_QUOTES_BY_CUSTOMER_STORE_CODE_MAX_VERSION_QUERY, ORDER_BY_QUOTE_MODIFIED_DATE_ASC)),
					createSortQueryData("byStateAsc",
							createQuery(FIND_QUOTES_BY_CUSTOMER_STORE_CODE_MAX_VERSION_QUERY, ORDER_BY_QUOTE_STATE_ASC)));
		}

		searchRestrictionService.disableSearchRestrictions();
		results = getPagedFlexibleSearchService().search(sortQueries, BY_CODE, queryParams, pageableData);
		searchRestrictionService.enableSearchRestrictions();
		return results;
	}

	/**
	 * This method is used to fetch InventoryReservationModel list used on Quote Page for populating serial numbers
	 * drop-down.
	 *
	 * @param productModel
	 * @param b2bUnitModel
	 * @return List of InventoryReservationModel
	 */
	@Override
	public List<InventoryReservationModel> getReservationQuoteData(final ProductModel productModel,
			final B2BUnitModel b2bUnitModel)
	{
		List<InventoryReservationModel> inventoryReservation = new ArrayList();
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("configVariantProduct", productModel);
		queryParams.put(DEALER, b2bUnitModel);
		queryParams.put(RESERVED, Boolean.FALSE);
		queryParams.put(UPDATE_FLAG, Boolean.TRUE);
		final SearchResult<InventoryReservationModel> result = getFlexibleSearchService().search(INVENTORY_RESERVATION_QUERY,
				queryParams);

		if (null != result)
		{
			inventoryReservation = result.getResult();
		}
		return inventoryReservation;
	}

	/**
	 * This method is used to fetch InventoryReservationModel list to update reserved and endDate column.
	 *
	 * @param serialNumbers
	 * @param b2bUnitModel
	 * @return List of InventoryReservationModel
	 */
	@Override
	public List<InventoryReservationModel> getReservationQuoteDataForUpdate(final String serialNumbers,
			final B2BUnitModel b2bUnitModel)
	{
		List<InventoryReservationModel> inventoryReservation = new ArrayList();
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(DEALER, b2bUnitModel);
		queryParams.put(RESERVED, Boolean.FALSE);
		queryParams.put(UPDATE_FLAG, Boolean.TRUE);
		final String query = INVENTORY_RESERVATIONFORUPDATE_QUERY.replace("?serialNumbers", serialNumbers);
		final SearchResult<InventoryReservationModel> result = getFlexibleSearchService().search(query, queryParams);

		if (null != result)
		{
			inventoryReservation = result.getResult();
		}
		return inventoryReservation;
	}

	/**
	 * Gets the search restriction service.
	 *
	 * @return the searchRestrictionService
	 */
	public SearchRestrictionService getSearchRestrictionService()
	{
		return searchRestrictionService;
	}

	/**
	 * @param searchRestrictionService
	 *           the searchRestrictionService to set
	 */
	public void setSearchRestrictionService(final SearchRestrictionService searchRestrictionService)
	{
		this.searchRestrictionService = searchRestrictionService;
	}

	@Override
	protected Map<String, Object> populateBasicQueryParams(final BaseStoreModel store, final CustomerModel customerModel,
			final Set<QuoteState> quoteStates)
	{
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(QuoteModel.USER, customerModel);
		queryParams.put(QuoteModel.STORE, store);
		queryParams.put(QuoteModel.STATE, QuoteState.SELLER_DRAFT);
		queryParams.put("quoteStates", quoteStates);
		return queryParams;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<InventoryReservationModel> getReservedSerialNumbers(final ProductModel productModel,
			final B2BUnitModel b2bUnitModel, final QuoteModel quoteModel)
	{
		List<InventoryReservationModel> inventoryReservation = new ArrayList();
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("configVariantProduct", productModel);
		queryParams.put(DEALER, b2bUnitModel);
		queryParams.put(RESERVED, Boolean.TRUE);
		queryParams.put(UPDATE_FLAG, Boolean.TRUE);
		queryParams.put(QUOTE, quoteModel);
		final SearchResult<InventoryReservationModel> result = getFlexibleSearchService().search(INVENTORY_RESERVED_QUERY,
				queryParams);
		if (null != result)
		{
			inventoryReservation = result.getResult();
		}
		return inventoryReservation;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<InventoryReservationModel> previousReservedSerialNumbers(final QuoteModel quoteModel)
	{
		List<InventoryReservationModel> inventoryReservation = new ArrayList();
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put(RESERVED, Boolean.TRUE);
		queryParams.put(UPDATE_FLAG, Boolean.TRUE);
		queryParams.put(QUOTE, quoteModel);
		final SearchResult<InventoryReservationModel> result = getFlexibleSearchService().search(INVENTORY_RESERVED_QUOTE_QUERY,
				queryParams);

		if (null != result)
		{
			inventoryReservation = result.getResult();
		}
		return inventoryReservation;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<InventoryReservationModel> fetchExpiredInventory()
	{
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("currentDate", new Date());
		final SearchResult<InventoryReservationModel> result = getFlexibleSearchService().search(FETCH_EXPIRED_INVENTORY,
				queryParams);
		if (null != result)
		{
			return result.getResult();
		}
		return Collections.emptyList();
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean checkIfOptionPresent(final String configurationLabel, final String configurationValue)
	{
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("type", configurationLabel.contains("CSA") ? "CSA" : "EPP");
		queryParams.put("option", configurationValue);
		final SearchResult<InventoryReservationModel> result = getFlexibleSearchService().search(FETCH_CSA_EPP_OPTION, queryParams);
		return ifNotEmptyResult(result);

	}

	/**
	 * @param result
	 *
	 */
	private boolean ifNotEmptyResult(final SearchResult<InventoryReservationModel> result)
	{
		return (null != result && !CollectionUtils.isEmpty(result.getResult())) ? true : false;
	}



}