/**
 *
 */
package com.cat.bcp.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.customer.dao.impl.DefaultCustomerAccountDao;
import de.hybris.platform.commerceservices.search.flexiblesearch.data.SortQueryData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.jalo.order.Order;
import de.hybris.platform.store.BaseStoreModel;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.model.OrderAdditionalInfoModel;


/**
 * @author manjam
 *
 */
public class CatCustomerAccountDaoImpl extends DefaultCustomerAccountDao
{
	private static final String PO_NUMBER = "poNumber";
	private static final String MSO_NUMBER = "msoNumber";
	private static final String LANE_TYPE = "laneType";
	private static final String BY_DATE = "byDate";
	private static final String BY_MSO = "byMSO";
	private static final String BY_ORDER_STATUS = "byOrderStatus";
	private static final String BY_DATE_ASC = "byDateAsc";
	private static final String BY_MSO_ASC = "byMSOAsc";
	private static final String BY_ORDER_STATUS_ASC = "byOrderStatusAsc";

	private static final String LIKE_CHARACTER = "%";
	private static final String SORT_ORDERS_BY_DATE = CatCoreConstants.ORDER_BY + " {" + OrderModel.CREATIONTIME + "} DESC, {"
			+ OrderModel.PK + "}";
	private static final String SORT_ORDERS_BY_DATE_PRODUCTFAMILY = CatCoreConstants.ORDER_BY + " {O.creationtime} DESC, {O.pk}";

	private static final String SORT_ORDERS_BY_MSO = CatCoreConstants.ORDER_BY + " {" + OrderModel.MSO + "} DESC NULLS LAST, {"
			+ OrderModel.PK + "}";
	private static final String SORT_ORDERS_BY_MSO_PRODUCTFAMILY = CatCoreConstants.ORDER_BY + " {O.mso} DESC NULLS LAST, {O.pk}";

	private static final String SORT_ORDERS_BY_STATUS = CatCoreConstants.ORDER_BY + " {S.code } DESC, {" + OrderModel.PK + "}";

	private static final String SORT_ORDERS_BY_STATUS_PRODUCTFAMILY = CatCoreConstants.ORDER_BY + " {S.code } DESC, {O.pk}";

	private static final String SORT_ORDERS_BY_DATE_ASC = CatCoreConstants.ORDER_BY + " {" + OrderModel.CREATIONTIME + "} ASC, {"
			+ OrderModel.PK + "}";

	private static final String SORT_ORDERS_BY_DATE_ASC_PRODUCTFAMILY = CatCoreConstants.ORDER_BY
			+ "  {O.creationtime} ASC, {O.pk}";

	private static final String SORT_ORDERS_BY_MSO_ASC = CatCoreConstants.ORDER_BY + " {" + OrderModel.MSO + "} ASC NULLS LAST, {"
			+ OrderModel.PK + "}";

	private static final String SORT_ORDERS_BY_MSO_ASC_PRODUCTFAMILY = CatCoreConstants.ORDER_BY
			+ " {O.mso} ASC NULLS LAST, {O.pk}";

	private static final String SORT_ORDERS_BY_STATUS_ASC = CatCoreConstants.ORDER_BY + " {S.code} ASC, {" + OrderModel.PK + "}";

	private static final String SORT_ORDERS_BY_STATUS_ASC_PRODUCTFAMILY = CatCoreConstants.ORDER_BY + " {S.code} ASC, {O.pk}";

	private static final String FILTER_ORDER_BY_SEARCH_VALUE = " AND ( upper({" + OrderModel.MSO + "}) LIKE ?" + OrderModel.MSO
			+ " OR upper({" + OrderModel.PURCHASEORDERNUMBER + "}) LIKE ?" + OrderModel.PURCHASEORDERNUMBER + " )";

	private static final String FIND_ORDERS_BY_CUSTOMER_STORE_QUERY = "SELECT {" + OrderModel.PK + "} FROM {"
			+ OrderModel._TYPECODE + " JOIN " + OrderStatus._TYPECODE + " as S ON { S:pk}= {" + Order.STATUS + "}} WHERE {"
			+ OrderModel.USER + "} = ?customer AND {" + OrderModel.VERSIONID + "} IS NULL AND {" + OrderModel.STORE + "} = ?store"
			+ " AND {" + OrderModel.PURCHASEORDERNUMBER + "} IS NOT NULL ";


	private static final String FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_QUERY = "select {OAI.order} from {"
			+ OrderAdditionalInfoModel._TYPECODE + " as OAI join " + CategoryModel._TYPECODE
			+ " as C on {C.pk}={OAI.productfamily} join " + OrderModel._TYPECODE + " as  O on {O.pk}={OAI.order} join "
			+ OrderStatus._TYPECODE + " as S on {S.pk}={O.status }} where upper({C.name}) = ?" + CategoryModel.NAME
			+ " and {O.user}= ?customer";

	private static final String FILTER_ORDER_BY_SEARCH_PARAM = " AND ( upper({O.mso}) LIKE ?msoNumber"
			+ " OR upper({O.purchaseOrderNumber}) LIKE ?poNumber)";

	private static final String FIND_ORDERS_BY_CUSTOMER_LANE_TYPE_QUERY = "select {O.pk} from {"
			+ "Order as O join OrderStatus as S on {S.pk}={O.status} join LaneTypeEnum as lane on {lane.pk}={O.laneType}}"
			+ "  where {lane.code}= ?laneType and {O.user}= ?customer";

	private static final String FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_LANETYPE_QUERY = "select {OAI.order} from {"
			+ OrderAdditionalInfoModel._TYPECODE + " as OAI join " + CategoryModel._TYPECODE
			+ " as C on {C.pk}={OAI.productfamily} join " + OrderModel._TYPECODE + " as  O on {O.pk}={OAI.order} join "
			+ OrderStatus._TYPECODE + " as S on {S.pk}={O.status } join LaneTypeEnum as lane on {lane.pk}={O.laneType}} " + ""
			+ " where upper({C.name}) = ?" + CategoryModel.NAME + " and {lane.code}= ?laneType and {O.user}= ?customer";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SearchPageData<OrderModel> findOrdersByCustomerAndStore(final CustomerModel customerModel, final BaseStoreModel store,
			final OrderStatus[] status, final PageableData pageableData)
	{

		validateParameterNotNull(customerModel, "Customer must not be null");
		validateParameterNotNull(store, "Store must not be null");
		final Map<String, Object> queryParams = new HashMap<>();
		queryParams.put("customer", customerModel);
		queryParams.put("store", store);
		List<SortQueryData> sortQueries;
		if (StringUtils.isNotBlank(pageableData.getProductFamilySearch()) && StringUtils.isNotBlank(pageableData.getLaneType())
				&& StringUtils.isNotBlank(pageableData.getSearch()))
		{
			sortQueries = createQueriesForCategoryLaneTypeAndTextSearch(pageableData, queryParams);
		}
		else if (StringUtils.isNotBlank(pageableData.getProductFamilySearch()) && StringUtils.isNotBlank(pageableData.getSearch()))
		{
			sortQueries = createQueriesForCategoryAndTextSearch(pageableData, queryParams);
		}
		else if (StringUtils.isNotBlank(pageableData.getLaneType()) && StringUtils.isNotBlank(pageableData.getSearch()))
		{
			sortQueries = createQueriesForLaneTypeAndTextSearch(pageableData, queryParams);
		}
		else if (StringUtils.isNotBlank(pageableData.getLaneType())
				&& StringUtils.isNotBlank(pageableData.getProductFamilySearch()))
		{
			sortQueries = createQueriesForLaneTypeAndCategory(pageableData, queryParams);
		}
		else if (StringUtils.isNotBlank(pageableData.getProductFamilySearch()))
		{
			sortQueries = createQueriesForCategory(pageableData, queryParams);
		}
		else if (StringUtils.isNotBlank(pageableData.getLaneType()))
		{
			sortQueries = createQueriesForLaneType(pageableData, queryParams);
		}
		else if (StringUtils.isNotBlank(pageableData.getSearch()))
		{
			sortQueries = createQueriesForTextSearch(pageableData, queryParams);
		}
		else
		{
			sortQueries = createQueriesForCustomer();
		}
		return getPagedFlexibleSearchService().search(sortQueries, BY_DATE, queryParams, pageableData);
	}

	/**
	 * Creates the queries for category lane type and text search.
	 *
	 * @param pageableData
	 *           the pageable data
	 * @param queryParams
	 *           the query params
	 * @return the list of sortQueries
	 */
	private List<SortQueryData> createQueriesForCategoryLaneTypeAndTextSearch(final PageableData pageableData,
			final Map<String, Object> queryParams)
	{

		List<SortQueryData> sortQueries;
		queryParams.put(CategoryModel.NAME, pageableData.getProductFamilySearch().toUpperCase());
		queryParams.put(LANE_TYPE, pageableData.getLaneType());
		queryParams.put(MSO_NUMBER, LIKE_CHARACTER + pageableData.getSearch().toUpperCase() + LIKE_CHARACTER);
		queryParams.put(PO_NUMBER, LIKE_CHARACTER + pageableData.getSearch().toUpperCase() + LIKE_CHARACTER);
		sortQueries = Arrays.asList(
				createSortQueryData(BY_MSO,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_LANETYPE_QUERY, FILTER_ORDER_BY_SEARCH_PARAM,
								SORT_ORDERS_BY_MSO_PRODUCTFAMILY)),
				createSortQueryData(BY_DATE,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_LANETYPE_QUERY, FILTER_ORDER_BY_SEARCH_PARAM,
								SORT_ORDERS_BY_DATE_PRODUCTFAMILY)),
				createSortQueryData(BY_ORDER_STATUS,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_LANETYPE_QUERY, FILTER_ORDER_BY_SEARCH_PARAM,
								SORT_ORDERS_BY_STATUS_PRODUCTFAMILY)),
				createSortQueryData(BY_DATE_ASC,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_LANETYPE_QUERY, FILTER_ORDER_BY_SEARCH_PARAM,
								SORT_ORDERS_BY_DATE_ASC_PRODUCTFAMILY)),
				createSortQueryData(BY_MSO_ASC,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_LANETYPE_QUERY, FILTER_ORDER_BY_SEARCH_PARAM,
								SORT_ORDERS_BY_MSO_ASC_PRODUCTFAMILY)),
				createSortQueryData(BY_ORDER_STATUS_ASC, createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_LANETYPE_QUERY,
						FILTER_ORDER_BY_SEARCH_PARAM, SORT_ORDERS_BY_STATUS_ASC_PRODUCTFAMILY)));
		return sortQueries;

	}

	/**
	 * Creates the queries for category and text search.
	 *
	 * @param pageableData
	 *           the pageable data
	 * @param queryParams
	 *           the query params
	 * @return the list of sortQueries
	 */
	private List<SortQueryData> createQueriesForCategoryAndTextSearch(final PageableData pageableData,
			final Map<String, Object> queryParams)
	{
		List<SortQueryData> sortQueries;
		queryParams.put(CategoryModel.NAME, pageableData.getProductFamilySearch().toUpperCase());
		queryParams.put(MSO_NUMBER, LIKE_CHARACTER + pageableData.getSearch().toUpperCase() + LIKE_CHARACTER);
		queryParams.put(PO_NUMBER, LIKE_CHARACTER + pageableData.getSearch().toUpperCase() + LIKE_CHARACTER);
		sortQueries = Arrays.asList(
				createSortQueryData(BY_MSO,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_QUERY, FILTER_ORDER_BY_SEARCH_PARAM,
								SORT_ORDERS_BY_MSO_PRODUCTFAMILY)),
				createSortQueryData(BY_DATE,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_QUERY, FILTER_ORDER_BY_SEARCH_PARAM,
								SORT_ORDERS_BY_DATE_PRODUCTFAMILY)),
				createSortQueryData(BY_ORDER_STATUS,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_QUERY, FILTER_ORDER_BY_SEARCH_PARAM,
								SORT_ORDERS_BY_STATUS_PRODUCTFAMILY)),
				createSortQueryData(BY_DATE_ASC,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_QUERY, FILTER_ORDER_BY_SEARCH_PARAM,
								SORT_ORDERS_BY_DATE_ASC_PRODUCTFAMILY)),
				createSortQueryData(BY_MSO_ASC,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_QUERY, FILTER_ORDER_BY_SEARCH_PARAM,
								SORT_ORDERS_BY_MSO_ASC_PRODUCTFAMILY)),
				createSortQueryData(BY_ORDER_STATUS_ASC, createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_QUERY,
						FILTER_ORDER_BY_SEARCH_PARAM, SORT_ORDERS_BY_STATUS_ASC_PRODUCTFAMILY)));
		return sortQueries;
	}

	/**
	 * Creates the queries for lane type and text search.
	 *
	 * @param pageableData
	 *           the pageable data
	 * @param queryParams
	 *           the query params
	 * @return the list of sortQueries
	 */
	private List<SortQueryData> createQueriesForLaneTypeAndTextSearch(final PageableData pageableData,
			final Map<String, Object> queryParams)
	{
		List<SortQueryData> sortQueries;
		queryParams.put(LANE_TYPE, pageableData.getLaneType());
		queryParams.put(MSO_NUMBER, LIKE_CHARACTER + pageableData.getSearch().toUpperCase() + LIKE_CHARACTER);
		queryParams.put(PO_NUMBER, LIKE_CHARACTER + pageableData.getSearch().toUpperCase() + LIKE_CHARACTER);
		sortQueries = Arrays.asList(
				createSortQueryData(BY_MSO,
						createQuery(FIND_ORDERS_BY_CUSTOMER_LANE_TYPE_QUERY, FILTER_ORDER_BY_SEARCH_PARAM,
								SORT_ORDERS_BY_MSO_PRODUCTFAMILY)),
				createSortQueryData(BY_DATE,
						createQuery(FIND_ORDERS_BY_CUSTOMER_LANE_TYPE_QUERY, FILTER_ORDER_BY_SEARCH_PARAM,
								SORT_ORDERS_BY_DATE_PRODUCTFAMILY)),
				createSortQueryData(BY_ORDER_STATUS,
						createQuery(FIND_ORDERS_BY_CUSTOMER_LANE_TYPE_QUERY, FILTER_ORDER_BY_SEARCH_PARAM,
								SORT_ORDERS_BY_STATUS_PRODUCTFAMILY)),
				createSortQueryData(BY_DATE_ASC,
						createQuery(FIND_ORDERS_BY_CUSTOMER_LANE_TYPE_QUERY, FILTER_ORDER_BY_SEARCH_PARAM,
								SORT_ORDERS_BY_DATE_ASC_PRODUCTFAMILY)),
				createSortQueryData(BY_MSO_ASC,
						createQuery(FIND_ORDERS_BY_CUSTOMER_LANE_TYPE_QUERY, FILTER_ORDER_BY_SEARCH_PARAM,
								SORT_ORDERS_BY_MSO_ASC_PRODUCTFAMILY)),
				createSortQueryData(BY_ORDER_STATUS_ASC, createQuery(FIND_ORDERS_BY_CUSTOMER_LANE_TYPE_QUERY,
						FILTER_ORDER_BY_SEARCH_PARAM, SORT_ORDERS_BY_STATUS_ASC_PRODUCTFAMILY)));
		return sortQueries;

	}

	/**
	 * Creates the queries for lane type and category.
	 *
	 * @param pageableData
	 *           the pageable data
	 * @param queryParams
	 *           the query params
	 * @return the list of sortQueries
	 */
	private List<SortQueryData> createQueriesForLaneTypeAndCategory(final PageableData pageableData,
			final Map<String, Object> queryParams)
	{
		List<SortQueryData> sortQueries;
		queryParams.put(CategoryModel.NAME, pageableData.getProductFamilySearch().toUpperCase());
		queryParams.put(LANE_TYPE, pageableData.getLaneType());
		sortQueries = Arrays.asList(
				createSortQueryData(BY_MSO,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_LANETYPE_QUERY, SORT_ORDERS_BY_MSO_PRODUCTFAMILY)),
				createSortQueryData(BY_DATE,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_LANETYPE_QUERY, SORT_ORDERS_BY_DATE_PRODUCTFAMILY)),
				createSortQueryData(BY_ORDER_STATUS,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_LANETYPE_QUERY, SORT_ORDERS_BY_STATUS_PRODUCTFAMILY)),
				createSortQueryData(BY_DATE_ASC,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_LANETYPE_QUERY, SORT_ORDERS_BY_DATE_ASC_PRODUCTFAMILY)),
				createSortQueryData(BY_MSO_ASC,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_LANETYPE_QUERY, SORT_ORDERS_BY_MSO_ASC_PRODUCTFAMILY)),
				createSortQueryData(BY_ORDER_STATUS_ASC,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_LANETYPE_QUERY, SORT_ORDERS_BY_STATUS_ASC_PRODUCTFAMILY)));
		return sortQueries;

	}

	/**
	 * Creates the queries for search text.
	 *
	 * @param pageableData
	 *           the pageable data
	 * @param queryParams
	 *           the query params
	 * @return the list of sortQueries
	 */
	private List<SortQueryData> createQueriesForTextSearch(final PageableData pageableData, final Map<String, Object> queryParams)
	{
		List<SortQueryData> sortQueries;
		queryParams.put(OrderModel.MSO, LIKE_CHARACTER + pageableData.getSearch().toUpperCase() + LIKE_CHARACTER);
		queryParams.put(OrderModel.PURCHASEORDERNUMBER, LIKE_CHARACTER + pageableData.getSearch().toUpperCase() + LIKE_CHARACTER);
		sortQueries = Arrays.asList(
				createSortQueryData(BY_MSO,
						createQuery(FIND_ORDERS_BY_CUSTOMER_STORE_QUERY, FILTER_ORDER_BY_SEARCH_VALUE, SORT_ORDERS_BY_MSO)),
				createSortQueryData(BY_DATE,
						createQuery(FIND_ORDERS_BY_CUSTOMER_STORE_QUERY, FILTER_ORDER_BY_SEARCH_VALUE, SORT_ORDERS_BY_DATE)),
				createSortQueryData(BY_ORDER_STATUS,
						createQuery(FIND_ORDERS_BY_CUSTOMER_STORE_QUERY, FILTER_ORDER_BY_SEARCH_VALUE, SORT_ORDERS_BY_STATUS)),
				createSortQueryData(BY_DATE_ASC,
						createQuery(FIND_ORDERS_BY_CUSTOMER_STORE_QUERY, FILTER_ORDER_BY_SEARCH_VALUE, SORT_ORDERS_BY_DATE_ASC)),
				createSortQueryData(BY_MSO_ASC,
						createQuery(FIND_ORDERS_BY_CUSTOMER_STORE_QUERY, FILTER_ORDER_BY_SEARCH_VALUE, SORT_ORDERS_BY_MSO_ASC)),
				createSortQueryData(BY_ORDER_STATUS_ASC,
						createQuery(FIND_ORDERS_BY_CUSTOMER_STORE_QUERY, FILTER_ORDER_BY_SEARCH_VALUE, SORT_ORDERS_BY_STATUS_ASC)));
		return sortQueries;
	}


	/**
	 * Creates the queries for lane type.
	 *
	 * @param pageableData
	 *           the pageable data
	 * @param queryParams
	 *           the query params
	 * @return the list of sortQueries
	 */
	private List<SortQueryData> createQueriesForLaneType(final PageableData pageableData, final Map<String, Object> queryParams)
	{
		List<SortQueryData> sortQueries;
		queryParams.put(LANE_TYPE, pageableData.getLaneType());
		sortQueries = Arrays.asList(
				createSortQueryData(BY_MSO, createQuery(FIND_ORDERS_BY_CUSTOMER_LANE_TYPE_QUERY, SORT_ORDERS_BY_MSO_PRODUCTFAMILY)),
				createSortQueryData(BY_DATE, createQuery(FIND_ORDERS_BY_CUSTOMER_LANE_TYPE_QUERY, SORT_ORDERS_BY_DATE_PRODUCTFAMILY)),
				createSortQueryData(BY_ORDER_STATUS,
						createQuery(FIND_ORDERS_BY_CUSTOMER_LANE_TYPE_QUERY, SORT_ORDERS_BY_STATUS_PRODUCTFAMILY)),
				createSortQueryData(BY_DATE_ASC,
						createQuery(FIND_ORDERS_BY_CUSTOMER_LANE_TYPE_QUERY, SORT_ORDERS_BY_DATE_ASC_PRODUCTFAMILY)),
				createSortQueryData(BY_MSO_ASC,
						createQuery(FIND_ORDERS_BY_CUSTOMER_LANE_TYPE_QUERY, SORT_ORDERS_BY_MSO_ASC_PRODUCTFAMILY)),
				createSortQueryData(BY_ORDER_STATUS_ASC,
						createQuery(FIND_ORDERS_BY_CUSTOMER_LANE_TYPE_QUERY, SORT_ORDERS_BY_STATUS_ASC_PRODUCTFAMILY)));
		return sortQueries;
	}


	/**
	 * Creates the queries for category.
	 *
	 * @param pageableData
	 *           the pageable data
	 * @param queryParams
	 *           the query params
	 * @return the list of sortQueries
	 */
	private List<SortQueryData> createQueriesForCategory(final PageableData pageableData, final Map<String, Object> queryParams)
	{
		List<SortQueryData> sortQueries;
		queryParams.put(CategoryModel.NAME, pageableData.getProductFamilySearch().toUpperCase());
		sortQueries = Arrays.asList(
				createSortQueryData(BY_MSO,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_QUERY, SORT_ORDERS_BY_MSO_PRODUCTFAMILY)),
				createSortQueryData(BY_DATE,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_QUERY, SORT_ORDERS_BY_DATE_PRODUCTFAMILY)),
				createSortQueryData(BY_ORDER_STATUS,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_QUERY, SORT_ORDERS_BY_STATUS_PRODUCTFAMILY)),
				createSortQueryData(BY_DATE_ASC,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_QUERY, SORT_ORDERS_BY_DATE_ASC_PRODUCTFAMILY)),
				createSortQueryData(BY_MSO_ASC,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_QUERY, SORT_ORDERS_BY_MSO_ASC_PRODUCTFAMILY)),
				createSortQueryData(BY_ORDER_STATUS_ASC,
						createQuery(FIND_ORDERS_BY_CUSTOMER_PRODUCTFAMILY_QUERY, SORT_ORDERS_BY_STATUS_ASC_PRODUCTFAMILY)));
		return sortQueries;
	}

	/**
	 * Creates the queries for customer.
	 *
	 * @return the list of sortQueries
	 */
	private List<SortQueryData> createQueriesForCustomer()
	{
		List<SortQueryData> sortQueries;
		sortQueries = Arrays.asList(
				createSortQueryData(BY_MSO, createQuery(FIND_ORDERS_BY_CUSTOMER_STORE_QUERY, SORT_ORDERS_BY_MSO)),
				createSortQueryData(BY_DATE, createQuery(FIND_ORDERS_BY_CUSTOMER_STORE_QUERY, SORT_ORDERS_BY_DATE)),
				createSortQueryData(BY_ORDER_STATUS, createQuery(FIND_ORDERS_BY_CUSTOMER_STORE_QUERY, SORT_ORDERS_BY_STATUS)),
				createSortQueryData(BY_DATE_ASC, createQuery(FIND_ORDERS_BY_CUSTOMER_STORE_QUERY, SORT_ORDERS_BY_DATE_ASC)),
				createSortQueryData(BY_MSO_ASC, createQuery(FIND_ORDERS_BY_CUSTOMER_STORE_QUERY, SORT_ORDERS_BY_MSO_ASC)),
				createSortQueryData(BY_ORDER_STATUS_ASC,
						createQuery(FIND_ORDERS_BY_CUSTOMER_STORE_QUERY, SORT_ORDERS_BY_STATUS_ASC)));
		return sortQueries;
	}

}