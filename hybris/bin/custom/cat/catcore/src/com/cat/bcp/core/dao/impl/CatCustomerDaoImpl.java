/**
 *
 */
package com.cat.bcp.core.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.CatCustomerDao;
import com.cat.bcp.core.model.CatCountyModel;
import com.cat.bcp.core.model.OrderAdditionalInfoModel;


/**
 * The Class CatCustomerDaoImpl used to fetch customer details, email suggestions and other customer information.
 *
 * @author sagdhingra
 */
public class CatCustomerDaoImpl implements CatCustomerDao
{

	private static final Logger LOG = Logger.getLogger(CatCustomerDaoImpl.class);
	private FlexibleSearchService flexibleSearchService;

	private SearchRestrictionService searchRestrictionService;

	private static final String SELECT_QUERY = "SELECT {";
	private static final String FROM_QUERY = "} FROM {";
	private static final String WHERE_LOWER_QUERY = "!} WHERE LOWER ({";
	private static final String UTF8_STR = "UTF-8";
	private static final String UTF8_WARN_STR = "Unable to Decode in UTF-8 format :";


	private static final String NAME_AUTO_SUGGESTIONS = SELECT_QUERY + CustomerModel.NAME + FROM_QUERY + CustomerModel._TYPECODE
			+ WHERE_LOWER_QUERY + CustomerModel.NAME + "}) like ?name ORDER BY {" + CustomerModel.NAME + "} ASC";


	private static final String EMAIL_AUTO_SUGGESTIONS = SELECT_QUERY + CustomerModel.CATCUSTOMEREMAILID + FROM_QUERY
			+ CustomerModel._TYPECODE + WHERE_LOWER_QUERY + CustomerModel.CATCUSTOMEREMAILID + "}) like ?email ORDER BY {"
			+ CustomerModel.CATCUSTOMEREMAILID + "} ASC";

	private static final String CUSTOMER_EMAIL_RESULTS = SELECT_QUERY + CustomerModel.PK + FROM_QUERY + CustomerModel._TYPECODE
			+ WHERE_LOWER_QUERY + CustomerModel.CATCUSTOMEREMAILID + "}) like ?email";


	private static final String CUSTOMER_NAME_RESULTS = SELECT_QUERY + CustomerModel.PK + FROM_QUERY + CustomerModel._TYPECODE
			+ WHERE_LOWER_QUERY + CustomerModel.NAME + "}) like ?name";

	private static final String FIND_USER_BY_EMAIL_ID = SELECT_QUERY + CustomerModel.PK + FROM_QUERY + CustomerModel._TYPECODE
			+ "} WHERE {" + CustomerModel.CATCUSTOMEREMAILID + "}=?" + CustomerModel.CATCUSTOMEREMAILID;


	private static final String FIND_ORDERS_ADDITIONAL_INFO_BY_CUSTOMER = "SELECT distinct{OAI.productFamily}" + " FROM {"
			+ OrderAdditionalInfoModel._TYPECODE + " AS OAI " + " JOIN " + OrderModel._TYPECODE + " as O on {O.pk}={OAI.order} "
			+ "} WHERE {O.user} = ?" + OrderModel.USER;

	private static final String FIND_INFO_BY_ORDER = "SELECT { " + OrderAdditionalInfoModel.PK + " } FROM {"
			+ OrderAdditionalInfoModel._TYPECODE + " } WHERE { " + OrderAdditionalInfoModel.ORDER + " } = ?"
			+ OrderAdditionalInfoModel.ORDER;
	private static final String GET_COUNTIES_FOR_DEALER = "select {pk} from {CatCounty} where {mainDealer} in ({{select {pk} from {B2BUnit} where {uid}=?mainDealer}}) and {branchDealer} in ({{select {pk} from {B2BUnit} where {uid}=?branchDealer}}) and {isMapped}=?isMapped order by {countyName}";

	private static final String GET_COUNTIES_FOR_DEALER_SORTED_BY_COUNTRY = "select {county.pk} from {CatCounty as county JOIN Country as country on {county.sellCountry}={country.pk}} where {county.mainDealer} in ({{select {pk} from {B2BUnit} where {uid}=?mainDealer}}) and {county.branchDealer} in ({{select {pk} from {B2BUnit} where {uid}=?branchDealer}}) and {isMapped}=?isMapped order by {country.name} ";

	private static final String GET_COUNTIES_FOR_DEALER_SORTED_BY_STATE = "select {county.pk} from {CatCounty as county JOIN Region as region on {county.sellState}={region.pk}} where {county.mainDealer} in ({{select {pk} from {B2BUnit} where {uid}=?mainDealer}}) and {county.branchDealer} in ({{select {pk} from {B2BUnit} where {uid}=?branchDealer}}) and {isMapped}=?isMapped order by {region.name} ";

	private static final String MAIN_DEALER = "mainDealer";
	private static final String BRANCH_DEALER = "branchDealer";
	private static final String COUNTY_NAME = "countyName";
	private static final String COUNTRY = "country";
	private static final String REGION = "state";
	private static final String IS_MAPPED = "isMapped";

	/**
	 * Gets the search restriction service.
	 *
	 * @return the searchRestrictionService
	 */
	public SearchRestrictionService getSearchRestrictionService()
	{
		return searchRestrictionService;
	}



	/**
	 * @param searchRestrictionService
	 *           the searchRestrictionService to set
	 */
	public void setSearchRestrictionService(final SearchRestrictionService searchRestrictionService)
	{
		this.searchRestrictionService = searchRestrictionService;
	}


	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCustomerNameSuggestions(final String name)
	{
		final Map params = new HashMap();
		try
		{
			final String decodedName = URLDecoder.decode(name.toLowerCase(), UTF8_STR);
			params.put("name", decodedName + "%");
		}
		catch (final UnsupportedEncodingException e)
		{
			LOG.warn(UTF8_WARN_STR, e);
		}
		final FlexibleSearchQuery query = new FlexibleSearchQuery(NAME_AUTO_SUGGESTIONS);
		query.addQueryParameters(params);
		query.setCount(CatCoreConstants.EMAIL_SUGGESTION_COUNT);
		query.setResultClassList(java.util.Arrays.asList(String.class));
		searchRestrictionService.disableSearchRestrictions();
		final SearchResult<List<String>> result = getFlexibleSearchService().search(query);
		searchRestrictionService.enableSearchRestrictions();
		return (List) result.getResult();
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCustomerEmailSuggestions(final String email)
	{
		final Map params = new HashMap();
		try
		{
			final String decodedEmail = URLDecoder.decode(email.toLowerCase(), UTF8_STR);
			params.put("email", decodedEmail + "%");
		}
		catch (final UnsupportedEncodingException e)
		{
			LOG.warn(UTF8_WARN_STR, e);
		}
		final FlexibleSearchQuery query = new FlexibleSearchQuery(EMAIL_AUTO_SUGGESTIONS);
		query.addQueryParameters(params);
		query.setCount(CatCoreConstants.EMAIL_SUGGESTION_COUNT);
		query.setResultClassList(java.util.Arrays.asList(String.class));
		searchRestrictionService.disableSearchRestrictions();
		final SearchResult<List<String>> result = getFlexibleSearchService().search(query);
		searchRestrictionService.enableSearchRestrictions();
		return (List) result.getResult();
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public CustomerModel fetchExistingCustomerDetailsByEmail(final String email)
	{
		final Map params = new HashMap();
		try
		{
			params.put("email", URLDecoder.decode(email.toLowerCase(), UTF8_STR));
		}
		catch (final UnsupportedEncodingException e)
		{
			LOG.warn(UTF8_WARN_STR, e);
		}
		final FlexibleSearchQuery query = new FlexibleSearchQuery(CUSTOMER_EMAIL_RESULTS);
		query.addQueryParameters(params);
		searchRestrictionService.disableSearchRestrictions();
		final SearchResult<CustomerModel> result = getFlexibleSearchService().search(query);
		searchRestrictionService.enableSearchRestrictions();
		final List<CustomerModel> customerList = result.getResult();
		return CollectionUtils.isNotEmpty(customerList) ? customerList.get(0) : null;

	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public CustomerModel fetchExistingCustomerDetailsByName(final String name)
	{
		final Map params = new HashMap();
		try
		{
			params.put("name", URLDecoder.decode(name.toLowerCase(), UTF8_STR));
		}
		catch (final UnsupportedEncodingException e)
		{
			LOG.warn(UTF8_WARN_STR, e);
		}
		final FlexibleSearchQuery query = new FlexibleSearchQuery(CUSTOMER_NAME_RESULTS);
		query.addQueryParameters(params);
		searchRestrictionService.disableSearchRestrictions();
		final SearchResult<CustomerModel> result = getFlexibleSearchService().search(query);
		searchRestrictionService.enableSearchRestrictions();
		final List<CustomerModel> customerList = result.getResult();
		return CollectionUtils.isNotEmpty(customerList) ? customerList.get(0) : null;

	}

	@Override
	public CustomerModel findUserByCustomerEmail(final String customerEmail)
	{
		final FlexibleSearchQuery query;

		query = new FlexibleSearchQuery(FIND_USER_BY_EMAIL_ID);
		query.addQueryParameter(CustomerModel.CATCUSTOMEREMAILID, customerEmail);
		final List<UserModel> resList = getFlexibleSearchService().<UserModel> search(query).getResult();

		return resList.isEmpty() ? null : (CustomerModel) resList.get(0);

	}

	@Override
	public List<CategoryModel> findProductFamilies(final CustomerModel customerModel)
	{
		validateParameterNotNull(customerModel, "Customer must not be null");

		final FlexibleSearchQuery query;

		query = new FlexibleSearchQuery(FIND_ORDERS_ADDITIONAL_INFO_BY_CUSTOMER);
		query.addQueryParameter(OrderModel.USER, customerModel);
		final List<CategoryModel> resList = getFlexibleSearchService().<CategoryModel> search(query).getResult();

		return resList.isEmpty() ? null : resList;
	}

	@Override
	public OrderAdditionalInfoModel getAdditionalInfoForOrder(final OrderModel orderModel)
	{
		validateParameterNotNull(orderModel, "Order must not be null");

		final FlexibleSearchQuery query;

		query = new FlexibleSearchQuery(FIND_INFO_BY_ORDER);
		query.addQueryParameter(OrderAdditionalInfoModel.ORDER, orderModel);
		final List<OrderAdditionalInfoModel> resList = getFlexibleSearchService().<OrderAdditionalInfoModel> search(query)
				.getResult();

		return resList.isEmpty() ? null : resList.get(0);
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CatCountyModel> getCountyDataForDealer(final String dealerShipTo, final String dealerCode,
			final String sortParameter, final String sortOrder)
	{
		searchRestrictionService.disableSearchRestrictions();
		FlexibleSearchQuery query = null;
		List<CatCountyModel> resList = new ArrayList<>();
		if (COUNTY_NAME.equalsIgnoreCase(sortParameter))
		{
			query = new FlexibleSearchQuery(GET_COUNTIES_FOR_DEALER + " " + sortOrder);
		}
		else if (COUNTRY.equalsIgnoreCase(sortParameter))
		{
			query = new FlexibleSearchQuery(GET_COUNTIES_FOR_DEALER_SORTED_BY_COUNTRY + " " + sortOrder);
		}
		else if (REGION.equalsIgnoreCase(sortParameter))
		{
			query = new FlexibleSearchQuery(GET_COUNTIES_FOR_DEALER_SORTED_BY_STATE + " " + sortOrder);
		}
		if (null != query)
		{
			query.addQueryParameter(MAIN_DEALER, dealerCode);
			query.addQueryParameter(BRANCH_DEALER, dealerShipTo);
			query.addQueryParameter(IS_MAPPED, Boolean.TRUE);
			resList = getFlexibleSearchService().<CatCountyModel> search(query).getResult();
		}
		searchRestrictionService.enableSearchRestrictions();
		return resList.isEmpty() ? Collections.emptyList() : resList;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CatCountyModel> getAvailableCountyForShipTo(final String mainDealerCode)
	{
		searchRestrictionService.disableSearchRestrictions();
		final FlexibleSearchQuery query;
		query = new FlexibleSearchQuery(CatCoreConstants.GET_AVAILABLE_COUNTIES_FOR_DEALER);
		query.addQueryParameter(MAIN_DEALER, mainDealerCode);
		query.addQueryParameter(IS_MAPPED, Boolean.FALSE);
		final List<CatCountyModel> resList = getFlexibleSearchService().<CatCountyModel> search(query).getResult();
		searchRestrictionService.enableSearchRestrictions();
		return resList.isEmpty() ? Collections.emptyList() : resList;
	}



}
