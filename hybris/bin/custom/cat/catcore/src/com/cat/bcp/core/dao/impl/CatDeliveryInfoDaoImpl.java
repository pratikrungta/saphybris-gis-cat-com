/**
 *
 */
package com.cat.bcp.core.dao.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.CatDeliveryInfoDao;
import com.cat.bcp.core.model.DeliveryInfoModel;


/**
 * Implementation class to perform all DAO functionalities for DeliveryInfo.
 *
 * @author megverma
 *
 */
public class CatDeliveryInfoDaoImpl extends AbstractItemDao implements CatDeliveryInfoDao
{
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	private static final String PRODUCT_PARAM = "product";
	private static final String DELIVERY_INFO_DEBUG = "Getting Delivery Infos for Product configuration : ";

	private static final Logger LOG = Logger.getLogger(CatDeliveryInfoDaoImpl.class);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DeliveryInfoModel getDeliveryInfoByMSO(final String msoNumber)
	{
		List<DeliveryInfoModel> deliveryInfos = null;
		DeliveryInfoModel deliveryInfo = null;

		final Map<String, Object> queryParams = new HashMap<>();

		if (msoNumber != null)
		{
			queryParams.put("msoNumber", msoNumber);

			if (LOG.isDebugEnabled())
			{
				LOG.debug("Getting Delivery Info for MSO number : " + msoNumber);
			}

			final SearchResult<DeliveryInfoModel> result = getFlexibleSearchService()
					.search(CatCoreConstants.FIND_DELIVERY_INFO_BY_MSO, queryParams);

			if (result != null)
			{
				deliveryInfos = result.getResult();

				if (CollectionUtils.isNotEmpty(deliveryInfos))
				{
					deliveryInfo = deliveryInfos.get(0);
				}
			}

		}

		return deliveryInfo;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DeliveryInfoModel> getProductDeliveryInfo(final ProductModel product)
	{
		List<DeliveryInfoModel> deliveryInfos = null;

		final Map<String, Object> queryParams = new HashMap<>();

		if (null != product)
		{
			queryParams.put(PRODUCT_PARAM, product);

			if (LOG.isDebugEnabled())
			{
				LOG.debug(DELIVERY_INFO_DEBUG + product.getCode());
			}

			final SearchResult<DeliveryInfoModel> result = getFlexibleSearchService()
					.search(CatCoreConstants.FIND_PRODUCT_DELIVERY_INFO, queryParams);

			if (result != null)
			{
				deliveryInfos = result.getResult();
			}

		}

		return deliveryInfos;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Boolean isConfigurationInTransit(final ProductModel product)
	{
		Boolean isConfigInTransit = Boolean.FALSE;

		final Map<String, Object> queryParams = new HashMap<>();

		if (null != product)
		{
			queryParams.put(PRODUCT_PARAM, product);

			if (LOG.isDebugEnabled())
			{
				LOG.debug(DELIVERY_INFO_DEBUG + product.getCode());
			}

			final SearchResult<DeliveryInfoModel> result = getFlexibleSearchService()
					.search(CatCoreConstants.FIND_PRODUCT_DELIVERY_INFO, queryParams);

			if (null != result && null != result.getResult() && result.getResult().size() > 0)
			{
				isConfigInTransit = Boolean.TRUE;

			}

		}

		return isConfigInTransit;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer getInTransitConfigurationCount(final ProductModel product)
	{
		Integer configInTransitCount = null;

		final Map<String, Object> queryParams = new HashMap<>();

		if (null != product)
		{
			queryParams.put(PRODUCT_PARAM, product);

			if (LOG.isDebugEnabled())
			{
				LOG.debug(DELIVERY_INFO_DEBUG + product.getCode());
			}

			final SearchResult<DeliveryInfoModel> result = getFlexibleSearchService()
					.search(CatCoreConstants.FIND_PRODUCT_DELIVERY_INFO, queryParams);

			if (null != result && null != result.getResult())
			{
				configInTransitCount = result.getResult().size();

			}

		}

		return configInTransitCount;
	}

}
