/**
 *
 */
package com.cat.bcp.core.dao.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.List;

import javax.annotation.Resource;

import com.cat.bcp.core.dao.CatOrderAcknowledgementDao;


/**
 * @author ravjonnalagadda
 *
 */
public class CatOrderAcknowledgementDaoImpl implements CatOrderAcknowledgementDao
{
	@Resource
	private FlexibleSearchService flexibleSearchService;

	/**
	 * {@inheritDoc}
	 *
	 **/
	@Override
	public List<OrderModel> getOrdersList(final String orderQuery)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(orderQuery);
		final SearchResult<OrderModel> result = flexibleSearchService.search(query);
		return result.getResult();

	}






}
