/**
 *
 */
package com.cat.bcp.core.dao.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.CatOrderLaneDao;
import com.cat.bcp.core.model.CatLaneOrderingWindowModel;
import com.cat.bcp.core.model.ProductLaneInfoModel;


/**
 * Class to check the validations for adding to cart
 *
 * @author sagdhingra
 *
 */
public class CatOrderLaneDaoImpl implements CatOrderLaneDao
{


	@Resource(name = "flexibleSearchService")
	FlexibleSearchService flexibleSearchService;

	private static final String SALES_MODEL_ID = "salesModelId";
	private static final String DEALER = "dealer";
	private static final String MONTH_YEAR = "monthYear";
	private static final String ORDERED_QTY = "orderedQuantity";
	private static final String LANE_CODE = "laneCode";

	private static final String CURRENT_DATE = "currentDate";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CatLaneOrderingWindowModel> getOrderWindowForLanes(final Date currentDate)
	{
		final FlexibleSearchQuery orderWindowQuery = new FlexibleSearchQuery(CatCoreConstants.LANE_ORDER_WINDOW_QUERY);
		orderWindowQuery.addQueryParameter(CURRENT_DATE, currentDate);

		final SearchResult<CatLaneOrderingWindowModel> orderWindowResults = flexibleSearchService.search(orderWindowQuery);

		return orderWindowResults.getResult().isEmpty() ? Collections.emptyList() : orderWindowResults.getResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductLaneInfoModel> getProductLaneInfo(final B2BUnitModel b2bUnit, final String monthYear)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.PRODUCT_LANE_INFO);
		query.addQueryParameter(DEALER, b2bUnit);
		query.addQueryParameter(MONTH_YEAR, monthYear);
		final SearchResult<ProductLaneInfoModel> searchResult = flexibleSearchService.search(query);

		if (null != searchResult.getResult() && !searchResult.getResult().isEmpty())
		{
			return searchResult.getResult();
		}
		return Collections.emptyList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductLaneInfoModel> getProductLaneInfoForSalesModel(final String salesModelId, final B2BUnitModel b2bUnit,
			final String monthYear)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.PRODUCT_LANE_INFO_SALES);
		query.addQueryParameter(SALES_MODEL_ID, salesModelId);
		query.addQueryParameter(DEALER, b2bUnit);
		query.addQueryParameter(MONTH_YEAR, monthYear);
		final SearchResult<ProductLaneInfoModel> searchResult = flexibleSearchService.search(query);

		if (null != searchResult.getResult() && !searchResult.getResult().isEmpty())
		{
			return searchResult.getResult();
		}
		return Collections.emptyList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductLaneInfoModel> getLaneInfoForLaneType(final String laneCode, final String salesModelId,
			final B2BUnitModel b2bUnit, final String monthYear)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.PRODUCT_LANE_INFO_LANETYPE);
		query.addQueryParameter(SALES_MODEL_ID, salesModelId);
		query.addQueryParameter(DEALER, b2bUnit);
		query.addQueryParameter(MONTH_YEAR, monthYear);
		query.addQueryParameter(LANE_CODE, laneCode);
		final SearchResult<ProductLaneInfoModel> searchResult = flexibleSearchService.search(query);

		if (null != searchResult.getResult() && !searchResult.getResult().isEmpty())
		{
			return searchResult.getResult();
		}
		return Collections.emptyList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductLaneInfoModel> getProductLaneInfoForOrderedQuantity(final String salesModelId, final B2BUnitModel b2bUnit,
			final String monthYear, final int orderedQty)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.PRODUCT_LANE_INFO_FOR_ORDERED_QTY);
		query.addQueryParameter(SALES_MODEL_ID, salesModelId);
		query.addQueryParameter(DEALER, b2bUnit);
		query.addQueryParameter(MONTH_YEAR, monthYear);
		query.addQueryParameter(ORDERED_QTY, orderedQty);
		final SearchResult<ProductLaneInfoModel> searchResult = flexibleSearchService.search(query);

		if (null != searchResult.getResult() && !searchResult.getResult().isEmpty())
		{
			return searchResult.getResult();
		}
		return Collections.emptyList();
	}


}
