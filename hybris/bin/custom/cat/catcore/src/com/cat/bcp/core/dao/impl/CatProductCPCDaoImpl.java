package com.cat.bcp.core.dao.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.util.Config;

import java.util.Collections;
import java.util.List;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.CatProductCPCDao;



/**
 * @author ravjonnalagadda
 * 
 * 
 */
/**
 * {@inheritDoc}
 */
public class CatProductCPCDaoImpl implements CatProductCPCDao
{
	private FlexibleSearchService flexibleSearchService;

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	@Override
	public List<ProductModel> getProductModelList()
	{
		final StringBuilder stringBuilder = new StringBuilder(Config.getParameter(CatCoreConstants.MEDIA_QUERY));
		final String query = stringBuilder.toString();
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(query);
		final SearchResult<ProductModel> result = getFlexibleSearchService().search(flexibleSearchQuery);

		if (result.getCount() > 0)
		{

			return result.getResult();
		}
		return Collections.emptyList();

	}

}


