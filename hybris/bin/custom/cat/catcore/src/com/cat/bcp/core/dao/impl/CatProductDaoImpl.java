/**
 *
 */
package com.cat.bcp.core.dao.impl;

import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.daos.impl.DefaultProductDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;

import java.util.Collections;
import java.util.List;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.CatProductDao;


/**
 * @author sagdhingra
 *
 */
public class CatProductDaoImpl extends DefaultProductDao implements CatProductDao
{


	/**
	 * @param typecode
	 */
	public CatProductDaoImpl(final String typecode)
	{
		super(typecode);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductReferenceModel> getAccessoryData()
	{
		final FlexibleSearchQuery query;
		query = new FlexibleSearchQuery(CatCoreConstants.GET_PRODUCTREFERENCE_FROM_PRODUCTREFERENCES);
		final List<ProductReferenceModel> resList = getFlexibleSearchService().<ProductReferenceModel> search(query).getResult();
		return resList.isEmpty() ? Collections.emptyList() : resList;
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductModel> getCUVModels()
	{
		final FlexibleSearchQuery query;
		query = new FlexibleSearchQuery(CatCoreConstants.GET_CUV_MODELS);
		final List<ProductModel> resList = getFlexibleSearchService().<ProductModel> search(query).getResult();
		return resList.isEmpty() ? Collections.emptyList() : resList;
	}


}
