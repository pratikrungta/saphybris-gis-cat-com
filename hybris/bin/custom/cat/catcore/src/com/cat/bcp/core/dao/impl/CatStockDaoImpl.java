/**
 *
 */
package com.cat.bcp.core.dao.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.fest.util.Collections;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.CatStockDao;
import com.cat.bcp.core.model.AlertProductModel;
import com.cat.bcp.core.model.OrderWindowModel;
import com.cat.bcp.core.model.RestockAlertModel;
import com.cat.bcp.core.model.ShipToAfterDateModel;


/**
 * @author avaijapurkar
 *
 */
public class CatStockDaoImpl implements CatStockDao
{

	@Resource(name = "flexibleSearchService")
	FlexibleSearchService flexibleSearchService;

	@Resource(name = "modelService")
	ModelService modelService;

	@Resource(name = "productService")
	ProductService productService;

	private static final String ALERT_ID_PARAM = "alertId";

	/*
	 * (non-Javadoc)
	 *
	 * @see com.cat.bcp.core.dao.DealerStockDao#updateAlertEntry(java.lang.String, java.lang.String, java.lang.String)
	 */
		@Override
	public void updateAlertEntry(final String alertId, final String configProduct, final String orderedQty)
	{
		RestockAlertModel restockAlertModel = null;
		final FlexibleSearchQuery restockAlertQuery = new FlexibleSearchQuery(CatCoreConstants.GET_RESTOCK_ALERT);
		restockAlertQuery.addQueryParameter(ALERT_ID_PARAM, alertId);
		final SearchResult<RestockAlertModel> alertSearchResult = flexibleSearchService.search(restockAlertQuery);
		if (null != alertSearchResult.getResult() && !alertSearchResult.getResult().isEmpty())
		{
			restockAlertModel = alertSearchResult.getResult().get(0);
			final int currentStockLevel = restockAlertModel.getCurrentStockLevel() != null ? restockAlertModel.getCurrentStockLevel()
					: 0;
			final int updatedStockLevel = currentStockLevel + Integer.parseInt(orderedQty);
			restockAlertModel.setCurrentStockLevel(updatedStockLevel);
			modelService.save(restockAlertModel);


			final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.GET_CONFIG_VARIANT_QUERY);
			query.addQueryParameter(ALERT_ID_PARAM, restockAlertModel);
			query.addQueryParameter("product", productService.getProductForCode(configProduct));

			final SearchResult<AlertProductModel> searchResult = flexibleSearchService.search(query);
			if (null != searchResult.getResult() && !searchResult.getResult().isEmpty())
			{
				final AlertProductModel alertProduct = searchResult.getResult().get(0);
				final int existingQty = alertProduct.getOrderedQuantity() != null ? alertProduct.getOrderedQuantity() : 0;
				final int updatedQty = existingQty + Integer.parseInt(orderedQty);
				alertProduct.setOrderedQuantity(updatedQty);
				modelService.save(alertProduct);
			}
		}
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestockAlertModel getStockForSalesModel(final String alertId)
	{
		RestockAlertModel restockAlert = null;

		final Map<String, Object> queryParams = new HashMap<>();

		queryParams.put(ALERT_ID_PARAM, alertId);

		final SearchResult<RestockAlertModel> result = flexibleSearchService.search(CatCoreConstants.GET_RESTOCK_ALERT_QUERY,
				queryParams);

		if (null != result.getResult() && !result.getResult().isEmpty())
		{
			restockAlert = result.getResult().get(0);
		}

		return restockAlert;

	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public OrderWindowModel getOrderWindowAndShipAfterDates(final List<ProductModel> prodList)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.GET_ORDER_WINDOW);
		query.addQueryParameter("currentDate", new Date());

		final OrderWindowModel owm = modelService.create(OrderWindowModel.class);

		final SearchResult<OrderWindowModel> searchResult = flexibleSearchService.search(query);
		final Set<ShipToAfterDateModel> shipAfterSet = new HashSet();
		final List<String> prodCodeList = new ArrayList();
		if (!Collections.isEmpty(prodList))
		{
			for (final ProductModel prodModel : prodList)
			{
				prodCodeList.add(prodModel.getCode());
			}
		}

		if (!Collections.isEmpty(searchResult.getResult()))
		{
			for (final OrderWindowModel orderWindowModel : searchResult.getResult())
			{
				final List<String> owProdCodeList = new ArrayList();
				for (final ProductModel prodModel : orderWindowModel.getProduct())
				{
					owProdCodeList.add(prodModel.getCode());
				}
				if (prodList == null)
				{
					shipAfterSet.addAll(orderWindowModel.getShipToAfterDate());
				}
				else if (prodCodeList.iterator().hasNext() && owProdCodeList.contains(prodCodeList.iterator().next()))
				{
					shipAfterSet.addAll(orderWindowModel.getShipToAfterDate());
				}
			}
			owm.setShipToAfterDate(shipAfterSet);
			return owm;
		}
		return null;
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public Boolean isIMAddToCartConfigProduct(final String alertId, final ProductModel productModel)
	{
		final Map<String, Object> queryParams = new HashMap<>();

		queryParams.put(ALERT_ID_PARAM, alertId);
		queryParams.put("product", productModel);

		final SearchResult<AlertProductModel> result = flexibleSearchService
				.search(CatCoreConstants.GET_IM_ADD_TO_CART_CONFIG_VARIANT_QUERY, queryParams);

		if (null != result.getResult() && !result.getResult().isEmpty())
		{
			return Boolean.TRUE;
		}
		else
		{
			return Boolean.FALSE;
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ShipToAfterDateModel> getShipToDates(final OrderWindowModel orderWindow)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.GET_SHIPTO_DATES);
		query.addQueryParameter("orderWindow", orderWindow);

		final SearchResult<ShipToAfterDateModel> searchResult = flexibleSearchService.search(query);


		return searchResult.getResult();
	}




}
