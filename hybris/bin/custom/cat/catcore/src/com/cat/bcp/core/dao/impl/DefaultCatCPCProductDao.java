package com.cat.bcp.core.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import com.cat.bcp.core.dao.CatCPCProductDao;
import com.cat.bcp.core.model.BaseVariantProductModel;


/**
 * The class implements the method defined in the interface {@link CatCPCProductDao}
 */
public class DefaultCatCPCProductDao implements CatCPCProductDao
{

    @Resource
    private FlexibleSearchService flexibleSearchService;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<BaseVariantProductModel> getProductforSpecificationRecords(final String productFamily)
    {
        final StringBuilder specificationRecordBuilder = new StringBuilder();
        specificationRecordBuilder.append(
                "select distinct {b.pk} from {BaseVariantproduct! as b join categoryproductrelation as c on {b.pk}={c.target} join Category as cat on {cat.pk}={c.source} join catalog as cata on {b.catalog}={cata.pk} join catalogversion as cv on {b.catalogversion}={cv.pk}} where {cat.name} in (?productFamily) and {cv.version}='Staged' and {cata.id}='NAProductCatalog'");
        final FlexibleSearchQuery specificationQuery = new FlexibleSearchQuery(specificationRecordBuilder.toString());
        specificationQuery.addQueryParameter("productFamily", productFamily);
        final SearchResult<BaseVariantProductModel> result = flexibleSearchService.search(specificationQuery);
        return result.getResult();


    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getExistingCategoryForUTV()
    {
        final String existingClassificationclassForUtv = "select {cat1.code} from {category as cat join categorycategoryrelation as c on {cat.pk}={c.target} join category as cat1 on {c.source}={cat1.pk} join catalog as cata on {cat.catalog}={cata.pk} join catalogversion as cv on {cat.catalogversion}={cv.pk}} where {cat.code}='utv' AND {cv.version}='Staged' and {cata.id}='NAProductCatalog' and {cat1.code} not in ('UTV_PROD','new','equipment')";
        final FlexibleSearchQuery query = new FlexibleSearchQuery(existingClassificationclassForUtv);
        query.setResultClassList(Arrays.asList(String.class));
        final SearchResult<String> result = flexibleSearchService.search(query);
        return result.getResult();
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getListOfClassificationAttribute(final List<String> classificationClassList)
    {
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(
                "select {attr.code} from {ClassAttributeAssignment as class join ClassificationAttribute as attr on {attr.pk}={class.classificationattribute}");
        stringBuilder.append(
                " join Category as category on {category.pk}={class.classificationclass}} where {category.code} in (?classificationClassRemovalList)");
        stringBuilder.append("and {attr.code} not in ('ENGINE_MODEL','NET_POWER')");
        final FlexibleSearchQuery query = new FlexibleSearchQuery(stringBuilder.toString());
        query.addQueryParameter("classificationClassRemovalList", classificationClassList);
        query.setResultClassList(Arrays.asList(String.class));
        final SearchResult<String> result = flexibleSearchService.search(query);
        return result.getResult();
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public List<List<String>> getListOfUnClassifiedAttribute(List<String> totalAttributeRemovalList) {
        final StringBuilder unClassifiedAttributeQueryBuilder = new StringBuilder();
        unClassifiedAttributeQueryBuilder.append("select {pr.qualifier},{p.code} from { productfeature as pr  join product as p on {p.pk}={pr.product}")
                .append(" join catalog as cata on {p.catalog}={cata.pk}")
                .append(" join catalogversion as cv on {p.catalogversion}={cv.pk}}")
                .append(" where {cv.version}='Staged' and {cata.id}='NAProductCatalog'")
                .append (" and  {pr.qualifier} in ({{select ({cata.id}||'/'||{cv.version}||'/'||{category.code}||'.'||lower({attr.code})) as qualifier")
                .append(" from {ClassAttributeAssignment as class join ClassificationAttribute as attr on {attr.pk}={class.classificationattribute}")
                .append(" join Category as category on {category.pk}={class.classificationclass}   join catalog as cata on {category.catalog}={cata.pk} ")
                .append(" join catalogversion as cv on {category.catalogversion}={cv.pk}}")
                .append(" where {attr.code} in (?totalAttributeRemovalList) }})");
        final FlexibleSearchQuery query = new FlexibleSearchQuery(unClassifiedAttributeQueryBuilder.toString());
        query.addQueryParameter("totalAttributeRemovalList", totalAttributeRemovalList);
        query.setResultClassList(Arrays.asList(String.class,String.class));
        final SearchResult<List<String>> result = flexibleSearchService.search(query);
        return result.getResult();
    }


}
