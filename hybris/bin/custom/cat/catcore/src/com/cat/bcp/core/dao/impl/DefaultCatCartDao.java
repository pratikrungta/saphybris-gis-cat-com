/**
 *
 */
package com.cat.bcp.core.dao.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.CatCartDao;
import com.cat.bcp.core.model.CatAdditionalInfoModel;
import com.cat.bcp.core.model.FBCPartsOrderInfoModel;
import com.cat.core.integration.fbc.request.Selection;
import com.cat.facades.order.PurchaseOrderCheck;


/**
 * @author sparkibanda
 *
 */
public class DefaultCatCartDao extends AbstractItemDao implements CatCartDao
{
	public static final String ORDER_PO_QUERY = "Select DISTINCT {PK}  from {Order} where {unit} = ?unit and UPPER({purchaseOrderNumber}) in (?poList)";

	private static final String ADDITIONALINFO_QUERY = "select {PK} from {CatAdditionalInfo} where {type} IN ({{SELECT {PK} from {AdditionalInfoType} where {code} = ?type}})";

	private static final Logger LOG = Logger.getLogger(DefaultCatCartDao.class);

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.cat.bcp.core.dao.CatCartDao#getPurchaseOrderCheck()
	 */
	@Override
	public PurchaseOrderCheck checkPOList(final List<String> purchaseOrderNumberList, final B2BCustomerModel customerModel)
	{

		final PurchaseOrderCheck purchaseOrderCheck = new PurchaseOrderCheck();

		purchaseOrderCheck.setPurchaseOrderPresent(Boolean.TRUE);

		if (CollectionUtils.isNotEmpty(purchaseOrderNumberList))
		{
			final List<String> purchaseOrderNumberListUpperCase = purchaseOrderNumberList.stream().map(String::toUpperCase)
					.collect(Collectors.toList());
			final FlexibleSearchQuery query = new FlexibleSearchQuery(ORDER_PO_QUERY);

			query.addQueryParameter("unit", customerModel.getDefaultB2BUnit());
			query.addQueryParameter("poList", purchaseOrderNumberListUpperCase);
			LOG.info("db query formed" + query);
			final SearchResult<OrderModel> searchResult = search(query);

			if (CollectionUtils.isNotEmpty(searchResult.getResult()))
			{
				purchaseOrderCheck.setPurchaseOrderPresent(Boolean.FALSE);
				final List listOfErrorPO = new ArrayList<String>();
				for (final OrderModel order : searchResult.getResult())
				{
					listOfErrorPO.add(order.getPurchaseOrderNumber().toUpperCase());
				}
				purchaseOrderCheck.setListOfErrorPO(listOfErrorPO);
				purchaseOrderCheck.setDuplicatePONumbers(Boolean.FALSE);
			}
		}

		return purchaseOrderCheck;
	}

	/**
	 * This method is used to fetch Cat Additional Info(Used for EPP/CSA drop-down options)
	 *
	 * @param type
	 * @return List of CatAdditionalInfoModel
	 */
	@Override
	public List<CatAdditionalInfoModel> getCatAdditionalInfo(final String type)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(ADDITIONALINFO_QUERY);
		query.addQueryParameter("type", type);
		final SearchResult<CatAdditionalInfoModel> searchResult = search(query);
		return searchResult.getResult();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addFBCPartInfo(final List<Selection> selectionList, final String configId, final long qty)
	{
		for (final Selection selection : selectionList)
		{
			final String cartId = cartService.getSessionCart().getCode();
			final Integer entryId = cartService.getSessionCart().getEntries().size() - 1;

			final ProductModel productModel = productService.getProductForCode(configId);
			if (!selection.getFeatureCode().contains(productModel.getName()))
			{
				final FBCPartsOrderInfoModel fbcPartsInfoModel = new FBCPartsOrderInfoModel();
				fbcPartsInfoModel.setConfigurationId(configId);
				fbcPartsInfoModel.setPartId(selection.getReferenceNumber());
				fbcPartsInfoModel.setPrice(new Double(selection.getListPrice()));
				fbcPartsInfoModel.setCartId(cartId);
				fbcPartsInfoModel.setQuantity((int) qty);
				fbcPartsInfoModel.setPartName(selection.getDescription());
				fbcPartsInfoModel.setEntryId(entryId.toString());
				modelService.save(fbcPartsInfoModel);
			}


		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public SearchResult<FBCPartsOrderInfoModel> updateCartForConfigurableProducts()
	{
		final CartModel cartModel = cartService.getSessionCart();
		final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.GET_FBC_PARTS_FOR_CART);
		query.addQueryParameter("cartId", cartModel.getCode());
		final SearchResult<FBCPartsOrderInfoModel> searchResult = flexibleSearchService.search(query);
		return searchResult;
	}
}
