/**
 *
 */
package com.cat.bcp.core.dao.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.Collection;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.CatCategoryDao;


/**
 * @author manjam
 *
 */
public class DefaultCatCategoryDao extends AbstractItemDao implements CatCategoryDao
{

	/*
	 * Fetch Low stock Products Categories.
	 *
	 * @see com.cat.bcp.core.dao.CatCategoryDao#findLowStockProductsC(de.hybris.platform.b2b.model.B2BUnitModel)
	 */
	@Override
	public Collection<CategoryModel> findLowStockProducts(final B2BUnitModel b2bUnit)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.LOW_STOCK_CATEGORES_FILTER_QUERY);

		query.addQueryParameter("dealer", b2bUnit);
		query.addQueryParameter("isReorderable", Boolean.FALSE);

		final SearchResult<CategoryModel> searchResult = search(query);

		if (null != searchResult.getResult() && !searchResult.getResult().isEmpty())
		{
			return searchResult.getResult();
		}
		else
		{
			return new ArrayList<CategoryModel>();
		}
	}
}
