/**
 *
 */
package com.cat.bcp.core.dao.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.CatSpeccheckDao;


/**
 * @author sankale
 *
 */
public class DefaultCatSpeccheckDao implements CatSpeccheckDao
{
	@Resource(name = "flexibleSearchService")
	FlexibleSearchService flexibleSearchService;

	/**
	 * This method returns Sales Models for comparison against selected Category
	 *
	 * @see com.cat.bcp.core.dao.CatSpeccheckDao#getSalesModelForCompare(java.lang.String)
	 */
	@Override
	public List<ProductModel> getSalesModelForCompare(final String categoryCode, final String catalogId,
			final String catalogVersion)
	{

		final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.POPULATE_COMPARE_SALES_MODELS_QUERY);

		query.addQueryParameter("category", categoryCode);
		query.addQueryParameter("catalogId", catalogId);
		query.addQueryParameter("catalogVersion", catalogVersion);

		final SearchResult<ProductModel> searchResult = flexibleSearchService.search(query);
		if (CollectionUtils.isNotEmpty(searchResult.getResult()))
		{
			return searchResult.getResult();
		}
		else
		{
			return Collections.emptyList();
		}
	}

}
