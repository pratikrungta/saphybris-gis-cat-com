/**
 *
 */
package com.cat.bcp.core.dao.impl;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.internal.dao.DefaultGenericDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.DealerAddressDao;


/**
 * @author bidavda
 *
 */
public class DefaultDealerAddressDao extends DefaultGenericDao<AddressModel> implements DealerAddressDao
{

	@Resource(name = "flexibleSearchService")
	FlexibleSearchService flexibleSearchService;



	/**
	 * @param typecode
	 */
	public DefaultDealerAddressDao(final String typecode)
	{
		super(typecode);
	}

	@Override
	public List<AddressModel> findByPosId(final String posId)
	{
		final Map parameters = new HashMap();
		parameters.put(AddressModel.POSID, posId);
		return find(parameters);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.cat.bcp.core.dao.DealerAddressDao#findMFUorPDCByPosId(java.lang.String)
	 */
	@Override
	public List<PointOfServiceModel> findMFUorPDCByPosId(final String posId)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.GET_PDC_MFU_QUERY);

		query.addQueryParameter(AddressModel.POSID, posId);

		final SearchResult<PointOfServiceModel> searchResult = flexibleSearchService.search(query);

		if (null != searchResult.getResult() && !searchResult.getResult().isEmpty())
		{
			return searchResult.getResult();
		}
		else
		{
			return new ArrayList<>();
		}

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.cat.bcp.core.dao.DealerAddressDao#findDealerHouseBycode(java.lang.String)
	 */
	@Override
	public List<WarehouseModel> findWareHouseBycode(final String dealerCode)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.GET_WAREHOUSE_BY_DEALERCODE_QUERY);
		query.addQueryParameter("dealerCode", dealerCode);
		final SearchResult<WarehouseModel> searchResult = flexibleSearchService.search(query);
		if (null != searchResult.getResult() && !searchResult.getResult().isEmpty())
		{
			return searchResult.getResult();
		}
		else
		{
			return new ArrayList<>();
		}
	}
}
