/**
 *
 */
package com.cat.bcp.core.dao.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.DealerSearchDao;
import com.cat.bcp.core.model.AlertProductModel;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.RestockAlertModel;


/**
 * @author sankale
 *
 */
public class DefaultDealerSearchDao implements DealerSearchDao
{
	@Resource(name = "flexibleSearchService")
	FlexibleSearchService flexibleSearchService;

	@Resource(name = "productService")
	ProductService productService;

	@Resource(name = "modelService")
	ModelService modelService;

	@Resource(name = "categoryService")
	CategoryService categoryService;

	private static final String DEALER_PARAM = "dealer";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<BaseVariantProductModel> getBaseModels(final ProductModel productModel)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.POPULATE_BASE_MODELS_QUERY);

		query.addQueryParameter("salesModel", productModel);

		final SearchResult<BaseVariantProductModel> searchResult = flexibleSearchService.search(query);

		if (null != searchResult.getResult() && !searchResult.getResult().isEmpty())
		{
			return searchResult.getResult();
		}
		else
		{
			return new ArrayList<BaseVariantProductModel>();
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductModel> getSalesModels(final String categoryCode, final boolean isUsed)
	{
		final FlexibleSearchQuery query = !isUsed ? new FlexibleSearchQuery(CatCoreConstants.POPULATE_SALES_MODELS_QUERY)
				: new FlexibleSearchQuery(CatCoreConstants.POPULATE_SALES_MODELS_USED_QUERY);

		query.addQueryParameter("Category", categoryCode);

		final SearchResult<ProductModel> searchResult = flexibleSearchService.search(query);

		if (null != searchResult.getResult() && !searchResult.getResult().isEmpty())
		{
			return searchResult.getResult();
		}
		else
		{
			return new ArrayList<ProductModel>();
		}
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<RestockAlertModel> getLowStockProducts(final B2BUnitModel b2bUnit, final String category,
			final String salesModelId)
	{
		FlexibleSearchQuery query = null;
		if (StringUtils.isEmpty(category) && StringUtils.isEmpty(salesModelId))
		{
			query = new FlexibleSearchQuery(CatCoreConstants.LOW_STOCK_QUERY);
			query.addQueryParameter(DEALER_PARAM, b2bUnit);
			query.addQueryParameter("isReorderable", Boolean.FALSE);
		}
		else if (!StringUtils.isEmpty(salesModelId))
		{
			query = new FlexibleSearchQuery(CatCoreConstants.LOW_STOCK_SALESMODEL_FILTER_QUERY);
			query.addQueryParameter(DEALER_PARAM, b2bUnit);
			query.addQueryParameter("product", productService.getProductForCode(salesModelId));
		}
		else if (!StringUtils.isEmpty(category))
		{
			final CategoryModel categoryModel = categoryService.getCategoryForCode(category);
			if (categoryModel != null)
			{
				query = new FlexibleSearchQuery(CatCoreConstants.LOW_STOCK_CATEGORY_FILTER_QUERY);
				query.addQueryParameter(DEALER_PARAM, b2bUnit);
				query.addQueryParameter("category", categoryModel);
				query.addQueryParameter("isReorderable", Boolean.FALSE);
			}
		}
		final SearchResult<RestockAlertModel> searchResult = flexibleSearchService.search(query);

		if (null != searchResult.getResult() && !searchResult.getResult().isEmpty())
		{
			return searchResult.getResult();
		}
		else
		{
			return new ArrayList<RestockAlertModel>();
		}


	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AlertProductModel> getAssociatedConfigProducts(final String alertId)
	{

		/*
		 * RestockAlertModel restockAlertModel = new RestockAlertModel(); restockAlertModel.setAlertId(alertId);
		 * restockAlertModel = categoryService.getCategoriesForCode(category);
		 */
		List<AlertProductModel> alertProductSearchResult = new ArrayList<AlertProductModel>();
		RestockAlertModel restockAlertModel = null;
		final FlexibleSearchQuery restockAlertQuery = new FlexibleSearchQuery(CatCoreConstants.GET_RESTOCK_ALERT);
		restockAlertQuery.addQueryParameter("alertId", alertId);
		final SearchResult<RestockAlertModel> alertSearchResult = flexibleSearchService.search(restockAlertQuery);
		if (null != alertSearchResult.getResult() && !alertSearchResult.getResult().isEmpty())
		{
			restockAlertModel = alertSearchResult.getResult().get(0);
			final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.GET_CONFIG_VARIANTS_QUERY);
			query.addQueryParameter("alertId", restockAlertModel);

			final SearchResult<AlertProductModel> searchResult = flexibleSearchService.search(query);

			if (null != searchResult.getResult() && !searchResult.getResult().isEmpty())
			{
				alertProductSearchResult = searchResult.getResult();
			}
		}
		return alertProductSearchResult;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ProductModel getProductByName(final String productName)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.PRODUCT_FROM_NAME_QUERY);

		query.addQueryParameter("name", productName.toUpperCase());

		final SearchResult<ProductModel> searchResult = flexibleSearchService.search(query);

		if (null != searchResult.getResult() && !searchResult.getResult().isEmpty())
		{
			return searchResult.getResult().get(0);
		}
		else
		{
			return new ProductModel();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CategoryModel getCategoryByName(final String categoryName)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.CATEGORY_FROM_NAME_QUERY);

		query.addQueryParameter("name", categoryName.toUpperCase());

		final SearchResult<CategoryModel> searchResult = flexibleSearchService.search(query);

		if (null != searchResult.getResult() && !searchResult.getResult().isEmpty())
		{
			return searchResult.getResult().get(0);
		}
		else
		{
			return new CategoryModel();
		}
	}
}
