/**
 *
 */
package com.cat.bcp.core.dynamic.attribute.handlers;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.model.attribute.AbstractDynamicAttributeHandler;

import org.apache.commons.lang.StringUtils;


/**
 * @author manjam
 *
 */
public class CatAddressLine3Attribute extends AbstractDynamicAttributeHandler<String, AddressModel>
{
	@Override
	public String get(final AddressModel addressModel)
	{
		if (addressModel == null)
		{
			throw new IllegalArgumentException("address model is required");
		}
		else
		{
			if (StringUtils.isNotBlank(addressModel.getAppartment()) && StringUtils.isNotBlank(addressModel.getBuilding()))
			{
				return addressModel.getAppartment() + " " + addressModel.getBuilding();
			}
			else if (StringUtils.isNotBlank(addressModel.getAppartment()))
			{
				return addressModel.getAppartment();
			}
			else if (StringUtils.isNotBlank(addressModel.getBuilding()))
			{
				return addressModel.getBuilding();
			}
			return StringUtils.EMPTY;
		}
	}

	@Override
	public void set(final AddressModel addressModel, final String value)
	{
		if (addressModel != null && StringUtils.isNotBlank(value))
		{

			final String[] data = value.split("\\s+");

			if (data.length > 0 || StringUtils.isNotBlank(data[0]))
			{
				addressModel.setAppartment(data[0]);
			}
			if (data.length > 1 && StringUtils.isNotBlank(data[1]))
			{
				addressModel.setBuilding(data[1]);
			}

		}

	}
}

