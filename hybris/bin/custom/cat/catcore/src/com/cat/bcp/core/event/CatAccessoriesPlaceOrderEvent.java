/**
 *
 */
package com.cat.bcp.core.event;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.event.ClusterAwareEvent;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;


/**
 * @author asomjal
 *
 */
public class CatAccessoriesPlaceOrderEvent extends AbstractEvent implements ClusterAwareEvent
{
	private final OrderModel orderModel;

	public CatAccessoriesPlaceOrderEvent(final OrderModel orderModel)
	{
		super();
		this.orderModel = orderModel;
	}

	public OrderModel getOrder()
	{
		return orderModel;
	}

	@Override
	public boolean publish(final int sourceNodeId, final int targetNodeId)
	{
		return (sourceNodeId == targetNodeId);
	}
}
