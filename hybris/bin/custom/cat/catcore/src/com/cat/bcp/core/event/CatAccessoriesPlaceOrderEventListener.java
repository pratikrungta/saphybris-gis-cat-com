/**
 *
 */
package com.cat.bcp.core.event;

import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.model.CatPlaceOrderProcessModel;


/**
 * @author asomjal
 *
 */
public class CatAccessoriesPlaceOrderEventListener extends AbstractEventListener<CatAccessoriesPlaceOrderEvent>
{

	private static final Logger LOG = Logger.getLogger(CatAccessoriesPlaceOrderEventListener.class);

	private static final String PROCESS_CODE_ACCESSORIES = "catPlaceOrderForAccessoriesProcess";

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "businessProcessService")
	private BusinessProcessService businessProcessService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onEvent(final CatAccessoriesPlaceOrderEvent event)
	{
		LOG.debug("*********Inside CatAccessoriesPlaceOrderEventListener event Listener***********");
		final CatPlaceOrderProcessModel businessProcessModel = (CatPlaceOrderProcessModel) businessProcessService
				.createProcess(PROCESS_CODE_ACCESSORIES + CatCoreConstants.UNDERSCORE + event.getOrder().getCode()
						+ CatCoreConstants.UNDERSCORE + System.currentTimeMillis(), PROCESS_CODE_ACCESSORIES);
		businessProcessModel.setOrder(event.getOrder());
		modelService.save(businessProcessModel);
		businessProcessService.startProcess(businessProcessModel);
	}
}
