package com.cat.bcp.core.event;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import java.util.List;


public class CatOrderAcknowledgementEmailEvent extends AbstractEvent
{

	private String groupId;

	private List<OrderModel> orderModelList;


	public CatOrderAcknowledgementEmailEvent(final String groupId, final List<OrderModel> orderModelList)
	{
		super();
		this.groupId = groupId;
		this.orderModelList = orderModelList;
	}


	/**
	 * @return the groupId
	 */
	public String getGroupId()
	{
		return groupId;
	}


	/**
	 * @param groupId
	 *           the groupId to set
	 */
	public void setGroupId(final String groupId)
	{
		this.groupId = groupId;
	}


	/**
	 * @return the orderModelList
	 */
	public List<OrderModel> getOrderModelList()
	{
		return orderModelList;
	}


	/**
	 * @param orderModelList
	 *           the orderModelList to set
	 */
	public void setOrderModelList(final List<OrderModel> orderModelList)
	{
		this.orderModelList = orderModelList;
	}







}
