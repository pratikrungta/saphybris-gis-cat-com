/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.core.event;

import de.hybris.platform.acceleratorservices.email.EmailService;
import de.hybris.platform.acceleratorservices.model.email.EmailAttachmentModel;
import de.hybris.platform.b2b.model.B2BCommentModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.keygenerator.impl.PersistentKeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.CatOrderAcknowledgementProcessModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;


/**
 * Listener for order confirmation events.
 */
public class CatOrderAcknowledgementEmailEventListener extends AbstractEventListener<CatOrderAcknowledgementEmailEvent>
{
	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "businessProcessService")
	private BusinessProcessService businessProcessService;

	@Resource(name = "baseSiteService")
	BaseSiteService baseSiteService;

	@Resource(name = "emailService")
	private EmailService emailService;

	@Resource(name = "productConverter")
	private Converter<ProductModel, ProductData> productConverter;

	private static final Logger LOG = Logger.getLogger(CatOrderAcknowledgementEmailEventListener.class);
	public static final String CAT_ORDER_ACKNOWLEDGEMENT_EMAIL_PROCESS = "catOrderAcknowledgementEmailProcess";

	private PersistentKeyGenerator emailAttachmentUidGenerator;


	/**
	 * @return the emailAttachmentUidGenerator
	 */
	public PersistentKeyGenerator getEmailAttachmentUidGenerator()
	{
		return emailAttachmentUidGenerator;
	}

	/**
	 * @param emailAttachmentUidGenerator
	 *           the emailAttachmentUidGenerator to set
	 */
	public void setEmailAttachmentUidGenerator(final PersistentKeyGenerator emailAttachmentUidGenerator)
	{
		this.emailAttachmentUidGenerator = emailAttachmentUidGenerator;
	}



	@Override
	protected void onEvent(final CatOrderAcknowledgementEmailEvent orderAcknowledgementEmailEvent)
	{
		LOG.info(" ######## Inside Cat Order Acknowledgement Email Event Listener ##########  ");

		final CatOrderAcknowledgementProcessModel businessProcessModel = (CatOrderAcknowledgementProcessModel) businessProcessService
				.createProcess(CAT_ORDER_ACKNOWLEDGEMENT_EMAIL_PROCESS + CatCoreConstants.UNDERSCORE + System.currentTimeMillis(),
						CAT_ORDER_ACKNOWLEDGEMENT_EMAIL_PROCESS);
		final BaseSiteModel baseSiteModel = baseSiteService.getBaseSiteForUID(CatCoreConstants.SITE_UID);
		businessProcessModel.setSite(baseSiteModel);
		businessProcessModel.setLanguage(baseSiteModel.getDefaultLanguage());
		businessProcessModel.setStore(baseSiteModel.getStores().get(0));
		businessProcessModel.setGroupId(getGroupId(orderAcknowledgementEmailEvent));
		businessProcessModel.setOrderList(orderAcknowledgementEmailEvent.getOrderModelList().stream().collect(Collectors.toList()));
		final List<EmailAttachmentModel> attachments = new ArrayList<>();
		try
		{
			final String dealerId = getDealerId(orderAcknowledgementEmailEvent);
			final String groupId = getGroupId(orderAcknowledgementEmailEvent);
			final InputStream inputStream = createExcelAttachment(orderAcknowledgementEmailEvent.getOrderModelList());
			final DataInputStream masterDataStream = new DataInputStream(inputStream);
			final StringBuilder filename = new StringBuilder(dealerId).append(CatCoreConstants.UNDERSCORE).append(groupId)
					.append(CatCoreConstants.PERIOD).append(CatCoreConstants.EXCEL);
			final String mimeType = CatCoreConstants.EXCEL;
			final EmailAttachmentModel attachment = emailService.createEmailAttachment(masterDataStream, filename.toString(),
					mimeType);
			attachments.add(attachment);
			inputStream.close();
			masterDataStream.close();
		}
		catch (final Exception ex)
		{
			LOG.error("Error has occured while creating order attachment", ex);
		}
		if (CollectionUtils.isNotEmpty(attachments))
		{
			businessProcessModel.setAttachments(attachments);

		}
		modelService.save(businessProcessModel);
		businessProcessService.startProcess(businessProcessModel);
		LOG.info("Starting CatOrderAcknowledgementEmailProcessModel: " + businessProcessModel.getCode());

	}

	/**
	 * @param orderAcknowledgementEmailEvent
	 * @return groupId
	 */
	private String getGroupId(final CatOrderAcknowledgementEmailEvent orderAcknowledgementEmailEvent)
	{
		return orderAcknowledgementEmailEvent.getGroupId();
	}

	/**
	 * @param orderAcknowledgementEmailEvent
	 * @return dealerId
	 */
	private String getDealerId(final CatOrderAcknowledgementEmailEvent orderAcknowledgementEmailEvent)
	{
		final B2BUnitModel unit = orderAcknowledgementEmailEvent.getOrderModelList().get(0).getUnit();
		if (unit != null)
		{
			return unit.getUid();
		}
		return StringUtils.EMPTY;
	}

	/**
	 * This method returns inputstream to the excel attachment file for acknowledgement email
	 *
	 * @param orderList
	 *           The list of splitted order models for a parent order
	 * @return InputStream for the generated excel attachment
	 */
	private InputStream createExcelAttachment(final List<OrderModel> orderList)
	{
		XSSFWorkbook workbook = null;
		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try
		{
			workbook = new XSSFWorkbook(new FileInputStream(
					getClass().getClassLoader().getResource(CatCoreConstants.ACKNOWLEDGEMENT_EMAIL_ATTACHMENT_PATH).getFile()));
			final XSSFSheet sheet = workbook.getSheetAt(0);
			final XSSFCellStyle style = workbook.createCellStyle();
			style.setBorderBottom(BorderStyle.THIN);
			style.setBorderLeft(BorderStyle.THIN);
			style.setBorderRight(BorderStyle.THIN);
			style.setBorderTop(BorderStyle.THIN);
			style.setWrapText(true);
			style.setAlignment(HorizontalAlignment.CENTER);
			int startRow = 3;
			final int startColumn = 0;
			for (final OrderModel orderModel : orderList)
			{
				final ProductData pd = productConverter.convert(orderModel.getEntries().get(0).getProduct());
				final B2BUnitModel unit = orderModel.getUnit();
				final Collection<B2BCommentModel> b2bComments = orderModel.getB2bcomments();
				final AddressModel deliveryAddress = orderModel.getDeliveryAddress();
				final XSSFRow row = sheet.createRow(startRow + 1);
				row.createCell(startColumn).setCellValue(orderModel.getPurchaseOrderNumber());
				row.getCell(startColumn).setCellStyle(style);
				row.createCell(startColumn + 1).setCellValue(orderModel.getMso());
				row.getCell(startColumn + 1).setCellStyle(style);
				row.createCell(startColumn + 2);
				row.getCell(startColumn + 2).setCellValue(CatCoreConstants.PRODUCT_CODE_EXCEL);
				row.getCell(startColumn + 2).setCellStyle(style);
				row.createCell(startColumn + 4);
				if (pd != null)
				{
					row.getCell(startColumn + 4).setCellValue(pd.getBaseProduct());
				}
				row.getCell(startColumn + 4).setCellStyle(style);
				String salesModelName = "";
				final BaseVariantProductModel baseVariantProductModel = (BaseVariantProductModel) ((ConfigVariantProductModel) orderModel
						.getEntries().get(0).getProduct()).getBaseProduct();
				salesModelName = getSalesModelName(salesModelName, baseVariantProductModel);
				row.createCell(startColumn + 3).setCellValue(salesModelName);
				row.getCell(startColumn + 3).setCellStyle(style);

				row.createCell(startColumn + 5);
				if (CollectionUtils.isNotEmpty(b2bComments))
				{
					row.getCell(startColumn + 5).setCellValue(((B2BCommentModel) b2bComments.toArray()[0]).getComment());
				}
				row.getCell(startColumn + 5).setCellStyle(style);

				row.createCell(startColumn + 6);

				if (unit != null)
				{
					row.getCell(startColumn + 6).setCellValue(unit.getUid());
				}
				row.getCell(startColumn + 6).setCellStyle(style);
				row.createCell(startColumn + 7);
				if (deliveryAddress != null)
				{
					row.getCell(startColumn + 7).setCellValue(deliveryAddress.getSapCustomerID());
				}
				row.getCell(startColumn + 7).setCellStyle(style);
				row.createCell(startColumn + 8).setCellValue(orderModel.getCreationtime());
				final XSSFCellStyle dateCellStyle = workbook.createCellStyle();
				final CreationHelper createHelper = workbook.getCreationHelper();
				dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat(CatCoreConstants.DATE_FORMAT_EXCEL));
				dateCellStyle.setBorderBottom(BorderStyle.THIN);
				dateCellStyle.setBorderLeft(BorderStyle.THIN);
				dateCellStyle.setBorderRight(BorderStyle.THIN);
				dateCellStyle.setBorderTop(BorderStyle.THIN);
				dateCellStyle.setWrapText(true);
				dateCellStyle.setAlignment(HorizontalAlignment.CENTER);
				row.getCell(startColumn + 8).setCellStyle(dateCellStyle);
				++startRow;
			}
			workbook.write(byteArrayOutputStream);
			return new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
		}
		catch (final IOException ioex)
		{
			LOG.error("IOException occurred while creating excel attachment ", ioex);
			return null;
		}
		catch (final Exception ex)
		{
			LOG.error("Exception occured while populating excel with data", ex);
			return null;
		}
		finally
		{
			try
			{
				byteArrayOutputStream.close();
				if (workbook != null)
				{
					workbook.close();
				}
				LOG.debug("workbook closed");
			}
			catch (final IOException ex)
			{
				LOG.error("workbook not closed", ex);
			}
		}
	}

	/**
	 * @param salesModelName
	 *           the sales model name to be assigned
	 * @param baseVariantProductModel
	 *           basevariantmodel
	 * @return name of sales model
	 */
	private String getSalesModelName(String salesModelName, final BaseVariantProductModel baseVariantProductModel)
	{
		if (baseVariantProductModel != null)
		{
			final ProductModel salesModel = baseVariantProductModel.getBaseProduct();
			if (salesModel != null)
			{
				salesModelName = salesModel.getName();
			}
		}
		return salesModelName;
	}
}
