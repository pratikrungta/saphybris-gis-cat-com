/**
 *
 */
package com.cat.bcp.core.event;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;


/**
 * @author asomjal
 *
 */
public class CatOrderConfirmationEmailEvent extends AbstractEvent
{
	private OrderModel orderModel;

	private String frontEndTemplateName;

	private final boolean removeParentOrder;


	/**
	 * @param orderModel
	 * @param frontEndTemplateName
	 * @param removeParentOrder
	 */
	public CatOrderConfirmationEmailEvent(final OrderModel orderModel, final String frontEndTemplateName,
			final boolean removeParentOrder)
	{
		super();
		this.orderModel = orderModel;
		this.frontEndTemplateName = frontEndTemplateName;
		this.removeParentOrder = removeParentOrder;
	}

	/**
	 * @return the frontEndTemplateName
	 */
	public String getFrontEndTemplateName()
	{
		return frontEndTemplateName;
	}

	/**
	 * @param frontEndTemplateName
	 *           the frontEndTemplateName to set
	 */
	public void setFrontEndTemplateName(final String frontEndTemplateName)
	{
		this.frontEndTemplateName = frontEndTemplateName;
	}

	/**
	 * @return the orderModel
	 */
	public OrderModel getOrderModel()
	{
		return orderModel;
	}

	/**
	 * @param orderModel
	 *           the orderModel to set
	 */
	public void setOrderModel(final OrderModel orderModel)
	{
		this.orderModel = orderModel;
	}

	/**
	 * @return the removeParentOrder
	 */
	public boolean isRemoveParentOrder()
	{
		return removeParentOrder;
	}

}
