/**
 *
 */
package com.cat.bcp.core.event;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.model.CatOrderConfirmationProcessModel;


/**
 * @author asomjal
 *
 */
public class CatOrderConfirmationEmailEventListner extends AbstractEventListener<CatOrderConfirmationEmailEvent>
{
	private static final Logger LOG = Logger.getLogger(CatOrderConfirmationEmailEventListner.class);

	private static final String CONFIRMATION_EMAIL_PROCESS = "catOrderConfirmationEmailProcess";

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "businessProcessService")
	private BusinessProcessService businessProcessService;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onEvent(final CatOrderConfirmationEmailEvent event)
	{
		LOG.info("*********Inside CatOrderConfirmationEmailEventListner event Listener***********");
		final OrderModel parentOrder = event.getOrderModel();
		final CatOrderConfirmationProcessModel businessProcessModel = (CatOrderConfirmationProcessModel) businessProcessService
				.createProcess(CONFIRMATION_EMAIL_PROCESS + CatCoreConstants.UNDERSCORE + parentOrder.getCode()
						+ CatCoreConstants.UNDERSCORE + System.currentTimeMillis(), CONFIRMATION_EMAIL_PROCESS);
		businessProcessModel.setFrontEndTemplateName(event.getFrontEndTemplateName());
		businessProcessModel.setOrder(parentOrder);
		businessProcessModel.setSite(parentOrder.getSite());
		businessProcessModel.setStore(parentOrder.getStore());
		businessProcessModel.setLanguage(commonI18NService.getCurrentLanguage());
		businessProcessModel.setCustomer((CustomerModel) parentOrder.getUser());
		businessProcessModel.setUser(parentOrder.getUser());
		businessProcessModel.setCurrency(parentOrder.getCurrency());
		businessProcessModel.setParentOrderCode(parentOrder.getCode());
		businessProcessModel.setRemoveParentOrder(Boolean.valueOf(event.isRemoveParentOrder()));

		LOG.info("Boolean.valueOf(event.isRemoveParentOrder())  - " + Boolean.valueOf(event.isRemoveParentOrder()));
		LOG.info("event.getFrontEndTemplateName()  - " + event.getFrontEndTemplateName());

		modelService.save(businessProcessModel);
		if (event.isRemoveParentOrder())
		{
			parentOrder.setStatus(OrderStatus.PARENT_ORDER);
			modelService.save(parentOrder);
		}
		businessProcessService.startProcess(businessProcessModel);
	}
}
