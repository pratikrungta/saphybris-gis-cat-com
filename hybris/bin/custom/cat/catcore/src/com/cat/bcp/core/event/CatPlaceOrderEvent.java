/**
 * 
 */
package com.cat.bcp.core.event;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import java.util.List;


/**
 * @author sparkibanda
 * 
 */
public class CatPlaceOrderEvent extends AbstractEvent
{

	private final OrderModel orderModel;
	private final List<String> poList;

	public CatPlaceOrderEvent(final OrderModel orderModel, final List<String> poList)
	{
		super();
		this.orderModel = orderModel;
		this.poList = poList;
	}

	public OrderModel getOrder()
	{
		return orderModel;
	}

	public List<String> getPOList()
	{
		return poList;
	}
}
