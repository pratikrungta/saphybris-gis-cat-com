/**
 *
 */
package com.cat.bcp.core.event;

import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.model.ModelService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.cat.bcp.core.model.CatPlaceBCPReOrderProcessModel;
import com.cat.bcp.core.model.CatPlaceOrderProcessModel;


/**
 * @author sparkibanda
 *
 */
public class CatPlaceOrderEventListener extends AbstractEventListener<CatPlaceOrderEvent>
{



	private static final String PROCESS_CODE = "catPlaceOrderProcess";
	private static final String PROCESS_BCP_CODE = "catPlaceBCPReOrderProcess";
	private static final String UNDERSCORE = "_";
	private static final Logger LOG = Logger.getLogger(CatPlaceOrderEventListener.class);

	private ModelService modelService;
	private BusinessProcessService businessProcessService;

	/**
	 * @return the businessProcessService
	 */
	protected BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * @param businessProcessService
	 *           the businessProcessService to set
	 */
	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	/**
	 * @return the modelService
	 */
	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onEvent(final CatPlaceOrderEvent event)
	{
		LOG.info(" Entered Split Order Event Listener ");
		if (CollectionUtils.isNotEmpty(event.getPOList()))
		{
			LOG.info(" Entered Split Order Event Listener for UTV ");
			final CatPlaceOrderProcessModel businessProcessModel = (CatPlaceOrderProcessModel) getBusinessProcessService()
					.createProcess(PROCESS_CODE + UNDERSCORE + event.getOrder().getCode() + UNDERSCORE + System.currentTimeMillis(),
							PROCESS_CODE);
			businessProcessModel.setOrder(event.getOrder());
			businessProcessModel.setPOList(event.getPOList());
			getModelService().save(businessProcessModel);
			getBusinessProcessService().startProcess(businessProcessModel);
			LOG.info("Starting CatPlaceOrderProcessModel for CUV order: " + businessProcessModel.getCode());
		}
		else
		{
			LOG.info(" Entered Split Order Event Listener for BCP ");
			final CatPlaceBCPReOrderProcessModel businessProcessModel = (CatPlaceBCPReOrderProcessModel) getBusinessProcessService()
					.createProcess(
							PROCESS_BCP_CODE + UNDERSCORE + event.getOrder().getCode() + UNDERSCORE + System.currentTimeMillis(),
							PROCESS_BCP_CODE);
			businessProcessModel.setOrder(event.getOrder());
			getModelService().save(businessProcessModel);
			getBusinessProcessService().startProcess(businessProcessModel);
		}

	}




}
