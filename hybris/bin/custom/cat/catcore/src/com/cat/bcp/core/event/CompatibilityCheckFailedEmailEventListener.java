/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.core.event;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.model.CatCompatibilityCheckFailedEmailProcessModel;
import com.cat.bcp.core.model.CatPlaceOrderProcessModel;
import de.hybris.platform.acceleratorservices.site.AbstractAcceleratorSiteEventListener;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.commerceservices.enums.SiteChannel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.orderprocessing.events.OrderPlacedEvent;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.site.BaseSiteService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import javax.annotation.Resource;
import java.util.List;

import static com.cat.bcp.core.constants.CatCoreConstants.SITE_UID;


/**
 * Listener for order confirmation events.
 */
public class CompatibilityCheckFailedEmailEventListener extends AbstractEventListener<CompatibilityCheckFailedEvent>
{
	private static final Logger LOG = Logger.getLogger(CatOrderConfirmationEmailEventListner.class);
	public static final String CAT_COMPATIBILITY_CHECK_FAILED_EMAIL_PROCESS = "CatCompatibilityCheckFailedEmailProcess";

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "businessProcessService")
	private BusinessProcessService businessProcessService;


    @Resource(name="baseSiteService")
    BaseSiteService baseSiteService;

	@Override
	protected void onEvent(CompatibilityCheckFailedEvent compatibilityCheckFailedEvent) {
		LOG.info(" Entered Compatibility check failed  Event Listener  ");

		final CatCompatibilityCheckFailedEmailProcessModel businessProcessModel = (CatCompatibilityCheckFailedEmailProcessModel) businessProcessService
				.createProcess(CAT_COMPATIBILITY_CHECK_FAILED_EMAIL_PROCESS +CatCoreConstants.UNDERSCORE  + System.currentTimeMillis(),"catCompatibilityCheckFailedEmailProcess"
						);
		businessProcessModel.setFailedConfigIDList(compatibilityCheckFailedEvent.getFailedConfigIDList());
        BaseSiteModel baseSiteModel=baseSiteService.getBaseSiteForUID(CatCoreConstants.SITE_UID);
		businessProcessModel.setSite(baseSiteModel);
		businessProcessModel.setLanguage(baseSiteModel.getDefaultLanguage());
		businessProcessModel.setStore(baseSiteModel.getStores().get(0));
		modelService.save(businessProcessModel);
		businessProcessService.startProcess(businessProcessModel);
		LOG.info("Starting CatCompatibilityCheckFailedEmailProcessModel: " + businessProcessModel.getCode());

	}
}
