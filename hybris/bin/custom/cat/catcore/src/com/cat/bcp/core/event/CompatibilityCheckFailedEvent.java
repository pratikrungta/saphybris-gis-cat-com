package com.cat.bcp.core.event;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

import java.util.List;

public class CompatibilityCheckFailedEvent extends AbstractEvent {

    private  List<ProductModel>  failedConfigIDList;
    public CompatibilityCheckFailedEvent(final List<ProductModel> failedConfigIDList )
    {
        super();
        this.failedConfigIDList = failedConfigIDList;
    }

    public List<ProductModel> getFailedConfigIDList() {
        return failedConfigIDList;
    }

    public void setFailedConfigIDList(List<ProductModel> failedConfigIDList) {
        this.failedConfigIDList = failedConfigIDList;
    }


}
