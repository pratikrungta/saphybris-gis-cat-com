/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.core.event;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.product.data.ProductData;

import java.io.Serializable;
import java.util.OptionalInt;
import java.util.stream.IntStream;

import com.cat.bcp.core.enums.ProductConditionEnum;
import com.cat.bcp.core.util.RecentViewedCircularQueue;


/**
 * Object containing queues of recently viewed products *
 */
public class RecentViewedItemsCollection implements Serializable
{

	private final RecentViewedCircularQueue<ProductData> productData;

	private B2BCustomerModel customer;

	private int maxEntries = 0;

	protected ProductConditionEnum productCondition = ProductConditionEnum.NEW;


	private static final int DEFAULT_MAX_ENTRIES = 6;



	public RecentViewedItemsCollection(final int maxEntry)
	{
		if (maxEntry < 1)
		{
			this.maxEntries = DEFAULT_MAX_ENTRIES;
			throw new IllegalArgumentException(
					"Maximum size not configured properly." + " Using default value " + DEFAULT_MAX_ENTRIES);
		}
		this.maxEntries = maxEntry;
		productData = new RecentViewedCircularQueue(maxEntry);
	}



	public synchronized void addProductCode(final ProductData productSampleData)
	{
		final OptionalInt optInt = checkIfProductExistsInCollection(productSampleData, productData);
		if (!optInt.isPresent())
		{
			productData.add(productSampleData);
		}
		else
		{
			productData.remove(optInt.getAsInt());
			productData.add(productSampleData);
		}
	}



	private OptionalInt checkIfProductExistsInCollection(final ProductData productSampleData,
			final RecentViewedCircularQueue<ProductData> productData2)
	{
		return IntStream.range(0, productData2.size())
				.filter(i -> productSampleData.getCode().equals(productData2.get(i).getCode())).findFirst();
	}

	public RecentViewedCircularQueue<ProductData> getProductCodes()
	{
		return productData;
	}



	public B2BCustomerModel getCustomer()
	{
		return customer;
	}


	public void setCustomer(final B2BCustomerModel customer)
	{
		this.customer = customer;
	}

	/**
	 * @return the productConditionEnum
	 */
	public ProductConditionEnum getProductCondition()
	{
		return productCondition;
	}

	/**
	 * Method to set the productConditionEnum
	 *
	 * @param productConditionEnum
	 */
	public void setProductCondition(final ProductConditionEnum productCondition)
	{
		this.productCondition = productCondition;
	}

}
