/**
 *
 */
package com.cat.bcp.core.event;

import de.hybris.platform.servicelayer.event.ClusterAwareEvent;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;


/**
 * @author sagdhingra
 *
 */
public class RecentViewedProductEvent extends AbstractEvent implements ClusterAwareEvent
{

	private final RecentViewedItemsCollection collection;

	public RecentViewedProductEvent(final RecentViewedItemsCollection collection)
	{
		super();
		this.collection = collection;
	}



	/**
	 * @return the collection
	 */
	public RecentViewedItemsCollection getCollection()
	{
		return collection;
	}



	@Override
	public boolean publish(final int arg0, final int arg1)
	{
		return arg0 == arg1;
	}

}
