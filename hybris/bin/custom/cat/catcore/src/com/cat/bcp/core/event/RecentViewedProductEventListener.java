/**
 *
 */
package com.cat.bcp.core.event;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.event.impl.AbstractEventListener;
import de.hybris.platform.servicelayer.model.ModelService;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.cat.bcp.core.util.RecentViewedCircularQueue;


/**
 * @author sagdhingra
 *
 */
public class RecentViewedProductEventListener extends AbstractEventListener<RecentViewedProductEvent>
{

	private static final String CATALOG_NAME = "NAProductCatalog";

	private static final String CATALOG_VERSION = "Online";

	private static final Logger LOG = Logger.getLogger(RecentViewedProductEventListener.class);

	private ProductService productService;

	private ModelService modelService;

	private CatalogVersionService catalogVersionService;


	@Override
	protected void onEvent(final RecentViewedProductEvent productEvent)
	{
		final RecentViewedItemsCollection collection = productEvent.getCollection();
		final B2BCustomerModel customer = collection.getCustomer();
		final RecentViewedCircularQueue<ProductModel> productSet = new RecentViewedCircularQueue<>(
				collection.getProductCodes().size());
		catalogVersionService.setSessionCatalogVersion(CATALOG_NAME, CATALOG_VERSION);
		collection.getProductCodes().forEach(product -> productSet.add(productService.getProductForCode(product.getCode())));


		if (null != collection.getProductCondition())
		{

			switch (collection.getProductCondition())
			{

				case NEW:
					customer.setRecentlyViewedProduct(productSet);
					break;

				case USED:
					customer.setRecentlyViewedUsedProduct(productSet);
					break;

				default:
					LOG.info("Product is neither NEW nor USED");
					break;

			}

		}

		modelService.save(customer);
	}


	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Required
	public void setProductService(final ProductService productService)
	{
		this.productService = productService;
	}

	@Required
	public void setCatalogVersionService(final CatalogVersionService catalogVersionService)
	{
		this.catalogVersionService = catalogVersionService;
	}
}
