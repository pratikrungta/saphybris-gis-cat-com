/**
 *
 */
package com.cat.bcp.core.exception;

/**
 * This exception is used in Cat Core classes.
 *
 * @author bidavda
 *
 */
public class CatCoreException extends Exception
{
    public CatCoreException(final String message)
    {
        super(message);
    }

    public CatCoreException(final Throwable cause)
    {
        super(cause);
    }

    public CatCoreException(final String message, final Throwable cause)
    {
        super(message, cause);
    }
}
