/**
 *
 */
package com.cat.bcp.core.exception;

/**
 * This Exceptional class is to catch if EDH records are not found
 *
 * @author sjeedula
 *
 */
public class CatEDHRecordNotFoundException extends CatException //NOSONAR
{
	public CatEDHRecordNotFoundException(final String message)
	{
		super(message);
	}

	public CatEDHRecordNotFoundException(final Throwable cause)
	{
		super(cause);
	}

	public CatEDHRecordNotFoundException(final String message, final Throwable cause)
	{
		super(message, cause);
	}
}
