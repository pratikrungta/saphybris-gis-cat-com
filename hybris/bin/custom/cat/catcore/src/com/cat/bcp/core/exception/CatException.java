/**
 *
 */
package com.cat.bcp.core.exception;

/**
 * This is generic CatException class.
 *
 * @author ravjonnalagadda
 *
 */
public class CatException extends Exception // NOSONAR
{
	public CatException(final String message)
	{
		super(message);
	}

	public CatException(final Throwable cause)
	{
		super(cause);
	}

	public CatException(final String message, final Throwable cause)
	{
		super(message, cause);
	}
}
