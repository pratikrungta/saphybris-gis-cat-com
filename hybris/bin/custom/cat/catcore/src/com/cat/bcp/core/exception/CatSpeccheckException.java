/**
 *
 */
package com.cat.bcp.core.exception;

/**
 *
 * This exception is used in Speccheck integration classes.
 *
 * @author asomjal
 *
 */
public class CatSpeccheckException extends Exception
{
	public CatSpeccheckException(final String message)
	{
		super(message);
	}

	public CatSpeccheckException(final Throwable cause)
	{
		super(cause);
	}

	public CatSpeccheckException(final String message, final Throwable cause)
	{
		super(message, cause);
	}
}
