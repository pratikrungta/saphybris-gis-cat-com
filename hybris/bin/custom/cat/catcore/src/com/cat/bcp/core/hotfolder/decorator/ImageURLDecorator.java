/**
 *
 */
package com.cat.bcp.core.hotfolder.decorator;

import de.hybris.platform.impex.jalo.header.AbstractImpExCSVCellDecorator;
import de.hybris.platform.util.Config;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.constants.CatCoreConstants;


/**
 * @author vkumarpendyam
 *
 */
public class ImageURLDecorator extends AbstractImpExCSVCellDecorator
{
	private static final Logger LOG = Logger.getLogger(ImageURLDecorator.class);
	private static final String imageSize = Config.getParameter(CatCoreConstants.URLFORMAT);

	@Override
	public String decorate(final int position, final Map<Integer, String> srcLine)
	{
		String imageURL = srcLine.get(Integer.valueOf(position));
		if (StringUtils.isNotBlank(imageURL) && !StringUtils.contains(imageURL, imageSize))
		{
			imageURL = imageURL + imageSize;
		}
		LOG.debug("ImageURL" + imageURL);
		return imageURL;
	}
}
