package com.cat.bcp.core.integration.beans;

import com.cat.bcp.core.integration.beans.CatOrderRecord;

public class CatControlRecord {


    private CatOrderRecord catOrderRecord;

    private String recordType;

    private String sequenceNbr;
    private String transMissiondate;

    private String miscIndicator;


    private  String shipDealerCode;

    private String salesModel;


    private String orderCopyQuanity;


    private String orderPriortiyType;

    private String orderProductType;

    private String dsuoCustomerCare;

    private String cancelRequest;

    private String ackRefreshIndicator;


    private String dbsReservedArea;

    private String dupLinkIndicator;

    private String orderValidationRequest;

    public CatOrderRecord getCatOrderRecord() {

        return catOrderRecord;
    }

    public void setCatOrderRecord(CatOrderRecord catOrderRecord) {
        this.catOrderRecord = catOrderRecord;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getSequenceNbr() {
        return sequenceNbr;
    }

    public void setSequenceNbr(String sequenceNbr) {
        this.sequenceNbr = sequenceNbr;
    }

    public String getTransMissiondate() {
        return transMissiondate;
    }

    public void setTransMissiondate(String transMissiondate) {
        this.transMissiondate = transMissiondate;
    }

    public String getMiscIndicator() {
        return miscIndicator;
    }

    public void setMiscIndicator(String miscIndicator) {
        this.miscIndicator = miscIndicator;
    }

    public String getShipDealerCode() {
        return shipDealerCode;
    }

    public void setShipDealerCode(String shipDealerCode) {
        this.shipDealerCode = shipDealerCode;
    }

    public String getSalesModel() {
        return salesModel;
    }

    public void setSalesModel(String salesModel) {
        this.salesModel = salesModel;
    }

    public String getOrderCopyQuanity() {
        return orderCopyQuanity;
    }

    public void setOrderCopyQuanity(String orderCopyQuanity) {
        this.orderCopyQuanity = orderCopyQuanity;
    }

    public String getOrderPriortiyType() {
        return orderPriortiyType;
    }

    public void setOrderPriortiyType(String orderPriortiyType) {
        this.orderPriortiyType = orderPriortiyType;
    }

    public String getOrderProductType() {
        return orderProductType;
    }

    public void setOrderProductType(String orderProductType) {
        this.orderProductType = orderProductType;
    }

    public String getDsuoCustomerCare() {
        return dsuoCustomerCare;
    }

    public void setDsuoCustomerCare(String dsuoCustomerCare) {
        this.dsuoCustomerCare = dsuoCustomerCare;
    }

    public String getCancelRequest() {
        return cancelRequest;
    }

    public void setCancelRequest(String cancelRequest) {
        this.cancelRequest = cancelRequest;
    }

    public String getAckRefreshIndicator() {
        return ackRefreshIndicator;
    }

    public void setAckRefreshIndicator(String ackRefreshIndicator) {
        this.ackRefreshIndicator = ackRefreshIndicator;
    }

    public String getDbsReservedArea() {
        return dbsReservedArea;
    }

    public void setDbsReservedArea(String dbsReservedArea) {
        this.dbsReservedArea = dbsReservedArea;
    }

    public String getDupLinkIndicator() {
        return dupLinkIndicator;
    }

    public void setDupLinkIndicator(String dupLinkIndicator) {
        this.dupLinkIndicator = dupLinkIndicator;
    }

    public String getOrderValidationRequest() {
        return orderValidationRequest;
    }

    public void setOrderValidationRequest(String orderValidationRequest) {
        this.orderValidationRequest = orderValidationRequest;
    }
}
