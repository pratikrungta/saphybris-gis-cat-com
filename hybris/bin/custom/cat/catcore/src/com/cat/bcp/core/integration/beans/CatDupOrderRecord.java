package com.cat.bcp.core.integration.beans;

import java.util.List;

/**
 * The class contains methods which needs to be implemented for the
 * dup orders.
 */
public class CatDupOrderRecord {
    private String recordType;

    private String sequenceNbr;

    private List<String> orderPONumbersList;
    
    /**
     * @return orderPONumbersList in list of strings
     */
     public List<String> getOrderPONumbersList() {
		return orderPONumbersList;
	}
     
     /**
      * sets dup orders to the list
      * @param orderPONumbersList
      */
	public void setOrderPONumbersList(List<String> orderPONumbersList) {
		this.orderPONumbersList = orderPONumbersList;
	}



	/**
	 * returns the record type
     * @return recordType in string
     */
    public String getRecordType() {
        return recordType;
    }

    /**
     * sets record type
     * @param recordType
     */
    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    /**
     * returns the sequence number of the dup record
     * @return sequenceNbr 
     */
    public String getSequenceNbr() {
        return sequenceNbr;
    }

    /**
     * sets sequence number to the order record
     * @param sequenceNbr
     */
    public void setSequenceNbr(String sequenceNbr) {
        this.sequenceNbr = sequenceNbr;
    }
 
}
