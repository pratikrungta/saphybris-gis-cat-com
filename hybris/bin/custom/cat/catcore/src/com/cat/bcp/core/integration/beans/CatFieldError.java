package com.cat.bcp.core.integration.beans;

public class CatFieldError {


    private String message;
    private String code;
    private String objectName;
    private String field;
    private Object rejectedValue;

    /**
     * @return the message
     */
    public String getMessage()
    {
        return message;
    }

    /**
     * @param message
     *           the message to set
     */
    public void setMessage(final String message)
    {
        this.message = message;
    }

    /**
     * @return the code
     */
    public String getCode()
    {
        return code;
    }

    /**
     * @param code
     *           the code to set
     */
    public void setCode(final String code)
    {
        this.code = code;
    }

    /**
     * @return the objectName
     */
    public String getObjectName()
    {
        return objectName;
    }

    /**
     * @param objectName
     *           the objectName to set
     */
    public void setObjectName(final String objectName)
    {
        this.objectName = objectName;
    }

    /**
     * @return the field
     */
    public String getField()
    {
        return field;
    }

    /**
     * @param field
     *           the field to set
     */
    public void setField(final String field)
    {
        this.field = field;
    }

    /**
     * @return the rejectedValue
     */
    public Object getRejectedValue()
    {
        return rejectedValue;
    }

    /**
     * @param rejectedValue
     *           the rejectedValue to set
     */
    public void setRejectedValue(final Object rejectedValue)
    {
        this.rejectedValue = rejectedValue;
    }



}
