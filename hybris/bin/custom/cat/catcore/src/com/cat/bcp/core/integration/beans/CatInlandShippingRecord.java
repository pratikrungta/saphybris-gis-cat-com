package com.cat.bcp.core.integration.beans;

public class CatInlandShippingRecord {


        private String recordType;

        private String sequenceNbr;

        private String freightChargeCode;

        private String transPorationCode;

        private String shipLessThanTruckload;

        private String freightRouting;

        private String orderShipAfterDate;

        private String orderShipBeforeDate;

        public String getRecordType() {
            return recordType;
        }

        public void setRecordType(String recordType) {
            this.recordType = recordType;
        }

        public String getSequenceNbr() {
            return sequenceNbr;
        }

        public void setSequenceNbr(String sequenceNbr) {
            this.sequenceNbr = sequenceNbr;
        }

        public String getFreightChargeCode() {
            return freightChargeCode;
        }

        public void setFreightChargeCode(String freightChargeCode) {
            this.freightChargeCode = freightChargeCode;
        }

        public String getTransPorationCode() {
            return transPorationCode;
        }

        public void setTransPorationCode(String transPorationCode) {
            this.transPorationCode = transPorationCode;
        }

        public String getShipLessThanTruckload() {
            return shipLessThanTruckload;
        }

        public void setShipLessThanTruckload(String shipLessThanTruckload) {
            this.shipLessThanTruckload = shipLessThanTruckload;
        }

        public String getFreightRouting() {
            return freightRouting;
        }

        public void setFreightRouting(String freightRouting) {
            this.freightRouting = freightRouting;
        }

        public String getOrderShipAfterDate() {
            return orderShipAfterDate;
        }

        public void setOrderShipAfterDate(String orderShipAfterDate) {
            this.orderShipAfterDate = orderShipAfterDate;
        }

        public String getOrderShipBeforeDate() {
            return orderShipBeforeDate;
        }

        public void setOrderShipBeforeDate(String orderShipBeforeDate) {
            this.orderShipBeforeDate = orderShipBeforeDate;
        }
}

