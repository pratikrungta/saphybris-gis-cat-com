package com.cat.bcp.core.integration.beans;

public class CatInvoiceCriteriaRecord {


    private String invoiceCriteriaRecordType;

    private String invoiceCriteriaSequenceNumber;

    private String paymentTermsCode;

    private String priceProtectionIndicator;

    private String authDocType;

    private String authDocNumber;

    public String getInvoiceCriteriaRecordType() {
        return invoiceCriteriaRecordType;
    }

    public void setInvoiceCriteriaRecordType(String invoiceCriteriaRecordType) {
        this.invoiceCriteriaRecordType = invoiceCriteriaRecordType;
    }

    public String getInvoiceCriteriaSequenceNumber() {
        return invoiceCriteriaSequenceNumber;
    }

    public void setInvoiceCriteriaSequenceNumber(String invoiceCriteriaSequenceNumber) {
        this.invoiceCriteriaSequenceNumber = invoiceCriteriaSequenceNumber;
    }

    public String getPaymentTermsCode() {
        return paymentTermsCode;
    }

    public void setPaymentTermsCode(String paymentTermsCode) {
        this.paymentTermsCode = paymentTermsCode;
    }

    public String getPriceProtectionIndicator() {
        return priceProtectionIndicator;
    }

    public void setPriceProtectionIndicator(String priceProtectionIndicator) {
        this.priceProtectionIndicator = priceProtectionIndicator;
    }

    public String getAuthDocType() {
        return authDocType;
    }

    public void setAuthDocType(String authDocType) {
        this.authDocType = authDocType;
    }

    public String getAuthDocNumber() {
        return authDocNumber;
    }

    public void setAuthDocNumber(String authDocNumber) {
        this.authDocNumber = authDocNumber;
    }
}
