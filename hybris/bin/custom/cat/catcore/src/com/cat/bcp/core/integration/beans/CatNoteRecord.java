package com.cat.bcp.core.integration.beans;

// NOSONAR
public class CatNoteRecord   {

    private String recordType;

    private String sequenceNbr;

    private CatOrderRecord catOrderRecord;

    private String orderNoteTxt;

 // NOSONAR
    public String getRecordType() {
        return recordType;
    }
 // NOSONAR
    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }
 // NOSONAR
    public String getSequenceNbr() {
        return sequenceNbr;
    }
 // NOSONAR
    public void setSequenceNbr(String sequenceNbr) {
        this.sequenceNbr = sequenceNbr;
    }
 // NOSONAR
    public CatOrderRecord getCatOrderRecord() {
        return catOrderRecord;
    }
 // NOSONAR
    public void setCatOrderRecord(CatOrderRecord catOrderRecord) {
        this.catOrderRecord = catOrderRecord;
    }
 // NOSONAR
    public String getOrderNoteTxt() {
        return orderNoteTxt;
    }
 // NOSONAR
    public void setOrderNoteTxt(String orderNoteTxt) {
        this.orderNoteTxt = orderNoteTxt;
    }



}
