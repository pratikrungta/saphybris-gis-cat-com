package com.cat.bcp.core.integration.beans;

public class CatOrderRecord {



    private String dealerCode;

    private String  purchaseOrdNbr;

    private String orderControlNmbr;


    public String getDealerCode() {
        return dealerCode;
    }

    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    public String getPurchaseOrdNbr() {
        return purchaseOrdNbr;
    }

    public void setPurchaseOrdNbr(String purchaseOrdNbr) {
        this.purchaseOrdNbr = purchaseOrdNbr;
    }

    public String getOrderControlNmbr() {
        return orderControlNmbr;
    }

    public void setOrderControlNmbr(String orderControlNmbr) {
        this.orderControlNmbr = orderControlNmbr;
    }
}
