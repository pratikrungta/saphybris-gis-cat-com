package com.cat.bcp.core.integration.beans;

// NOSONAR
public class CatSpecificationRecord  {

    private String recordType;

    private String sequenceNbr;

    private CatOrderRecord catOrderRecord;

    private String materialNumber;

    private String productDescription;

    private String orderQuantity;

    private String shipLessIndicator;

    private String orderAssemblyCode;

    private String nonStandardItemCode;

    private String actionCode ;

 // NOSONAR
    public String getRecordType() {
        return recordType;
    }
 // NOSONAR
    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }
 // NOSONAR
    public String getSequenceNbr() {
        return sequenceNbr;
    }
 // NOSONAR
    public void setSequenceNbr(String sequenceNbr) {
        this.sequenceNbr = sequenceNbr;
    }
 // NOSONAR
    public CatOrderRecord getCatOrderRecord() {
        return catOrderRecord;
    }
 // NOSONAR
    public void setCatOrderRecord(CatOrderRecord catOrderRecord) {
        this.catOrderRecord = catOrderRecord;
    }
 // NOSONAR
    public String getMaterialNumber() {
        return materialNumber;
    }
 // NOSONAR
    public void setMaterialNumber(String materialNumber) {
        this.materialNumber = materialNumber;
    }
 // NOSONAR
    public String getProductDescription() {
        return productDescription;
    }
 // NOSONAR
    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }
 // NOSONAR
    public String getOrderQuantity() {
        return orderQuantity;
    }
 // NOSONAR
    public void setOrderQuantity(String orderQuantity) {
        this.orderQuantity = orderQuantity;
    }
 // NOSONAR
    public String getShipLessIndicator() {
        return shipLessIndicator;
    }
 // NOSONAR
    public void setShipLessIndicator(String shipLessIndicator) {
        this.shipLessIndicator = shipLessIndicator;
    }
 // NOSONAR
    public String getOrderAssemblyCode() {
        return orderAssemblyCode;
    }
 // NOSONAR
    public void setOrderAssemblyCode(String orderAssemblyCode) {
        this.orderAssemblyCode = orderAssemblyCode;
    }
 // NOSONAR
    public String getNonStandardItemCode() {
        return nonStandardItemCode;
    }
 // NOSONAR
    public void setNonStandardItemCode(String nonStandardItemCode) {
        this.nonStandardItemCode = nonStandardItemCode;
    }
 // NOSONAR
    public String getActionCode() {
        return actionCode;
    }
 // NOSONAR
    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }
}
