package com.cat.bcp.core.integration.beans;

// NOSONAR
public class CatTrailerRecord  {

    private String recordType;

    private String sequenceNbr;

    private CatOrderRecord catOrderRecord;
    private String proPrietaryRecord;

 // NOSONAR
    public String getRecordType() {
        return recordType;
    }
 // NOSONAR
    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }
 // NOSONAR
    public String getSequenceNbr() {
        return sequenceNbr;
    }
 // NOSONAR
    public void setSequenceNbr(String sequenceNbr) {
        this.sequenceNbr = sequenceNbr;
    }
 // NOSONAR
    public CatOrderRecord getCatOrderRecord() {
        return catOrderRecord;
    }
 // NOSONAR
    public void setCatOrderRecord(CatOrderRecord catOrderRecord) {
        this.catOrderRecord = catOrderRecord;
    }
 // NOSONAR
    public String getProPrietaryRecord() {
        return proPrietaryRecord;
    }
 // NOSONAR
    public void setProPrietaryRecord(String proPrietaryRecord) {
        this.proPrietaryRecord = proPrietaryRecord;
    }




}
