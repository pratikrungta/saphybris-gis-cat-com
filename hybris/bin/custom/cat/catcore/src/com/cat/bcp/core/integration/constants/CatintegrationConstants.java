/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.core.integration.constants;

/**
 * Global class for all Catintegration constants. You can add global constants for your extension into this class.
 */
public final class CatintegrationConstants
{
	public static final String EXTENSIONNAME = "catintegration";

	public static final String PLATFORM_LOGO_CODE = "catintegrationPlatformLogo";

	public static final String FAILURE = "Failure";

	public static final String SUCCESS = "Success";

	public static final int PURCHASER_ORDER_NUMBER_LENGTH = 12;

	public static final int DEALER_CODE_LENGTH = 6;

	public static final int SHIP_TO_CODE_LENGTH = 6;

	public static final int SALES_CODE_LENGTH = 10;

	public static final int BASEVARIANT_NAME_LENGTH = 30;

	public static final int BASEVARIANT_CODE_LENGTH = 30;

	public static final int MATERIAL_NUMBER_LENGTH = 10;

	public static final int CONTROL_NUMBER_LENGTH = 7;

	public static final int RECORD_TYPE_LENGTH = 2;

	public static final int SEQUENCE_NUMBER_LENGTH = 3;

	public static final int TRANSMISSION_DATE_LENGTH = 8;

	public static final int INDICATOR_LENGTH = 1;

	public static final int ORDER_COPY_QUANTITY_LENGTH = 3;

	public static final int ORDER_PRIORITY_TYPE_LENGTH = 2;

	public static final int ORDER_PRODUCT_TYPE_LENGTH = 1;

	public static final int DSUO_CUSTOMER_CARE_LENGTH = 6;

	public static final int CANCEL_REQUEST_LENGTH = 1;

	public static final int ACK_REFRESH_INDICATOR_LENGTH = 1;

	public static final int DBS_RESERVED_AREA_LENGTH = 7;

	public static final int DUP_LINK_INDICATOR = 1;

	public static final int ORDER_VALIDATION_REQ_LENGTH = 1;

	public static final int MATERIAL_NUMBER_WITH_LENGTH_SIX = 6;

	public static final int MATERIAL_NUMBER_WITH_LENGTH_SEVEN = 7;

	public static final int MATERIAL_NUMBER_WITH_LENGTH_NINE = 9;

	public static final int MATERIAL_NUMBER_WITH_LENGTH_TEN = 10;

	public static final int SHIPLESS_INDICATOR_LENGTH = 1;

	public static final int ORDER_ASSEMBLY_CODE_LENGTH = 1;

	public static final int NON_STANDARD_ITEM_CODE_LENGTH = 3;

	public static final int ACTION_CODE_LENGTH = 1;

	public static final int ORDER_NOTES_LENGTH = 40;

	public static final int FREIGHT_CHARGE_CODE_LENGTH = 1;

	public static final int TRANSPORT_CODE_LENGTH = 1;

	public static final int SHIPLESS_THAN_TRUCK_LOAD_LENTH = 1;

	public static final int FREIGHT_ROUTING_LENGTH = 25;

	public static final int SHIP_AFTER_DATE_LENGTH = 8;

	public static final int SHIP_BEFORE_DATE_LENGTH = 8;

	public static final int PRO_PRIETARY_LENGTH = 6;

	public static final int ORDER_RECORD_LENGTH = 80;

	public static final String ORDERMACHINEREQUEST = "orderMachinesRequest";

	public static final String ORDERMAHCINERESPONSE = "orderMachinesResponse";

	public static final String PONUMBER = "PO NUMBER ";

	public static final String DEALERCODE = "DEALER CODE ";

	public static final String SHIPTOID = "SHIP TO ID ";

	public static final String SALESCODE = "SALES CODE ";

	public static final String BASEVARIANT = "BASE VARIANT ";

	public static final String BASEVARIANTCODE = "BASE VARIANT CODE ";
	public static final int PAYMENT_TERMS_LENGTH = 5;

	public static final int PRICE_PROTECTION_INDICATOR_LENGTH = 1;

	public static final int AUTH_DOC_TYPE_LENGTH = 1;

	public static final int AUTH_DOC_NUMBER_LENGTH = 8;

	/** Quote Data AWS Integration constants */

	public static final String CRONJOB_EXECUTION_STARTED = "CRONJOB_EXECUTION_STARTED";

	public static final String QUERY_EXECUTION_STARTED = "QUERY_EXECUTION_STARTED";

	public static final String QUERY_EXECUTION_COMPLETED = "QUERY_EXECUTION_COMPLETED";

	public static final String FILE_GENERATION_STARTED = "FILE_GENERATION_STARTED";

	public static final String FILE_GENERATION_ENDED = "FILE_GENERATION_ENDED";

	public static final String FILE_TRANSFER_STARTED = "FILE_TRANSFER_STARTED";

	public static final String FILE_TRANSFER_COMPLETED_WITHSUCCESS = "FILE_TRANSFER_COMPLETED_WITHSUCCESS";

	public static final String FILE_TRANSFER_COMPLETED_WITHFAILURE = "FILE_TRANSFER_COMPLETED_WITHFAILURE";

	public static final String CRONJOB_EXECUTION_COMPLETED = "CRONJOB_EXECUTION_COMPLETED";

	public static final String QUOTE_AWS = "QUOTE_AWS";


	public static final String FEED_AUDIT_STATUS_FAILURE = "Failure";

	public static final String EMPTYSPACE = " ";
	
	public static final String ZERO = "0";

	public static final String ORDERDIRECTOR_ORDERINDICATOR = "P";

	public static final String COMPATIBILITYPASSED = "COMPATIBILITY PASSED";

	public static final String COMPATIBLE = "COMPATIBLE";

	public static final String LOADFAILED = "LOADFAILED";

	public static final String DISCONTINUE = "DISCONTINUE";

	public static final int ORDERDIRECTOR_QUANTITY_LENGTH = 3;

	public static final String FILEFORMAT = "file:";

	public static final String TRYLATER = "TRY LATER";



	private CatintegrationConstants()
	{
		//empty to avoid instantiating this constant class
	}
}
