package com.cat.bcp.core.integration.dao;

import de.hybris.platform.cronjob.jalo.AbortCronJobException;
import de.hybris.platform.cronjob.model.CronJobModel;

import java.io.IOException;
import java.util.Date;

import com.cat.bcp.core.integration.jobs.CatAbstractAbortableCronJob;
import com.cat.core.integration.model.FeedAuditModel;


public interface CatFeedsDao {
    /**
     * generateFeedResultForCsv generates the results required for the feed
     *
     * @param feedName
     * @param feedAuditModel
	 * @param catAbstractAbortableCronJob
	 * @param cronJobModel
     * @throws AbortCronJobException
     * @throws IOException
     */
	void generateFeedResultForCsv(final String feedName, final FeedAuditModel feedAuditModel,
			final CatAbstractAbortableCronJob catAbstractAbortableCronJob, final CronJobModel cronJobModel)
			throws AbortCronJobException, IOException;

    /**
     * getFeedQueryStartTime returns the query start time to pick the records
     *
     * @param feedAuditModel
     * @return Date
     */
     Date getFeedQueryStartTime(final FeedAuditModel feedAuditModel);

    /**
     * Creates the file with the specified file type defined
     *
     * @param feedName
     * @return
     */
     String getFileName(final String feedName);

	/**
	 * returns the records count
	 * 
	 * @param feedAuditModel
	 * @param feedName
	 * @return int
	 */
	int getFeedQueryResultCount(final FeedAuditModel feedAuditModel, String feedName);



	/**
	 *  returns the time when the cronjob was last successful
	 *
	 * @param feedAuditModel the feed audit model
	 * @return Date the last successful run date
	 */
	Date getLastSuccessfulRunTime(final FeedAuditModel feedAuditModel);

}
