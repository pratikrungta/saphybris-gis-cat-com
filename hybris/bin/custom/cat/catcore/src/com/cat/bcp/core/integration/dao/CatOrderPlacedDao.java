package com.cat.bcp.core.integration.dao;

import de.hybris.platform.core.model.order.OrderModel;
import com.cat.bcp.core.model.FBCPartsOrderInfoModel;

import java.util.Date;
import java.util.List;

import com.cat.bcp.core.model.CompatibilityInfoModel;
import de.hybris.platform.core.model.product.ProductModel;


/**
 * Contains methods to get the list of order to be submitted to Order Director
 */
public interface CatOrderPlacedDao
{

    /**
     * The method used to get the list of orders to be submitted to Order Director for the submission
     * @param odQuery
     * 			query to retrieve the OD orders to process
     * @return List<OrderModel>
     *     the list of order  received from the database based on the query
     */
    List<OrderModel> getListOfPlacedOrderToOrderDirector(String odQuery);

    /**
	 * The method used to get the list of FBCPartsOrderInfoModel
	 *
	 * @param fbcPartsOrderInfoQuery
	 *           query to retrieve the Lane2, Lane3 orders
	 * @param orderID
	 *           order code
	 * @return List<FBCPartsOrderInfoModel> the list of FBCPartsOrderInfoModels
	 */
	List<FBCPartsOrderInfoModel> getFBCPartsOrderInfoModelList(final String fbcPartsOrderInfoQuery, String orderID);

	/**
	 * The method used to get the first Order with in the dup order group
	 *
	 * @param poNumber
	 * @param order
	 * @return List<OrderModel> the list of order received from the database based on the query
	 */
     List<OrderModel> getFirstOrderInDupOrders(String poNumber,OrderModel order);

     /**
      * The method used to get marketingCompany
      *
      * @param countryCode
      * @return marketingCompany
      *     marketingCompany recieved from the lookup table which is used for FBC services
      */
     List<String> getMarketingCompany(String countryCode);


	/**
	 * The method used to get the list of CompatibilityInfoModel
	 *
	 * @param compatibilitycheckQuery
	 *           query to retrieve CompatibilityInfoModel
	 * @return List<CompatibilityInfoModel> the list of CompatibilityInfoModels
	 */
	List<CompatibilityInfoModel> getListOfConfigs(String compatibilitycheckQuery);

    /**
     * The method is used to get the list of config id for which the compatibility check has failed.
     * @param compatibilitycheckQuery the query to fetch the compatibility check failed config IDs.
     * @return the list of compatibility check failed config products
     */
    List<ProductModel> getListOfCompatibilityCheckFailedConfigIDs(String compatibilitycheckQuery,Date lastQuerySuccessfulTime);
}

