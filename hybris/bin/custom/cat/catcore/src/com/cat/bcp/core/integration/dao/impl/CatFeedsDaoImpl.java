/**
 *
 */
package com.cat.bcp.core.integration.dao.impl;

import static de.hybris.platform.servicelayer.internal.converter.util.ModelUtils.getFieldValue;

import de.hybris.platform.cronjob.jalo.AbortCronJobException;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.util.Config;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.integration.constants.CatintegrationConstants;
import com.cat.bcp.core.integration.dao.CatFeedsDao;
import com.cat.bcp.core.integration.jobs.CatAbstractAbortableCronJob;
import com.cat.bcp.core.integration.jobs.service.CatCSVFileService;
import com.cat.bcp.core.integration.jobs.service.impl.CatFeedsUtilsService;
import com.cat.core.integration.model.FeedAuditModel;

import au.com.bytecode.opencsv.CSVWriter;


/**
 * @author vkumarpendyam
 *
 */
public class CatFeedsDaoImpl implements CatFeedsDao
{

	private static final String ENDED = " ended";
	private static final String STARTED = " started";
	private static final String OUTBOUND_DATA_FEED = "outboundDataFeed.";
	private static final Logger LOG = Logger.getLogger(CatFeedsDaoImpl.class);
	public static final String EMPTY_STRING = "";
	public static final int QUERYBATCHSIZE = 1000;
	public static final String DELIMITER = "|";
	public static final String FILEEXTENSION = ".csv";
	public static final String DATEFORMAT = "yyMMdd_HHmmss";
	public static final String OUTBOUNGDATAFEED = "outboundDataFeed";
	public static final String FLEXIQUERYRESULTCOUNT = "flexiQueryResultCount";


	@Resource
	private FlexibleSearchService flexibleSearchService;
	@Resource
	private ModelService modelService;
	@Resource
	private CatFeedsUtilsService catFeedsUtilsService;
	@Resource
	private CatCSVFileService catCSVFileService;

	/**
	 * {@inheritDoc}
	 *
	 * @throws IOException
	 */
	@Override
	public void generateFeedResultForCsv(final String feedName, final FeedAuditModel feedAuditModel,
			final CatAbstractAbortableCronJob catAbstractAbortableCronJob, final CronJobModel cronJobModel)
			throws AbortCronJobException, IOException
	{
		LOG.info("DefaultFeedsDaoImpl.generateFeedResultForCsv() for " + feedName + STARTED);

		final String feedQuery = Config.getString(OUTBOUND_DATA_FEED + feedName + ".flexiQuery", EMPTY_STRING);

		final String resulSetDataTypes = Config.getString(OUTBOUND_DATA_FEED + feedName + ".resultSetDataTypes", EMPTY_STRING);

		final List<String> feedDatatypesList = Arrays.asList(resulSetDataTypes.split(","));
		final List<Class> resultTypeList = new ArrayList<>();

		for (final String dataType : feedDatatypesList)
		{
			try
			{
				final Class classType = Class.forName(dataType);
				resultTypeList.add(classType);
			}
			catch (final ClassNotFoundException e)
			{
				LOG.log("exception is in DefaultFeedsDaoImpl.generateFeedResultForCsv(): ", null, false, e);
				throw new IllegalArgumentException(
						"exception is in DefaultFeedsDaoImpl.generateFeedResultForCsv(): " + e.getMessage());
			}
		}

		catFeedsUtilsService.addAuditLogEntry(feedAuditModel, CatintegrationConstants.QUERY_EXECUTION_STARTED);
		catFeedsUtilsService.addAuditLogEntry(feedAuditModel, CatintegrationConstants.FILE_GENERATION_STARTED);

		boolean isQueryExecuted = false;

		isQueryExecuted = executeQuery(feedQuery, resultTypeList, feedName, feedAuditModel, catAbstractAbortableCronJob,
				cronJobModel);

		if (isQueryExecuted)
		{
			catFeedsUtilsService.addAuditLogEntry(feedAuditModel, CatintegrationConstants.QUERY_EXECUTION_COMPLETED);
			catFeedsUtilsService.addAuditLogEntry(feedAuditModel, CatintegrationConstants.FILE_GENERATION_ENDED);
		}
		LOG.info("DefaultFeedsDaoImpl.generateFeedResultForCsv() for " + feedName + ENDED);
	}


	/**
	 * executeQuery executes the flexible query and returns the boolean value of the status
	 *
	 * @param feedQuery
	 * @param resultTypeList
	 * @param feedName
	 * @param feedAuditModel
	 * @param catQuoteDataToAWSJob
	 * @param cronJobModel
	 * @throws AbortCronJobException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	private boolean executeQuery(final String feedQuery, final List<Class> resultTypeList, final String feedName,
			final FeedAuditModel feedAuditModel, final CatAbstractAbortableCronJob catAbstractAbortableCronJob,
			final CronJobModel cronJobModel) throws AbortCronJobException, IOException
	{
		LOG.info("DefaultFeedsDaoImpl.executeQuery() for " + feedName + STARTED);
		CSVWriter csvWriter = null;
		boolean isQueryExecuted = true;
		try
		{
			final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(feedQuery);
			flexibleSearchQuery.setResultClassList(resultTypeList);

			final int range = Config.getInt(OUTBOUND_DATA_FEED + feedName + ".flexiQueryBatchSize", QUERYBATCHSIZE);

			SearchResult<List<?>> searchResult = null;
			int start = 0;
			flexibleSearchQuery.setCount(range);
			flexibleSearchQuery.setNeedTotal(true);
			final String internalHybrisFolderPath = Config.getString(OUTBOUND_DATA_FEED + feedName + ".internalHybrisFolderPath",
					EMPTY_STRING);

			final String delimiter = Config.getString(OUTBOUND_DATA_FEED + feedName + ".csvDelimiter", DELIMITER);
			final Boolean isSingleFile = Config.getBoolean(OUTBOUND_DATA_FEED + feedName + ".issinglefile", true);
			String header = Config.getString(OUTBOUND_DATA_FEED + feedName + ".headers", EMPTY_STRING);
			header = StringUtils.replace(header, ":", " ");
			final String[] headerInStringArray = header.split(",");
			int count = 0;
			do
			{
				count++;
				csvWriter = getCSVWriter(feedName, csvWriter, internalHybrisFolderPath, delimiter, isSingleFile, headerInStringArray,
						count);
				flexibleSearchQuery.setStart(start);
				flexibleSearchQuery.addQueryParameter("qryStartTime", feedAuditModel.getStartTimeParamForFeedQry());
				flexibleSearchQuery.addQueryParameter("queryEndTime", feedAuditModel.getEndTimeParamForFeedQry());
				searchResult = flexibleSearchService.search(flexibleSearchQuery);
				start += range;
				final List<List<?>> rawList = (List<List<?>>) getFieldValue(searchResult, "resultList");
				final List<ArrayList<String>> feedResultsInStrings = new ArrayList<>();
				processFeedResults(rawList, feedResultsInStrings);

				catCSVFileService.generateCsvFile(csvWriter, feedResultsInStrings);
				csvWriter = closeCSVWriter(csvWriter, isSingleFile);

				catAbstractAbortableCronJob.checkIfRequestedToAbortCron(cronJobModel);
			}
			while (start < searchResult.getTotalCount());
		}
		catch (final AbortCronJobException aceX)
		{
			throw aceX;
		}
		catch (final IOException ioe)
		{
			LOG.log("csvWriter instance not created due to IO Exception ", null, false, ioe);
			throw ioe;
		}
		catch (final Exception e)
		{
			LOG.log("Exception is in DefaultFeedsDaoImpl.executeQuery(): ", null, false, e);
			isQueryExecuted = false;
		}
		finally
		{
			try
			{
				if (csvWriter != null)
				{
					csvWriter.close();
				}
			}
			catch (final Exception e)
			{
				LOG.error("Exception is in DefaultFeedsDaoImpl.executeQuery() : " + e);
			}
			csvWriter = null;

		}
		LOG.info("DefaultFeedsDaoImpl.executeQuery() for " + feedName + ENDED);
		return isQueryExecuted;
	}


	/**
	 * Method to close the CSV Writer
	 *
	 * @param csvWriter
	 * @param isSingleFile
	 * @return
	 */
	private static CSVWriter closeCSVWriter(CSVWriter csvWriter, final Boolean isSingleFile)
	{
		if (!isSingleFile)
		{
			try
			{
				if (csvWriter != null)
				{
					csvWriter.close();
				}
			}
			catch (final Exception e)
			{
				LOG.error("Exception is in DefaultFeedsDaoImpl.executeQuery() : " + e);
			}
			csvWriter = null;
		}
		return csvWriter;
	}


	/**
	 * Method to process feed results.
	 *
	 * @param rawList
	 * @param feedResultsInStrings
	 */
	private void processFeedResults(final List<List<?>> rawList, final List<ArrayList<String>> feedResultsInStrings)
	{
		for (final List<?> list : rawList)
		{
			final ArrayList<String> recordList = new ArrayList<>();
			for (int i = 0; i < list.size(); i++)
			{
				final String value = StringUtils.isNotBlank((String) list.get(i)) ? (String) list.get(i) : "";
				recordList.add(value);
			}
			feedResultsInStrings.add(recordList);
		}
	}


	/**
	 * Method to get CSV Writer
	 *
	 * @param feedName
	 * @param csvWriter
	 * @param internalHybrisFolderPath
	 * @param delimiter
	 * @param isSingleFile
	 * @param headerInStringArray
	 * @param count
	 * @return
	 * @throws IOException
	 */
	private CSVWriter getCSVWriter(final String feedName, CSVWriter csvWriter, final String internalHybrisFolderPath,
			final String delimiter, final Boolean isSingleFile, final String[] headerInStringArray, final int count)
			throws IOException
	{
		if (isSingleFile && csvWriter == null)
		{
			final String fileName = getFileName(feedName);
			final String localFilePath = internalHybrisFolderPath.concat(fileName);
			csvWriter = catCSVFileService.createSheet(localFilePath, delimiter);
			if (headerInStringArray.length > 0)
			{
				csvWriter.writeNext(headerInStringArray);
			}
		}
		else if (!isSingleFile)
		{
			final String fileName = getFileName(feedName, count);
			final String localFilePath = internalHybrisFolderPath.concat(fileName);
			csvWriter = catCSVFileService.createSheet(localFilePath, delimiter);
			if (headerInStringArray.length > 0)
			{
				csvWriter.writeNext(headerInStringArray);
			}
		}
		return csvWriter;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getFileName(final String feedName)
	{
		LOG.info("DefaultFeedsDaoImpl.getFileName() for" + feedName + STARTED);
		String fileName = Config.getString(OUTBOUND_DATA_FEED + feedName + ".fileNamingConvention", EMPTY_STRING);
		final String fileExtension = Config.getString(OUTBOUND_DATA_FEED + feedName + ".fileExtension", FILEEXTENSION);
		final String dataFormat = Config.getString(OUTBOUND_DATA_FEED + feedName + ".dateFormatInFileName", DATEFORMAT);
		if (StringUtils.isNoneBlank(dataFormat))
		{
			fileName = fileName + new SimpleDateFormat(dataFormat).format(new Date()) + fileExtension;
		}
		else
		{
			fileName = fileName + new SimpleDateFormat(DATEFORMAT).format(new Date()) + fileExtension;
		}
		// Make file extn configurable
		LOG.info("DefaultFeedsDaoImpl.getFileName() for" + feedName + ENDED);
		return fileName;
	}

	/**
	 * Method to get the file name
	 *
	 * @param feedName
	 * @param count
	 */
	public String getFileName(final String feedName, final int count)
	{
		LOG.info("DefaultFeedsDaoImpl.getFileName(feedName,count) for" + feedName + STARTED + " and file count is " + count);

		String fileName = Config.getString(OUTBOUND_DATA_FEED + feedName + ".fileNamingConvention", EMPTY_STRING);
		final String fileExtension = Config.getString(OUTBOUND_DATA_FEED + feedName + ".fileExtension", FILEEXTENSION);
		final String dataFormat = Config.getString(OUTBOUND_DATA_FEED + feedName + ".dateFormatInFileName", DATEFORMAT);
		if (StringUtils.isNoneBlank(dataFormat))
		{
			fileName = fileName + new SimpleDateFormat(dataFormat).format(new Date()) + "_0" + count + fileExtension;
		}
		else
		{
			fileName = fileName + new SimpleDateFormat(DATEFORMAT).format(new Date()) + "_0" + count + fileExtension;
		}

		LOG.info("DefaultFeedsDaoImpl.getFileName(feedName,count) for" + feedName + ENDED + " and file count is " + count);
		return fileName;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Date getFeedQueryStartTime(final FeedAuditModel feedAuditModel)
	{
		final String feedName = feedAuditModel.getInterfaceName().getCode();
		LOG.info("DefaultFeedsDaoImpl.getFeedQueryStartTime()  started for " + feedName);
		final String lastTriggerTimeQuery = Config.getString("outbounddatafeed.lasttriggertime.flexiquery", EMPTY_STRING);
		final Map<String, String> params = Collections.singletonMap("feedname", feedName);
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(lastTriggerTimeQuery);
		flexibleSearchQuery.addQueryParameters(params);
		flexibleSearchQuery.setResultClassList(java.util.Arrays.asList(Date.class));
		final SearchResult searchResult = flexibleSearchService.search(flexibleSearchQuery);
		final Date date = (Date) searchResult.getResult().get(0);
		LOG.info("DefaultFeedsDaoImpl.getFeedQueryStartTime()  ended for " + feedAuditModel.getInterfaceName().getCode()
				+ " and query start time is " + date);
		return date;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getFeedQueryResultCount(final FeedAuditModel feedAuditModel, String feedName)
	{
		final String resultCount = OUTBOUNGDATAFEED + "." + feedName + "." + FLEXIQUERYRESULTCOUNT;
		final String resultCountQuery = Config.getParameter(resultCount);
		final FlexibleSearchQuery query = new FlexibleSearchQuery(resultCountQuery);
		query.addQueryParameter("qryStartTime", feedAuditModel.getStartTimeParamForFeedQry());
		query.addQueryParameter("queryEndTime", feedAuditModel.getEndTimeParamForFeedQry());
		query.setResultClassList(Arrays.asList(Integer.class));
		final SearchResult<Integer> result = flexibleSearchService.search(query);
		final List<Integer> resultList = result.getResult();
		return resultList.stream().mapToInt(Integer::intValue).sum();
	}

	/**
	 *{@inheritDoc}
	 */
	@Override
	public Date getLastSuccessfulRunTime(FeedAuditModel feedAuditModel) {
		final String feedName = feedAuditModel.getInterfaceName().getCode();
		LOG.info("DefaultFeedsDaoImpl.getLastSuccessfulRunTime()  started for " + feedName);
		final String lastTriggerTimeQuery ="select TO_CHAR(MAX({FA.cronJobEndtTimeStamp}),'YYYY-MM-DD HH24:MI:SS') as cronJobEndtTimeStamp from {feedaudit as FA join InterfaceNameEnum as FE ON {FA.interfaceName} = {FE.pk}} where {FE.code} = ?feedname AND {FA.feedAuditStatus} = 'Success'";
		final Map<String, String> params = Collections.singletonMap("feedname", feedName);
		final FlexibleSearchQuery flexibleSearchQuery = new FlexibleSearchQuery(lastTriggerTimeQuery);
		flexibleSearchQuery.addQueryParameters(params);
		flexibleSearchQuery.setResultClassList(java.util.Arrays.asList(Date.class));
		final SearchResult searchResult = flexibleSearchService.search(flexibleSearchQuery);
		return (Date) searchResult.getResult().get(0);
	}
}
