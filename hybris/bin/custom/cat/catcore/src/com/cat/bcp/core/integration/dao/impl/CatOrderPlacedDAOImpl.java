package com.cat.bcp.core.integration.dao.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.util.Config;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.cat.bcp.core.integration.dao.CatOrderPlacedDao;
import com.cat.bcp.core.model.CompatibilityInfoModel;
import com.cat.bcp.core.model.FBCPartsOrderInfoModel;

import static com.cat.bcp.core.integration.constants.CatintegrationConstants.CRONJOB_EXECUTION_COMPLETED;


/**
 * The class contains the implementaion of methods of interface {@link CatOrderPlacedDao}
 */
public class CatOrderPlacedDAOImpl implements CatOrderPlacedDao
{

	@Resource
	private FlexibleSearchService flexibleSearchService;

	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public List<OrderModel> getListOfPlacedOrderToOrderDirector(final String odQuery)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(odQuery);
		final SearchResult<OrderModel> result = flexibleSearchService.search(query);
		return result.getResult();
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public List<FBCPartsOrderInfoModel> getFBCPartsOrderInfoModelList(final String fbcPartsOrderInfoQuery, final String orderID)
	{
		final FlexibleSearchQuery fbcPartsOrderInfoFlexibleQuery = new FlexibleSearchQuery(fbcPartsOrderInfoQuery);
		fbcPartsOrderInfoFlexibleQuery.addQueryParameter("orderID", orderID);
		final SearchResult<FBCPartsOrderInfoModel> result = flexibleSearchService.search(fbcPartsOrderInfoFlexibleQuery);
		return result.getResult();
	}

	@Override
	public List<CompatibilityInfoModel> getListOfConfigs(final String compatibilitycheckQuery)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(compatibilitycheckQuery);
		final SearchResult<CompatibilityInfoModel> result = flexibleSearchService.search(query);
		return result.getResult();
	}



    /**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public List<OrderModel> getFirstOrderInDupOrders(final String poNumber, final OrderModel order)
	{
		final String retrieveFirstOrderDupOrderQuery = Config.getParameter("orderdirector.dupOrders.firsOrder.flexiQuery");

		final FlexibleSearchQuery query = new FlexibleSearchQuery(retrieveFirstOrderDupOrderQuery);
		query.addQueryParameter("purchaseordernumber", poNumber);
		query.addQueryParameter("dealercode", order.getUnit().getUid());
		query.setResultClassList(Arrays.asList(OrderModel.class));
		final SearchResult<OrderModel> result = flexibleSearchService.search(query);
		return result.getResult();

	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getMarketingCompany(final String countrycode) {
		final String fbcMarketingCompanyQuery = Config.getParameter("cat.ws.base.url.marketingcompany.flexiQuery");
		
		final FlexibleSearchQuery query = new FlexibleSearchQuery(fbcMarketingCompanyQuery);
		query.addQueryParameter("countrycode", countrycode);
		query.setResultClassList(Arrays.asList(String.class));
		final SearchResult<String> result = flexibleSearchService.search(query);
		return result.getResult();
	}

	/**
	 *
	 * {@inheritDoc}
	 */

	@Override
	public List<ProductModel> getListOfCompatibilityCheckFailedConfigIDs(String compatibilitycheckQuery, Date lastQuerySuccessfulTime) {
		StringBuilder stringBuilder =new StringBuilder();
		final FlexibleSearchQuery query;
		stringBuilder.append(compatibilitycheckQuery);
			if(lastQuerySuccessfulTime !=null){
				stringBuilder.append(" and {ci.creationtime} >= ?lastQuerySuccessfulTime ");
				 query = new FlexibleSearchQuery(stringBuilder.toString());
				query.addQueryParameter("lastQuerySuccessfulTime",lastQuerySuccessfulTime);
			}
			else{
				 query = new FlexibleSearchQuery(stringBuilder.toString());
			}



		final SearchResult<ProductModel> result = flexibleSearchService.search(query);
		return result.getResult();
	}
}
