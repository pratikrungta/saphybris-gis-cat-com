
package com.cat.bcp.core.integration.exception;

import org.apache.log4j.Logger;


/**
 * The class contains the implementation of the methods which is used to retry connecting to order Director after
 * connection timeout.
 */
public class CatIntegrationRetryOnExceptionStrategy
{

	private final int retriesCount;
	private int retriesRemaining;
	private final long delay;
	private static final Logger LOGGER = Logger.getLogger(CatIntegrationRetryOnExceptionStrategy.class);

	/**
	 * @param numberOfRetries
	 *           no of times we would try connecting to order director in case of connection issues
	 * @param delay
	 *           the wait time if the connection times out
	 */
	public CatIntegrationRetryOnExceptionStrategy(final int numberOfRetries, final long delay)
	{
		this.retriesCount = numberOfRetries;
		retriesRemaining = numberOfRetries;
		this.delay = delay;
	}

	/**
	 * @return true if there are tries left
	 */
	public boolean shouldRetry()
	{
		LOGGER.debug("Retry Try No. : " + retriesRemaining);
		return retriesRemaining > 0;
	}

	/**
	 * checks for retry attempt make strategy where to continue or throw error
	 *
	 * @param errorCode
	 *           the errorcode for the exception
	 * @param errorMessage
	 *           the errormessage for the exception
	 * @throws CatODException
	 *            when retry attempts becomes zero
	 */
	public void errorOccured(final String errorCode, final String errorMessage) throws CatODException
	{
		retriesRemaining--;
		if (!shouldRetry())
		{
			throw new CatODException(errorCode,
					"Retry Failed: Total " + retriesCount + " attempts made at interval " + getDelay() + "ms :\n" + errorMessage);
		}
		waitUntilNextTry();
	}

	/**
	 * Make the current thread sleep for given time
	 */
	private void waitUntilNextTry()
	{
		try
		{
			Thread.sleep(getDelay());
		}
		catch (final InterruptedException ie)
		{
			LOGGER.error("Exception in CatIntegrationRetryOnExceptionStrategy.waitUntilNextTry() :", ie);
			Thread.currentThread().interrupt();
		}
	}

	/**
	 * The method returns the time to be delayed before checking for the connection again
	 * 
	 * @return long the delay time before checking for the connection again.
	 */

	public long getDelay()
	{
		return delay;
	}


}
