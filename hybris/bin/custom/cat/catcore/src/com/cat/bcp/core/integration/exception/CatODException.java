package com.cat.bcp.core.integration.exception;

/**
 *
 */
public class CatODException extends Exception
{

	/**
	 * Declaration
	 */
	private String catErrorCode;//NOSONAR
	/**
	 * Declaration
	 */
	private String catErrorMessage;//NOSONAR




	/**
	 * default constructor.
	 */
	public CatODException()
	{
		super();
	}



	/**
	 * Overridden constructor accepting error parameters.
	 *
	 * @param catCause
	 *           actual exception
	 * @param catErrorCode
	 *           the error code for the exception
	 * @param catErrorMessage
	 *           the errorMessage to be displayed
	 */
	public CatODException(final Throwable catCause, final String catErrorCode, final String catErrorMessage)
	{

		super(catCause);
		this.catErrorCode = catErrorCode;
		this.catErrorMessage = catErrorMessage;
	}

	/**
	 * Overridden constructor accepting error parameters.
	 *
	 * @param catErrorCode
	 *           error code in numeric format
	 * @param catErrorMessage
	 *           error message
	 * @return
	 */
	public CatODException(final String catErrorCode, final String catErrorMessage)
	{
		super();
		this.catErrorCode = catErrorCode;
		this.catErrorMessage = catErrorMessage;
	}

	public String getCatErrorCode()
	{
		return catErrorCode;
	}

	public void setCatErrorCode(final String catErrorCode)
	{
		this.catErrorCode = catErrorCode;
	}

	public String getCatErrorMessage()
	{
		return catErrorMessage;
	}

	public void setCatErrorMessage(final String catErrorMessage)
	{
		this.catErrorMessage = catErrorMessage;
	}
}
