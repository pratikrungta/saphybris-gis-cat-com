package com.cat.bcp.core.integration.ftp;

/**
 *
 */

/**
 * @author vkumarpendyam
 *
 */
public interface CatSFTPClient
{

	/**
	 * storeFileViaSFTP copies the file from the hybris in to FTP server
	 *
	 * @param localDirectory
	 * @param fileToFTP
	 * @param remoteDirectory
	 * @param hostName
	 * @param userName
	 * @return boolean value as status
	 */
	boolean storeFileViaSFTP(String localDirectory, String fileToFTP, String remoteDirectory, String hostName, String userName,
			String password, int sftpPORT);
}
