package com.cat.bcp.core.integration.ftp.impl;
/**
 *
 */

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.cat.bcp.core.integration.ftp.CatSFTPClient;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;


/**
 * @author vkumarpendyam
 *
 */
public class CatSFTPClientImpl implements CatSFTPClient
{
	private static final Logger LOG = Logger.getLogger(CatSFTPClientImpl.class);
	private static final String STRICT_HOST_KEY_CHECKING = "StrictHostKeyChecking";
	private static final String NO = "no";



	/**
	 * {@inheritDoc}
	 */
	public boolean storeFileViaSFTP(final String localDirectory, final String fileToFTP, final String remoteDirectory,
			final String hostName, final String userName, final String password, final int SFTPPORT)
	{
		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		FileInputStream fis = null;
		final Properties config = new Properties();
		config.put(STRICT_HOST_KEY_CHECKING, NO);
		boolean isFileMoveToFTP = false;
		try
		{
			final JSch jsch = new JSch();
			session = jsch.getSession(userName, hostName, SFTPPORT);
			session.setPassword(password);

			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			final StringBuilder sBuild = new StringBuilder();
			sBuild.append("SFTP is connected Hostname: ").append(hostName).append(", remoteDirectory: ").append(remoteDirectory)
					.append(", userName: ").append(userName);
			if (LOG.isDebugEnabled())
			{
				LOG.debug(sBuild);
			}
			channelSftp = (ChannelSftp) channel;
			channelSftp.cd(remoteDirectory);
			fis = new FileInputStream(localDirectory + fileToFTP);
			channelSftp.put(fis, fileToFTP);

			LOG.info(fileToFTP + " is moved to FTP Server via SFTP client");
			isFileMoveToFTP = true;
		}
		catch (final Exception e)
		{
			LOG.error("Exception while file moving to SFTP server ", e);
		}

		finally
		{
			clear(session, channel, fis);
		}
		return isFileMoveToFTP;

	}

	private void clear(final Session session, final Channel channel, final FileInputStream fis)
	{
		try
		{
			if (null != fis)
			{
				fis.close();
			}
			if (null != channel && channel.isConnected())
			{
				channel.disconnect();
				LOG.info("SFTP session is disconnected");
			}

			if (null != session && session.isConnected())
			{
				session.disconnect();
				LOG.info("SFTP session is disconnected");
			}
		}
		catch (final IOException ioe)
		{
			LOG.error("Exception while closing the FileInputStream", ioe);
		}

	}
}
