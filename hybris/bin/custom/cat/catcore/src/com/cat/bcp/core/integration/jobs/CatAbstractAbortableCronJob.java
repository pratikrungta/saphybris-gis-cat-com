package com.cat.bcp.core.integration.jobs;



import de.hybris.platform.cronjob.jalo.AbortCronJobException;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;


/**
 *
 */
public abstract class CatAbstractAbortableCronJob extends AbstractJobPerformable<CronJobModel>
{


	@Override
	public boolean isAbortable()
	{
		return true;
	}

	/**
	 * Used to check if there is an abort request for the cron job
	 * 
	 * @param cronJobModel
	 *           the cron job model
	 * @throws AbortCronJobException
	 *            if the cron job is aborted manually
	 */
	public void checkIfRequestedToAbortCron(final CronJobModel cronJobModel) throws AbortCronJobException
	{

		if (clearAbortRequestedIfNeeded(cronJobModel))
		{
			throw new AbortCronJobException(" CatAbortableCronJob is aborted");
		}

	}
}


