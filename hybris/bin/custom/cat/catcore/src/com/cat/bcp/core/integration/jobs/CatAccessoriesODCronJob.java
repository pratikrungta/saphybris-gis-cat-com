package com.cat.bcp.core.integration.jobs;


import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.jalo.AbortCronJobException;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.net.MalformedURLException;

import javax.annotation.Resource;
import javax.xml.ws.soap.SOAPFaultException;

import org.apache.log4j.Logger;

import com.cat.bcp.core.integration.jobs.service.impl.CatAccessoriesODIntegrationServiceImpl;


/**
 * The cron job is used to fetch all the orders to be submitted to Order Director,generated request in the xml format
 * recognized by the Order Director, save the request and response with the necessary exception details if any in an
 * {@link com.cat.core.integration.model.InterfaceLogsModel}. On successful submission , the order goes to SUBMITTED
 * Status On Unsuccessful submission,the order goes to FAILED_SUBMISSION status
 */
public class CatAccessoriesODCronJob extends CatAbstractAbortableCronJob
{

	private static final Logger LOGGER = Logger.getLogger(CatAccessoriesODCronJob.class.getName());

	@Resource
	private CatAccessoriesODIntegrationServiceImpl catAccessoriesODIntegrationService;

	

	@Override
	public PerformResult perform(final CronJobModel cronJob)
	{
		try
		{
			catAccessoriesODIntegrationService.submitPlacedOrdersToOrderDirector(this, cronJob);
		}
		catch (final AbortCronJobException e)
		{
			LOGGER.error("Cron job is aborted", e);
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}
		catch (final MalformedURLException malformedURLException)
		{
			LOGGER.error("Webservice url is not formed correctly", malformedURLException);
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}
		catch (final SOAPFaultException soapFaultException)
		{
			LOGGER.error(soapFaultException.getFault().getFaultString(), soapFaultException);
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}
}
