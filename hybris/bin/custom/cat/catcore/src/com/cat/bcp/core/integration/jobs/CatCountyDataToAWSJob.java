package com.cat.bcp.core.integration.jobs;

import de.hybris.platform.core.GenericSearchConstants.LOG;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.jalo.AbortCronJobException;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.util.Config;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.cat.bcp.core.integration.jobs.service.CatOutboundService;


/**
 * Cron job is used to send County DATA to AWS system
 */
//NO SONAR
public class CatCountyDataToAWSJob extends CatAbstractAbortableCronJob
{

	private static final Logger LOG = Logger.getLogger(CatCountyDataToAWSJob.class.getName());
	@Resource
	private CatOutboundService catOutboundService;
	@Resource
	private ConfigurationService configurationService;

	@Override
	public PerformResult perform(final CronJobModel cronJobModel)
	{
		LOG.info("CatCountyDataToAWSJob started");
		try
		{
			final String countyMapping = Config.getString("outboundDataFeed.countymapping.feedName", "countymapping");
			catOutboundService.startFeedGeneration(countyMapping, this, cronJobModel);
		}
		catch (final AbortCronJobException e)
		{
			LOG.log("Cron job is aborted", null, false, e);
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}
}

