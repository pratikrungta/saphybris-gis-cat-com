/**
 *
 */
package com.cat.bcp.core.integration.jobs;

import com.cat.bcp.core.integration.constants.CatintegrationConstants;
import com.cat.bcp.core.integration.jobs.service.impl.CatFeedsUtilsService;
import com.cat.core.integration.enums.InterfaceNameEnum;
import com.cat.core.integration.model.FeedAuditModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.jalo.AbortCronJobException;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import java.net.MalformedURLException;
import java.util.Date;

import javax.annotation.Resource;
import javax.xml.ws.soap.SOAPFaultException;

import de.hybris.platform.servicelayer.model.ModelService;
import org.apache.log4j.Logger;
import org.springframework.web.client.ResourceAccessException;

import com.cat.bcp.core.integration.jobs.service.impl.CatCompatibilityCheckServiceImpl;


/**
 * @author vkumarpendyam
 *
 */
public class CatOrderCompatibilityCheckJob extends CatAbstractAbortableCronJob
{

	private static final Logger LOGGER = Logger.getLogger(CatOrderCompatibilityCheckJob.class.getName());

	@Resource
	CatCompatibilityCheckServiceImpl catCompatibilityCheckService;

	@Resource
	CatFeedsUtilsService catFeedsUtilsService;

	@Resource
	ModelService modelService;

	@Override
	public PerformResult perform(final CronJobModel cronJob)
	{
		FeedAuditModel feedAuditModel=new FeedAuditModel();
		feedAuditModel.setInterfaceName(InterfaceNameEnum.COMPATIBILITY_CHECK);
		try
		{

			catCompatibilityCheckService.checkCompatibility(this, cronJob);
		}
		catch (final AbortCronJobException e)
		{

			LOGGER.error("Cron job is aborted", e);
			updateFeedAuditStatusAndCronJobEndTime(feedAuditModel,CatintegrationConstants.FAILURE);
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}
		catch (final ResourceAccessException resourceAccessException)
		{
			LOGGER.error("Cron job is aborted", resourceAccessException);
			updateFeedAuditStatusAndCronJobEndTime(feedAuditModel,CatintegrationConstants.FAILURE);
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}
		catch (final MalformedURLException malformedURLException)
		{
			LOGGER.error("Webservice url is not formed correctly", malformedURLException);
			updateFeedAuditStatusAndCronJobEndTime(feedAuditModel,CatintegrationConstants.FAILURE);
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);

		}
		catch (final SOAPFaultException soapFaultException)
		{
			LOGGER.error(soapFaultException.getFault().getFaultString(), soapFaultException);
			updateFeedAuditStatusAndCronJobEndTime(feedAuditModel,CatintegrationConstants.FAILURE);
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}
		try {
			catCompatibilityCheckService.sentEmailForCompatibilityFailedCheckConfigIDs(feedAuditModel);
		}catch(Exception e){
			LOGGER.error("Problem ocurred while sending compatibility failed check email", e);
			updateFeedAuditStatusAndCronJobEndTime(feedAuditModel,CatintegrationConstants.FAILURE);
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}
		catFeedsUtilsService.addAuditLogEntry(feedAuditModel, CatintegrationConstants.CRONJOB_EXECUTION_COMPLETED);

		updateFeedAuditStatusAndCronJobEndTime(feedAuditModel,CatintegrationConstants.SUCCESS);
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

	private void updateFeedAuditStatusAndCronJobEndTime(FeedAuditModel feedAuditModel,String status) {
		feedAuditModel.setFeedAuditStatus(status);
		feedAuditModel.setCronJobEndtTimeStamp(new Date());
		modelService.save(feedAuditModel);
	}
}