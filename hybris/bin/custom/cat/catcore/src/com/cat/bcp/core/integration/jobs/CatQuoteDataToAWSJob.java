package com.cat.bcp.core.integration.jobs;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.jalo.AbortCronJobException;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.util.Config;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.cat.bcp.core.integration.jobs.service.CatOutboundService;


/**
 * Cron job is used to send Quote DATA to AWS system
 */
//NO SONAR
public class CatQuoteDataToAWSJob extends CatAbstractAbortableCronJob
{

	private static final Logger LOG = Logger.getLogger(CatQuoteDataToAWSJob.class.getName());
	@Resource
	private CatOutboundService catOutboundService;
	@Resource
	private ConfigurationService configurationService;

	@Override
	public PerformResult perform(final CronJobModel cronJobModel)
	{
		LOG.info("CatQuoteDataToAWSJob started");
		try
		{
			final String quoteClosure = Config.getString("outboundDataFeed.customerquote.feedName", "customerquote");
			catOutboundService.startFeedGeneration(quoteClosure, this, cronJobModel);
		}
		catch (final AbortCronJobException e)
		{
			LOG.log("Cron job is aborted", null, false, e);
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
		}
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}
}
