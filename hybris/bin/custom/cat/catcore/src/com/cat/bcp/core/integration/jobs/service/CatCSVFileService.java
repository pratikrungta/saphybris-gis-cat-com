package com.cat.bcp.core.integration.jobs.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import au.com.bytecode.opencsv.CSVWriter;


/**
 * CatCSVFileService
 */
public interface CatCSVFileService
{


	/**
	 * generateCsvFile copies the records in the CSV file
	 *
	 * @param csvWriter
	 *           reference of CSVWriter object
	 * @param sourceList
	 *           contains the string list
	 */
	void generateCsvFile(final CSVWriter csvWriter, final List<ArrayList<String>> sourceList);

	/**
	 * createSheet creates the CSVWriter
	 *
	 * @param filePath
	 *           path of the file
	 * @param delimitter
	 *           to divide the columns
	 * @return CSVWriter returns the csvWriter object
	 * @throws IOException
	 *            input/output exception
	 */
	CSVWriter createSheet(final String filePath, final String delimitter) throws IOException;
}
