package com.cat.bcp.core.integration.jobs.service;

import com.cat.core.integration.model.FeedAuditModel;
import de.hybris.platform.cronjob.jalo.AbortCronJobException;
import de.hybris.platform.cronjob.model.CronJobModel;

import java.net.MalformedURLException;

import com.cat.bcp.core.integration.jobs.CatAbstractAbortableCronJob;


/**
 * The interface to be implemented to start the process of submitting the configuration to CompatibilityCheck
 */

public interface CatCompatibilityCheckService
{
	/**
	 * The method to be called for the initiation of the process of config submission to CompatibilityCheck.
	 *
	 * @param catPickUpOrderPlacedCronJob
	 *           the cron job to be run for the order submission
	 * @param cronJob
	 *           the cron job model
	 * @throws AbortCronJobException
	 *            the exception when cron job is aborted
	 * @throws MalformedURLException
	 *            if the url for the connection is not proper
	 */
	void checkCompatibility(CatAbstractAbortableCronJob catPickUpOrderPlacedCronJob, CronJobModel cronJob)
			throws AbortCronJobException, MalformedURLException;

	/**
	 * The method is used to call the event for the initiation of config failed emails.
	 * @param feedAuditModel the feed audit model
	 */
	void sentEmailForCompatibilityFailedCheckConfigIDs(FeedAuditModel feedAuditModel);

}
