/**
 *
 */
package com.cat.bcp.core.integration.jobs.service;

import de.hybris.platform.commercefacades.order.data.CatReserveQuoteData;

import com.cat.bcp.core.exception.CatException;
import com.cat.core.integration.edh.CatEDHInventoryDetailsDataList;


/**
 * This Interface is to fetch EDH Inventory Data.
 *
 * @author ravjonnalagadda
 *
 */

public interface CatIntegrationService
{
	/**
	 * This method is to get InventoryDetails from EDH.
	 *
	 * @param inventoryDetailsDataList
	 *           :Object it holds URL parameters list.
	 * @return : return inventory Details.
	 * @throws CatException
	 *            : throws generic CatException.
	 */
	String getInventoryDetails(CatEDHInventoryDetailsDataList inventoryDetailsDataList) throws CatException;

	/**
	 * This method is to get inventory count by party ID.
	 *
	 * @param partyId
	 *           : Code which identifies a given PDC, manufacturing facility or dealer.
	 * @param config
	 *           : A configuration ID associated with a given machine
	 * @param getDescendantNodes
	 *           : A boolean flag ('true', 'false') for whether children objects should be included
	 * @return : returns JSON response
	 * @throws CatException
	 *            : throws generic CatException.
	 */
	public String getInventoryCountByPartyId(String partyId, String config, String getDescendantNodes) throws CatException;

	/**
	 * This method is to get serialNumber details for the given partyId and ConfigId.
	 *
	 * @param productCode
	 *           : ConfigProduct
	 * @param dealerCode
	 *           : dealerCode
	 * @return : return catReserveQuoteData
	 * @throws CatException
	 *            : : throws generic CatException.
	 */
	public CatReserveQuoteData getSerialNumbersData(final String productCode, final String dealerCode) throws CatException;


}
