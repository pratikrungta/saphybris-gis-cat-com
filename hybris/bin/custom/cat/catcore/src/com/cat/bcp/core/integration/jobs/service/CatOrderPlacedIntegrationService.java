package com.cat.bcp.core.integration.jobs.service;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.jalo.AbortCronJobException;
import de.hybris.platform.cronjob.model.CronJobModel;

import java.net.MalformedURLException;

import javax.management.InvalidAttributeValueException;
import javax.xml.bind.JAXBException;

import com.cat.bcp.core.integration.exception.CatODException;
import com.cat.bcp.core.integration.jobs.CatAbstractAbortableCronJob;


/**
 * The interface to be implemented to start the process of submitting the hybris order to Order Director
 */

public interface CatOrderPlacedIntegrationService
{
	/**
	 * The method to be called for the initiation of the process of order submission to order director.
	 * 
	 * @param catPickUpOrderPlacedCronJob
	 *           the cron job to be run for the order submission
	 * @param cronJob
	 *           the cron job model
	 * @throws AbortCronJobException
	 *            the exception when cron job is aborted
	 * @throws MalformedURLException
	 *            if the url for the connection is not proper
	 * @throws CatODException
	 *            when an error occurs during the submission
	 * @throws JAXBException
	 *            the exception if there is any issue during the marshalling and unmarshalling of xml
	 */
	void submitPlacedOrdersToOrderDirector(CatAbstractAbortableCronJob catPickUpOrderPlacedCronJob, CronJobModel cronJob)
			throws AbortCronJobException, MalformedURLException;

	/**
	 * The implementation of the method will validate the attributes of the order to be equal to required length
	 * 
	 * @param orderModel
	 *           the order to be validated
	 * @param orderEntry
	 *           the order entry of the order
	 * @throws InvalidAttributeValueException
	 *            if an attribute is invalid
	 */
	void validateOrderAttributeValues(OrderModel orderModel, AbstractOrderEntryModel orderEntry)
			throws InvalidAttributeValueException;
}
