package com.cat.bcp.core.integration.jobs.service;

import de.hybris.platform.cronjob.jalo.AbortCronJobException;
import de.hybris.platform.cronjob.model.CronJobModel;

import java.io.FileNotFoundException;

import com.cat.bcp.core.integration.jobs.CatAbstractAbortableCronJob;


/**
 * @author sjeedula
 *
 */
/**
 * @author sjeedula
 *
 */
public interface CatOutboundService {

	/**
	 * startFeedGeneration starts necessary data required for actual files generation
	 *
	 * @param feedName
	 *           integration type name
	 * @param catAbstractAbortableCronJob
	 *           cronjob reference
	 * @param cronJobModel
	 *           cronjob datamodel
	 * @throws AbortCronJobException
	 *            exception thrown if cronjob is aborted
	 */
	void startFeedGeneration(String feedName, final CatAbstractAbortableCronJob catAbstractAbortableCronJob,
			CronJobModel cronJobModel) throws AbortCronJobException;

    /**
     * transfers files to external FTP server
     *
     * @param feedName integration type name
     * @param internalHybrisFolderPath file path of the generatedfile
     * @return boolean value which shows whether the file is transfered or not
     */
     boolean transferFileToThirdPartyFTP(final String feedName, final String internalHybrisFolderPath);

    
    /**
     * checkIfFoldersExists checks for the source folder existence
     *
     * @param feedName integration type name
     * @return boolean checks if folder exists or not
     * @throws FileNotFoundException exception thrown if file doesn't exist
     */
     boolean checkIfFoldersExists(final String feedName) throws FileNotFoundException;


    /**
     * deleteOldFiles deletes the old files
     *
     * @param feedName integration type name
     * @throws IllegalArgumentException exception thrown if file doesn't exist
     */
     void deleteOldFiles(final String feedName) throws IllegalArgumentException;
}
