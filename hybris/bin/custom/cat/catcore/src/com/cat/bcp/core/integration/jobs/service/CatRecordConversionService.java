package com.cat.bcp.core.integration.jobs.service;

import java.util.List;

import com.cat.bcp.core.integration.beans.CatControlRecord;
import com.cat.bcp.core.integration.beans.CatDupOrderRecord;
import com.cat.bcp.core.integration.beans.CatInlandShippingRecord;
import com.cat.bcp.core.integration.beans.CatInvoiceCriteriaRecord;
import com.cat.bcp.core.integration.beans.CatNoteRecord;
import com.cat.bcp.core.integration.beans.CatOrderRecord;
import com.cat.bcp.core.integration.beans.CatSpecificationRecord;
import com.cat.bcp.core.integration.beans.CatTrailerRecord;


/**
 * The interface contains method which needs to be implemented for the conversion of different order records into String
 * format.
 */
public interface CatRecordConversionService
{
	/**
	 * Converts the {@link CatOrderRecord} in web service recognizable String
	 * 
	 * @param catControlRecord
	 *           the {@link CatControlRecord}
	 * @param catOrderRecordString
	 *           the string to be appended to the catControlRecord string
	 * @return CatControlRecord in string format
	 */
	String convertControlRecordToString(CatControlRecord catControlRecord, String catOrderRecordString);

	/**
	 * Converts the {@link CatSpecificationRecord} in web service recognizable String
	 * 
	 * @param specificationRecord
	 *           the {@link CatSpecificationRecord}
	 * @param catOrderRecordString
	 *           the string to be appended to the specificationRecord string
	 * @return CatSpecificationRecord in string format
	 */
	List<String> convertSpecificationRecordsToString(List<CatSpecificationRecord> specificationRecord,
			String catOrderRecordString);

	/**
	 * Converts the {@link CatDupOrderRecord} in web service recognizable String
	 * 
	 * @param catDupOrderRecords
	 *           list of {@link CatDupOrderRecord}
	 * @param catOrderRecordString
	 *           the string to be appended to the noteRecord string
	 * @return catDupOrderRecord in string format
	 */
	List<String> convertDupRecordsToString(List<CatDupOrderRecord> catDupOrderRecords, String catOrderRecordString);

	/**
	 * Converts the {@link CatNoteRecord} in web service recognizable String
	 * 
	 * @param noteRecord
	 *           {@link CatNoteRecord}
	 * @param catOrderRecordString
	 *           the string to be appended to the noteRecord string
	 * @return CatNoteRecord in string format
	 */
	List<String> convertNoteRecordsToString(List<CatNoteRecord> noteRecord, String catOrderRecordString);

	/**
	 * Converts the {@link CatInlandShippingRecord} in web service recognizable String
	 * 
	 * @param catInlandShippingRecord
	 *           the {@link CatInlandShippingRecord}
	 * @param catOrderRecordString
	 *           the string to be appended to the catInlandShippingRecord string
	 * @return CatInlandShippingRecord in string format
	 */
	String convertCatInlandShippingRecordString(CatInlandShippingRecord catInlandShippingRecord, String catOrderRecordString);

	/**
	 * Converts the {@link CatTrailerRecord} in web service recognizable String
	 * 
	 * @param trailerRecord
	 *           the {@link CatTrailerRecord}
	 * @param catOrderRecordString
	 *           the string to be appended to the trailerRecord string
	 * @return CatTrailerRecord in string format
	 */
	String convertTrailerRecordString(CatTrailerRecord trailerRecord, String catOrderRecordString);

	/**
	 * The method used to convert catOrderRecord into String format
	 * 
	 * @param catOrderRecord
	 *           The order records
	 * @return the catOrderRecord in string format
	 */
	String convertOrderRecordToString(CatOrderRecord catOrderRecord);


	/**
	 * The method used to convert CatInvoiceCriteriaRecord into String format
	 * 
	 * @param catInvoiceCriteriaRecord
	 *           the N5 order record
	 * @param catOrderRecordString
	 *           the string to be appended to the invoice criteria string
	 * @return the catOrderRecord in string format
	 */
	String convertCatInvoiceCriteriarecordString(CatInvoiceCriteriaRecord catInvoiceCriteriaRecord, String catOrderRecordString);
}
