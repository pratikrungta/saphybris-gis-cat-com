package com.cat.bcp.core.integration.jobs.service;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;
import java.util.Map;

import com.cat.bcp.core.integration.beans.CatControlRecord;
import com.cat.bcp.core.integration.beans.CatDupOrderRecord;
import com.cat.bcp.core.integration.beans.CatInlandShippingRecord;
import com.cat.bcp.core.integration.beans.CatInvoiceCriteriaRecord;
import com.cat.bcp.core.integration.beans.CatNoteRecord;
import com.cat.bcp.core.integration.beans.CatOrderRecord;
import com.cat.bcp.core.integration.beans.CatSpecificationRecord;
import com.cat.bcp.core.integration.beans.CatTrailerRecord;


/**
 * The interface contains different methods which needs to be implemented to create order records to be passed to Order
 * Director for the order submission
 */
public interface CatRecordCreationService
{

	/**
	 * The method implementation will create CatOrderRecord which will be common to all records
	 * 
	 * @param order
	 *           the order for which the order record needs to be created
	 * @return CatOrderRecord the cat order record
	 */

	CatOrderRecord createOrderRecord(OrderModel order);

	/**
	 * The method implementation will create Control Record(N1)
	 * 
	 * @param order
	 *           the order for which the order record needs to be created
	 * @param orderEntry
	 *           the order entry within the order
	 * @return CatControlRecord the {@link CatControlRecord }
	 */

	CatControlRecord createControlRecord(OrderModel order, AbstractOrderEntryModel orderEntry);

	/**
	 * The method implementation will create Control Record(N1) for dup orders
	 * 
	 * @param order
	 *           the order for which the order record needs to be created
	 * @param orderEntry
	 *           the order entry within the order
	 * @param listOfPONumbers
	 *           for the duporders
	 * @return CatControlRecord the {@link CatControlRecord }
	 */
	CatControlRecord createControlRecord(OrderModel order, AbstractOrderEntryModel orderEntry, List<String> listOfPONumbers);

	/**
	 * The method implementation will create Specification record(N2)
	 * 
	 * @param order
	 *           the order for which the order record needs to be created
	 * @param orderEntry
	 *           the entry for the order
	 * @return List<CatSpecificationRecord> the list of {@link CatSpecificationRecord }
	 */

	List<CatSpecificationRecord> createSpecificationrecord(OrderModel order, AbstractOrderEntryModel orderEntry);
	
	/**
	 * The method implementation will create UTV Accessroies Specification records(N2)
	 * 
	 * @param order
	 *           the order for which the order record needs to be created
	 * @return List<CatSpecificationRecord> the list of {@link CatSpecificationRecord }
	 */

	List<CatSpecificationRecord> createAccessorySpecificationrecords(OrderModel order);

	/**
	 * The method implementation will create Note & truckload record(N3)
	 * 
	 * @param order
	 *           the order for which the order record needs to be created
	 * @param truckLoadIDs 
	 * 			 mix and match UTV truckload ids
	 * @return List<CatNoteRecord> the list of {@link CatNoteRecord}
	 */

	List<CatNoteRecord> createNotesWithTruckloadRecord(OrderModel order, Map<String, String> truckLoadIDs);
	
	
	/**
	 * The method implementation will create Note record(N3)
	 * 
	 * @param order
	 *           the order for which the order record needs to be created
	 * @return List<CatNoteRecord> the list of {@link CatNoteRecord}
	 */

	List<CatNoteRecord> createNotesRecord(OrderModel order);

	/**
	 * This method creates OD dup orders
	 * 
	 * @param subSetsOfPONumbers
	 *           the string to be appended to the noteRecord string
	 * @return List<CatDupOrderRecord> list of {@link CatDupOrderRecord}
	 */
	List<CatDupOrderRecord> createDupOrderRecord(List<List<String>> subSetsOfPONumbers);

	/**
	 * The method implementation will create record(N4)
	 * 
	 * @param order
	 *           the order for which the order record needs to be created
	 * @param frieghtChargeCodeMap
	 *           tha map containing the dealer and freightchargecode
	 * @return CatInlandShippingRecord the {@link CatInlandShippingRecord}
	 */

	CatInlandShippingRecord createInlandShippingRecord(OrderModel order, Map<String, String> frieghtChargeCodeMap);

	/**
	 * The method implementation will create record(N4)
	 *
	 * @param order
	 *           the order for which the order record needs to be created
	 * @param salesModel
	 *           the salesModel record holds the different freight codes
	 * @return CatInlandShippingRecord the {@link CatInlandShippingRecord}
	 */

	CatInlandShippingRecord createFreightCodesRecord(OrderModel order, ProductModel salesModel);

	/**
	 * The method implementation will create record(N9)
	 * 
	 * @param order
	 *           the order for which the order record needs to be created
	 * @return CatTrailerRecord the {@link CatTrailerRecord}
	 */

	CatTrailerRecord createTrailerRecord(OrderModel order);

	/**
	 * The method used to remove special characters from the String value
	 * 
	 * @param value
	 *           the value on which the regex expression will be applied
	 * @param regex
	 *           the expression used for the removal of special characters
	 * @return String the value after the removal of special characters
	 */
	String getValueWithoutSpecialCharacters(String value, String regex);

	/**
	 * The method implementation will create record(N5)
	 * 
	 * @param orderModel
	 *           the order for which the order record needs to be created
	 * @param paymentTermsMap
	 *           the map containing the dealer code and payment terms
	 * @return CatInvoiceCriteriaRecord the {@link CatInvoiceCriteriaRecord}
	 */
	CatInvoiceCriteriaRecord createInvoiceCriteriaRecord(OrderModel orderModel, Map<String, String> paymentTermsMap);

	/**
	 * The method implementation will create record(N5)
	 * 
	 * @param abstractOrderEntryModel
	 *           reference of orderentry
	 * @return CatInvoiceCriteriaRecord the {@link CatInvoiceCriteriaRecord}
	 */
	CatInvoiceCriteriaRecord createInvoiceRecordForSalesModel(AbstractOrderEntryModel abstractOrderEntryModel);
}
