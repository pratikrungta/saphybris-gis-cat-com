package com.cat.bcp.core.integration.jobs.service;

import java.net.MalformedURLException;

import com.cat.dice.cmops.slaes.v1.export1.CMOPSOrderingService;
import com.cat.service.cmops.cmopscompatibilitycheckservice.v1.CheckCompatibility;



/**
 * The class for getting the connection from the Order Director web service
 */

public interface CatSoapConfigurationService
{


	/**
	 *
	 * @return CMOPSOrderingService the {@link CMOPSOrderingService} used for the creation of order record to be
	 *         submitted through web service
	 * @throws MalformedURLException
	 *            if the url for the web service connection is not created properly
	 */
	CMOPSOrderingService getSOAPClientService() throws MalformedURLException;

	/**
	 *
	 * @return CheckCompatibility the {@link CheckCompatibility} used for the compatibility records for the
	 *         checkcompatibility
	 * @throws MalformedURLException
	 *            if the url for the web service connection is not created properly
	 */
	CheckCompatibility getCompatibilityCheckSOAPClientService() throws MalformedURLException;
}
