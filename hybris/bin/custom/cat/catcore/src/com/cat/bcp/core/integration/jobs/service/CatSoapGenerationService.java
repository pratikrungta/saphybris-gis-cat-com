package com.cat.bcp.core.integration.jobs.service;

import java.util.List;

import javax.xml.bind.JAXBException;

import com.cat.bcp.core.model.CompatibilityInfoModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.dice.cmops.slaes.v1.export1.OrderMachinesRequest;
import com.cat.dice.cmops.slaes.v1.export1.OrderMachinesResponse;
import com.cat.service.cmops.cmopscompatibilitycheckrequest.v1.CMOPSCompatibilityCheckRequest;


/**
 * The interface having the method which needs to be implemented for the generation of SOAP xml to be sent to Order
 * Director for the order submission.
 */
public interface CatSoapGenerationService
{

	/**
	 * Adding the list of order records to OrderRecordTypes
	 *
	 * @param orderRecords
	 *           the orderrecords to be sent to Order Director for order submission
	 * @param orderMachinesRequest
	 *           request in the form of XML sent to orderdirector
	 * @return OrderMachinesResponse the response from the Order Director
	 */
	void getOrderRecordsTypes(List<String> orderRecords, OrderMachinesRequest orderMachinesRequest);

	/**
	 * Adding the list of parts to for the request
	 *
	 * @param configVariantProduct
	 *           the configvariant to be sent to CompatibilityCheck
	 * @param cmopsCompatibilityCheckRequest
	 *           request in the form of XML sent to CompatibilityCheck
	 * @param compatibilityInfoModel
	 *           request in the form of XML sent to CompatibilityCheck
	 * @return compatibilityInfoModel model which contains compatibility status and lane type
	 */
	public void setCompatibityCheckValues(ConfigVariantProductModel configVariantProduct,
			CMOPSCompatibilityCheckRequest cmopsCompatibilityCheckRequest, CompatibilityInfoModel compatibilityInfoModel);

	/**
	 * The method converts the request in String format to be saved in logs table
	 *
	 * @param orderMachinesRequest
	 *           the request to be sent to Order Director
	 * @return the request in String format to be saved in logs
	 * @throws JAXBException
	 *            when there is error while marshalling and unmarshalling
	 */
	String createXmlRequest(final OrderMachinesRequest orderMachinesRequest) throws JAXBException;

	/**
	 * this method creates compatibilitycheck request XML
	 *
	 * @param cmopsCompatibilityCheckRequest
	 *           request in the form of XML sent to CompatibilityCheck
	 * @return the request in String format to be saved in logs
	 * @throws JAXBException
	 *            when there is error while marshalling and unmarshalling
	 */
	String createCompatibilityCheckXmlRequest(final CMOPSCompatibilityCheckRequest cmopsCompatibilityCheckRequest)
			throws JAXBException;

	/**
	 * The method converts the response in String format to be saved in logs table
	 *
	 * @param orderMachinesResponse
	 *           the response from the Order Director
	 * @return response in String format
	 * @throws JAXBException
	 *            when there is an error during the marshalling and unmarshalling
	 */
	String createReXmlResponse(final OrderMachinesResponse orderMachinesResponse) throws JAXBException;

}
