package com.cat.bcp.core.integration.jobs.service.impl;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.jalo.AbortCronJobException;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.Config;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.management.InvalidAttributeValueException;
import javax.xml.bind.JAXBException;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.integration.beans.CatControlRecord;
import com.cat.bcp.core.integration.beans.CatInlandShippingRecord;
import com.cat.bcp.core.integration.beans.CatNoteRecord;
import com.cat.bcp.core.integration.beans.CatOrderRecord;
import com.cat.bcp.core.integration.beans.CatSpecificationRecord;
import com.cat.bcp.core.integration.beans.CatTrailerRecord;
import com.cat.bcp.core.integration.constants.CatintegrationConstants;
import com.cat.bcp.core.integration.dao.CatOrderPlacedDao;
import com.cat.bcp.core.integration.exception.CatIntegrationRetryOnExceptionStrategy;
import com.cat.bcp.core.integration.jobs.CatAbstractAbortableCronJob;
import com.cat.bcp.core.integration.jobs.service.CatRecordConversionService;
import com.cat.bcp.core.integration.jobs.service.CatRecordCreationService;
import com.cat.bcp.core.integration.jobs.service.CatSoapConfigurationService;
import com.cat.bcp.core.integration.jobs.service.CatSoapGenerationService;
import com.cat.core.integration.CatIntegrationUtils;
import com.cat.core.integration.handler.CatServiceLogHandler;
import com.cat.core.integration.model.InterfaceLogsModel;
import com.cat.dice.cmops.slaes.v1.export1.CMOPSOrderingService;
import com.cat.dice.cmops.slaes.v1.export1.OrderMachinesRequest;
import com.cat.dice.cmops.slaes.v1.export1.OrderMachinesResponse;
import com.cat.dice.cmops.slaes.v1.export1.OrderResultsType;



/**
 * The class is used to create the order records to be sent to Order Director.
 */
public class CatAccessoriesODIntegrationServiceImpl extends CatDupOrderPlacedIntegrationServiceImpl
{

	private static final Logger LOGGER = Logger.getLogger(CatAccessoriesODIntegrationServiceImpl.class.getName());
	public static final String ERROR_WHILE_CREATING_ORDER_RECORDS_FOR_ORDER = "Error while creating order records for Order ";

	@Resource
	private CatRecordConversionService catRecordConversionService;

	@Resource
	private CatRecordCreationService catRecordCreationService;

	@Resource
	private CatOrderPlacedDao catOrderPlacedDao;

	@Resource
	ModelService modelService;

	@Resource
	ConfigurationService configurationService;

	@Resource
	CatServiceLogHandler catServiceLogHandler;

	@Resource
	private CatSoapConfigurationService catSoapConfigurationService;

	@Resource
	private CatSoapGenerationService catSoapGenerationService;

	public CatServiceLogHandler getCatServiceLogHandler()
	{
		return catServiceLogHandler;
	}


	/**
	 * {@inheritDoc}
	 */

	@Override
	public void submitPlacedOrdersToOrderDirector(final CatAbstractAbortableCronJob catAbortableCronJob,
			final CronJobModel cronJob) throws AbortCronJobException, MalformedURLException
	{

		final String odAccessoriesQuery = Config.getParameter("orderdirector.accessories.flexiQuery");
		final List<OrderModel> orderList = catOrderPlacedDao.getListOfPlacedOrderToOrderDirector(odAccessoriesQuery);
		final Iterator<OrderModel> orderModelIterator = orderList.iterator();
		final String frieghtChargeCode = Config.getString("orderdirector.utv.freightchargecode", "default|N");
		final Map<String, String> frieghtChargeCodeMap = CatIntegrationUtils.getvaluesMap(frieghtChargeCode);
		CatIntegrationRetryOnExceptionStrategy catIntegrationRetryOnExceptionStrategy = null;

		if (orderList.isEmpty())
		{
			LOGGER.info("there are no orders to place");
			return;
		}

		while (orderModelIterator.hasNext())
		{
			OrderMachinesResponse orderMachinesResponse = null;

			catAbortableCronJob.checkIfRequestedToAbortCron(cronJob);
			InterfaceLogsModel interfaceLogsModel = null;
			final OrderModel order = orderModelIterator.next();
			final int retryCount = order.getRetryCount() != null ? order.getRetryCount() : 0;
			try
			{
				LOGGER.debug("Started creating order records for order number:" + order.getCode());

				interfaceLogsModel = createAuditLog(order);
				final AbstractOrderEntryModel orderEntry = order.getEntries().get(0);

				validateOrderAttributeValues(order, orderEntry);
				final List<String> orderRecords = createOrderRecords(order, orderEntry, frieghtChargeCodeMap);


				final CMOPSOrderingService cmopsOrderingService = catSoapConfigurationService.getSOAPClientService();
				final BindingProvider bindProv = (BindingProvider) cmopsOrderingService;
				final List<Handler> handlers = bindProv.getBinding().getHandlerChain();

				handlers.add(getCatServiceLogHandler());
				bindProv.getBinding().setHandlerChain(handlers);

				final OrderMachinesRequest orderMachinesRequest = new OrderMachinesRequest();
				catSoapGenerationService.getOrderRecordsTypes(orderRecords, orderMachinesRequest);

				interfaceLogsModel.setRequestData(catSoapGenerationService.createXmlRequest(orderMachinesRequest));

				final String RETRY = Config.getParameter("cat.retry.count.place.order");
				catIntegrationRetryOnExceptionStrategy = new CatIntegrationRetryOnExceptionStrategy(Integer.parseInt(RETRY),
						Long.parseLong(Config.getParameter("cat.retry.delay")));

				orderMachinesResponse = retryOrderRequests(catIntegrationRetryOnExceptionStrategy, interfaceLogsModel, order,
						cmopsOrderingService, orderMachinesRequest, retryCount);

			}
			catch (final InvalidAttributeValueException e)
			{
				LOGGER.error(ERROR_WHILE_CREATING_ORDER_RECORDS_FOR_ORDER + order.getCode(), e);
				logErrorsatus(interfaceLogsModel, order, e.getMessage(), retryCount);
			}
			catch (final IndexOutOfBoundsException e)
			{
				LOGGER.error(ERROR_WHILE_CREATING_ORDER_RECORDS_FOR_ORDER + order.getCode(), e);
				logErrorsatus(interfaceLogsModel, order, "Order doesn't have nay Product association: ", retryCount);
			}
			catch (final JAXBException jaxbException)
			{
				LOGGER.error(ERROR_WHILE_CREATING_ORDER_RECORDS_FOR_ORDER + order.getCode(), jaxbException);
				logErrorsatus(interfaceLogsModel, order, jaxbException.getMessage(), retryCount);
			}
			finally
			{
				if (orderMachinesResponse != null)
				{
					setOrderStatus(order, orderMachinesResponse);
				}
				modelService.save(interfaceLogsModel);
				modelService.save(order);
			}

		}
	}


	/**
	 * Sets the order status to submitted after successful submission to Order Director
	 *
	 * @param order
	 *           the order for which the response recived from Order Director
	 * @param orderMachinesResponse
	 *           the response recived for the order
	 */
	private void setOrderStatus(final OrderModel order, final OrderMachinesResponse orderMachinesResponse)
	{
		if ("P".equals(orderMachinesResponse.getOrderIndicator()))
		{
			order.setStatus(OrderStatus.SUBMITTED);
			setOrderMsoNumber(orderMachinesResponse, order);
		}
		else
		{
			int retryCount = order.getRetryCount() != null ? order.getRetryCount() : 0;
			order.setRetryCount(++retryCount);
			order.setStatus(OrderStatus.FAILED_SUBMISSION);
		}

	}

	/**
	 * The method sets the mso number to order after successful response from the Order Director
	 *
	 * @param orderMachinesResponse
	 *           the response from the order director
	 * @param orderModel
	 *           the order for which the response came
	 */
	private void setOrderMsoNumber(final OrderMachinesResponse orderMachinesResponse, final OrderModel orderModel)
	{
		final List<OrderResultsType> orderResultsTypes = orderMachinesResponse.getOrderResults();
		for (final OrderResultsType orderResultsType : orderResultsTypes)
		{
			final String orderMessage = orderResultsType.getOrderMessage().toString();//NOSONAR
			if (orderMessage != null && StringUtils.containsIgnoreCase(orderMessage, "MSO NUMBER"))
			{

				final String[] msoNumberString = orderMessage.split(":");
				final String[] msoNumber = msoNumberString[1].split(" ");
				orderModel.setMso(msoNumber[1]);
				modelService.save(orderModel);
				break;

			}
		}
	}



	/**
	 * The method adds the note records to orderRecords
	 *
	 * @param orderRecords
	 *           the list of orderRecords to be sent to order director
	 * @param catNoteRecordStringList
	 *           the list of note records to be added to order records
	 */

	private static void addNoteRecordsToOrderRecords(final List<String> orderRecords, final List<String> catNoteRecordStringList)
	{
		for (final String noterecord : catNoteRecordStringList)
		{
			orderRecords.add(noterecord);
		}
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validateOrderAttributeValues(final OrderModel orderModel, final AbstractOrderEntryModel orderEntry)
			throws InvalidAttributeValueException
	{
		StringBuilder exceptionValues = new StringBuilder();



		final String poNumber = orderModel.getPurchaseOrderNumber();
		final String dealerCode = orderModel.getUnit() == null ? "" : orderModel.getUnit().getUid().toString();//NOSONAR
		final String shipToID = orderModel.getDeliveryAddress() == null ? "" : orderModel.getDeliveryAddress().getSapCustomerID();

		exceptionValues = validate(poNumber, CatintegrationConstants.PURCHASER_ORDER_NUMBER_LENGTH,
				CatintegrationConstants.PONUMBER, exceptionValues);

		exceptionValues = validate(dealerCode, CatintegrationConstants.DEALER_CODE_LENGTH, CatintegrationConstants.DEALERCODE,
				exceptionValues);

		exceptionValues = validate(shipToID, CatintegrationConstants.SHIP_TO_CODE_LENGTH, CatintegrationConstants.SHIPTOID,
				exceptionValues);

		if (StringUtils.isNotBlank(exceptionValues.toString()))
		{
			throw new InvalidAttributeValueException(exceptionValues.toString());
		}
	}


	/**
	 *
	 * Create all the required order records to be sent to Order Director
	 *
	 * @param orderModel
	 *           the order to be sent to Order Director
	 * @param orderEntry
	 *           the order entry
	 * @param frieghtChargeCodeMap
	 * @return List<String> list of order records to be sent to Order Director
	 *
	 */
	private List<String> createOrderRecords(final OrderModel orderModel, final AbstractOrderEntryModel orderEntry, final Map<String, String> frieghtChargeCodeMap)
	{

		final List<String> orderRecords = new ArrayList<>();
		final CatOrderRecord catOrderRecord = catRecordCreationService.createOrderRecord(orderModel);
		final CatControlRecord catControlRecord = catRecordCreationService.createControlRecord(orderModel, orderEntry);
		final String catOrderRecordString = catRecordConversionService.convertOrderRecordToString(catOrderRecord);
		final String catControlRecordString = catRecordConversionService.convertControlRecordToString(catControlRecord,
				catOrderRecordString);

		orderRecords.add(catControlRecordString);

		final List<CatSpecificationRecord> specificationRecord = catRecordCreationService
				.createAccessorySpecificationrecords(orderModel);
		final List<String> catSpecificationRecordStringList = catRecordConversionService
				.convertSpecificationRecordsToString(specificationRecord, catOrderRecordString);
		addSpecificationRecordsToOrderRecords(orderRecords, catSpecificationRecordStringList);

		if (!orderModel.getB2bcomments().isEmpty())
		{
			final List<CatNoteRecord> noteRecord = catRecordCreationService.createNotesRecord(orderModel);
			final List<String> catNoteRecordStringList = catRecordConversionService.convertNoteRecordsToString(noteRecord,
					catOrderRecordString);
			addNoteRecordsToOrderRecords(orderRecords, catNoteRecordStringList);
		}

		final CatInlandShippingRecord catInlandShippingRecord = catRecordCreationService.createInlandShippingRecord(orderModel,
				frieghtChargeCodeMap);
		final String catInlandShippingRecordString  = catRecordConversionService.convertCatInlandShippingRecordString(catInlandShippingRecord,
				catOrderRecordString);

		if (StringUtils.isNotBlank(catInlandShippingRecordString))
		{
			orderRecords.add(catInlandShippingRecordString);
		}
		
		final CatTrailerRecord trailerRecord = catRecordCreationService.createTrailerRecord(orderModel);
		final String trailerRecordString = catRecordConversionService.convertTrailerRecordString(trailerRecord,
				catOrderRecordString);
		orderRecords.add(trailerRecordString);

		return orderRecords;
	}
}
