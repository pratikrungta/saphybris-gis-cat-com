package com.cat.bcp.core.integration.jobs.service.impl;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.integration.jobs.service.CatCSVFileService;

import au.com.bytecode.opencsv.CSVWriter;


public class CatCSVFileServiceImpl implements CatCSVFileService
{
	private static final Logger LOG = Logger.getLogger(CatCSVFileServiceImpl.class);

	/**
	 * {@inheritDoc}
	 */
	public void generateCsvFile(final CSVWriter csvWriter, final List<ArrayList<String>> sourceList)
	{
		LOG.info("DefaultFeedsDaoImpl.generateCsvFile() started");
		try
		{
			if (csvWriter != null)
			{
				for (final List<String> row : sourceList)
				{
					final String[] rowData = row.toArray(new String[row.size()]);
					csvWriter.writeNext(rowData);
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception is in DefaultFeedsDaoImpl.generateCsvFile(): ", e);
		}
		LOG.info("DefaultFeedsDaoImpl.generateCsvFile() ended");
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws IOException
	 *            throws read/writeexception
	 */
	public CSVWriter createSheet(final String filePath, final String delimitter) throws IOException
	{
		LOG.info("Starting CSVWriter instance creatino in file path " + filePath);
		CSVWriter csvWriter = null;
		final Writer writer = new OutputStreamWriter(new FileOutputStream(filePath), "UTF-8");
		if (StringUtils.isNotBlank(delimitter))
		{
			csvWriter = new CSVWriter(writer, delimitter.charAt(0), CSVWriter.NO_QUOTE_CHARACTER);
		}
		else
		{
			csvWriter = new CSVWriter(writer);
		}
		return csvWriter;
	}
}
