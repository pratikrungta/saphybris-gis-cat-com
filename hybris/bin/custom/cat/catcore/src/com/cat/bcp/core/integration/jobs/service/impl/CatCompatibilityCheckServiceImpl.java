/**
 *
 */
package com.cat.bcp.core.integration.jobs.service.impl;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.jalo.AbortCronJobException;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.Config;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.web.client.ResourceAccessException;

import com.cat.bcp.core.enums.CompatibilityStatusEnum;
import com.cat.bcp.core.event.CompatibilityCheckFailedEvent;
import com.cat.bcp.core.integration.constants.CatintegrationConstants;
import com.cat.bcp.core.integration.dao.CatFeedsDao;
import com.cat.bcp.core.integration.dao.CatOrderPlacedDao;
import com.cat.bcp.core.integration.jobs.CatAbstractAbortableCronJob;
import com.cat.bcp.core.integration.jobs.service.CatCompatibilityCheckService;
import com.cat.bcp.core.integration.jobs.service.CatRecordConversionService;
import com.cat.bcp.core.integration.jobs.service.CatRecordCreationService;
import com.cat.bcp.core.integration.jobs.service.CatSoapConfigurationService;
import com.cat.bcp.core.integration.jobs.service.CatSoapGenerationService;
import com.cat.bcp.core.model.CompatibilityInfoModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.core.integration.handler.CatCompatibilityCheckLogHandler;
import com.cat.core.integration.model.FeedAuditModel;
import com.cat.service.cmops.cmopscompatibilitycheckrequest.v1.CMOPSCompatibilityCheckRequest;
import com.cat.service.cmops.cmopscompatibilitycheckresponse.v1.CMOPSCompatibilityCheckResponse;
import com.cat.service.cmops.cmopscompatibilitycheckservice.v1.CheckCompatibility;
import com.cat.service.cmops.cmopscompatibilitycheckservice.v1.CheckCompatibilityCheckCompatibilityFaultMsg;
import com.cat.service.cmops.specsresults.v1.SpecsResults;
import com.sun.xml.internal.ws.fault.ServerSOAPFaultException;


/**
 * @author vkumarpendyam
 *
 */
public class CatCompatibilityCheckServiceImpl implements CatCompatibilityCheckService
{

	@Resource(name = "productConverter")
	private Converter<ProductModel, ProductData> productConverter;

	@Resource(name = "eventService")
	private EventService eventService;

	private static final Logger LOGGER = Logger.getLogger(CatCompatibilityCheckServiceImpl.class.getName());
	public static final String INDEXOUTOFBOUNDSEXCEPTION = "Error while creating Compatiblity Check Records ";
	public static final String JAXBEXCEPTION = "JAXB binding exception";
	public static final String COMPATIBILITYCHECKFAULTMSG = "Compatibility Check Fault Message";


	@Resource
	private CatRecordConversionService catRecordConversionService;

	@Resource
	private CatRecordCreationService catRecordCreationService;

	@Resource
	private CatOrderPlacedDao catOrderPlacedDao;

	@Resource
	ModelService modelService;

	@Resource
	ConfigurationService configurationService;

	@Resource
	CatCompatibilityCheckLogHandler catCompatibilityCheckLogHandler;

	@Resource
	private CatSoapConfigurationService catSoapConfigurationService;

	@Resource
	private CatSoapGenerationService catSoapGenerationService;

	@Resource
	private CatFeedsDao catFeedsDao;


	/**
	 * {@inheritDoc}
	 */

	@Override
	public void checkCompatibility(final CatAbstractAbortableCronJob catAbortableCronJob, final CronJobModel cronJob)
			throws AbortCronJobException, MalformedURLException, ResourceAccessException
	{
		final String compatibilitycheckQuery = Config.getParameter("order.compatibility.check.flexiQuery");

		final List<CompatibilityInfoModel> compatibilityInfoList = catOrderPlacedDao.getListOfConfigs(compatibilitycheckQuery);

		if (compatibilityInfoList.isEmpty())
		{
			LOGGER.info("there are no configs to check compatibility");
			return;
		}

		for (final CompatibilityInfoModel compatibilityInfoModel : compatibilityInfoList)
		{
			checkConfigCompatibility(compatibilityInfoModel);

		}
	}



	private void checkConfigCompatibility(final CompatibilityInfoModel compatibilityInfoModel)
			throws AbortCronJobException, MalformedURLException, ResourceAccessException
	{
		CMOPSCompatibilityCheckResponse cmopsCompatibilityCheckResponse = null;
		try
		{
			final CheckCompatibility checkCompatibility = catSoapConfigurationService.getCompatibilityCheckSOAPClientService();
			final BindingProvider bindProv = (BindingProvider) checkCompatibility;
			final List<Handler> handlers = bindProv.getBinding().getHandlerChain();

			handlers.add(catCompatibilityCheckLogHandler);
			bindProv.getBinding().setHandlerChain(handlers);

			final ConfigVariantProductModel configVariantProductModel = compatibilityInfoModel.getConfigVariantProduct();
			final CMOPSCompatibilityCheckRequest cmopsCompatibilityCheckRequest = new CMOPSCompatibilityCheckRequest();
			catSoapGenerationService.setCompatibityCheckValues(configVariantProductModel, cmopsCompatibilityCheckRequest,
					compatibilityInfoModel);

			cmopsCompatibilityCheckResponse = checkCompatibility.checkCompatibility(cmopsCompatibilityCheckRequest);
		}
		catch (final IndexOutOfBoundsException e)
		{
			LOGGER.error(INDEXOUTOFBOUNDSEXCEPTION, e);
		}
		catch (final CheckCompatibilityCheckCompatibilityFaultMsg e)
		{
			LOGGER.error(COMPATIBILITYCHECKFAULTMSG, e);
		}
		catch (final ServerSOAPFaultException e)
		{
			compatibilityInfoModel.setCompatibilityStatus(CompatibilityStatusEnum.LOADFAILED);
			LOGGER.error(e);
		}
		finally
		{
			if (cmopsCompatibilityCheckResponse != null)
			{
				setCompatibilityCheckStatus(compatibilityInfoModel, cmopsCompatibilityCheckResponse);
			}
		}
	}

	private void setCompatibilityCheckStatus(final CompatibilityInfoModel compatibilityInfoModel,
			final CMOPSCompatibilityCheckResponse cmopsCompatibilityCheckResponse)
	{
		final CompatibilityStatusEnum compatibilityStatusEnum = compatibilityInfoModel.getCompatibilityStatus();

		final List<SpecsResults> specResultsList = cmopsCompatibilityCheckResponse.getSpecsResults();
		final List<String> errorMessageList = new ArrayList<>();
		for (final SpecsResults specsResults : specResultsList)
		{
			errorMessageList.add(specsResults.getErrorMessage().trim());
		}

		boolean isCompatible = false;

		String compCheckMsg = null;

		compCheckMsg = errorMessageList.stream().map(Object::toString).collect(Collectors.joining(", "));


		if (compCheckMsg != null && compCheckMsg.contains(CatintegrationConstants.TRYLATER))
		{
			return;
		}
		if (compCheckMsg != null && compCheckMsg.contains(CatintegrationConstants.COMPATIBILITYPASSED))
		{
			isCompatible = true;
		}
		if (isCompatible)
		{
			compatibilityInfoModel.setCompatibilityStatus(CompatibilityStatusEnum.COMPATIBLE);
		}
		else
		{
			if (CompatibilityStatusEnum.COMPATIBLE.getCode().equalsIgnoreCase(compatibilityStatusEnum.getCode()))
			{
				compatibilityInfoModel.setCompatibilityStatus(CompatibilityStatusEnum.DISCONTINUE);
			}
			else
			{
				compatibilityInfoModel.setCompatibilityStatus(CompatibilityStatusEnum.LOADFAILED);
			}
		}
		modelService.save(compatibilityInfoModel);

	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void sentEmailForCompatibilityFailedCheckConfigIDs(final FeedAuditModel feedAuditModel)
	{

		final Date lastQuerySuccessfulTime = catFeedsDao.getLastSuccessfulRunTime(feedAuditModel);


		final String compatibilityCheckFailedConfigIDsQuery = Config
				.getParameter("order.compatibility.check.failed.config.flexiQuery");

		final List<ProductModel> failedConfigIDList = catOrderPlacedDao
				.getListOfCompatibilityCheckFailedConfigIDs(compatibilityCheckFailedConfigIDsQuery, lastQuerySuccessfulTime);

		if (CollectionUtils.isNotEmpty(failedConfigIDList))
		{
			LOGGER.debug("**** Calling Generate Failed Compatibility Email Event ****");
			eventService.publishEvent(new CompatibilityCheckFailedEvent(failedConfigIDList));
		}
		else
		{
			LOGGER.info("No failed compatible products found");
		}



	}


}
