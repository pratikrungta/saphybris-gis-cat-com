package com.cat.bcp.core.integration.jobs.service.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.jalo.AbortCronJobException;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.Config;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.management.InvalidAttributeValueException;
import javax.xml.bind.JAXBException;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.enums.FreightChargeCodeEnum;
import com.cat.bcp.core.integration.beans.CatControlRecord;
import com.cat.bcp.core.integration.beans.CatDupOrderRecord;
import com.cat.bcp.core.integration.beans.CatInlandShippingRecord;
import com.cat.bcp.core.integration.beans.CatInvoiceCriteriaRecord;
import com.cat.bcp.core.integration.beans.CatNoteRecord;
import com.cat.bcp.core.integration.beans.CatOrderRecord;
import com.cat.bcp.core.integration.beans.CatSpecificationRecord;
import com.cat.bcp.core.integration.beans.CatTrailerRecord;
import com.cat.bcp.core.integration.constants.CatintegrationConstants;
import com.cat.bcp.core.integration.dao.CatOrderPlacedDao;
import com.cat.bcp.core.integration.exception.CatIntegrationRetryOnExceptionStrategy;
import com.cat.bcp.core.integration.exception.CatODException;
import com.cat.bcp.core.integration.jobs.CatAbstractAbortableCronJob;
import com.cat.bcp.core.integration.jobs.service.CatDupOrderPlacedIntegrationService;
import com.cat.bcp.core.integration.jobs.service.CatRecordConversionService;
import com.cat.bcp.core.integration.jobs.service.CatRecordCreationService;
import com.cat.bcp.core.integration.jobs.service.CatSoapConfigurationService;
import com.cat.bcp.core.integration.jobs.service.CatSoapGenerationService;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.core.integration.CatIntegrationUtils;
import com.cat.core.integration.enums.InterfaceNameEnum;
import com.cat.core.integration.handler.CatServiceLogHandler;
import com.cat.core.integration.model.InterfaceLogsModel;
import com.cat.dice.cmops.slaes.v1.export1.CMOPSOrderingService;
import com.cat.dice.cmops.slaes.v1.export1.GetOrderMachinesGetOrderMachinesFaultMsg;
import com.cat.dice.cmops.slaes.v1.export1.OrderMachinesRequest;
import com.cat.dice.cmops.slaes.v1.export1.OrderMachinesResponse;
import com.cat.dice.cmops.slaes.v1.export1.OrderResultsType;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;


/**
 * The class is used to create the order records to be sent to Order Director.
 */
public class CatDupOrderPlacedIntegrationServiceImpl implements CatDupOrderPlacedIntegrationService
{

	private static final Logger LOGGER = Logger.getLogger(CatDupOrderPlacedIntegrationServiceImpl.class.getName());
	public static final String UTV = "utv";
	public static final String ERROR_WHILE_CREATING_ORDER_RECORDS_FOR_ORDER = "Error while creating order records for Order ";
	public static final String ZERO = "0";

	@Resource
	private CatRecordConversionService catRecordConversionService;

	@Resource
	private CatRecordCreationService catRecordCreationService;

	@Resource
	private CatOrderPlacedDao catOrderPlacedDao;

	@Resource
	ModelService modelService;

	@Resource
	ConfigurationService configurationService;

	@Resource
	CatServiceLogHandler catServiceLogHandler;

	@Resource
	private CatSoapConfigurationService catSoapConfigurationService;

	@Resource
	private CatSoapGenerationService catSoapGenerationService;

	private Map<String, String> truckLoadIDs;

	/**
	 * {@inheritDoc}
	 */

	@Override
	public void submitPlacedOrdersToOrderDirector(final CatAbstractAbortableCronJob catAbortableCronJob,
			final CronJobModel cronJob) throws AbortCronJobException, MalformedURLException, ResourceAccessException
	{
		final String odBCPUTVQuery = Config.getParameter("orderdirector.flexiQuery");
		final List<OrderModel> orderList = catOrderPlacedDao.getListOfPlacedOrderToOrderDirector(odBCPUTVQuery);
		final String paymentTerms = Config.getString("orderdirector.utv.paymentterms", "default|AZZW5");
		final Map<String, String> paymentTermsMap = CatIntegrationUtils.getvaluesMap(paymentTerms);
		final String frieghtChargeCode = Config.getString("orderdirector.utv.freightchargecode", "default|N");
		final Map<String, String> frieghtChargeCodeMap = CatIntegrationUtils.getvaluesMap(frieghtChargeCode);

		if (orderList.isEmpty())
		{
			LOGGER.info("there are no orders to place");
			return;
		}

		final Map<Object, List<OrderModel>> orderGroups = groupOrders(orderList);

		for (final List<OrderModel> orderModelGroupList : orderGroups.values())
		{

			final List<String> listOfPONumbersInOrder = createOrderMapWithPOs(orderModelGroupList);

			final int dupOrdersSplintCount = Config.getInt("orderdirector.duporders.splitcount", 24);
			final OrderModel order = orderModelGroupList.get(0);

			final List<List<String>> subSetsOfOrders = Lists.partition(listOfPONumbersInOrder, dupOrdersSplintCount);
			catAbortableCronJob.checkIfRequestedToAbortCron(cronJob);
			processOrdersForOD(subSetsOfOrders, order, frieghtChargeCodeMap, paymentTermsMap, orderModelGroupList);

		}
	}

	/**
	 * @param subSetsOfOrders
	 * @param orderModel
	 * @param frieghtChargeCodeMap
	 * @param paymentTermsMap
	 * @param orderModelGroupList
	 * @throws AbortCronJobException
	 * @throws MalformedURLException
	 */
	private void processOrdersForOD(final List<List<String>> subSetsOfOrders, final OrderModel orderModel,
			final Map<String, String> frieghtChargeCodeMap, final Map<String, String> paymentTermsMap,
			final List<OrderModel> orderModelGroupList) throws AbortCronJobException, MalformedURLException, ResourceAccessException
	{
		InterfaceLogsModel interfaceLogsModel = null;
		CatIntegrationRetryOnExceptionStrategy catIntegrationRetryOnExceptionStrategy = null;
		OrderMachinesResponse orderMachinesResponse = null;
		for (final List<String> listOfOrders : subSetsOfOrders)
		{
			final List<OrderModel> orderModelList = catOrderPlacedDao.getFirstOrderInDupOrders(listOfOrders.get(0), orderModel);
			final OrderModel order = orderModelList.get(0);
			final int retryCount = order.getRetryCount() != null ? order.getRetryCount() : 0;
			try
			{
				if (LOGGER.isDebugEnabled())
				{
					LOGGER.debug("Started creating order records for order number:" + order.getCode());
				}
				interfaceLogsModel = createAuditLog(order);
				final AbstractOrderEntryModel orderEntry = order.getEntries().get(0);

				validateOrderAttributeValues(order, orderEntry);
				final List<String> orderRecords = createOrderRecords(order, orderEntry, paymentTermsMap, frieghtChargeCodeMap,
						listOfOrders);

				final CMOPSOrderingService cmopsOrderingService = catSoapConfigurationService.getSOAPClientService();
				final BindingProvider bindProv = (BindingProvider) cmopsOrderingService;
				final List<Handler> handlers = bindProv.getBinding().getHandlerChain();

				handlers.add(catServiceLogHandler);
				bindProv.getBinding().setHandlerChain(handlers);

				final OrderMachinesRequest orderMachinesRequest = new OrderMachinesRequest();
				catSoapGenerationService.getOrderRecordsTypes(orderRecords, orderMachinesRequest);

				interfaceLogsModel.setRequestData(catSoapGenerationService.createXmlRequest(orderMachinesRequest));

				final String RETRY = Config.getParameter("cat.retry.count.place.order");
				catIntegrationRetryOnExceptionStrategy = new CatIntegrationRetryOnExceptionStrategy(Integer.parseInt(RETRY),
						Long.parseLong(Config.getParameter("cat.retry.delay")));

				orderMachinesResponse = retryOrderRequests(catIntegrationRetryOnExceptionStrategy, interfaceLogsModel, order,
						cmopsOrderingService, orderMachinesRequest, retryCount);

			}
			catch (final InvalidAttributeValueException e)
			{
				LOGGER.error(ERROR_WHILE_CREATING_ORDER_RECORDS_FOR_ORDER + order.getCode(), e);
				logErrorsatus(interfaceLogsModel, order, e.getMessage(), retryCount);
			}
			catch (final IndexOutOfBoundsException e)
			{
				LOGGER.error(ERROR_WHILE_CREATING_ORDER_RECORDS_FOR_ORDER + order.getCode(), e);
				logErrorsatus(interfaceLogsModel, order, "Order doesn't have nay Product association: ", retryCount);
			}
			catch (final JAXBException jaxbException)
			{
				LOGGER.error(ERROR_WHILE_CREATING_ORDER_RECORDS_FOR_ORDER + order.getCode(), jaxbException);
				logErrorsatus(interfaceLogsModel, order, jaxbException.getMessage(), retryCount);
			}
			finally
			{
				if (orderMachinesResponse != null)
				{
					setOrderStatus(orderMachinesResponse, orderModelGroupList, listOfOrders);
				}
				modelService.save(interfaceLogsModel);
			}
		}
	}

	/**
	 * @param catIntegrationRetryOnExceptionStrategy
	 * @param orderMachinesResponse
	 * @param interfaceLogsModel
	 * @param order
	 * @param cmopsOrderingService
	 * @param orderMachinesRequest
	 * @param retryCount
	 * @return OrderMachinesResponse
	 * @throws JAXBException
	 */
	protected OrderMachinesResponse retryOrderRequests(
			final CatIntegrationRetryOnExceptionStrategy catIntegrationRetryOnExceptionStrategy,
			final InterfaceLogsModel interfaceLogsModel, final OrderModel order, final CMOPSOrderingService cmopsOrderingService,
			final OrderMachinesRequest orderMachinesRequest, final int retryCount) throws JAXBException
	{
		OrderMachinesResponse orderMachinesResponse = null;
		while (catIntegrationRetryOnExceptionStrategy.shouldRetry())
		{
			try
			{
				orderMachinesResponse = cmopsOrderingService.getOrderMachines(orderMachinesRequest);
				interfaceLogsModel.setResponseData(catSoapGenerationService.createReXmlResponse(orderMachinesResponse));
				interfaceLogsModel.setStatus(CatintegrationConstants.SUCCESS);
				break;
			}
			catch (final GetOrderMachinesGetOrderMachinesFaultMsg getOrderMachinesGetOrderMachinesFaultMsg)
			{
				LOGGER.error("Exception while calling request", getOrderMachinesGetOrderMachinesFaultMsg);
				logErrorOnConnectionIssues(catIntegrationRetryOnExceptionStrategy, interfaceLogsModel, order, retryCount);
				
			}
			catch (final HttpClientErrorException e)
			{
				logErrorOnConnectionIssues(catIntegrationRetryOnExceptionStrategy, interfaceLogsModel, order, retryCount);
			}
		}
		return orderMachinesResponse;
	}

	/**
	 * Used to log exception when there is an connection timeout issues.
	 *
	 * @param catIntegrationRetryOnExceptionStrategy
	 *           the strategy to be used for retrying on connection timeout issues
	 * @param interfaceLogsModel
	 *           the audit log model to be created for the order
	 * @param order
	 *           the current order in execution
	 * @param httpClientErrorException
	 *           the exception for the connection timeout
	 */
	private void logErrorOnConnectionIssues(final CatIntegrationRetryOnExceptionStrategy catIntegrationRetryOnExceptionStrategy,
			final InterfaceLogsModel interfaceLogsModel, final OrderModel order,
			 final int retryCount)
	{
		try
		{
			LOGGER.error("Connection issues from CMOPS");
			catIntegrationRetryOnExceptionStrategy.errorOccured(CatintegrationConstants.FAILURE,
					"Exception from CMOPS");
		}
		catch (final CatODException catODException)
		{
			LOGGER.error(ERROR_WHILE_CREATING_ORDER_RECORDS_FOR_ORDER + order.getCode(), catODException);

			logErrorsatus(interfaceLogsModel, order, catODException.getCatErrorMessage(), retryCount);
		}
	}

	/**
	 * Sets the order status to submitted after successful submission to Order Director
	 *
	 * @param order
	 *           the order for which the response recived from Order Director
	 * @param orderMachinesResponse
	 *           the response recived for the order
	 */
	protected void setOrderStatus(final OrderMachinesResponse orderMachinesResponse, final List<OrderModel> orderModelGroupList,
			final List<String> listOfOrders)
	{
		if (String.valueOf(CatintegrationConstants.ORDERDIRECTOR_ORDERINDICATOR).equals(orderMachinesResponse.getOrderIndicator()))
		{
			setOrderMsoNumber(orderMachinesResponse, orderModelGroupList);
		}
		else
		{
			setFailedOrderStatus(orderModelGroupList, listOfOrders);
		}
	}

	/**
	 * The method sets the mso number to order after successful response from the Order Director
	 *
	 * @param orderMachinesResponse
	 *           the response from the order director
	 * @param orderModel
	 *           the order for which the response came
	 */
	private void setFailedOrderStatus(final List<OrderModel> orderModelGroupList, final List<String> listOfOrders)
	{
		for (final String poNumber : listOfOrders)
		{
			for (final OrderModel orderModel : orderModelGroupList)
			{
				if (orderModel.getPurchaseOrderNumber().equalsIgnoreCase(poNumber))
				{
					int retryCount = orderModel.getRetryCount() != null ? orderModel.getRetryCount() : 0;
					orderModel.setRetryCount(++retryCount);
					orderModel.setStatus(OrderStatus.FAILED_SUBMISSION);
					modelService.save(orderModel);
				}
			}
		}
	}

	/**
	 * The method sets the mso number to order after successful response from the Order Director
	 *
	 * @param orderMachinesResponse
	 *           the response from the order director
	 * @param orderModel
	 *           the order for which the response came
	 */
	private void setOrderMsoNumber(final OrderMachinesResponse orderMachinesResponse, final List<OrderModel> orderList)
	{
		final List<OrderResultsType> orderResultsTypes = orderMachinesResponse.getOrderResults();

		final Map<String, String> msoPOMap = new HashMap<>();

		for (final OrderResultsType orderResultsType : orderResultsTypes)
		{
			final String orderMessage = String.valueOf(orderResultsType.getOrderMessage());

			if (orderMessage != null && StringUtils.containsIgnoreCase(orderMessage, "MSO NUMBER"))
			{

				final String[] msoNumberString = orderMessage.split("MSO NUMBER:");
				final String[] msoNumber = msoNumberString[1].split(" ");

				final String[] poNumberString = orderMessage.split("ORDER NUMBER:");
				final String[] poNumber = poNumberString[1].split(" ");
				msoPOMap.put(poNumber[1].trim(), msoNumber[1].trim());
			}
		}

		if (msoPOMap.size() >= 1)
		{
			msoPOMap.forEach((poNumber, msoNumber) -> {
				for (final OrderModel orderModel : orderList)
				{
					if (orderModel.getPurchaseOrderNumber().equalsIgnoreCase(poNumber))
					{
						orderModel.setMso(msoNumber);
						orderModel.setStatus(OrderStatus.SUBMITTED);
					}
				}
			});
		}
		else
		{
			for (final OrderModel orderModel : orderList)
			{
				orderModel.setStatus(OrderStatus.SUBMITTED);
			}
		}
		modelService.saveAll(orderList);
	}

	/**
	 * The method adds the specification records to orderRecords
	 *
	 * @param orderRecords
	 *           the list of orderRecords to be sent to order director
	 * @param catSpecificationRecordStringList
	 *           the list of specification records to be added to order records
	 */

	protected static void addSpecificationRecordsToOrderRecords(final List<String> orderRecords,
			final List<String> catSpecificationRecordStringList)
	{
		for (final String specificationrecord : catSpecificationRecordStringList)
		{
			orderRecords.add(specificationrecord);
		}
	}

	/**
	 * Creates the inland shipping record as String
	 *
	 * @param order
	 *           the order for which the record needs to be created
	 * @param catOrderRecordString
	 *           the string to be added to the catinlandShippingRecord
	 * @param frieghtChargeCodeMap
	 * @return inlandShippingRecord(N4) in String format.
	 */

	protected String createInlandShippingRecordString(final OrderModel order, final String catOrderRecordString,
			final Map<String, String> frieghtChargeCodeMap)
	{

		String catInlandShippingRecordString = null;

		final CatInlandShippingRecord catInlandShippingRecord = catRecordCreationService.createInlandShippingRecord(order,
				frieghtChargeCodeMap);
		catInlandShippingRecordString = catRecordConversionService.convertCatInlandShippingRecordString(catInlandShippingRecord,
				catOrderRecordString);

		return catInlandShippingRecordString;

	}

	/**
	 * Creates the freight charge code records for BCP products
	 *
	 * @param order
	 *           the order for which the record needs to be created
	 * @param catOrderRecordString
	 *           the string to be added to the catinlandShippingRecord
	 * @param salesModel
	 *           saleModel used to get the freight codes
	 * @return inlandShippingRecord(N4) in String format.
	 */

	private String createFreightCodesForBcpProducts(final OrderModel order, final ProductModel salesModel,
			final String catOrderRecordString)
	{

		final CatInlandShippingRecord catInlandShippingRecord = catRecordCreationService.createFreightCodesRecord(order,
				salesModel);
		final String catInlandShippingRecordString = catRecordConversionService
				.convertCatInlandShippingRecordString(catInlandShippingRecord,
				catOrderRecordString);

		return catInlandShippingRecordString;

	}

	/**
	 * Creates the invoice criteria record as String *
	 *
	 * @param order
	 *           the order for which the record needs to be created
	 * @param catOrderRecordString
	 *           the string to be added to the catinlandShippingRecord
	 * @param paymentTermsMap
	 * @return catInvoiceCriteriaRecord(N4) in String format.
	 */
	protected String createInvoiceCriteriarecordString(final OrderModel orderModel, final String catOrderRecordString,
			final Map<String, String> paymentTermsMap)
	{

		final CatInvoiceCriteriaRecord catInvoiceCriteriaRecord = catRecordCreationService.createInvoiceCriteriaRecord(orderModel,
				paymentTermsMap);
		return catRecordConversionService.convertCatInvoiceCriteriarecordString(catInvoiceCriteriaRecord, catOrderRecordString);

	}

	/**
	 * Creates the N5 record for sales model
	 *
	 * @param orderEntry
	 * @param catOrderRecordString
	 * @return paymentTermrecord(N5) in String format.
	 */
	private String createPaymentTermForSalesModel(final AbstractOrderEntryModel orderEntry, final String catOrderRecordString)
	{

		final CatInvoiceCriteriaRecord catInvoiceCriteriaRecord = catRecordCreationService
				.createInvoiceRecordForSalesModel(orderEntry);
		return catRecordConversionService.convertCatInvoiceCriteriarecordString(catInvoiceCriteriaRecord, catOrderRecordString);

	}

	/**
	 * The method creates the generic attributes of the log model before saving the request and response objects.
	 *
	 * @param orderModel
	 *           the order for which the audit logs needs to be created
	 * @return the log model
	 */
	protected InterfaceLogsModel createAuditLog(final OrderModel orderModel)
	{
		final InterfaceLogsModel catInterfaceLogModel = modelService.create(InterfaceLogsModel.class);
		catInterfaceLogModel.setRequestDateTime(new Date());
		catInterfaceLogModel.setOrderNbr(orderModel.getCode());
		catInterfaceLogModel.setResponseDateTime(new Date());
		catInterfaceLogModel
				.setRequestUrl(configurationService.getConfiguration().getString("orderdirector.webservice.endpoint.url"));
		catInterfaceLogModel.setSystem(InterfaceNameEnum.ORDERDIRECTOR);

		return catInterfaceLogModel;
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public void validateOrderAttributeValues(final OrderModel orderModel, final AbstractOrderEntryModel orderEntry)
			throws InvalidAttributeValueException
	{
		StringBuilder exceptionValues = new StringBuilder();

		final ProductModel salesModel = CatIntegrationUtils.getProduct(orderEntry);
		final ConfigVariantProductModel configVariantProductModel = (ConfigVariantProductModel) orderEntry.getProduct();
		final BaseVariantProductModel baseVariantProductModel = (BaseVariantProductModel) configVariantProductModel
				.getBaseProduct();

		final String poNumber = orderModel.getPurchaseOrderNumber();
		final String dealerCode = orderModel.getUnit() == null ? "" : orderModel.getUnit().getUid();
		final String shipToID = orderModel.getDeliveryAddress() == null ? "" : orderModel.getDeliveryAddress().getSapCustomerID();
		final String salesCode = salesModel.getCode();
		final String baseVariant = baseVariantProductModel.getName();
		final String baseVariantCode = baseVariantProductModel.getCode();

		exceptionValues = validate(poNumber, CatintegrationConstants.PURCHASER_ORDER_NUMBER_LENGTH,
				CatintegrationConstants.PONUMBER, exceptionValues);

		exceptionValues = validate(dealerCode, CatintegrationConstants.DEALER_CODE_LENGTH, CatintegrationConstants.DEALERCODE,
				exceptionValues);

		exceptionValues = validate(shipToID, CatintegrationConstants.SHIP_TO_CODE_LENGTH, CatintegrationConstants.SHIPTOID,
				exceptionValues);

		exceptionValues = validate(salesCode, CatintegrationConstants.SALES_CODE_LENGTH, CatintegrationConstants.SALESCODE,
				exceptionValues);

		exceptionValues = validate(baseVariant, CatintegrationConstants.BASEVARIANT_NAME_LENGTH,
				CatintegrationConstants.BASEVARIANT, exceptionValues);

		exceptionValues = validate(baseVariantCode, CatintegrationConstants.BASEVARIANT_CODE_LENGTH,
				CatintegrationConstants.BASEVARIANTCODE, exceptionValues);
		if (StringUtils.isNotBlank(exceptionValues.toString()))
		{
			throw new InvalidAttributeValueException(exceptionValues.toString());
		}
	}

	/**
	 *
	 * The method validates whether the attribute of an order is an having expected value length.
	 *
	 * @param value
	 *           the attribute value to be validated
	 * @param length
	 *           the max length of the attribute accepted by the Order Director
	 * @param entryName
	 *           the attribute name of the order to be validated
	 * @param exceptionValues
	 *           the exception message
	 * @return StringBuilder String containing the exception messages
	 */

	protected StringBuilder validate(final String value, final int length, final String entryName,
			final StringBuilder exceptionValues)
	{
		try
		{
			Preconditions.checkNotNull(value, entryName + " should'nt be null: ");
			Preconditions.checkArgument(value.length() <= length, "\n" + entryName + " should not be > " + length + ".");
			if (StringUtils.isEmpty(value))
			{
				exceptionValues.append("\n" + entryName + " should'nt be empty. ");
			}
		}
		catch (final NullPointerException e)//NOSONAR
		{
			LOGGER.error("Error while validating order attribute " + entryName, e);
			exceptionValues.append(e.getMessage());
		}
		catch (final IllegalArgumentException e)
		{
			LOGGER.error("Illegal arguments supplied", e);
			exceptionValues.append(e.getMessage());
		}
		return exceptionValues;
	}

	/**
	 *
	 * Create all the required order records to be sent to Order Director
	 *
	 * @param orderModel
	 *           the order to be sent to Order Director
	 * @param orderEntry
	 *           the order entry
	 * @param paymentTermsMap
	 * @param frieghtChargeCodeMap
	 * @return List<String> list of order records to be sent to Order Director
	 *
	 */
	private List<String> createOrderRecords(final OrderModel orderModel, final AbstractOrderEntryModel orderEntry,
			final Map<String, String> paymentTermsMap, final Map<String, String> frieghtChargeCodeMap,
			final List<String> listOfOrders) throws ResourceAccessException
	{

		final List<String> orderRecords = new ArrayList<>();
		final CatOrderRecord catOrderRecord = catRecordCreationService.createOrderRecord(orderModel);
		final CatControlRecord catControlRecord = catRecordCreationService.createControlRecord(orderModel, orderEntry,
				listOfOrders);
		final String catOrderRecordString = catRecordConversionService.convertOrderRecordToString(catOrderRecord);
		final String catControlRecordString = catRecordConversionService.convertControlRecordToString(catControlRecord,
				catOrderRecordString);
		boolean isUTVCategory = false;

		orderRecords.add(catControlRecordString);

		// Need to write logic to create the list of the specification
		// records based on the primary offering DC1 value.
		final List<CatSpecificationRecord> specificationRecord = catRecordCreationService.createSpecificationrecord(orderModel,
				orderEntry);
		final List<String> catSpecificationRecordStringList = catRecordConversionService
				.convertSpecificationRecordsToString(specificationRecord, catOrderRecordString);
		addSpecificationRecordsToOrderRecords(orderRecords, catSpecificationRecordStringList);



		if (listOfOrders.size() > 1)
		{
			final int countOfdupOrdersInN3 = Config.getInt("orderdirector.duporders.splitcount.inn3record", 3);
			final List<String> ordersExcludingFirsOrder = listOfOrders.subList(1, listOfOrders.size());
			final List<List<String>> subSetsOfPONumbers = Lists.partition(ordersExcludingFirsOrder, countOfdupOrdersInN3);

			final List<CatDupOrderRecord> catDupOrderRecord = catRecordCreationService.createDupOrderRecord(subSetsOfPONumbers);
			final List<String> catDupOrdersRecordStringList = catRecordConversionService.convertDupRecordsToString(catDupOrderRecord,
					catOrderRecordString);
			addSpecificationRecordsToOrderRecords(orderRecords, catDupOrdersRecordStringList);
		}

		final List<CatNoteRecord> noteRecord = catRecordCreationService.createNotesWithTruckloadRecord(orderModel, truckLoadIDs);
		if (!noteRecord.isEmpty())
		{
			final List<String> catNoteRecordStringList = catRecordConversionService.convertNoteRecordsToString(noteRecord,
					catOrderRecordString);
			addSpecificationRecordsToOrderRecords(orderRecords, catNoteRecordStringList);
		}


		final Collection<CategoryModel> categories = getSuperCategories(orderEntry);
		for (final CategoryModel category : categories)
		{
			if (category.getCode().equalsIgnoreCase(CatCoreConstants.UTV))
			{
				isUTVCategory = true;
			}
		}

		if (isUTVCategory)
		{
			final String catInlandShippingRecordString = createInlandShippingRecordString(orderModel, catOrderRecordString,
					frieghtChargeCodeMap);
			if (StringUtils.isNotBlank(catInlandShippingRecordString))
			{
				orderRecords.add(catInlandShippingRecordString);
			}
			final String catInvoiceCriteriaRecordString = createInvoiceCriteriarecordString(orderModel, catOrderRecordString,
					paymentTermsMap);
			orderRecords.add(catInvoiceCriteriaRecordString);
		}
		else
		{
			final ProductModel salesModel = CatIntegrationUtils.getProduct(orderEntry);
			final FreightChargeCodeEnum freightChargeCodeEnum = salesModel.getFreightChargeCode();
			if (null != freightChargeCodeEnum)
			{
				final String freightChargeCodeString = createFreightCodesForBcpProducts(orderModel, salesModel, catOrderRecordString);
				orderRecords.add(freightChargeCodeString);
			}
			
			if (!StringUtils.isEmpty(salesModel.getPaymentTerm()))
			{
				final String catInlandShippingRecordString = createPaymentTermForSalesModel(orderEntry, catOrderRecordString);
				orderRecords.add(catInlandShippingRecordString);
			}
		}

		final CatTrailerRecord trailerRecord = catRecordCreationService.createTrailerRecord(orderModel);
		final String trailerRecordString = catRecordConversionService.convertTrailerRecordString(trailerRecord,
				catOrderRecordString);
		orderRecords.add(trailerRecordString);

		return orderRecords;
	}

	/**
	 * Used to save exception messages corresponding to the order being validated.
	 *
	 * @param interfaceLogsModel
	 *           the log model to be saved to the database
	 * @param orderModel
	 *           the order for which the logs needs to be saved
	 * @param exceptionMsg
	 *           the exception message which will be saved in case of an exception while submitting order to Order
	 *           Director
	 */
	protected void logErrorsatus(final InterfaceLogsModel interfaceLogsModel, final OrderModel orderModel, final String exceptionMsg,
			int retryCount)
	{
		orderModel.setStatus(OrderStatus.FAILED_SUBMISSION);
		orderModel.setRetryCount(++retryCount);
		interfaceLogsModel.setStatus(CatintegrationConstants.FAILURE);
		interfaceLogsModel.setException(exceptionMsg);
	}

	private Map<Object, List<OrderModel>> groupOrders(final List<OrderModel> orderList)
	{
		final List<OrderModel> listOfOrdersWithoutGroupId = orderList.stream().filter(obj -> obj.getOrderGroupId() == null)
				.collect(Collectors.toList());
		final List<OrderModel> listOfOrdersWithGroupId = orderList.stream().filter(obj -> obj.getOrderGroupId() != null)
				.collect(Collectors.toList());
		final Map<Object, List<OrderModel>> orderODGroups = new HashMap<>();
		truckLoadIDs = new HashMap<>();

		for (final OrderModel orderModel : listOfOrdersWithoutGroupId)
		{
			final List<OrderModel> orderModelList = new ArrayList<>();
			orderModelList.add(orderModel);
			orderODGroups.put(orderModel.getPurchaseOrderNumber(), orderModelList);
		}

		final Map<String, List<OrderModel>> groupByOrderGroupId = listOfOrdersWithGroupId.stream()
				.collect(Collectors.groupingBy(obj -> obj.getOrderGroupId()));
		for (final List<OrderModel> orderGroups : groupByOrderGroupId.values())
		{
			final OrderModel orderModel = orderGroups.get(0);
			final Collection<CategoryModel> categories = getSuperCategories(orderModel.getEntries().get(0));
			groupingForUTVOrBCPOrders(categories, orderODGroups, orderGroups, truckLoadIDs);
		}
		return orderODGroups;

	}

	private List<String> createOrderMapWithPOs(final List<OrderModel> orderGroupList)
	{
		final List<String> poNumbers = new ArrayList<>();
		for (final OrderModel orderModel : orderGroupList)
		{
			poNumbers.add(orderModel.getPurchaseOrderNumber());
		}
		return poNumbers;
	}

	private void groupingForUTVOrBCPOrders(final Collection<CategoryModel> categories,
			final Map<Object, List<OrderModel>> orderODGroups, final List<OrderModel> orderGroups,
			final Map<String, String> truckLoadIDs)
	{
		boolean isUTVCategory = false;
		int truckloadId = 1;
		for (final CategoryModel category : categories)
		{
			if (category.getCode().equalsIgnoreCase(CatCoreConstants.UTV))
			{
				isUTVCategory = true;
			}
		}
		if (isUTVCategory)
		{
			final List<OrderModel> listOfOrdersWithTruckLoadId = orderGroups.stream().filter(obj -> obj.getTruckloadId() != null)
					.collect(Collectors.toList());

			final Map<Object, List<OrderModel>> groupsByTruckLoadID = listOfOrdersWithTruckLoadId.stream()
					.collect(Collectors.groupingBy(obj -> obj.getTruckloadId()));

			for (final Object key : groupsByTruckLoadID.keySet())
			{
				truckLoadIDs.put(key.toString(), ZERO + String.valueOf(truckloadId++));
			}
			for (final List<OrderModel> orderGroupsOnTruckLoadID : groupsByTruckLoadID.values())
			{

				final Map<Object, List<OrderModel>> groupsByProductCode = orderGroupsOnTruckLoadID.stream()
						.collect(
								Collectors.groupingBy(obj -> obj.getOrderGroupId() + obj.getTruckloadId()
										+ obj.getEntries().get(0).getProduct().getCode()));
				orderODGroups.putAll(groupsByProductCode);

			}
		}
		else
		{
			orderODGroups.putAll(orderGroups.stream()
					.collect(Collectors.groupingBy(obj -> obj.getOrderGroupId() + obj.getEntries().get(0).getProduct().getCode())));
		}

	}

	private List<CategoryModel> getSuperCategories(final AbstractOrderEntryModel orderEntry)
	{
		final ProductModel salesModel = CatIntegrationUtils.getProduct(orderEntry);
		final Collection<CategoryModel> categories = salesModel.getSupercategories();
		return (List<CategoryModel>) categories;
	}
}
