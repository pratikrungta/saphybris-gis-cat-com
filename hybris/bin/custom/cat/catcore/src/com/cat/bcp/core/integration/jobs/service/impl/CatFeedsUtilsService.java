/**
 *
 */
package com.cat.bcp.core.integration.jobs.service.impl;

import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.Config;

import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.lang3.time.DateUtils;

import com.cat.bcp.core.integration.constants.CatintegrationConstants;
import com.cat.core.integration.enums.FeedProcessStepEnum;
import com.cat.core.integration.model.FeedAuditLogModel;
import com.cat.core.integration.model.FeedAuditModel;


/**
 * @author vkumarpendyam
 *
 */

public class CatFeedsUtilsService
{

	public static final String QUERYENDTIME = "23_30";

	@Resource
	private ModelService modelService;

	/**
	 * addAuditLogEntry adds the process step in the FeedAudit
	 *
	 * @param feedAuditModel
	 *           reference of feedAudit
	 * @param feedProcessStepEnum
	 *           enum of process step
	 */
	public void addAuditLogEntry(final FeedAuditModel feedAuditModel, final String feedProcessStepEnum) //NOSONAR
	{
		final FeedAuditLogModel feedAuditLogModel = new FeedAuditLogModel();

		switch (feedProcessStepEnum)
		{
			case CatintegrationConstants.CRONJOB_EXECUTION_STARTED:
				feedAuditLogModel.setProcessStep(FeedProcessStepEnum.CRONJOB_EXECUTION_STARTED);
				break;
			case CatintegrationConstants.QUERY_EXECUTION_STARTED:
				feedAuditLogModel.setProcessStep(FeedProcessStepEnum.QUERY_EXECUTION_STARTED);
				break;
			case CatintegrationConstants.FILE_GENERATION_STARTED:
				feedAuditLogModel.setProcessStep(FeedProcessStepEnum.FILE_GENERATION_STARTED);
				break;
			case CatintegrationConstants.QUERY_EXECUTION_COMPLETED:
				feedAuditLogModel.setProcessStep(FeedProcessStepEnum.QUERY_EXECUTION_COMPLETED);
				break;
			case CatintegrationConstants.FILE_GENERATION_ENDED:
				feedAuditLogModel.setProcessStep(FeedProcessStepEnum.FILE_GENERATION_ENDED);
				break;
			case CatintegrationConstants.FILE_TRANSFER_STARTED:
				feedAuditLogModel.setProcessStep(FeedProcessStepEnum.FILE_TRANSFER_STARTED);
				break;
			case CatintegrationConstants.FILE_TRANSFER_COMPLETED_WITHSUCCESS:
				feedAuditLogModel.setProcessStep(FeedProcessStepEnum.FILE_TRANSFER_COMPLETED_WITHSUCCESS);
				break;
			case CatintegrationConstants.FILE_TRANSFER_COMPLETED_WITHFAILURE:
				feedAuditLogModel.setProcessStep(FeedProcessStepEnum.FILE_TRANSFER_COMPLETED_WITHFAILURE);
				break;
			case CatintegrationConstants.CRONJOB_EXECUTION_COMPLETED:
				feedAuditLogModel.setProcessStep(FeedProcessStepEnum.CRONJOB_EXECUTION_COMPLETED);
				break;
			default:
				break;
		}

		feedAuditLogModel.setFeedAudit(feedAuditModel);
		modelService.save(feedAuditLogModel);
	}

	/**
	 * getDailyEndTriggerTime returns the query execution end time to pick the records
	 *
	 * @param feedName
	 *           interface name
	 * @return Date returns the query end time
	 */
	public Date getDailyEndTriggerTime(final String feedName)
	{
		// Current day time concat with time from property
		final String dailyFeedTriggerTime = Config.getString("outboundDataFeed." + feedName + ".feedtime.HH_MM", QUERYENDTIME);
		Date dailyTriggerTime = DateUtils.setHours(new Date(), Integer.valueOf(dailyFeedTriggerTime.split("_")[0]));
		dailyTriggerTime = DateUtils.setMinutes(dailyTriggerTime, Integer.valueOf(dailyFeedTriggerTime.split("_")[1]));
		dailyTriggerTime = DateUtils.setSeconds(dailyTriggerTime, 0);
		dailyTriggerTime = DateUtils.setMilliseconds(dailyTriggerTime, 0);
		return dailyTriggerTime;
	}

}
