/**
 *
 */
package com.cat.bcp.core.integration.jobs.service.impl;

import de.hybris.platform.commercefacades.order.data.CatReserveQuoteData;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.exception.CatEDHRecordNotFoundException;
import com.cat.bcp.core.exception.CatException;
import com.cat.bcp.core.integration.jobs.service.CatIntegrationService;
import com.cat.bcp.core.model.InventoryReservationModel;
import com.cat.bcp.core.servicelayer.DealerAddressService;
import com.cat.core.integration.edh.CatEDHInventoryDetailsDataList;
import com.cat.core.integration.edh.details.response.EDHInventoryDetailsResponse;
import com.cat.core.integration.edh.response.Address;
import com.cat.core.integration.edh.response.Child;
import com.cat.core.integration.edh.response.EDHInventoryResponse;
import com.cat.core.integration.exception.CatEDHException;
import com.cat.core.integration.exception.CatRecordNotFoundException;
import com.cat.core.integration.service.CatEDHConfiguratorService;
import com.cat.core.integration.utils.CatRestClientUtil;


/**
 * The Service Impl class for a EDH Implementation.
 *
 * @author ravjonnalagadda
 */
public class CatIntegrationServiceImpl implements CatIntegrationService
{
	private static final Logger LOG = Logger.getLogger(CatIntegrationServiceImpl.class);
	private static final String UNEXPECTED_EXCEPTION = "Unexpected Exception :";
	private static final String NO_RECORDS_FOUND = "No Records Found";
	public static final String HYBRIS_DATA_ENABLE = "cat.ws.edh.data.hybris.enable";
	public static final String YES = "Yes";
	@Resource(name = "catEDHConfiguratorService")
	CatEDHConfiguratorService catEDHConfiguratorService;
	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	DealerAddressService dealerAddressService;



	/**
	 * {@inheritDoc}
	 *
	 */
	@Override
	public String getInventoryDetails(final CatEDHInventoryDetailsDataList inventoryDetailsDataList) throws CatException
	{
		String inventoryDetails = null;
		try
		{
			inventoryDetails = catEDHConfiguratorService.getInventoryDetails(inventoryDetailsDataList);
		}
		catch (final CatEDHException e)
		{
			processException(e);
		}
		return inventoryDetails;
	}

	/**
	 * {@inheritDoc}
	 *
	 */
	@Override
	public CatReserveQuoteData getSerialNumbersData(final String productCode, final String dealerCode) throws CatException
	{
		CatReserveQuoteData catReserveQuoteData = new CatReserveQuoteData();
		if (StringUtils.isNotBlank(productCode) && StringUtils.isNotBlank(dealerCode))
		{
			final ArrayList<String> partyIdList = new ArrayList<>();
			final ArrayList<String> configIdList = new ArrayList<>();
			partyIdList.add(dealerCode);
			configIdList.add(productCode);
			final CatEDHInventoryDetailsDataList inventoryDeatilsData = new CatEDHInventoryDetailsDataList();
			inventoryDeatilsData.setPartyIdList(partyIdList);
			inventoryDeatilsData.setConfigIdList(configIdList);
			String inventoryDetails = null;
			try
			{
				inventoryDetails = catEDHConfiguratorService.getInventoryDetails(inventoryDeatilsData);
				if (StringUtils.isNotBlank(inventoryDetails))
				{
					catReserveQuoteData = getInventoryReserveDetails(inventoryDetails);
				}
			}
			catch (final CatEDHException e)
			{
				processException(e);
			}
		}
		return catReserveQuoteData;
	}

	/**
	 * This method is to update the JSON response with reserved attribute.
	 *
	 * @param inventoryDetails
	 *           : inventory details response
	 * @return : JSON response
	 * @throws CatEDHRecordNotFoundException
	 */
	private CatReserveQuoteData getInventoryReserveDetails(final String inventoryDetails) throws CatEDHRecordNotFoundException
	{
		final EDHInventoryDetailsResponse[] inventoryDetailsData = this.getInventoryDetailsData(inventoryDetails);
		if (null != inventoryDetailsData && null == inventoryDetailsData[0].getEdhError())
		{
			return getReservedData(inventoryDetailsData);
		}
		return null;
	}

	/**
	 * This method is to get reserve data for serialNumbers
	 *
	 * @param inventoryDetailRequest
	 *           : actual EDH details Response
	 * @return : return response
	 */
	private CatReserveQuoteData getReservedData(final EDHInventoryDetailsResponse[] inventoryDetailRequest)
	{
		final List<String> serialNumbersList = new ArrayList();
		final CatReserveQuoteData catReserveQuoteDataDetails = new CatReserveQuoteData();
		for (int reserveCount = 0; reserveCount < inventoryDetailRequest.length; reserveCount++)
		{
			setReservedAttribute(inventoryDetailRequest, reserveCount);
			if ("false".equalsIgnoreCase(inventoryDetailRequest[reserveCount].getReserved()))
			{
				serialNumbersList.add(inventoryDetailRequest[reserveCount].getSerialNumber());
			}
		}
		catReserveQuoteDataDetails.setSerialNumbers(serialNumbersList);
		return catReserveQuoteDataDetails;
	}

	/**
	 * This Method is to add / update the reserve flag in JSON Response.
	 *
	 * @param inventoryDetailRequest
	 *           : JSON Response
	 * @param loopValue
	 *           : iterator for InventoryDetails List.
	 * @return : list of JSON inventory response.
	 */
	private EDHInventoryDetailsResponse[] setReservedAttribute(final EDHInventoryDetailsResponse[] inventoryDetailRequest,
			final int loopValue)
	{
		final InventoryReservationModel inventoryReservation = modelService.create(InventoryReservationModel.class);
		try
		{
			inventoryReservation.setSerialNumber(inventoryDetailRequest[loopValue].getSerialNumber());
			inventoryReservation.setReserved(Boolean.TRUE);
			final InventoryReservationModel inventoryReservationModel = flexibleSearchService
					.getModelByExample(inventoryReservation);
			inventoryDetailRequest[loopValue].setReserved(inventoryReservationModel.getReserved().toString());
		}
		catch (final ModelNotFoundException mnfe)
		{
			LOG.error(CatCoreConstants.MODEL_NOTFOUND_EXCEPTION + inventoryReservation, mnfe);
			inventoryDetailRequest[loopValue].setReserved("false");
		}
		return inventoryDetailRequest;
	}

	/**
	 * This Method is to get JSON Response for Inventory Details
	 *
	 * @param inventoryDetails
	 *           : Input data which we get from Front End.
	 * @return : inventoryDetails Request Input mapping to Object.
	 */
	private EDHInventoryDetailsResponse[] getInventoryDetailsData(final String inventoryDetails)
	{
		EDHInventoryDetailsResponse[] inventoryDetailsRequest = null;
		inventoryDetailsRequest = CatRestClientUtil.getJSONObject(EDHInventoryDetailsResponse[].class, inventoryDetails);
		CatRestClientUtil.logResponse(inventoryDetailsRequest, "getInventoryDetailsData");
		return inventoryDetailsRequest;
	}


	/**
	 * {@inheritDoc}
	 *
	 */
	@Override
	public String getInventoryCountByPartyId(final String partyId, final String config, final String getDescendantNodes)
			throws CatException
	{
		String inventoryCountByPartyId = null;
		try
		{
			inventoryCountByPartyId = catEDHConfiguratorService.getInventoryCountByPartyId(partyId, config, getDescendantNodes);
			if (YES.equalsIgnoreCase(Config.getParameter(HYBRIS_DATA_ENABLE)))
			{
				inventoryCountByPartyId = getInventoryCount(inventoryCountByPartyId);
			}
		}
		catch (final CatEDHException e)
		{
			processException(e);
		}

		return inventoryCountByPartyId;
	}

	/**
	 * @param e
	 * @throws CatEDHRecordNotFoundException
	 * @throws CatException
	 */
	private void processException(final CatEDHException e) throws CatEDHRecordNotFoundException, CatException //NOSONAR
	{
		if (e instanceof CatRecordNotFoundException)
		{
			throw new CatEDHRecordNotFoundException(NO_RECORDS_FOUND, e);
		}
		else
		{
			throw new CatException(UNEXPECTED_EXCEPTION, e);
		}
	}


	private String getInventoryCount(String inventoryCounts) throws CatException //NOSONAR
	{
		final EDHInventoryResponse[] inventoryCountData = this.getInventoryCountData(inventoryCounts);
		if (null != inventoryCountData && null == inventoryCountData[0].getEdhError())
		{
			inventoryCounts = getAddressForMFDandPLC(inventoryCountData);
		}
		return inventoryCounts;
	}

	private EDHInventoryResponse[] getInventoryCountData(final String inventoryCountByPartyId)
	{
		EDHInventoryResponse[] inventoryCountResponse = null;
		inventoryCountResponse = CatRestClientUtil.getJSONObject(EDHInventoryResponse[].class, inventoryCountByPartyId);
		CatRestClientUtil.logResponse(inventoryCountResponse, "getInventoryCountData");
		return inventoryCountResponse;
	}

	private String getAddressForMFDandPLC(final EDHInventoryResponse[] inventoryCountResponse)
	{
		for (int i = 0; i < inventoryCountResponse.length; i++)
		{
			if (StringUtils.isNotBlank(inventoryCountResponse[i].getType())
					&& ("PDC".equalsIgnoreCase(inventoryCountResponse[i].getType())
							|| "MFR".equalsIgnoreCase(inventoryCountResponse[i].getType())))
			{
				setAddress(inventoryCountResponse, i);
			}
			if (StringUtils.isNotBlank(inventoryCountResponse[i].getType())
					&& "DLR".equalsIgnoreCase(inventoryCountResponse[i].getType()))
			{
				setDLRWarehouseName(inventoryCountResponse, i);
				updateChildWareHouseName(inventoryCountResponse, i);
			}
		}
		return CatRestClientUtil.getJSONResponse(inventoryCountResponse);
	}

	/**
	 * @param inventoryCountResponse
	 * @param i
	 */
	private void updateChildWareHouseName(final EDHInventoryResponse[] inventoryCountResponse, final int i)
	{
		final List<Child> childrens = inventoryCountResponse[i].getChildren();
		if (CollectionUtils.isNotEmpty(childrens))
		{
			final List<Child> locChildrens = new ArrayList<>();
			for (final Child children : childrens)
			{
				final WarehouseModel childWareHouse = dealerAddressService.findWarehouseByDealerCode(children.getPartyId());
				if (null != childWareHouse)
				{
					children.setName(childWareHouse.getName());
				}
				locChildrens.add(children);
			}
			inventoryCountResponse[i].setChildren(locChildrens);
		}

	}
	private void setDLRWarehouseName(final EDHInventoryResponse[] inventoryCountResponse, final int i)
	{
		final WarehouseModel whm = dealerAddressService.findWarehouseByDealerCode(inventoryCountResponse[i].getPartyId());
		if (null != whm)
		{
			inventoryCountResponse[i].setName(whm.getName());
		}
	}

	/**
	 * @param inventoryCountResponse
	 * @param inventoryCount
	 *
	 *           Get address for PDC and MFR from Hybris database
	 */
	private void setAddress(final EDHInventoryResponse[] inventoryCountResponse, final int inventoryCount)
	{
		final String partyId = getPartyId(inventoryCountResponse, inventoryCount);
		final PointOfServiceModel pos = dealerAddressService.findMFUorPDCByPosId(partyId);
		if (null != pos)
		{
			inventoryCountResponse[inventoryCount].setName(pos.getDisplayName());
			if (null != pos.getAddress())
			{
				final Address address = new Address();
				address.setCity(pos.getAddress().getTown());
				if (null != pos.getAddress().getCountry())
				{
					address.setCountry(pos.getAddress().getCountry().getName());
				}
				address.setStreetAddress(pos.getAddress().getStreetname());
				if (null != pos.getAddress().getRegion())
				{
					address.setState(pos.getAddress().getRegion().getName());
				}
				address.setPostalCode(pos.getAddress().getPostalcode());
				inventoryCountResponse[inventoryCount].setAddress(address);
			}
		}


	}

	/**
	 * @param inventoryCountResponse
	 * @param inventoryCount
	 * @return
	 */
	private String getPartyId(final EDHInventoryResponse[] inventoryCountResponse, final int inventoryCount)
	{
		String partyId = StringUtils.EMPTY;
		if ("MFR".equalsIgnoreCase(inventoryCountResponse[inventoryCount].getType()))
		{
			partyId = inventoryCountResponse[inventoryCount].getPartyId();
			partyId = partyId.substring(0, Math.min(partyId.length(), 2));
		}
		else if ("PDC".equalsIgnoreCase(inventoryCountResponse[inventoryCount].getType()))
		{
			partyId = inventoryCountResponse[inventoryCount].getPartyId();
		}
		return partyId;
	}

	public DealerAddressService getDealerAddressService()
	{
		return dealerAddressService;
	}


	public void setDealerAddressService(final DealerAddressService dealerAddressService)
	{
		this.dealerAddressService = dealerAddressService;
	}


}
