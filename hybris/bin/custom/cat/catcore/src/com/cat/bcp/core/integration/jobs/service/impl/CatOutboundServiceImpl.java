/**
 *
 */
package com.cat.bcp.core.integration.jobs.service.impl;

import de.hybris.platform.cronjob.jalo.AbortCronJobException;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.Config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

import javax.annotation.Resource;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.integration.constants.CatintegrationConstants;
import com.cat.bcp.core.integration.dao.CatFeedsDao;
import com.cat.bcp.core.integration.ftp.CatSFTPClient;
import com.cat.bcp.core.integration.jobs.CatAbstractAbortableCronJob;
import com.cat.bcp.core.integration.jobs.service.CatOutboundService;
import com.cat.core.integration.enums.InterfaceNameEnum;
import com.cat.core.integration.model.FeedAuditModel;


/**
 * @author vkumarpendyam
 *
 */
public class CatOutboundServiceImpl implements CatOutboundService
{
	private static final String INTERNAL_HYBRIS_FOLDER_PATH = ".internalHybrisFolderPath";

	private static final String OUTBOUND_DATA_FEED = "outboundDataFeed.";

	private static final Logger LOG = Logger.getLogger(CatOutboundServiceImpl.class);

	public static final String EMPTY_STRING = "";
	public static final int SFTPPORT = 22;
	public static final String SFTPREMOTEDIR = "/";

	@Resource
	private CatFeedsUtilsService catFeedsUtilsService;
	@Resource
	private CatFeedsDao catFeedsDao;
	@Resource
	private ModelService modelService;
	@Resource
	private CatSFTPClient catSFTPClient;

	private static final HashMap<String, InterfaceNameEnum> feedEnumMap = new HashMap<String, InterfaceNameEnum>();
	static
	{
		{
			feedEnumMap.put("customerquote", InterfaceNameEnum.QUOTE_CUSTOMER);
			feedEnumMap.put("countymapping", InterfaceNameEnum.COUNTY_MAPPING);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void startFeedGeneration(final String feedName, final CatAbstractAbortableCronJob catAbstractAbortableCronJob,
			final CronJobModel cronJobModel) throws AbortCronJobException
	{

		final FeedAuditModel feedAuditModel = new FeedAuditModel();
		feedAuditModel.setCronJobStartTimeStamp(new Date());
		catFeedsUtilsService.addAuditLogEntry(feedAuditModel, CatintegrationConstants.CRONJOB_EXECUTION_STARTED);
		feedAuditModel.setInterfaceName(feedEnumMap.get(feedName));


		final Date qryStartTime = catFeedsDao.getFeedQueryStartTime(feedAuditModel);
		if (qryStartTime != null)
		{
			feedAuditModel.setStartTimeParamForFeedQry(qryStartTime);
		}
		else
		{
			feedAuditModel.setStartTimeParamForFeedQry(DateUtils.setYears(new Date(), 2017));
		}
		feedAuditModel.setEndTimeParamForFeedQry(catFeedsUtilsService.getDailyEndTriggerTime(feedName));


		final int recordCount = catFeedsDao.getFeedQueryResultCount(feedAuditModel, feedName);
		if (recordCount == 0)
		{
			LOG.info("Doesn't exist records from last execution");
			return;
		}
		modelService.save(feedAuditModel);

		try
		{
			// Check if folder paths exists. Only then start the feed generation
			checkIfFoldersExists(feedName);
			// Delete the old files
			deleteOldFiles(feedName);

			generateFeedFile(feedName, feedAuditModel, catAbstractAbortableCronJob, cronJobModel);
			catFeedsUtilsService.addAuditLogEntry(feedAuditModel, CatintegrationConstants.CRONJOB_EXECUTION_COMPLETED);
			feedAuditModel.setCronJobEndtTimeStamp(new Date());
		}
		catch (final AbortCronJobException aceX)
		{
			throw aceX;
		}
		catch (final Exception e)
		{
			LOG.error("Exception in FeedsService startfeedgeneration method ", e);
			feedAuditModel.setFeedAuditStatus(CatintegrationConstants.FAILURE);
		}
		modelService.save(feedAuditModel);
	}

	/**
	 * @param feedName
	 * @throws Exception
	 */
	@Override
	public void deleteOldFiles(final String feedName) throws IllegalArgumentException
	{
		final String isDeleteOldFiles = Config.getString(OUTBOUND_DATA_FEED + feedName + ".isDeleteOldFiles", "true");
		if (!"false".equalsIgnoreCase(isDeleteOldFiles))
		{
			final String internalHybrisFolderPath = Config.getString(OUTBOUND_DATA_FEED + feedName + INTERNAL_HYBRIS_FOLDER_PATH,
					EMPTY_STRING);

			final File folder = new File(internalHybrisFolderPath);
			final File[] listOfFiles = folder.listFiles();
			clearFiles(internalHybrisFolderPath, listOfFiles);
		}
	}

	private void clearFiles(final String internalHybrisFolderPath, final File[] listOfFiles)
	{
		if (ArrayUtils.isNotEmpty(listOfFiles))
		{
			for (final File file : listOfFiles)
			{
				if (file.isFile())
				{
					deleteFile(internalHybrisFolderPath, file.getName());
				}
			}
		}
	}

	/**
	 * deleteFile deletes all the files before generating new files
	 *
	 * @param fileName
	 * @param localFilePath
	 * @throws Exception
	 */
	private static void deleteFile(final String localFilePath, final String fileName) throws IllegalArgumentException
	{

		final File sourceFile = new File(localFilePath + fileName);
		if (!sourceFile.delete())
		{
			throw new IllegalArgumentException(
					"*************** File is not deleted from local processed folder ******************" + sourceFile);
		}
	}

	/**
	 * checkIfFoldersExists checks for the source folder existence
	 *
	 * @param feedName
	 * @throws Exception
	 */
	@Override
	public boolean checkIfFoldersExists(final String feedName) throws FileNotFoundException
	{

		final String internalHybrisFolderPath = Config.getString(OUTBOUND_DATA_FEED + feedName + INTERNAL_HYBRIS_FOLDER_PATH,
				EMPTY_STRING);

		final File hybrisFolder = new File(internalHybrisFolderPath);
		if (hybrisFolder.exists())
		{
			return true;
		}
		else
		{
			LOG.error("Folder paths doesnot exists. Exiting the feed generation:" + hybrisFolder);
			throw new FileNotFoundException();
		}
	}

	/**
	 * generateFeedFile generates the source file
	 *
	 * @param feedName
	 * @param feedAuditModel
	 * @param CatAbstractAbortableCronJob
	 * @param cronJobModel
	 * @throws AbortCronJobException
	 * @throws IOException
	 */
	private void generateFeedFile(final String feedName, final FeedAuditModel feedAuditModel,
			final CatAbstractAbortableCronJob catAbstractAbortableCronJob, final CronJobModel cronJobModel)
			throws AbortCronJobException, IOException
	{
		boolean isUploadedToThirdPartyFTP = false;
		final String fileType = Config.getString(OUTBOUND_DATA_FEED + feedName + ".fileType", "csv");
		if ("csv".equalsIgnoreCase(fileType))
		{
			catFeedsDao.generateFeedResultForCsv(feedName, feedAuditModel, catAbstractAbortableCronJob, cronJobModel);
		}

		catFeedsUtilsService.addAuditLogEntry(feedAuditModel, CatintegrationConstants.FILE_TRANSFER_STARTED);

		final String internalHybrisFolderPath = Config.getString(OUTBOUND_DATA_FEED + feedName + INTERNAL_HYBRIS_FOLDER_PATH,
				EMPTY_STRING);

		isUploadedToThirdPartyFTP = transferFileToThirdPartyFTP(feedName, internalHybrisFolderPath);

		if (isUploadedToThirdPartyFTP)
		{
			catFeedsUtilsService.addAuditLogEntry(feedAuditModel, CatintegrationConstants.FILE_TRANSFER_COMPLETED_WITHSUCCESS);
			feedAuditModel.setFeedAuditStatus(CatintegrationConstants.SUCCESS);
		}
		else
		{
			catFeedsUtilsService.addAuditLogEntry(feedAuditModel, CatintegrationConstants.FILE_TRANSFER_COMPLETED_WITHFAILURE);
		}
		LOG.info("FeedsService.generateFeedFile() ended");
	}

	/**
	 * transferFileToThirdPartyFTP copies the source file to the FTP server
	 *
	 * @param feedName
	 * @param internalHybrisFolderPath
	 */
	@Override
	public boolean transferFileToThirdPartyFTP(final String feedName, final String internalHybrisFolderPath)
	{
		LOG.info("FeedsService.transferFileToThirdPartyFTP() started for " + feedName);

		// SFTP properties
		final String sftpRemoteUserName = Config.getString(OUTBOUND_DATA_FEED + feedName + ".sftpUsername", EMPTY_STRING);
		final String sftpPassword = Config.getString(OUTBOUND_DATA_FEED + feedName + ".sftpPwd", EMPTY_STRING);
		final String sftpRemoteHost = Config.getString(OUTBOUND_DATA_FEED + feedName + ".sftpIP", EMPTY_STRING);
		final int sftpPort = Config.getInt(OUTBOUND_DATA_FEED + feedName + ".sftpPort", SFTPPORT);
		final String sftpRemoteDir = Config.getString(OUTBOUND_DATA_FEED + feedName + ".externalSFTPFolder", SFTPREMOTEDIR);

		final File folder = new File(internalHybrisFolderPath);
		final File[] listOfFiles = folder.listFiles();
		boolean isAllFilesUploadedToFTP = true;
		if (listOfFiles != null)
		{
			for (int i = 0; i < listOfFiles.length; i++)
			{
				isAllFilesUploadedToFTP = storeFileViaSFTP(internalHybrisFolderPath, sftpRemoteUserName, sftpPassword, sftpRemoteHost,
						sftpPort, sftpRemoteDir, listOfFiles, isAllFilesUploadedToFTP, i);
			}
		}
		LOG.info("FeedsService.transferFileToThirdPartyFTP() ended for " + feedName);
		return isAllFilesUploadedToFTP;
	}

	private boolean storeFileViaSFTP(final String internalHybrisFolderPath, final String sftpRemoteUserName,
			final String sftpPassword, final String sftpRemoteHost, final int sftpPort, final String sftpRemoteDir,
			final File[] listOfFiles, boolean isAllFilesUploadedToFTP, final int i)
	{
		boolean isUploadedToFTP;
		if (listOfFiles[i].isFile())
		{
			isUploadedToFTP = catSFTPClient.storeFileViaSFTP(internalHybrisFolderPath, listOfFiles[i].getName(), sftpRemoteDir,
					sftpRemoteHost, sftpRemoteUserName, sftpPassword, sftpPort);
			if (!isUploadedToFTP)
			{
				isAllFilesUploadedToFTP = isUploadedToFTP;
			}
		}
		return isAllFilesUploadedToFTP;
	}

}
