package com.cat.bcp.core.integration.jobs.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.cat.bcp.core.integration.beans.CatControlRecord;
import com.cat.bcp.core.integration.beans.CatDupOrderRecord;
import com.cat.bcp.core.integration.beans.CatInlandShippingRecord;
import com.cat.bcp.core.integration.beans.CatInvoiceCriteriaRecord;
import com.cat.bcp.core.integration.beans.CatNoteRecord;
import com.cat.bcp.core.integration.beans.CatOrderRecord;
import com.cat.bcp.core.integration.beans.CatSpecificationRecord;
import com.cat.bcp.core.integration.beans.CatTrailerRecord;
import com.cat.bcp.core.integration.constants.CatintegrationConstants;
import com.cat.bcp.core.integration.jobs.service.CatRecordConversionService;


/**
 * The class contains the implementation of {@link CatRecordConversionService}
 */

public class CatRecordConversionServiceImpl implements CatRecordConversionService
{

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public String convertControlRecordToString(final CatControlRecord catControlRecord, final String catOrderRecordString)
	{
		final StringBuilder controlRecordBuilder = new StringBuilder(80);

		controlRecordBuilder.append(StringUtils.rightPad(catControlRecord.getRecordType(),
				CatintegrationConstants.RECORD_TYPE_LENGTH, CatintegrationConstants.EMPTYSPACE));
		controlRecordBuilder.append(StringUtils.rightPad(catControlRecord.getSequenceNbr(),
				CatintegrationConstants.SEQUENCE_NUMBER_LENGTH, CatintegrationConstants.EMPTYSPACE));
		controlRecordBuilder.append(String.valueOf(catOrderRecordString));
		controlRecordBuilder.append(StringUtils.rightPad(catControlRecord.getTransMissiondate(),
				CatintegrationConstants.TRANSMISSION_DATE_LENGTH, CatintegrationConstants.EMPTYSPACE));
		controlRecordBuilder.append(StringUtils.rightPad(catControlRecord.getMiscIndicator(),
				CatintegrationConstants.INDICATOR_LENGTH, CatintegrationConstants.EMPTYSPACE));

		controlRecordBuilder.append(StringUtils.rightPad(catControlRecord.getShipDealerCode(),
				CatintegrationConstants.SHIP_TO_CODE_LENGTH, CatintegrationConstants.EMPTYSPACE));
		controlRecordBuilder.append(StringUtils.rightPad(catControlRecord.getSalesModel(),
				CatintegrationConstants.SALES_CODE_LENGTH, CatintegrationConstants.EMPTYSPACE));

		controlRecordBuilder.append(StringUtils.rightPad(catControlRecord.getOrderCopyQuanity(),
				CatintegrationConstants.ORDER_COPY_QUANTITY_LENGTH, CatintegrationConstants.EMPTYSPACE));
		controlRecordBuilder.append(StringUtils.rightPad(catControlRecord.getOrderPriortiyType(),
				CatintegrationConstants.ORDER_PRIORITY_TYPE_LENGTH, CatintegrationConstants.EMPTYSPACE));

		controlRecordBuilder.append(StringUtils.rightPad(catControlRecord.getOrderProductType(),
				CatintegrationConstants.ORDER_PRODUCT_TYPE_LENGTH, CatintegrationConstants.EMPTYSPACE));
		controlRecordBuilder.append(StringUtils.rightPad(catControlRecord.getDsuoCustomerCare(),
				CatintegrationConstants.DSUO_CUSTOMER_CARE_LENGTH, CatintegrationConstants.EMPTYSPACE));
		controlRecordBuilder.append(StringUtils.rightPad(catControlRecord.getCancelRequest(),
				CatintegrationConstants.CANCEL_REQUEST_LENGTH, CatintegrationConstants.EMPTYSPACE));
		controlRecordBuilder.append(StringUtils.rightPad(catControlRecord.getAckRefreshIndicator(),
				CatintegrationConstants.ACK_REFRESH_INDICATOR_LENGTH, CatintegrationConstants.EMPTYSPACE));
		controlRecordBuilder.append(StringUtils.rightPad(catControlRecord.getDbsReservedArea(),
				CatintegrationConstants.DBS_RESERVED_AREA_LENGTH, CatintegrationConstants.EMPTYSPACE));
		controlRecordBuilder.append(StringUtils.rightPad(catControlRecord.getDupLinkIndicator(),
				CatintegrationConstants.DUP_LINK_INDICATOR, CatintegrationConstants.EMPTYSPACE));
		controlRecordBuilder.append(StringUtils.rightPad(catControlRecord.getOrderValidationRequest(),
				CatintegrationConstants.ORDER_VALIDATION_REQ_LENGTH, CatintegrationConstants.EMPTYSPACE));

		return StringUtils.rightPad(String.valueOf(controlRecordBuilder), CatintegrationConstants.ORDER_RECORD_LENGTH,
				CatintegrationConstants.EMPTYSPACE);
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public List<String> convertSpecificationRecordsToString(final List<CatSpecificationRecord> catSpecificationRecordList,
			final String catOrderRecordString)
	{

		final List<String> specificationRecordList = new ArrayList<>();
		for (final CatSpecificationRecord specificationRecord : catSpecificationRecordList)
		{

			final StringBuilder specificationRecordBuilder = new StringBuilder(80);

			specificationRecordBuilder.append(StringUtils.rightPad(specificationRecord.getRecordType(),
					CatintegrationConstants.RECORD_TYPE_LENGTH, CatintegrationConstants.EMPTYSPACE));
			specificationRecordBuilder.append(StringUtils.rightPad(specificationRecord.getSequenceNbr(),
					CatintegrationConstants.SEQUENCE_NUMBER_LENGTH, CatintegrationConstants.EMPTYSPACE));
			specificationRecordBuilder.append(catOrderRecordString);
			String materialNumber = null;
			final boolean isNumeric = StringUtils.isNumeric(specificationRecord.getMaterialNumber());
			if (isNumeric)
			{
				if (specificationRecord.getMaterialNumber().length() == CatintegrationConstants.MATERIAL_NUMBER_WITH_LENGTH_SIX)
				{
					materialNumber = "0" + specificationRecord.getMaterialNumber();
					materialNumber = StringUtils.leftPad(materialNumber, CatintegrationConstants.MATERIAL_NUMBER_WITH_LENGTH_NINE,
							CatintegrationConstants.EMPTYSPACE);
					materialNumber = StringUtils.rightPad(materialNumber, CatintegrationConstants.MATERIAL_NUMBER_WITH_LENGTH_TEN,
							CatintegrationConstants.EMPTYSPACE);
				}
				if (specificationRecord.getMaterialNumber().length() == CatintegrationConstants.MATERIAL_NUMBER_WITH_LENGTH_SEVEN)
				{
					materialNumber = specificationRecord.getMaterialNumber();
					materialNumber = StringUtils.leftPad(materialNumber, CatintegrationConstants.MATERIAL_NUMBER_WITH_LENGTH_NINE,
							CatintegrationConstants.EMPTYSPACE);
					materialNumber = StringUtils.rightPad(materialNumber, CatintegrationConstants.MATERIAL_NUMBER_WITH_LENGTH_TEN,
							CatintegrationConstants.EMPTYSPACE);
				}
			}
			if (StringUtils.isAlphanumeric(specificationRecord.getMaterialNumber())
					&& specificationRecord.getMaterialNumber().length() == CatintegrationConstants.MATERIAL_NUMBER_WITH_LENGTH_SIX)
			{
				materialNumber = specificationRecord.getMaterialNumber();
				materialNumber = StringUtils.leftPad(materialNumber, CatintegrationConstants.MATERIAL_NUMBER_WITH_LENGTH_NINE,
						CatintegrationConstants.EMPTYSPACE);
				materialNumber = StringUtils.rightPad(materialNumber, CatintegrationConstants.MATERIAL_NUMBER_WITH_LENGTH_TEN,
						CatintegrationConstants.EMPTYSPACE);
			}
			specificationRecordBuilder.append(materialNumber);
			if (specificationRecord.getMaterialNumber().length() > CatintegrationConstants.MATERIAL_NUMBER_WITH_LENGTH_SEVEN)
			{
				specificationRecordBuilder.append(StringUtils.rightPad(specificationRecord.getMaterialNumber(),
						CatintegrationConstants.MATERIAL_NUMBER_LENGTH, CatintegrationConstants.EMPTYSPACE));
			}

			specificationRecordBuilder.append(StringUtils.rightPad(specificationRecord.getProductDescription(),
					CatintegrationConstants.BASEVARIANT_NAME_LENGTH, CatintegrationConstants.EMPTYSPACE));

			specificationRecordBuilder.append(StringUtils.leftPad(specificationRecord.getOrderQuantity(),
					CatintegrationConstants.ORDER_COPY_QUANTITY_LENGTH, '0'));
			specificationRecordBuilder.append(StringUtils.rightPad(specificationRecord.getShipLessIndicator(),
					CatintegrationConstants.SHIPLESS_INDICATOR_LENGTH, CatintegrationConstants.EMPTYSPACE));
			specificationRecordBuilder.append(StringUtils.rightPad(specificationRecord.getOrderAssemblyCode(),
					CatintegrationConstants.ORDER_ASSEMBLY_CODE_LENGTH, CatintegrationConstants.EMPTYSPACE));
			specificationRecordBuilder.append(StringUtils.rightPad(specificationRecord.getNonStandardItemCode(),
					CatintegrationConstants.NON_STANDARD_ITEM_CODE_LENGTH, CatintegrationConstants.EMPTYSPACE));
			specificationRecordBuilder.append(StringUtils.rightPad(specificationRecord.getActionCode(),
					CatintegrationConstants.ACTION_CODE_LENGTH, CatintegrationConstants.EMPTYSPACE));
			final String specificationRecordStr = StringUtils.rightPad(String.valueOf(specificationRecordBuilder),
					CatintegrationConstants.ORDER_RECORD_LENGTH, CatintegrationConstants.EMPTYSPACE);

			specificationRecordList.add(specificationRecordStr);
		}
		return specificationRecordList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> convertNoteRecordsToString(final List<CatNoteRecord> noteRecordList, final String catOrderRecordString)
	{

		final List<String> noteRecordStringList = new ArrayList<>();
		for (final CatNoteRecord noteRecord : noteRecordList)
		{

			final StringBuilder catNoteRecordBuilder = new StringBuilder(80);

			catNoteRecordBuilder.append(StringUtils.rightPad(noteRecord.getRecordType(), CatintegrationConstants.RECORD_TYPE_LENGTH,
					CatintegrationConstants.EMPTYSPACE));
			catNoteRecordBuilder.append(StringUtils.rightPad(noteRecord.getSequenceNbr(),
					CatintegrationConstants.SEQUENCE_NUMBER_LENGTH, CatintegrationConstants.EMPTYSPACE));
			catNoteRecordBuilder.append(String.valueOf(catOrderRecordString));
			catNoteRecordBuilder.append(StringUtils.rightPad(noteRecord.getOrderNoteTxt(),
					CatintegrationConstants.ORDER_NOTES_LENGTH, CatintegrationConstants.EMPTYSPACE));
			final String note = StringUtils.rightPad(String.valueOf(catNoteRecordBuilder),
					CatintegrationConstants.ORDER_RECORD_LENGTH, CatintegrationConstants.EMPTYSPACE);
			noteRecordStringList.add(note);
		}

		return noteRecordStringList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> convertDupRecordsToString(final List<CatDupOrderRecord> catDupOrderRecords,
			final String catOrderRecordString)
	{
		final List<String> catDupOrderRecordStringList = new ArrayList<>();
		for (final CatDupOrderRecord catDupOrderRecord : catDupOrderRecords)
		{
			final StringBuilder catDupOrderRecordBuilder = new StringBuilder(80);
			catDupOrderRecordBuilder.append(StringUtils.rightPad(catDupOrderRecord.getRecordType(),
					CatintegrationConstants.RECORD_TYPE_LENGTH, CatintegrationConstants.EMPTYSPACE));
			catDupOrderRecordBuilder.append(StringUtils.rightPad(catDupOrderRecord.getSequenceNbr(),
					CatintegrationConstants.SEQUENCE_NUMBER_LENGTH, CatintegrationConstants.EMPTYSPACE));
			catDupOrderRecordBuilder.append(String.valueOf(catOrderRecordString));
			final List<String> poNumbers = catDupOrderRecord.getOrderPONumbersList();
			catDupOrderRecordBuilder.append("DUP:");
			for (final String poNumberString : poNumbers)
			{
				catDupOrderRecordBuilder.append(StringUtils.rightPad(poNumberString,
						CatintegrationConstants.PURCHASER_ORDER_NUMBER_LENGTH, CatintegrationConstants.EMPTYSPACE));
			}
			catDupOrderRecordStringList.add(StringUtils.rightPad(String.valueOf(catDupOrderRecordBuilder),
					CatintegrationConstants.ORDER_RECORD_LENGTH, CatintegrationConstants.EMPTYSPACE));
		}
		return catDupOrderRecordStringList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String convertCatInlandShippingRecordString(final CatInlandShippingRecord catInlandShippingRecord,
			final String catOrderRecordString)
	{
		final StringBuilder catInlandShippingRecordBuilder = new StringBuilder(80);
		catInlandShippingRecordBuilder.append(StringUtils.rightPad(catInlandShippingRecord.getRecordType(),
				CatintegrationConstants.RECORD_TYPE_LENGTH, CatintegrationConstants.EMPTYSPACE));
		catInlandShippingRecordBuilder.append(StringUtils.rightPad(catInlandShippingRecord.getSequenceNbr(),
				CatintegrationConstants.SEQUENCE_NUMBER_LENGTH, CatintegrationConstants.EMPTYSPACE));
		catInlandShippingRecordBuilder.append(catOrderRecordString);
		catInlandShippingRecordBuilder.append(StringUtils.rightPad(catInlandShippingRecord.getFreightChargeCode(),
				CatintegrationConstants.FREIGHT_CHARGE_CODE_LENGTH, CatintegrationConstants.EMPTYSPACE));
		catInlandShippingRecordBuilder.append(StringUtils.rightPad(catInlandShippingRecord.getTransPorationCode(),
				CatintegrationConstants.TRANSPORT_CODE_LENGTH, CatintegrationConstants.EMPTYSPACE));
		catInlandShippingRecordBuilder.append(StringUtils.rightPad(catInlandShippingRecord.getShipLessThanTruckload(),
				CatintegrationConstants.SHIPLESS_THAN_TRUCK_LOAD_LENTH, CatintegrationConstants.EMPTYSPACE));
		catInlandShippingRecordBuilder.append(StringUtils.rightPad(catInlandShippingRecord.getFreightRouting(),
				CatintegrationConstants.FREIGHT_ROUTING_LENGTH, CatintegrationConstants.EMPTYSPACE));
		catInlandShippingRecordBuilder.append(StringUtils.rightPad(catInlandShippingRecord.getOrderShipAfterDate(),
				CatintegrationConstants.SHIP_AFTER_DATE_LENGTH, CatintegrationConstants.EMPTYSPACE));
		catInlandShippingRecordBuilder.append(StringUtils.rightPad(catInlandShippingRecord.getOrderShipBeforeDate(),
				CatintegrationConstants.SHIP_BEFORE_DATE_LENGTH, CatintegrationConstants.EMPTYSPACE));
		return StringUtils.rightPad(String.valueOf(catInlandShippingRecordBuilder), CatintegrationConstants.ORDER_RECORD_LENGTH,
				CatintegrationConstants.EMPTYSPACE);

	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public String convertCatInvoiceCriteriarecordString(final CatInvoiceCriteriaRecord catInvoiceCriteriaRecord,
			final String catOrderRecordString)
	{

		final StringBuilder catInvoiceCriteriaRecordBuilder = new StringBuilder(80);

		catInvoiceCriteriaRecordBuilder.append(StringUtils.rightPad(catInvoiceCriteriaRecord.getInvoiceCriteriaRecordType(),
				CatintegrationConstants.RECORD_TYPE_LENGTH, CatintegrationConstants.EMPTYSPACE));
		catInvoiceCriteriaRecordBuilder.append(StringUtils.rightPad(catInvoiceCriteriaRecord.getInvoiceCriteriaSequenceNumber(),
				CatintegrationConstants.SEQUENCE_NUMBER_LENGTH, CatintegrationConstants.EMPTYSPACE));
		catInvoiceCriteriaRecordBuilder.append(catOrderRecordString);
		catInvoiceCriteriaRecordBuilder.append(StringUtils.rightPad(catInvoiceCriteriaRecord.getPaymentTermsCode(),
				CatintegrationConstants.PAYMENT_TERMS_LENGTH, CatintegrationConstants.EMPTYSPACE));
		catInvoiceCriteriaRecordBuilder.append(StringUtils.rightPad(catInvoiceCriteriaRecord.getPriceProtectionIndicator(),
				CatintegrationConstants.PRICE_PROTECTION_INDICATOR_LENGTH, CatintegrationConstants.EMPTYSPACE));
		catInvoiceCriteriaRecordBuilder.append(StringUtils.rightPad(catInvoiceCriteriaRecord.getAuthDocType(),
				CatintegrationConstants.AUTH_DOC_TYPE_LENGTH, CatintegrationConstants.EMPTYSPACE));
		catInvoiceCriteriaRecordBuilder.append(StringUtils.rightPad(catInvoiceCriteriaRecord.getAuthDocNumber(),
				CatintegrationConstants.AUTH_DOC_NUMBER_LENGTH, CatintegrationConstants.EMPTYSPACE));
		return StringUtils.rightPad(String.valueOf(catInvoiceCriteriaRecordBuilder), CatintegrationConstants.ORDER_RECORD_LENGTH,
				CatintegrationConstants.EMPTYSPACE);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String convertTrailerRecordString(final CatTrailerRecord trailerRecord, final String catOrderRecordString)
	{

		final StringBuilder catTrailerRecordBuilder = new StringBuilder(80);

		catTrailerRecordBuilder.append(StringUtils.rightPad(trailerRecord.getRecordType(),
				CatintegrationConstants.RECORD_TYPE_LENGTH, CatintegrationConstants.EMPTYSPACE));
		catTrailerRecordBuilder.append(StringUtils.rightPad(trailerRecord.getSequenceNbr(),
				CatintegrationConstants.SEQUENCE_NUMBER_LENGTH, CatintegrationConstants.EMPTYSPACE));
		catTrailerRecordBuilder.append(catOrderRecordString);
		catTrailerRecordBuilder
				.append(StringUtils.leftPad(trailerRecord.getProPrietaryRecord(), CatintegrationConstants.PRO_PRIETARY_LENGTH, '0'));

		return StringUtils.rightPad(String.valueOf(catTrailerRecordBuilder), CatintegrationConstants.ORDER_RECORD_LENGTH,
				CatintegrationConstants.EMPTYSPACE);
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public String convertOrderRecordToString(final CatOrderRecord catOrderRecord)
	{
		final StringBuilder controlRecordBuilder = new StringBuilder(25);

		controlRecordBuilder.append(StringUtils.rightPad(catOrderRecord.getDealerCode(), CatintegrationConstants.DEALER_CODE_LENGTH,
				CatintegrationConstants.EMPTYSPACE));
		controlRecordBuilder.append(StringUtils.rightPad(catOrderRecord.getPurchaseOrdNbr(),
				CatintegrationConstants.PURCHASER_ORDER_NUMBER_LENGTH, CatintegrationConstants.EMPTYSPACE));
		controlRecordBuilder.append(StringUtils.rightPad(catOrderRecord.getOrderControlNmbr(),
				CatintegrationConstants.CONTROL_NUMBER_LENGTH, CatintegrationConstants.EMPTYSPACE));

		return controlRecordBuilder.toString();
	}

}
