
package com.cat.bcp.core.integration.jobs.service.impl;

import de.hybris.platform.b2b.model.B2BCommentModel;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.ResourceAccessException;

import com.cat.bcp.core.enums.FreightChargeCodeEnum;
import com.cat.bcp.core.enums.FreightTransportCodeEnum;
import com.cat.bcp.core.enums.LaneTypeEnum;
import com.cat.bcp.core.integration.beans.CatControlRecord;
import com.cat.bcp.core.integration.beans.CatDupOrderRecord;
import com.cat.bcp.core.integration.beans.CatInlandShippingRecord;
import com.cat.bcp.core.integration.beans.CatInvoiceCriteriaRecord;
import com.cat.bcp.core.integration.beans.CatNoteRecord;
import com.cat.bcp.core.integration.beans.CatOrderRecord;
import com.cat.bcp.core.integration.beans.CatSpecificationRecord;
import com.cat.bcp.core.integration.beans.CatTrailerRecord;
import com.cat.bcp.core.integration.dao.CatOrderPlacedDao;
import com.cat.bcp.core.integration.jobs.service.CatRecordCreationService;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.FBCPartsOrderInfoModel;
import com.cat.bcp.core.model.PartVariantProductModel;
import com.cat.core.integration.CatIntegrationUtils;
import com.cat.core.integration.fbc.request.FBCCreateRequestMapper;
import com.cat.core.integration.fbc.request.FeatureData;
import com.cat.core.integration.fbc.request.Selection;
import com.cat.core.integration.service.CatFBCConfiguratorService;





/**
 * The class implements the method of {@link CatRecordCreationService} interface.
 */
public class CatRecordCreationServiceImpl implements CatRecordCreationService
{
	private static final Logger LOGGER = Logger.getLogger(CatRecordCreationServiceImpl.class.getName());
	public static final String FREIGHT_ROUTING = "MANAGED";
	public static final String SPECIAL_CHARACTER_REMOVAL_REGEX = "[^a-zA-Z0-9]+";
	public static final String INLAND_SHIPPING_RECORD_TYPE = "N4";
	public static final String TRANS_PORATION_CODE = "T";
	public static final String INVOICE_CRITERIA_RECORD_TYPE = "N5";
	public static final String SEQUENCE_NBR = "001";
	public static final String TRAILER_RECORD_TYPE = "N9";
	public static final String NOTES_RECORD_TYPE = "N3";
	public static final String ACCESSORY_BASE_PRODUCT = "CUV MISC";

	@Autowired
	private SessionService sessionService;
	@Autowired
	private UserService userService;
	@Resource
	ConfigurationService configurationService;
	@Resource
	CatOrderPlacedDao catOrderPlacedDao;
	@Resource
	CatFBCConfiguratorService catFBCConfiguratorService;

	ConfigVariantProductModel configVariantProductModel;

	public static final String NEW_ORDER_SPECIFICATION_RECORD_TYPE = "N2";
	public static final String EMPTY_STRING = "";
	public static final String EMPTY_SPACE = " ";
	public static final String TRUCKLOAD = "Truckload";
	public static final String DEALER = "Dealer";
	public static final String QUANTITY = "001";
	public static final String LANE2ORDER = "LANE 2 ORDER";
	public static final String LANE3ORDER = "LANE 3 ORDER";
	public static final String LANE2TYPE = "LANE2";
	public static final String LANE3TYPE = "LANE3";



	private static final List<String> specificationRecordSequenceList = new ArrayList<>();
	static
	{
		for (int i = 1; i < 105; i++)
		{
			specificationRecordSequenceList.add(String.format("%03d", i));
		}
	}

	private int recordCounter = 0;

	private int notesCounter = 0;

	private int mixandMatchcount = 100;

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public CatOrderRecord createOrderRecord(final OrderModel order)
	{
		recordCounter = 0;
		notesCounter = 0;
		mixandMatchcount = 100;
		final CatOrderRecord catOrderRecord = new CatOrderRecord();
		final String dealerCode = order.getUnit().getUid();
		final String purchaseOrderNo = order.getPurchaseOrderNumber();
		catOrderRecord.setDealerCode(dealerCode);
		catOrderRecord.setPurchaseOrdNbr(purchaseOrderNo);
		catOrderRecord.setOrderControlNmbr(EMPTY_STRING);
		return catOrderRecord;
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public CatControlRecord createControlRecord(final OrderModel order, final AbstractOrderEntryModel orderEntry)
	{

		final CatControlRecord catControlRecord = new CatControlRecord();

		final String transmissionDate = CatIntegrationUtils.getdateStringFormat(new Date());
		final String shipToCode = order.getDeliveryAddress().getSapCustomerID();

		catControlRecord.setOrderCopyQuanity(QUANTITY);

		String baseProduct = null;
		if (StringUtils.isNotBlank(orderEntry.getCompatibleModel()))
		{
			baseProduct = ACCESSORY_BASE_PRODUCT;
		}
		else
		{
			final ProductModel salesModel = CatIntegrationUtils.getProduct(orderEntry);
			baseProduct = salesModel.getCode();
		}
		setCatControlRecord(catControlRecord, transmissionDate, baseProduct, shipToCode);
		return catControlRecord;
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public CatControlRecord createControlRecord(final OrderModel order, final AbstractOrderEntryModel orderEntry,
			final List<String> listOfPONumbers)
	{

		final CatControlRecord catControlRecord = new CatControlRecord();

		final String transmissionDate = CatIntegrationUtils.getdateStringFormat(new Date());

		final ProductModel salesModel = CatIntegrationUtils.getProduct(orderEntry);
		final String shipToCode = order.getDeliveryAddress().getSapCustomerID();
		catControlRecord.setOrderCopyQuanity(specificationRecordSequenceList.get(listOfPONumbers.size() - 1));

		setCatControlRecord(catControlRecord, transmissionDate, salesModel.getCode(), shipToCode);
		return catControlRecord;
	}

	/**
	 * Sets the cat control record.
	 *
	 * @param catControlRecord
	 *           the cat control record
	 * @param transmissionDate
	 *           the transmission date
	 * @param salesModel
	 *           the sales model
	 * @param shipToCode
	 *           the ship to code
	 */
	private void setCatControlRecord(final CatControlRecord catControlRecord, final String transmissionDate,
			final String salesModel, final String shipToCode)
	{
		catControlRecord.setRecordType("N1");
		catControlRecord.setSequenceNbr("001");
		catControlRecord.setTransMissiondate(transmissionDate);
		catControlRecord.setMiscIndicator(EMPTY_STRING);
		catControlRecord.setShipDealerCode(shipToCode);
		catControlRecord.setSalesModel(salesModel);

		catControlRecord.setMiscIndicator(EMPTY_STRING);
		catControlRecord.setOrderPriortiyType(EMPTY_STRING);
		catControlRecord.setOrderProductType("M");
		catControlRecord.setDsuoCustomerCare(EMPTY_STRING);
		catControlRecord.setDsuoCustomerCare(EMPTY_STRING);
		catControlRecord.setCancelRequest(EMPTY_STRING);
		catControlRecord.setAckRefreshIndicator(EMPTY_STRING);
		catControlRecord.setDbsReservedArea(EMPTY_STRING);
		catControlRecord.setDupLinkIndicator(EMPTY_STRING);
		catControlRecord.setOrderValidationRequest(EMPTY_STRING);
		recordCounter++;
	}

	/**
	 *
	 * {@inheritDoc}
	 */

	@Override
	public List<CatSpecificationRecord> createSpecificationrecord(final OrderModel order, final AbstractOrderEntryModel orderEntry)
			throws ResourceAccessException
	{
		int specificationRecordCounter = 0;
		final List<CatSpecificationRecord> listOfSpecificationRecord = new ArrayList<>();

		configVariantProductModel = (ConfigVariantProductModel) orderEntry.getProduct();
		final BaseVariantProductModel baseVariantProductModel = (BaseVariantProductModel) configVariantProductModel
				.getBaseProduct();
		final Long quantity = orderEntry.getQuantity();


		if (order.getShipToAfter() != null)
		{
			createDCASpecificationRecord(specificationRecordCounter, listOfSpecificationRecord, baseVariantProductModel, quantity);

			final String opcodeUtv = Config.getString("orderdirector.opmaterial.utv.code",
					"0P9003|Utility Vehicles&&0P0210|Utility Vehicles");
			final String[] opcodeUtvCodeDescriptions = opcodeUtv.split("&&");

			for (final String opcodedetails : opcodeUtvCodeDescriptions)
			{
				recordCounter++;
				specificationRecordCounter++;
				final CatSpecificationRecord specificationRecord = getCatSpecificationRecordWithOpCode(specificationRecordCounter,
						quantity, opcodedetails);
				listOfSpecificationRecord.add(specificationRecord);
			}
		}
		else
		{
			generateMandatoryPartRecords(specificationRecordCounter, listOfSpecificationRecord, baseVariantProductModel, quantity,
					orderEntry, order);
		}
		return listOfSpecificationRecord;
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public List<CatSpecificationRecord> createAccessorySpecificationrecords(final OrderModel order)
	{

		int specificationRecordCounter = 0;
		final List<CatSpecificationRecord> listOfSpecificationRecord = new ArrayList<>();
		final List<AbstractOrderEntryModel> listOfOrderEntries = order.getEntries();

		final String compatilbleSalesModel = listOfOrderEntries.get(0).getCompatibleModel();

		final String opcodeUtv = Config.getString(
				"orderdirector." + StringUtils.deleteWhitespace(compatilbleSalesModel) + ".toplevel.accessories.code",
				"0P4711|CUV82&&0P0210|Utility Vehicles");
		final String[] opcodeUtvCodeDescriptions = opcodeUtv.split("&&");
		for (final String opcodedetails : opcodeUtvCodeDescriptions)
		{
			recordCounter++;
			specificationRecordCounter++;
			final CatSpecificationRecord specificationRecord = getCatSpecificationRecordWithOpCode(specificationRecordCounter,
					Long.valueOf(1), opcodedetails);
			listOfSpecificationRecord.add(specificationRecord);
		}

		for (final AbstractOrderEntryModel orderEntry : listOfOrderEntries)
		{
			recordCounter++;
			specificationRecordCounter++;
			final Long quantity = orderEntry.getQuantity();
			final PartVariantProductModel partVariantProductModel = (PartVariantProductModel) orderEntry.getProduct();
			createAccessorySpecificationRecord(specificationRecordCounter, listOfSpecificationRecord, partVariantProductModel,
					quantity);
		}

		return listOfSpecificationRecord;

	}

	/**
	 * @param specificationRecordCounter
	 * @param listOfSpecificationRecord
	 * @param baseVariantProductModel
	 * @param quantity
	 */
	private void createDCASpecificationRecord(final int specificationRecordCounter,
			final List<CatSpecificationRecord> listOfSpecificationRecord, final BaseVariantProductModel baseVariantProductModel,
			final Long quantity)
	{
		recordCounter++;
		final CatSpecificationRecord specificationRecord1 = new CatSpecificationRecord();
		specificationRecord1.setSequenceNbr(specificationRecordSequenceList.get(specificationRecordCounter));
		specificationRecord1.setMaterialNumber(
				getValueWithoutSpecialCharacters(baseVariantProductModel.getCode(), SPECIAL_CHARACTER_REMOVAL_REGEX));
		specificationRecord1.setProductDescription(baseVariantProductModel.getName().length() > 30
				? baseVariantProductModel.getName().substring(0, 30) : baseVariantProductModel.getName());
		specificationRecord1.setOrderQuantity(String.valueOf(quantity));
		specificationRecord1.setOrderAssemblyCode(ProductReferenceTypeEnum.I.getCode());
		createN2GenericValues(specificationRecord1);
		listOfSpecificationRecord.add(specificationRecord1);
	}


	private void createOPCodesForLanes(final int specificationRecordCounter,
			final List<CatSpecificationRecord> listOfSpecificationRecord, final String laneType, final long quantity)
	{
		recordCounter++;
		final CatSpecificationRecord specificationRecord = new CatSpecificationRecord();
		specificationRecord.setSequenceNbr(specificationRecordSequenceList.get(specificationRecordCounter));
		String opCode;
		if (laneType.equals(LANE2TYPE))
		{
			opCode = Config.getParameter("orderdirector.lane2");
			specificationRecord.setProductDescription(LANE2ORDER);
		}
		else
		{
			opCode = Config.getParameter("orderdirector.lane3");
			specificationRecord.setProductDescription(LANE3ORDER);
		}

		specificationRecord.setMaterialNumber(getValueWithoutSpecialCharacters(opCode, SPECIAL_CHARACTER_REMOVAL_REGEX));

		specificationRecord.setOrderQuantity(String.valueOf(quantity));
		specificationRecord.setOrderAssemblyCode(ProductReferenceTypeEnum.I.getCode());
		createN2GenericValues(specificationRecord);
		listOfSpecificationRecord.add(specificationRecord);
	}

	/**
	 * @param specificationRecordCounter
	 * @param listOfSpecificationRecord
	 * @param partVariantProductModel
	 * @param quantity
	 */
	private void createAccessorySpecificationRecord(final int specificationRecordCounter,
			final List<CatSpecificationRecord> listOfSpecificationRecord, final PartVariantProductModel partVariantProductModel,
			final Long quantity)
	{
		final CatSpecificationRecord specificationRecord = new CatSpecificationRecord();
		specificationRecord.setSequenceNbr(specificationRecordSequenceList.get(specificationRecordCounter));
		specificationRecord.setMaterialNumber(
				getValueWithoutSpecialCharacters(partVariantProductModel.getCode(), SPECIAL_CHARACTER_REMOVAL_REGEX));
		specificationRecord.setProductDescription(partVariantProductModel.getName().length() > 30
				? partVariantProductModel.getName().substring(0, 30) : partVariantProductModel.getName());
		specificationRecord.setOrderQuantity(String.valueOf(quantity));
		specificationRecord.setOrderAssemblyCode(ProductReferenceTypeEnum.I.getCode());
		createN2GenericValues(specificationRecord);
		listOfSpecificationRecord.add(specificationRecord);
	}

	private void generateMandatoryPartRecords(final int specificationRecordCounter,
			final List<CatSpecificationRecord> listOfSpecificationRecord, final BaseVariantProductModel baseVariantProductModel,
			final Long quantity, final AbstractOrderEntryModel orderEntry, final OrderModel order) throws ResourceAccessException
	{
		final ProductModel salesModel = CatIntegrationUtils.getProduct(orderEntry);

		if (orderEntry.getConfigurable() == null || BooleanUtils.isFalse(orderEntry.getConfigurable()))
		{
			addLane1SpecificationRecords(specificationRecordCounter, listOfSpecificationRecord, baseVariantProductModel, quantity,
					orderEntry, order, salesModel);

		}
		else
		{
			final String fbcPartsOrderInfoQuery = Config.getParameter("orderdirector.FBCPartsOrderInfo.flexiQuery");
			final String orderID = order.getOrderGroupId();
			final List<FBCPartsOrderInfoModel> fbcPartsOrderInfoList = catOrderPlacedDao
					.getFBCPartsOrderInfoModelList(fbcPartsOrderInfoQuery, orderID);
			addLane23SpecificationRecords(specificationRecordCounter, listOfSpecificationRecord, fbcPartsOrderInfoList, quantity);
		}
	}
	private void addLane1SpecificationRecords(final int specificationRecordCounter,
			final List<CatSpecificationRecord> listOfSpecificationRecord, final BaseVariantProductModel baseVariantProductModel,
			final Long quantity, final AbstractOrderEntryModel orderEntry, final OrderModel order, final ProductModel salesModel)
			{
			final String laneType = order.getLaneType().getCode();
			if (laneType.equals(LaneTypeEnum.LANE1.getCode()))
				{
				if (null != salesModel.getControlFBCProcess() && salesModel.getControlFBCProcess())
					{
						try
						{
							final List<Selection> mandatoryParts = createMandatoryParts(order, orderEntry);
							addFBCMandatoryParts(mandatoryParts, specificationRecordCounter, listOfSpecificationRecord);
						}
						catch (final IllegalArgumentException illegalArgumentException)
						{
							LOGGER.error("FBC mandatory data is missing for " + salesModel.getCode(), illegalArgumentException);

						}
						catch (final Exception e)
						{
							LOGGER.error("excpetion during FBC call: ", e);
						}
					}
					else
					{
						createDCASpecificationRecord(specificationRecordCounter, listOfSpecificationRecord, baseVariantProductModel,
								quantity);
						final Collection<ProductReferenceModel> productReferences = getProductReferences(baseVariantProductModel);
						addOPcodesSpecificationRecords(productReferences, specificationRecordCounter, quantity,
								listOfSpecificationRecord);
					}
				}
				else
				{
					createDCASpecificationRecord(specificationRecordCounter, listOfSpecificationRecord, baseVariantProductModel,
							quantity);
				createOPCodesForLanes(specificationRecordCounter, listOfSpecificationRecord, laneType, quantity);
					final Collection<ProductReferenceModel> productReferences = getProductReferencesForConfigVariantProduct(
							configVariantProductModel);
					addOPcodesSpecificationRecords(productReferences, specificationRecordCounter, quantity, listOfSpecificationRecord);
				}


	}

	private void addFBCMandatoryParts(final List<Selection> mandatoryParts, int specificationRecordCounter,
			final List<CatSpecificationRecord> listOfSpecificationRecord)
	{
		for (final Selection mandatoryPart : mandatoryParts)
		{
			recordCounter++;
			specificationRecordCounter++;
			final CatSpecificationRecord specificationRecord = new CatSpecificationRecord();
			specificationRecord.setSequenceNbr(specificationRecordSequenceList.get(specificationRecordCounter));
			specificationRecord.setMaterialNumber(mandatoryPart.getReferenceNumber());
			specificationRecord.setProductDescription(mandatoryPart.getDescription());
			specificationRecord.setOrderQuantity(String.valueOf(mandatoryPart.getQuantity()));
			specificationRecord.setOrderAssemblyCode(mandatoryPart.getAssemblyIndicator());
			createN2GenericValues(specificationRecord);
			listOfSpecificationRecord.add(specificationRecord);

		}
	}

	private void addOPcodesSpecificationRecords(final Collection<ProductReferenceModel> productReferences,
			int specificationRecordCounter, final Long quantity, final List<CatSpecificationRecord> listOfSpecificationRecord)
	{
		if (!productReferences.isEmpty())
		{
			for (final ProductReferenceModel productReferenceModel : productReferences)
			{
				recordCounter++;
				specificationRecordCounter++;
				final ProductModel productModel = productReferenceModel.getTarget();
				final CatSpecificationRecord specificationRecord = new CatSpecificationRecord();
				specificationRecord.setSequenceNbr(specificationRecordSequenceList.get(specificationRecordCounter));
				specificationRecord.setMaterialNumber(productModel.getCode());
				specificationRecord.setProductDescription(
						productModel.getName().length() > 30 ? productModel.getName().substring(0, 30) : productModel.getName());
				specificationRecord.setOrderQuantity(quantity.toString());
				specificationRecord.setShipLessIndicator(EMPTY_STRING);
				final ProductReferenceTypeEnum partType = productReferenceModel.getReferenceType();
				getAssemblyCode(specificationRecord, partType);
				createN2GenericValues(specificationRecord);
				listOfSpecificationRecord.add(specificationRecord);

			}
		}
	}
	private void addLane23SpecificationRecords(int specificationRecordCounter,
			final List<CatSpecificationRecord> listOfSpecificationRecord, final List<FBCPartsOrderInfoModel> fbcPartsOrderInfoList,
			final Long quantity)
	{
		for (final FBCPartsOrderInfoModel fbcCPartsOrderInfoModel : fbcPartsOrderInfoList)
		{
			recordCounter++;
			specificationRecordCounter++;
			final CatSpecificationRecord specificationRecord = new CatSpecificationRecord();
			specificationRecord.setSequenceNbr(specificationRecordSequenceList.get(specificationRecordCounter));
			specificationRecord.setMaterialNumber(fbcCPartsOrderInfoModel.getPartId());
			specificationRecord.setProductDescription(fbcCPartsOrderInfoModel.getPartName().length() > 30
					? fbcCPartsOrderInfoModel.getPartName().substring(0, 30) : fbcCPartsOrderInfoModel.getPartName());
			specificationRecord.setOrderQuantity(quantity.toString());
			specificationRecord.setShipLessIndicator(EMPTY_STRING);
			specificationRecord.setOrderAssemblyCode(ProductReferenceTypeEnum.I.getCode());
			createN2GenericValues(specificationRecord);
			listOfSpecificationRecord.add(specificationRecord);
		}
	}

	private void getAssemblyCode(final CatSpecificationRecord specificationRecord, final ProductReferenceTypeEnum partType)
	{
		if (partType != null)
		{
			setOrderAssemblyCode(specificationRecord, partType);
		}
		else
		{
			specificationRecord.setOrderAssemblyCode(ProductReferenceTypeEnum.I.getCode());
		}
	}

	/**
	 * used to populate assembly code in the specification record based on part type
	 *
	 * @param specificationRecord
	 *           the N3 record
	 * @param partType
	 *           the part type
	 */
	private void setOrderAssemblyCode(final CatSpecificationRecord specificationRecord, final ProductReferenceTypeEnum partType)
	{
		if (ProductReferenceTypeEnum.I.getCode().equalsIgnoreCase(partType.getCode()))
		{
			specificationRecord.setOrderAssemblyCode(ProductReferenceTypeEnum.I.getCode());
		}
		else if (ProductReferenceTypeEnum.N.getCode().equalsIgnoreCase(partType.getCode()))
		{
			specificationRecord.setOrderAssemblyCode(ProductReferenceTypeEnum.N.getCode());
		}
		else if (ProductReferenceTypeEnum.D.getCode().equalsIgnoreCase(partType.getCode()))
		{
			specificationRecord.setOrderAssemblyCode(ProductReferenceTypeEnum.D.getCode());
		}
		else
		{
			specificationRecord.setOrderAssemblyCode(ProductReferenceTypeEnum.I.getCode());
		}
	}

	/**
	 * Created Specification records wit the Op code
	 *
	 * @param specificationRecordCounter
	 *           the counter to set the sequence no of the specification record
	 * @param quantity
	 *           the order quantity
	 * @param opcodeUtvCodeDescriptions
	 *           contains Opcode and description separated by '|'
	 * @return CatSpecificationRecord specification record having OPCODE
	 */
	private CatSpecificationRecord getCatSpecificationRecordWithOpCode(final int specificationRecordCounter, final Long quantity,
			final String opcodeUtvCodeDescriptions)
	{
		final CatSpecificationRecord specificationRecord = new CatSpecificationRecord();

		specificationRecord.setSequenceNbr(specificationRecordSequenceList.get(specificationRecordCounter));
		specificationRecord.setOrderQuantity(String.valueOf(quantity));

		specificationRecord.setOrderAssemblyCode("I");
		final String[] opCodeDetails = getOpCodeDetails(opcodeUtvCodeDescriptions);
		specificationRecord.setMaterialNumber(opCodeDetails[0]);
		specificationRecord.setProductDescription(opCodeDetails[1]);
		createN2GenericValues(specificationRecord);
		return specificationRecord;
	}

	/**
	 * Splits the opcodeUtvCodeDescription into Opcode and description
	 *
	 * @param opcodeUtvCodeDescription
	 *           contains Opcode and description separated by '|'
	 * @return array of strings
	 */

	private static String[] getOpCodeDetails(final String opcodeUtvCodeDescription)
	{
		return opcodeUtvCodeDescription.split(Pattern.quote("|"));
	}

	/**
	 *
	 * {@inheritDoc}
	 */

	@Override
	public List<CatNoteRecord> createNotesWithTruckloadRecord(final OrderModel order, final Map<String, String> truckLoadIDs)
	{

		final List<CatNoteRecord> noteRecordList = createNotesRecord(order);

		if (StringUtils.isNotEmpty(order.getTruckloadId()))
		{
			final CatNoteRecord truckLoadNoteRecord = new CatNoteRecord();
			truckLoadNoteRecord.setRecordType(NOTES_RECORD_TYPE);
			truckLoadNoteRecord.setSequenceNbr(specificationRecordSequenceList.get(mixandMatchcount));
			truckLoadNoteRecord.setOrderNoteTxt(TRUCKLOAD + EMPTY_SPACE + truckLoadIDs.get(order.getTruckloadId()) + EMPTY_SPACE
					+ DEALER + EMPTY_SPACE + order.getUnit().getUid());
			noteRecordList.add(truckLoadNoteRecord);
			recordCounter++;
		}
		return noteRecordList;
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public List<CatNoteRecord> createNotesRecord(final OrderModel order)
	{
		final List<CatNoteRecord> noteRecordList = new ArrayList<>();
		if (!order.getB2bcomments().isEmpty())
		{
			final List<B2BCommentModel> b2bcommentsRecords = (List<B2BCommentModel>) order.getB2bcomments();
			final B2BCommentModel b2bCommentModel = b2bcommentsRecords.get(0);
			final String notes = b2bCommentModel.getComment();
			final Configuration config = configurationService.getConfiguration();
			final int maxLength = config.getInt("orderdirector.string.split.length");
			final Pattern p = Pattern.compile("\\G\\s*(.{1," + maxLength + "})(?=\\s|$)", Pattern.DOTALL);
			final Matcher matcher = p.matcher(notes);
			while (matcher.find())
			{
				final CatNoteRecord catNoteRecord = new CatNoteRecord();
				catNoteRecord.setRecordType(NOTES_RECORD_TYPE);
				catNoteRecord.setSequenceNbr(specificationRecordSequenceList.get(notesCounter));
				catNoteRecord.setOrderNoteTxt(matcher.group(1));
				noteRecordList.add(catNoteRecord);
				notesCounter++;
				recordCounter++;
			}
		}
		return noteRecordList;
	}

	/**
	 *
	 * {@inheritDoc}
	 */

	@Override
	public List<CatDupOrderRecord> createDupOrderRecord(final List<List<String>> subSetsOfPONumbers)
	{

		final List<CatDupOrderRecord> catDupOrderRecordList = new ArrayList<>();
		for (final List<String> poNumberList : subSetsOfPONumbers)
		{
			final CatDupOrderRecord catDupOrderRecord = new CatDupOrderRecord();
			catDupOrderRecord.setRecordType(NOTES_RECORD_TYPE);
			catDupOrderRecord.setSequenceNbr(specificationRecordSequenceList.get(notesCounter));
			catDupOrderRecord.setOrderPONumbersList(poNumberList);
			catDupOrderRecordList.add(catDupOrderRecord);
			notesCounter++;
			recordCounter++;
		}
		return catDupOrderRecordList;
	}

	/**
	 *
	 * {@inheritDoc}
	 */

	public CatInlandShippingRecord createInlandShippingRecord(final OrderModel order,
			final Map<String, String> frieghtChargeCodeMap)
	{

		final CatInlandShippingRecord catInlandShippingRecord = new CatInlandShippingRecord();
		catInlandShippingRecord.setRecordType(INLAND_SHIPPING_RECORD_TYPE);

		catInlandShippingRecord.setSequenceNbr(SEQUENCE_NBR);
		final String dealercode = order.getUnit().getUid();

		if (frieghtChargeCodeMap.get(dealercode) == null)
		{
			catInlandShippingRecord.setFreightChargeCode(frieghtChargeCodeMap.get("default"));
		}
		else
		{
			catInlandShippingRecord.setFreightChargeCode(frieghtChargeCodeMap.get(dealercode));
		}
		catInlandShippingRecord.setTransPorationCode(TRANS_PORATION_CODE);
		catInlandShippingRecord.setShipLessThanTruckload(EMPTY_STRING);
		catInlandShippingRecord.setFreightRouting(FREIGHT_ROUTING);
		if (StringUtils.isNotBlank(order.getEntries().get(0).getCompatibleModel()))
		{
			catInlandShippingRecord.setOrderShipAfterDate(EMPTY_STRING);
		}
		else
		{
			catInlandShippingRecord.setOrderShipAfterDate(CatIntegrationUtils.getdateStringFormat(order.getShipToAfter()));
		}

		catInlandShippingRecord.setOrderShipBeforeDate(EMPTY_STRING);
		recordCounter++;
		return catInlandShippingRecord;
	}

	/**
	 *
	 * {@inheritDoc}
	 */

	public CatInlandShippingRecord createFreightCodesRecord(final OrderModel order, final ProductModel salesModel)
	{

		final CatInlandShippingRecord catInlandShippingRecord = new CatInlandShippingRecord();
		catInlandShippingRecord.setRecordType(INLAND_SHIPPING_RECORD_TYPE);

		catInlandShippingRecord.setSequenceNbr(SEQUENCE_NBR);

		final FreightChargeCodeEnum freightChargeCodeEnum = salesModel.getFreightChargeCode();
		catInlandShippingRecord.setFreightChargeCode(freightChargeCodeEnum.getCode());

		final FreightTransportCodeEnum freightTransportCodeEnum = salesModel.getFreightTransportCode();
		catInlandShippingRecord.setTransPorationCode(freightTransportCodeEnum.getCode());

		catInlandShippingRecord.setShipLessThanTruckload(EMPTY_STRING);
		catInlandShippingRecord.setFreightRouting(salesModel.getFreightRouteCode());
		catInlandShippingRecord.setOrderShipAfterDate(EMPTY_STRING);
		catInlandShippingRecord.setOrderShipBeforeDate(EMPTY_STRING);
		recordCounter++;
		return catInlandShippingRecord;
	}

	/**
	 *
	 * {@inheritDoc}
	 */

	@Override
	public CatInvoiceCriteriaRecord createInvoiceCriteriaRecord(final OrderModel orderModel,
			final Map<String, String> paymentTermsMap)
	{
		recordCounter++;
		final CatInvoiceCriteriaRecord catInvoiceCriteriaRecord = new CatInvoiceCriteriaRecord();
		final String dealercode = orderModel.getUnit().getUid();
		if (paymentTermsMap.get(dealercode) == null)
		{
			catInvoiceCriteriaRecord.setPaymentTermsCode(paymentTermsMap.get("default"));
		}
		else
		{
			catInvoiceCriteriaRecord.setPaymentTermsCode(paymentTermsMap.get(dealercode));
		}
		createInvoiceGenericValues(catInvoiceCriteriaRecord);
		return catInvoiceCriteriaRecord;
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public CatInvoiceCriteriaRecord createInvoiceRecordForSalesModel(final AbstractOrderEntryModel orderEntry)
	{
		recordCounter++;
		final CatInvoiceCriteriaRecord catInvoiceCriteriaRecord = new CatInvoiceCriteriaRecord();
		final ProductModel salesModel = CatIntegrationUtils.getProduct(orderEntry);
		final String paymentTerm = salesModel.getPaymentTerm();
		catInvoiceCriteriaRecord.setPaymentTermsCode(paymentTerm);
		createInvoiceGenericValues(catInvoiceCriteriaRecord);
		return catInvoiceCriteriaRecord;
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public CatTrailerRecord createTrailerRecord(final OrderModel order)
	{

		final CatTrailerRecord catTrailerRecord = new CatTrailerRecord();

		recordCounter++;
		catTrailerRecord.setRecordType(TRAILER_RECORD_TYPE);
		catTrailerRecord.setSequenceNbr(SEQUENCE_NBR);
		catTrailerRecord.setProPrietaryRecord(String.valueOf(recordCounter));
		recordCounter = 0;

		return catTrailerRecord;
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public String getValueWithoutSpecialCharacters(final String value, final String regex)
	{
		return value.replaceAll(regex, "");
	}

	/**
	 * @param baseVariantProductModel
	 * @return List<ProductReferenceModel>
	 */
	@SuppressWarnings("unchecked")
	public List<ProductReferenceModel> getProductReferences(final BaseVariantProductModel baseVariantProductModel)
	{
		return (List<ProductReferenceModel>) sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Collection<ProductReferenceModel> execute()
			{
				userService.setCurrentUser(userService.getAdminUser());
				return baseVariantProductModel.getProductReferences();
			}
		});
	}

	/**
	 * @param configVarinatProductModel
	 * @return List<ProductReferenceModel>
	 */
	@SuppressWarnings("unchecked")
	public List<ProductReferenceModel> getProductReferencesForConfigVariantProduct(
			final ConfigVariantProductModel configVarinatProductModel)
	{
		return (List<ProductReferenceModel>) sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Collection<ProductReferenceModel> execute()
			{
				userService.setCurrentUser(userService.getAdminUser());
				return configVarinatProductModel.getProductReferences();
			}
		});

	}

	private List<Selection> createMandatoryParts(final OrderModel order, final AbstractOrderEntryModel orderEntry) throws Exception
	{

		final FeatureData featureData = new FeatureData();
		final String firstFeatureSet = Config.getString("cat.ws.base.url.firstfeatureset", "CFG");
		final int requetMapModeCount = Config.getInt("cat.ws.base.url.requetmapper.mode.count", 2);

		final ProductModel salesModel = CatIntegrationUtils.getProduct(orderEntry);
		final String dealerCode = order.getUnit().getUid();
		final List<AddressModel> addresses = (List<AddressModel>) order.getUnit().getAddresses();
		final String countryCode = addresses.get(0).getCountry().getIsocode();

		final List<String> countryCodes = catOrderPlacedDao.getMarketingCompany(countryCode);

		featureData.setMarketingCompany(countryCodes.get(0));
		featureData.setDelearCode(dealerCode);
		featureData.setSalesModel(salesModel.getExternalName());
		featureData.setFirstFeatureSet(firstFeatureSet);

		final FBCCreateRequestMapper fbcCreateRequestMapper = new FBCCreateRequestMapper();

		final String featureFileID = catFBCConfiguratorService.getFeatureFileIDForSalesModel(featureData);

		fbcCreateRequestMapper.setFeatureFileId(featureFileID);
		fbcCreateRequestMapper.setMarketingCompany(countryCodes.get(0));
		fbcCreateRequestMapper.setDealerCode(dealerCode);
		fbcCreateRequestMapper.setMode(requetMapModeCount);

		final ConfigVariantProductModel configVariantProductModelLocal = (ConfigVariantProductModel) orderEntry.getProduct();
		final BaseVariantProductModel baseVariantProductModel = (BaseVariantProductModel) configVariantProductModelLocal
				.getBaseProduct();
		final List<Selection> invokeFBCRequest = catFBCConfiguratorService
				.getFBCComaptibleMandatoryMaterialsRequest(fbcCreateRequestMapper, baseVariantProductModel.getCode());
		return invokeFBCRequest;
	}

	private void createN2GenericValues(final CatSpecificationRecord specificationRecord)
	{
		specificationRecord.setRecordType(NEW_ORDER_SPECIFICATION_RECORD_TYPE);
		specificationRecord.setShipLessIndicator(EMPTY_STRING);
		specificationRecord.setNonStandardItemCode(EMPTY_STRING);
		specificationRecord.setActionCode(EMPTY_STRING);
	}

	private void createInvoiceGenericValues(final CatInvoiceCriteriaRecord catInvoiceCriteriaRecord)
	{
		catInvoiceCriteriaRecord.setInvoiceCriteriaRecordType(INVOICE_CRITERIA_RECORD_TYPE);
		catInvoiceCriteriaRecord.setInvoiceCriteriaSequenceNumber(SEQUENCE_NBR);
		catInvoiceCriteriaRecord.setPriceProtectionIndicator(EMPTY_STRING);
		catInvoiceCriteriaRecord.setAuthDocType(EMPTY_STRING);
		catInvoiceCriteriaRecord.setAuthDocNumber(EMPTY_STRING);
	}

}
