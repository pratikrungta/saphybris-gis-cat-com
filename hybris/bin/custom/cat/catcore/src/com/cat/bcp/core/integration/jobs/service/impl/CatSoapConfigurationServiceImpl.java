package com.cat.bcp.core.integration.jobs.service.impl;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.util.Config;
import de.hybris.platform.util.Utilities;

import java.net.MalformedURLException;
import java.net.URL;

import javax.annotation.Resource;
import javax.xml.namespace.QName;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.integration.constants.CatintegrationConstants;
import com.cat.bcp.core.integration.jobs.service.CatSoapConfigurationService;
import com.cat.dice.cmops.slaes.v1.export1.CMOPSOrderingService;
import com.cat.dice.cmops.slaes.v1.export1.CMOPSOrderingServiceExport1CMOPSOrderingServiceHttpService;
import com.cat.service.cmops.cmopscompatibilitycheckservice.v1.CheckCompatibility;
import com.cat.service.cmops.cmopscompatibilitycheckservice.v1.CheckCompatibilityExport1CheckCompatibilityHttpService;


public class CatSoapConfigurationServiceImpl implements CatSoapConfigurationService
{

	private static final Logger LOGGER = Logger.getLogger(CatSoapConfigurationServiceImpl.class);


	public static final String SERVICE = "CMOPSOrderingServiceExport1_CMOPSOrderingServiceHttpService";

	public static final String COMPATIBILITY_CHECK_SERVICE = "checkCompatibilityExport1_checkCompatibilityHttpService";

	private static final String PROD_WSDL_LOCATION = "orderdirector.webservice.prod.wsdl.location";

	private static final String STAGE_WSDL_LOCATION = "orderdirector.webservice.stage.wsdl.location";

	private static final String ENDPOINT_URL = "orderdirector.webservice.endpoint.url";

	private static final String PROD_COMPATIBILITY_CHECK_WSDL_LOCATION = "order.compatability.check.webservice.prod.wsdl.location";

	private static final String STAGECOMPATIBILITY_CHECK_WSDL_LOCATION = "order.compatability.check.webservice.stage.wsdl.location";

	private static final String COMPATIBILITY_CHECK_ENDPOINT_URL = "order.compatability.check.webservice.endpoint.url";

	@Resource
	private ConfigurationService configurationService;


	/**
	 * {@inheritDoc}
	 */
	@Override
	public CMOPSOrderingService getSOAPClientService() throws MalformedURLException
	{

		LOGGER.debug("get the service using the stub started ");

		URL wsdlUrl;
		String filePath;
		final String hostName = Config.getParameter("website.catSite.http");
		final String prodHostName = Config.getParameter("website.catsite.prod.http");


		if (StringUtils.contains(hostName, prodHostName))
		{
			filePath = CatintegrationConstants.FILEFORMAT
					+ Utilities.getPlatformConfig().getExtensionInfo(CatintegrationConstants.EXTENSIONNAME)
					.getExtensionDirectory().getAbsolutePath() + Config.getParameter(PROD_WSDL_LOCATION);
			wsdlUrl = new URL(filePath);
		}
		else
		{
			filePath = CatintegrationConstants.FILEFORMAT
					+ Utilities.getPlatformConfig().getExtensionInfo(CatintegrationConstants.EXTENSIONNAME)
					.getExtensionDirectory().getAbsolutePath() + Config.getParameter(STAGE_WSDL_LOCATION);
			wsdlUrl = new URL(filePath);
		}




		final QName qname = new QName(Config.getParameter(ENDPOINT_URL), SERVICE);
		CMOPSOrderingService cmopsOrderingService = null;

		final CMOPSOrderingServiceExport1CMOPSOrderingServiceHttpService webServiceClient = new CMOPSOrderingServiceExport1CMOPSOrderingServiceHttpService(
				wsdlUrl, qname);
		cmopsOrderingService = webServiceClient.getCMOPSOrderingServiceExport1CMOPSOrderingServiceHttpPort();

		LOGGER.debug("get the service using the stub ended ");
		return cmopsOrderingService;
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.cat.bcp.core.integration.jobs.service.CatSoapConfigurationService#getCompatibilityCheckSOAPClientService()
	 */
	@Override
	public CheckCompatibility getCompatibilityCheckSOAPClientService() throws MalformedURLException
	{


		LOGGER.debug("get the service using the stub started ");

		URL wsdlUrl;
		String filePath;
		final String hostName = Config.getParameter("website.catSite.http");
		final String prodHostName = Config.getParameter("website.catsite.prod.http");


		if (StringUtils.contains(hostName, prodHostName))
		{
			filePath = CatintegrationConstants.FILEFORMAT
					+ Utilities.getPlatformConfig().getExtensionInfo(CatintegrationConstants.EXTENSIONNAME)
					.getExtensionDirectory().getAbsolutePath() + Config.getParameter(PROD_COMPATIBILITY_CHECK_WSDL_LOCATION);
			wsdlUrl = new URL(filePath);
		}
		else
		{
			filePath = CatintegrationConstants.FILEFORMAT + Utilities.getPlatformConfig()
					.getExtensionInfo(CatintegrationConstants.EXTENSIONNAME)
					.getExtensionDirectory().getAbsolutePath() + Config.getParameter(STAGECOMPATIBILITY_CHECK_WSDL_LOCATION);
			wsdlUrl = new URL(filePath);
		}




		final QName qname = new QName(Config.getParameter(COMPATIBILITY_CHECK_ENDPOINT_URL), COMPATIBILITY_CHECK_SERVICE);

		CheckCompatibility checkCompatibility = null;

		final CheckCompatibilityExport1CheckCompatibilityHttpService webServiceClient = new CheckCompatibilityExport1CheckCompatibilityHttpService(
				wsdlUrl, qname);
		checkCompatibility = webServiceClient.getCheckCompatibilityExport1CheckCompatibilityHttpPort();

		LOGGER.debug("get the service using the stub ended ");
		return checkCompatibility;

	}
}
