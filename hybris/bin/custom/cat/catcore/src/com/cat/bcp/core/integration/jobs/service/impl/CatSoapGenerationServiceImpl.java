package com.cat.bcp.core.integration.jobs.service.impl;


import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionExecutionBody;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cat.bcp.core.enums.LaneTypeEnum;
import com.cat.bcp.core.integration.jobs.service.CatSoapConfigurationService;
import com.cat.bcp.core.integration.jobs.service.CatSoapGenerationService;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.CompatibilityInfoModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.core.integration.handler.CatServiceLogHandler;
import com.cat.dice.cmops.slaes.v1.export1.OrderMachinesRequest;
import com.cat.dice.cmops.slaes.v1.export1.OrderMachinesResponse;
import com.cat.dice.cmops.slaes.v1.export1.OrderRecordsType;
import com.cat.service.cmops.cmopscompatibilitycheckrequest.v1.CMOPSCompatibilityCheckRequest;
import com.cat.service.cmops.specs.v1.Specs;


/**
 * The class implements the methods defined by interface {@link CatSoapGenerationService}
 */
public class CatSoapGenerationServiceImpl implements CatSoapGenerationService
{
	private static final Logger LOGGER = Logger.getLogger(CatSoapGenerationServiceImpl.class);

	@Autowired
	private SessionService sessionService;

	@Autowired
	private UserService userService;

	@Resource
	private CatSoapConfigurationService catSoapConfigurationService;

	@Resource
	ModelService modelService;

	@Resource
	CatServiceLogHandler catServiceLogHandler;


	@Resource
	ConfigurationService configurationService;


	public CatServiceLogHandler getCatServiceLogHandler()
	{
		return catServiceLogHandler;
	}

	public void setCatServiceLogHandler(final CatServiceLogHandler catServiceLogHandler)
	{
		this.catServiceLogHandler = catServiceLogHandler;
	}

	/**
	 * {@inheritDoc}
	 */
	public void getOrderRecordsTypes(final List<String> orderRecords, final OrderMachinesRequest orderMachinesRequest)
	{
		LOGGER.debug("creating list of order record types for the request to be sent to Order Director");
		final List<OrderRecordsType> listOrderRecordsTypes = orderMachinesRequest.getOrderRecords();
		for (final String orderRecord : orderRecords)
		{
			final OrderRecordsType orderRecordsType = new OrderRecordsType();
			orderRecordsType.setOrderRecord(orderRecord);
			listOrderRecordsTypes.add(orderRecordsType);

		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCompatibityCheckValues(final ConfigVariantProductModel configVariantProduct,
			final CMOPSCompatibilityCheckRequest cmopsCompatibilityCheckRequest, final CompatibilityInfoModel compatibilityInfoModel)
	{
		final String dealerCode = Config.getParameter("order.compatibility.check.dealercode");
		cmopsCompatibilityCheckRequest.setDealerCode(dealerCode);
		cmopsCompatibilityCheckRequest.setMessageType("");

		final List<Specs> specificationsList = new ArrayList<>();

		final Collection<ProductReferenceModel> productReferences = getProductReferences(configVariantProduct);
		if (CollectionUtils.isNotEmpty(productReferences))
		{
			addSpecifications(specificationsList, productReferences);
		}

		addOpCodesDcaCodes(specificationsList, configVariantProduct, compatibilityInfoModel);

		cmopsCompatibilityCheckRequest.setSpecs(specificationsList);

	}

	/**
	 * The method converts the request in String format to be saved in logs table
	 *
	 * @param orderMachinesRequest
	 *           the request to be sent to Order Director
	 * @return the request in String format to be saved in logs
	 * @throws JAXBException
	 *            when there is error while marshalling and unmarshalling
	 */
	public String createXmlRequest(final OrderMachinesRequest orderMachinesRequest) throws JAXBException
	{
		final StringWriter request = new StringWriter();

		final JAXBContext requestContext = JAXBContext.newInstance(orderMachinesRequest.getClass());
		final Marshaller requestMarshaller = requestContext.createMarshaller();
		requestMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		final JAXBElement<OrderMachinesRequest> orderMachinesReq = new JAXBElement<>(new QName("orderMachinesRequest"),
				OrderMachinesRequest.class, orderMachinesRequest);
		requestMarshaller.marshal(orderMachinesReq, request);
		return request.toString();
	}


	public String createCompatibilityCheckXmlRequest(final CMOPSCompatibilityCheckRequest cmopsCompatibilityCheckRequest)
			throws JAXBException
	{
		final StringWriter request = new StringWriter();

		final JAXBContext requestContext = JAXBContext.newInstance(cmopsCompatibilityCheckRequest.getClass());
		final Marshaller requestMarshaller = requestContext.createMarshaller();
		requestMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		final JAXBElement<CMOPSCompatibilityCheckRequest> cmopsCompatibilityCheckRequestJAXB = new JAXBElement<>(
				new QName("cmopsCompatibilityCheckRequest"), CMOPSCompatibilityCheckRequest.class, cmopsCompatibilityCheckRequest);
		requestMarshaller.marshal(cmopsCompatibilityCheckRequestJAXB, request);
		return request.toString();
	}

	/**
	 * The method converts the response in String format to be saved in logs table
	 *
	 * @param orderMachinesResponse
	 *           the response from the Order Director
	 * @return response in String format
	 * @throws JAXBException
	 *            when there is an error during the marshalling and unmarshalling
	 */
	public String createReXmlResponse(final OrderMachinesResponse orderMachinesResponse) throws JAXBException
	{
		final StringWriter request = new StringWriter();
		final JAXBContext requestContext = JAXBContext.newInstance(orderMachinesResponse.getClass());
		final Marshaller requestMarshaller = requestContext.createMarshaller();
		requestMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		final JAXBElement<OrderMachinesResponse> orderMachinesRes = new JAXBElement<>(new QName("orderMachinesResponse"),
				OrderMachinesResponse.class, orderMachinesResponse);

		requestMarshaller.marshal(orderMachinesRes, request);
		return request.toString();
	}

	/**
	 * @param baseVariantProductModel
	 * @return List<ProductReferenceModel>
	 */
	@SuppressWarnings("unchecked")
	public List<ProductReferenceModel> getProductReferences(final ConfigVariantProductModel configVariantProductModel)
	{
		return (List<ProductReferenceModel>) sessionService.executeInLocalView(new SessionExecutionBody()
		{
			@Override
			public Collection<ProductReferenceModel> execute()
			{
				userService.setCurrentUser(userService.getAdminUser());
				return configVariantProductModel.getProductReferences();
			}
		});
	}

	private void addSpecifications(final List<Specs> specificationsList, final Collection<ProductReferenceModel> productReferences)
	{


		for (final ProductReferenceModel productReferenceModel : productReferences)
		{
			final Specs specs = new Specs();

			final ProductModel productModel = productReferenceModel.getTarget();
			if (productModel != null)
			{
				specs.setProductReferenceNumber(productModel.getCode());
				final Integer quantity = productReferenceModel.getQuantity();
				if (quantity != null)
				{
					specs.setOrderQuantity(quantity);
				}
				else
				{
					specs.setOrderQuantity(1);
				}

				specs.setOrderAssemblyCode("I");
				specificationsList.add(specs);
			}
		}
	}

	private void addOpCodesDcaCodes(final List<Specs> specificationsList, final ConfigVariantProductModel configVariantProduct,
			final CompatibilityInfoModel compatibilityInfoModel)
	{

		final LaneTypeEnum laneTypeEnum = compatibilityInfoModel.getLaneType();
		String opCode;


		if (LaneTypeEnum.LANE2.getCode().equalsIgnoreCase(laneTypeEnum.getCode()))
		{
			opCode = Config.getParameter("order.compatibility.check.lane2.opcode");
		}
		else
		{
			opCode = Config.getParameter("order.compatibility.check.lane3.opcode");
		}



		final List<String> productReferenceCodeList = new ArrayList<>();
		for (final Specs specs : specificationsList)
		{
			productReferenceCodeList.add(specs.getProductReferenceNumber());
		}

		if (!productReferenceCodeList.contains(opCode))
		{
			final Specs opCodeSpecs = new Specs();
			opCodeSpecs.setProductReferenceNumber(opCode);
			opCodeSpecs.setOrderQuantity(1);
			opCodeSpecs.setOrderAssemblyCode("I");
			specificationsList.add(opCodeSpecs);
		}


		final BaseVariantProductModel baseVariantProductModel = (BaseVariantProductModel) configVariantProduct.getBaseProduct();
		if (baseVariantProductModel != null && !productReferenceCodeList.contains(baseVariantProductModel.getCode()))
		{
				final Specs dcaSpecs = new Specs();
			dcaSpecs.setProductReferenceNumber(baseVariantProductModel.getCode());
				dcaSpecs.setOrderQuantity(1);
				dcaSpecs.setOrderAssemblyCode("I");
				specificationsList.add(dcaSpecs);
		}


	}

}

