/**
 *
 */
package com.cat.bcp.core.integration.service;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;


/**
 * @author sjeedula This interface provides methods to fetch configurable product details from FBC API
 */
public interface CatFBCService
{


	/**
	 * Method returns response for SelectAPI response
	 * 
	 * @param dca
	 *           dca code i.e. Sales model code
	 * @param laneCode
	 *           The lane type selected
	 * @return response
	 * @throws Exception
	 */
	String initiateFBCAPIRequest(String dca, String laneCode) throws Exception;


	/**
	 * Method returns response for SelectAPI Response.
	 *
	 * @param configId
	 *           : input configId
	 * @param featureCode
	 *           : input featureCode
	 * @param productCode
	 *           : input productCode
	 * @return JSON Formatted data : returns parts for the featureCode
	 */
	String getSelectNextResponse(String configId, String featureCode, String productCode);


	/**
	 * @param dcaCode
	 *           could be dca or config product code
	 * @return Product - model
	 */
	List<ProductModel> getPartVariants(final String dcaCode);


}
