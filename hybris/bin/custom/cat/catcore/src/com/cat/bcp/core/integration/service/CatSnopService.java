/**
 *
 */
package com.cat.bcp.core.integration.service;

import de.hybris.platform.commercefacades.order.data.CartData;

import com.cat.core.integration.snop.response.CatSnopResponseMapper;


/**
 * @author sjeedula
 *
 */
public interface CatSnopService
{

	/**
	 * Method returns response for SelectAPI Response.
	 *
	 * @param cartData
	 *           - cart data
	 *
	 *
	 * @return JSON Formatted data : returns JSON Formatted data
	 */
	CatSnopResponseMapper validateSnopRequest(CartData cartData);




}
