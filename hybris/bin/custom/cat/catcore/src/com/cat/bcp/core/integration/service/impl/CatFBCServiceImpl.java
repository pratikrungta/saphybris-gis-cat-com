/**
 *
 */
package com.cat.bcp.core.integration.service.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.integration.service.CatFBCService;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.core.integration.fbc.request.FBCCreateRequestMapper;
import com.cat.core.integration.fbc.request.Feature;
import com.cat.core.integration.fbc.request.FeatureData;
import com.cat.core.integration.fbc.response.FBCResponseMapper;
import com.cat.core.integration.model.SalesAreaLookupModel;
import com.cat.core.integration.service.CatFBCConfiguratorService;
import com.cat.core.integration.utils.CatRestClientUtil;


/**
 * @author sjeedula
 *
 */
public class CatFBCServiceImpl implements CatFBCService
{
	@Resource(name = "catFBCConfiguratorService")
	CatFBCConfiguratorService catFBCConfiguratorService;
	@Autowired
	private CommonI18NService commonI18NService;
	@Autowired
	private ProductService productService;
	@Resource(name = "userService")
	private UserService userService;
	@Resource(name = "flexibleSearchService")
	FlexibleSearchService flexibleSearchService;
	@Resource(name = "sessionService")
	private SessionService sessionService;

	private static final String SALESAREA_LOOKUP = "select {pk} from {SalesAreaLookup} where {country} = ?country";
	private static final Logger LOG = Logger.getLogger(CatFBCServiceImpl.class);



	/**
	 * {@inheritDoc}
	 */
	@Override
	public String initiateFBCAPIRequest(final String dca, final String laneCode) throws Exception
	{
		FBCCreateRequestMapper fbcCreateRequestMapper = null;
		try
		{
			final FeatureData featureData = new FeatureData();
			fbcCreateRequestMapper = new FBCCreateRequestMapper();
			final B2BCustomerModel customerModel = (B2BCustomerModel) userService.getCurrentUser();
			final String uid = customerModel.getDefaultB2BUnit().getUid();
			AddressModel soldToAddress = null;
			if (CollectionUtils.isEmpty(customerModel.getDefaultB2BUnit().getAddresses()))
			{
				throw new IllegalArgumentException("Address not found for the Dealer " + uid);
			}
			final Iterator<AddressModel> iterator = customerModel.getDefaultB2BUnit().getAddresses().iterator();
			if (iterator.hasNext())
			{
				soldToAddress = iterator.next();
				if (null == soldToAddress.getCountry())
				{
					throw new IllegalArgumentException("Please Provide Country For the Dealer" + uid + " to continue FBC Flow ");
				}
				LOG.info(soldToAddress.getCountry().getIsocode());
			}
			final FlexibleSearchQuery salesAreaQuery = new FlexibleSearchQuery(SALESAREA_LOOKUP);
			salesAreaQuery.addQueryParameter(SalesAreaLookupModel.COUNTRY,
					soldToAddress.getCountry().getIsocode());
			final SearchResult<SalesAreaLookupModel> salesAreaSearchResults = flexibleSearchService.search(salesAreaQuery);
			if (CollectionUtils.isNotEmpty(salesAreaSearchResults.getResult()))
			{
				featureData.setMarketingCompany(salesAreaSearchResults.getResult().get(0).getMarketingCompany());
			}
			final BaseVariantProductModel baseProductModel = (BaseVariantProductModel) productService.getProductForCode(dca);
			final ProductModel salesModel = baseProductModel.getBaseProduct();
			featureData.setDelearCode(uid);
			featureData.setSalesModel(salesModel.getCode());
			featureData.setFirstFeatureSet("CFG");
			final String featureFileID = catFBCConfiguratorService.getFeatureFileIDForSalesModel(featureData);
			fbcCreateRequestMapper.setFeatureFileId(featureFileID);
			if (CollectionUtils.isNotEmpty(salesAreaSearchResults.getResult()))
			{
				fbcCreateRequestMapper.setMarketingCompany(salesAreaSearchResults.getResult().get(0).getMarketingCompany());
			}
			fbcCreateRequestMapper.setDealerCode(uid);
			fbcCreateRequestMapper.setMode(2);
		}
		catch (final Exception e)
		{
			LOG.log(Level.ERROR, "Exception" + e.getMessage(), e);
		}
		return catFBCConfiguratorService.initiateFBCSelectAPIRequest(fbcCreateRequestMapper, dca, laneCode);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductModel> getPartVariants(final String dcaCode)
	{
		final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.GET_PARTVARIANTS_FOR_BASEVARIANT);
		query.addQueryParameter("dcaCode", dcaCode);
		final SearchResult<ProductModel> searchResult = flexibleSearchService.search(query);
		return searchResult.getResult();
	}

	private Boolean isPartExists(final String productCode, final String partId)
	{
		List<ProductModel> result;
		try
		{
			final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.IS_PART_EXISTS);
			query.addQueryParameter("productCode", productCode);
			query.addQueryParameter("partId", partId);
			final SearchResult<ProductModel> searchResult = flexibleSearchService.search(query);
			result = searchResult.getResult();
		}
		catch (final Exception e)
		{
			result = Collections.emptyList();
			LOG.log(Level.ERROR, "Exception occurred while validating existence of part "+e.getMessage(), e);
		}
		return CollectionUtils.isEmpty(result) ? Boolean.FALSE : Boolean.TRUE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getSelectNextResponse(final String configId, final String featureCode, final String productCode)
	{
		String jsonResponse = null;
		final FBCResponseMapper selectNextResponse = catFBCConfiguratorService.getSelectNextResponse(configId, featureCode);
		if (null != selectNextResponse.getFeatureSet() && null == selectNextResponse.getEdhError())
		{
			final List<Feature> features = selectNextResponse.getFeatureSet().getFeatures();
			for (final Feature feature : features)
			{
				if (BooleanUtils.isTrue(isPartExists(productCode, feature.getReferenceNumber())))
				{
					return this.getSelectNextResponse(configId, feature.getFeatureCode(), productCode);
				}

			}
		}
		if (null == selectNextResponse.getFeatureSet() && null == selectNextResponse.getEdhError())
		{
			sessionService.setAttribute("selections", selectNextResponse.getSelections());
		}
		jsonResponse = CatRestClientUtil.getJSONFormattedString(selectNextResponse, jsonResponse, "getSelectNextResponse");
		return jsonResponse;
	}

}
