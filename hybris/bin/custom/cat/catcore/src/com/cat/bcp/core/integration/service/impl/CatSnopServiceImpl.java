/**
 *
 */
package com.cat.bcp.core.integration.service.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;

import com.cat.bcp.core.integration.service.CatSnopService;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.core.integration.service.CatSnopValidatorService;
import com.cat.core.integration.snop.request.CatSnopRequestMapper;
import com.cat.core.integration.snop.request.CatSnopSubRequest;
import com.cat.core.integration.snop.response.CatSnopResponseMapper;
import com.cat.core.integration.snop.response.CatSnopSubResponse;
import com.google.common.collect.Lists;


/**
 * @author sjeedula
 *
 */
public class CatSnopServiceImpl implements CatSnopService
{
	@Resource(name = "catSnopValidatorService")
	CatSnopValidatorService catSnopValidatorService;
	@Autowired
	private CartService cartService;
	@Autowired
	private ProductService productService;
	@Resource(name = "userService")
	private UserService userService;
	@Resource(name = "flexibleSearchService")
	FlexibleSearchService flexibleSearchService;
	@Resource(name = "sessionService")
	private SessionService sessionService;

	private static final String LANE2 = "LANE2";
	private static final String LANE3 = "LANE3";



	/**
	 * {@inheritDoc}
	 */
	@Override
	public CatSnopResponseMapper validateSnopRequest(final CartData cartData)
	{
		final String transactioCode = Config.getParameter("cat.snop.transaction.code");
		final Map<ProductData, List<OrderEntryData>> groupByProduct = cartData.getEntries().stream()
				.collect(Collectors.groupingBy(OrderEntryData::getProduct));

		final String[] dca =
		{ "" };
		final int[] qty =
		{ 0 };
		final Map<String, Integer> dcaQtyMap = new HashMap();
		groupByProduct.forEach((product, productDataList) -> {
			final List<OrderEntryData> oedList = groupByProduct.get(product);
			final ProductModel productModel = productService.getProduct(product.getCode());
			populateDcaQtyMap(oedList, productModel, dca, qty, dcaQtyMap, product);

		});

		final List<CatSnopSubRequest> reqList = populateSubRequest(dcaQtyMap);
		final CatSnopRequestMapper catSnopRequestMapper = new CatSnopRequestMapper();
		CatSnopResponseMapper catSnopResponseMapper = new CatSnopResponseMapper();
		final List<CatSnopSubResponse> responseList = new ArrayList();

		final List<List<CatSnopSubRequest>> splitList = Lists.partition(reqList, 10);
		for (int i = 0; i < splitList.size(); i++)
		{
			catSnopRequestMapper.setMessageArea(splitList.get(i));
			catSnopRequestMapper.setTransactionCode(transactioCode);
			catSnopResponseMapper = catSnopValidatorService.validateSnopRequest(catSnopRequestMapper);
			if (catSnopResponseMapper.getMessageArea() != null)
			{
				responseList.addAll(catSnopResponseMapper.getMessageArea());
			}
		}
		catSnopResponseMapper.setMessageArea(responseList);


		return catSnopResponseMapper;

	}



	/**
	 * @param oedList
	 *           - order entry list
	 * @param productModel
	 *           - product model
	 * @param dca
	 *           - dca
	 * @param qty
	 *           - qty
	 * @param dcaQtyMap
	 *           - dc qty map
	 * @param product
	 *           - product
	 */
	private void populateDcaQtyMap(final List<OrderEntryData> oedList, final ProductModel productModel, final String[] dca,
			final int[] qty, final Map<String, Integer> dcaQtyMap, final ProductData product)
	{
		for (final OrderEntryData oed : oedList)
		{
			if (productModel instanceof ConfigVariantProductModel)
			{
				if (product.getBaseProduct().equalsIgnoreCase(dca[0]))
				{
					qty[0] += oed.getQuantity().intValue();
					dca[0] = replaceHyphenIfAny(dca[0]);

					dcaQtyMap.put(dca[0], qty[0]);
				}
				else
				{
					dca[0] = product.getBaseProduct();
					qty[0] = oed.getQuantity().intValue();
					dca[0] = replaceHyphenIfAny(dca[0]);
					dcaQtyMap.put(dca[0], qty[0]);

				}
			}
		}

	}



	/**
	 * This method replaces Hyphen if any
	 *
	 * @param string
	 *           - dca
	 */
	private String replaceHyphenIfAny(final String dca)
	{
		String modifiedDca = dca;
		if (dca.indexOf('-') != -1)
		{
			modifiedDca = dca.replace("-", "");
		}
		return modifiedDca;
	}



	/**
	 * @param dcaQtyMap
	 *           - dca qty map
	 * @param qty
	 *           - qty
	 * @param dca
	 *           - dca
	 * @return list - sub req list
	 */
	private List<CatSnopSubRequest> populateSubRequest(final Map<String, Integer> dcaQtyMap)
	{
		final Iterator<Entry<String, Integer>> iterator = dcaQtyMap.entrySet().iterator();
		CatSnopSubRequest catSnopSubRequest = null;
		final List<CatSnopSubRequest> reqList = new ArrayList();
		while (iterator.hasNext())
		{
			final Entry<String, Integer> dcaKey = iterator.next();
			catSnopSubRequest = new CatSnopSubRequest();
			final B2BCustomerModel customerModel = (B2BCustomerModel) userService.getCurrentUser();
			final String uid = customerModel.getDefaultB2BUnit().getUid();
			catSnopSubRequest.setDealerCode(uid);

			catSnopSubRequest.setTopLevelPartNumber(dcaKey.getKey());

			final int tempQty = dcaQtyMap.get(dcaKey.getKey());
			final String paddedQty = String.format("%03d", tempQty);

			catSnopSubRequest.setOrderedQty(paddedQty);
			String laneType = cartService.getSessionCart().getLaneType() != null
					? cartService.getSessionCart().getLaneType().toString() : "";
			switch (laneType)
			{
				case LANE2:
					laneType = "2";
					break;
				case LANE3:
					laneType = "3";
					break;
				default:
					break;
			}
			catSnopSubRequest.setLaneType(laneType);
			reqList.add(catSnopSubRequest);
		}
		return reqList;
	}
}