/**
 *
 */
package com.cat.bcp.core.interceptor;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.ordersplitting.model.VendorModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.storelocator.pos.PointOfServiceService;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.cat.bcp.core.enums.WarehouseTypeEnum;


/**
 * @author ravjonnalagadda
 *
 */
public class B2BUnitPrepareInterceptor implements PrepareInterceptor<B2BUnitModel>
{
	private static final Logger LOG = Logger.getLogger(B2BUnitPrepareInterceptor.class);

	private ModelService modelService;
	private FlexibleSearchService flexibleSearchService;
	private PointOfServiceService pointOfServiceService;

	@Override
	public void onPrepare(final B2BUnitModel unitModel, final InterceptorContext ctx)
	{
		try
		{
			if ((Boolean.parseBoolean(Config.getParameter("enable.prepare.interceptor"))) && ctx.isNew(unitModel))
			{
				final String unitCode = (null != unitModel.getUid()) ? unitModel.getUid() : "";

				final PointOfServiceModel posModel = getPointOfServiceService().getPointOfServiceForName(unitCode);
				if (null == posModel)
				{
					createPosAndWarehouse(unitModel);
				}
				LOG.debug("B2B Unit created :" + unitCode);
			}
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage(), e);
		}
	}

	protected void createPosAndWarehouse(final B2BUnitModel unitModel)
	{
		final String unitCode = (null != unitModel.getUid()) ? unitModel.getUid() : "";

		final Set<PointOfServiceModel> posSet = new HashSet<>();

		final PointOfServiceModel pos = getModelService().create(PointOfServiceModel.class);
		pos.setName(unitCode);
		pos.setType(PointOfServiceTypeEnum.POS);
		pos.setDisplayName(formatName(unitModel.getName()));
		LOG.debug("POS created for B2B Unit : " + unitCode);

		unitModel.setPointsOfService(posSet);

		final List<WarehouseModel> warehouses = new ArrayList<>();

		final String vendorCode = Config.getString("cat.vendorCode", "caterpillarVendor");
		final VendorModel vendor = new VendorModel();
		vendor.setCode(vendorCode);
		final VendorModel vendorModel = getFlexibleSearchService().getModelByExample(vendor);

		if (null != vendorModel)
		{
			final WarehouseModel warehouse = getModelService().create(WarehouseModel.class);
			warehouse.setCode(unitCode);
			warehouse.setName(formatName(unitModel.getName()));
			warehouse.setVendor(vendorModel);
			warehouse.setWarehouseType(WarehouseTypeEnum.WAREHOUSE);
			LOG.debug("Warehouse created for B2B Unit :" + unitCode);
			warehouses.add(warehouse);

			pos.setWarehouses(warehouses);
			posSet.add(pos);
		}

	}

	protected String formatName(final String string)
	{
		return string.replaceAll("^\"|\"$", "");
	}

	/**
	 * @return the pointOfServiceService
	 */
	public PointOfServiceService getPointOfServiceService()
	{
		return pointOfServiceService;
	}

	/**
	 * @param pointOfServiceService
	 *           the pointOfServiceService to set
	 */
	public void setPointOfServiceService(final PointOfServiceService pointOfServiceService)
	{
		this.pointOfServiceService = pointOfServiceService;
	}

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

}
