/**
 *
 */
package com.cat.bcp.core.interceptor;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.RemoveInterceptor;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Date;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;


/**
 * @author asomjal
 *
 */
public class BaseVariantProductRemoveInterceptor implements RemoveInterceptor<BaseVariantProductModel>
{

	private static final Logger LOG = Logger.getLogger(BaseVariantProductRemoveInterceptor.class);

	@Resource(name = "userService")
	private UserService userService;

	/**
	 * This method is executed when an entry is removed from BaseVariantProduct table.
	 *
	 * @param baseVariantProductModel
	 *           BaseVariantProductModel Object
	 * @param ctx
	 *           InterceptorContext Object
	 */
	@Override
	public void onRemove(final BaseVariantProductModel baseVariantProductModel, final InterceptorContext ctx)
			throws InterceptorException
	{
		try
		{
			LOG.info("**********************************************************************");
			LOG.info("START:::::BaseVariantProductRemoveInterceptor Remove Interceptor:::::::");
			if (baseVariantProductModel instanceof ConfigVariantProductModel)
			{
				LOG.info("Config Product is removed with code - " + baseVariantProductModel.getCode());
				LOG.info("BaseProduct - " + (baseVariantProductModel.getBaseProduct() != null
						? baseVariantProductModel.getBaseProduct().getCode() : "BaseProduct object is Null"));
			}
			else
			{
				LOG.info("Base Product is removed with code - " + baseVariantProductModel.getCode());
				LOG.info("SalesModel - " + (baseVariantProductModel.getBaseProduct() != null
						? baseVariantProductModel.getBaseProduct().getCode() : "SalesModel object is Null"));
			}
			final Date date = new Date();
			LOG.info("Time when removed - " + date.toString());
			final UserModel userModel = userService.getCurrentUser();
			LOG.info("User - " + (userModel != null ? userService.getCurrentUser().getUid() : "UserModel object is Null"));
			LOG.info("END:::::BaseVariantProductRemoveInterceptor Remove Interceptor:::::::");
		}
		catch (final Exception e)
		{
			LOG.error("Exception occured in BaseVariantProductRemoveInterceptor class", e);
		}
	}
}
