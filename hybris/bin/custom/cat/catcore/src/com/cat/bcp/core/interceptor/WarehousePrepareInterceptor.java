/**
 *
 */
package com.cat.bcp.core.interceptor;

import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.PrepareInterceptor;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.storelocator.pos.PointOfServiceService;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.cat.bcp.core.enums.WarehouseTypeEnum;
import com.cat.bcp.core.servicelayer.DealerAddressService;


/**
 * @author bidavda
 *
 */
public class WarehousePrepareInterceptor implements PrepareInterceptor<WarehouseModel>
{
	private static final Logger LOG = Logger.getLogger(WarehousePrepareInterceptor.class);

	private ModelService modelService;
	private PointOfServiceService pointOfServiceService;
	private DealerAddressService dealerAddressService;

	@Override
	public void onPrepare(final WarehouseModel warehouseModel, final InterceptorContext ctx)
	{
		try
		{
			if ((Boolean.parseBoolean(Config.getParameter("enable.prepare.interceptor"))) && ctx.isNew(warehouseModel)
					&& !(WarehouseTypeEnum.WAREHOUSE.equals(warehouseModel.getWarehouseType())))
			{
				final List<WarehouseModel> warehouses = new ArrayList<>();
				warehouses.add(warehouseModel);

				final String warehouseCode = (null != warehouseModel.getCode()) ? warehouseModel.getCode() : "";
				warehouseModel.setName(formatName(warehouseModel.getName()));
				final PointOfServiceModel posModel = new PointOfServiceModel();
				posModel.setName(warehouseCode);

				final PointOfServiceModel pos = getPointOfServiceService().getPointOfServiceForName(warehouseCode);
				if (null == pos)
				{
					createPos(warehouseModel);
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage(), e);
		}
	}

	protected void createPos(final WarehouseModel warehouseModel)
	{
		final String warehouseCode = (null != warehouseModel.getCode()) ? warehouseModel.getCode() : "";

		final Set<PointOfServiceModel> posSet = new HashSet<>();

		final AddressModel posAddress = getDealerAddressService().getPosAddressByPosId(warehouseCode);

		final PointOfServiceModel pointOfService = getModelService().create(PointOfServiceModel.class);
		pointOfService.setName(warehouseCode);
		pointOfService.setType(PointOfServiceTypeEnum.WAREHOUSE);
		pointOfService.setDisplayName(formatName(warehouseModel.getName()));
		if (null != posAddress)
		{
			pointOfService.setAddress(posAddress);
		}
		posSet.add(pointOfService);
		warehouseModel.setPointsOfService(posSet);
		LOG.debug("POS created for Warehouse : " + warehouseCode);

	}

	protected String formatName(final String string)
	{
		return string.replaceAll("^\"|\"$", "");
	}

	/**
	 * @return the pointOfServiceService
	 */
	public PointOfServiceService getPointOfServiceService()
	{
		return pointOfServiceService;
	}

	/**
	 * @param pointOfServiceService
	 *           the pointOfServiceService to set
	 */
	public void setPointOfServiceService(final PointOfServiceService pointOfServiceService)
	{
		this.pointOfServiceService = pointOfServiceService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the dealerAddressService
	 */
	public DealerAddressService getDealerAddressService()
	{
		return dealerAddressService;
	}

	/**
	 * @param dealerAddressService
	 *           the dealerAddressService to set
	 */
	public void setDealerAddressService(final DealerAddressService dealerAddressService)
	{
		this.dealerAddressService = dealerAddressService;
	}
}
