package com.cat.bcp.core.job;



import de.hybris.platform.cronjob.jalo.AbortCronJobException;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;

/**
 *
 */
public abstract class CatAbstractAbortableCronJob extends AbstractJobPerformable<CronJobModel> {


    @Override
    public boolean isAbortable() {
        return true;
    }

    /**
     * @param cronJobModel
     * @throws AbortCronJobException
     */
    public void checkIfRequestedToAbortCron(final CronJobModel cronJobModel) throws AbortCronJobException {

            if (clearAbortRequestedIfNeeded(cronJobModel)) {
                throw new AbortCronJobException( " CatAbortableCronJob is aborted");
            }

        }
    }


