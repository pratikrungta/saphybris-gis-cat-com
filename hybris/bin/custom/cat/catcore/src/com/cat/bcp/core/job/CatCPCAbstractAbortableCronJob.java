package com.cat.bcp.core.job;

import com.cat.bcp.core.model.CpcCronJobModel;
import de.hybris.platform.cronjob.jalo.AbortCronJobException;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;

/**
 * The class contains the implementation of the methods to abort the cronjob if required.
 */
public  abstract class CatCPCAbstractAbortableCronJob extends AbstractJobPerformable<CpcCronJobModel> {

    @Override
    public boolean isAbortable() {
        return true;
    }

    /**
     * The method is used to abort the cron job and throw an abort exception
     * @param cronJobModel the cron job model
     * @throws AbortCronJobException the exception when the cron job is aborted.
     */
    public void checkIfRequestedToAbortCron(final CpcCronJobModel cronJobModel) throws AbortCronJobException {

        if (clearAbortRequestedIfNeeded(cronJobModel)) {
            throw new AbortCronJobException( " CatCPCAbstractAbortableCronJob is aborted");
        }

    }
}