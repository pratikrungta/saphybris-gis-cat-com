package com.cat.bcp.core.job;

import com.cat.bcp.core.exception.CatCoreException;
import com.cat.bcp.core.model.CpcCronJobModel;
import com.cat.bcp.core.servicelayer.CatCPCProductService;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.apache.log4j.Logger;

import javax.annotation.Resource;

/**
 * The cronjob is used to generate the classification data for the UTv product specifications
 * after parsing the CPC xml
 */
public class CatCPCImportCronJob extends  CatCPCAbstractAbortableCronJob {

    private static final Logger LOG = Logger.getLogger(CatCPCImportCronJob.class);

    @Resource(name="catCPCProductService")
    CatCPCProductService catCPCProductService;

    /*
     * (non-Javadoc)
     *
     * @see
     * de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.CronJobModel)
     */
    @Override
    public PerformResult perform(final CpcCronJobModel cronJobModel)
    {
        try
        {
            catCPCProductService.productSpecificationService(this, cronJobModel);
        }
        catch (final CatCoreException catCoreException)
        {
            LOG.error("Exception while running the cron job: " + catCoreException);
            return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);
        }


        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }
}
