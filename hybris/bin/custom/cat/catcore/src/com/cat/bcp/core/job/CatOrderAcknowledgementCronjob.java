package com.cat.bcp.core.job;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.cat.bcp.core.servicelayer.CatOrderAcknowledgementService;


/**
 * This cronjob is to fetch the orders to send acknowledgment order email to the user.
 * @author ravjonnalagadda
 *
 */
public class CatOrderAcknowledgementCronjob extends CatAbstractAbortableCronJob
{
	private static final Logger LOG = Logger.getLogger(CatOrderAcknowledgementCronjob.class);
	@Resource(name = "catOrderAcknowledgementService")
	private CatOrderAcknowledgementService orderAcknowledgementService;


	@Override
	public PerformResult perform(final CronJobModel cronJobModel)
	{
		LOG.debug("#########Inside perform method of Cat Order Acknowledgment Cronjob######");
		orderAcknowledgementService.sendOrderAcknowledgementEmail(this, cronJobModel);
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

	}

}
