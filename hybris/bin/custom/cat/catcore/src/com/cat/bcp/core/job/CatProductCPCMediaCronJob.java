package com.cat.bcp.core.job;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.CronJobService;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.cat.bcp.core.dao.CatProductCPCDao;
import com.cat.bcp.core.exception.CatException;
import com.cat.bcp.core.servicelayer.CatProductCPCMediaService;


/**
 * @author ravjonnalagadda
 *
 */
public class CatProductCPCMediaCronJob extends CatAbstractAbortableCronJob
{
	private static final Logger LOG = Logger.getLogger(CatProductCPCMediaCronJob.class);

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;
	@Resource(name = "catProductCPCMediaService")
	private CatProductCPCMediaService catProductCPCMediaService;
	@Resource(name = "cronJobService")
	private CronJobService cronJobService;
	@Resource(name = "catProductCPCDao")
	private CatProductCPCDao catProductCPCDao;


	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.CronJobModel)
	 */
	@Override
	public PerformResult perform(final CronJobModel cronJobModel)
	{
		try
		{
			catProductCPCMediaService.productCPCMediaService(this, cronJobModel);
		}
		catch (final CatException e)
		{

			LOG.error("Exception has occured: :", e);
			return new PerformResult(CronJobResult.FAILURE, CronJobStatus.ABORTED);

		}


		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

}
