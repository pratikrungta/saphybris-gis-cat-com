
/**
 * @author amitsinha5
 *
 */

package com.cat.bcp.core.job;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.util.Config;

import java.io.File;
import java.io.IOException;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.servicelayer.CatSftpChannelService;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;



/**
 * @author amitsinha5
 *
 *         CatSftpChannelJobPerformable class is used to trigger a job for pulling the files from MFT servers via SFTP
 *         on daily basis.
 */
public class CatSftpChannelJobPerformable extends AbstractJobPerformable<CronJobModel>
{

	private static final Logger LOG = Logger.getLogger(CatSftpChannelJobPerformable.class.getName());

	private CatSftpChannelService catSftpChannelService;

	@SuppressWarnings("boxing")
	@Override
	public PerformResult perform(final CronJobModel fTPCronJobModel)
	{
		LOG.info("CatSftpChannelJobPerformable: perform: started");

		PerformResult result = null;
		CronJobResult status = null;
		Channel channel = null;
		Session session = null;

		try
		{

			session = getCatSftpChannelService().connectSessionForSftp(
					Config.getParameter(CatCoreConstants.SFTP_HOST_ADDRESS).trim(),
					Config.getParameter(CatCoreConstants.SFTP_USER_ID).trim(), CatCoreConstants.SFTP_PORT,
					Config.getParameter(CatCoreConstants.SFTP_KEY).trim());


			channel = getCatSftpChannelService().openChannelType(Config.getParameter(CatCoreConstants.FTP_TYPE).trim(), session);

			final ChannelSftp channelSftp = getCatSftpChannelService()
					.changeDirectoryForSftp(Config.getParameter(CatCoreConstants.CHANGE_DIRECTORY_PATH).trim(), channel);

			final Vector<LsEntry> entries = getCatSftpChannelService().getFilePatternEntries(channelSftp, //NOSONAR
					Config.getParameter(CatCoreConstants.FTP_FILE_PATTERN).trim());

			for (final LsEntry entry : entries)
			{
				final File filePath = catSftpChannelService.storeFileToTargetFolder(entry,
						Config.getParameter(CatCoreConstants.FTP_FILE_TARGET_LOCATION).trim(),
						Config.getParameter(CatCoreConstants.FTP_FILE_TARGET_SITE).trim());
				catSftpChannelService.writingFileLogs(entry);
				catSftpChannelService.readFilesFromMftServer(channelSftp, CatCoreConstants.FILE_BUFFER_SIZE, entry, filePath);
			}
			LOG.info("Files downloading end from MFT server");

		}


		catch (final JSchException e)
		{
			LOG.error("CatSftpFileTransferJob job is failed & & unable to connect channel with MFT server", e);
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}
		catch (final IOException e)
		{
			LOG.error("CatSftpFileTransferJob job is failed & unable to locate file directory path", e);
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}
		catch (final Exception e)
		{
			LOG.error("CatSftpFileTransferJob job is failed & unable to download CSV files from given MFT server location ", e);
			return new PerformResult(CronJobResult.ERROR, CronJobStatus.ABORTED);
		}

		finally
		{
			if (null != channel && channel.isConnected())
			{
				channel.disconnect();
				LOG.info("SFTP session is disconnected");
			}

			if (null != session && session.isConnected())
			{
				session.disconnect();
				LOG.info("SFTP session is disconnected");
			}
		}


		status = CronJobResult.SUCCESS;
		result = new PerformResult(status, CronJobStatus.FINISHED);
		LOG.info("CatSftpFileTransferJob: perform:finished ");
		return result;

	}

	/**
	 * @return the catSftpChannelService
	 */
	protected CatSftpChannelService getCatSftpChannelService()
	{
		return catSftpChannelService;
	}

	/**
	 * @param catSftpChannelService
	 *           the catSftpChannelService to set
	 */
	@Required
	public void setCatSftpChannelService(final CatSftpChannelService catSftpChannelService)
	{
		this.catSftpChannelService = catSftpChannelService;
	}







}
