/**
 *
 */
package com.cat.bcp.core.job;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.PerformResult;

import javax.annotation.Resource;

import com.cat.bcp.core.order.CatCommerceQuoteService;


/**
 * Cronjob to update the flag of the reserved inventory for quotation
 * 
 * @author sagdhingra
 *
 */
public class CatUpdateInventoryReservationFlagJob extends CatAbstractAbortableCronJob
{
	@Resource(name = "catCommerceQuoteService")
	private CatCommerceQuoteService catCommerceQuoteService;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable#perform(de.hybris.platform.cronjob.model.
	 * CronJobModel)
	 */
	@Override
	public PerformResult perform(final CronJobModel arg0)
	{
		catCommerceQuoteService.fetchandUpdateExpiredInventory();
		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
	}

}
