/**
 *
 */
package com.cat.bcp.core.mail.context;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.model.CatOrderConfirmationProcessModel;
import com.cat.bcp.core.model.ShipToAfterDateModel;
import com.cat.bcp.core.servicelayer.CatStockService;
import com.cat.facades.email.*;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.util.Config;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Resource;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;


public class CatAccessoriesOrderConfirmationEmailContext extends AbstractEmailContext<CatOrderConfirmationProcessModel>
{


	@Resource(name = "addressConverter")
	private Converter<AddressModel, AddressData> addressConverter;

	@Resource(name = "productConverter")
	private Converter<ProductModel, ProductData> productConverter;

	@Resource(name = "mediaService")
	private MediaService mediaService;

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Resource(name = "catStockService")
	private CatStockService catStockService;

	@Resource(name="orderConverter")
	private Converter<OrderModel, OrderData> orderConverter;

	public static final String CAT_ORDER = "catOrder";

	public static final String EMAIL_ORDER_DATA = "emailOrderData";

	public static final String CAT_LOGO_URL = "catLogoURL";

	public static final String CAT_WARNING_URL = "catWarningURL";

	private static final String CAT_LOGO_MEDIA_ID = "catLogoEmail";

	private static final String CAT_WARNING_MEDIA_ID = "warningEmail";

	private static final String DECIMAL_FORMAT = "0.00";

	private static final String ACCESSORIES_ORDER_CONFIRMATION_DATA = "accessoriesOrderConfirmationData";

	@Override
	public void init(final CatOrderConfirmationProcessModel businessProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(businessProcessModel, emailPageModel);
		setOrderInContext(businessProcessModel);
	}

	/**
	 * This method is used to set Order in Context.
	 *
	 * @param catOrderConfirmationProcessModel
	 *           CatOrderConfirmationProcessModel Object Business Process Object
	 */
	public void setOrderInContext(final CatOrderConfirmationProcessModel catOrderConfirmationProcessModel)
	{
		OrderDataForAccessoriesEmail orderDataForAccessoriesEmail=new OrderDataForAccessoriesEmail();
		final AbstractOrderModel parentOrder = catOrderConfirmationProcessModel.getOrder();
		final SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm a");
		sdfTime.setTimeZone(TimeZone.getTimeZone("CST"));
		final SimpleDateFormat sdfDate = new SimpleDateFormat("(MM/dd/yyyy)");
		sdfDate.setTimeZone(TimeZone.getTimeZone("CST"));
		final StringBuilder creationDateTime = new StringBuilder(sdfTime.format(parentOrder.getCreationtime())).append(" CST ")
				.append(sdfDate.format(parentOrder.getCreationtime()));

		final DecimalFormat formatter = new DecimalFormat(DECIMAL_FORMAT);
		final Double totalPrice = (parentOrder.getTotalPrice() != null) ? parentOrder.getTotalPrice() : Double.valueOf(0);
		orderDataForAccessoriesEmail.setDealerCode(parentOrder.getUnit().getUid());
		orderDataForAccessoriesEmail.setDealerDisplayName(parentOrder.getUnit().getDisplayName());
		orderDataForAccessoriesEmail.setFormattedTotalListPrice((formatter.format(totalPrice)));
		orderDataForAccessoriesEmail.setTotalListPrice(Double.valueOf(totalPrice));
		orderDataForAccessoriesEmail.setOrderPlacedOn(creationDateTime.toString());
		createOrderSummary(orderDataForAccessoriesEmail,parentOrder);
		orderDataForAccessoriesEmail.setShipToAddress(addressConverter.convert(parentOrder.getDeliveryAddress()));

		put(CAT_ORDER, catOrderConfirmationProcessModel.getOrder());
		put(ACCESSORIES_ORDER_CONFIRMATION_DATA, orderDataForAccessoriesEmail);
		setCatLOGOInContext();
		setCatWarningURLInContext();
	}

	private OrderDataForAccessoriesEmail createOrderSummary(OrderDataForAccessoriesEmail orderDataForAccessoriesEmail, AbstractOrderModel parentOrder) {

			int totalQuantity = 0;

		List<OrderSummaryLineItemForAccessories> orderSummaryList=new ArrayList<>();


		final Map<String, List<AbstractOrderEntryModel>> compatibleModelAbstractOrderEntryModelMap = parentOrder.getEntries()
				.stream().collect(Collectors.groupingBy(AbstractOrderEntryModel::getCompatibleModel));

		for (final Map.Entry<String, List<AbstractOrderEntryModel>> compatibleModelAbstractOrderEntryModelMapEntry : compatibleModelAbstractOrderEntryModelMap
				.entrySet())
		{
			OrderSummaryLineItemForAccessories orderSummaryLineItemForAccessories=new OrderSummaryLineItemForAccessories();
			orderSummaryLineItemForAccessories.setSalesModel(compatibleModelAbstractOrderEntryModelMapEntry.getKey());
			final List<AbstractOrderEntryModel> abstractOrderEntryModelList = compatibleModelAbstractOrderEntryModelMap
					.get(compatibleModelAbstractOrderEntryModelMapEntry.getKey());
			orderSummaryLineItemForAccessories.setPurchaseOrderNumber(abstractOrderEntryModelList.iterator().next().getAccessoriesPONumber());
			List<AccessoriesProductData> accessoriesProductDataList=new ArrayList<>();
			for(AbstractOrderEntryModel orderEntryModel:abstractOrderEntryModelList){

				AccessoriesProductData accessoriesProductData = new AccessoriesProductData();
				ProductData productData = productConverter.convert(orderEntryModel.getProduct());
				accessoriesProductData.setPartName(productData.getName());
				accessoriesProductData.setPartNumber(productData.getCode());
				totalQuantity = totalQuantity + orderEntryModel.getQuantity().intValue();
				accessoriesProductData.setQuantity(String.valueOf(orderEntryModel.getQuantity()));
				accessoriesProductDataList.add(accessoriesProductData);
				orderSummaryLineItemForAccessories.setProductSummary(accessoriesProductDataList);

			}

			orderSummaryList.add(orderSummaryLineItemForAccessories);

		}
		orderDataForAccessoriesEmail.setOrderSummary(orderSummaryList);
		orderDataForAccessoriesEmail.setTotalQuantity(Long.valueOf(totalQuantity));


		return orderDataForAccessoriesEmail;
	}



	/**
	 * This method is used to set Cat LOGO URL in context.
	 */
	private void setCatLOGOInContext()
	{
		final MediaModel catLOGOMediaModel = getMediaModel(CAT_LOGO_MEDIA_ID);
		if (catLOGOMediaModel != null)
		{
			put(CAT_LOGO_URL, catLOGOMediaModel.getUrl());
		}
	}

	/**
	 * This method is used to set Cat Warning URL in context
	 */
	private void setCatWarningURLInContext()
	{
		final MediaModel catWarningMediaModel = getMediaModel(CAT_WARNING_MEDIA_ID);
		if (catWarningMediaModel != null)
		{
			put(CAT_WARNING_URL, catWarningMediaModel.getUrl());
		}
	}

	/**
	 * This method is used to get Media Model from mediaCode.
	 *
	 * @return MediaModel Object
	 */
	private MediaModel getMediaModel(final String mediaCode)
	{
		final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(
				Config.getParameter(CatCoreConstants.CAT_CONTENT_CATALOG),
				Config.getParameter(CatCoreConstants.CAT_CONTENT_CATALOG_VERSION));
		return mediaService.getMedia(catalogVersionModel, mediaCode);
	}

	/**
	 * @return the catWarningUrl
	 */
	public String getCatWarningUrl()
	{
		return (String) get(CAT_WARNING_URL);
	}

	/**
	 * @return the catOrder
	 */
	public AbstractOrderModel getCatOrder()
	{
		return (AbstractOrderModel) get(CAT_ORDER);
	}

	/**
	 * @return the emailOrderData
	 */
	public OrderDataForTruckloadEmail getEmailOrderData()
	{
		return (OrderDataForTruckloadEmail) get(EMAIL_ORDER_DATA);
	}

	/**
	 * @return the catLogoUrl
	 */
	public String getCatLogoUrl()
	{
		return (String) get(CAT_LOGO_URL);
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	protected BaseSiteModel getSite(final CatOrderConfirmationProcessModel businessProcessModel)
	{
		return businessProcessModel.getSite();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected CustomerModel getCustomer(final CatOrderConfirmationProcessModel businessProcessModel)
	{
		return businessProcessModel.getCustomer();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected LanguageModel getEmailLanguage(final CatOrderConfirmationProcessModel businessProcessModel)
	{
		return businessProcessModel.getLanguage();
	}
}
