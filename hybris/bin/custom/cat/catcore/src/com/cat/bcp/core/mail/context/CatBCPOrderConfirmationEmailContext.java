/**
 *
 */
package com.cat.bcp.core.mail.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.b2b.model.B2BCommentModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.util.Config;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.enums.LaneTypeEnum;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.CatOrderConfirmationProcessModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.facades.email.ConfigVariantProductForEmail;
import com.cat.facades.email.OrderDataForEmail;
import com.cat.facades.email.OrderSummaryLineItemForEmail;
import com.cat.facades.order.LaneTypeData;


/**
 * @author asomjal
 *
 */
public class CatBCPOrderConfirmationEmailContext extends AbstractEmailContext<CatOrderConfirmationProcessModel>
{

	@Resource(name = "addressConverter")
	private Converter<AddressModel, AddressData> addressConverter;

	@Resource(name = "productConverter")
	private Converter<ProductModel, ProductData> productConverter;

	@Resource(name = "catLaneTypeConverter")
	private Converter<LaneTypeEnum, LaneTypeData> catLaneTypeConverter;

	@Resource(name = "mediaService")
	private MediaService mediaService;

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	public static final String CAT_ORDER = "catOrder";

	public static final String EMAIL_ORDER_DATA = "emailOrderData";

	public static final String CAT_LOGO_URL = "catLogoURL";

	public static final String CAT_WARNING_URL = "catWarningURL";

	public static final String CAT_LANE_TYPE_URL = "catLaneTypeURL";

	private static final String CAT_LOGO_MEDIA_ID = "catLogoEmail";

	private static final String CAT_WARNING_MEDIA_ID = "warningEmail";

	private static final String DECIMAL_FORMAT = "0.00";

	/**
	 * @return the catWarningUrl
	 */
	public String getCatWarningUrl()
	{
		return (String) get(CAT_WARNING_URL);
	}

	/**
	 * @return the catOrder
	 */
	public AbstractOrderModel getCatOrder()
	{
		return (AbstractOrderModel) get(CAT_ORDER);
	}

	/**
	 * @return the emailOrderData
	 */
	public OrderDataForEmail getEmailOrderData()
	{
		return (OrderDataForEmail) get(EMAIL_ORDER_DATA);
	}

	/**
	 * @return the catLogoUrl
	 */
	public String getCatLogoUrl()
	{
		return (String) get(CAT_LOGO_URL);
	}

	@Override
	public void init(final CatOrderConfirmationProcessModel businessProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(businessProcessModel, emailPageModel);
		setOrderInContext(businessProcessModel);
	}

	/**
	 * This method is used to set Order in Context.
	 *
	 * @param catOrderConfirmationProcessModel
	 *           CatOrderConfirmationProcessModel Object Business Process Object
	 */
	public void setOrderInContext(final CatOrderConfirmationProcessModel catOrderConfirmationProcessModel)
	{
		final OrderDataForEmail orderDataForEmail = new OrderDataForEmail();
		final AbstractOrderModel parentOrder = catOrderConfirmationProcessModel.getOrder();
		orderDataForEmail.setDealerDisplayName(parentOrder.getUnit().getDisplayName());
		orderDataForEmail.setDealerCode(parentOrder.getUnit().getUid());

		final SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm a");
		sdfTime.setTimeZone(TimeZone.getTimeZone("CST"));
		final SimpleDateFormat sdfDate = new SimpleDateFormat("(MM/dd/yyyy)");
		sdfDate.setTimeZone(TimeZone.getTimeZone("CST"));
		final StringBuilder dateTime = new StringBuilder(sdfTime.format(parentOrder.getCreationtime())).append(" CST ")
				.append(sdfDate.format(parentOrder.getCreationtime()));
		orderDataForEmail.setOrderPlacedOn(dateTime.toString());

		orderDataForEmail.setShipToAddress(addressConverter.convert(parentOrder.getDeliveryAddress()));
		if (CollectionUtils.isNotEmpty(parentOrder.getEntries()))
		{
			createOrderSummary(orderDataForEmail, parentOrder);
		}
		if (CollectionUtils.isNotEmpty(parentOrder.getB2bcomments()))
		{
			final B2BCommentModel firstCommentModel = parentOrder.getB2bcomments().iterator().next();
			orderDataForEmail.setComments(firstCommentModel != null ? firstCommentModel.getComment() : "");
		}
		put(CAT_ORDER, catOrderConfirmationProcessModel.getOrder());
		put(EMAIL_ORDER_DATA, orderDataForEmail);
		setCatLOGOInContext();
		setCatWarningURLInContext();
	}

	/**
	 * Create Order Summary for Email.
	 *
	 * @param orderDataForEmail
	 *           OrderDataForEmail Object
	 * @param parentOrder
	 *           AbstractOrderModel Object
	 */
	private void createOrderSummary(final OrderDataForEmail orderDataForEmail, final AbstractOrderModel parentOrder)
	{
		final Map<ProductModel, List<AbstractOrderEntryModel>> groupBySalesModelMap = parentOrder.getEntries().stream()
				.collect(Collectors.groupingBy(
						entry -> ((BaseVariantProductModel) ((ConfigVariantProductModel) entry.getProduct()).getBaseProduct())
								.getBaseProduct()));

		String laneType = StringUtils.EMPTY;
		if (parentOrder.getLaneType() != null)
		{
			final LaneTypeData laneTypeData = catLaneTypeConverter.convert(parentOrder.getLaneType());
			laneType = laneTypeData.getName();
			setLaneTypeIconInContext(laneTypeData);
		}

		final List<OrderSummaryLineItemForEmail> orderSummaryLineItemForEmailList = new ArrayList();
		long totalQuantity = 0;
		for (final Map.Entry<ProductModel, List<AbstractOrderEntryModel>> salesModelOrderEntryMapEntry : groupBySalesModelMap
				.entrySet())
		{
			final ProductModel productModel = salesModelOrderEntryMapEntry.getKey();
			final OrderSummaryLineItemForEmail orderSummaryLineItemForEmail = new OrderSummaryLineItemForEmail();
			orderSummaryLineItemForEmail.setSalesModel(productConverter.convert(productModel));
			final List<AbstractOrderEntryModel> configVariantProductEntries = salesModelOrderEntryMapEntry.getValue();
			if (CollectionUtils.isNotEmpty(configVariantProductEntries))
			{
				final List<ConfigVariantProductForEmail> configVariantProductForEmailList = new ArrayList();
				for (final AbstractOrderEntryModel configVariantProductEntry : configVariantProductEntries)
				{
					final ConfigVariantProductForEmail configVariantProductForEmail = new ConfigVariantProductForEmail();
					configVariantProductForEmail.setCode(
							configVariantProductEntry.getProduct() != null ? configVariantProductEntry.getProduct().getName() : "");
					final Map<String, List<String>> poNumberMap = parentOrder.getPoNumberMap();
					final Collection<String> poNumbersForConfigVariantProduct = poNumberMap
							.get(configVariantProductEntry.getProduct().getCode() + "__" + configVariantProductEntry.getEntryNumber());
					configVariantProductForEmail.setPoNumbers(StringUtils.join(poNumbersForConfigVariantProduct, ','));
					configVariantProductForEmail.setQuantity(
							poNumbersForConfigVariantProduct != null ? String.valueOf(poNumbersForConfigVariantProduct.size()) : "");
					configVariantProductForEmailList.add(configVariantProductForEmail);
					totalQuantity = totalQuantity
							+ (poNumbersForConfigVariantProduct != null ? poNumbersForConfigVariantProduct.size() : 0);
				}
				orderSummaryLineItemForEmail.setConfigurationList(configVariantProductForEmailList);
				orderSummaryLineItemForEmail.setLaneType(laneType);
			}
			orderSummaryLineItemForEmailList.add(orderSummaryLineItemForEmail);
		}
		orderDataForEmail.setTotalQuantity(Long.valueOf(totalQuantity));
		final DecimalFormat formatter = new DecimalFormat(DECIMAL_FORMAT);
		final Double totalPrice = (parentOrder.getTotalPrice() != null) ? parentOrder.getTotalPrice() : Double.valueOf(0);
		orderDataForEmail.setTotalListPrice(totalPrice);
		orderDataForEmail.setFormattedTotalListPrice(formatter.format(totalPrice));
		orderDataForEmail.setOrderSummary(orderSummaryLineItemForEmailList);
	}

	/**
	 * This method is used to set Cat LOGO URL in context.
	 */
	private void setCatLOGOInContext()
	{
		final MediaModel catLOGOMediaModel = getMediaModel(CAT_LOGO_MEDIA_ID);
		if (catLOGOMediaModel != null)
		{
			put(CAT_LOGO_URL, catLOGOMediaModel.getUrl());
		}
	}

	/**
	 * This method is used to set Cat Warning URL in context
	 */
	private void setCatWarningURLInContext()
	{
		final MediaModel catWarningMediaModel = getMediaModel(CAT_WARNING_MEDIA_ID);
		if (catWarningMediaModel != null)
		{
			put(CAT_WARNING_URL, catWarningMediaModel.getUrl());
		}
	}

	/**
	 * Sets the lane type icon in context.
	 *
	 * @param laneTypeData
	 *           lane type data
	 */
	private void setLaneTypeIconInContext(final LaneTypeData laneTypeData)
	{
		final MediaModel catWarningMediaModel = getMediaModel(laneTypeData.getCode());
		if (catWarningMediaModel != null)
		{
			put(CAT_LANE_TYPE_URL, catWarningMediaModel.getUrl());
		}

	}

	/**
	 * This method is used to get Media Model from mediaCode.
	 *
	 * @return MediaModel Object
	 */
	private MediaModel getMediaModel(final String mediaCode)
	{
		final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(
				Config.getParameter(CatCoreConstants.CAT_CONTENT_CATALOG),
				Config.getParameter(CatCoreConstants.CAT_CONTENT_CATALOG_VERSION));
		return mediaService.getMedia(catalogVersionModel, mediaCode);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected BaseSiteModel getSite(final CatOrderConfirmationProcessModel businessProcessModel)
	{
		return businessProcessModel.getSite();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected CustomerModel getCustomer(final CatOrderConfirmationProcessModel businessProcessModel)
	{
		return businessProcessModel.getCustomer();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected LanguageModel getEmailLanguage(final CatOrderConfirmationProcessModel businessProcessModel)
	{
		return businessProcessModel.getLanguage();
	}
}
