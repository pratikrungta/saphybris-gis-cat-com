/**
 *
 */
package com.cat.bcp.core.mail.context;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.event.CatOrderConfirmationEmailEventListner;
import com.cat.bcp.core.model.CatCompatibilityCheckFailedEmailProcessModel;
import com.cat.facades.email.*;
import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.util.Config;
import org.apache.log4j.Logger;
import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * The class is used to create the data to be put in the context so that data for email
 * can be populated through VM file.
 *
 */
public class CatCompatibilityCheckFailedEmailContext extends AbstractEmailContext<CatCompatibilityCheckFailedEmailProcessModel>
{

	private static final Logger LOG = Logger.getLogger(CatOrderConfirmationEmailEventListner.class);
	public static final String FAILED_CONFIG_ID_DATA = "failedConfigIDData";
	private static final String COMPATIBIITY_CHECKE_FAILED_MAIL_RUN_TIME ="CompatibilityCheckFailedRuntime" ;

	public static final String CAT_LOGO_URL = "catLogoURL";

	public static final String CAT_WARNING_URL = "catWarningURL";

	private static final String CAT_LOGO_MEDIA_ID = "catLogoEmail";

	private static final String CAT_WARNING_MEDIA_ID = "warningEmailContext";

	@Resource(name = "productConverter")
	private Converter<ProductModel, ProductData> productConverter;

	@Resource(name = "mediaService")
	private MediaService mediaService;

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	@Resource(name="searchRestrictionService")
	SearchRestrictionService searchRestrictionService;

	public String getCatLogoUrl()
	{
		return (String) get(CAT_LOGO_URL);
	}

	@Override
	public void init(final CatCompatibilityCheckFailedEmailProcessModel businessProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(businessProcessModel, emailPageModel);
		insertFailedConfigIDInContext(businessProcessModel);
	}

	/**
	 * The method is used to put the data in the context
	 * @param businessProcessModel the business process to be executed
	 */
	private void insertFailedConfigIDInContext(CatCompatibilityCheckFailedEmailProcessModel businessProcessModel) {
		final SimpleDateFormat sdf = new SimpleDateFormat("(MM/dd/yyyy) hh:mm aa");
		sdf.setTimeZone(TimeZone.getTimeZone("CST"));
		Calendar cal = Calendar.getInstance();
		Date todayDate = cal.getTime();


		searchRestrictionService.disableSearchRestrictions();
		List<ProductData> failedConfigIDListData = productConverter.convertAll(businessProcessModel.getFailedConfigIDList());
		searchRestrictionService.enableSearchRestrictions();
		Iterator<ProductData> failedConfigIterator = failedConfigIDListData.iterator();
		DataForCompatibilityCheckFailedEmail dataForCompatibilityCheckFailedEmail = new DataForCompatibilityCheckFailedEmail();
		dataForCompatibilityCheckFailedEmail.setTotalConfigurations(failedConfigIDListData.size());
		Map<String, List<ProductData>> categoryFailedConfigIdMap = new HashMap<>();

		//separate the category
		while (failedConfigIterator.hasNext()) {
			ProductData failedConfigIDData = failedConfigIterator.next();
			String categoryName = failedConfigIDData.getCategories().iterator().next().getName();
			if (!categoryFailedConfigIdMap.containsKey(categoryName)) {
				List<ProductData> failedConfigIDLList = new ArrayList<>();
				failedConfigIDLList.add(failedConfigIDData);
				categoryFailedConfigIdMap.put(categoryName, failedConfigIDLList);
			} else {
				categoryFailedConfigIdMap.get(categoryName).add(failedConfigIDData);
			}
		}

		Set<String> categorySet = categoryFailedConfigIdMap.keySet();
		List<FailedConfigIDCategoryDetailsLineItem> failedConfigIDCategoryDetailsLineItemList = new ArrayList<>();
		//iterate the category
		for (String categoryName : categorySet) {
			FailedConfigIDCategoryDetailsLineItem failedConfigIDCategoryDetailsLineItem = new FailedConfigIDCategoryDetailsLineItem();
			failedConfigIDCategoryDetailsLineItem.setCategoryName(categoryName);
			List<FailedConfigIDLineItem> failedConfigIDLineItemList=new ArrayList<>();
			List<ProductData> productDataList = categoryFailedConfigIdMap.get(categoryName);

			for (ProductData productData : productDataList) {
				FailedConfigIDLineItem failedConfigIDLineItem=new FailedConfigIDLineItem();
				failedConfigIDLineItem.setSalesModel(productData.getSalesModelId());
				failedConfigIDLineItem.setBaseVariantID(productData.getPrimaryOfferingId());
				failedConfigIDLineItem.setBaseVariantName(productData.getPrimaryOfferingName());
				failedConfigIDLineItem.setFailedConfigID(productData.getCode());
				failedConfigIDLineItem.setConfigIDCreationDate(sdf.format(productData.getCreationTime()));
				failedConfigIDLineItemList.add(failedConfigIDLineItem);
			}
			failedConfigIDCategoryDetailsLineItem.setConfigDetailsList(failedConfigIDLineItemList);

			failedConfigIDCategoryDetailsLineItemList.add(failedConfigIDCategoryDetailsLineItem);
		}

		dataForCompatibilityCheckFailedEmail.setFailedConfigIDLineItemList(failedConfigIDCategoryDetailsLineItemList);
		put(FAILED_CONFIG_ID_DATA, dataForCompatibilityCheckFailedEmail);
		final String TO_EMAIL = Config.getParameter("mail.to.person.compatibility.check.failed");
		setCatLOGOInContext();
		setCatWarningURLInContext();
		put(EMAIL, TO_EMAIL);
		put(DISPLAY_NAME, TO_EMAIL);
		put(COMPATIBIITY_CHECKE_FAILED_MAIL_RUN_TIME,sdf.format(todayDate));

	}





	/**
	 * This method is used to set Cat LOGO URL in context.
	 */
	private void setCatLOGOInContext()
	{
		final MediaModel catLOGOMediaModel = getMediaModel(CAT_LOGO_MEDIA_ID);
		if (catLOGOMediaModel != null)
		{
			put(CAT_LOGO_URL, catLOGOMediaModel.getUrl());
		}
	}


	private MediaModel getMediaModel(final String mediaCode)
	{
		final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(
				Config.getParameter(CatCoreConstants.CAT_CONTENT_CATALOG),
				Config.getParameter(CatCoreConstants.CAT_CONTENT_CATALOG_VERSION));
		return mediaService.getMedia(catalogVersionModel, mediaCode);
	}


	/**
	 * This method is used to set Cat Warning URL in context
	 */
	private void setCatWarningURLInContext()
	{
		final MediaModel catWarningMediaModel = getMediaModel(CAT_WARNING_MEDIA_ID);
		if (catWarningMediaModel != null)
		{
			put(CAT_WARNING_URL, catWarningMediaModel.getUrl());
		}
	}


	@Override
	protected BaseSiteModel getSite(CatCompatibilityCheckFailedEmailProcessModel businessProcessModel) {
		return businessProcessModel.getSite();
	}

	@Override
	protected CustomerModel getCustomer(CatCompatibilityCheckFailedEmailProcessModel businessProcessModel) {
		return null;
	}

	@Override
	protected LanguageModel getEmailLanguage(CatCompatibilityCheckFailedEmailProcessModel businessProcessModel) {
		return 	businessProcessModel.getLanguage();
	}
}
