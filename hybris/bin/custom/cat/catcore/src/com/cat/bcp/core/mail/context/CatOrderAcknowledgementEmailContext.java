/**
 *
 */
package com.cat.bcp.core.mail.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.model.CatOrderAcknowledgementProcessModel;


/**
 * The class is used to create the data to be put in the context so that data for email can be populated through VM
 * file.
 *
 */
public class CatOrderAcknowledgementEmailContext extends AbstractEmailContext<CatOrderAcknowledgementProcessModel>
{

	private static final Logger LOG = Logger.getLogger(CatOrderAcknowledgementEmailContext.class);

	private static final String PONUMBERS = "PONUMBERS";

	public static final String CAT_LOGO_URL = "catLogoURL";

	public static final String CAT_WARNING_URL = "catWarningURL";

	private static final String CAT_LOGO_MEDIA_ID = "catLogoEmail";

	private static final String CAT_WARNING_MEDIA_ID = "warningEmailContext";


	@Resource(name = "mediaService")
	private MediaService mediaService;

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	@Resource(name = "modelService")
	private ModelService modelService;


	@Override
	public void init(final CatOrderAcknowledgementProcessModel businessProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(businessProcessModel, emailPageModel);
		final B2BCustomerModel customer = (B2BCustomerModel) businessProcessModel.getOrderList().stream()
				.collect(Collectors.toList()).get(0).getUser();
		final String TO_EMAIL = customer.getEmail();
		setCatLOGOInContext();
		setCatWarningURLInContext();
		put(EMAIL, TO_EMAIL);
		put(DISPLAY_NAME, customer.getName());
		put(PONUMBERS, getPoNumbers(businessProcessModel.getOrderList()));
		setisAckEmailSent(businessProcessModel.getOrderList());

	}


	@Override
	protected BaseSiteModel getSite(final CatOrderAcknowledgementProcessModel businessProcessModel)
	{
		return businessProcessModel.getSite();
	}

	@Override
	protected CustomerModel getCustomer(final CatOrderAcknowledgementProcessModel businessProcessModel)
	{
		return null;
	}

	@Override
	protected LanguageModel getEmailLanguage(final CatOrderAcknowledgementProcessModel businessProcessModel)
	{
		return businessProcessModel.getLanguage();
	}

	/**
	 * This method is used to fetch the po numbers from ordermodels
	 *
	 * @param orderList
	 *           the order list
	 * @return PO numbers list
	 */
	private List<String> getPoNumbers(final Collection<AbstractOrderModel> orderList)
	{
		final List<String> poList = new ArrayList<>();
		for (final AbstractOrderModel aoem : orderList)
		{
			poList.add(aoem.getPurchaseOrderNumber());
		}
		return poList;
	}

	/**
	 * This method is used to set the acknowledgement email sent status to true for the orders
	 *
	 * @param orderList
	 *           The order list
	 */
	private void setisAckEmailSent(final Collection<AbstractOrderModel> orderList)
	{
		for (final AbstractOrderModel aoem : orderList)
		{
			aoem.setIsAckMailSent(true);
			modelService.save(aoem);
		}
	}

	/**
	 * This method is used to set Cat LOGO URL in context.
	 */
	private void setCatLOGOInContext()
	{
		final MediaModel catLOGOMediaModel = getMediaModel(CAT_LOGO_MEDIA_ID);
		if (catLOGOMediaModel != null)
		{
			put(CAT_LOGO_URL, catLOGOMediaModel.getUrl());
		}
	}


	private MediaModel getMediaModel(final String mediaCode)
	{
		final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(
				Config.getParameter(CatCoreConstants.CAT_CONTENT_CATALOG),
				Config.getParameter(CatCoreConstants.CAT_CONTENT_CATALOG_VERSION));
		return mediaService.getMedia(catalogVersionModel, mediaCode);
	}


	/**
	 * This method is used to set Cat Warning URL in context
	 */
	private void setCatWarningURLInContext()
	{
		final MediaModel catWarningMediaModel = getMediaModel(CAT_WARNING_MEDIA_ID);
		if (catWarningMediaModel != null)
		{
			put(CAT_WARNING_URL, catWarningMediaModel.getUrl());
		}
	}
}
