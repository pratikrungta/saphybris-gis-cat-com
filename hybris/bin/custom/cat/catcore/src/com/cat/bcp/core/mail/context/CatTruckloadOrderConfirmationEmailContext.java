/**
 *
 */
package com.cat.bcp.core.mail.context;

import de.hybris.platform.acceleratorservices.model.cms2.pages.EmailPageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.util.Config;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.model.CatOrderConfirmationProcessModel;
import com.cat.bcp.core.model.ShipToAfterDateModel;
import com.cat.bcp.core.servicelayer.CatStockService;
import com.cat.facades.email.OrderDataForTruckloadEmail;
import com.cat.facades.email.OrderSummaryLineItemForTruckloadEmail;
import com.cat.facades.email.OrderSummaryTruckloadEntry;



public class CatTruckloadOrderConfirmationEmailContext extends AbstractEmailContext<CatOrderConfirmationProcessModel>
{

	@Resource(name = "addressConverter")
	private Converter<AddressModel, AddressData> addressConverter;

	@Resource(name = "productConverter")
	private Converter<ProductModel, ProductData> productConverter;

	@Resource(name = "mediaService")
	private MediaService mediaService;

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Resource(name = "catStockService")
	private CatStockService catStockService;

	public static final String CAT_ORDER = "catOrder";

	public static final String EMAIL_ORDER_DATA = "emailOrderData";

	public static final String CAT_LOGO_URL = "catLogoURL";

	public static final String CAT_WARNING_URL = "catWarningURL";

	private static final String CAT_LOGO_MEDIA_ID = "catLogoEmail";

	private static final String CAT_WARNING_MEDIA_ID = "warningEmail";

	private static final String DECIMAL_FORMAT = "0.00";


	@Override
	public void init(final CatOrderConfirmationProcessModel businessProcessModel, final EmailPageModel emailPageModel)
	{
		super.init(businessProcessModel, emailPageModel);
		setOrderInContext(businessProcessModel);
	}

	/**
	 * This method is used to set Order in Context.
	 *
	 * @param catOrderConfirmationProcessModel
	 *           CatOrderConfirmationProcessModel Object Business Process Object
	 */
	public void setOrderInContext(final CatOrderConfirmationProcessModel catOrderConfirmationProcessModel)
	{
		final OrderDataForTruckloadEmail orderDataForEmail = new OrderDataForTruckloadEmail();
		final AbstractOrderModel parentOrder = catOrderConfirmationProcessModel.getOrder();
		orderDataForEmail.setDealerDisplayName(parentOrder.getUnit().getDisplayName());
		orderDataForEmail.setDealerCode(parentOrder.getUnit().getUid());

		final SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm a");
		sdfTime.setTimeZone(TimeZone.getTimeZone("CST"));
		final SimpleDateFormat sdfDate = new SimpleDateFormat("(MM/dd/yyyy)");
		sdfDate.setTimeZone(TimeZone.getTimeZone("CST"));
		final StringBuilder dateTime = new StringBuilder(sdfTime.format(parentOrder.getCreationtime())).append(" CST ")
				.append(sdfDate.format(parentOrder.getCreationtime()));
		orderDataForEmail.setOrderPlacedOn(dateTime.toString());

		if (CollectionUtils.isNotEmpty(parentOrder.getEntries()))
		{
			createOrderSummary(orderDataForEmail, parentOrder);
		}
		final DecimalFormat formatter = new DecimalFormat(DECIMAL_FORMAT);
		final Double totalPrice = (parentOrder.getTotalPrice() != null) ? parentOrder.getTotalPrice() : Double.valueOf(0);
		orderDataForEmail.setTotalPrice(formatter.format(totalPrice));

		put(CAT_ORDER, catOrderConfirmationProcessModel.getOrder());
		put(EMAIL_ORDER_DATA, orderDataForEmail);
		setCatLOGOInContext();
		setCatWarningURLInContext();
	}


	/**
	 * Create Order Summary for Email.
	 *
	 * @param orderDataForEmail
	 *           OrderDataForEmail Object
	 * @param order
	 *           AbstractOrderModel Object
	 */
	private void createOrderSummary(final OrderDataForTruckloadEmail orderDataForEmail, final AbstractOrderModel order)
	{
		final List<OrderSummaryLineItemForTruckloadEmail> orderSummaryLineItemList = new ArrayList<>();
		final Map<String, List<AbstractOrderEntryModel>> groupByTruckloadIdMap = order.getEntries().stream()
				.collect(Collectors.groupingBy(entry -> (entry.getTruckloadId())));
		orderDataForEmail.setTotalTruckloads(String.valueOf(groupByTruckloadIdMap.size()));

		final Map<String, List<String>> poNumberMap = order.getPoNumberMap();

		long totalQuantity = 0;
		int truckId = 1;

		for (final Entry<String, List<AbstractOrderEntryModel>> orderEntryMap : groupByTruckloadIdMap.entrySet())
		{
			final OrderSummaryLineItemForTruckloadEmail orderSummaryLineItem = new OrderSummaryLineItemForTruckloadEmail();
			orderSummaryLineItem.setTruckId("TRUCK " + truckId);

			final List<OrderSummaryTruckloadEntry> truckloadEntries = new ArrayList<>();

			final List<AbstractOrderEntryModel> listOfOrderEntries = orderEntryMap.getValue();
			for (final AbstractOrderEntryModel orderEntry : listOfOrderEntries)
			{
				final OrderSummaryTruckloadEntry truckEntry = new OrderSummaryTruckloadEntry();

				truckEntry.setProduct(productConverter.convert(orderEntry.getProduct()));

				truckEntry.setQuantity(orderEntry.getQuantity().toString());
				totalQuantity = totalQuantity + orderEntry.getQuantity();

				ShipToAfterDateModel shipToModel = modelService.create(ShipToAfterDateModel.class);
				shipToModel.setShipAfterDate(orderEntry.getShipToAfter());
				shipToModel = flexibleSearchService.getModelByExample(shipToModel);
				final String formattedShipAfterDate = catStockService.formatShipToAfterDate(shipToModel).toString();
				orderSummaryLineItem.setShipToAfter(formattedShipAfterDate);

				orderSummaryLineItem.setShipToAddress(addressConverter.convert(orderEntry.getShippingAddress()));

				final Collection<String> poNumbers = poNumberMap
						.get(orderEntry.getProduct().getCode() + "__" + orderEntry.getTruckloadId());
				truckEntry.setPoNumber(StringUtils.join(poNumbers, ", "));

				truckloadEntries.add(truckEntry);
				orderSummaryLineItem.setTruckloadEntries(truckloadEntries);
			}
			truckId++;
			orderSummaryLineItemList.add(orderSummaryLineItem);
		}
		orderDataForEmail.setTotalQuantity(String.valueOf(totalQuantity));
		orderDataForEmail.setOrderSummaryLineItemList(orderSummaryLineItemList);
	}

	/**
	 * This method is used to set Cat LOGO URL in context.
	 */
	private void setCatLOGOInContext()
	{
		final MediaModel catLOGOMediaModel = getMediaModel(CAT_LOGO_MEDIA_ID);
		if (catLOGOMediaModel != null)
		{
			put(CAT_LOGO_URL, catLOGOMediaModel.getUrl());
		}
	}

	/**
	 * This method is used to set Cat Warning URL in context
	 */
	private void setCatWarningURLInContext()
	{
		final MediaModel catWarningMediaModel = getMediaModel(CAT_WARNING_MEDIA_ID);
		if (catWarningMediaModel != null)
		{
			put(CAT_WARNING_URL, catWarningMediaModel.getUrl());
		}
	}

	/**
	 * This method is used to get Media Model from mediaCode.
	 *
	 * @return MediaModel Object
	 */
	private MediaModel getMediaModel(final String mediaCode)
	{
		final CatalogVersionModel catalogVersionModel = catalogVersionService.getCatalogVersion(
				Config.getParameter(CatCoreConstants.CAT_CONTENT_CATALOG),
				Config.getParameter(CatCoreConstants.CAT_CONTENT_CATALOG_VERSION));
		return mediaService.getMedia(catalogVersionModel, mediaCode);
	}

	/**
	 * @return the catWarningUrl
	 */
	public String getCatWarningUrl()
	{
		return (String) get(CAT_WARNING_URL);
	}

	/**
	 * @return the catOrder
	 */
	public AbstractOrderModel getCatOrder()
	{
		return (AbstractOrderModel) get(CAT_ORDER);
	}

	/**
	 * @return the emailOrderData
	 */
	public OrderDataForTruckloadEmail getEmailOrderData()
	{
		return (OrderDataForTruckloadEmail) get(EMAIL_ORDER_DATA);
	}

	/**
	 * @return the catLogoUrl
	 */
	public String getCatLogoUrl()
	{
		return (String) get(CAT_LOGO_URL);
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	protected BaseSiteModel getSite(final CatOrderConfirmationProcessModel businessProcessModel)
	{
		return businessProcessModel.getSite();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected CustomerModel getCustomer(final CatOrderConfirmationProcessModel businessProcessModel)
	{
		return businessProcessModel.getCustomer();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected LanguageModel getEmailLanguage(final CatOrderConfirmationProcessModel businessProcessModel)
	{
		return businessProcessModel.getLanguage();
	}
}
