/**
 *
 */
package com.cat.bcp.core.order;

import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.exceptions.BusinessException;

import java.util.List;

import com.cat.bcp.core.model.TruckloadModel;
import com.cat.facades.order.CATOrderReviewData;


/**
 * The Interface CatCommerceCheckoutService.
 *
 * @author vjagannadharaotel
 */
public interface CatCommerceCheckoutService extends CommerceCheckoutService
{




	/**
	 * This method takes review data from Facade layer and creates an order by calling respective service methods and
	 * returns the created order model list.
	 *
	 * @param reviewOrderComments
	 *           reviewOrderComments received from user
	 * @param purchaseOrderNumbers
	 *           purchaseOrderNumbers received from user
	 * @param selectedShippingAddressPK
	 *           selectedShippingAddressPK of the address to verify if the address already exists.
	 * @return Order Model
	 * @throws BusinessException
	 *            the business exception
	 */
	OrderModel placeCatOrder(final String reviewOrderComments, final List<String> purchaseOrderNumbers,
			final String selectedShippingAddressPK) throws BusinessException;


	/**
	 * This method is used to Place Order For Accessories Flow.
	 *
	 * @param reviewOrderComments
	 *           reviewOrderComments
	 * @param selectedShippingAddressPK
	 *           selectedShippingAddressPK
	 * @return OrderModel Object
	 * @throws BusinessException
	 *            BusinessException
	 */
	OrderModel placeCatOrderForAccessories(final String reviewOrderComments, final String selectedShippingAddressPK)
			throws BusinessException;

	/**
	 * This method takes updated cart quantity and calls commerce cart service to update the cart quantity.
	 *
	 * @param orderedQty
	 *           new quantity to be updated.
	 */
	void updateCartQuantity(final Long orderedQty);


	/**
	 * This method will call the service which will trigger business process inorder to split the parent Order based on
	 * list of purchase order number.
	 *
	 * @param orderCode
	 *           Parent Order Code
	 * @param poList
	 *           list of PO Numbers
	 */
	void splitOrderByPO(final String orderCode, final List<String> poList);

	/**
	 * This method is used to split Accessories Order.
	 *
	 * @param orderCode
	 *           order Code
	 */
	void splitOrderForAccessories(final String orderCode);

	/**
	 * This method is to create order additional info table for an order with given order code.
	 *
	 * @param localOrder
	 *           Order Model.
	 * @param productCode
	 *           Product Code.
	 */
	void createAdditionalInfo(final OrderModel localOrder, final String productCode);


	/**
	 * Get all truckload combinations
	 *
	 * @return List of truckload data
	 */
	List<TruckloadModel> getTruckloadCombinations();


	/**
	 * This method validates truckload
	 *
	 * @param twoSeaterQty
	 *           - 2 seaters
	 * @param fiveSeaterQty
	 *           - 5 seaters
	 * @return truckload -truckload
	 */
	boolean validateTruckload(int twoSeaterQty, int fiveSeaterQty);


	/**
	 * This method populates truckload recommendation
	 *
	 * @param twoSeaterQty
	 *           - 2 seaters
	 * @param fiveSeaterQty
	 *           - 5 seaters
	 * @return List - Truckload Model list
	 */
	List<TruckloadModel> populateTruckloadSuggestions(int twoSeaterQty, int fiveSeaterQty);

	/**
	 * This method removes the cart entry based on the truckload id.
	 *
	 * @param truckloadId
	 *           the truckload id
	 * @throws CalculationException
	 *            the calculation exception
	 */
	void removeTruckload(String truckloadId) throws CalculationException;

	/**
	 * Used to create the truckload mix and match order
	 *
	 * @param reviewOrderData
	 *           the review order data
	 * @return the created order
	 * @throws BusinessException
	 *            exception during creation of an order
	 */
	OrderModel placeTruckloadOrder(final CATOrderReviewData reviewOrderData) throws BusinessException;
}
