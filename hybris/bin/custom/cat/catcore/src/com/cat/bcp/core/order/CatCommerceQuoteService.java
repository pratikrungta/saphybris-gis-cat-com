/**
 *
 */
package com.cat.bcp.core.order;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.order.data.CatReserveQuoteData;
import de.hybris.platform.commerceservices.enums.DiscountType;
import de.hybris.platform.commerceservices.order.CommerceQuoteService;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;

import java.util.Date;
import java.util.List;

import com.cat.bcp.core.model.InventoryReservationModel;
import com.cat.facades.quote.CatQuoteVETData;


/**
 * The Interface CatCommerceQuoteService.
 *
 * @author sagdhingra
 */
public interface CatCommerceQuoteService extends CommerceQuoteService
{

	/**
	 * Apply quote discount entry level.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param entryNumber
	 *           the entry number
	 * @param currentQuoteUser
	 *           the current quote user
	 * @param discntValue
	 *           the discnt value
	 * @param percent
	 *           the percent
	 */
	void applyQuoteDiscountEntryLevel(AbstractOrderModel abstractOrderModel, long entryNumber, UserModel currentQuoteUser,
			double discntValue, DiscountType percent);

	/**
	 * Saving the additional quotation details in the service layer
	 *
	 * @param quoteModel
	 *           The quote model which came out of data object
	 */
	void saveQuoteDetails(QuoteModel quoteModel);

	/**
	 * Delete the quote if a user navigates away from the quote page
	 *
	 * @param quoteModel
	 *           The quote model which has to be removed
	 */
	void deleteQuote(QuoteModel quoteModel);

	/**
	 * Update quote.
	 *
	 * @param quoteCode
	 *           the quote code
	 * @param submittedToDSU
	 *           the submitted to DSU
	 * @return the quote model
	 * @throws ModelSavingException
	 *            the model saving exception
	 */
	QuoteModel updateQuote(String quoteCode, boolean submittedToDSU) throws ModelSavingException;

	/**
	 * Gets the order additional info data.
	 *
	 * @return the order additional info data
	 */
	List<String> getProductFamilies();



	/**
	 * This method is to get list of products with no stock for a quote.
	 *
	 * @param quoteCode
	 *           Quote which needs to be verified for stock level
	 *
	 * @return list of quote entries with no stock.
	 */
	List<ProductModel> getQuoteItemsWithNoStock(final String quoteCode);

	/**
	 * This method is used to calculate stock of product.
	 *
	 * @param configProduct
	 *           the Config Variant Product for which stock is calculated
	 * @param b2bCustomer
	 *           Stock is checked in all the POS associated with Default B2bUnit of given B2BCusotmer
	 * @return stock value
	 */
	long calculateStock(final ProductModel configProduct, final B2BCustomerModel b2bCustomer);


	/**
	 * This method is to get the reservation data for a quote.
	 *
	 * @param productModel
	 *           Product for which reservation data needs to be fetched
	 *
	 * @param b2bUnitModel
	 *           the reservation data is dealer specific
	 * @return List<InventoryReservationModel> list of inventory reservation model.
	 */
	List<InventoryReservationModel> getReservationQuoteData(final ProductModel productModel, final B2BUnitModel b2bUnitModel);

	/**
	 * This method is to get the reservation data for updating.
	 *
	 * @param serialNumbers
	 *           The serial numbers for which reservation flag needs to be updated
	 *
	 * @param b2bUnitModel
	 *           the reservation data is dealer specific
	 * @return List<InventoryReservationModel> list of inventory reservation model.
	 */
	List<InventoryReservationModel> getReservationQuoteDataForUpdate(final String serialNumbers, final B2BUnitModel b2bUnitModel);


	/**
	 * This method is to reserve the serial numbers for config products.
	 *
	 * @param catReserveQuoteData
	 *           The data which will be converted to model
	 *
	 * @param quoteModel
	 *           the reference of the quote for which inventory is to be reserved
	 *
	 * @param inventoryReservationModel
	 *           the model which is to be saved
	 */
	void reserveSerialNumbersForConfigurations(final CatReserveQuoteData catReserveQuoteData, final QuoteModel quoteModel,
			final InventoryReservationModel inventoryReservationModel);


	/**
	 * Method to fetch and update the flag for expired inventory
	 *
	 */
	void fetchandUpdateExpiredInventory();

	/**
	 * Gets the reserved serial numbers.
	 *
	 * @param productModel
	 *           the product
	 * @param quoteModel
	 *           the quote
	 * @return the reserved serial numbers
	 */
	List<InventoryReservationModel> getReservedSerialNumbers(ProductModel productModel, QuoteModel quoteModel);

	/**
	 * Clear previous reserved serial numbers.
	 *
	 * @param quoteModel
	 *           the quote model
	 */
	void clearPreviousReservedSerialNumbers(QuoteModel quoteModel);

	/**
	 * Save reserved serial numbers for configuration.
	 *
	 * @param quoteModel
	 *           the quote model
	 * @param b2bCustomerModel
	 *           the b 2 b customer model
	 * @param endDate
	 *           the end date
	 * @param productModel
	 *           the product model
	 * @param serialNumber
	 *           the serial number
	 * @throws ModelSavingException
	 *            the model saving exception
	 */
	void saveReservedSerialNumbersForConfiguration(final QuoteModel quoteModel, final B2BCustomerModel b2bCustomerModel,
			final Date endDate, final ProductModel productModel, final String serialNumber) throws ModelSavingException;

	/**
	 * Adds the value estimation entry level.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param entryNumber
	 *           the entry number
	 * @param vetLink
	 *           the vet links
	 * @param vetProduct
	 *           the vet products
	 */
	void addValueEstimationEntryLevel(AbstractOrderModel abstractOrderModel, long entryNumber, String vetLink, String vetProduct);

	/**
	 * Removes the value estimation entry level.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param entryNumber
	 *           the entry number
	 * @param vetLink
	 *           the vet link
	 * @param vetProduct
	 *           the vet product
	 */
	void removeValueEstimationEntryLevel(AbstractOrderModel abstractOrderModel, long entryNumber, String vetLink,
			String vetProduct);

	/**
	 * Edits the value estimation entry level.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param entryNumber
	 *           the entry number
	 * @param catQuoteVETDatas
	 *           the cat quote VET datas
	 */
	void editValueEstimationEntryLevel(AbstractOrderModel abstractOrderModel, long entryNumber,
			List<CatQuoteVETData> catQuoteVETDatas);

}
