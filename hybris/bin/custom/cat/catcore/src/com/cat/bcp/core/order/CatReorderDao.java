/**
 *
 */
package com.cat.bcp.core.order;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import com.cat.bcp.core.model.AlertProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.RestockAlertModel;


/**
 * Class to seperate the reorder dao layer
 *
 * @author sagdhingra
 *
 */
public interface CatReorderDao
{


	/**
	 * Method to get low stock products
	 *
	 * @param b2bUnit
	 *           Fetch restock alerts model for dealer code
	 * @param salesModelId
	 *           Fetch restock alerts model for sales model id
	 * @param category
	 *           Fetch restock alerts model for category code
	 * @return List<RestockAlertModel> list of restock alerts.
	 */
	public List<RestockAlertModel> getLowStockProducts(B2BUnitModel b2bUnit, String category, String salesModelId);



	/**
	 * Method to get the associated config products based on alert id
	 *
	 * @param alertId
	 *           used to fetch the associated config products
	 * @return List<AlertProductModel> alert products model
	 */
	public List<AlertProductModel> getAssociatedConfigProducts(String alertId);


	/**
	 * Method to fetch the compatible config variant list specific to a lane.
	 *
	 * @param salesModelCode
	 *           Config Variants for a sales model code
	 * @param laneType
	 *           lane type whether lane2 or lane3
	 * @param configVariantId
	 *           config variant id to check the compatibility
	 * @return List<ConfigVariantProductModel> list of config variant products model
	 */
	public List<ConfigVariantProductModel> getCompatibleConfigProducts(String salesModelCode, String laneType,
			String configVariantId);



	/**
	 * Gets the alert product for config variant product.
	 *
	 * @param product
	 *           the product
	 * @return the alert product for config variant product
	 */
	public List<AlertProductModel> getAlertProductForConfigVariantProduct(ConfigVariantProductModel product);



	/**
	 * Gets the restock alerts for sales model.
	 *
	 * @param product
	 *           the product
	 * @return the restock alerts for sales model
	 */
	public List<RestockAlertModel> getRestockAlertsForSalesModel(ProductModel product);




}
