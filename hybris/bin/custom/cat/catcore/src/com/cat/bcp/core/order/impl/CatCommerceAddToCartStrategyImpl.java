/**
 *
 */
package com.cat.bcp.core.order.impl;

import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartModificationStatus;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceAddToCartStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import javax.annotation.Resource;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.servicelayer.CatConfigurableProductAddToCartMethodHook;


/**
 * @author vjagannadharaotel
 *
 */
public class CatCommerceAddToCartStrategyImpl extends DefaultCommerceAddToCartStrategy
{


	@Resource(name = "configurableProductAddToCartMethodHook")
	CatConfigurableProductAddToCartMethodHook catConfigurableProductAddToCartMethodHook;

	/**
	 * Do add to cart.
	 *
	 * @param parameter
	 *           the parameter
	 * @return the commerce cart modification
	 * @throws CommerceCartModificationException
	 *            the commerce cart modification exception
	 */
	@Override
	protected CommerceCartModification doAddToCart(final CommerceCartParameter parameter) throws CommerceCartModificationException
	{
		CommerceCartModification modification;

		final CartModel cartModel = parameter.getCart();
		final ProductModel productModel = parameter.getProduct();
		final long quantityToAdd = parameter.getQuantity();
		final PointOfServiceModel deliveryPointOfService = parameter.getPointOfService();

		this.beforeAddToCart(parameter);
		validateAddToCart(parameter);

		if (isProductForCode(parameter).booleanValue())
		{
			// So now work out what the maximum allowed to be added is (note that this may be negative!)
			final long actualAllowedQuantityChange = getAllowedCartAdjustmentForProduct(cartModel, productModel, quantityToAdd,
					deliveryPointOfService);
			final Integer maxOrderQuantity = productModel.getMaxOrderQuantity();
			final long cartLevel = checkCartLevel(productModel, cartModel, deliveryPointOfService);
			final long cartLevelAfterQuantityChange = actualAllowedQuantityChange + cartLevel;

			if (actualAllowedQuantityChange > 0)
			{
				// We are allowed to add items to the cart
				final CartEntryModel entryModel = addCartEntry(parameter, actualAllowedQuantityChange);
				//add ship to after date to the cart entry
				entryModel.setShipToAfter(parameter.getShipToAfterDate());
				entryModel.setTruckloadId(parameter.getTruckloadId());
				entryModel.setConfigurable(parameter.getConfigurable());
				entryModel.setTruckloadImageUrl(parameter.getTruckloadImageUrl());
				entryModel.setMandatoryProduct(parameter.getMandatoryProduct());
				entryModel.setConfigVariantId(parameter.getConfigVariantId());
				entryModel.setCompatibleModel(parameter.getCompatibleModel());
				getModelService().save(entryModel);

				final String statusCode = getStatusCodeAllowedQuantityChange(actualAllowedQuantityChange, maxOrderQuantity,
						quantityToAdd, cartLevelAfterQuantityChange);

				modification = createAddToCartResp(parameter, statusCode, entryModel, actualAllowedQuantityChange);
			}
			else
			{
				// Not allowed to add any quantity, or maybe even asked to reduce the quantity
				// Do nothing!
				final String status = getStatusCodeForNotAllowedQuantityChange(maxOrderQuantity, maxOrderQuantity);

				modification = createAddToCartResp(parameter, status, createEmptyCartEntry(parameter), 0);

			}
		}
		else
		{
			modification = createAddToCartResp(parameter, CommerceCartModificationStatus.UNAVAILABLE,
					createEmptyCartEntry(parameter), 0);
		}

		return modification;
	}




	/**
	 * Add to cart method for adding new products to cart for SP users only.
	 *
	 * @param parameter
	 *           the CommerceCartParameter
	 * @return the commerce cart modification CommerceCartModification
	 * @throws CommerceCartModificationException
	 *            the commerce cart modification exception
	 */
	@Override
	public CommerceCartModification addToCart(final CommerceCartParameter parameter) throws CommerceCartModificationException
	{
		final CommerceCartModification modification = doAddToCart(parameter);
		if (parameter.getProduct() != null && (parameter.getProduct().getCode().equals(CatCoreConstants.EPPPRODUCT)
				|| parameter.getProduct().getCode().equals(CatCoreConstants.CSAPRODUCT)))
		{
			catConfigurableProductAddToCartMethodHook.catAfterAddToCart(parameter, modification);
		}
		else
		{
			afterAddToCart(parameter, modification);
		}
		// Here the entry is fully populated, so we can search for a similar one and merge.

		mergeEntry(modification, parameter);
		getCommerceCartCalculationStrategy().recalculateCart(parameter);
		return modification;
	}


}
