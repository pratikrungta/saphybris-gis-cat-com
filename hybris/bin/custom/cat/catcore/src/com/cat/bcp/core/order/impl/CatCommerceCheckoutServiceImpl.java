/**
 *
 */
package com.cat.bcp.core.order.impl;

import de.hybris.platform.b2b.model.B2BCommentModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BOrderService;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCheckoutService;
import de.hybris.platform.commerceservices.strategies.CheckoutCustomerStrategy;
import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.order.OrderService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.exceptions.BusinessException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cat.bcp.core.dao.CatB2BUnitDao;
import com.cat.bcp.core.dao.CatCartDao;
import com.cat.bcp.core.dao.CatCommerceCheckoutDao;
import com.cat.bcp.core.event.CatAccessoriesPlaceOrderEvent;
import com.cat.bcp.core.event.CatPlaceOrderEvent;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.FBCPartsOrderInfoModel;
import com.cat.bcp.core.model.OrderAdditionalInfoModel;
import com.cat.bcp.core.model.TruckloadModel;
import com.cat.bcp.core.order.CatCommerceCheckoutService;
import com.cat.bcp.core.servicelayer.CatCartService;
import com.cat.facades.order.CATOrderReviewData;


/**
 * @author vjagannadharaotel
 *
 */
public class CatCommerceCheckoutServiceImpl extends DefaultCommerceCheckoutService implements CatCommerceCheckoutService
{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(CatCommerceCheckoutServiceImpl.class);

	private static final boolean IS_LOG_DEBUG_ENABLED = LOG.isDebugEnabled();

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "cartService")
	private CatCartService cartService;

	@Resource(name = "checkoutCustomerStrategy")
	CheckoutCustomerStrategy checkoutCustomerStrategy;

	@Resource(name = "orderService")
	private OrderService orderService;

	@Resource(name = "calculationService")
	private CalculationService calculationService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "b2bUnitDao")
	private CatB2BUnitDao b2bUnitDao;

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Resource(name = "b2bOrderService")
	private B2BOrderService b2bOrderService;

	@Resource(name = "productService")
	private ProductService productService;

	@Autowired
	private CustomerAccountService customerAccountService;
	@Autowired
	private EventService eventService;

	@Resource(name = "catCommerceCheckoutDao")
	private CatCommerceCheckoutDao catCommerceCheckoutDao;

	@Resource(name = "businessProcessService")
	private BusinessProcessService businessProcessService;

	@Resource(name = "catCartDao")
	private CatCartDao catCartDao;

	/**
	 * @return the cartService
	 */
	public CatCartService getCartService()
	{
		return cartService;
	}

	/**
	 * @param cartService
	 *           the cartService to set
	 */
	public void setCartService(final CatCartService cartService)
	{
		this.cartService = cartService;
	}

	/**
	 * @return the catCommerceCheckoutDao
	 */
	public CatCommerceCheckoutDao getCatCommerceCheckoutDao()
	{
		return catCommerceCheckoutDao;
	}

	/**
	 * @param catCommerceCheckoutDao
	 *           the catCommerceCheckoutDao to set
	 */
	public void setCatCommerceCheckoutDao(final CatCommerceCheckoutDao catCommerceCheckoutDao)
	{
		this.catCommerceCheckoutDao = catCommerceCheckoutDao;
	}

	/**
	 * @return the eventService
	 */
	public EventService getEventService()
	{
		return eventService;
	}

	/**
	 * @param eventService
	 *           the eventService to set
	 */
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

	/**
	 * @return the customerAccountService
	 */
	public CustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}

	/**
	 * @param customerAccountService
	 *           the customerAccountService to set
	 */
	public void setCustomerAccountService(final CustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}

	/**
	 * @param selectedShippingAddressPK
	 * @param order
	 */
	private void setShippingAddress(final String selectedShippingAddressPK, final AbstractOrderModel order)
	{
		if (StringUtils.isNotEmpty(selectedShippingAddressPK))
		{
			if (IS_LOG_DEBUG_ENABLED)
			{
				LOG.debug("Setting shipping address to local carts:" + order.getCode());
			}


			if (b2bUnitDao.findAddressModel(selectedShippingAddressPK) != null)
			{
				final AddressModel salesAreaShippingAddress = b2bUnitDao.findAddressModel(selectedShippingAddressPK);
				if (salesAreaShippingAddress.getOwner() instanceof B2BUnitModel)
				{
					final B2BUnitModel salesAreaDealer = (B2BUnitModel) salesAreaShippingAddress.getOwner();
					salesAreaShippingAddress.setSapCustomerID(salesAreaDealer.getUid());
					LOG.debug("SapCustomerID for Order" + order + "SAPCustomerID" + salesAreaDealer.getUid());
				}
				modelService.save(salesAreaShippingAddress);
				order.setDeliveryAddress(salesAreaShippingAddress);


			}

		}
	}

	/**
	 * @param order
	 */
	private void setOrderQuantityAndPrice(final OrderModel order)
	{
		if (CollectionUtils.isNotEmpty(order.getEntries()))
		{
			Date shipToAfterDate = null;
			for (final AbstractOrderEntryModel entry : order.getEntries())
			{
				shipToAfterDate = entry.getShipToAfter();
				modelService.save(entry);
			}
			order.setShipToAfter(shipToAfterDate);
			modelService.save(order);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateCartQuantity(final Long orderedQty)
	{
		final CartModel cartModel = cartService.getSessionCart();
		for (final AbstractOrderEntryModel entry : cartModel.getEntries())
		{
			entry.setQuantity(orderedQty);
			modelService.save(entry);
		}
		modelService.save(cartModel);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void splitOrderByPO(final String orderCode, final List<String> poList)
	{
		LOG.debug(" **** Entered Split Order Service ****");
		final OrderModel orderModel = b2bOrderService.getOrderForCode(orderCode);

		if (orderModel != null)
		{
			LOG.debug("**** Calling Split Order Event ****");
			getEventService().publishEvent(new CatPlaceOrderEvent(orderModel, poList));
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void splitOrderForAccessories(final String orderCode)
	{
		LOG.debug(" **** Entered Split Order Service For Accessories****");
		final OrderModel orderModel = b2bOrderService.getOrderForCode(orderCode);

		if (orderModel != null)
		{
			LOG.debug("**** Calling Split Order Event For Accessories****");
			getEventService().publishEvent(new CatAccessoriesPlaceOrderEvent(orderModel));
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OrderModel placeCatOrder(final String reviewOrderComments, final List<String> purchaseOrderNumbers,
			final String selectedShippingAddressPK) throws BusinessException
	{

		return getCreatedOrder(reviewOrderComments, purchaseOrderNumbers, selectedShippingAddressPK, false);
	}


	private OrderModel getCreatedOrder(final String reviewOrderComments, final List<String> purchaseOrderNumbers,
			final String selectedShippingAddressPK, final boolean isTruckloadMixAndMatchCheckout)
			throws InvalidCartException, CalculationException
	{
		if (cartService.hasSessionCart())
		{
			final CartModel cartModel = cartService.getSessionCart();
			final B2BCustomerModel customerModel = (B2BCustomerModel) checkoutCustomerStrategy.getCurrentUserForCheckout();
			final B2BUnitModel b2bUnitModel = customerModel.getDefaultB2BUnit();
			setReorderComment(reviewOrderComments, cartModel);
			if (!isTruckloadMixAndMatchCheckout)
			{
				setShippingAddress(selectedShippingAddressPK, cartModel);
			}
			modelService.save(cartModel);
			modelService.refresh(cartModel);
			cartModel.setUnit(b2bUnitModel);
			cartModel.setStatus(OrderStatus.PENDING_SUBMISSION);
			if (CollectionUtils.isNotEmpty(purchaseOrderNumbers) && CollectionUtils.size(purchaseOrderNumbers) == 1)
			{
				cartModel.setPurchaseOrderNumber(purchaseOrderNumbers.get(0));
			}

			modelService.save(cartModel);
			final OrderModel order = orderService.createOrderFromCart(cartModel);
			setOrderQuantityAndPrice(order);
			order.setDate(cartModel.getModifiedtime());
			order.setOrderGroupId(order.getCode());
			removePartVariants(order);
			modelService.save(order);
			final SearchResult<FBCPartsOrderInfoModel> searchResult = catCartDao.updateCartForConfigurableProducts();
			for (final FBCPartsOrderInfoModel fBCPartsOrderInfo : searchResult.getResult())
			{
				fBCPartsOrderInfo.setOrderId(order.getCode());
				modelService.save(fBCPartsOrderInfo);
			}
			cartService.removeSessionCart();


			return order;

		}
		return new OrderModel();
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public OrderModel placeCatOrderForAccessories(final String reviewOrderComments, final String selectedShippingAddressPK)
			throws BusinessException
	{
		if (cartService.hasSessionCart())
		{
			final CartModel cartModel = cartService.getSessionCart();
			final B2BCustomerModel customerModel = (B2BCustomerModel) checkoutCustomerStrategy.getCurrentUserForCheckout();
			final B2BUnitModel b2bUnitModel = customerModel.getDefaultB2BUnit();
			setReorderComment(reviewOrderComments, cartModel);
			setShippingAddress(selectedShippingAddressPK, cartModel);
			modelService.save(cartModel);
			modelService.refresh(cartModel);
			cartModel.setUnit(b2bUnitModel);
			cartModel.setStatus(OrderStatus.PENDING_SUBMISSION);

			modelService.save(cartModel);
			final OrderModel order = orderService.createOrderFromCart(cartModel);
			order.setDate(cartModel.getModifiedtime());
			order.setOrderGroupId(order.getCode());
			modelService.save(order);
			cartService.removeSessionCart();
			return order;
		}
		return new OrderModel();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createAdditionalInfo(final OrderModel localOrder, final String productCode)
	{
		final OrderAdditionalInfoModel orderAdditionalInfoModel = modelService.create(OrderAdditionalInfoModel.class);
		orderAdditionalInfoModel.setOrder(localOrder);
		if (productCode != null)
		{
			final ProductModel productModel = productService.getProductForCode(productCode);
			orderAdditionalInfoModel.setProductFamily(productModel.getSupercategories().iterator().next());
		}
		modelService.save(orderAdditionalInfoModel);
	}

	/**
	 * Remove part variant product from the cart before placing order. Remove part variant product from the order.
	 *
	 * @param orderModel
	 */
	private void removePartVariants(final OrderModel orderModel)
	{
		final List<AbstractOrderEntryModel> entries = orderModel.getEntries();
		final List<AbstractOrderEntryModel> configProductEntries = new ArrayList<>();

		for (final AbstractOrderEntryModel entry : entries)
		{
			final ProductModel product = entry.getProduct();
			if (product instanceof ConfigVariantProductModel)
			{
				configProductEntries.add(entry);
			}
		}
		orderModel.setEntries(configProductEntries);
	}

	/**
	 * Method to set Re-order comment.
	 *
	 * @param reviewOrderComments
	 * @param cartModel
	 */
	private void setReorderComment(final String reviewOrderComments, final CartModel cartModel)
	{
		if (StringUtils.isNotBlank(reviewOrderComments))
		{
			final B2BCommentModel reviewOrderComment = modelService.create(B2BCommentModel.class);
			reviewOrderComment.setComment(reviewOrderComments);
			modelService.save(reviewOrderComment);
			cartModel.setB2bcomments(Collections.singletonList(reviewOrderComment));
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<TruckloadModel> getTruckloadCombinations()
	{
		return catCommerceCheckoutDao.getTruckloadCombinations();
	}

	/**
	 * ({@inheritDoc}
	 */
	@Override
	public boolean validateTruckload(final int twoSeaterQty, final int fiveSeaterQty)
	{
		return catCommerceCheckoutDao.validateTruckload(twoSeaterQty, fiveSeaterQty);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<TruckloadModel> populateTruckloadSuggestions(final int twoSeaterQty, final int fiveSeaterQty)
	{
		SearchResult<TruckloadModel> searchResult = catCommerceCheckoutDao.getFiveSeaterSuggestion(fiveSeaterQty);
		final List<TruckloadModel> suggestedTruckloadList = new ArrayList();

		if (CollectionUtils.isNotEmpty(searchResult.getResult()))
		{
			final TruckloadModel truckloadModel = searchResult.getResult().get(0);
			truckloadModel.setIsFull(Boolean.FALSE);
			suggestedTruckloadList.add(truckloadModel);
		}

		searchResult = catCommerceCheckoutDao.getTwoSeaterSuggestion(twoSeaterQty);
		if (CollectionUtils.isNotEmpty(searchResult.getResult()))
		{
			final TruckloadModel truckloadModel = searchResult.getResult().get(0);
			truckloadModel.setIsFull(Boolean.FALSE);
			suggestedTruckloadList.add(truckloadModel);
		}
		return suggestedTruckloadList;

	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeTruckload(final String truckloadId) throws CalculationException
	{
		final CartModel cart = cartService.getSessionCart();
		final List<AbstractOrderEntryModel> cartEntries = cart.getEntries();
		final Map<Integer, Long> quantityMap = new HashMap<>();
		if (CollectionUtils.isNotEmpty(cartEntries))
		{
			for (final AbstractOrderEntryModel cartEntry : cartEntries)
			{
				final String truckId = cartEntry.getTruckloadId();
				if (StringUtils.equalsIgnoreCase(truckloadId, truckId))
				{
					quantityMap.put(cartEntry.getEntryNumber(), Long.valueOf(0L));
				}
			}
			cartService.updateQuantities(cart, quantityMap);
			calculationService.calculate(cart);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OrderModel placeTruckloadOrder(final CATOrderReviewData reviewOrderData) throws BusinessException
	{
		final OrderModel order = getCreatedOrder(reviewOrderData.getComments(), reviewOrderData.getPurchaseOrder(),
				reviewOrderData.getShippingAddressList().get(0), true);

		int addrCounter = 0;
		String truckloadId = StringUtils.EMPTY;

		for (final AbstractOrderEntryModel orderEntry : order.getEntries())
		{
			if (StringUtils.isEmpty(truckloadId) || StringUtils.equalsIgnoreCase(truckloadId, orderEntry.getTruckloadId()))
			{
				orderEntry.setShippingAddress(b2bUnitDao.findAddressModel(reviewOrderData.getShippingAddressList().get(addrCounter)));
			}
			else
			{
				addrCounter++;
				orderEntry.setShippingAddress(b2bUnitDao.findAddressModel(reviewOrderData.getShippingAddressList().get(addrCounter)));
			}

			//need to change the logic here for only one order entry
			truckloadId = orderEntry.getTruckloadId();
			modelService.save(orderEntry);
		}

		modelService.save(order);

		return order;
	}


}

