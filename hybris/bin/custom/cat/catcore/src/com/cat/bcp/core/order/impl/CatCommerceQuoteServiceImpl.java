/**
 *
 */
package com.cat.bcp.core.order.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.order.data.CatReserveQuoteData;
import de.hybris.platform.commerceservices.constants.CommerceServicesConstants;
import de.hybris.platform.commerceservices.enums.DiscountType;
import de.hybris.platform.commerceservices.enums.QuoteAction;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceQuoteService;
import de.hybris.platform.commerceservices.stock.CommerceStockService;
import de.hybris.platform.core.enums.QuoteState;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.order.AbstractOrderEntryService;
import de.hybris.platform.order.QuoteService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.textfieldconfiguratortemplateservices.model.TextFieldConfiguredProductInfoModel;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.CatCommerceQuoteDao;
import com.cat.bcp.core.dao.CatCustomerDao;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.InventoryReservationModel;
import com.cat.bcp.core.order.CatCommerceQuoteService;
import com.cat.facades.quote.CatQuoteVETData;
import com.google.common.math.DoubleMath;


/**
 * The Class CatCommerceQuoteServiceImpl.
 *
 * @author sagdhingra
 */
public class CatCommerceQuoteServiceImpl extends DefaultCommerceQuoteService implements CatCommerceQuoteService
{

	private static final Logger LOG = Logger.getLogger(CatCommerceQuoteServiceImpl.class);

	private static final double EPSILON = 0.00d;



	@Resource(name = "abstractOrderEntryService")
	AbstractOrderEntryService abstractOrderEntryService;

	@Resource
	private QuoteService quoteService;

	@Resource(name = "catCustomerDao")
	private CatCustomerDao catCustomerDao;

	@Resource(name = "commerceStockService")
	private CommerceStockService commerceStockService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "commerceQuoteDao")
	CatCommerceQuoteDao catCommerceQuoteDao;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "productService")
	private ProductService productService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void applyQuoteDiscountEntryLevel(final AbstractOrderModel abstractOrderModel, final long entryNumber,
			final UserModel currentQuoteUser, final double discntValue, final DiscountType percent)
	{
		validateParameterNotNullStandardMessage("abstractOrderModel", abstractOrderModel);
		validateParameterNotNullStandardMessage("user", currentQuoteUser);
		validateParameterNotNullStandardMessage("discountRate", discntValue);
		validateParameterNotNullStandardMessage("discountType", percent);

		QuoteModel quoteModel;
		if (abstractOrderModel instanceof CartModel)
		{
			validateQuoteCart((CartModel) abstractOrderModel);
			quoteModel = ((CartModel) abstractOrderModel).getQuoteReference();
		}
		else if (abstractOrderModel instanceof QuoteModel)
		{
			quoteModel = (QuoteModel) abstractOrderModel;
		}
		else
		{
			throw new IllegalArgumentException(
					"The abstract order model is neither a quote model nor a cart model created from quote model.");
		}
		final double discValue = discntValue;
		getQuoteActionValidationStrategy().validate(QuoteAction.DISCOUNT, quoteModel, currentQuoteUser);
		validateDiscountRate(discntValue, percent, abstractOrderModel, entryNumber);
		boolean isCalculationRequired = CollectionUtils
				.isNotEmpty(getOrderQuoteDiscountValuesAccessor().getQuoteDiscountValues(abstractOrderModel));


		try
		{
			if (discValue >= EPSILON)
			{
				isCalculationRequired = true;

				DiscountValue discountValue = null;
				if (!percent.equals(DiscountType.ABSOLUTE))
				{
					discountValue = createDiscountValue(Double.valueOf(discValue), percent,
							abstractOrderModel.getCurrency().getIsocode())
									.orElseThrow(() -> new IllegalArgumentException("Discount type cannot be created or supported"));
				}

				processDiscountOnEntryLevel(abstractOrderModel, discountValue, percent, discValue, entryNumber);

			}

			// calculate if existing ones have been removed and/or new ones have been added
			if (isCalculationRequired)
			{
				getCalculationService().calculateTotals(abstractOrderModel, true);
				getModelService().save(abstractOrderModel);
			}
		}
		catch (final CalculationException e)
		{
			LOG.error("Failed to calculate cart [" + abstractOrderModel.getCode() + "]", e);
			throw new SystemException("Could not calculate cart [" + abstractOrderModel.getCode() + "] due to : " + e.getMessage(),
					e);
		}

	}

	/**
	 * This method is used to validate Discount Rate
	 *
	 * @param rate
	 *           Rate
	 * @param discountType
	 *           Discount Type
	 * @param abstractOrderModel
	 *           Abstract Order Model
	 * @param entryNumber
	 *           Entry Number
	 */
	protected void validateDiscountRate(final double rate, final DiscountType discountType,
			final AbstractOrderModel abstractOrderModel, final long entryNumber)
	{
		if (DoubleMath.fuzzyCompare(rate, 0, EPSILON) < 0)
		{
			throw new IllegalArgumentException("The discount rate is less then 0!");
		}
		if (DiscountType.PERCENT.equals(discountType) && (DoubleMath.fuzzyCompare(rate, 100, EPSILON) > 0))
		{
			throw new IllegalArgumentException("Discount type is percent, but the discount rate is greater than 100!");
		}
		final AbstractOrderEntryModel abstractOrderEntryModel = getEntryForNumber(abstractOrderModel, entryNumber);
		final double basePriceForConfiguration = calculateBasePrice(abstractOrderEntryModel, abstractOrderEntryModel.getOrder());
		final double abstractOrderEntryModelPriceWithoutDiscount = basePriceForConfiguration
				* (abstractOrderEntryModel.getQuantity().longValue());
		if (DiscountType.ABSOLUTE.equals(discountType)
				&& DoubleMath.fuzzyCompare(rate, abstractOrderEntryModelPriceWithoutDiscount, EPSILON) > 0)
		{
			throw new IllegalArgumentException(
					"Discount type is absolute, but the discount rate is greater than total transaction price for entry.");
		}
	}

	/**
	 * This method is used to Calculate Base Price.
	 *
	 * @param entry
	 *           entry model for which base price is to be called
	 * @param order
	 *           the order model from which entries needs to be calculated.
	 * @return double Base Price
	 */
	private double calculateBasePrice(final AbstractOrderEntryModel entry, final AbstractOrderModel order)
	{
		BigDecimal basePrice = BigDecimal.valueOf(entry.getBasePrice());
		if (CollectionUtils.isNotEmpty(order.getEntries()))
		{
			for (final AbstractOrderEntryModel abstractOrderEntryModel : order.getEntries())
			{
				if (abstractOrderEntryModel.getMandatoryProduct().booleanValue()
						&& abstractOrderEntryModel.getConfigVariantId().equals(entry.getProduct().getCode()))
				{
					basePrice = basePrice.add(BigDecimal.valueOf(abstractOrderEntryModel.getBasePrice()));
				}
			}
		}
		return basePrice.doubleValue();
	}

	/**
	 * This method is used to get AbstractOrderEntryModel for given entry number
	 *
	 * @param order
	 *           to get the entries
	 * @param number
	 *           requested entry number
	 * @return AbstractOrderEntryModel
	 */
	protected AbstractOrderEntryModel getEntryForNumber(final AbstractOrderModel order, final long number)
	{
		final List<AbstractOrderEntryModel> entries = order.getEntries();
		if (CollectionUtils.isNotEmpty(entries) && Long.valueOf(number) != null)
		{
			final Integer requestedEntryNumber = Integer.valueOf((int) number);
			for (final AbstractOrderEntryModel entry : entries)
			{
				if (entry != null && requestedEntryNumber.equals(entry.getEntryNumber()))
				{
					return entry;
				}
			}
		}
		return null;
	}

	/**
	 * Process discount on entry level.
	 *
	 * @param abstractOrderModel
	 *           the abstract order model
	 * @param discountValue
	 *           the discount value
	 * @param percent
	 *           the percent
	 * @param discValue
	 *           the disc value
	 * @param entryNumber
	 *           the entry number
	 */
	private void processDiscountOnEntryLevel(final AbstractOrderModel abstractOrderModel, final DiscountValue discountValue,
			final DiscountType percent, final double discValue, final long entryNumber)
	{
		DiscountValue newDiscountValue = discountValue;
		for (final AbstractOrderEntryModel entry : abstractOrderModel.getEntries())
		{
			if (entry.getEntryNumber().intValue() == entryNumber)
			{
				removeDiscountValueIfAny(entry);
				if (percent.equals(DiscountType.ABSOLUTE))
				{
					newDiscountValue = DiscountValue.createAbsolute(CommerceServicesConstants.QUOTE_DISCOUNT_CODE,
							Double.valueOf(discValue / entry.getQuantity().longValue()), abstractOrderModel.getCurrency().getIsocode());
				}
				abstractOrderEntryService.addDiscountValue(entry, newDiscountValue);
				getModelService().save(entry);
			}
		}

	}

	/**
	 * Remove Discount Value from abstract order entry model.
	 *
	 * @param entry
	 *           entry from which disc val is to be removed
	 */
	private void removeDiscountValueIfAny(final AbstractOrderEntryModel entry)
	{
		final List<DiscountValue> discountValueList = entry.getDiscountValues();
		if (CollectionUtils.isNotEmpty(discountValueList))
		{
			for (final DiscountValue discVal : discountValueList)
			{
				abstractOrderEntryService.removeDiscountValue(entry, discVal);
			}
			getModelService().save(entry);
		}
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	protected Optional<DiscountValue> createDiscountValue(final Double discountRate, final DiscountType discountType,
			final String currencyIsoCode)
	{
		DiscountValue discountValue = null;

		if (DiscountType.PERCENT.equals(discountType))
		{
			discountValue = DiscountValue.createRelative(CommerceServicesConstants.QUOTE_DISCOUNT_CODE, discountRate);
		}
		else if (DiscountType.TARGET.equals(discountType))
		{
			discountValue = DiscountValue.createTargetPrice(CommerceServicesConstants.QUOTE_DISCOUNT_CODE, discountRate,
					currencyIsoCode);
		}

		return (discountType == null || discountValue == null) ? Optional.empty() : Optional.of(discountValue);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public QuoteModel submitQuote(final QuoteModel quoteModel, final UserModel userModel)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Submitting Quote");
		}
		final QuoteModel updatedQuoteModel = isSessionQuoteSameAsRequestedQuote(quoteModel)
				? updateQuoteFromCart(getCartService().getSessionCart(), userModel) : quoteModel;

		if (null == updatedQuoteModel.getQuoteCreatedTime())
		{
			updatedQuoteModel.setQuoteCreatedTime(new Date());
		}
		getModelService().save(updatedQuoteModel);
		getModelService().refresh(updatedQuoteModel);

		return updatedQuoteModel;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveQuoteDetails(final QuoteModel quoteModel)
	{
		getModelService().save(quoteModel);
		getModelService().refresh(quoteModel);
	}





	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteQuote(final QuoteModel quoteModel)
	{
		getModelService().remove(quoteModel.getPk());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CartModel loadQuoteAsSessionCart(final QuoteModel quoteModel, final UserModel userModel)
	{
		getQuoteActionValidationStrategy().validate(QuoteAction.EDIT, quoteModel, userModel);
		return updateAndLoadQuoteCartWithAction(quoteModel, QuoteAction.EDIT, userModel);
	}

	/**
	 * Method to retrieve the quote cart everytime a user comes again to edit
	 *
	 * @param quoteModel
	 *           Quote Model from which the cart will be returned
	 *
	 * @param quoteAction
	 *           Quote Action for validating quote action
	 *
	 * @param userModel
	 *           User Model for updating the quote state
	 */
	@Override
	protected CartModel updateAndLoadQuoteCartWithAction(final QuoteModel quoteModel, final QuoteAction quoteAction,
			final UserModel userModel)
	{
		// load quote to cart
		QuoteModel updatedQuoteModel = getQuoteUpdateExpirationTimeStrategy().updateExpirationTime(quoteAction, quoteModel,
				userModel);
		if (quoteModel.getState().equals(QuoteState.SELLER_DRAFT))
		{
			updatedQuoteModel = getQuoteUpdateStateStrategy().updateQuoteState(quoteAction, updatedQuoteModel, userModel);
			getModelService().save(updatedQuoteModel);
			getModelService().refresh(updatedQuoteModel);
		}
		else
		{
			updatedQuoteModel.setState(quoteModel.getState());
			getModelService().save(updatedQuoteModel);
			getModelService().refresh(updatedQuoteModel);
		}
		return getCartService().createCartFromQuote(updatedQuoteModel);
	}



	@Override
	public QuoteModel updateQuote(final String quoteCode, final boolean submittedToDSU) throws ModelSavingException
	{
		final QuoteModel quoteModel = quoteService.getCurrentQuoteForCode(quoteCode);
		if (submittedToDSU)
		{
			quoteModel.setState(QuoteState.SELLERAPPROVER_PENDING);

		}
		else
		{
			quoteModel.setState(QuoteState.BUYER_OFFER);
		}
		getModelService().save(quoteModel);
		return quoteModel;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getProductFamilies()
	{
		final CustomerModel currentCustomer = (CustomerModel) getUserService().getCurrentUser();
		final List<CategoryModel> categoryModels = catCustomerDao.findProductFamilies(currentCustomer);
		final List<String> productFamilies = new ArrayList<>();
		if (categoryModels != null)
		{
			for (final CategoryModel categoryModel : categoryModels)
			{
				productFamilies.add(categoryModel.getName());
			}
			productFamilies.sort(String.CASE_INSENSITIVE_ORDER);
		}
		return productFamilies;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductModel> getQuoteItemsWithNoStock(final String quoteCode)
	{
		final QuoteModel quote = quoteService.getCurrentQuoteForCode(quoteCode);
		final List<ProductModel> productWithNoStock = new ArrayList<ProductModel>();
		for (final AbstractOrderEntryModel quoteEntry : quote.getEntries())
		{
			final ProductModel configProduct = quoteEntry.getProduct();
			if (configProduct instanceof ConfigVariantProductModel)
			{
				addProductWithNoStock(productWithNoStock, configProduct);
			}
		}
		if (quote.getEntries().size() == productWithNoStock.size())
		{
			//All products in the quote are out of stock.
			return null; //NOSONAR
		}
		return productWithNoStock;
	}

	/**
	 * Add Product With No Stock in the List
	 *
	 * @param productWithNoStock
	 * @param configProduct
	 */
	private void addProductWithNoStock(final List<ProductModel> productWithNoStock, final ProductModel configProduct)
	{
		final B2BCustomerModel b2bCustomer = (B2BCustomerModel) userService.getCurrentUser();
		if (b2bCustomer.getDefaultB2BUnit() != null)
		{
			final long stock = calculateStock(configProduct, b2bCustomer);
			if (stock == 0)
			{
				productWithNoStock.add(configProduct);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long calculateStock(final ProductModel configProduct, final B2BCustomerModel b2bCustomer)
	{
		final List<PointOfServiceModel> pointOfServices = new ArrayList<>();
		pointOfServices.addAll(b2bCustomer.getDefaultB2BUnit().getPointsOfService());
		long stock = 0L;
		for (final PointOfServiceModel pos : pointOfServices)
		{
			final Long stockAtWarehouse = commerceStockService.getStockLevelForProductAndPointOfService(configProduct, pos);
			if (stockAtWarehouse != null)
			{
				stock += stockAtWarehouse.longValue();
			}
			else
			{
				//force out of stock scenario. make stock as positive number and return
				stock = 1;
			}
		}
		return stock;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<InventoryReservationModel> getReservationQuoteData(final ProductModel productModel,
			final B2BUnitModel b2bUnitModel)
	{
		return catCommerceQuoteDao.getReservationQuoteData(productModel, b2bUnitModel);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<InventoryReservationModel> getReservationQuoteDataForUpdate(final String serialNumbers,
			final B2BUnitModel b2bUnitModel)
	{
		return catCommerceQuoteDao.getReservationQuoteDataForUpdate(serialNumbers, b2bUnitModel);
	}

	/**
	 * This method is used to reserve Serial Numbers for configurations.
	 *
	 * @param catReserveQuoteData
	 * @param quoteModel
	 * @param inventoryReservationModel
	 *
	 */
	@Override
	public void reserveSerialNumbersForConfigurations(final CatReserveQuoteData catReserveQuoteData, final QuoteModel quoteModel,
			final InventoryReservationModel inventoryReservationModel)
	{
		inventoryReservationModel.setReservationEndDate(catReserveQuoteData.getEndDate());
		inventoryReservationModel.setReserved(Boolean.TRUE);
		inventoryReservationModel.setQuote(quoteModel);
		modelService.save(inventoryReservationModel);
		modelService.refresh(inventoryReservationModel);
	}


	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("deprecation")
	@Override
	public List<InventoryReservationModel> getReservedSerialNumbers(final ProductModel productmodel, final QuoteModel quoteModel)
	{
		final B2BCustomerModel b2bCustomerModel = (B2BCustomerModel) userService.getCurrentUser();

		return catCommerceQuoteDao.getReservedSerialNumbers(productmodel, b2bCustomerModel.getDefaultB2BUnit(), quoteModel);
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clearPreviousReservedSerialNumbers(final QuoteModel quoteModel)
	{
		final List<InventoryReservationModel> inventoryReservationModels = catCommerceQuoteDao
				.previousReservedSerialNumbers(quoteModel);
		if (CollectionUtils.isNotEmpty(inventoryReservationModels))
		{
			modelService.removeAll(inventoryReservationModels);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fetchandUpdateExpiredInventory()
	{
		final List<InventoryReservationModel> expiredInventoryReservation = catCommerceQuoteDao.fetchExpiredInventory();

		if (CollectionUtils.isNotEmpty(expiredInventoryReservation))
		{
			for (final InventoryReservationModel inventoryReservation : expiredInventoryReservation)
			{
				inventoryReservation.setQuote(null);
				inventoryReservation.setReserved(Boolean.FALSE);
				inventoryReservation.setReservationEndDate(null);
				modelService.save(inventoryReservation);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public QuoteModel requote(final QuoteModel quote, final UserModel user)
	{
		validateParameterNotNullStandardMessage("quoteModel", quote);

		getQuoteActionValidationStrategy().validate(QuoteAction.REQUOTE, quote, user);

		final QuoteModel quoteModel = getRequoteStrategy().requote(quote);

		getModelService().save(quoteModel);
		getModelService().refresh(quoteModel);

		final List<AbstractOrderEntryModel> entriesToBeRemoved = new ArrayList<>();
		final List<AbstractOrderEntryModel> eppOrCsaEntries = filterEppAndCsaProductsFromQuote(quoteModel);
		for (final AbstractOrderEntryModel entry : eppOrCsaEntries)
		{
			boolean checkIfOptionPresent = true;
			final Optional<AbstractOrderEntryProductInfoModel> configurationEntry = entry.getProductInfos().stream()
					.filter(configuration -> ((TextFieldConfiguredProductInfoModel) configuration).getConfigurationLabel()
							.equalsIgnoreCase(CatCoreConstants.EPPOPTION)
							|| ((TextFieldConfiguredProductInfoModel) configuration).getConfigurationLabel()
									.equalsIgnoreCase(CatCoreConstants.CSAOPTION))
					.findFirst();
			if (configurationEntry.isPresent())
			{
				checkIfOptionPresent = checkIfOptionPersisted(
						((TextFieldConfiguredProductInfoModel) configurationEntry.get()).getConfigurationLabel(),
						((TextFieldConfiguredProductInfoModel) configurationEntry.get()).getConfigurationValue());
			}
			if (!checkIfOptionPresent)
			{
				entriesToBeRemoved.add(entry);
			}
		}
		for (final AbstractOrderEntryModel entryModel : entriesToBeRemoved)
		{
			getModelService().remove(entryModel);
		}
		getModelService().save(quoteModel);
		getModelService().refresh(quoteModel);
		try
		{
			getCalculationService().recalculate(quoteModel);
		}
		catch (final CalculationException e)
		{
			LOG.error("Calculation exception for QUote" + quoteModel.getCode(), e);
		}
		return quoteModel;
	}


	/**
	 * Method to call the dao layer to check whether the option is present in db
	 *
	 * @param configurationLabel
	 *           option label i.e csa or epp
	 * @param configurationValue
	 *           value which is to be checked
	 * @return true if value is present
	 */
	private boolean checkIfOptionPersisted(final String configurationLabel, final String configurationValue)
	{
		return catCommerceQuoteDao.checkIfOptionPresent(configurationLabel, configurationValue);
	}

	/**
	 * Method to filter epp and csa products from quote model
	 *
	 * @param quoteModel
	 *           the quote model from which entries are to be fetched
	 * @return List<AbstractOrderEntryModel>
	 */
	private List<AbstractOrderEntryModel> filterEppAndCsaProductsFromQuote(final QuoteModel quoteModel)
	{
		return quoteModel.getEntries().stream()
				.filter(entry -> (!entry.getMandatoryProduct().booleanValue() && StringUtils.isNotBlank(entry.getConfigVariantId())))
				.collect(Collectors.toList());
	}

	/**
	 * Save reserved serial numbers for configuration.
	 *
	 * @param quoteModel
	 *           the quote model
	 * @param b2bCustomerModel
	 *           the b 2 b customer model
	 * @param endDate
	 *           the end date
	 * @param productModel
	 *           the product model
	 * @param serialNumber
	 *           the serial Number
	 */
	@Override
	public void saveReservedSerialNumbersForConfiguration(final QuoteModel quoteModel, final B2BCustomerModel b2bCustomerModel,
			final Date endDate, final ProductModel productModel, final String serialNumber) throws ModelSavingException
	{

		final InventoryReservationModel inventoryReservationModel = modelService.create(InventoryReservationModel.class);
		inventoryReservationModel.setSerialNumber(serialNumber);
		inventoryReservationModel.setConfigVariantProduct(productModel);
		inventoryReservationModel.setDealer(b2bCustomerModel.getDefaultB2BUnit());
		inventoryReservationModel.setReservationEndDate(endDate);
		inventoryReservationModel.setReserved(Boolean.TRUE);
		inventoryReservationModel.setQuote(quoteModel);
		inventoryReservationModel.setUpdateFlag(Boolean.TRUE);
		modelService.save(inventoryReservationModel);
		modelService.refresh(inventoryReservationModel);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addValueEstimationEntryLevel(final AbstractOrderModel abstractOrderModel, final long entryNumber,
			final String vetLink, final String vetProduct)
	{

		final Map<String, String> vetLinkMap = new HashMap<>();

		addValueAnalysis(abstractOrderModel, entryNumber, vetLink, vetProduct, vetLinkMap);
	}

	/**
	 * @param abstractOrderModel
	 * @param entryNumber
	 * @param vetLink
	 * @param vetProduct
	 * @param vetLinkMap
	 */
	private void addValueAnalysis(final AbstractOrderModel abstractOrderModel, final long entryNumber, final String vetLink,
			final String vetProduct, final Map<String, String> vetLinkMap)
	{
		for (final AbstractOrderEntryModel entry : abstractOrderModel.getEntries())
		{
			if (entry.getEntryNumber().intValue() == entryNumber)
			{
				vetLinkMap.put(vetLink, vetProduct);
				if (null != entry.getVETLinkMap() && !entry.getVETLinkMap().isEmpty())
				{
					vetLinkMap.putAll(entry.getVETLinkMap());
				}

				entry.setVETLinkMap(vetLinkMap);
				getModelService().save(entry);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeValueEstimationEntryLevel(final AbstractOrderModel abstractOrderModel, final long entryNumber,
			final String vetLink, final String vetProduct)
	{


		removeValueAnalysis(abstractOrderModel, entryNumber, vetLink, vetProduct);

	}

	/**
	 * @param abstractOrderModel
	 * @param entryNumber
	 * @param vetLink
	 * @param vetProduct
	 */
	private void removeValueAnalysis(final AbstractOrderModel abstractOrderModel, final long entryNumber, final String vetLink,
			final String vetProduct)
	{
		final Map<String, String> vetLinkMap = new HashMap<>();
		for (final AbstractOrderEntryModel entry : abstractOrderModel.getEntries())
		{
			if ((entry.getEntryNumber().intValue() == entryNumber)
					&& (null != entry.getVETLinkMap() && !entry.getVETLinkMap().isEmpty()))
			{
				vetLinkMap.putAll(entry.getVETLinkMap());
				vetLinkMap.remove(vetLink, vetProduct);
				entry.setVETLinkMap(vetLinkMap);
				getModelService().save(entry);

			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void editValueEstimationEntryLevel(final AbstractOrderModel abstractOrderModel, final long entryNumber,
			final List<CatQuoteVETData> catQuoteVETDatas)
	{
		final Map<String, String> vetLinkMap = new HashMap<>();

		editValueAnalysis(abstractOrderModel, entryNumber, catQuoteVETDatas, vetLinkMap);

	}

	/**
	 * @param abstractOrderModel
	 * @param entryNumber
	 * @param catQuoteVETDatas
	 * @param vetLinkMap
	 */
	private void editValueAnalysis(final AbstractOrderModel abstractOrderModel, final long entryNumber,
			final List<CatQuoteVETData> catQuoteVETDatas, final Map<String, String> vetLinkMap)
	{
		for (final AbstractOrderEntryModel entry : abstractOrderModel.getEntries())
		{
			if (entry.getEntryNumber().intValue() == entryNumber)
			{
				for (final CatQuoteVETData catQuoteVETData : catQuoteVETDatas)
				{
					vetLinkMap.put(catQuoteVETData.getVetLink(), catQuoteVETData.getVetProduct());
				}
				entry.setVETLinkMap(vetLinkMap);
				getModelService().save(entry);
			}
		}
	}
}
