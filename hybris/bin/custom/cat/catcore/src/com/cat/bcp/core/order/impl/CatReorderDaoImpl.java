/**
 *
 */
package com.cat.bcp.core.order.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.model.AlertProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.RestockAlertModel;
import com.cat.bcp.core.order.CatReorderDao;


/**
 * @author sagdhingra
 *
 */
public class CatReorderDaoImpl implements CatReorderDao
{

	@Resource(name = "flexibleSearchService")
	FlexibleSearchService flexibleSearchService;

	@Resource(name = "productService")
	ProductService productService;

	@Resource(name = "categoryService")
	CategoryService categoryService;

	@Resource(name = "userService")
	UserService userService;

	private static final String DEALER_PARAM = "dealer";

	private static final String REORDERABLE_FLAG = "isReorderable";

	private static final String ALERT_ID = "alertId";

	private static final String PRODUCT = "product";

	private static final String CATEGORY = "category";

	private static final String SALES_MODEL = "salesModel";

	private static final String LANE_TYPE = "laneType";

	private static final String CONFIG_ID = "configVariantId";

	private static final String CONFIG_VARIANT_PRODUCT = "configVariantProduct";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<RestockAlertModel> getLowStockProducts(final B2BUnitModel b2bUnit, final String category,
			final String salesModelId)
	{
		FlexibleSearchQuery query = null;
		if (StringUtils.isEmpty(category) && StringUtils.isEmpty(salesModelId))
		{
			query = new FlexibleSearchQuery(CatCoreConstants.LOW_STOCK_QUERY);
			query.addQueryParameter(DEALER_PARAM, b2bUnit);
			query.addQueryParameter(REORDERABLE_FLAG, Boolean.FALSE);
		}
		else if (!StringUtils.isEmpty(salesModelId))
		{
			query = new FlexibleSearchQuery(CatCoreConstants.LOW_STOCK_SALESMODEL_FILTER_QUERY);
			query.addQueryParameter(DEALER_PARAM, b2bUnit);
			query.addQueryParameter(PRODUCT, productService.getProductForCode(salesModelId));
		}
		else if (!StringUtils.isEmpty(category))
		{
			final CategoryModel categoryModel = categoryService.getCategoryForCode(category);
			if (categoryModel != null)
			{
				query = new FlexibleSearchQuery(CatCoreConstants.LOW_STOCK_CATEGORY_FILTER_QUERY);
				query.addQueryParameter(DEALER_PARAM, b2bUnit);
				query.addQueryParameter(CATEGORY, categoryModel);
				query.addQueryParameter(REORDERABLE_FLAG, Boolean.FALSE);
			}
		}
		final SearchResult<RestockAlertModel> searchResult = flexibleSearchService.search(query);

		if (null != searchResult.getResult() && !searchResult.getResult().isEmpty())
		{
			return searchResult.getResult();
		}
		else
		{
			return new ArrayList<RestockAlertModel>();
		}


	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AlertProductModel> getAssociatedConfigProducts(final String alertId)
	{
		List<AlertProductModel> alertProductSearchResult = new ArrayList<AlertProductModel>();
		RestockAlertModel restockAlertModel = null;
		final FlexibleSearchQuery restockAlertQuery = new FlexibleSearchQuery(CatCoreConstants.GET_RESTOCK_ALERT);
		restockAlertQuery.addQueryParameter(ALERT_ID, alertId);
		final SearchResult<RestockAlertModel> alertSearchResult = flexibleSearchService.search(restockAlertQuery);
		if (null != alertSearchResult.getResult() && !alertSearchResult.getResult().isEmpty())
		{
			restockAlertModel = alertSearchResult.getResult().get(0);
			final FlexibleSearchQuery query = new FlexibleSearchQuery(CatCoreConstants.GET_CONFIG_VARIANTS_QUERY);
			query.addQueryParameter(ALERT_ID, restockAlertModel);

			final SearchResult<AlertProductModel> searchResult = flexibleSearchService.search(query);

			if (null != searchResult.getResult() && !searchResult.getResult().isEmpty())
			{
				alertProductSearchResult = searchResult.getResult();
			}
		}
		return alertProductSearchResult;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ConfigVariantProductModel> getCompatibleConfigProducts(final String salesModelCode, final String laneType,
			final String configVariantId)
	{
		final FlexibleSearchQuery query = StringUtils.isNotEmpty(salesModelCode)
				? new FlexibleSearchQuery(CatCoreConstants.COMPATIBLE_CONFIG_PRODUCTS)
				: new FlexibleSearchQuery(CatCoreConstants.IS_CONFIG_COMPATIBLE);

		if (userService.getCurrentUser() instanceof B2BCustomerModel)
		{
			final B2BCustomerModel customer = (B2BCustomerModel) userService.getCurrentUser();
			if (StringUtils.isNotEmpty(salesModelCode))
			{
				query.addQueryParameter(SALES_MODEL, salesModelCode);
				query.addQueryParameter(DEALER_PARAM, customer.getDefaultB2BUnit());
			}
		}
		query.addQueryParameter(LANE_TYPE, laneType);

		if (StringUtils.isNotEmpty(configVariantId))
		{
			query.addQueryParameter(CONFIG_ID, configVariantId);
		}


		final SearchResult<ConfigVariantProductModel> searchResult = flexibleSearchService.search(query);
		return CollectionUtils.isNotEmpty(searchResult.getResult()) ? searchResult.getResult() : Collections.emptyList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AlertProductModel> getAlertProductForConfigVariantProduct(final ConfigVariantProductModel product)
	{
		final FlexibleSearchQuery alertProductQuery = new FlexibleSearchQuery(CatCoreConstants.GET_ALERT_PRODUCT);
		alertProductQuery.addQueryParameter(CONFIG_VARIANT_PRODUCT, product);
		final SearchResult<AlertProductModel> alertSearchResult = flexibleSearchService.search(alertProductQuery);
		return CollectionUtils.isNotEmpty(alertSearchResult.getResult()) ? alertSearchResult.getResult() : Collections.emptyList();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<RestockAlertModel> getRestockAlertsForSalesModel(final ProductModel product)
	{
		final FlexibleSearchQuery restockAlertProduct = new FlexibleSearchQuery(CatCoreConstants.GET_RESTOCK_PRODUCT);
		restockAlertProduct.addQueryParameter(PRODUCT, product);
		final SearchResult<RestockAlertModel> restockSearchResult = flexibleSearchService.search(restockAlertProduct);
		return CollectionUtils.isNotEmpty(restockSearchResult.getResult()) ? restockSearchResult.getResult()
				: Collections.emptyList();
	}
}
