/**
 *
 */
package com.cat.bcp.core.order.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import javax.annotation.Resource;

import com.cat.bcp.core.model.AlertProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.RestockAlertModel;
import com.cat.bcp.core.order.CatReorderDao;
import com.cat.bcp.core.order.CatReorderService;


/**
 * Class to seperate the reorder logic from Search related services
 *
 * @author sagdhingra
 *
 */
public class CatReorderServiceImpl implements CatReorderService
{
	@Resource(name = "catReorderDao")
	CatReorderDao catReorderDao;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<RestockAlertModel> getLowStockProducts(final B2BUnitModel b2bUnit, final String category,
			final String salesModelId)
	{
		return catReorderDao.getLowStockProducts(b2bUnit, category, salesModelId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AlertProductModel> getAssociatedConfigProducts(final String alertId)
	{
		return catReorderDao.getAssociatedConfigProducts(alertId);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ConfigVariantProductModel> getCompatibleConfigProducts(final String salesModelCode, final String laneType,
			final String configVariantId)
	{
		return catReorderDao.getCompatibleConfigProducts(salesModelCode, laneType, configVariantId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AlertProductModel> getAlertProductForConfigVariantProduct(final ConfigVariantProductModel product)
	{
		return catReorderDao.getAlertProductForConfigVariantProduct(product);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<RestockAlertModel> getRestockAlertsForSalesModel(final ProductModel product)
	{
		return catReorderDao.getRestockAlertsForSalesModel(product);
	}


}
