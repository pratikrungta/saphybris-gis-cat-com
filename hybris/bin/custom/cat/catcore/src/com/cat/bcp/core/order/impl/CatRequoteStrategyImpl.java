/**
 *
 */
package com.cat.bcp.core.order.impl;

import de.hybris.platform.commerceservices.order.impl.DefaultRequoteStrategy;
import de.hybris.platform.core.enums.QuoteState;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.QuoteModel;

import java.util.Date;

import org.apache.commons.collections.CollectionUtils;


/**
 * This class is to override default re-quote strategy to implement custom changes for CAT.
 *
 * @author vjagannadharaotel
 *
 */
public class CatRequoteStrategyImpl extends DefaultRequoteStrategy
{

	/**
	 * This method will override the OOTB post process method to persist discounts from the original, set quote state to
	 * "BUYER_OFFER", and set Quote name as required.
	 *
	 * @param original
	 *           The Original Quote model to replicate.
	 * @param copy
	 *           Replicated quote model.
	 */
	@Override
	protected void postProcess(final QuoteModel original, final QuoteModel copy)
	{
		copy.setComments(null);
		if (CollectionUtils.isNotEmpty(copy.getEntries()))
		{
			for (final AbstractOrderEntryModel orderEntry : copy.getEntries())
			{
				orderEntry.setComments(null);
			}
		}
		copy.setVersion(Integer.valueOf(1));
		copy.setGeneratedNotifications(null);
		copy.setName("Quote " + copy.getCode());
		copy.setState(QuoteState.SELLER_DRAFT);
		copy.setQuoteCreatedTime(new Date());
		copy.setSubmittedToCustomerDate(null);
		copy.setAcceptedByCustomerDate(null);
	}

}
