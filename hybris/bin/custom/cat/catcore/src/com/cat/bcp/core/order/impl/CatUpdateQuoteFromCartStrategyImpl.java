/**
 *
 */
package com.cat.bcp.core.order.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commerceservices.order.impl.DefaultUpdateQuoteFromCartStrategy;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.QuoteModel;

import java.util.Optional;


/**
 * @author vjagannadharaotel
 *
 */
public class CatUpdateQuoteFromCartStrategyImpl extends DefaultUpdateQuoteFromCartStrategy
{

	/**
	 * {@inheritDoc}
	 * This method is to add the quote replicated time to the new updated quote after submit.
	 */
	@Override
	public QuoteModel updateQuoteFromCart(final CartModel cart)
	{
		validateParameterNotNullStandardMessage("cart", cart);

		final QuoteModel outdatedQuote = getQuoteForCart(cart);
		final QuoteModel updatedQuote = clone(cart, Optional.of(outdatedQuote.getCode()));

		updatedQuote.setVersion(outdatedQuote.getVersion());
		updatedQuote.setState(outdatedQuote.getState());
		updatedQuote.setQuoteCreatedTime(outdatedQuote.getQuoteCreatedTime());
		updatedQuote.setPreviousEstimatedTotal(outdatedQuote.getPreviousEstimatedTotal());

		postProcess(cart, updatedQuote);

		return updatedQuote;
	}
}
