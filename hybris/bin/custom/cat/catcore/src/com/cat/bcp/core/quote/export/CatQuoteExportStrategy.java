/**
 *
 */
package com.cat.bcp.core.quote.export;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.quote.data.QuoteData;
import de.hybris.platform.commercefacades.user.data.CustomerData;

import java.io.FileInputStream;
import java.util.List;
import java.util.Map;


/**
 * The Interface CatQuoteExportStrategy.
 *
 * @author prrungta
 */
public interface CatQuoteExportStrategy
{

    /**
     * Method is used to prepare word documents pages & paragraphs using Apache Poi classes.
     *
     * @param fileStream
     *           the file stream
     * @param quoteData
     *           the quote data
     * @param customerdata
     *           the customer data
     *
     * @param productList
     *           the product list
     * @param localeMap
     *           the locale map
     * @param hostURL
     *           the host URL
     */
     void prepareExportDocument(final FileInputStream fileStream, final QuoteData quoteData, final CustomerData customerdata,
            List<ProductData> productList, Map<String, String> localeMap, String hostURL);

}
