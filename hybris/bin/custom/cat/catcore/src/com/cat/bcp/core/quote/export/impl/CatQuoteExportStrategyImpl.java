/**
 *
 */
package com.cat.bcp.core.quote.export.impl;

import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.commercefacades.quote.data.QuoteData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.util.Config;

import java.awt.Image;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.URL;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.BreakType;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.UnderlinePatterns;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.apache.xmlbeans.XmlCursor;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHMerge;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTVMerge;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STMerge;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.quote.export.CatQuoteExportStrategy;
import com.cat.facades.quote.CatQuoteVETContentData;
import com.cat.facades.quote.CatQuoteVETData;
import com.cat.facades.quote.CatQuoteVETResponseData;
import com.cat.facades.quote.CatQuoteVETVariableData;
import com.google.gson.Gson;


/**
 * The Class CatQuoteExportStrategyImpl.
 *
 * @author prrungta
 *
 */
public class CatQuoteExportStrategyImpl implements CatQuoteExportStrategy
{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(CatQuoteExportStrategyImpl.class);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void prepareExportDocument(final FileInputStream fileStream, final QuoteData quoteData, final CustomerData customerdata,
			final List<ProductData> productList, final Map<String, String> localeMap, final String hostURL)
	{
		XWPFDocument doc = null;
		final ClassLoader classLoader = getClass().getClassLoader();

		try
		{
			doc = new XWPFDocument(fileStream);
			final Map<String, String> data = replacePlaceHolders(quoteData, customerdata);
			for (final XWPFParagraph para : doc.getParagraphs())
			{
				getWordTextsReplace(data, para, quoteData);
			}
			createProductDetails(productList, doc, localeMap, hostURL);
			createCustomerDetailsPara(quoteData, customerdata, doc, localeMap);
			createCartDetails(quoteData, doc, localeMap);

			doc.write(new FileOutputStream(classLoader.getResource(CatCoreConstants.UPDATED_EXPORT_QUOTE_FILE).getFile()));
		}
		catch (final FileNotFoundException e)
		{
			LOG.error("Export file is not generated as file is not available on given absolute path ", e);
		}
		catch (final IOException e)
		{
			LOG.error("Export file is not found or file is not available on given absolute path ", e);
		}
		catch (final InvalidFormatException e)
		{
			LOG.error("Export file is not generated due to invalid exception ", e);

		}
	}

	/**
	 * Replace place holders.
	 *
	 * @param quoteData
	 *           the quote data
	 * @param customerdata
	 *           the customer data
	 * @return the map
	 */
	private Map<String, String> replacePlaceHolders(final QuoteData quoteData, final CustomerData customerData)
	{
		final Map<String, String> data = new HashMap();
		final Date date = new Date();
		final SimpleDateFormat sdf = new SimpleDateFormat(CatCoreConstants.DATE_EXPORT_PATTERN);
		data.put(CatCoreConstants.DATE_PLACE_HOLDER, sdf.format(date));
		data.put(CatCoreConstants.QUOTE_PLACE_HOLDER, quoteData.getCode());
		if (null != customerData.getUnit())
		{
			data.put(CatCoreConstants.DEALER_PLACE_HOLDER, customerData.getUnit().getName().toUpperCase());
		}
		data.put(CatCoreConstants.USER_PLACE_HOLDER, toCamelCase(customerData.getName()));
		data.put(CatCoreConstants.CUST_NAME_HOLDER, toCamelCase(quoteData.getCustomerName()));
		if (null != quoteData.getQuoteCustomer())
		{
			data.put(CatCoreConstants.CUST_EMAIL_HOLDER, quoteData.getCustomerEmail());
		}
		if (null != quoteData.getQuoteCustomer() && null != quoteData.getQuoteCustomer().getDefaultShippingAddress())
		{
			data.put(CatCoreConstants.CUST_ADDRESS_HOLDER, quoteData.getQuoteCustomer().getDefaultShippingAddress().getLine1() + " "
					+ quoteData.getQuoteCustomer().getDefaultShippingAddress().getLine2());
		}
		if (null != quoteData.getQuoteCustomer() && null != quoteData.getQuoteCustomer().getDefaultBillingAddress())
		{
			data.put(CatCoreConstants.CUST_BILLING_HOLDER, quoteData.getQuoteCustomer().getDefaultBillingAddress().getLine1() + " "
					+ quoteData.getQuoteCustomer().getDefaultBillingAddress().getLine2());
		}
		return data;
	}

	/**
	 * This method is used to add spaces between different para runs.
	 *
	 * @param paragraphQuoteRun
	 *           the paragraph quote run
	 */
	private void addSpaceRun(final XWPFRun paragraphQuoteRun)
	{
		paragraphQuoteRun.addBreak();
		paragraphQuoteRun.addBreak();
		paragraphQuoteRun.addBreak();
	}


	/**
	 * this method is used to add font for headers.
	 *
	 * @param paragraphQuoteRun
	 *           the paragraph quote run
	 * @param text
	 *           the text
	 */
	private void addProductHeaderFont(final XWPFRun paragraphQuoteRun, final String text)
	{
		paragraphQuoteRun.setBold(true);
		paragraphQuoteRun.setFontFamily(CatCoreConstants.OPEN_SANS);
		paragraphQuoteRun.setFontSize(16);
		paragraphQuoteRun.setText(text);
	}

	/**
	 * this method is used to add font for headers.
	 *
	 * @param paragraphQuoteRun
	 *           the paragraph quote run
	 * @param text
	 *           the text
	 */
	private void addHeaderFont(final XWPFRun paragraphQuoteRun, final String text)
	{
		paragraphQuoteRun.setBold(true);
		paragraphQuoteRun.setFontFamily(CatCoreConstants.OPEN_SANS);
		paragraphQuoteRun.setFontSize(14);
		paragraphQuoteRun.setText(text);
	}

	/**
	 * this method is used to add font for headers.
	 *
	 * @param paragraphQuoteRun
	 *           the paragraph quote run
	 * @param text
	 *           the text
	 */
	private void addConfigHeaderFont(final XWPFRun paragraphQuoteRun, final String text)
	{
		paragraphQuoteRun.setBold(true);
		paragraphQuoteRun.setFontFamily(CatCoreConstants.OPEN_SANS_SEMIBOLD);
		paragraphQuoteRun.setFontSize(14);
		paragraphQuoteRun.setText(text);
	}


	/**
	 * this method is used to create word paragraph for customer quote details.
	 *
	 * @param quoteData
	 *           the quote data
	 * @param customerData
	 *           the customer data
	 * @param doc
	 *           the doc
	 * @param localeMap
	 *           the locale map
	 */

	private void createCustomerDetailsPara(final QuoteData quoteData, final CustomerData customerData, final XWPFDocument doc,
			final Map<String, String> localeMap)
	{

		final XWPFParagraph paraQuoteDetails = doc.createParagraph();
		final XWPFRun paragraphQuoteRun = paraQuoteDetails.createRun();
		addHeaderFont(paragraphQuoteRun, localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_QUOTE_DETAILS));
		paragraphQuoteRun.addBreak();

		final String font1 = CatCoreConstants.FONT_LIGHT;
		final String font2 = CatCoreConstants.HELVETICA_NEUE_MEDIUM;

		createQuoteTable1(quoteData, customerData, doc, font1, font2, localeMap);

		final XWPFParagraph blankPara1 = doc.createParagraph();
		final XWPFRun blankParaRun1 = blankPara1.createRun();
		addSpaceRun(blankParaRun1);

		final XWPFParagraph customerDetails = doc.createParagraph();
		final XWPFRun customerDetailsRun = customerDetails.createRun();
		addHeaderFont(customerDetailsRun, localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_CUSTOMER_DETAILS));
		customerDetailsRun.addBreak();

		createQuoteTable2(quoteData, doc, font1, font2, localeMap);

		final XWPFParagraph blankPara2 = doc.createParagraph();
		final XWPFRun blankParaRun2 = blankPara2.createRun();

		addSpaceRun(blankParaRun2);
	}

	/**
	 * Creates the quote table 2.
	 *
	 * @param quoteData
	 *           the quote data
	 * @param doc
	 *           the doc
	 * @param font1
	 *           the font 1
	 * @param font2
	 *           the font 2
	 * @param localeMap
	 *           the locale map
	 */
	private void createQuoteTable2(final QuoteData quoteData, final XWPFDocument doc, final String font1, final String font2,
			final Map<String, String> localeMap)
	{
		final XWPFTable table = doc.createTable(3, 4);
		table.setWidth(4000);
		table.getCTTbl().getTblPr().unsetTblBorders();

		final XWPFParagraph t1p1 = table.getRow(0).getCell(0).getParagraphs().get(0);
		table.getRow(0).getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(1000));
		t1p1.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r1 = t1p1.createRun();
		setRunForTable(t1r1, localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_CUSTOMER_NAME), font1, 12);

		final XWPFParagraph t1p2 = table.getRow(0).getCell(1).getParagraphs().get(0);
		table.getRow(0).getCell(1).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(5000));
		t1p2.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r2 = t1p2.createRun();
		setRunForTable(t1r2, toCamelCase(quoteData.getCustomerName()), font2, 12);

		final XWPFParagraph t1p3 = table.getRow(0).getCell(2).getParagraphs().get(0);
		table.getRow(0).getCell(2).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(1000));
		t1p3.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r3 = t1p3.createRun();
		setRunForTable(t1r3, localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_CUSTOMER_EMAIL), font1, 12);

		final XWPFParagraph t1p4 = table.getRow(0).getCell(3).getParagraphs().get(0);
		table.getRow(0).getCell(3).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(5000));
		t1p4.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r4 = t1p4.createRun();
		setRunForTable(t1r4, quoteData.getCustomerEmail(), font2, 12);

		final XWPFParagraph t1p5 = table.getRow(2).getCell(0).getParagraphs().get(0);
		t1p5.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r5 = t1p5.createRun();
		setRunForTable(t1r5, localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_CUSTOMER_SHIPPING_ADDRESS), font1, 12);

		final XWPFParagraph t1p6 = table.getRow(2).getCell(1).getParagraphs().get(0);
		t1p6.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r6 = t1p6.createRun();
		final String shippingAddr1 = quoteData.getQuoteCustomer().getDefaultShippingAddress().getLine1() + " "
				+ quoteData.getQuoteCustomer().getDefaultShippingAddress().getLine2() + ", ";
		setRunForTable(t1r6, shippingAddr1, font2, 12);
		t1r6.addBreak();

		final XWPFRun t1r62 = t1p6.createRun();
		final String shippingAddr2 = quoteData.getQuoteCustomer().getDefaultShippingAddress().getTown() + ", "
				+ quoteData.getQuoteCustomer().getDefaultShippingAddress().getRegion().getName();
		setRunForTable(t1r62, shippingAddr2, font2, 12);
		t1r62.addBreak();

		final XWPFRun t1r63 = t1p6.createRun();
		final String shippingAddr3 = quoteData.getQuoteCustomer().getDefaultShippingAddress().getPostalCode();
		setRunForTable(t1r63, shippingAddr3, font2, 12);


		final XWPFParagraph t1p7 = table.getRow(2).getCell(2).getParagraphs().get(0);
		t1p7.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r7 = t1p7.createRun();
		setRunForTable(t1r7, localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_CUSTOMER_BILLING_ADDRESS), font1, 12);


		final XWPFParagraph t1p8 = table.getRow(2).getCell(3).getParagraphs().get(0);
		t1p8.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r8 = t1p8.createRun();
		final String billingAddr1 = quoteData.getQuoteCustomer().getDefaultBillingAddress().getLine1() + " "
				+ quoteData.getQuoteCustomer().getDefaultBillingAddress().getLine2() + ", ";
		setRunForTable(t1r8, billingAddr1, font2, 12);
		t1r8.addBreak();

		final XWPFRun t1r82 = t1p8.createRun();
		final String billingAddr2 = quoteData.getQuoteCustomer().getDefaultBillingAddress().getTown() + ", "
				+ quoteData.getQuoteCustomer().getDefaultBillingAddress().getRegion().getName();
		setRunForTable(t1r82, billingAddr2, font2, 12);
		t1r82.addBreak();

		final XWPFRun t1r83 = t1p8.createRun();
		final String billingAddr3 = quoteData.getQuoteCustomer().getDefaultBillingAddress().getPostalCode();
		setRunForTable(t1r83, billingAddr3, font2, 12);

		final XWPFParagraph blankPara = doc.createParagraph();
		final XWPFRun blankParaRun = blankPara.createRun();
		blankParaRun.setText("");

		final XWPFTable commentTable = doc.createTable(1, 2);
		commentTable.setWidth(4000);
		commentTable.getCTTbl().getTblPr().unsetTblBorders();

		final XWPFParagraph t1p9 = commentTable.getRow(0).getCell(0).getParagraphs().get(0);
		commentTable.getRow(0).getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(2100));
		t1p9.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r9 = t1p9.createRun();
		setRunForTable(t1r9, "COMMENTS", font1, 12);

		final XWPFParagraph t1p10 = commentTable.getRow(0).getCell(1).getParagraphs().get(0);
		commentTable.getRow(0).getCell(1).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(9900));
		t1p10.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r10 = t1p10.createRun();
		setRunForTable(t1r10, quoteData.getQuoteComments(), font2, 12);
	}

	/**
	 * Creates the quote table 1.
	 *
	 * @param quoteData
	 *           the quote data
	 * @param customerData
	 *           the customer data
	 * @param doc
	 *           the doc
	 * @param font1
	 *           the font 1
	 * @param font2
	 *           the font 2
	 * @param localeMap
	 *           the locale map
	 */
	private void createQuoteTable1(final QuoteData quoteData, final CustomerData customerData, final XWPFDocument doc,
			final String font1, final String font2, final Map<String, String> localeMap)
	{
		final XWPFTable table = doc.createTable(7, 4);
		table.setWidth(4000);
		table.getCTTbl().getTblPr().unsetTblBorders();

		final XWPFParagraph t1p1 = table.getRow(0).getCell(0).getParagraphs().get(0);
		table.getRow(0).getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(1000));
		t1p1.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r1 = t1p1.createRun();
		setRunForTable(t1r1, CatCoreConstants.QUOTE_NO, font1, 12);

		final XWPFParagraph t1p2 = table.getRow(0).getCell(1).getParagraphs().get(0);
		table.getRow(0).getCell(1).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(4000));
		t1p2.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r2 = t1p2.createRun();
		setRunForTable(t1r2, quoteData.getCode(), font2, 12);

		final XWPFParagraph t1p3 = table.getRow(0).getCell(2).getParagraphs().get(0);
		table.getRow(0).getCell(2).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(1000));
		t1p3.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r3 = t1p3.createRun();
		setRunForTable(t1r3, CatCoreConstants.OWNER, font1, 12);

		final XWPFParagraph t1p4 = table.getRow(0).getCell(3).getParagraphs().get(0);
		table.getRow(0).getCell(3).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(6000));
		t1p4.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r4 = t1p4.createRun();
		if (null != customerData.getUnit())
		{
			setRunForTable(t1r4, customerData.getUnit().getName().toUpperCase(), font2, 12);
		}

		final XWPFParagraph t1p5 = table.getRow(2).getCell(0).getParagraphs().get(0);
		t1p5.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r5 = t1p5.createRun();
		setRunForTable(t1r5, CatCoreConstants.QUOTATION_DATE, font1, 12);

		final XWPFParagraph t1p6 = table.getRow(2).getCell(1).getParagraphs().get(0);
		t1p6.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r6 = t1p6.createRun();
		setRunForTable(t1r6, new SimpleDateFormat(CatCoreConstants.DATE_PATTERN).format(quoteData.getCreationTime()), font2, 12);

		final XWPFParagraph t1p7 = table.getRow(2).getCell(2).getParagraphs().get(0);
		t1p7.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r7 = t1p7.createRun();
		setRunForTable(t1r7, CatCoreConstants.CREATED_BY, font1, 12);

		final XWPFParagraph t1p8 = table.getRow(2).getCell(3).getParagraphs().get(0);
		t1p8.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r8 = t1p8.createRun();
		setRunForTable(t1r8, toCamelCase(customerData.getName()), font2, 12);

		final XWPFParagraph t1p9 = table.getRow(4).getCell(0).getParagraphs().get(0);
		t1p9.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r9 = t1p9.createRun();
		setRunForTable(t1r9, localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_QUOTE_COUNTRY), font1, 12);

		final XWPFParagraph t1p10 = table.getRow(4).getCell(1).getParagraphs().get(0);
		t1p10.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r10 = t1p10.createRun();
		setRunForTable(t1r10, quoteData.getQuoteCustomer().getDefaultShippingAddress().getCountry().getName(), font2, 12);

		final XWPFParagraph t1p11 = table.getRow(4).getCell(2).getParagraphs().get(0);
		t1p11.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r11 = t1p11.createRun();
		setRunForTable(t1r11, localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_QUOTE_DESCRIPTION), font1, 12);

		final XWPFParagraph t1p12 = table.getRow(4).getCell(3).getParagraphs().get(0);
		t1p12.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r12 = t1p12.createRun();
		setRunForTable(t1r12, quoteData.getDescription(), font2, 12);

		final XWPFParagraph t1p13 = table.getRow(6).getCell(0).getParagraphs().get(0);
		t1p13.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r13 = t1p13.createRun();
		setRunForTable(t1r13, localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_LAST_DATE), font1, 12);

		final XWPFParagraph t1p14 = table.getRow(6).getCell(1).getParagraphs().get(0);
		t1p14.setAlignment(ParagraphAlignment.LEFT);
		final XWPFRun t1r14 = t1p14.createRun();
		setRunForTable(t1r14, new SimpleDateFormat(CatCoreConstants.DATE_PATTERN).format(quoteData.getUpdatedTime()), font2, 12);
	}

	/**
	 * this method is used to create word paragraph for product & its image details.
	 *
	 * @param productList
	 *           the product list
	 * @param doc
	 *           the doc
	 * @param localeMap
	 *           the locale map
	 * @param hostURL
	 *           the host URL
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 * @throws InvalidFormatException
	 *            the invalid format exception
	 */

	private void createProductDetails(final List<ProductData> productList, final XWPFDocument doc,
			final Map<String, String> localeMap, final String hostURL) throws IOException, InvalidFormatException
	{

		final XWPFParagraph headerParagraph = doc.createParagraph();
		final XWPFRun paragraphOneRunOne = headerParagraph.createRun();

		addProductHeaderFont(paragraphOneRunOne, localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_PRODUCT_DETAILS));
		paragraphOneRunOne.addBreak();

		for (final ProductData productData : productList)
		{
			final XWPFParagraph paragraph = doc.createParagraph();
			final XWPFRun paragraphOneRunTwo = paragraph.createRun();

			if (CollectionUtils.isNotEmpty(productData.getCategories()) && productData.getCategories().iterator().hasNext())
			{
				paragraphOneRunTwo.setText(productData.getCategories().iterator().next().getName() + " " + productData.getName());
			}

			paragraphOneRunTwo.setFontFamily(CatCoreConstants.HELVETICA_NEUE);
			paragraphOneRunTwo.setFontSize(14);
			paragraphOneRunTwo.setTextPosition(100);

			final XWPFTable table = doc.createTable(1, 2);
			table.setWidth(4000);
			table.getCTTbl().getTblPr().unsetTblBorders();

			final XWPFParagraph paragraph1 = table.getRow(0).getCell(0).getParagraphs().get(0);
			final XWPFRun paragraph1OneRunOne = paragraph1.createRun();
			final XWPFParagraph paragraph2 = table.getRow(0).getCell(1).getParagraphs().get(0);
			final XWPFRun paragraph2OneRunOne = paragraph2.createRun();

			try
			{
				final List<ImageData> imageList = getGalleryImages(productData);

				if (CollectionUtils.isNotEmpty(imageList))
				{

					final String sampleScene7URL = Config.getString(CatCoreConstants.SAMPLE_SCENE7_URL,
							CatCoreConstants.DEFAULT_SCENE7_URL);

					final String imageUrl = getImageUrl(hostURL, imageList, sampleScene7URL);

					final URL url = new URL(imageUrl);
					final Image image = ImageIO.read(url);
					final int format = XWPFDocument.PICTURE_TYPE_JPEG;
					final ByteArrayOutputStream os = new ByteArrayOutputStream();
					ImageIO.write((RenderedImage) image, CatCoreConstants.IMAGE_TYPE, os);
					final InputStream fis = new ByteArrayInputStream(os.toByteArray());
					paragraph1OneRunOne.addPicture(fis, format, null, Units.toEMU(132), Units.toEMU(104));
					paragraph1.setWordWrapped(true);
					LOG.debug("Image Rendered");
				}
			}
			catch (final IIOException ex)
			{
				LOG.error("Can't get input stream from URL!", ex);
			}
			catch (final IOException e)
			{
				LOG.error("Error while fetching scene7 image ", e);
			}
			catch (final Exception ex)
			{
				LOG.error("Export file is not found or file is not available on given absolute path ", ex);
			}

			paragraph2OneRunOne.setText(productData.getDescription());
			paragraph2OneRunOne.setFontFamily(CatCoreConstants.HELVETICA_NEUE_LIGHT);
			paragraph2OneRunOne.setFontSize(12);
			paragraph2.setWordWrapped(true);
			paragraph2.setAlignment(ParagraphAlignment.LEFT);

			final XWPFParagraph blankPara = doc.createParagraph();
			final XWPFRun blankParaRun = blankPara.createRun();
			blankParaRun.addBreak();

			final XWPFParagraph paragraph3 = doc.createParagraph();
			final XWPFRun paragraphOneRun4 = paragraph3.createRun();
			final XWPFRun paragraphOneRun5 = paragraph3.createRun();
			final XWPFRun paragraphOneRun6 = paragraph3.createRun();
			final XWPFRun paragraphOneRun7 = paragraph3.createRun();

			addConfigHeaderFont(paragraphOneRun4, localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_MACHINE_CONFIG));
			paragraphOneRun4.addBreak();

			final List<ProductReferenceData> references = productData.getProductReferences();

			for (final ProductReferenceData reference : references)
			{
				if (reference.getReferenceType().equals(ProductReferenceTypeEnum.PART))
				{

					paragraphOneRun5.setText("• " + reference.getTarget().getName());
					paragraphOneRun5.setFontFamily(CatCoreConstants.HELVETICA_NEUE_LIGHT);
					paragraphOneRun5.setFontSize(12);
					paragraphOneRun5.addBreak();
				}
			}
			addSpaceRun(paragraphOneRun5);
			addConfigHeaderFont(paragraphOneRun6, localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_MACHINE_ATTACHMENTS));
			paragraphOneRun6.addBreak();

			for (final ProductReferenceData reference : references)
			{
				if (reference.getReferenceType().equals(ProductReferenceTypeEnum.ATTACHMENT))
				{
					paragraphOneRun7.setText("• " + reference.getTarget().getName());
					paragraphOneRun7.setFontFamily(CatCoreConstants.HELVETICA_NEUE_LIGHT);
					paragraphOneRun7.setFontSize(12);
					paragraphOneRun7.addBreak();
				}
			}
			addSpaceRun(paragraphOneRun7);
			paragraphOneRun7.addBreak(BreakType.PAGE);
		}

	}

	/**
	 * Gets the image url.
	 *
	 * @param hostURL
	 *           the host URL
	 * @param imageList
	 *           the image list
	 * @param sampleScene7URL
	 *           the sample scene 7 URL
	 * @return the image url
	 */
	private String getImageUrl(final String hostURL, final List<ImageData> imageList, final String sampleScene7URL)
	{
		String imageUrl;
		if (imageList.get(0).getUrl().contains(sampleScene7URL))
		{
			imageUrl = imageList.get(0).getUrl();
		}
		else
		{
			imageUrl = hostURL + imageList.get(0).getUrl();
		}
		return imageUrl;
	}

	/**
	 * this method is used to create cart details on word export page.
	 *
	 * @param quoteData
	 *           the quote data
	 * @param doc
	 *           the doc
	 * @param localeMap
	 *           the locale map
	 */

	private final void createCartDetails(final QuoteData quoteData, final XWPFDocument doc, final Map<String, String> localeMap)
	{
		final XWPFParagraph paragraph3 = doc.createParagraph();
		final XWPFRun paragraphOneRunEight = paragraph3.createRun();
		paragraphOneRunEight.addBreak(BreakType.PAGE);

		addHeaderFont(paragraphOneRunEight, localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_CART_DETAILS));
		paragraphOneRunEight.addBreak();
		if (CollectionUtils.isNotEmpty(quoteData.getEntries()))
		{
			//add new table
			final XWPFTable table = doc.createTable();
			table.setWidth(4000);

			final XWPFTableRow row0 = table.getRow(0);

			// write to first row, first column
			final XWPFParagraph p1 = row0.getCell(0).addParagraph();
			row0.getCell(0).setColor(CatCoreConstants.EXPORT_CART_TABLE_BG_COLOR);
			p1.setAlignment(ParagraphAlignment.LEFT);
			setRunForTable(p1.createRun(), localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_CART_PRODUCT_FAMILY),
					CatCoreConstants.FONT_LIGHT, 10);

			// write to first row, second column
			row0.addNewTableCell();
			final XWPFParagraph p2 = row0.getCell(1).addParagraph();
			row0.getCell(1).setColor(CatCoreConstants.EXPORT_CART_TABLE_BG_COLOR);
			p2.setAlignment(ParagraphAlignment.LEFT);
			setRunForTable(p2.createRun(), localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_CART_CONFIG_ID),
					CatCoreConstants.FONT_LIGHT, 10);

			// write to first row, third column
			row0.addNewTableCell();
			final XWPFParagraph p3 = row0.getCell(2).addParagraph();
			row0.getCell(2).setColor(CatCoreConstants.EXPORT_CART_TABLE_BG_COLOR);
			p3.setAlignment(ParagraphAlignment.LEFT);
			setRunForTable(p3.createRun(), localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_CART_SALES_MODEL),
					CatCoreConstants.FONT_LIGHT, 10);

			// write to first row, fourth column
			row0.addNewTableCell();
			final XWPFParagraph p4 = row0.getCell(3).addParagraph();
			row0.getCell(3).setColor(CatCoreConstants.EXPORT_CART_TABLE_BG_COLOR);
			p4.setAlignment(ParagraphAlignment.LEFT);
			setRunForTable(p4.createRun(), localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_CART_LIST_PRICE),
					CatCoreConstants.FONT_LIGHT, 10);

			// write to fifth row, fifth column
			row0.addNewTableCell();
			final XWPFParagraph p5 = row0.getCell(4).addParagraph();
			row0.getCell(4).setColor(CatCoreConstants.EXPORT_CART_TABLE_BG_COLOR);
			p5.setAlignment(ParagraphAlignment.LEFT);
			setRunForTable(p5.createRun(), localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_CART_QTY), CatCoreConstants.FONT_LIGHT,
					10);

			// write to sixth row, sixth column
			row0.addNewTableCell();
			final XWPFParagraph p6 = row0.getCell(5).addParagraph();
			row0.getCell(5).setColor(CatCoreConstants.EXPORT_CART_TABLE_BG_COLOR);
			p6.setAlignment(ParagraphAlignment.LEFT);
			setRunForTable(p6.createRun(), localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_CART_DISCOUNT),
					CatCoreConstants.FONT_LIGHT, 10);

			// write to seventh row, seventh column
			row0.addNewTableCell();
			final XWPFParagraph p7 = row0.getCell(6).addParagraph();
			row0.getCell(6).setColor(CatCoreConstants.EXPORT_CART_TABLE_BG_COLOR);
			p7.setAlignment(ParagraphAlignment.LEFT);
			setRunForTable(p7.createRun(), localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_CART_TRANSACTION),
					CatCoreConstants.FONT_LIGHT, 10);

			int rowCount = 1;
			for (final OrderEntryData orderEntry : quoteData.getEntries())
			{
				table.createRow();
				final XWPFTableRow row = table.getRow(rowCount);
				String[] salesModel = null;
				if (null != orderEntry.getProduct())
				{
					final String categoryName = getCategoryName(orderEntry.getProduct());

					final XWPFParagraph pfParagraph = row.getCell(0).addParagraph();
					setRunForTable(pfParagraph.createRun(), categoryName, CatCoreConstants.HELVETICA_NEUE, 12);
					if (null != orderEntry.getProduct().getPrimaryOfferingName())
					{
						salesModel = orderEntry.getProduct().getPrimaryOfferingName().split(" ");
					}
					if (null != salesModel[0])
					{
						final XWPFParagraph nameParagraph = row.getCell(1).addParagraph();
						setRunForTable(nameParagraph.createRun(), orderEntry.getProduct().getName(), CatCoreConstants.HELVETICA_NEUE,
								12);
					}
					final XWPFParagraph salesModelParagraph = row.getCell(2).addParagraph();
					setRunForTable(salesModelParagraph.createRun(), salesModel[0], CatCoreConstants.HELVETICA_NEUE, 12);

					final XWPFParagraph basePriceParagraph = row.getCell(3).addParagraph();
					basePriceParagraph.setAlignment(ParagraphAlignment.RIGHT);
					setRunForTable(basePriceParagraph.createRun(), orderEntry.getBasePrice().getFormattedValue(),
							CatCoreConstants.HELVETICA_NEUE, 12);

					final XWPFParagraph qtyParagraph = row.getCell(4).addParagraph();
					qtyParagraph.setAlignment(ParagraphAlignment.RIGHT);
					setRunForTable(qtyParagraph.createRun(), orderEntry.getQuantity().toString(), CatCoreConstants.HELVETICA_NEUE, 12);

					final String discount = getDiscountValue(quoteData.getQuoteDiscountsType(), orderEntry);
					final XWPFParagraph discountParagraph = row.getCell(5).addParagraph();
					discountParagraph.setAlignment(ParagraphAlignment.RIGHT);
					setRunForTable(discountParagraph.createRun(), discount, CatCoreConstants.HELVETICA_NEUE, 12);

					final XWPFParagraph totalPriceParagraph = row.getCell(6).addParagraph();
					totalPriceParagraph.setAlignment(ParagraphAlignment.RIGHT);
					setRunForTable(totalPriceParagraph.createRun(), orderEntry.getTotalPrice().getFormattedValue(),
							CatCoreConstants.HELVETICA_NEUE, 12);

					rowCount = setAdditionalOrderInfo(table, rowCount, orderEntry, row);
					if (null != salesModel[0])
					{
						rowCount = setValueAnalysisInfo(table, rowCount, orderEntry, row, categoryName, salesModel[0]);
					}
					rowCount++;
				}
			}

			final XWPFParagraph blankPara1 = doc.createParagraph();
			final XWPFRun blankParaRun1 = blankPara1.createRun();
			addSpaceRun(blankParaRun1);

			createPriceTable(quoteData, doc, localeMap);

			final XWPFParagraph blankPara2 = doc.createParagraph();
			final XWPFRun blankParaRun2 = blankPara2.createRun();
			blankParaRun2.addBreak();
			blankParaRun2.addBreak();

			final XWPFParagraph tnc = doc.createParagraph();
			tnc.setAlignment(ParagraphAlignment.RIGHT);
			tnc.setWordWrapped(true);
			final XWPFRun tncRun = tnc.createRun();
			tncRun.setText(localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_QUOTE_TERMS));
			tncRun.setFontFamily(CatCoreConstants.FONT_LIGHT);
			tncRun.setFontSize(14);
			tncRun.setItalic(true);
		}
	}

	/**
	 * Sets the value analysis info.
	 *
	 * @param table
	 *           the table
	 * @param rowCount
	 *           the row count
	 * @param orderEntry
	 *           the order entry
	 * @param row
	 *           the row
	 * @param salesModel
	 * @param categoryName
	 * @return the int
	 */
	private int setValueAnalysisInfo(final XWPFTable table, int rowCount, final OrderEntryData orderEntry, final XWPFTableRow row,
			final String categoryName, final String salesModel)
	{

		int valueAnalysisCount = 0;

		if (CollectionUtils.isNotEmpty(orderEntry.getValueAnalysis()))
		{
			final CTVMerge vmerge = CTVMerge.Factory.newInstance();
			vmerge.setVal(STMerge.RESTART);
			row.getCell(0).getCTTc().addNewTcPr();
			row.getCell(0).getCTTc().getTcPr().setVMerge(vmerge);
			rowCount++;
			table.createRow();

			final XWPFTableRow vetRow = table.getRow(rowCount);

			final CTVMerge vmerge1 = CTVMerge.Factory.newInstance();
			vmerge1.setVal(STMerge.CONTINUE);
			vetRow.getCell(0).getCTTc().addNewTcPr();
			vetRow.getCell(0).getCTTc().getTcPr().setVMerge(vmerge1);

			mergeHorizontal(table, rowCount);
			for (final CatQuoteVETData catQuoteVETData : orderEntry.getValueAnalysis())
			{
				if (null != catQuoteVETData)
				{

					valueAnalysisCount++;


					final XWPFParagraph vetParagraph = vetRow.getCell(1).addParagraph();
					final XWPFRun vetRun1 = vetParagraph.createRun();
					setRunForTable(vetRun1,
							CatCoreConstants.VALUE_ANALYSIS + CatCoreConstants.SPACE_DELIMITER + valueAnalysisCount + ":",
							CatCoreConstants.FONT_LIGHT, 12);
					vetRun1.addBreak();
					setValueAnalysisData(catQuoteVETData, vetParagraph, categoryName, salesModel);
				}
			}
		}

		return rowCount;

	}

	/**
	 * Sets the value analysis data.
	 *
	 * @param catQuoteVETData
	 *           the cat quote VET data
	 * @param vetParagraph
	 *           the vet paragraph
	 * @param salesModel
	 * @param categoryName
	 */
	@SuppressWarnings("boxing")
	private void setValueAnalysisData(final CatQuoteVETData catQuoteVETData, final XWPFParagraph vetParagraph,
			final String categoryName, final String salesModel)
	{
		if (null != catQuoteVETData)
		{
			String vetMessage = CatCoreConstants.BIG_A + CatCoreConstants.CAT + categoryName + " " + salesModel
					+ CatCoreConstants.WAS + CatCoreConstants.COMPARED_TO + catQuoteVETData.getVetProduct();

			// VET Product
			if (null != catQuoteVETData.getVetProduct())
			{

				try
				{
					final double vetTotalSavings = getTotalSavings(catQuoteVETData);

					if (vetTotalSavings > 0)
					{
						vetMessage = CatCoreConstants.YOU_SAVE + "$" + vetTotalSavings + CatCoreConstants.WITH
								+ CatCoreConstants.SMALL_A + CatCoreConstants.CAT + categoryName + " " + salesModel
								+ CatCoreConstants.WHEN + CatCoreConstants.COMPARED_TO + catQuoteVETData.getVetProduct();
					}
				}
				catch (final Exception e)
				{
					LOG.error("Error Ocurred while calculating Total Savings:::::::" + e);

				}
				final XWPFRun vetRun4 = vetParagraph.createRun();
				setRunForTable(vetRun4, vetMessage, CatCoreConstants.HELVETICA_NEUE, 10);
				vetRun4.addBreak();

			}

			// VET Link
			if (null != catQuoteVETData.getVetLink())
			{
				final XWPFRun vetRun5 = vetParagraph.createRun();
				setRunForTable(vetRun5, CatCoreConstants.VET_LINK, CatCoreConstants.OPEN_SANS, 10);

				final XWPFRun vetRun6 = vetParagraph.createRun();
				vetRun6.setFontFamily(CatCoreConstants.HELVETICA_NEUE);
				vetRun6.setFontSize(8);
				vetRun6.setText(catQuoteVETData.getVetLink());
				vetRun6.setUnderline(UnderlinePatterns.SINGLE);
				vetRun6.setBold(true);
				vetRun6.addBreak();
			}
		}
	}

	/**
	 * @param catQuoteVETData
	 * @throws IOException
	 */
	@SuppressWarnings("boxing")
	private double getTotalSavings(final CatQuoteVETData catQuoteVETData) throws IOException
	{

		Response response;
		final String vetLink = catQuoteVETData.getVetLink();
		final String vetlinkStudyId = vetLink.split("studyId=")[1];
		final String vetApiLink = CatCoreConstants.VET_API + vetlinkStudyId;
		LOG.info(vetApiLink);
		if (Boolean.parseBoolean(Config.getParameter(CatCoreConstants.SPECCHECK_PROXYENABLED)))
		{
			final String proxyPort = Config.getParameter(CatCoreConstants.SPECCHECK_PROXYPORT);
			final String proxyUrl = Config.getParameter(CatCoreConstants.SPECCHECK_PROXYURL);
			LOG.debug(proxyPort + " - Port Number - " + Integer.parseInt(proxyPort));
			LOG.debug("proxy URL - " + proxyUrl);
			final Proxy proxy = new Proxy(Type.HTTP, new InetSocketAddress(proxyUrl, Integer.parseInt(proxyPort)));
			response = Jsoup.connect(vetApiLink).ignoreContentType(true).proxy(proxy).execute();
		}
		else
		{
			response = Jsoup.connect(vetApiLink).ignoreContentType(true).execute();
		}
		final String json = response.body();
		final CatQuoteVETResponseData vetResponseData = new Gson().fromJson(json, CatQuoteVETResponseData.class);
		final List<CatQuoteVETContentData> contents = vetResponseData.getData().getContent();
		final List<CatQuoteVETVariableData> variables = contents.get(0).getVariables();
		final Double costPerHour1 = Double.valueOf(variables.get(9).getFirstValue());
		final Double costPerHour2 = Double.valueOf(variables.get(9).getSecondValue());
		final Double totalHours1 = Double.valueOf(variables.get(2).getFirstValue().replace(",", ""));
		final Double totalHours2 = Double.valueOf(variables.get(2).getSecondValue().replace(",", ""));
		final Double totalSavings = (costPerHour2 * totalHours2) - (costPerHour1 * totalHours1);
		BigDecimal bd = new BigDecimal(Double.toString(totalSavings));
		bd = bd.setScale(4, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	/**
	 * Create table for price related information
	 *
	 * @param quoteData
	 *           the quote data
	 * @param doc
	 *           the doc
	 * @param localeMap
	 *           the locale map
	 */
	private void createPriceTable(final QuoteData quoteData, final XWPFDocument doc, final Map<String, String> localeMap)
	{
		final String font1 = CatCoreConstants.FONT_LIGHT;
		final String font2 = CatCoreConstants.HELVETICA_NEUE_MEDIUM;

		final XWPFTable priceTable = doc.createTable(4, 6);
		priceTable.setWidth(4000);
		priceTable.getCTTbl().getTblPr().unsetTblBorders();

		priceTable.getRow(0).getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(1600));
		priceTable.getRow(0).getCell(0).setText("");

		priceTable.getRow(0).getCell(1).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(1600));
		priceTable.getRow(0).getCell(1).setText("");

		priceTable.getRow(0).getCell(2).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(1600));
		priceTable.getRow(0).getCell(2).setText("");

		priceTable.getRow(0).getCell(3).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(1600));
		priceTable.getRow(0).getCell(3).setText("");

		final XWPFParagraph tlp = priceTable.getRow(0).getCell(4).getParagraphs().get(0);
		priceTable.getRow(0).getCell(4).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(4000));
		tlp.setAlignment(ParagraphAlignment.RIGHT);
		final XWPFRun tlpRun = tlp.createRun();

		final XWPFParagraph tlp2 = priceTable.getRow(0).getCell(5).getParagraphs().get(0);
		priceTable.getRow(0).getCell(5).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(1600));
		tlp2.setAlignment(ParagraphAlignment.RIGHT);
		final XWPFRun tlp2Run = tlp2.createRun();

		final BigDecimal totalListPrice = quoteData.getTotalPrice().getValue().add(quoteData.getTotalDiscounts().getValue());
		String totalListPriceRounded = null;

		if (null != totalListPrice)
		{
			totalListPriceRounded = getPriceFormattedValue(totalListPrice);
		}

		if (StringUtils.isNotEmpty(totalListPriceRounded))
		{
			setRunForTable(tlpRun, localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_QUOTE_PRICE), font1, 12);
			setRunForTable(tlp2Run, totalListPriceRounded, font2, 12);
		}

		final XWPFParagraph td = priceTable.getRow(1).getCell(4).getParagraphs().get(0);
		td.setAlignment(ParagraphAlignment.RIGHT);
		final XWPFRun tdRun = td.createRun();

		final XWPFParagraph td2 = priceTable.getRow(1).getCell(5).getParagraphs().get(0);
		td2.setAlignment(ParagraphAlignment.RIGHT);
		final XWPFRun td2Run = td2.createRun();


		String totalDiscountRounded = null;
		if (null != quoteData.getTotalDiscounts().getValue())
		{
			totalDiscountRounded = getPriceFormattedValue(quoteData.getTotalDiscounts().getValue());

		}
		setRunForTable(tdRun, localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_QUOTE_DISCOUNT), font1, 12);
		setRunForTable(td2Run, totalDiscountRounded, font2, 12);

		final XWPFParagraph ttp = priceTable.getRow(3).getCell(4).getParagraphs().get(0);
		ttp.setAlignment(ParagraphAlignment.RIGHT);
		final XWPFRun ttpRun = ttp.createRun();

		final XWPFParagraph ttp2 = priceTable.getRow(3).getCell(5).getParagraphs().get(0);
		ttp2.setAlignment(ParagraphAlignment.RIGHT);
		final XWPFRun ttp2Run = ttp2.createRun();

		setRunForTable(ttpRun, localeMap.get(CatCoreConstants.CAT_QUOTE_EXPORT_QUOTE_TRANSACTION), font1, 12);
		setRunForTable(ttp2Run, quoteData.getTotalPrice().getFormattedValue(), CatCoreConstants.HELVETICA_NEUE, 12);
		ttp2Run.setBold(true);
	}

	/**
	 * @param priceValue
	 *           price Value
	 * @return formatted price value
	 */
	public String getPriceFormattedValue(final BigDecimal priceValue)
	{
		String price = StringUtils.EMPTY;
		try
		{
			final NumberFormat numberFormat = NumberFormat.getCurrencyInstance(Locale.US);
			price = numberFormat.format(priceValue);
		}
		catch (final NumberFormatException e)
		{
			LOG.error("Error while formatting price value", e);
		}
		return price;
	}

	/**
	 * Set Additional Order Info - EPP and CSA
	 *
	 * @param table
	 * @param rowCount
	 * @param orderEntry
	 * @param row
	 * @return rowCount
	 */
	private int setAdditionalOrderInfo(final XWPFTable table, int rowCount, final OrderEntryData orderEntry,
			final XWPFTableRow row)
	{
		if (CollectionUtils.isNotEmpty(orderEntry.getEppEntries()) || CollectionUtils.isNotEmpty(orderEntry.getCsaEntries()))
		{
			final CTVMerge vmerge = CTVMerge.Factory.newInstance();
			vmerge.setVal(STMerge.RESTART);
			row.getCell(0).getCTTc().addNewTcPr();
			row.getCell(0).getCTTc().getTcPr().setVMerge(vmerge);
		}

		int eppProductCount = 0;

		if (CollectionUtils.isNotEmpty(orderEntry.getEppEntries()))
		{
			for (final OrderEntryData eppAdditonalInfoEntry : orderEntry.getEppEntries())
			{
				if (null != eppAdditonalInfoEntry)
				{
					rowCount++;
					eppProductCount++;
					table.createRow();
					final XWPFTableRow eppRow = table.getRow(rowCount);

					final CTVMerge vmerge1 = CTVMerge.Factory.newInstance();
					vmerge1.setVal(STMerge.CONTINUE);
					eppRow.getCell(0).getCTTc().addNewTcPr();
					eppRow.getCell(0).getCTTc().getTcPr().setVMerge(vmerge1);

					mergeHorizontal(table, rowCount);

					final XWPFParagraph eppParagraph = eppRow.getCell(1).addParagraph();
					final XWPFRun eppRun1 = eppParagraph.createRun();
					setRunForTable(eppRun1, CatCoreConstants.EPP_OPTION_APPLIED + CatCoreConstants.SPACE_DELIMITER + eppProductCount
							+ CatCoreConstants.SPACE_DELIMITER + CatCoreConstants.EPP_CSA_APPLIED + CatCoreConstants.SPACE_DELIMITER,
							CatCoreConstants.FONT_LIGHT, 12);

					setEPPConfigurationInfo(eppAdditonalInfoEntry, eppParagraph);

					final XWPFParagraph eppQtyParagraph = eppRow.getCell(4).addParagraph();
					eppQtyParagraph.setAlignment(ParagraphAlignment.RIGHT);
					final XWPFRun eppQtyRun = eppQtyParagraph.createRun();
					setRunForTable(eppQtyRun, eppAdditonalInfoEntry.getQuantity().toString(), CatCoreConstants.HELVETICA_NEUE, 12);

					final XWPFParagraph eppDiscountParagraph = eppRow.getCell(5).addParagraph();
					eppDiscountParagraph.setAlignment(ParagraphAlignment.RIGHT);
					final XWPFRun eppDiscountRun = eppDiscountParagraph.createRun();
					setRunForTable(eppDiscountRun, "-", CatCoreConstants.HELVETICA_NEUE, 12);

					final XWPFParagraph eppPriceParagraph = eppRow.getCell(6).addParagraph();
					eppPriceParagraph.setAlignment(ParagraphAlignment.RIGHT);
					final XWPFRun eppPriceRun = eppPriceParagraph.createRun();
					setRunForTable(eppPriceRun, eppAdditonalInfoEntry.getTotalPrice().getFormattedValue(),
							CatCoreConstants.HELVETICA_NEUE, 12);
				}
			}
		}

		int csaProductCount = 0;
		if (CollectionUtils.isNotEmpty(orderEntry.getCsaEntries()))
		{
			for (final OrderEntryData csaAdditonalInfoEntry : orderEntry.getCsaEntries())
			{
				if (null != csaAdditonalInfoEntry)
				{
					rowCount++;
					csaProductCount++;
					table.createRow();
					final XWPFTableRow csaRow = table.getRow(rowCount);

					final CTVMerge vmerge1 = CTVMerge.Factory.newInstance();
					vmerge1.setVal(STMerge.CONTINUE);
					csaRow.getCell(0).getCTTc().addNewTcPr();
					csaRow.getCell(0).getCTTc().getTcPr().setVMerge(vmerge1);

					mergeHorizontal(table, rowCount);

					final XWPFParagraph csaParagraph = csaRow.getCell(1).addParagraph();
					final XWPFRun csaRun1 = csaParagraph.createRun();
					setRunForTable(csaRun1, CatCoreConstants.CSA_OPTION_APPLIED + CatCoreConstants.SPACE_DELIMITER + csaProductCount
							+ CatCoreConstants.SPACE_DELIMITER + CatCoreConstants.EPP_CSA_APPLIED + CatCoreConstants.SPACE_DELIMITER,
							CatCoreConstants.FONT_LIGHT, 12);

					setCSAConfigurationInfo(csaAdditonalInfoEntry, csaParagraph);

					final XWPFParagraph csaQtyParagraph = csaRow.getCell(4).addParagraph();
					csaQtyParagraph.setAlignment(ParagraphAlignment.RIGHT);
					final XWPFRun csaQtyRun = csaQtyParagraph.createRun();
					setRunForTable(csaQtyRun, csaAdditonalInfoEntry.getQuantity().toString(), CatCoreConstants.HELVETICA_NEUE, 12);

					final XWPFParagraph csaDiscountParagraph = csaRow.getCell(5).addParagraph();
					csaDiscountParagraph.setAlignment(ParagraphAlignment.RIGHT);
					final XWPFRun csaDiscountRun = csaDiscountParagraph.createRun();
					setRunForTable(csaDiscountRun, "-", CatCoreConstants.HELVETICA_NEUE, 12);

					final XWPFParagraph csaPriceParagraph = csaRow.getCell(6).addParagraph();
					csaPriceParagraph.setAlignment(ParagraphAlignment.RIGHT);
					final XWPFRun csaPriceRun = csaPriceParagraph.createRun();
					setRunForTable(csaPriceRun, csaAdditonalInfoEntry.getTotalPrice().getFormattedValue(),
							CatCoreConstants.HELVETICA_NEUE, 12);

				}
			}
		}

		return rowCount;

	}

	/**
	 * Set CSA Configuration Info
	 *
	 * @param additonalInfoEntry
	 * @param csaParagraph
	 */
	private void setCSAConfigurationInfo(final OrderEntryData additonalInfoEntry, final XWPFParagraph csaParagraph)
	{
		final List<ConfigurationInfoData> configurationInfo = additonalInfoEntry.getConfigurationInfos();
		if (CollectionUtils.isNotEmpty(configurationInfo))
		{
			final XWPFRun csaRun2 = csaParagraph.createRun();
			if (null != configurationInfo.get(1))
			{
				setRunForTable(csaRun2, configurationInfo.get(1).getConfigurationValue(), CatCoreConstants.HELVETICA_NEUE, 12);
				csaRun2.addBreak();
			}

			if (null != configurationInfo.get(0))
			{
				final XWPFRun csaRun3 = csaParagraph.createRun();
				setRunForTable(csaRun3, CatCoreConstants.HOURS, CatCoreConstants.FONT_LIGHT, 12);

				final XWPFRun csaRun4 = csaParagraph.createRun();
				setRunForTable(csaRun4, configurationInfo.get(0).getConfigurationValue(), CatCoreConstants.HELVETICA_NEUE, 12);
				csaRun4.addBreak();
			}

			if (null != configurationInfo.get(2))
			{
				final XWPFRun csaRun5 = csaParagraph.createRun();
				setRunForTable(csaRun5, CatCoreConstants.YEARS, CatCoreConstants.FONT_LIGHT, 12);

				final XWPFRun csaRun6 = csaParagraph.createRun();
				setRunForTable(csaRun6, configurationInfo.get(2).getConfigurationValue(), CatCoreConstants.HELVETICA_NEUE, 12);
			}
		}
	}

	/**
	 * Set EPP Configuration Info
	 *
	 * @param additonalInfoEntry
	 * @param eppParagraph
	 */
	private void setEPPConfigurationInfo(final OrderEntryData additonalInfoEntry, final XWPFParagraph eppParagraph)
	{
		if (CollectionUtils.isNotEmpty(additonalInfoEntry.getConfigurationInfos()))
		{
			for (final ConfigurationInfoData configInfo : additonalInfoEntry.getConfigurationInfos())
			{
				final XWPFRun eppRun2 = eppParagraph.createRun();
				setRunForTable(eppRun2, configInfo.getConfigurationValue(), CatCoreConstants.HELVETICA_NEUE, 12);
			}
		}
	}

	/**
	 * Merge Table Cells Horizontally for EPP and CSA
	 *
	 * @param table
	 * @param rowCount
	 */
	private void mergeHorizontal(final XWPFTable table, final int rowCount)
	{
		final CTHMerge hMerge = CTHMerge.Factory.newInstance();
		hMerge.setVal(STMerge.RESTART);
		table.getRow(rowCount).getCell(1).getCTTc().addNewTcPr();
		table.getRow(rowCount).getCell(1).getCTTc().getTcPr().setHMerge(hMerge);

		final CTHMerge hMerge1 = CTHMerge.Factory.newInstance();
		hMerge1.setVal(STMerge.CONTINUE);
		table.getRow(rowCount).getCell(2).getCTTc().addNewTcPr();
		table.getRow(rowCount).getCell(2).getCTTc().getTcPr().setHMerge(hMerge1);

		final CTHMerge hMerge2 = CTHMerge.Factory.newInstance();
		hMerge2.setVal(STMerge.CONTINUE);
		table.getRow(rowCount).getCell(3).getCTTc().addNewTcPr();
		table.getRow(rowCount).getCell(3).getCTTc().getTcPr().setHMerge(hMerge2);
	}

	/**
	 * Get Discount value to be displayed.
	 *
	 * @param quoteDiscountType
	 * @param orderEntry
	 * @return string discountValue
	 */
	private String getDiscountValue(final String quoteDiscountType, final OrderEntryData orderEntry)
	{
		if (null != orderEntry.getDiscount() && null != orderEntry.getDiscount().getValue())
		{
			if (CatCoreConstants.ABSOLUTE_DISCOUNT_TYPE.equals(quoteDiscountType))
			{
				return CatCoreConstants.ABSOLUTE_DISCOUNT_PREFIX + orderEntry.getDiscount().getValue().toString();
			}
			else if (CatCoreConstants.PERCENT_DISCOUNT_TYPE.equals(quoteDiscountType))
			{
				return orderEntry.getDiscount().getValue().toString() + CatCoreConstants.PERCENT_DISCOUNT_PREFIX;
			}
		}
		return "-";
	}

	/**
	 * Sets the run for table.
	 *
	 * @param run
	 *           the run
	 * @param text
	 *           the text
	 * @param font
	 *           the font
	 */
	private static void setRunForTable(final XWPFRun run, final String text, final String font, final int fontSize)
	{
		run.setFontFamily(font);
		run.setFontSize(fontSize);
		run.setText(text);
	}

	/**
	 * To camel case.
	 *
	 * @param s
	 *           the s
	 * @return the string
	 */
	private static String toCamelCase(final String s)
	{
		final String[] parts = s.split(" ");
		StringBuilder camelCaseString = new StringBuilder("");
		for (final String part : parts)
		{
			camelCaseString = camelCaseString.append(toProperCase(part));

			if (!(camelCaseString.length() == s.length()))
			{
				camelCaseString.append(" ");
			}
		}
		return camelCaseString.toString();
	}

	/**
	 * To proper case.
	 *
	 * @param s
	 *           the s
	 * @return the string
	 */
	private static String toProperCase(final String s)
	{
		return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
	}

	/**
	 * Gets the word texts replace.
	 *
	 * @param data
	 *           the data
	 * @param para
	 *           the para
	 * @param quoteData
	 *           the quote data
	 */
	private void getWordTextsReplace(final Map<String, String> data, final XWPFParagraph para, final QuoteData quoteData)
	{
		final List<XWPFRun> runs = para.getRuns();
		if (runs != null)
		{
			for (final XWPFRun r : runs)
			{
				String text = r.getText(0);
				if (text != null && data.containsKey(text))
				{
					text = text.replace(text, data.get(text));
					r.setText(text, 0);
				}
				if (text != null && text.equals(CatCoreConstants.PLACE_HOLDER))
				{
					text = text.replace(text, "");
					r.setText(text, 0);

					final XmlCursor cursor = para.getCTP().newCursor();
					final XWPFTable table = para.getDocument().insertNewTbl(cursor);
					table.getCTTbl().getTblPr().unsetTblBorders();
					table.setCellMargins(0, 500, 0, 0);

					final XWPFTableRow curRow = table.getRow(0);
					final XWPFParagraph p1 = curRow.getCell(0).addParagraph();
					setRunForTable(p1.createRun(), CatCoreConstants.PRODUCT_FAMILY, CatCoreConstants.HELVETICA_NEUE_LIGHT, 12);

					curRow.addNewTableCell();
					final XWPFParagraph p2 = curRow.getCell(1).addParagraph();
					setRunForTable(p2.createRun(), CatCoreConstants.SALES_MODEL, CatCoreConstants.HELVETICA_NEUE_LIGHT, 12);

					fillProductFamilyTable(quoteData, table);
				}
			}
		}
	}

	/**
	 * Fill Product Family - Sales Model Table (Doc Page 1)
	 *
	 * @param quoteData
	 * @param table
	 */
	private void fillProductFamilyTable(final QuoteData quoteData, final XWPFTable table)
	{
		int rowCount = 1;
		if (CollectionUtils.isNotEmpty(quoteData.getEntries()))
		{
			for (final OrderEntryData orderEntry : quoteData.getEntries())
			{
				if (null != orderEntry.getProduct())
				{
					final String categoryName = getCategoryName(orderEntry.getProduct());

					final String[] salesModel = orderEntry.getProduct().getPrimaryOfferingName().split(" ");

					table.createRow();
					final XWPFTableRow newRow = table.getRow(rowCount);

					final XWPFParagraph p3 = newRow.getCell(0).addParagraph();
					setRunForTable(p3.createRun(), categoryName, CatCoreConstants.HELVETICA_NEUE_MEDIUM, 12);

					final XWPFParagraph p4 = newRow.getCell(1).addParagraph();
					setRunForTable(p4.createRun(), salesModel[0], CatCoreConstants.HELVETICA_NEUE_MEDIUM, 12);

					rowCount++;
				}

			}

		}
	}

	/**
	 * Fetch product category name
	 *
	 * @param product
	 * @return categoryName
	 */
	private String getCategoryName(final ProductData product)
	{
		String categoryName = null;
		if (CollectionUtils.isNotEmpty(product.getCategories()) && product.getCategories().iterator().hasNext())
		{
			categoryName = product.getCategories().iterator().next().getName();
		}
		return categoryName;
	}


	/**
	 * Gets the gallery images.
	 *
	 * @param productData
	 *           the product data
	 * @return the gallery images
	 */
	private List<ImageData> getGalleryImages(final ProductData productData)
	{
		List<ImageData> images = null;
		if (CollectionUtils.isNotEmpty(productData.getImages()))
		{
			images = new ArrayList<>();
			for (final ImageData image : productData.getImages())
			{
				if (ImageDataType.GALLERY.equals(image.getImageType()))
				{
					images.add(image);
				}
			}
			if (CollectionUtils.isNotEmpty(images))
			{
				images.sort((final ImageData image1, final ImageData image2) -> image1.getGalleryIndex()
						.compareTo(image2.getGalleryIndex()));
			}
		}
		return images;
	}
}
