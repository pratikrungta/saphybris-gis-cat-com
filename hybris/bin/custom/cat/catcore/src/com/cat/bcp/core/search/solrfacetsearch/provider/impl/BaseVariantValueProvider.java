/**
 *
 */
package com.cat.bcp.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;


/**
 * @author sankale
 *
 */
public class BaseVariantValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider
{
	private FieldNameProvider fieldNameProvider;

	private static final Logger LOG = Logger.getLogger(BaseVariantValueProvider.class);

	protected FieldNameProvider getFieldNameProvider()
	{
		return fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}


	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		if (model instanceof ConfigVariantProductModel)
		{
			final ConfigVariantProductModel product = (ConfigVariantProductModel) model;
			final BaseVariantProductModel baseModel = (BaseVariantProductModel) product.getBaseProduct();

			final Collection<FieldValue> fieldValues = new ArrayList<FieldValue>();

			fieldValues.add(createFieldValue(baseModel.getCode(), indexedProperty));

			return fieldValues;
		}
		else
		{
			LOG.error("Cannot get SalesModel Id of non-product item");
			throw new FieldValueProviderException("Cannot get SalesModel Id of non-product item");
		}
	}



	protected FieldValue createFieldValue(final String salesModelId, final IndexedProperty indexedProperty)
	{
		final String fieldName = getFieldNameProvider().getFieldName(indexedProperty, null, FieldNameProvider.FieldType.INDEX);

		return new FieldValue(fieldName, salesModelId);
	}

}
