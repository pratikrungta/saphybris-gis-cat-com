/**
 *
 */
package com.cat.bcp.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.ordersplitting.daos.impl.DefaultWarehouseDao;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.stock.StockService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Required;

import com.cat.bcp.core.enums.WarehouseTypeEnum;
import com.cat.bcp.core.model.ConfigVariantProductModel;


/**
 * @author sankale
 *
 */
public class CatAvailabilityValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider
{

	@Resource
	private StockService stockService;

	@Resource
	private DefaultWarehouseDao warehouseDao;

	private FieldNameProvider fieldNameProvider;

	protected FieldNameProvider getFieldNameProvider()
	{
		return fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}


	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		if (model instanceof ConfigVariantProductModel)
		{
			Boolean available = false;
			final ConfigVariantProductModel product = (ConfigVariantProductModel) model;

			final Collection<FieldValue> fieldValues = new ArrayList<FieldValue>();

			// Fetch all the warehouses in which current product has stock level present (Quantity can be zero)
			//	And then filter out warehouses whose type is MFU
			final List<WarehouseModel> warehouses = warehouseDao.getWarehouses(product.getCode());

			if (!warehouses.isEmpty())
			{
				if ("inStockMFUFlag".equals(indexedProperty.getName()))
				{
					available = isAvailableInMFU(warehouses);
				}
				else if ("inStockPDCFlag".equals(indexedProperty.getName()))
				{
					available = isAvailableInPDC(warehouses);
				}
			}
			else
			{
				LOG.info("CatAvailabilityValueProvider : Product - " + product.getCode() + "does not exist in any warehouse !");
				return Collections.emptyList();
			}

			fieldValues.add(createFieldValue(available, indexedProperty));

			return fieldValues;
		}
		else
		{
			throw new FieldValueProviderException(
					"CatAvailabilityValueProvider : Skipping Entity as model is not of type ConfigVariantModel");
		}
	}

	/**
	 * @param available
	 * @param warehouses
	 * @return
	 */
	private Boolean isAvailableInPDC(final List<WarehouseModel> warehouses)
	{
		Boolean available = false;
		for (final WarehouseModel warehouse : warehouses)
		{
			if (WarehouseTypeEnum.PDC.equals(warehouse.getWarehouseType()))
			{
				available = true;
				break;
			}
		}
		return available;
	}

	/**
	 * @param warehouses
	 * @return Boolean
	 */
	private Boolean isAvailableInMFU(final List<WarehouseModel> warehouses)
	{
		Boolean available = false;
		for (final WarehouseModel warehouse : warehouses)
		{
			if (WarehouseTypeEnum.MFU.equals(warehouse.getWarehouseType()))
			{
				available = true;
				break;
			}
		}
		return available;
	}



	protected FieldValue createFieldValue(final Boolean isAvailable, final IndexedProperty indexedProperty)
	{
		final String fieldName = getFieldNameProvider().getFieldName(indexedProperty, null, FieldNameProvider.FieldType.INDEX);

		return new FieldValue(fieldName, isAvailable);
	}

}
