/**
 *
 */
package com.cat.bcp.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.search.solrfacetsearch.provider.CategorySource;
import de.hybris.platform.ordersplitting.daos.impl.DefaultWarehouseDao;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.stock.StockService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import com.cat.bcp.core.enums.WarehouseTypeEnum;
import com.cat.bcp.core.model.ConfigVariantProductModel;



/**
 * @author asomjal
 *
 *         This Value Provider is used to index categories and warehouse type code. Index property
 *         :'catAvailableInWsAndCategory'
 */
public class CatAvailableInWsAndCategoryValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider
{
	private FieldNameProvider fieldNameProvider;
	private CategorySource categorySource;
	private CommonI18NService commonI18NService;

	@Resource
	private DefaultWarehouseDao warehouseDao;

	@Resource
	private StockService stockService;

	protected CategorySource getCategorySource()
	{
		return categorySource;
	}

	@Required
	public void setCategorySource(final CategorySource categorySource)
	{
		this.categorySource = categorySource;
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	protected FieldNameProvider getFieldNameProvider()
	{
		return fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	/**
	 * This method is used to index 'catAvailableInWsAndCategory' property.
	 *
	 * @param indexConfig
	 * @param indexedProperty
	 * @param model
	 * @return Collection of field value.
	 */
	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		final Collection<FieldValue> fieldValues = new ArrayList();
		if (model instanceof ConfigVariantProductModel)
		{
			//populate Warehouse code
			final ConfigVariantProductModel product = (ConfigVariantProductModel) model;
			// Fetch all the warehouses in which current product has stock level present (Quantity can be zero)

			final List<WarehouseModel> warehouses = warehouseDao.getWarehouses(product.getCode());
			if (CollectionUtils.isNotEmpty(warehouses))
			{
				for (final WarehouseModel warehouse : warehouses)
				{
					final String warehouseCode = getWarehouseCode(warehouse);
					fieldValues.addAll(createFieldValue(warehouseCode, indexedProperty));
				}
			}
			//populate Category Code
			final Collection<CategoryModel> categories = getCategorySource().getCategoriesForConfigAndProperty(indexConfig,
					indexedProperty, model);
			if (CollectionUtils.isNotEmpty(categories))
			{
				for (final CategoryModel category : categories)
				{
					fieldValues.addAll(createFieldValue(category, indexedProperty));
				}
			}
		}
		return fieldValues;
	}

	/**
	 * This method is used to create Field values.
	 *
	 * @param category
	 * @param indexedProperty
	 * @return List of field Values.
	 */
	protected List<FieldValue> createFieldValue(final CategoryModel category, final IndexedProperty indexedProperty)
	{
		final List<FieldValue> fieldValues = new ArrayList();
		final Object value = getPropertyValue(category);
		final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty, null);
		for (final String fieldName : fieldNames)
		{
			fieldValues.add(new FieldValue(fieldName, value));
		}
		return fieldValues;
	}

	protected Object getPropertyValue(final Object model)
	{
		return getPropertyValue(model, "code");
	}

	protected Object getPropertyValue(final Object model, final String propertyName)
	{
		return modelService.getAttributeValue(model, propertyName);
	}

	/**
	 * Get Warehouse Code - MFU, PDC or Dealer Warehouse Code
	 *
	 * @param warehouse
	 * @return String
	 */
	private String getWarehouseCode(final WarehouseModel warehouse)
	{
		String warehouseCode = "";
		if (WarehouseTypeEnum.MFU.equals(warehouse.getWarehouseType()))
		{
			warehouseCode = WarehouseTypeEnum.MFU.name();
		}
		else if (WarehouseTypeEnum.PDC.equals(warehouse.getWarehouseType()))
		{
			warehouseCode = WarehouseTypeEnum.PDC.name();
		}
		else if (WarehouseTypeEnum.WAREHOUSE.equals(warehouse.getWarehouseType()))
		{
			warehouseCode = warehouse.getCode();
		}
		return warehouseCode;
	}


	/**
	 * This method is used to create Field values.
	 *
	 * @param isAvailable
	 * @param indexedProperty
	 * @return List of field values.
	 */
	protected List<FieldValue> createFieldValue(final String isAvailable, final IndexedProperty indexedProperty)
	{
		final List<FieldValue> fieldValues = new ArrayList();

		final Collection<String> fieldNames = fieldNameProvider.getFieldNames(indexedProperty, null);
		for (final String fieldName : fieldNames)
		{
			fieldValues.add(new FieldValue(fieldName, isAvailable));
		}
		return fieldValues;
	}

}
