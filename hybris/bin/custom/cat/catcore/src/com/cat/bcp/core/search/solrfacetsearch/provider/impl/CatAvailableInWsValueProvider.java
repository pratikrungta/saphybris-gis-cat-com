/**
 *
 */
package com.cat.bcp.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.ordersplitting.daos.impl.DefaultWarehouseDao;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;
import de.hybris.platform.stock.StockService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.cat.bcp.core.enums.WarehouseTypeEnum;
import com.cat.bcp.core.model.ConfigVariantProductModel;


/**
 * @author sankale
 *
 */
public class CatAvailableInWsValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider
{
	private FieldNameProvider fieldNameProvider;

	private static final Logger LOG = Logger.getLogger(CatAvailableInWsValueProvider.class);

	@Resource
	private StockService stockService;

	@Resource
	private DefaultWarehouseDao warehouseDao;

	protected FieldNameProvider getFieldNameProvider()
	{
		return fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}


	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{

		if (model instanceof ConfigVariantProductModel)
		{
			final ConfigVariantProductModel product = (ConfigVariantProductModel) model;

			final Collection<FieldValue> fieldValues = new ArrayList<FieldValue>();

			// Fetch all the warehouses in which current product has stock level present (Quantity can be zero)

			final List<WarehouseModel> warehouses = warehouseDao.getWarehouses(product.getCode());
			if (!warehouses.isEmpty())
			{
				for (final WarehouseModel warehouse : warehouses)
				{
					final String warehouseCode = getWarehouseCode(warehouse);
					fieldValues.addAll(createFieldValue(warehouseCode, indexedProperty));
				}
				return fieldValues;
			}
			else
			{
				LOG.info("CatAvailableInWsValueProvider : Product - " + product.getCode() + "does not exist in any warehouse !");
				return Collections.emptyList();
			}
		}
		else
		{
			throw new FieldValueProviderException(
					"CatAvailableInWsValueProvider : Skipping Entity as model is not of type ConfigVariantModel");
		}
	}

	/**
	 * Get Warehouse Code - MFU, PDC or Dealer Warehouse Code
	 *
	 * @param warehouse
	 * @return String
	 */
	private String getWarehouseCode(final WarehouseModel warehouse)
	{
		String warehouseCode = "";
		if (WarehouseTypeEnum.MFU.equals(warehouse.getWarehouseType()))
		{
			warehouseCode = "MFU";
		}
		else if (WarehouseTypeEnum.PDC.equals(warehouse.getWarehouseType()))
		{
			warehouseCode = "PDC";
		}
		else if (WarehouseTypeEnum.WAREHOUSE.equals(warehouse.getWarehouseType()))
		{
			warehouseCode = warehouse.getCode();
		}
		return warehouseCode;
	}



	protected List<FieldValue> createFieldValue(final String isAvailable, final IndexedProperty indexedProperty)
	{
		final List<FieldValue> fieldValues = new ArrayList<FieldValue>();

		final Collection<String> fieldNames = fieldNameProvider.getFieldNames(indexedProperty, null);
		for (final String fieldName : fieldNames)
		{
			fieldValues.add(new FieldValue(fieldName, isAvailable));
		}
		return fieldValues;
	}

}
