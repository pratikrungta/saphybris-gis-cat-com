/**
*
*/
package com.cat.bcp.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;


/**
 * @author bidavda
 *
 */
public class CatCategoryNameValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider
{
	private FieldNameProvider fieldNameProvider;

	private static final Logger LOG = Logger.getLogger(SalesModelNameValueProvider.class);

	protected FieldNameProvider getFieldNameProvider()
	{
		return fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}


	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		if (model instanceof ConfigVariantProductModel || model instanceof BaseVariantProductModel)
		{
			final ProductModel product = (ProductModel) model;

			final Collection<CategoryModel> categories = product.getSupercategories();
			final Collection<FieldValue> fieldValues = new ArrayList<>();
			String categoryName = "";
			for (final CategoryModel category : categories)
			{
				categoryName = category.getName();
				fieldValues.add(createFieldValue(categoryName.toLowerCase(), indexedProperty));
			}

			return fieldValues;
		}
		else
		{
			LOG.error("Cannot get Category name of non-product item");
			throw new FieldValueProviderException("Cannot get Category name of non-product item");
		}
	}

	protected FieldValue createFieldValue(final String categoryName, final IndexedProperty indexedProperty)
	{
		final String fieldName = getFieldNameProvider().getFieldName(indexedProperty, null, FieldNameProvider.FieldType.INDEX);

		return new FieldValue(fieldName, categoryName);
	}


}
