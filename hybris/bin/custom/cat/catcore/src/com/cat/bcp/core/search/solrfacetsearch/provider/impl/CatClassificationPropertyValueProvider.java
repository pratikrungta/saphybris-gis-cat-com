/**
 *
 */
package com.cat.bcp.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.catalog.jalo.classification.ClassAttributeAssignment;
import de.hybris.platform.catalog.jalo.classification.ClassificationAttributeValue;
import de.hybris.platform.catalog.jalo.classification.util.Feature;
import de.hybris.platform.catalog.jalo.classification.util.FeatureContainer;
import de.hybris.platform.catalog.jalo.classification.util.FeatureValue;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.impl.ClassificationPropertyValueProvider;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;


/**
 * @author sankale
 *
 */
public class CatClassificationPropertyValueProvider extends ClassificationPropertyValueProvider
{

	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		if (model instanceof ProductModel)
		{
			final Product product;
			if (model instanceof ConfigVariantProductModel)
			{
				// Look in the Base product
				final BaseVariantProductModel baseModel = ((BaseVariantProductModel) ((VariantProductModel) model).getBaseProduct());

				// Get Classification Values from sales product
				product = (Product) modelService.getSource(baseModel.getBaseProduct());
			}
			else
			{
				product = (Product) modelService.getSource(model);
			}
			final ClassAttributeAssignmentModel classAttributeAssignmentModel = indexedProperty.getClassAttributeAssignment();
			final ClassAttributeAssignment classAttributeAssignment = modelService.getSource(classAttributeAssignmentModel);

			final FeatureContainer cont = FeatureContainer.loadTyped(product, classAttributeAssignment);
			if (cont.hasFeature(classAttributeAssignment))
			{
				final Feature feature = cont.getFeature(classAttributeAssignment);
				if (feature == null || feature.isEmpty())
				{
					return Collections.emptyList();
				}
				else
				{
					return getFeaturesValues(indexConfig, feature, indexedProperty);
				}
			}
			else
			{
				return Collections.emptyList();
			}
		}
		else
		{
			throw new FieldValueProviderException(
					"CatClassificationPropertyValueProvider : Cannot provide classification property of non-product item");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected List<FieldValue> extractFieldValues(final IndexedProperty indexedProperty, final LanguageModel language,
			final List<FeatureValue> list) throws FieldValueProviderException
	{
		final List<FieldValue> result = new ArrayList();

		for (final FeatureValue featureValue : list)
		{
			Object value = featureValue.getValue();
			if (value instanceof ClassificationAttributeValue)
			{
				value = ((ClassificationAttributeValue) value).getName();
			}
			final List<String> rangeNameList = getRangeNameList(indexedProperty, value);
			final Collection<String> fieldNames = getFieldNameProvider().getFieldNames(indexedProperty,
					language == null ? null : language.getIsocode());
			for (final String fieldName : fieldNames)
			{
				addFieldValuesBasedOnRangeNameList(result, value, rangeNameList, fieldName);
			}
		}
		return result;
	}

	/**
	 * This method is used to add Field Values
	 *
	 * @param result
	 *           List of FieldValue Object
	 * @param value
	 *           value
	 * @param rangeNameList
	 *           rangeNameList
	 * @param fieldName
	 *           fieldName
	 */
	private void addFieldValuesBasedOnRangeNameList(final List<FieldValue> result, final Object value,
			final List<String> rangeNameList, final String fieldName)
	{
		if (rangeNameList.isEmpty())
		{
			addFieldValues(result, fieldName, value);
		}
		else
		{
			for (final String rangeName : rangeNameList)
			{
				addFieldValues(result, fieldName, rangeName == null ? value : rangeName);
			}
		}
	}

	/**
	 * This method is used to add Field Values
	 *
	 * @param result
	 *           List of FieldValue Object
	 * @param fieldName
	 *           field Name
	 * @param value
	 *           value
	 */
	protected void addFieldValues(final List<FieldValue> result, final String fieldName, final Object value)
	{
		if (value instanceof String)
		{
			final String stringValue = (String) value;
			if (fieldName.contains(CatCoreConstants.DEALER_CERTIFIED) && StringUtils.isNotBlank(stringValue))
			{
				result.add(new FieldValue(fieldName, stringValue.toLowerCase()));
			}
			else
			{
				result.add(new FieldValue(fieldName, stringValue));
			}
		}
		else
		{
			result.add(new FieldValue(fieldName, value));
		}
	}
}
