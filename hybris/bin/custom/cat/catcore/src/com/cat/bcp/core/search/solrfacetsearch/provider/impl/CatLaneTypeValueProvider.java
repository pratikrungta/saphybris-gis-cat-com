/**
 *
 */
package com.cat.bcp.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Required;

import com.cat.bcp.core.enums.LaneTypeEnum;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.order.CatReorderService;
import com.cat.bcp.core.servicelayer.CatOrderLaneProductService;


/**
 * The Class CatLaneTypeValueProvider.
 *
 * @author sagdhingra
 */
public class CatLaneTypeValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider
{

	private FieldNameProvider fieldNameProvider;

	@Resource(name = "catOrderLaneProductService")
	private CatOrderLaneProductService catOrderLaneProductService;


	@Resource(name = "catReorderService")
	private CatReorderService catReorderService;



	protected FieldNameProvider getFieldNameProvider()
	{
		return fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	/**
	 * This method is used to index 'catlaneType' property.
	 *
	 * @param indexConfig
	 *           the index config
	 * @param indexedProperty
	 *           the indexed property
	 * @param model
	 *           the model
	 * @return Collection of field value.
	 * @throws FieldValueProviderException
	 *            the field value provider exception
	 */
	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		final Collection<FieldValue> fieldValues = new ArrayList();
		if (model instanceof ConfigVariantProductModel)
		{
			final ConfigVariantProductModel product = (ConfigVariantProductModel) model;

			checkIfConfigIsOrderableForLane(indexedProperty, fieldValues, product, LaneTypeEnum.LANE1.getCode());

			checkIfConfigIsOrderableForLane(indexedProperty, fieldValues, product, LaneTypeEnum.LANE2.getCode());

			checkIfConfigIsOrderableForLane(indexedProperty, fieldValues, product, LaneTypeEnum.LANE3.getCode());

		}
		return fieldValues;
	}


	/**
	 * Check if config is orderable for lane.
	 *
	 * @param indexedProperty
	 *           the indexed property
	 * @param fieldValues
	 *           the field values
	 * @param product
	 *           the product
	 * @param laneType
	 *           the lane type
	 */
	private void checkIfConfigIsOrderableForLane(final IndexedProperty indexedProperty, final Collection<FieldValue> fieldValues,
			final ConfigVariantProductModel product, final String laneType)
	{
		final List<ConfigVariantProductModel> laneProductList = catReorderService.getCompatibleConfigProducts(null, laneType,
				product.getCode());
		if (CollectionUtils.isNotEmpty(laneProductList))
		{
			final Optional<ConfigVariantProductModel> configVar = laneProductList.stream()
					.filter(laneProd -> isProductReorderable(laneProd)).findFirst();
			if (configVar.isPresent())
			{
				fieldValues.addAll(createFieldValue(laneType, indexedProperty));
			}
		}
	}



	/**
	 * This method is used to create Field values.
	 *
	 * @param laneType
	 *           the lane type
	 * @param indexedProperty
	 *           the indexed property
	 * @return List of field values.
	 */
	protected List<FieldValue> createFieldValue(final String laneType, final IndexedProperty indexedProperty)
	{
		final List<FieldValue> fieldValues = new ArrayList();

		final Collection<String> fieldNames = fieldNameProvider.getFieldNames(indexedProperty, null);
		for (final String fieldName : fieldNames)
		{
			fieldValues.add(new FieldValue(fieldName, laneType));
		}
		return fieldValues;
	}

	/**
	 * Checks if is product reorderable.
	 *
	 * @param productModel
	 *           the product model
	 * @return true, if is product reorderable
	 */
	private boolean isProductReorderable(final ProductModel productModel)
	{
		final ConfigVariantProductModel configVariant = (ConfigVariantProductModel) productModel;
		final BaseVariantProductModel baseVariantProductModel = (BaseVariantProductModel) configVariant.getBaseProduct();
		if (BooleanUtils.isFalse(baseVariantProductModel.getIsReorderable()))
		{
			return false;
		}
		final ProductModel salesProductModel = baseVariantProductModel.getBaseProduct();
		if (BooleanUtils.isFalse(salesProductModel.getIsReorderable()))
		{
			return false;
		}
		return true;
	}



}
