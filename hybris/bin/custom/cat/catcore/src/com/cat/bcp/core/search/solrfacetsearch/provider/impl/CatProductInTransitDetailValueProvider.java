/**
 *
 */
package com.cat.bcp.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.service.CatDeliveryInfoService;


/**
 * @author megverma
 *
 */
public class CatProductInTransitDetailValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider
{
	private static final Logger LOG = Logger.getLogger(CatProductInTransitDetailValueProvider.class);

	/** field name provider **/
	private FieldNameProvider fieldNameProvider;

	@Resource(name = "catDeliveryInfoService")
	private CatDeliveryInfoService catDeliveryInfoService;

	protected FieldNameProvider getFieldNameProvider()
	{
		return fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}


	/**
	 * @return the catDeliveryInfoService
	 */
	public CatDeliveryInfoService getCatDeliveryInfoService()
	{
		return catDeliveryInfoService;
	}

	/**
	 * @param catDeliveryInfoService
	 *           the catDeliveryInfoService to set
	 */
	public void setCatDeliveryInfoService(final CatDeliveryInfoService catDeliveryInfoService)
	{
		this.catDeliveryInfoService = catDeliveryInfoService;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.solrfacetsearch.provider.FieldValueProvider#getFieldValues(de.hybris.platform.solrfacetsearch.
	 * config.IndexConfig, de.hybris.platform.solrfacetsearch.config.IndexedProperty, java.lang.Object)
	 */
	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig arg0,
			final de.hybris.platform.solrfacetsearch.config.IndexedProperty indexedProperty, final Object model)
			throws FieldValueProviderException
	{
		if (model instanceof ProductModel)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Start of CatProductInTransitDetailValueProvider");
			}

			final List<FieldValue> fieldValues = new ArrayList();
			final String fieldName = indexedProperty.getName() + "_" + indexedProperty.getType();
			Boolean configurationInTransit;

			if (model instanceof ConfigVariantProductModel)
			{

				configurationInTransit = catDeliveryInfoService.isConfigurationInTransit((ProductModel) model);

				if (configurationInTransit != null)
				{
					fieldValues.add(new FieldValue(fieldName, configurationInTransit));
					logConfigurationInTransitStatus(configurationInTransit);
					return fieldValues;
				}
				else
				{
					return Collections.emptyList();
				}
			}
			else
			{
				return Collections.emptyList();
			}
		}
		else
		{
			throw new FieldValueProviderException(
					"CatProductInTransitDetailValueProvider : Cannot provide classification property of non-product item");
		}

	}

	/**
	 * This method is used to log configuration In Transit status.
	 *
	 * @param configurationInTransit
	 *           configurationInTransit status for given product.
	 */
	private static void logConfigurationInTransitStatus(final Boolean configurationInTransit)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("configurationInTransit : " + configurationInTransit);
		}
	}


}
