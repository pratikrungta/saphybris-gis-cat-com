/**
*
*/
package com.cat.bcp.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.service.CatDeliveryInfoService;


/**
 * @author megverma
 *
 */
public class CatProductInTransitStockValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider
{
	private static final Logger LOG = Logger.getLogger(CatProductInTransitDetailValueProvider.class);

	/** field name provider **/
	private FieldNameProvider fieldNameProvider;

	@Resource(name = "catDeliveryInfoService")
	private CatDeliveryInfoService catDeliveryInfoService;

	protected FieldNameProvider getFieldNameProvider()
	{
		return fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	/**
	 * @return the catDeliveryInfoService
	 */
	public CatDeliveryInfoService getCatDeliveryInfoService()
	{
		return catDeliveryInfoService;
	}

	/**
	 * @param catDeliveryInfoService
	 *           the catDeliveryInfoService to set
	 */
	public void setCatDeliveryInfoService(final CatDeliveryInfoService catDeliveryInfoService)
	{
		this.catDeliveryInfoService = catDeliveryInfoService;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.solrfacetsearch.provider.FieldValueProvider#getFieldValues(de.hybris.platform.solrfacetsearch.
	 * config.IndexConfig, de.hybris.platform.solrfacetsearch.config.IndexedProperty, java.lang.Object)
	 */
	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig arg0,
			final de.hybris.platform.solrfacetsearch.config.IndexedProperty indexedProperty, final Object model)
			throws FieldValueProviderException
	{
		Integer configurationStockInTransit;

		if (model instanceof ProductModel)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Start of CatProductInTransitStockValueProvider");
			}

			final List<FieldValue> fieldValues = new ArrayList<>();
			final String fieldName = indexedProperty.getName() + "_" + indexedProperty.getType();

			if (model instanceof ConfigVariantProductModel)
			{

				configurationStockInTransit = catDeliveryInfoService.getInTransitConfigurationCount((ProductModel) model);

				if (null != configurationStockInTransit)
				{
					fieldValues.add(new FieldValue(fieldName, configurationStockInTransit));
					logconfigurationStockInTransitStatus(configurationStockInTransit);
					return fieldValues;
				}
				else
				{
					return Collections.emptyList();
				}
			}
			else
			{
				return Collections.emptyList();
			}
		}
		else
		{
			throw new FieldValueProviderException(
					"CatProductInTransitStockValueProvider : Cannot provide classification property of non-product item");
		}
	}

	/**
	 * This method is used to log configuration stock In Transit status.
	 *
	 * @param configurationStockInTransit
	 *           configurationStockInTransit status for given product.
	 *
	 */
	private static void logconfigurationStockInTransitStatus(final Integer configurationStockInTransit)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("configurationStockInTransit " + configurationStockInTransit);
		}
	}

}


