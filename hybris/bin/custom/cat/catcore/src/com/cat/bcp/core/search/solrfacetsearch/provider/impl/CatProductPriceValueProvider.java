package com.cat.bcp.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.product.PriceService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.cat.bcp.core.model.ConfigVariantProductModel;


public class CatProductPriceValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider
{
	private FieldNameProvider fieldNameProvider;
	private PriceService priceService;

	public CatProductPriceValueProvider()
	{
		//empty block
	}

	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty, //NOSONAR
			final Object model) throws FieldValueProviderException
	{
		final ArrayList fieldValues = new ArrayList();

		try
		{
			List<String> rangeNameList = null;
			ConfigVariantProductModel configProduct = null;
			if (!(model instanceof ProductModel))
			{
				throw new FieldValueProviderException("Cannot evaluate price of non-product item");
			}
			else
			{
				configProduct = (ConfigVariantProductModel) model;
				String fieldName;
				Iterator var14;
				if (indexConfig.getCurrencies().isEmpty())
				{
					final List<PriceInformation> prices = this.priceService.getPriceInformationsForProduct(configProduct);
					if (prices != null && !prices.isEmpty()) //NOSONAR
					{
						final PriceInformation price = prices.get(0);
						final Double value = price.getPriceValue().getValue();
						rangeNameList = this.getRangeNameList(indexedProperty, value);
						final Collection<String> fieldNames = this.fieldNameProvider.getFieldNames(indexedProperty,
								price.getPriceValue().getCurrencyIso());
						final Iterator var12 = fieldNames.iterator();

						while (true)
						{
							while (var12.hasNext())
							{
								fieldName = (String) var12.next();
								if (rangeNameList.isEmpty())
								{
									fieldValues.add(new FieldValue(fieldName, value));
								}
								else
								{
									var14 = rangeNameList.iterator();

									while (var14.hasNext())
									{
										fieldName = (String) var14.next();
										fieldValues.add(new FieldValue(fieldName, fieldName == null ? value : fieldName));
									}
								}
							}

							return fieldValues;
						}
					}
				}
				else
				{
					final Iterator var23 = indexConfig.getCurrencies().iterator();

					label170: while (var23.hasNext()) //NOSONAR
					{
						final CurrencyModel currency = (CurrencyModel) var23.next();
						final CurrencyModel sessionCurrency = this.i18nService.getCurrentCurrency();

						try
						{
							this.i18nService.setCurrentCurrency(currency);
							final List<PriceInformation> prices = this.priceService.getPriceInformationsForProduct(configProduct);
							if (prices != null && !prices.isEmpty())
							{
								final Double value = (prices.get(0)).getPriceValue().getValue();
								rangeNameList = this.getRangeNameList(indexedProperty, value, currency.getIsocode());
								final Collection<String> fieldNames = this.fieldNameProvider.getFieldNames(indexedProperty,
										currency.getIsocode().toLowerCase());
								var14 = fieldNames.iterator();

								while (true)
								{
									while (true)
									{
										if (!var14.hasNext())
										{
											continue label170;
										}

										fieldName = (String) var14.next();
										if (rangeNameList.isEmpty())
										{
											fieldValues.add(new FieldValue(fieldName, value));
										}
										else
										{
											final Iterator var16 = rangeNameList.iterator();

											while (var16.hasNext())
											{
												final String rangeName = (String) var16.next();
												fieldValues.add(new FieldValue(fieldName, rangeName == null ? value : rangeName));
											}
										}
									}
								}
							}
						}
						finally
						{
							this.i18nService.setCurrentCurrency(sessionCurrency);
						}
					}
				}

				return fieldValues;
			}
		}
		catch (final Exception var21)
		{
			LOG.error(var21);
			throw new FieldValueProviderException(
					"Cannot evaluate " + indexedProperty.getName() + " using " + this.getClass().getName(), var21);
		}
	}

	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}

	public void setPriceService(final PriceService priceService)
	{
		this.priceService = priceService;
	}
}
