/**
 *
 */
package com.cat.bcp.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;


/**
 * @author manjam
 *
 */
public class CatReorderableProductValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider
{
	private FieldNameProvider fieldNameProvider;

	private static final Logger LOG = Logger.getLogger(SalesModelValueProvider.class);

	protected FieldNameProvider getFieldNameProvider()
	{
		return fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}


	@SuppressWarnings("deprecation")
	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		if (model instanceof ConfigVariantProductModel)
		{
			final ConfigVariantProductModel product = (ConfigVariantProductModel) model;
			final BaseVariantProductModel baseModel = (BaseVariantProductModel) product.getBaseProduct();

			final Collection<FieldValue> fieldValues = new ArrayList<>();
			if (BooleanUtils.isFalse(baseModel.getIsReorderable()))
			{
				fieldValues.add(createFieldValue(Boolean.FALSE, indexedProperty));
			}
			else if (baseModel.getBaseProduct() != null && BooleanUtils.isFalse(baseModel.getBaseProduct().getIsReorderable()))
			{

				fieldValues.add(createFieldValue(Boolean.FALSE, indexedProperty));
			}
			else
			{
				fieldValues.add(createFieldValue(Boolean.TRUE, indexedProperty));
			}
			return fieldValues;
		}
		else
		{
			LOG.error("Cannot get Re-orderable status of non-product item");
			throw new FieldValueProviderException("Cannot get Re-orderable status of non-product item");
		}
	}



	protected FieldValue createFieldValue(final Boolean isReorderable, final IndexedProperty indexedProperty)
	{
		final String fieldName = getFieldNameProvider().getFieldName(indexedProperty, null, FieldNameProvider.FieldType.INDEX);

		return new FieldValue(fieldName, isReorderable);
	}

}
