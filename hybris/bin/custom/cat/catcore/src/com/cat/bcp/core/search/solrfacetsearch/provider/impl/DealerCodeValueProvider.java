/**
 *
 */
package com.cat.bcp.core.search.solrfacetsearch.provider.impl;

import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedProperty;
import de.hybris.platform.solrfacetsearch.config.exceptions.FieldValueProviderException;
import de.hybris.platform.solrfacetsearch.provider.FieldNameProvider;
import de.hybris.platform.solrfacetsearch.provider.FieldValue;
import de.hybris.platform.solrfacetsearch.provider.FieldValueProvider;
import de.hybris.platform.solrfacetsearch.provider.impl.AbstractPropertyFieldValueProvider;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.cat.bcp.core.model.BaseVariantProductModel;


/**
 * @author bidavda
 *
 */
public class DealerCodeValueProvider extends AbstractPropertyFieldValueProvider implements FieldValueProvider
{
	private FieldNameProvider fieldNameProvider;

	private static final Logger LOG = Logger.getLogger(DealerCodeValueProvider.class);

	protected FieldNameProvider getFieldNameProvider()
	{
		return fieldNameProvider;
	}

	@Required
	public void setFieldNameProvider(final FieldNameProvider fieldNameProvider)
	{
		this.fieldNameProvider = fieldNameProvider;
	}


	@Override
	public Collection<FieldValue> getFieldValues(final IndexConfig indexConfig, final IndexedProperty indexedProperty,
			final Object model) throws FieldValueProviderException
	{
		if (model instanceof BaseVariantProductModel)
		{
			final BaseVariantProductModel product = (BaseVariantProductModel) model;

			final Collection<FieldValue> fieldValues = new ArrayList<>();

			final String dealerCode = (product.getDealerCode() != null) ? product.getDealerCode().getUid() : "";

			fieldValues.add(createFieldValue(dealerCode, indexedProperty));

			return fieldValues;
		}
		else
		{
			LOG.error("Cannot get dealer code of non-product item");
			throw new FieldValueProviderException("Cannot get dealer code of non-product item");
		}
	}



	protected FieldValue createFieldValue(final String dealerCode, final IndexedProperty indexedProperty)
	{
		final String fieldName = getFieldNameProvider().getFieldName(indexedProperty, null, FieldNameProvider.FieldType.INDEX);

		return new FieldValue(fieldName, dealerCode);
	}

}
