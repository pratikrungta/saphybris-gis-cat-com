/**
 *
 */
package com.cat.bcp.core.service;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.CustomerModel;

import java.util.List;

import com.cat.bcp.core.model.CatCountyModel;


/**
 * The Interface CatCustomerService.
 *
 * @author sagdhingra
 */
public interface CatCustomerService
{

	/**
	 * Gets the customer name suggestions.
	 *
	 * @param name
	 *           the input which will be passed to the dao layer to return result
	 * @return List<String> list of string suggestions
	 */
	List<String> getCustomerNameSuggestions(final String name);

	/**
	 * Gets the customer email suggestions.
	 *
	 * @param email
	 *           the input which will be passed to the dao layer to return result
	 * @return List<String> list of string suggestions
	 */
	List<String> getCustomerEmailSuggestions(String email);

	/**
	 * Fetch existing customer details by email.
	 *
	 * @param email
	 *           the input which will be passed to the dao layer to return result
	 * @return CustomerModel customerModel details
	 */
	CustomerModel fetchExistingCustomerDetailsByEmail(String email);

	/**
	 * Fetch existing customer details by name.
	 *
	 * @param name
	 *           the input which will be passed to the dao layer to return result
	 * @return CustomerModel customerModel details
	 */
	CustomerModel fetchExistingCustomerDetailsByName(String name);


	/**
	 * Find user by customer email.
	 *
	 * @param customerEmail
	 *           the customer email
	 * @return the customer model
	 */
	CustomerModel findUserByCustomerEmail(String customerEmail);


	/**
	 * Method to persist logged in user details and request URL in db
	 *
	 * @param requestURI
	 *           request URI
	 * @param userID
	 *           User ID
	 * @param userType
	 *           User Type
	 * @param userAgent
	 *           User Agent
	 */
	void persistUserDetails(String requestURI, String userID, String userType, String userAgent);

	/**
	 * Removed unauthorized products
	 *
	 * @param customer
	 *           - The customer for which we need to remove unauthorized products from fav lists
	 */
	void removeUnauthorizedFavoriteAndRecentProducts(B2BCustomerModel customer);

	/**
	 * Method to get the county data for dealer based on the sort parameters and order
	 *
	 * @param dealerShipTo
	 *           the branch dealer code
	 * @param dealerCode
	 *           the main dealer code
	 * @param sortOrder
	 *           sort Order i.e ASC or DESC
	 * @param sortParameter
	 *           sort Parameter i.e CountyName,Country or State
	 * @return List<CatCountyModel> list of county model
	 */
	List<CatCountyModel> getCountyDataForDealer(String dealerShipTo, String dealerCode, String sortParameter, String sortOrder);

	/**
	 * Method to unlink a county from a ship to
	 *
	 * @param countyModel
	 *           The model which is to be unlinked
	 */
	void unlinkCounty(CatCountyModel countyModel);

	/**
	 * Method to get the available counties for a ship to
	 *
	 * @param mainDealerCode
	 *           Logged in user's b2bUnit Code
	 * @return List<CatCountyModel>
	 */
	List<CatCountyModel> getAvailableCountyForShipTo(String mainDealerCode);

	/**
	 * Used to concatenate the region and town address data
	 *
	 * @param address
	 *           the shipping address of the user
	 * @return string containing concatenated town and region
	 */
	String getTownRegionConcatenatedAddress(AddressData address);

	/**
	 * Used to concatenate the country iso code and postal code
	 *
	 * @param address
	 *           the shipping address of the user
	 * @return string containing concatenated country iso code and postal code
	 */
	String getIsoCodePostalConcatenatedAddress(AddressData address);

	/**
	 * The method concatenates the addressLine with the required string
	 *
	 * @param addressLine
	 *           the elements of the address
	 * @param concateString
	 *           the string used
	 * @return the concatenated String if addressLine is not empty
	 */
	public String getAddressString(String addressLine, String concateString);
}
