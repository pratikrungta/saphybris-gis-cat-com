/**
 *
 */
package com.cat.bcp.core.service;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import com.cat.bcp.core.model.DeliveryInfoModel;


/**
 * Interface for services related to DeliveryInfo
 *
 * @author megverma
 *
 */
public interface CatDeliveryInfoService
{
    /**
     * Method to retrieve the Serial Number, Estimated Delivery Date and In-transit status for an order using MSO number.
     *
     * @param msoNumber
     *           MSO Number
     * @return DeliveryInfoModel : An object of DeliveryInfoModel having matching msoNumber.
     */
     DeliveryInfoModel retrieveDeliveryInfoByMSO(String msoNumber);

    /**
     * Method to get the Delivery Info for a Product configuration.
     *
     * @param product
     *           Product Model
     * @return List<DeliveryInfoModel> : A list of DeliveryInfoModel objects matching the product configuration.
     */
     List<DeliveryInfoModel> getProductDeliveryInfos(ProductModel product);

    /**
     * Method to check if there are any product configurations in transit for the Product Model.
     *
     * @param product
     *           Product Model
     * @return Boolean : A Boolean to indicate if there are any product configurations in transit for the Product Model.
     */
     Boolean isConfigurationInTransit(ProductModel product);

    /**
     * Method to get count of product configurations in transit for the Product Model.
     *
     * @param product
     *           Product Model
     * @return Integer : An Integer to get count of product configurations in transit for the Product Model.
     */
     Integer getInTransitConfigurationCount(ProductModel product);


}
