/**
 *
 */
package com.cat.bcp.core.service.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.cat.bcp.core.dao.CatCustomerDao;
import com.cat.bcp.core.model.CatCountyModel;
import com.cat.bcp.core.model.CatUserDetailsModel;
import com.cat.bcp.core.service.CatCustomerService;


/**
 * The Class CatCustomerServiceImpl.
 *
 * @author sagdhingra
 */
public class CatCustomerServiceImpl implements CatCustomerService
{

	@Resource(name = "modelService")
	private ModelService modelService;
	private CatCustomerDao catCustomerDao;

	@Resource(name = "userService")
	private UserService userService;

	private static final String UNDERSCO = "_";
	private static final String DATE_FORMAT = "yyyy.MM.dd.HH.mm.ss";


	/**
	 * @return the catCustomerDao
	 */
	public CatCustomerDao getCatCustomerDao()
	{
		return catCustomerDao;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @param catCustomerDao
	 *           the catCustomerDao to set
	 */
	public void setCatCustomerDao(final CatCustomerDao catCustomerDao)
	{
		this.catCustomerDao = catCustomerDao;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCustomerNameSuggestions(final String name)
	{
		return catCustomerDao.getCustomerNameSuggestions(name);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCustomerEmailSuggestions(final String email)
	{
		return catCustomerDao.getCustomerEmailSuggestions(email);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public CustomerModel fetchExistingCustomerDetailsByEmail(final String email)
	{
		return catCustomerDao.fetchExistingCustomerDetailsByEmail(email);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public CustomerModel fetchExistingCustomerDetailsByName(final String name)
	{
		return catCustomerDao.fetchExistingCustomerDetailsByName(name);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CustomerModel findUserByCustomerEmail(final String customerEmail)
	{
		return catCustomerDao.findUserByCustomerEmail(customerEmail);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void persistUserDetails(final String requestURI, final String userID, final String userType, final String userAgent)
	{
		final CatUserDetailsModel catUserDetails = modelService.create(CatUserDetailsModel.class);
		catUserDetails.setRequestURI(requestURI);
		final String timeStamp = new SimpleDateFormat(DATE_FORMAT).format(new Date());
		final String userIdentifier = userID + UNDERSCO + String.valueOf(timeStamp);
		catUserDetails.setUserID(userIdentifier);
		catUserDetails.setUserAgent(userAgent);
		modelService.save(catUserDetails);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeUnauthorizedFavoriteAndRecentProducts(final B2BCustomerModel customer)
	{
		final Set<ProductModel> favProdList = customer.getFavoriteProducts();
		final Collection<ProductModel> recProdList = customer.getRecentlyViewedProduct();
		final Set<ProductModel> favList = new HashSet();
		final List<ProductModel> recList = new ArrayList();
		Iterator<ProductModel> iterator = favProdList.iterator();
		while (iterator.hasNext())
		{
			final ProductModel product = iterator.next();
			for (final CategoryModel category : product.getSupercategories())
			{
				if (!(Boolean.TRUE).equals(category.getGatewayCategory()))
				{
					favList.add(product);
				}
			}

		}


		iterator = recProdList.iterator();
		while (iterator.hasNext())
		{
			final ProductModel product = iterator.next();
			for (final CategoryModel category : product.getSupercategories())
			{
				if (!(Boolean.TRUE).equals(category.getGatewayCategory()))
				{
					recList.add(product);
				}
			}

		}

		customer.setFavoriteProducts(favList);
		customer.setRecentlyViewedProduct(recList);


		modelService.save(customer);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CatCountyModel> getCountyDataForDealer(final String dealerShipTo, final String dealerCode,
			final String sortParameter, final String sortCode)
	{
		return catCustomerDao.getCountyDataForDealer(dealerShipTo, dealerCode, sortParameter, sortCode);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void unlinkCounty(final CatCountyModel countyModel)
	{
		countyModel.setIsMapped(Boolean.FALSE);
		modelService.save(countyModel);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CatCountyModel> getAvailableCountyForShipTo(final String mainDealerCode)
	{
		return catCustomerDao.getAvailableCountyForShipTo(mainDealerCode);
	}


	/**
	 * Used to concatenate the region and town address data
	 *
	 * @param address
	 *           the shipping address of the user
	 * @return string containing concatenated town and region
	 */
	@Override
	public String getTownRegionConcatenatedAddress(final AddressData address)
	{
		final String town = address.getTown();
		final String regionName = (null != address.getRegion()) ? getAddressString(address.getRegion().getName(), ", ")
				: StringUtils.EMPTY;
		final boolean isRegionNotEmpty = StringUtils.isNotBlank(regionName);
		final boolean isTownNotEmpty = StringUtils.isNotBlank(town);
		if (isRegionNotEmpty && isTownNotEmpty)
		{
			return getAddressString(town, " ").concat(regionName);
		}
		else if (isRegionNotEmpty)
		{
			return getAddressString(regionName, StringUtils.EMPTY);
		}
		else if (isTownNotEmpty)
		{
			return getAddressString(town, ", ");
		}
		else
		{
			return StringUtils.EMPTY;
		}
	}

	/**
	 * Used to concatenate the country iso code and postal code
	 *
	 * @param address
	 *           the shipping address of the user
	 * @return string containing concatenated country iso code and postal code
	 */
	@Override
	public String getIsoCodePostalConcatenatedAddress(final AddressData address)
	{
		final String postalCode = address.getPostalCode();
		final String countryIsoCode = (null != address.getCountry()) ? getAddressString(address.getCountry().getIsocode(), " ")
				: StringUtils.EMPTY;
		final boolean isPostalCodeNotEmpty = StringUtils.isNotBlank(postalCode);
		final boolean isCountryIsoCodeNotEmpty = StringUtils.isNotBlank(countryIsoCode);
		if (isCountryIsoCodeNotEmpty && isPostalCodeNotEmpty)
		{
			return getAddressString(countryIsoCode, StringUtils.EMPTY).concat(postalCode);
		}
		else if (isPostalCodeNotEmpty)
		{
			return getAddressString(postalCode, StringUtils.EMPTY);
		}
		else if (isCountryIsoCodeNotEmpty)
		{
			return getAddressString(countryIsoCode, StringUtils.EMPTY);
		}
		else
		{
			return StringUtils.EMPTY;
		}
	}

	/**
	 * used to return the address line elements after suffixing
	 *
	 * @param addressLine
	 *           one of the elements of the address
	 * @param concatString
	 *           the string to be suffixed
	 * @return the address line with the concatenated string if the address line is not empty
	 */
	@Override
	public String getAddressString(final String addressLine, final String concatString)
	{
		return StringUtils.isNotBlank(addressLine) ? addressLine.concat(concatString) : StringUtils.EMPTY;


	}

}
