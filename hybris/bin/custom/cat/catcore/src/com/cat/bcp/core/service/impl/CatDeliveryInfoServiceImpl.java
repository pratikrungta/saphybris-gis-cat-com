/**
 *
 */
package com.cat.bcp.core.service.impl;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import javax.annotation.Resource;

import com.cat.bcp.core.dao.CatDeliveryInfoDao;
import com.cat.bcp.core.model.DeliveryInfoModel;
import com.cat.bcp.core.service.CatDeliveryInfoService;


/**
 * Class to perform Delivery Info related functionalities for Caterpillar.
 *
 * @author megverma
 *
 */
public class CatDeliveryInfoServiceImpl implements CatDeliveryInfoService
{
	@Resource(name = "catDeliveryInfoDao")
	private CatDeliveryInfoDao catDeliveryInfoDao;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public DeliveryInfoModel retrieveDeliveryInfoByMSO(final String msoNumber)
	{
		return catDeliveryInfoDao.getDeliveryInfoByMSO(msoNumber);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<DeliveryInfoModel> getProductDeliveryInfos(final ProductModel product)
	{
		return catDeliveryInfoDao.getProductDeliveryInfo(product);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Boolean isConfigurationInTransit(final ProductModel product)
	{

		return catDeliveryInfoDao.isConfigurationInTransit(product);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Integer getInTransitConfigurationCount(final ProductModel product)
	{
		return catDeliveryInfoDao.getInTransitConfigurationCount(product);
	}



}

