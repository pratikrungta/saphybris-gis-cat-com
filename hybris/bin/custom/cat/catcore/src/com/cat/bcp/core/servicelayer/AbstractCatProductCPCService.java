/**
 *
 */
package com.cat.bcp.core.servicelayer;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.jalo.AbortCronJobException;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.util.Config;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.CatProductCPCDao;
import com.cat.bcp.core.job.CatAbstractAbortableCronJob;


/**
 * This Abstract class is for CPC service.
 * 
 * @author ravjonnalagadda
 *
 */
public abstract class AbstractCatProductCPCService
{
	private CatProductCPCDao catProductCPCDao;


	/**
	 * @return the catProductCPCDao
	 */
	public CatProductCPCDao getCatProductCPCDao()
	{
		return catProductCPCDao;
	}


	/**
	 * @param catProductCPCDao
	 *           the catProductCPCDao to set
	 */
	public void setCatProductCPCDao(final CatProductCPCDao catProductCPCDao)
	{
		this.catProductCPCDao = catProductCPCDao;
	}

	/**
	 * This Abstract method will help to fetch the cpc product xml
	 *
	 * @param catAbortableCronJob
	 * @param cronJobModel
	 */


	protected Map<ProductModel, Document> fetchCPCProductXML(final CatAbstractAbortableCronJob catAbortableCronJob,
			final CronJobModel cronJobModel) throws AbortCronJobException, ParserConfigurationException, SAXException, IOException
	{
		final Map<ProductModel, Document> productXml = new HashMap<>();
		final List<ProductModel> productModelList = getCatProductCPCDao().getProductModelList();
		for (final ProductModel productModel : productModelList)
		{
			catAbortableCronJob.checkIfRequestedToAbortCron(cronJobModel);
			if (null != productModel && StringUtils.isNotBlank(productModel.getCpcGroupId())
					&& StringUtils.isNotBlank(productModel.getCpcProductId()))
			{
				final DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
				f.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
				final StringBuilder str = new StringBuilder(Config.getParameter(CatCoreConstants.CAT_PRODUCT_MEDIA_XML_URL))
						.append(productModel.getCpcGroupId())
						.append(Config.getParameter(CatCoreConstants.CAT_PRODUCT_MEDIA_XML_SEPERATOR))
						.append(productModel.getCpcProductId())
						.append(Config.getParameter(CatCoreConstants.CAT_PRODUCT_MEDIA_XML_EXTENSION));
				final DocumentBuilder db = f.newDocumentBuilder();
				final Document doc = db.parse(new URL(str.toString()).openStream());
				productXml.put(productModel, doc);
			}
		}
		return productXml;
	}

}
