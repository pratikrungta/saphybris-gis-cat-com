/**
 *
 */
package com.cat.bcp.core.servicelayer;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.B2BUnitService;

import java.util.List;


/**
 * @author manjam
 *
 */
public interface CatB2BUnitService extends B2BUnitService<B2BUnitModel, B2BCustomerModel>
{

	/**
	 * Fetches ShipsTo B2Bunits from sales area.
	 *
	 * @param customerModel
	 *           B2BCustomerModel customerModel
	 * @return List<B2BUnitModel>
	 */
	List<B2BUnitModel> getShipsToUnitsForUid(final B2BCustomerModel customerModel);
}
