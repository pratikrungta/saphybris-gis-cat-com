package com.cat.bcp.core.servicelayer;

import de.hybris.platform.util.CSVWriter;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * The class contains the method to create the CSV writer corresponding to a path and then write the appropriate data in the files.
 */

public interface CatCPCFileWriterService {

    /**
     * The method is used to split the header array and returns a map containing the cell index and column header
     * @param headerArray  the header array of the csv file
     * @return the map where
     *          key is the column index
     *          value is the Column header for the key
     */
    Map<Integer, String> getCsvData(String headerArray);


    /**
     * The method used to write the data present in the {@param valueMap } to the csv file.
     * @param valueMap the map containing the key value pair to be written to CSV file
     * @param csvWriter the writer to write the data in the file
     * @param header the header corresponding to which the data will be written
     * @throws IOException if there ia an error while writing the data
     */
    void createCSVFile(Map<String, String> valueMap, CSVWriter csvWriter, String header) throws IOException;


    /**
     * The method is used to write the classificationAttributeAssignment data to the CSV file present in the {@param classAttributeAssignmentMap}
     * @param classAttributeAssignmentMap the map containing the the key value pair to be written to CSV file
     * @param csvClassificationAttributeAssignmentWriter the writer to write the data in the file
     * @param classificationAttributeAssignmentHeader the header corresponding to which the data will be written
     * @throws IOException if there ia an error while writing the data
     */
    void createClassificationAttributeAssignmentFile(Map<String, String> classAttributeAssignmentMap, CSVWriter csvClassificationAttributeAssignmentWriter, String classificationAttributeAssignmentHeader)throws IOException;

    /**
     * The method is used to write the value corresponding to the classificationAttribute against the product model from the {@param productValueMap }
     * @param productValueMap the map containing the the key value pair to be written to CSV file
     * @param productValueMapWriter the writer to write the data in the file
     * @param productValueHeader the header corresponding to which the data will be written
     * @throws IOException IOException if there ia an error while writing the data
     */
    void createClassificationProductCsvFile(Map<String, Map<String, String>> productValueMap, CSVWriter productValueMapWriter, String productValueHeader)throws IOException;


    /**
     * The method is used to write the data to be removed from the database taking it from the {@parm classificationCleanUpDataList}
     * @param classificationCleanUpDataList the list containing the data to be removed
     * @param classificationDataCleanupWriter the writer to write the data in the file
     * @param classificationDataRemovalHeader the header corresponding to which the data will be written
     * @throws IOException  if there ia an error while writing the data
     */
    void createCSVFileForDataCleanup(List<String> classificationCleanUpDataList, CSVWriter classificationDataCleanupWriter, String classificationDataRemovalHeader) throws IOException;

    /**
     * The method creates and returns the CSVWriter
     * @param path  the path at which the file needs to be created
     * @param separator the separator used between the values corresponding to headers
     * @return the CSVWriter
     * @throws IOException if there ia an error while writing the data
     */
     CSVWriter getCsvWriter(String path, char separator) throws IOException ;

    /**
     * The method is used to write the data to be removed from the database taking it from the {@parm classificationCleanUpDataList}
     * @param unclassifiedCleanUpDataList the list containing the data to be removed
     * @param classificationDataCleanupWriter the writer to write the data in the file
     * @param classificationDataRemovalHeader the header corresponding to which the data will be written
     * @throws IOException  if there ia an error while writing the data
     */
    void createUnclassifiedCSVFileForDataCleanup(List<List<String>> unclassifiedCleanUpDataList, CSVWriter classificationDataCleanupWriter, String classificationDataRemovalHeader) throws IOException;


}
