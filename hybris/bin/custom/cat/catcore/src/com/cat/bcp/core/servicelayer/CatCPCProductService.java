package com.cat.bcp.core.servicelayer;

import com.cat.bcp.core.exception.CatCoreException;
import com.cat.bcp.core.job.CatCPCAbstractAbortableCronJob;
import com.cat.bcp.core.model.CpcCronJobModel;
import de.hybris.platform.core.model.product.ProductModel;
import org.w3c.dom.Document;

import java.util.List;
import java.util.Map;

/**
 * The interface contains the method for the CPC integration of utv product specifications.
 */
public interface CatCPCProductService {

    /**
     * The method serves as an starting point  for the CPC integration of the UTV product specifications.
     * @param catAbstractAbortableCronJob the abortable cron job
     * @param cpcCronJobModel the cron job model
     *@throws CatCoreException if there is problem during parsing,configuration or I/O
     */
    void productSpecificationService(CatCPCAbstractAbortableCronJob catAbstractAbortableCronJob, CpcCronJobModel cpcCronJobModel)
            throws CatCoreException;

    /**
     * The method forms a meaning CPC xml url by combining the
     * cpcGroupId and CpcProductID of the product and parses it.
     * @param productModel the product model
     * @return the document to be read
     * @throws CatCoreException if there is problem during parsing,configuration or I/O
     *
     */

    Document getDocumentAfterFormingUrl(ProductModel productModel)  throws CatCoreException;


    /**
     * The method compares the data after parsing the xml with the existing data and returns a list of object
     * which are not found in the classificationDataMap to be removed from the database.
     * @param existingClassificationDataList the existing classification data to be compared
     * @param classificationDataMap the classificationClassMap the map containing the data from the parsed xml of the product
     * @return List of object which needs to be removed
     */
     List<String> getClassficationDataToBeCleaned(List<String> existingClassificationDataList, Map<String, String> classificationDataMap);

}
