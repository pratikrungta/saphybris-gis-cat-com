package com.cat.bcp.core.servicelayer;

import java.io.IOException;
import java.util.List;


/**
 * The Interface CatCSVFileService.
 *
 * @author ravjonnalagadda
 *
 *         This method is to write csv file.
 */
public interface CatCSVFileService
{
	/**
	 * This method is to write csv file.
	 *
	 * @param productList
	 *           Product List is to create csv.
	 * @param cpcType
	 *           this has product,media or facedata value.
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 *            
	 */
	void csvService(List<String> productList, String cpcType) throws IOException;
}
