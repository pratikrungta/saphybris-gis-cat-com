/**
 *
 */
package com.cat.bcp.core.servicelayer;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.CartService;

import java.util.List;
import java.util.Set;

import com.cat.bcp.core.model.CatAdditionalInfoModel;
import com.cat.facades.order.PurchaseOrderCheck;


/**
 * @author sparkibanda This interface provides methods to validate PO numbers and modify/remove cart
 */
public interface CatCartService extends CartService
{
	/**
	 * This method is to call the DAO layer for checking the PO number against customer.
	 *
	 * @param purchaseOrderNumberList
	 *           Purchase Order Number List
	 * @param customerModel
	 *           Customer Model
	 * @return PurchaseOrderCheck object
	 */
	PurchaseOrderCheck doPOListCheck(List<String> purchaseOrderNumberList, B2BCustomerModel customerModel);

	/**
	 * This method is to clear cart entries from cart.
	 */
	void clearCart();

	/**
	 * This method is to remove session cart and any other carts passed as the parameter to this method from hybris
	 * system.
	 *
	 * @param cartList
	 *           List of carts to be removed
	 */
	void removeCarts(final List<CartModel> cartList);

	/**
	 * This method is used to fetch Cat Additional Info(Used for EPP/CSA drop-down options)
	 *
	 * @param type
	 *           Additional Info Type(EPP or CSA)
	 * @return List of CatAdditionalInfoModel
	 */
	List<CatAdditionalInfoModel> getCatAdditionalInfo(String type);

	/**
	 * This method is used to update Cart Entry PO Numbers.
	 *
	 * @param orderEntryModel
	 *           OrderEntry Model
	 * @param orderEntryPOList
	 *           Order Entry PO List
	 * @param isUtv
	 *           - the utv flag
	 * @param isTruckload
	 *           - the truckload flag
	 */
	void updateCartEntryPONumbers(final AbstractOrderEntryModel orderEntryModel, final Set<String> orderEntryPOList, boolean isUtv,
			boolean isTruckload);



	/**
	 * This method is used to update the cart for configurable product**
	 *
	 * @param productCode
	 *           - product code*
	 *
	 * @param qty
	 *           - quantity
	 * @param entryNumber
	 *           - entry number
	 *
	 */
	void updateCartForConfigurableProducts(String productCode, long qty, Integer entryNumber);


	/**
	 * Gets the config variant entries in current cart.
	 *
	 * @param cartModel
	 *           the cart model
	 * @return the config variant entries in current cart
	 */
	List<AbstractOrderEntryModel> getConfigVariantEntriesInCurrentCart(CartModel cartModel);

}
