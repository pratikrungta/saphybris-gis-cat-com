/**
 *
 */
package com.cat.bcp.core.servicelayer;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;

import java.util.List;


/**
 * @author manjam
 *
 */
public interface CatCategoryService extends CategoryService
{
	/**
	 * The method provides Low Stock Products Categories
	 *
	 * @param b2bUnitModel
	 *           the b2bUnitModel is to fetch LowStockProductsCategories from CatCategoryDao
	 * @return LowStockProducts return list of lowStockProducts from CatCategoryDAO.
	 */
	List<CategoryModel> getLowStockProductsCategories(final B2BUnitModel b2bUnitModel);
}