/**
 *
 */
package com.cat.bcp.core.servicelayer;

import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.catalog.enums.ProductInfoStatus;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.ProductConfigurationHandler;
import de.hybris.platform.commerceservices.order.hook.impl.ConfigurableProductAddToCartMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.product.model.AbstractConfiguratorSettingModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;
import de.hybris.platform.textfieldconfiguratortemplateservices.model.TextFieldConfiguratorSettingModel;
import de.hybris.platform.textfieldconfiguratortemplateservices.model.TextFieldConfiguredProductInfoModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections.CollectionUtils;

import com.cat.bcp.core.constants.CatCoreConstants;


/**
 * This class is used to add Additional Configuration info to the cart.
 *
 * @author asomjal
 *
 */
public class CatConfigurableProductAddToCartMethodHook extends ConfigurableProductAddToCartMethodHook
{


	String[] productOptions =
	{ CatCoreConstants.EPPOPTION, CatCoreConstants.CSAOPTION, CatCoreConstants.CSAHOURS, CatCoreConstants.CSAYEAR };

	/**
	 * This method is used to add additional Info to Cart(EPP and CSA Products).
	 *
	 * @param parameters
	 *           parameters which is to be added against the entry
	 * @param result
	 *           cart modification data
	 * @throws CommerceCartModificationException
	 */
	public void catAfterAddToCart(final CommerceCartParameter parameters, final CommerceCartModification result)
			throws CommerceCartModificationException
	{
		ServicesUtil.validateParameterNotNullStandardMessage("parameters", parameters);
		ServicesUtil.validateParameterNotNullStandardMessage("result", result);

		final AbstractOrderEntryModel entry = result.getEntry();
		if (null != entry && (entry.getProduct().getCode().equals(CatCoreConstants.EPPPRODUCT)
				|| entry.getProduct().getCode().equals(CatCoreConstants.CSAPRODUCT)))
		{
			removeAdditionalProductInfoFromEntry(entry, parameters);
			getConfiguratorSettingsService().getConfiguratorSettingsForProduct(parameters.getProduct())
					.forEach(config -> catCreateProductInfo(config, entry, parameters));
		}
	}

	/**
	 * This method is used to create Product Info.
	 *
	 * @param configuration
	 *           configuration which is to be added against an entry
	 * @param entry
	 *           entry model in case of an update
	 * @param parameters
	 */
	protected void catCreateProductInfo(final AbstractConfiguratorSettingModel configuration, final AbstractOrderEntryModel entry,
			final CommerceCartParameter parameters)
	{
		entry.setBasePrice(parameters.getBasePrice());
		final ProductConfigurationHandler productConfigurationHandler = getConfigurationFactory()
				.handlerOf(configuration.getConfiguratorType());
		if (productConfigurationHandler == null)
		{
			throw new IllegalStateException(
					"No ProductConfigurationHandler registered for configurator type " + configuration.getConfiguratorType());
		}
		final List<AbstractOrderEntryProductInfoModel> infos = createProductInfoConfigurations(configuration, parameters);
		entry.setProductInfos(Stream
				.concat(entry.getProductInfos() == null ? Stream.empty() : entry.getProductInfos().stream(),
						infos.stream().peek(item -> item.setOrderEntry(entry)).peek(getModelService()::save))
				.collect(Collectors.toList()));
		getModelService().save(entry);
	}

	/**
	 * This method is used to remove EPP Product Info From Cart Entry if already Present as we will be having only one EPP
	 * and CSA products per Configurations in the cart.
	 *
	 * @param entry
	 *           entry model
	 * @param parameters
	 *           parameters to be added/removed
	 */
	private void removeAdditionalProductInfoFromEntry(final AbstractOrderEntryModel entry, final CommerceCartParameter parameters)
	{
		final List<AbstractOrderEntryProductInfoModel> abstractOrderEntryProductInfoModelList = entry.getProductInfos();
		if (CollectionUtils.isNotEmpty(abstractOrderEntryProductInfoModelList))
		{
			final List<AbstractOrderEntryProductInfoModel> eppOrCSAProductInfoList = new ArrayList();
			for (final AbstractOrderEntryProductInfoModel abstractOrderEntryProductInfoModel : abstractOrderEntryProductInfoModelList)
			{
				final TextFieldConfiguredProductInfoModel textFieldConfiguredProductInfoModel = (TextFieldConfiguredProductInfoModel) abstractOrderEntryProductInfoModel;
				removeConfigurationsFromEntry(eppOrCSAProductInfoList, textFieldConfiguredProductInfoModel);
			}
			getModelService().removeAll(eppOrCSAProductInfoList);
			getModelService().save(entry);
			getModelService().save(parameters.getCart());
		}
	}

	/**
	 * This method prepares a list of configurations which are to be removed
	 *
	 * @param eppOrCSAProductInfoList
	 *           list of configurations
	 * @param textFieldConfiguredProductInfoModel
	 *           a textFieldConfiguredProductInfoModel
	 */
	private void removeConfigurationsFromEntry(final List<AbstractOrderEntryProductInfoModel> eppOrCSAProductInfoList,
			final TextFieldConfiguredProductInfoModel textFieldConfiguredProductInfoModel)
	{
		for (final String productOption : productOptions)
		{
			if (textFieldConfiguredProductInfoModel.getConfigurationLabel().equalsIgnoreCase(productOption))
			{
				eppOrCSAProductInfoList.add(textFieldConfiguredProductInfoModel);
			}
		}
	}

	/**
	 * This method is used to add EPP/CSA Options entered by SP user.
	 *
	 * @param productSettings
	 *           settings which are to be checked
	 * @param parameters
	 *           to set the values of additional options
	 * @return List<AbstractOrderEntryProductInfoModel>
	 */
	public List<AbstractOrderEntryProductInfoModel> createProductInfoConfigurations(
			final AbstractConfiguratorSettingModel productSettings, final CommerceCartParameter parameters)
	{
		if (productSettings instanceof TextFieldConfiguratorSettingModel)
		{
			final TextFieldConfiguratorSettingModel textSetting = (TextFieldConfiguratorSettingModel) productSettings;

			final TextFieldConfiguredProductInfoModel result = new TextFieldConfiguredProductInfoModel();
			result.setConfiguratorType(ConfiguratorType.TEXTFIELD);
			result.setConfigurationLabel(textSetting.getTextFieldLabel());
			if (textSetting.getTextFieldLabel() != null)
			{
				if (textSetting.getTextFieldLabel().equals(CatCoreConstants.EPPOPTION))
				{
					result.setConfigurationValue(parameters.getEppOption());
				}
				else if (textSetting.getTextFieldLabel().equals(CatCoreConstants.CSAOPTION))
				{
					result.setConfigurationValue(parameters.getCsaOption());
				}
				else if (textSetting.getTextFieldLabel().equals(CatCoreConstants.CSAHOURS))
				{
					result.setConfigurationValue(String.valueOf(parameters.getCsaHours()));
				}
				else if (textSetting.getTextFieldLabel().equals(CatCoreConstants.CSAYEAR))
				{
					result.setConfigurationValue(String.valueOf(parameters.getCsaYear()));
				}
			}
			validate(result);
			return Collections.singletonList(result);
		}
		else
		{
			throw new IllegalArgumentException("Argument must be a type of TextFieldConfiguratorSettingsModel");
		}
	}

	/**
	 * This method is used to validate product info.
	 *
	 * @param item
	 *           the entry against which product configurations are saved
	 */
	protected void validate(final TextFieldConfiguredProductInfoModel item)
	{
		final String value = item.getConfigurationValue();
		if (value == null || value.isEmpty())
		{
			item.setProductInfoStatus(ProductInfoStatus.ERROR);
		}
		else
		{
			item.setProductInfoStatus(ProductInfoStatus.SUCCESS);
		}
	}
}
