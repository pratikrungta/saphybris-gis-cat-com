package com.cat.bcp.core.servicelayer;

import com.cat.bcp.core.servicelayer.impl.DefaultCatCreateCSVWriterService;

import java.io.IOException;

/**
 * The method contains the implementation to get the class corresponding to csv writer service
 */
public interface CatCreateCSVWriterService {

    /**
     *
     * @return  DefaultCatCreateCSVWriterService the catCreateCSVWriterService
     * @throws IOException id there is an error during I/O operation
     */
    DefaultCatCreateCSVWriterService createCSVWriter() throws IOException;
}
