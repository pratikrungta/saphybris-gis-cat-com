/**
 *
 */
package com.cat.bcp.core.servicelayer;



import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.order.impl.DefaultCalculationService;
import de.hybris.platform.order.strategies.calculation.OrderRequiresCalculationStrategy;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.PriceValue;
import de.hybris.platform.util.TaxValue;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.cat.bcp.core.constants.CatCoreConstants;


/**
 * This class is used to calculate discount applied for quotes.
 *
 * @author asomjal
 *
 *
 *
 */
public class CatDefaultCalculationService extends DefaultCalculationService
{

	@Resource(name = "orderRequiresCalculationStrategy")
	private OrderRequiresCalculationStrategy orderRequiresCalculationStrategy;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;


	/**
	 * This method is used to calculate totals.
	 *
	 * @param entry
	 *           abstract order entry model on which calculation is to be done
	 * @param recalculate
	 *           boolean flag which tells if we have to recalculate or not
	 */
	@Override
	public void calculateTotals(final AbstractOrderEntryModel entry, final boolean recalculate)
	{
		if (recalculate || orderRequiresCalculationStrategy.requiresCalculation(entry))
		{
			final AbstractOrderModel order = entry.getOrder();
			final CurrencyModel curr = order.getCurrency();
			final int digits = curr.getDigits().intValue();

			final double basePrice = calculateBasePrice(entry, order);

			final double totalPriceWithoutDiscount = commonI18NService
					.roundCurrency(entry.getBasePrice().doubleValue() * entry.getQuantity().longValue(), digits);
			final double quantity = entry.getQuantity().doubleValue();
			/*
			 * apply discounts (will be rounded each) convert absolute discount values in case their currency doesn't match
			 * the order currency
			 */
			final double totalPriceOfAllEntries = commonI18NService.roundCurrency(basePrice * entry.getQuantity().longValue(),
					digits);
			final List appliedDiscounts = DiscountValue.apply(quantity, totalPriceOfAllEntries, digits,
					convertDiscountValues(order, entry.getDiscountValues()), curr.getIsocode());
			entry.setDiscountValues(appliedDiscounts);
			double totalPrice = totalPriceWithoutDiscount;
			for (final Iterator it = appliedDiscounts.iterator(); it.hasNext();)
			{
				totalPrice -= ((DiscountValue) it.next()).getAppliedValue();
			}
			// set total price
			entry.setTotalPrice(Double.valueOf(totalPrice));
			// apply tax values too
			calculateTotalTaxValues(entry);
			setCalculatedStatus(entry);
			getModelService().save(entry);

		}
	}


	/**
	 * This method is used to Calculate Base Price.
	 *
	 * @param entry
	 *           entry model for which base price is to be called
	 * @param order
	 *           the order model from which entries needs to be calculated.
	 * @return double Base Price
	 */
	private double calculateBasePrice(final AbstractOrderEntryModel entry, final AbstractOrderModel order)
	{
		BigDecimal basePrice = BigDecimal.valueOf(entry.getBasePrice());
		if (CollectionUtils.isNotEmpty(order.getEntries()))
		{
			for (final AbstractOrderEntryModel abstractOrderEntryModel : order.getEntries())
			{
				if (abstractOrderEntryModel.getMandatoryProduct().booleanValue()
						&& abstractOrderEntryModel.getConfigVariantId().equals(entry.getProduct().getCode()))
				{
					basePrice = basePrice.add(BigDecimal.valueOf(abstractOrderEntryModel.getBasePrice()));
				}
			}
		}
		return basePrice.doubleValue();
	}

	/**
	 * This method is used to recalculate Order Entry. We will not fetch prices for EPP Products from DB by calling price
	 * factory and these prices will be entered by SP user.
	 *
	 * @param entry
	 *           entry model which needs to be calculated.
	 * @param forceRecalculation
	 *           flag which states if we have to recalculate the entry or not
	 */
	@Override
	protected void recalculateOrderEntryIfNeeded(final AbstractOrderEntryModel entry, final boolean forceRecalculation)
			throws CalculationException
	{
		final boolean eppOrCsaProduct = (entry.getProduct().getCode().equals(CatCoreConstants.EPPPRODUCT)
				|| entry.getProduct().getCode().equals(CatCoreConstants.CSAPRODUCT)) ? true : false;

		if (forceRecalculation || orderRequiresCalculationStrategy.requiresCalculation(entry))
		{
			if (!eppOrCsaProduct)
			{
				resetAllValues(entry);
			}
			calculateTotals(entry, true);
		}
	}


	/**
	 * This method is used to reset all the values for an Order Entry.
	 *
	 * @param entry
	 *           entry model which needs to be reset.
	 *
	 */
	@Override
	protected void resetAllValues(final AbstractOrderEntryModel entry) throws CalculationException
	{
		final Collection<TaxValue> entryTaxes = findTaxValues(entry);
		entry.setTaxValues(entryTaxes);
		final PriceValue pv = findBasePrice(entry);
		final AbstractOrderModel order = entry.getOrder();
		final PriceValue basePrice = convertPriceIfNecessary(pv, order.getNet().booleanValue(), order.getCurrency(), entryTaxes);
		entry.setBasePrice(Double.valueOf(basePrice.getValue()));
		entry.setDiscountValues(entry.getDiscountValues());
	}


}