/**
 *
 */
package com.cat.bcp.core.servicelayer;

import de.hybris.platform.commerceservices.order.strategies.impl.DefaultEntryMergeStrategy;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.StringUtils;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.model.PartVariantProductModel;


/**
 * This strategy is used for merge entry while adding Config Variant to Cart.
 *
 * @author asomjal
 *
 */
public class CatDefaultEntryMergeStrategy extends DefaultEntryMergeStrategy
{

	/**
	 * This method is used to get Entry to Merge while adding Config Variant to Cart.
	 *
	 * @param entries
	 *           collection of entries
	 * @param newEntry
	 *           entry to be merged
	 * @return AbstractOrderEntryModel
	 */
	@Override
	public AbstractOrderEntryModel getEntryToMerge(final List<AbstractOrderEntryModel> entries,
			@Nonnull final AbstractOrderEntryModel newEntry)
	{
		ServicesUtil.validateParameterNotNullStandardMessage("newEntry", newEntry);

		if (entries == null)
		{
			return null;
		}
		final List<AbstractOrderEntryModel> listToFindMergeTarget = new ArrayList();
		boolean needToMerge = false;
		if (newEntry.getProduct() != null)
		{
			if (newEntry.getProduct() instanceof PartVariantProductModel && StringUtils.isBlank(newEntry.getConfigVariantId()))
			{
				needToMerge = needToMergeAccessory(entries, newEntry, listToFindMergeTarget);
			}
			else if (newEntry.getProduct() instanceof PartVariantProductModel)
			{
				needToMerge = needToMerge(entries, newEntry, listToFindMergeTarget);
			}
			else if (newEntry.getProduct().getCode().equals(CatCoreConstants.EPPPRODUCT)
					|| newEntry.getProduct().getCode().equals(CatCoreConstants.CSAPRODUCT))
			{
				needToMerge = false;
			}
			else
			{
				needToMerge = true;
				listToFindMergeTarget.addAll(entries);
			}
		}
		if (needToMerge)
		{
			return listToFindMergeTarget.stream().filter(Objects::nonNull).filter(e -> !newEntry.equals(e))
					.filter(entry -> canMerge(entry, newEntry).booleanValue()).sorted(getEntryModelComparator()).findFirst()
					.orElse(null);
		}
		return null;
	}

	/**
	 * This method is used to decide if entry needs to be merged and also prepare list to find merge target.
	 *
	 * @param entries
	 *           collection of entries
	 * @param newEntry
	 *           entry to be merged
	 * @param listToFindMergeTarget
	 *           collection of entries to find merge target
	 * @return boolean Value
	 */
	private boolean needToMergeAccessory(final List<AbstractOrderEntryModel> entries, final AbstractOrderEntryModel newEntry,
			final List<AbstractOrderEntryModel> listToFindMergeTarget)
	{
		boolean needToMerge = false;
		final List<AbstractOrderEntryModel> abstractOrderEntryModelList = new ArrayList();
		abstractOrderEntryModelList.addAll(entries);
		abstractOrderEntryModelList.remove(newEntry);
		for (final AbstractOrderEntryModel abstractOrderEntryModel : abstractOrderEntryModelList)
		{
			if (StringUtils.isNotBlank(abstractOrderEntryModel.getCompatibleModel())
					&& StringUtils.isNotBlank(newEntry.getCompatibleModel())
					&& abstractOrderEntryModel.getCompatibleModel().equalsIgnoreCase(newEntry.getCompatibleModel())
					&& ((abstractOrderEntryModel.getProduct().getCode()).equalsIgnoreCase(newEntry.getProduct().getCode())))
			{
				needToMerge = true;
			}
			prepareFindMergeTargetList(newEntry, listToFindMergeTarget, abstractOrderEntryModel);
		}
		return needToMerge;
	}

	/**
	 * This method is used to decide if entry needs to be merged and also prepare list to find merge target.
	 *
	 * @param entries
	 *           collection of entries
	 * @param newEntry
	 *           entry to be merged
	 * @param listToFindMergeTarget
	 *           collection of entries to find merge target
	 * @return boolean Value
	 */
	private boolean needToMerge(final List<AbstractOrderEntryModel> entries, final AbstractOrderEntryModel newEntry,
			final List<AbstractOrderEntryModel> listToFindMergeTarget)
	{
		boolean needToMerge = false;
		final List<AbstractOrderEntryModel> abstractOrderEntryModelList = new ArrayList();
		abstractOrderEntryModelList.addAll(entries);
		abstractOrderEntryModelList.remove(newEntry);
		for (final AbstractOrderEntryModel abstractOrderEntryModel : abstractOrderEntryModelList)
		{
			if (StringUtils.isNotBlank(abstractOrderEntryModel.getConfigVariantId())
					&& StringUtils.isNotBlank(newEntry.getConfigVariantId())
					&& abstractOrderEntryModel.getConfigVariantId().equalsIgnoreCase(newEntry.getConfigVariantId())
					&& ((abstractOrderEntryModel.getProduct().getCode()).equalsIgnoreCase(newEntry.getProduct().getCode())))
			{
				needToMerge = true;
			}
			prepareFindMergeTargetList(newEntry, listToFindMergeTarget, abstractOrderEntryModel);
		}
		return needToMerge;
	}

	/**
	 * This method is used to prepare merge Target List.
	 *
	 * @param newEntry
	 *           entry to be merged
	 * @param listToFindMergeTarget
	 *           list of merge targets
	 * @param abstractOrderEntryModel
	 */
	private void prepareFindMergeTargetList(final AbstractOrderEntryModel newEntry,
			final List<AbstractOrderEntryModel> listToFindMergeTarget, final AbstractOrderEntryModel abstractOrderEntryModel)
	{
		if (abstractOrderEntryModel.getConfigVariantId().equalsIgnoreCase(newEntry.getConfigVariantId()))
		{
			listToFindMergeTarget.add(abstractOrderEntryModel);
		}
	}

}
