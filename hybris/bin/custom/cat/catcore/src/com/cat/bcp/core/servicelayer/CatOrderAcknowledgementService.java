package com.cat.bcp.core.servicelayer;

import de.hybris.platform.cronjob.model.CronJobModel;

import com.cat.bcp.core.job.CatAbstractAbortableCronJob;


/**
 * This Interface is to send cat order acknowledgement Email
 *
 * @author ravjonnalagadda
 *
 */
public interface CatOrderAcknowledgementService
{
	/**
	 * This Interface is to send order Acknowledgement Email
	 *
	 * @param catAbstractAbortableCronJob
	 *           : abort the cronjob at specific time
	 * @param cronJobModel
	 *           Cronjob Model Object
	 */
	void sendOrderAcknowledgementEmail(CatAbstractAbortableCronJob catAbstractAbortableCronJob, CronJobModel cronJobModel);

}
