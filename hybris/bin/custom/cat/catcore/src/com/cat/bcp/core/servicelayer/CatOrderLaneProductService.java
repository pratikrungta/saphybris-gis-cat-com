/**
 *
 */
package com.cat.bcp.core.servicelayer;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.OrderModel;

import java.text.ParseException;
import java.util.List;

import com.cat.bcp.core.model.ProductLaneInfoModel;


/**
 * The Interface CatOrderLaneProductService.
 *
 * @author bidavda
 */
public interface CatOrderLaneProductService
{

	/**
	 * Gets the lane info for current dealer.
	 *
	 * @param b2bUnit
	 *           the b2b unit
	 * @return the lane info for current dealer
	 */
	List<ProductLaneInfoModel> getLaneInfoForCurrentDealer(B2BUnitModel b2bUnit);


	/**
	 * Gets the lane info for sales model.
	 *
	 * @param salesModelId
	 *           the sales model id
	 * @param b2bUnit
	 *           the b2b unit
	 * @return the lane info for sales model
	 */
	List<ProductLaneInfoModel> getLaneInfoForSalesModel(String salesModelId, B2BUnitModel b2bUnit);


	/**
	 * Gets the lane info for ordered quantity.
	 *
	 * @param salesModelId
	 *           the sales model id
	 * @param b2bUnit
	 *           the b2b unit
	 * @param orderedQty
	 *           the ordered qty
	 * @return the lane info for ordered quantity
	 */
	List<ProductLaneInfoModel> getLaneInfoForOrderedQuantity(String salesModelId, B2BUnitModel b2bUnit, int orderedQty);


	/**
	 * Gets the lane info for lane type.
	 *
	 * @param laneCode
	 *           the lane code
	 * @param salesModelId
	 *           the sales model id
	 * @param b2bUnit
	 *           the b2b unit
	 * @return the lane info for lane type
	 */
	ProductLaneInfoModel getLaneInfoForLaneType(String laneCode, String salesModelId, B2BUnitModel b2bUnit);


	/**
	 * Checks if is product orderable for lane.
	 *
	 * @param laneCode
	 *           the lane code
	 * @param salesModelId
	 *           the sales model id
	 * @param configVariantId
	 *           configVariantId
	 * @param orderingWindowValidation
	 *           the ordering window validation
	 * @param dateValidation
	 *           the date validation
	 * @param compatibilityValidation
	 *           the compatibility validation
	 * @param stockValidation
	 *           the stock validation
	 * @return true, if is product orderable for lane
	 * @throws ParseException
	 *            the parse exception
	 */
	boolean isProductOrderableForLane(String laneCode, String salesModelId, String configVariantId,
			boolean orderingWindowValidation, boolean dateValidation, boolean compatibilityValidation, boolean stockValidation)
			throws ParseException;

	/**
	 * This method is used to update ReOrdered Quantity in ProductLaneInfo Table.
	 *
	 * @param parentOrder
	 *           parent Order
	 */
	void updateReOrderedQuantityForProductLaneInfo(OrderModel parentOrder);
}
