package com.cat.bcp.core.servicelayer;

import de.hybris.platform.cronjob.model.CronJobModel;

import com.cat.bcp.core.exception.CatException;
import com.cat.bcp.core.job.CatAbstractAbortableCronJob;


/**
 * Cat Product CPC Service is to fetch product media .
 * 
 * @author ravjonnalagadda
 *
 */
public interface CatProductCPCMediaService
{
	/**
	 * Product CPC Service.
	 *
	 * @param catAbstractAbortableCronJob
	 *           : abort the cronjob at specific time
	 * @param cronJobModel
	 *           Cronjob Model Object
	 * @throws CatException
	 *            : Custom Exception class handle all the exceptions
	 */
	void productCPCMediaService(CatAbstractAbortableCronJob catAbstractAbortableCronJob, CronJobModel cronJobModel)
			throws CatException; //NOSONAR

}
