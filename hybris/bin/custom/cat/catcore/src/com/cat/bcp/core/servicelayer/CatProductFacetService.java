package com.cat.bcp.core.servicelayer;

import de.hybris.platform.cronjob.model.CronJobModel;

import com.cat.bcp.core.exception.CatException;
import com.cat.bcp.core.job.CatAbstractAbortableCronJob;


/**
 * This Interface is to fetch Product Facet Details
 *
 * @author ravjonnalagadda
 *
 */
public interface CatProductFacetService
{
	/**
	 * Product Facet Service.
	 *
	 * @param catAbstractAbortableCronJob
	 *           : abort the cronjob at specific time
	 * @param cronJobModel
	 *           Cronjob Model Object
	 * @throws CatException
	 *            : Custom Exception class handle all the exceptions
	 */
	void productFacetService(CatAbstractAbortableCronJob catAbstractAbortableCronJob, CronJobModel cronJobModel)
			throws CatException; //NOSONAR

}
