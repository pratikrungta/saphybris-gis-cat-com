/**
 *
 */
package com.cat.bcp.core.servicelayer;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;

import java.util.List;

import com.cat.bcp.core.model.InventoryReservationModel;



/**
 * The Interface CatProductService.
 *
 * @author bidavda
 */
public interface CatProductService extends ProductService
{

	/**
	 * check if product is a UTV product based on product model.
	 *
	 * @param productModel
	 *           Product Model
	 * @return true if its UTV product else return false
	 */
	boolean isUtvProduct(final ProductModel productModel);

	/**
	 * check if product is a UTV product based on product data
	 *
	 * @param productData
	 *           - The product data
	 *
	 * @return true if its UTV product else return false
	 */
	boolean isUtvProduct(ProductData productData);

	/**
	 * check if product is a UTV product based on category.
	 *
	 * @param category
	 *           Category
	 * @return true if its UTV product else return false
	 */
	boolean isUtvProduct(final String category);

	/**
	 * get the product type (new, used, UTV or accessory) based on product model.
	 *
	 * @param product
	 *           Product Model
	 * @return productType
	 */
	String getProductType(ProductModel product);

	/**
	 * Gets the inventory reservation for product.
	 *
	 * @param productModel
	 *           the product model
	 * @param b2bUnitModel
	 *           the b 2 b unit model
	 * @return the inventory reservation for product
	 */
	List<InventoryReservationModel> getInventoryReservationForProduct(final ProductModel productModel,
			final B2BUnitModel b2bUnitModel);

	/**
	 * Method to get the accessory data
	 *
	 * @return List<PartVariantProductModel>
	 */
	List<ProductReferenceModel> getAccessoryData();


	/**
	 * Method to return CUV Models
	 *
	 * @return List<ProductModel>
	 */
	List<ProductModel> getCUVModels();

	/**
	 * Checks if product is accessory product.
	 *
	 * @param product
	 *           the product
	 * @return true, if product is accessory product
	 */
	boolean isAccessoryProduct(ProductData product);

}
