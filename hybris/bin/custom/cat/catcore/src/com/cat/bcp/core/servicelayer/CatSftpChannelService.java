/**
 *
 */
package com.cat.bcp.core.servicelayer;

import java.io.File;
import java.io.IOException;
import java.util.Vector;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;


/**
 * @author amitsinha5
 *
 */
public interface CatSftpChannelService

{
    /**
     * This method connectSessionForSftp() is used to establish FTP session & connection with MFT server.
     *
     * @param host
     *           Host
     * @param userId
     *           UserId
     * @param port
     *           Port
     * @param authPassword
     *           AuthPassword
     * @return Session Object
     * @throws IOException
     *            Input/Output Exception
     * @throws JSchException
     *            JSch Exception
     */
     Session connectSessionForSftp(String host, String userId, final int port, String authPassword)
            throws IOException, JSchException;

    /**
     * This method openChannelType() is used to create open FTP channels after connection to SFTP session.
     *
     * @param ftpType
     *           FTP Type
     * @param session
     *           Object
     * @return Channel Object
     * @throws JSchException
     *            JSch Exception
     */
     Channel openChannelType(final String ftpType, Session session) throws JSchException;

    /**
     * This method changeDirectoryForSftp() is used to change directory or file location at MFT server.
     *
     * @param filePath
     *           File Path
     * @param channel
     *           Channel Object
     * @return ChannelSftp Object
     * @throws IOException
     *            Input/Output Exception
     */
     ChannelSftp changeDirectoryForSftp(String filePath, Channel channel) throws IOException;

    /**
     * This method getLSEntries() is used to get list of Ls entries.
     *
     * @param channelSftp
     *           ChannelSftp Object
     * @param searchPatternType
     *           Search Pattern Type
     * @return Vector
     * @throws IOException
     *            Input/Output Exception
     */
     Vector<LsEntry> getFilePatternEntries(ChannelSftp channelSftp, String searchPatternType) throws IOException; //NOSONAR

    /**
     * This method fileTransferSftpStrategy() is used to fetch file from remote MFT server location.
     *
     * @param channelSftp
     *           channelSftp Object
     * @param bufferByte
     *           buffer Byte
     * @param entry
     *           LsEntry Object
     * @param file
     *           File Object
     * @throws IOException
     *            Input/Output Exception
     * @throws SftpException
     *            Sftp Exception
     */
     void readFilesFromMftServer(ChannelSftp channelSftp, int bufferByte, LsEntry entry, final File file)
            throws IOException, SftpException;

    /**
     * This method storeFileToTargetFolder() create a new file location for local target folder & return the same.
     *
     * @param entry
     *           LsEntry Object
     * @param path
     *           Path
     * @param sitePath
     *           site Path
     * @return File
     * @throws IOException
     *            Input/Output Exception
     */
     File storeFileToTargetFolder(LsEntry entry, final String path, final String sitePath) throws IOException;

    /**
     * This method is used to write file logs.
     *
     * @param entry
     *           LsEntry Object
     * @throws IOException
     *            Input/Output Exception
     */
     void writingFileLogs(LsEntry entry) throws IOException;
}
