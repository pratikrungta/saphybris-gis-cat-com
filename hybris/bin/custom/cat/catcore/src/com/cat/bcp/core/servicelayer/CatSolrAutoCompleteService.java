/**
 *
 */
package com.cat.bcp.core.servicelayer;

import java.util.Collection;
import java.util.Map;

import com.cat.bcp.core.enums.CatIndexedTypeEnum;


/**
 * @author sagdhingra
 *
 */
public interface CatSolrAutoCompleteService
{
	/**
	 * Method to get a map of auto suggestions:
	 *
	 * @param input
	 *           Input Text
	 * @param indexType
	 *           Index Type
	 * @return Map<String, Collection<String>>
	 */
	Map<String, Collection<String>> getAutocompleteSuggestionsMap(final String input, final CatIndexedTypeEnum indexType);
}
