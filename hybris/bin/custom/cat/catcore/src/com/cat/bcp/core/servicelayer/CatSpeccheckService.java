/**
 *
 */
package com.cat.bcp.core.servicelayer;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;

import java.io.IOException;
import java.util.List;

import org.codehaus.jackson.map.JsonMappingException;
import org.springframework.http.ResponseEntity;

import com.cat.bcp.core.exception.CatSpeccheckException;


/**
 * @author asomjal
 *
 */
public interface CatSpeccheckService
{

	/**
	 * This method is used for speccheck integration
	 *
	 * @param requestURI
	 *           Request URI
	 * @return Response from Speccheck APIs.
	 */
	ResponseEntity<Object> callSpeccheckAPI(final String requestURI);

	/**
	 * This method is used to get competitors for given product
	 *
	 * @param requestURI
	 *           request URI
	 * @return JSON Object
	 * @throws CatSpeccheckException
	 *            Cat Speccheck Exception
	 */
	String callSpeccheckAPIForCompetitor(final String requestURI) throws CatSpeccheckException;

	/**
	 * This method returns the sales models to compare
	 *
	 * @param categoryCode
	 *           Category Code
	 * @return List<ProductModel>
	 */
	List<ProductModel> getSalesModelForCompare(String categoryCode);

	/**
	 * This method returns the sales model json to compare
	 *
	 * @param products
	 *           List of PRoducts
	 * @return JSON Object
	 * @throws IOException
	 *            Input/Output Exception
	 * @throws JsonMappingException
	 *            JSON Mapping Exception
	 */
	Object getSalesModelJson(List<ProductModel> products) throws JsonMappingException, IOException;

	/**
	 * This method returns sales models to compare for UTV products
	 *
	 * @param category
	 *           Category Model
	 * @return List<ProductModel>
	 */
	List<ProductModel> getSalesModelForCompare(CategoryModel category);


	/**
	 * This method returns the json data for product specifications of the sales models selected for comapre.
	 *
	 * @param products
	 *           List of ProductData Objects
	 * @param uom
	 *           Unit Of Measurement
	 * @return JSON Object
	 * @throws IOException
	 *            Input/Output Exception
	 * @throws JsonMappingException
	 *            JSON Mapping Exception
	 */
	Object getProductSpecs(List<ProductData> products, String uom) throws JsonMappingException, IOException;


}
