/**
 *
 */
package com.cat.bcp.core.servicelayer;


import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;
import java.util.Map;

import com.cat.bcp.core.model.RestockAlertModel;
import com.cat.bcp.core.model.ShipToAfterDateModel;
import com.cat.facades.order.CatShiptoQuantitiesData;


/**
 * The Interface CatStockService.
 *
 * @author avaijapurkar
 */
public interface CatStockService
{

	/**
	 * This method will update Alert Entry.
	 *
	 * @param alertId
	 *           - The alertId
	 * @param configProduct
	 *           - Config Variant product id
	 * @param orderedQty
	 *           - The ordered qty
	 */
	void updateAlertEntry(final String alertId, final String configProduct, final String orderedQty);

	/**
	 * This method will get Stock For Sales Model.
	 *
	 * @param alertId
	 *           the alert id
	 * @return RestockAlertModel
	 */
	RestockAlertModel getStockForSalesModel(String alertId);

	/**
	 * Method to check if the associated config product exist based on alert id.
	 *
	 * @param salesModelId
	 *           Sales Model Id
	 * @param productModel
	 *           Product Model Object
	 * @param customerModel
	 *           Customer Model Object
	 * @return Boolean
	 */
	Boolean isIMAddToCartConfigProduct(final String salesModelId, final ProductModel productModel,
			final B2BCustomerModel customerModel);

	/**
	 * Gets the ship to date qty.
	 *
	 * @param entries
	 *           the entries
	 * @return the ship to date qty
	 */
	List<CatShiptoQuantitiesData> getShipToDateQty(List<OrderEntryData> entries);

	/**
	 * This method return Order window for product
	 *
	 * @param prodList
	 *           - product list
	 * @return map - order window map
	 */
	Map getOrderWindowAndShipAfterDates(List<ProductModel> prodList);


	/**
	 * Format ship to after date.
	 *
	 * @param shipAfterModel
	 *           the ship after model
	 * @return the formatted ship to after date string builder
	 */
	StringBuilder formatShipToAfterDate(ShipToAfterDateModel shipAfterModel);

}
