/**
 *
 */
package com.cat.bcp.core.servicelayer;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;


/**
 * @author bidavda
 *
 */
public interface DealerAddressService
{
	/**
	 * This method is used to get POS Address By POS ID.
	 *
	 * @param posId
	 *           POS ID
	 * @return AddtessModel
	 */
	public AddressModel getPosAddressByPosId(final String posId);

	public PointOfServiceModel findMFUorPDCByPosId(final String posId);

	public WarehouseModel findWarehouseByDealerCode(final String dealerCode);
}
