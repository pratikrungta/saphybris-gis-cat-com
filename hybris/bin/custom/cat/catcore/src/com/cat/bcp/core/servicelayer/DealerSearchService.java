/**
 *
 */
package com.cat.bcp.core.servicelayer;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import com.cat.bcp.core.model.AlertProductModel;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.RestockAlertModel;


/**
 * @author sankale
 *
 *         This interface defines the custom services required for user defined handlers
 */
public interface DealerSearchService
{
	/**
	 * Method to return Base Models for populating landing page drop down.
	 *
	 * @param productModel
	 *           sales model for which the base model needs to be fetched
	 * @return List<BaseVariantProductModel> base models
	 */
	public List<BaseVariantProductModel> getBaseModels(ProductModel productModel);

	/**
	 * Method to return Sales Models for populating landing page drop down.
	 *
	 * @param categoryCode
	 *           Sales model specific to a category
	 * @param isUsed
	 *           Flag to get results specific to product hierarchy <<<<<<< HEAD
	 * @return List of Sales Models ======= >>>>>>> remotes/origin/R2.6/develop
	 */
	public List<ProductModel> getSalesModels(String categoryCode, boolean isUsed);



	/**
	 * Method to get low stock products
	 *
	 * @param defaultB2BUnit
	 *           Fetch restock alerts model for dealer code
	 * @param salesModelId
	 *           Fetch restock alerts model for sales model id
	 * @param category
	 *           Fetch restock alerts model for category code
	 * @return List<RestockAlertModel> list of restock alerts.
	 */
	public List<RestockAlertModel> getLowStockProducts(B2BUnitModel defaultB2BUnit, String category, String salesModelId);


	/**
	 * Method to get the associated config products based on alert id
	 *
	 * @param alertId
	 *           used to fetch the associated config products
	 * @return List<AlertProductModel> alert products model
	 */
	public List<AlertProductModel> getAssociatedConfigProducts(String alertId);

	/**
	 * Method to return a product based on the name
	 *
	 * @param productName
	 *           name of the product
	 * @return ProductModel
	 */
	public ProductModel getProductByName(String productName);


	/**
	 * Method to fetch the category based on the category name
	 *
	 * @param categoryName
	 *           Category Name
	 * @return CategoryModel
	 */
	public CategoryModel getCategoryByName(String categoryName);

}
