/**
 *
 */
package com.cat.bcp.core.servicelayer.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2b.services.impl.DefaultB2BUnitService;
import de.hybris.platform.commerceservices.i18n.CommerceCommonI18NService;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.cat.bcp.core.dao.CatB2BUnitDao;
import com.cat.bcp.core.servicelayer.CatB2BUnitService;


/**
 * @author manjam
 *
 */
public class CatB2BUnitServiceImpl extends DefaultB2BUnitService implements CatB2BUnitService
{

	private CatB2BUnitDao b2bUnitDao;
	@Autowired
	private CommerceCommonI18NService commerceCommonI18NService;
	@Autowired
	private FlexibleSearchService flexibleSearchService;
	private static final Logger LOG = Logger.getLogger(CatB2BUnitServiceImpl.class.getName());

	/**
	 * @return the flexibleSearchService
	 */
	public FlexibleSearchService getFlexibleSearchService()
	{
		return flexibleSearchService;
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<B2BUnitModel> getShipsToUnitsForUid(final B2BCustomerModel customerModel)
	{
		AddressModel soldToAddress;
		final String uid = customerModel.getDefaultB2BUnit().getUid();
		if (customerModel.getDefaultB2BUnit().getAddresses().iterator().hasNext())
		{
			soldToAddress = customerModel.getDefaultB2BUnit().getAddresses().iterator().next();
			if (LOG.isDebugEnabled())
			{
				LOG.debug(soldToAddress.getCountry().getIsocode());
			}

			return b2bUnitDao.findShipTosB2BUnits(uid, String.valueOf(soldToAddress.getCountry().getIsocode()));
		}
		return new ArrayList();
	}

	/**
	 * @return the commerceCommonI18NService
	 */
	public CommerceCommonI18NService getCommerceCommonI18NService()
	{
		return commerceCommonI18NService;
	}

	/**
	 * @param commerceCommonI18NService
	 *           the commerceCommonI18NService to set
	 */
	public void setCommerceCommonI18NService(final CommerceCommonI18NService commerceCommonI18NService)
	{
		this.commerceCommonI18NService = commerceCommonI18NService;
	}

	/**
	 * @return the b2bUnitDao
	 */
	@Override
	public CatB2BUnitDao getB2bUnitDao()
	{
		return b2bUnitDao;
	}

	/**
	 * @param b2bUnitDao
	 *           the b2bUnitDao to set
	 */
	@Required
	public void setB2bUnitDao(final CatB2BUnitDao b2bUnitDao)
	{
		this.b2bUnitDao = b2bUnitDao;
	}

}
