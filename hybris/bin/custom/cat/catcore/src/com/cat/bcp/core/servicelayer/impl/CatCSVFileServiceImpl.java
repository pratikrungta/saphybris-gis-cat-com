package com.cat.bcp.core.servicelayer.impl;

import de.hybris.platform.util.Config;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.servicelayer.CatCSVFileService;

import au.com.bytecode.opencsv.CSVWriter;



/**
 * @author ravjonnalagadda
 *
 *         This class is to create csv file and write Facet details.
 */
public class CatCSVFileServiceImpl implements CatCSVFileService
{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void csvService(final List<String> productList, final String cpcType) throws IOException
	{
		final boolean flag = checkIfFoldersExists();
		if (flag)
		{
			createCSV(productList, cpcType);
		}
	}

	/**
	 * This method will create csv file in the specific location
	 *
	 * @param productList
	 *           productlist
	 * @param cpcType
	 *           gives csv file is for product or media
	 * @throws IOException
	 *            input output exception
	 */
	private void createCSV(final List<String> productList, final String cpcType) throws IOException
	{
		CSVWriter csvWriter;
		if (cpcType.equalsIgnoreCase(CatCoreConstants.PRODUCT))
		{
			final String header = Config.getParameter(CatCoreConstants.CSV_HEADER_PRODUCT);
			final String filePath = Config.getParameter(CatCoreConstants.CSV_FILEPATH_CPC);
			final String file = Config.getParameter(CatCoreConstants.CSV_FILENAME_PRODUCT);
			final StringBuilder fileName = createFileName(file);
			csvWriter = createCSVHeader(header, filePath, fileName.toString());

		}
		else if (cpcType.equalsIgnoreCase(CatCoreConstants.MEDIA))
		{
			final String header = Config.getParameter(CatCoreConstants.CSV_HEADER_MEDIA);
			final String filePath = Config.getParameter(CatCoreConstants.CSV_FILEPATH_CPC);
			final String file = Config.getParameter(CatCoreConstants.CSV_FILENAME_MEDIA);
			final StringBuilder fileName = createFileName(file);
			csvWriter = createCSVHeader(header, filePath, fileName.toString());
		}
		else
		{
			final String header = Config.getParameter(CatCoreConstants.CSV_HEADER);
			final String filePath = Config.getParameter(CatCoreConstants.CSV_FILEPATH);
			final String fileName = Config.getParameter(CatCoreConstants.CSV_FILENAME);
			csvWriter = createCSVHeader(header, filePath, fileName);
		}
		createCSVData(csvWriter, productList);
		csvWriter.close();

	}


	/**
	 * @param fileNames
	 *           CSV File
	 * @return CSV File Name
	 */
	private StringBuilder createFileName(final String fileNames)
	{
		final Date dt = new Date();
		final SimpleDateFormat df = new SimpleDateFormat(CatCoreConstants.DATE_FORMAT);
		final String date = df.format(dt);
		return new StringBuilder(fileNames).append("-").append(date).append(CatCoreConstants.CSV_FILE_VALUE)
				.append(Config.getParameter(CatCoreConstants.CSV_FILE_FORMAT));
	}

	/**
	 * This method will create CSV file Data
	 *
	 * @param productList
	 *
	 */
	private void createCSVData(final CSVWriter csvWriter, final List<String> productList)
	{
		String[] productsData = null;
		if (null != csvWriter)
		{
			for (final String prd : productList)

			{
				productsData = prd.split(CatCoreConstants.SEMICOLON);
				csvWriter.writeNext(productsData);
			}
		}
	}

	/**
	 * This method will create CSV file Header
	 *
	 * @param fileHeader
	 *           file header
	 * @param fileName
	 *           file Name
	 * @param filePath
	 *           file Path
	 *
	 * @throws IOException
	 *            input / ouput exception
	 *
	 */
	private CSVWriter createCSVHeader(final String fileHeader, final String filePath, final String fileName) throws IOException
	{
		final String delimiter = Config.getParameter(CatCoreConstants.CSV_DELIMITER);
		String header = fileHeader;
		header = StringUtils.replace(header, ":", " ");
		final StringBuilder fileRootPath = new StringBuilder(filePath).append(CatCoreConstants.SLASH).append(fileName);
		final String[] headerInStringArray = header.split("l");
		final CSVWriter csvWriter = creatCSVSheet(fileRootPath.toString(), delimiter);
		csvWriter.writeNext(headerInStringArray);
		return csvWriter;

	}

	/**
	 * This method will create csv sheet
	 *
	 * @throws IOException
	 */
	private CSVWriter creatCSVSheet(final String filePath, final String delimitter) throws IOException
	{
		final File file = new File(filePath);
		final Writer w = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
		return new CSVWriter(w, delimitter.charAt(0), CSVWriter.NO_QUOTE_CHARACTER);
	}

	/**
	 * This method will check whether folder is exist or not
	 *
	 *
	 */
	private boolean checkIfFoldersExists()
	{
		final String internalHybrisFolderPath = Config.getParameter(CatCoreConstants.CSV_FILEPATH);

		final File hybrisFolder = new File(internalHybrisFolderPath);
		boolean flag = false;
		if (hybrisFolder.exists())
		{
			flag = true;
		}
		return flag;
	}



}
