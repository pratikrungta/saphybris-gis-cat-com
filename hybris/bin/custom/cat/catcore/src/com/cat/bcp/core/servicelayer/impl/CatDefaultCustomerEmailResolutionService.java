/**
 *
 */
package com.cat.bcp.core.servicelayer.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commerceservices.customer.impl.DefaultCustomerEmailResolutionService;
import de.hybris.platform.commerceservices.enums.CustomerType;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.util.mail.MailUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.EmailException;
import org.apache.log4j.Logger;


/**
 * @author asomjal
 *
 */
public class CatDefaultCustomerEmailResolutionService extends DefaultCustomerEmailResolutionService
{

	private static final Logger LOG = Logger.getLogger(CatDefaultCustomerEmailResolutionService.class);

	/**
	 * This method is used to validate Email For Customer.
	 * 
	 * @param customerModel
	 *           CustomerModel Object
	 */
	@Override
	protected String validateAndProcessEmailForCustomer(final CustomerModel customerModel)
	{
		validateParameterNotNullStandardMessage("customerModel", customerModel);

		if (customerModel instanceof B2BCustomerModel)
		{
			final B2BCustomerModel b2bCustomerModel = (B2BCustomerModel) customerModel;
			final String email = CustomerType.GUEST.equals(customerModel.getType())
					? StringUtils.substringAfter(b2bCustomerModel.getEmail(), "|") : b2bCustomerModel.getEmail();
			try
			{
				MailUtils.validateEmailAddress(email, "customer email");
				return email;
			}
			catch (final EmailException e) //NOSONAR
			{
				LOG.info("Given uid is not appropriate email. Customer PK: " + customerModel.getPk() + " Exception: "
						+ e.getClass().getName());
			}
		}
		return null;
	}
}
