/**
 *
 */
package com.cat.bcp.core.servicelayer.impl;

import de.hybris.platform.acceleratorservices.email.impl.DefaultEmailGenerationService;
import de.hybris.platform.acceleratorservices.model.email.EmailAddressModel;
import de.hybris.platform.acceleratorservices.model.email.EmailMessageModel;
import de.hybris.platform.acceleratorservices.process.email.context.AbstractEmailContext;
import de.hybris.platform.processengine.model.BusinessProcessModel;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;


/**
 * @author asomjal
 *
 */
public class CatDefaultEmailGenerationService extends DefaultEmailGenerationService
{
	private static final String DEFAULT_TO_EMAIL_LIST = "cat.mail.defaultToEmailList";

	private static final String IS_PRODUCTION_ENVIRONMENT = "cat.environment.production";

	@Override
	protected EmailMessageModel createEmailMessage(final String emailSubject, final String emailBody,
			final AbstractEmailContext<BusinessProcessModel> emailContext)
	{
		final List<EmailAddressModel> toEmails = new ArrayList();
		if (Boolean.parseBoolean(Config.getParameter(IS_PRODUCTION_ENVIRONMENT)))
		{
			getToEmailStringFromContext(emailContext, toEmails);
		}
		else
		{
			final String defaultToEmailList = Config.getParameter(DEFAULT_TO_EMAIL_LIST);
			if (StringUtils.isNotBlank(defaultToEmailList))
			{
				final List<String> defaultToEmailArrayList = Arrays.asList(defaultToEmailList.split("\\s*,\\s*"));
				for (final String toEmail : defaultToEmailArrayList)
				{
					final EmailAddressModel toAddress = getEmailService().getOrCreateEmailAddressForEmail(toEmail, toEmail);
					toEmails.add(toAddress);
				}
			}
		}
		final EmailAddressModel fromAddress = getEmailService().getOrCreateEmailAddressForEmail(emailContext.getFromEmail(),
				emailContext.getFromDisplayName());
		return getEmailService().createEmailMessage(toEmails, new ArrayList<EmailAddressModel>(),
				new ArrayList<EmailAddressModel>(), fromAddress, emailContext.getFromEmail(), emailSubject, emailBody, null);
	}

	private void getToEmailStringFromContext(AbstractEmailContext<BusinessProcessModel> emailContext, List<EmailAddressModel> toEmails) {
		final String Email = emailContext.getToEmail();
		final List<String> defaultToEmailArrayList = Arrays.asList(Email.split("\\s*,\\s*"));

		if(defaultToEmailArrayList.size()>1) {
            for (final String toEmail : defaultToEmailArrayList) {
                final EmailAddressModel toAddress = getEmailService().getOrCreateEmailAddressForEmail(toEmail,
                        toEmail);
                toEmails.add(toAddress);
            }
        }else{
            final EmailAddressModel toAddress = getEmailService().getOrCreateEmailAddressForEmail(defaultToEmailArrayList.get(0),
                    emailContext.getDisplayName());
            toEmails.add(toAddress);
        }
	}

}
