/**
 *
 */
package com.cat.bcp.core.servicelayer.impl;

import de.hybris.platform.acceleratorservices.email.impl.DefaultEmailService;
import de.hybris.platform.acceleratorservices.model.email.EmailAttachmentModel;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.io.DataInputStream;

import javax.annotation.Resource;

import com.cat.bcp.core.constants.CatCoreConstants;


/**
 * @author ravjonnalagadda
 *
 */
public class CatDefaultEmailService extends DefaultEmailService
{

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	@Resource(name = "userService")
	private UserService userService;

	@Override
	public EmailAttachmentModel createEmailAttachment(final DataInputStream masterDataStream, final String filename,
			final String mimeType)
	{
		final EmailAttachmentModel attachment = getModelService().create(EmailAttachmentModel.class);
		attachment.setCode(filename);
		attachment.setMime(mimeType);
		attachment.setRealFileName(filename);
		if (userService.getCurrentUser() instanceof B2BCustomerModel)
		{
			attachment.setCatalogVersion(getCatalogVersion());
		}
		else
		{
			attachment.setCatalogVersion(getCatalogVersionForId());
		}

		getModelService().save(attachment);

		getMediaService().setStreamForMedia(attachment, masterDataStream, filename, mimeType, getEmailAttachmentsMediaFolder());
		return attachment;
	}

	/**
	 * To get Online CatalogVersion
	 */

	protected CatalogVersionModel getCatalogVersionForId()
	{

		return catalogVersionService.getCatalogVersion(CatCoreConstants.CAT_CATALOG, CatCoreConstants.CAT_VERSION);

	}
}
