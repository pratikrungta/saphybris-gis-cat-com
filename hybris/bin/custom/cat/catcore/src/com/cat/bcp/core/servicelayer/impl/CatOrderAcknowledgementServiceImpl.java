package com.cat.bcp.core.servicelayer.impl;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.util.Config;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.CatOrderAcknowledgementDao;
import com.cat.bcp.core.event.CatOrderAcknowledgementEmailEvent;
import com.cat.bcp.core.job.CatAbstractAbortableCronJob;
import com.cat.bcp.core.servicelayer.CatOrderAcknowledgementService;


/**
 *
 * @author ravjonnalagadda
 *
 */
public class CatOrderAcknowledgementServiceImpl implements CatOrderAcknowledgementService
{
	private static final Logger LOG = Logger.getLogger(CatOrderAcknowledgementServiceImpl.class);

	@Resource(name = "catOrderAcknowledgementDao")
	private CatOrderAcknowledgementDao orderAcknowledgementDao;

	@Resource(name = "eventService")
	private EventService eventService;

	/**
	 * {@inheritDoc}
	 *
	 **/
	@Override
	public void sendOrderAcknowledgementEmail(final CatAbstractAbortableCronJob catAbstractAbortableCronJob,
			final CronJobModel cronJobModel)
	{
		final String orderQuery = Config.getParameter("cat.order.acknowledgement.email.query");
		final List<OrderModel> orderList = orderAcknowledgementDao.getOrdersList(orderQuery);

		final Map<String, List<OrderModel>> orderGroups = groupOrders(orderList);
		final Map<String, List<OrderModel>> updatedOrderGroups = groupOrders(orderList);

		final Set<String> keys = orderGroups.keySet();
		final Iterator<String> iterator = keys.iterator();
		while (iterator.hasNext())
		{
			final String itr = iterator.next();
			if (itr.contains(CatCoreConstants.DOUBLEUNDERSCORE))
			{
				final List<OrderModel> values = orderGroups.get(itr);
				final String groupId = itr.split(CatCoreConstants.DOUBLEUNDERSCORE)[0];
				updatedOrderGroups.put(groupId, values);
			}
			else
			{
				updatedOrderGroups.put(itr, orderGroups.get(itr));
			}

		}

		final Set<String> groupIds = updatedOrderGroups.keySet();

		for (final String groupId : groupIds)
		{
			LOG.info("####### Order Acknowledgement Email Process ###############");
			eventService.publishEvent(new CatOrderAcknowledgementEmailEvent(groupId, updatedOrderGroups.get(groupId)));
		}

	}



	private Map<String, List<OrderModel>> groupOrders(final List<OrderModel> orderList)
	{
		final List<OrderModel> listOfOrdersWithGroupId = orderList.stream().filter(obj -> obj.getOrderGroupId() != null)
				.collect(Collectors.toList());
		return listOfOrdersWithGroupId.stream().collect(Collectors.groupingBy(obj -> obj.getOrderGroupId()));
	}
}
