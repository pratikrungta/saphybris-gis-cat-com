/**
 *
 */
package com.cat.bcp.core.servicelayer.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.CatOrderLaneDao;
import com.cat.bcp.core.enums.LaneTypeEnum;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.ProductLaneInfoModel;
import com.cat.bcp.core.order.CatReorderService;
import com.cat.bcp.core.servicelayer.CatOrderLaneProductService;
import com.cat.facades.order.CatLaneThreeData;
import com.cat.facades.order.CatLaneTwoData;


/**
 * Class for important validations related to add to cart
 *
 * @author bidavda
 *
 */
public class CatOrderLaneProductServiceImpl implements CatOrderLaneProductService
{

	private static final String WINDOW_VALIDATION = "orderingWindowValidation";

	private static final String DATE_VALIDATION = "dateValidation";

	private static final String COMPATIBILITY_VALIDATION = "compatibilityValidation";

	private static final String STOCK_VALIDATION = "stockValidation";


	@Resource(name = "catOrderLaneDao")
	private CatOrderLaneDao catOrderLaneDao;

	@Resource(name = "catReorderService")
	private CatReorderService catReorderService;

	@Resource(name = "catOrderWindowCalculationUtility")
	private CatOrderWindowCalculationUtility catOrderWindowCalculationUtility;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "modelService")
	private ModelService modelService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductLaneInfoModel> getLaneInfoForCurrentDealer(final B2BUnitModel b2bUnit)
	{
		return catOrderLaneDao.getProductLaneInfo(b2bUnit, getMonthYear());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductLaneInfoModel> getLaneInfoForSalesModel(final String salesModelId, final B2BUnitModel b2bUnit)
	{
		return catOrderLaneDao.getProductLaneInfoForSalesModel(salesModelId, b2bUnit, getMonthYear());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ProductLaneInfoModel getLaneInfoForLaneType(final String laneCode, final String salesModelId,
			final B2BUnitModel b2bUnit)
	{
		final List<ProductLaneInfoModel> laneInfoModel = catOrderLaneDao.getLaneInfoForLaneType(laneCode, salesModelId, b2bUnit,
				getMonthYear());

		if (CollectionUtils.isNotEmpty(laneInfoModel))
		{
			final Optional<ProductLaneInfoModel> laneInfo = laneInfoModel.stream().findFirst();
			if (laneInfo.isPresent())
			{
				return laneInfo.get();
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductLaneInfoModel> getLaneInfoForOrderedQuantity(final String salesModelId, final B2BUnitModel b2bUnit,
			final int orderedQty)
	{
		return catOrderLaneDao.getProductLaneInfoForOrderedQuantity(salesModelId, b2bUnit, getMonthYear(), orderedQty);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isProductOrderableForLane(final String laneCode, final String salesModelId, final String configVariantId,
			final boolean orderingWindowValidation, final boolean dateValidation, final boolean compatibilityValidation,
			final boolean stockValidation) throws ParseException
	{
		final Map<String, Boolean> productValidationMap = new HashMap<>();
		productValidationMap.put(WINDOW_VALIDATION, Boolean.valueOf(orderingWindowValidation));
		productValidationMap.put(DATE_VALIDATION, Boolean.valueOf(dateValidation));
		productValidationMap.put(COMPATIBILITY_VALIDATION, Boolean.valueOf(compatibilityValidation));
		productValidationMap.put(STOCK_VALIDATION, Boolean.valueOf(stockValidation));
		boolean isOrderableForLane = true;
		for (final Map.Entry<String, Boolean> entry : productValidationMap.entrySet())
		{
			if (Boolean.TRUE.equals(entry.getValue()))
			{
				switch (entry.getKey())
				{
					case WINDOW_VALIDATION:
						isOrderableForLane = isProductOrderableInOrderWindow(laneCode);
						break;
					case DATE_VALIDATION:
						isOrderableForLane = isProductOrderableForCurrentMonthYear(laneCode, salesModelId);
						break;
					case COMPATIBILITY_VALIDATION:
						isOrderableForLane = isProductCompatibleToOrder(laneCode, configVariantId);
						break;
					case STOCK_VALIDATION:
						isOrderableForLane = isProductOrderableForQuantity(laneCode, salesModelId);
						break;
					default:
						break;
				}
				if (!isOrderableForLane)
				{
					return false;
				}
			}
		}
		return isOrderableForLane;
	}

	/**
	 * Method to check the compatibility of a product
	 *
	 * @param laneCode
	 *           lane type
	 * @param configVariantId
	 *           configVariantId to be checked
	 * @return boolean
	 */
	private boolean isProductCompatibleToOrder(final String laneCode, final String configVariantId)
	{
		final List<ConfigVariantProductModel> configVariantList = catReorderService.getCompatibleConfigProducts(null, laneCode,
				configVariantId);
		if (CollectionUtils.isNotEmpty(configVariantList))
		{
			return (configVariantList.size() == 1 && configVariantList.get(0).getCode().equalsIgnoreCase(configVariantId)) ? true
					: false;
		}
		return false;
	}

	/**
	 * Method to check if a product can be ordered for current month year
	 *
	 * @param laneCode
	 *           lane type
	 * @param salesModelId
	 *           sales model id
	 * @return boolean
	 */
	private boolean isProductOrderableForCurrentMonthYear(final String laneCode, final String salesModelId)
	{
		final ProductLaneInfoModel laneInfoModel = getLaneInfoForLaneType(laneCode, salesModelId, getDealerB2BUnit());
		return null != laneInfoModel ? true : false;
	}

	/**
	 * Method to check if a product is orderable for a qty
	 *
	 * @param laneCode
	 *           lane type
	 * @param salesModelId
	 *           sales model id
	 * @return boolean
	 */
	private boolean isProductOrderableForQuantity(final String laneCode, final String salesModelId)
	{
		final ProductLaneInfoModel laneInfoModel = getLaneInfoForLaneType(laneCode, salesModelId, getDealerB2BUnit());
		if (null != laneInfoModel)
		{
			final int maximumCount = getMaximumCount(laneInfoModel);
			if (maximumCount > 0)
			{
				return true;
			}
		}
		return false;
	}


	/**
	 * Method to get the maximum count from lane info model
	 *
	 * @param laneInfoModel
	 *           lane info model
	 * @return int
	 */
	private int getMaximumCount(final ProductLaneInfoModel laneInfoModel)
	{
		final int maximumOrderableQty = null != laneInfoModel.getMaximumOrderableQuantity()
				? laneInfoModel.getMaximumOrderableQuantity().intValue() : 0;
		final int reOrderableQty = null != laneInfoModel.getReorderedQuantity() ? laneInfoModel.getReorderedQuantity().intValue()
				: 0;
		return maximumOrderableQty - reOrderableQty;
	}

	/**
	 * Method to check if the order window is open for lane
	 *
	 * @param laneCode
	 *           lane type
	 * @return boolean
	 */
	private boolean isProductOrderableInOrderWindow(final String laneCode) throws ParseException
	{
		final Map<String, Object> orderingWindowMessage = catOrderWindowCalculationUtility.getOrderingWindowMessage();
		if (LaneTypeEnum.LANE2.getCode().equalsIgnoreCase(laneCode))
		{
			final CatLaneTwoData lane2Data = (CatLaneTwoData) orderingWindowMessage.get(CatCoreConstants.LANE2);
			if (StringUtils.equals(lane2Data.getOrderWindowStatus(), CatCoreConstants.OPEN))
			{
				return true;
			}
		}
		else if (LaneTypeEnum.LANE3.getCode().equalsIgnoreCase(laneCode))
		{
			final CatLaneThreeData lane3Data = (CatLaneThreeData) orderingWindowMessage.get(CatCoreConstants.LANE3);
			if (StringUtils.equals(lane3Data.getOrderWindowStatus(), CatCoreConstants.OPEN))
			{
				return true;
			}
		}
		return false;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateReOrderedQuantityForProductLaneInfo(final OrderModel parentOrder)
	{
		if (parentOrder != null && CollectionUtils.isNotEmpty(parentOrder.getEntries()) && parentOrder.getLaneType() != null)
		{
			final List<AbstractOrderEntryModel> orderEntries = parentOrder.getEntries();
			final Map<String, Integer> salesModelOrderedQuantityMap = new HashMap();
			for (final AbstractOrderEntryModel orderEntry : orderEntries)
			{
				if (orderEntry.getProduct() instanceof ConfigVariantProductModel)
				{
					final ProductModel salesModel = ((BaseVariantProductModel) ((ConfigVariantProductModel) orderEntry.getProduct())
							.getBaseProduct()).getBaseProduct();
					populateGenericProductMap(salesModelOrderedQuantityMap, orderEntry, salesModel);
				}
			}
			if (MapUtils.isNotEmpty(salesModelOrderedQuantityMap))
			{
				updateReOrderedQuantity(parentOrder, salesModelOrderedQuantityMap);
			}
		}
	}

	/**
	 * This method is used to populate generic Product Map used to updated reOrdered Quantity.
	 *
	 * @param salesModelOrderedQuantityMap
	 *           genericProductMap
	 * @param orderEntry
	 *           orderEntry
	 * @param salesModel
	 *           salesModel
	 */
	private void populateGenericProductMap(final Map<String, Integer> salesModelOrderedQuantityMap,
			final AbstractOrderEntryModel orderEntry, final ProductModel salesModel)
	{
		if (MapUtils.isNotEmpty(salesModelOrderedQuantityMap))
		{
			final Integer quantity = salesModelOrderedQuantityMap.get(salesModel.getCode()) != null
					? salesModelOrderedQuantityMap.get(salesModel.getCode()) : Integer.valueOf(0);
			final Integer updatedQuantity = Integer.valueOf(quantity.intValue() + orderEntry.getQuantity().intValue());
			salesModelOrderedQuantityMap.put(salesModel.getCode(), updatedQuantity);
		}
		else
		{
			salesModelOrderedQuantityMap.put(salesModel.getCode(), Integer.valueOf(orderEntry.getQuantity().intValue()));
		}
	}

	/**
	 * This method is used to update ReOrdered Quantity in ProductLaneInfo Table.
	 *
	 * @param parentOrder
	 *           parentOrder
	 * @param salesModelOrderedQuantityMap
	 *           genericProduct Map
	 */
	private void updateReOrderedQuantity(final OrderModel parentOrder, final Map<String, Integer> salesModelOrderedQuantityMap)
	{

		for (final Map.Entry<String, Integer> salesModelOrderedQuantity : salesModelOrderedQuantityMap.entrySet())
		{
			final ProductLaneInfoModel productLaneInfoModel = getLaneInfoForLaneType(parentOrder.getLaneType().getCode(),
					salesModelOrderedQuantity.getKey(), getDealerB2BUnit());
			if (productLaneInfoModel != null)
			{
				final int existingReorderedQuantity = productLaneInfoModel.getReorderedQuantity() != null
						? productLaneInfoModel.getReorderedQuantity().intValue() : 0;
				productLaneInfoModel.setReorderedQuantity(
						Integer.valueOf(salesModelOrderedQuantity.getValue().intValue() + existingReorderedQuantity));
				modelService.save(productLaneInfoModel);
			}
		}
	}

	/**
	 * Method to get the dealer b2b unit
	 *
	 */
	private B2BUnitModel getDealerB2BUnit()
	{
		final B2BCustomerModel customerModel = (B2BCustomerModel) userService.getCurrentUser();
		return customerModel.getDefaultB2BUnit();
	}

	/**
	 * Method to get the current month and year
	 *
	 */
	private String getMonthYear()
	{
		final SimpleDateFormat sdf = new SimpleDateFormat("MM-yyyy");
		return sdf.format(new Date());
	}

}
