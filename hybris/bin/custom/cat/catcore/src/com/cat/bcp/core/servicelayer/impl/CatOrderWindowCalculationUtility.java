/**
 *
 */
package com.cat.bcp.core.servicelayer.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.CatOrderLaneDao;
import com.cat.bcp.core.model.CatLaneOrderingWindowModel;
import com.cat.facades.order.CatLaneThreeData;
import com.cat.facades.order.CatLaneTwoData;


/**
 * Class to calculate the order window throughout the application
 *
 * @author sagdhingra
 *
 */
public class CatOrderWindowCalculationUtility
{
	/**
	 *
	 */
	private static final String DD_MM_YYYY = "dd-MM-yyyy";
	private static final String END_DATE_FOR_LANE3 = "endDateForLane3";
	private static final String START_DATE_FOR_LANE3 = "startDateForLane3";
	private static final String END_DATE_FOR_LANE2 = "endDateForLane2";
	private static final String START_DATE_FOR_LANE2 = "startDateForLane2";

	private CatOrderLaneDao catOrderLaneDao;

	private static final Logger LOG = Logger.getLogger(CatOrderWindowCalculationUtility.class);



	/**
	 * Gets the ordering window message.
	 *
	 * @return the ordering window message
	 */
	public Map<String, Object> getOrderingWindowMessage()
	{
		final Map<String, Object> orderingWindowMessage = new HashMap<>();

		try
		{
			final CatLaneTwoData lane2 = getLane2Message(startAndEndDateMapMonthWise());
			orderingWindowMessage.put(CatCoreConstants.LANE2, lane2);

			final CatLaneThreeData lane3 = getLane3Message(startAndEndDateMapMonthWise());
			orderingWindowMessage.put(CatCoreConstants.LANE3, lane3);
		}
		catch (final Exception e)
		{
			orderingWindowMessage.put("Something went wrong", null);
			LOG.error(e.getMessage(), e);
		}
		return orderingWindowMessage;
	}

	/**
	 * Gets the next month calendar.
	 *
	 * @return the next month calendar
	 */
	public Calendar getNextMonthCalendar()
	{
		//Adding 1 month to the calendar
		final Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, 1);
		return calendar;
	}

	/**
	 * Gets the lane 2 message.
	 *
	 * @param startAndEndDateMap
	 *           the start and end date map
	 * @return the lane 2 message
	 * @throws ParseException
	 *            the parse exception
	 */
	public CatLaneTwoData getLane2Message(final Map<String, Date> startAndEndDateMap) throws ParseException
	{
		final CatLaneTwoData lane2 = new CatLaneTwoData();
		final Calendar cal = Calendar.getInstance();
		if (MapUtils.isNotEmpty(startAndEndDateMap) && startAndEndDateMap.containsKey(START_DATE_FOR_LANE2)
				&& startAndEndDateMap.containsKey(END_DATE_FOR_LANE2))
		{
			if (checkIfDateFallsInOrderWindow(startAndEndDateMap.get(START_DATE_FOR_LANE2),
					startAndEndDateMap.get(END_DATE_FOR_LANE2)))
			{
				final int noOfDaysToClose = findNumberOfDaysLeftForOrderWindowToClose(cal.getTime(),
						startAndEndDateMap.get(END_DATE_FOR_LANE2));
				lane2.setLeftDays(Integer.valueOf(noOfDaysToClose));
				lane2.setOrderWindowStatus(CatCoreConstants.OPEN);
			}
			else
			{
				final int noOfDaysToOpen = findNumberOfDaysLeftForOrderWindowToOpen(cal.getTime(),
						startAndEndDateMap.get(START_DATE_FOR_LANE2));
				lane2.setLeftDays(Integer.valueOf(noOfDaysToOpen));
				lane2.setOrderWindowStatus(CatCoreConstants.CLOSED);
			}
		}
		else
		{
			lane2.setLeftDays(Integer.valueOf(0));
			lane2.setOrderWindowStatus(CatCoreConstants.CLOSED);
		}
		return lane2;
	}

	/**
	 * Gets the lane 3 message.
	 *
	 * @param startAndEndDateMap
	 *           the start and end date map
	 * @return the lane 3 message
	 * @throws ParseException
	 *            the parse exception
	 */
	public CatLaneThreeData getLane3Message(final Map<String, Date> startAndEndDateMap) throws ParseException
	{
		final CatLaneThreeData lane3 = new CatLaneThreeData();
		final Calendar cal = Calendar.getInstance();
		if (MapUtils.isNotEmpty(startAndEndDateMap) && startAndEndDateMap.containsKey(START_DATE_FOR_LANE3)
				&& startAndEndDateMap.containsKey(END_DATE_FOR_LANE3))
		{
			if (checkIfDateFallsInOrderWindow(startAndEndDateMap.get(START_DATE_FOR_LANE3),
					startAndEndDateMap.get(END_DATE_FOR_LANE3)))
			{
				final int noOfDaysToClose = findNumberOfDaysLeftForOrderWindowToClose(cal.getTime(),
						startAndEndDateMap.get(END_DATE_FOR_LANE3));
				lane3.setLeftDays(Integer.valueOf(noOfDaysToClose));
				lane3.setOrderWindowStatus(CatCoreConstants.OPEN);
			}
			else
			{
				final int noOfDaysToOpen = findNumberOfDaysLeftForOrderWindowToOpen(cal.getTime(),
						startAndEndDateMap.get(START_DATE_FOR_LANE3));
				lane3.setLeftDays(Integer.valueOf(noOfDaysToOpen));
				lane3.setOrderWindowStatus(CatCoreConstants.CLOSED);

			}
		}
		else
		{
			lane3.setLeftDays(Integer.valueOf(0));
			lane3.setOrderWindowStatus(CatCoreConstants.CLOSED);
		}
		return lane3;
	}



	/**
	 * Start and end date map month wise.
	 *
	 * @return the map
	 * @throws ParseException
	 *            the parse exception
	 */
	private Map<String, Date> startAndEndDateMapMonthWise() throws ParseException
	{
		final Map<String, Date> startAndEndDateMap = new HashMap<>();
		final List<CatLaneOrderingWindowModel> catLaneOrderingWindowModel = getStartAndEndDate();
		if (CollectionUtils.isNotEmpty(catLaneOrderingWindowModel))
		{
			final Optional<CatLaneOrderingWindowModel> orderWindowLane2Stream = catLaneOrderingWindowModel.stream()
					.filter(orderWindow -> orderWindow.getLaneType().equalsIgnoreCase(CatCoreConstants.LANE2)).findFirst();
			if (orderWindowLane2Stream.isPresent())
			{
				startAndEndDateMap.put(START_DATE_FOR_LANE2, orderWindowLane2Stream.get().getFromDate());
				startAndEndDateMap.put(END_DATE_FOR_LANE2, orderWindowLane2Stream.get().getToDate());
			}
			final Optional<com.cat.bcp.core.model.CatLaneOrderingWindowModel> orderWindowLane3Stream = catLaneOrderingWindowModel
					.stream().filter(orderWindow -> orderWindow.getLaneType().equalsIgnoreCase(CatCoreConstants.LANE3)).findFirst();
			if (orderWindowLane3Stream.isPresent())
			{
				startAndEndDateMap.put(START_DATE_FOR_LANE3, orderWindowLane3Stream.get().getFromDate());
				startAndEndDateMap.put(END_DATE_FOR_LANE3, orderWindowLane3Stream.get().getToDate());
			}
		}
		return startAndEndDateMap;
	}


	/**
	 * Check if date falls in order window.
	 *
	 * @param startDateForLane2
	 *           the start date for lane 2
	 * @param endDateForLane2
	 *           the end date for lane 2
	 * @return true, if date falls in order window
	 */
	private boolean checkIfDateFallsInOrderWindow(final Date startDateForLane2, final Date endDateForLane2)
	{
		try
		{
			final SimpleDateFormat sdf = new SimpleDateFormat(DD_MM_YYYY);

			final Date orderDate = sdf.parse(sdf.format(new Date()));

			if (((orderDate.before(endDateForLane2) && orderDate.after(startDateForLane2)) || orderDate.equals(startDateForLane2)
					|| orderDate.equals(endDateForLane2)))
			{
				return true;
			}
		}
		catch (final ParseException parseException)
		{
			LOG.error("Error while parsing date", parseException);
		}
		return false;
	}

	/**
	 * Gets the start and end date.
	 *
	 * @param currentYear
	 *           the current year
	 * @return the start and end date
	 * @throws ParseException
	 *            the parse exception
	 */
	private List<CatLaneOrderingWindowModel> getStartAndEndDate() throws ParseException
	{
		final SimpleDateFormat sdf = new SimpleDateFormat(DD_MM_YYYY);
		final Date currentDate = sdf.parse(sdf.format(new Date()));
		return catOrderLaneDao.getOrderWindowForLanes(currentDate);
	}

	/**
	 * Find number of days left for order window to open.
	 *
	 * @param currentDate
	 *           the current date
	 * @param nextMonthStartDate
	 *           the next month start date
	 * @return the number of days left to open
	 * @throws ParseException
	 */
	private int findNumberOfDaysLeftForOrderWindowToOpen(final Date currentDate, final Date nextMonthStartDate)
			throws ParseException
	{
		final SimpleDateFormat sdf = new SimpleDateFormat(DD_MM_YYYY);

		final Calendar startCal = Calendar.getInstance();
		startCal.setTime(sdf.parse(sdf.format(currentDate)));

		final Calendar endCal = Calendar.getInstance();
		endCal.setTime(nextMonthStartDate);
		int workDays = 0;

		if (startCal.getTimeInMillis() == endCal.getTimeInMillis())
		{
			return 0;
		}

		if (startCal.getTimeInMillis() > endCal.getTimeInMillis())
		{
			startCal.setTime(nextMonthStartDate);
			endCal.setTime(currentDate);
		}

		do
		{
			startCal.add(Calendar.DAY_OF_MONTH, 1);
			++workDays;
		}
		while (startCal.getTimeInMillis() < endCal.getTimeInMillis());

		return workDays;
	}

	/**
	 * Find number of days left for order window to close.
	 *
	 * @param orderDate
	 *           the order date
	 * @param endDate
	 *           the end date
	 * @return the number of days left to close
	 * @throws ParseException
	 */
	private int findNumberOfDaysLeftForOrderWindowToClose(final Date orderDate, final Date endDate) throws ParseException
	{
		final SimpleDateFormat sdf = new SimpleDateFormat(DD_MM_YYYY);

		final Calendar startCal = Calendar.getInstance();
		startCal.setTime(sdf.parse(sdf.format(orderDate)));

		final Calendar endCal = Calendar.getInstance();
		endCal.setTime(endDate);
		int workDays = 0;

		if (startCal.getTimeInMillis() == endCal.getTimeInMillis())
		{
			return 0;
		}

		if (startCal.getTimeInMillis() > endCal.getTimeInMillis())
		{
			startCal.setTime(endDate);
			endCal.setTime(orderDate);
		}

		do
		{
			startCal.add(Calendar.DAY_OF_MONTH, 1);
			++workDays;
		}
		while (startCal.getTimeInMillis() < endCal.getTimeInMillis()); //excluding end date
		return workDays;
	}

	/**
	 * @return the catOrderLaneDao
	 */
	public CatOrderLaneDao getCatOrderLaneDao()
	{
		return catOrderLaneDao;
	}

	/**
	 * @param catOrderLaneDao
	 *           the orderLaneDao to set
	 */
	@Required
	public void setCatOrderLaneDao(final CatOrderLaneDao catOrderLaneDao)
	{
		this.catOrderLaneDao = catOrderLaneDao;
	}

}
