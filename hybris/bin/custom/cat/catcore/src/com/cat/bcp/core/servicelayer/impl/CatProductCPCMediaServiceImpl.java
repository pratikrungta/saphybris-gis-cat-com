/**
 *
 */
package com.cat.bcp.core.servicelayer.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.media.MediaFormatModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.jalo.AbortCronJobException;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.util.Config;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.exception.CatException;
import com.cat.bcp.core.job.CatAbstractAbortableCronJob;
import com.cat.bcp.core.servicelayer.AbstractCatProductCPCService;
import com.cat.bcp.core.servicelayer.CatCSVFileService;
import com.cat.bcp.core.servicelayer.CatProductCPCMediaService;



/**
 * @author ravjonnalagadda
 *
 *         Implementation of Cat Product CPC Service
 *
 */
public class CatProductCPCMediaServiceImpl extends AbstractCatProductCPCService implements CatProductCPCMediaService
{
	private static final Logger LOG = Logger.getLogger(CatProductCPCMediaServiceImpl.class);
	public static final String IO_EXCEPTION = "IO Exception: ";
	@Resource(name = "mediaService")
	private MediaService mediaService;
	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;
	@Resource(name = "configurationService")
	private ConfigurationService configurationService;
	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;
	@Resource(name = "catCoreCSVFileService")
	private CatCSVFileService catCSVFileService;

	/**
	 * {@inheritDoc}
	 *
	 * @throws CatException
	 *            : Custom Exception class handle all the exceptions
	 */
	@Override
	public void productCPCMediaService(final CatAbstractAbortableCronJob catAbortableCronJob, final CronJobModel cronJobModel)
			throws CatException
	{
		try
		{
			final Map<ProductModel, Document> cpcProductDocument = fetchCPCProductXML(catAbortableCronJob, cronJobModel);
			fetchMediaForProducts(cpcProductDocument);
		}
		catch (final MalformedURLException e)
		{
			LOG.error("URL Exception: " + e);
			throw new CatException(" URL Exception in Cat product Media Service ", e);
		}
		catch (final ParserConfigurationException e)
		{
			LOG.error("Parser Exception: " + e);
			throw new CatException(" Parser Exception in Cat product Media Service: ", e);
		}
		catch (final SAXException e)
		{
			LOG.error("SAXException: " + e);
			throw new CatException(" SAXException in Cat product Media Service: ", e);
		}
		catch (final AbortCronJobException e)
		{
			LOG.error("AbortCronJobException: " + e);
			throw new CatException(" AbortCronJobException in Cat product Media Service: ", e);
		}
		catch (final IOException e)
		{
			LOG.error(" IO Exception in Cat product Media Service" + e);
			throw new CatException(IO_EXCEPTION, e);
		}

	}


	/**
	 * @param cpcProductDoucment
	 *           This is to hold product xml document from CPC service
	 */
	private void fetchMediaForProducts(final Map<ProductModel, Document> cpcProductDoucment)
	{
		final List mediaList = new ArrayList();
		final List productMedias = new ArrayList<>();
		try
		{
			for (final Map.Entry<ProductModel, Document> cpcProductxml : cpcProductDoucment.entrySet())
			{
				final List mediaContainerList = new ArrayList<>();
				final List mediaContainersList = new ArrayList();
				final String productCode = cpcProductxml.getKey().getCode();
				final Document productXml = cpcProductxml.getValue();
				getMediaForProduct(productXml, productCode, mediaContainerList, mediaList, mediaContainersList, productMedias);
			}

			catCSVFileService.csvService(mediaList, CatCoreConstants.MEDIA);
			catCSVFileService.csvService(productMedias, CatCoreConstants.PRODUCT);
		}
		catch (final IOException e)
		{
			LOG.error(IO_EXCEPTION, e);
		}
	}

	/**
	 * Generic method to fetch and update Media details
	 *
	 * @param doc
	 *           Product Document
	 * @param productCode
	 *           Product Code
	 * @param mediaContainerList
	 *           This is to hold media container for GalleryImages for a product
	 * @param mediaList
	 *           This is to have Media List to generate Media csv file.
	 * @param mediaContainersList
	 *           This is to have galleryImages list for a product
	 * @param productMedias
	 *           This is to send the product code , product picture , product thumbnail and product galleryImages to CSV
	 *           writer.
	 */
	private void getMediaForProduct(final Document doc, final String productCode, final List mediaContainerList,
			final List mediaList, final List mediaContainersList, final List productMedias)
	{
		doc.getDocumentElement().normalize();
		final NodeList productNodeList = doc.getElementsByTagName(CatCoreConstants.MARKETING_CONTENT);
		getMarketingContent(productNodeList, productCode, mediaContainerList, mediaList, mediaContainersList, productMedias);
	}

	/**
	 * This Method provides marketing content for the product
	 *
	 * @param productNodeList
	 *           This gives complete node list in the product xml.
	 * @param mediaList
	 *           This is to have Media List to generate Media csv file.
	 * @param mediaContainerList
	 *           This is to hold media container for GalleryImages for a product
	 * @param mediaContainersList
	 *           This is to have galleryImages list for a product
	 * @param productMedias
	 *           This is to send the product code , product picture , product thumbnail and product galleryImages to CSV
	 *           writer.
	 */
	private void getMarketingContent(final NodeList productNodeList, final String productCode, final List mediaContainerList,
			final List mediaList, final List mediaContainersList, final List productMedias)
	{
		final List medias = new ArrayList();
		for (int temp = 0; temp < productNodeList.getLength(); temp++)
		{
			final NodeList primaryContentNodeList = getContent(temp, productNodeList);

			for (int tem = 0; tem < primaryContentNodeList.getLength(); tem++)
			{
				final Node primaryContentNode = primaryContentNodeList.item(tem);
				final Element primaryContentElement = (Element) primaryContentNode;
				final NodeList secondaryContentNodeList = primaryContentElement.getElementsByTagName(CatCoreConstants.CONTENT);
				for (int te = 0; te < secondaryContentNodeList.getLength(); te++)
				{
					final Node secondaryContentNode = secondaryContentNodeList.item(te);
					final Element secondayContentElement = (Element) secondaryContentNode;
					final NodeList mediaNodeList = secondayContentElement.getElementsByTagName(CatCoreConstants.MEDIA);
					medias.add(mediaContainerList);
					medias.add(mediaList);
					medias.add(mediaContainersList);
					medias.add(productMedias);

					findMedia(mediaNodeList, productCode, secondayContentElement, te, medias);
				}
			}
		}

	}

	/**
	 * This method helps to get Media Details for the product Picture,Gallery Images and PDF .
	 *
	 * @param loopValue
	 *           This is to have loop number
	 * @param mediaList
	 *           This is to have Media List to generate Media csv file.
	 *
	 */
	private void findMedia(final NodeList mediaNodeList, final String productCode, final Element secondayContentElement,
			final int loopValue, final List mediaList)
	{
		for (int innerTemp = 0; innerTemp < mediaNodeList.getLength(); innerTemp++)
		{
			final Node mediaNode = mediaNodeList.item(innerTemp);
			final Element mediaElement = (Element) mediaNode;
			final NodeList childNodes = mediaElement.getChildNodes();
			final Node childNode = childNodes.item(0);
			final Element childElement = (Element) childNode;
			final String mediaContext = childElement.getTextContent();
			final String mediaUrl = getMediaUrl(productCode, mediaContext);

			if (secondayContentElement.getAttribute(CatCoreConstants.TYPE).equalsIgnoreCase(CatCoreConstants.IMAGE)
					&& checkSortValueAndLevel(secondayContentElement))
			{
				final String mainImageUrl = mediaUrl;
				final String mimeType = mediaElement.getAttribute(CatCoreConstants.TYPE);
				final String mediaImageUrl = imageUrlPattern(mainImageUrl);
				createMainMediaForProduct(productCode, mediaImageUrl, mimeType, (List) mediaList.get(1));
			}
			if (mediaElement.getAttribute(CatCoreConstants.TYPE).equals(Config.getParameter(CatCoreConstants.TYPE_MEDIA_PDF)))
			{
				supportMaterialLinkForProduct(productCode, secondayContentElement, (List) mediaList.get(1), mediaUrl);
			}
			else if (mediaElement.getAttribute(CatCoreConstants.TYPE).equals(Config.getParameter(CatCoreConstants.TYPE_MEDIA_IMAGE))
					&& secondayContentElement.getAttribute(CatCoreConstants.TYPE).equals(CatCoreConstants.IMAGE))
			{
				final String mimeType = mediaElement.getAttribute(CatCoreConstants.TYPE);
				final String mediasUrl = imageUrlPattern(mediaUrl);
				final List mediasList = createMedia(productCode, mediasUrl, loopValue + 1, mimeType, (List) mediaList.get(1));
				final List mediaContainersList = createMediaContainer(mediasList, productCode, loopValue + 1, (List) mediaList.get(0),
						(List) mediaList.get(2));
				createGalleryImagesForProduct(productCode, mediaContainersList, (List) mediaList.get(2), (List) mediaList.get(3));
			}
			else if (mediaElement.getAttribute(CatCoreConstants.TYPE).equals(Config.getParameter(CatCoreConstants.TYPE_MEDIA_VIDEO)))
			{
				LOG.debug("Youtube Video Link: " + mediaUrl);
			}
			else
			{
				LOG.debug("Media is not correct  :" + mediaUrl);
			}
		}
	}


	/**
	 * @param productCode
	 *           productCode Value
	 * @param secondayContentElement
	 *           ContentElement Value
	 * @param mediaList
	 *           Media Container List
	 * @param mediaUrl
	 *           Media URL
	 */
	private void supportMaterialLinkForProduct(final String productCode, final Element secondayContentElement,
			final List mediaList, final String mediaUrl)
	{
		LOG.debug("Support Material Link || CPC Media Service Method Started:");
		if (secondayContentElement.getAttribute(CatCoreConstants.TYPE).equals(CatCoreConstants.DOCUMENT)
				&& checkSortValueAndLevel(secondayContentElement))
		{
			final MediaModel media = getProductspecificationDoc(mediaUrl, productCode);
			if (null != media && StringUtils.isNotEmpty(media.getCode()))
			{
				mediaList.add(media.getCode() + ";" + media.getInternalURL());
				assignPDFMediaToProduct(media, productCode);
			}
		}
		LOG.debug("Support Material Link || CPC Media Service Method Ended");
	}

	/**
	 * @param mainImageUrl
	 *           the mainImageUrl is the Scene7 URL coming from ProductXml.
	 * @return it return ProductMediaUrl along with requried pattern.
	 */
	private String imageUrlPattern(final String mainImageUrl)
	{
		String mediaImageUrl;
		final String urlPattern = Config.getParameter(CatCoreConstants.URLFORMAT);
		if (StringUtils.isNotBlank(urlPattern))
		{
			mediaImageUrl = mainImageUrl + urlPattern;
		}
		else
		{
			mediaImageUrl = mainImageUrl;
		}
		return mediaImageUrl;
	}

	/**
	 * Check Sort Value And Level
	 *
	 * @param secondayContentElement
	 *           Secondary content value
	 * @return boolean boolean value.
	 */
	private boolean checkSortValueAndLevel(final Element secondayContentElement)
	{
		return "1".equals(secondayContentElement.getAttribute(CatCoreConstants.SORTVALUE))
				&& "2".equals(secondayContentElement.getAttribute(CatCoreConstants.LEVEL));
	}

	/**
	 * This Method is to Assign Pdf media to ProductModel
	 *
	 * @param productCode
	 *           Product Code
	 */
	private void assignPDFMediaToProduct(final MediaModel media, final String productCode)
	{
		final ProductModel productModel = modelService.create(ProductModel.class);
		try
		{
			productModel.setCode(productCode);
			productModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);
			productModel.setCatalogVersion(getCatalogVersion());
			final ProductModel product = flexibleSearchService.getModelByExample(productModel);
			product.setSupportMaterial(media);
			LOG.debug("Product Code :" + productCode + " : " + "Support Material Link" + media.getURL());
			modelService.save(product);
		}
		catch (final ModelNotFoundException mnfe)
		{
			LOG.error(CatCoreConstants.MODEL_NOTFOUND_EXCEPTION + productCode, mnfe);
		}
		catch (final ModelSavingException mse)
		{
			LOG.error(CatCoreConstants.MODEL_SAVE_EXCEPTION + productCode, mse);
		}

	}

	/**
	 * This method help to get Media URl of the product.
	 *
	 * @param productCode
	 *           productCode value
	 * @param mediaContext
	 *           it is to hold media url value.
	 *
	 */
	private static String getMediaUrl(final String productCode, final String mediaContext)
	{
		final String[] mediaRegex = mediaContext.split(productCode);
		final String mediaText = mediaRegex[0].trim();
		final String[] mediaUrlList = mediaText.split(" ");
		return mediaUrlList[0];

	}

	/**
	 * This method provides Content value of the Media URL.
	 *
	 * @param productNodeList
	 *           it is to fetch data from content node.
	 */
	private NodeList getContent(final int temp, final NodeList productNodeList)
	{
		final Node productNode = productNodeList.item(temp);
		final Element productElement = (Element) productNode;
		return productElement.getElementsByTagName(CatCoreConstants.CONTENT);

	}

	/**
	 * This method is to save Product SpecificPdfurl
	 *
	 * @param productCode
	 *           productCode
	 * @param url
	 *           media Url
	 *
	 */
	private MediaModel getProductspecificationDoc(final String url, final String productCode)
	{

		final MediaModel mediaModel = modelService.create(MediaModel.class);
		final MediaFormatModel mediaFormatModel = getMediaFormatModel();
		try
		{
			mediaModel.setCode(productCode + "_" + CatCoreConstants.PDF);
			mediaModel.setCatalogVersion(getCatalogVersion());
			final MediaModel media = flexibleSearchService.getModelByExample(mediaModel);
			media.setURL(url);
			modelService.save(media);
			return media;
		}
		catch (final ModelNotFoundException mnfe)
		{
			mediaModel.setMediaFormat(mediaFormatModel);
			mediaModel.setMime(CatCoreConstants.MEDIA_FORMAT);
			mediaModel.setRealFileName(CatCoreConstants.IMAGE);
			mediaModel.setURL(url);
			modelService.save(mediaModel);
			LOG.error(CatCoreConstants.MODEL_NOTFOUND_EXCEPTION + mediaModel.getCode(), mnfe);
			return mediaModel;
		}
		catch (final ModelSavingException mse)
		{
			LOG.error(CatCoreConstants.MODEL_SAVE_EXCEPTION + mediaModel.getCode(), mse);
		}
		return null;
	}

	/**
	 * update Media for Media Model
	 *
	 * @param mimeType
	 *           media format type
	 * @param url
	 *           media url
	 * @param code
	 *           media model code value
	 *
	 */
	private MediaModel getMediaModelByExample(final String code, final String url, final String mimeType)
	{
		final MediaModel mediaModel = modelService.create(MediaModel.class);
		final MediaFormatModel mediaFormatModel = getMediaFormatModel();
		try
		{
			mediaModel.setCode(code);
			mediaModel.setCatalogVersion(getCatalogVersion());
			final MediaModel media = flexibleSearchService.getModelByExample(mediaModel);
			media.setURL(url);
			media.setMime(mimeType);
			media.setMediaFormat(mediaFormatModel);
			media.setRealFileName(CatCoreConstants.IMAGE);
			media.setCatalogVersion(getCatalogVersion());
			modelService.save(media);
			return media;
		}
		catch (final ModelNotFoundException mnfe)
		{
			mediaModel.setURL(url);
			mediaModel.setMime(mimeType);
			mediaModel.setMediaFormat(mediaFormatModel);
			mediaModel.setRealFileName(CatCoreConstants.IMAGE);
			modelService.save(mediaModel);
			mediaModel.getMime();
			LOG.error("MODEL NOT FOUND HENCE CREATING THE MEDIA MODEL : " + mediaModel.getCode(), mnfe);
			return mediaModel;
		}
		catch (final ModelSavingException mse)
		{
			LOG.error(CatCoreConstants.MODEL_SAVE_EXCEPTION + mediaModel.getCode(), mse);


		}
		return mediaModel;
	}

	/**
	 * This method helps to create Gallery Images for Product
	 *
	 * @param productCode
	 *           productCode
	 * @param mediaContainersList
	 *           mediaContiner list for galleryImages
	 * @param productMedias
	 *           this is to send product code, product thumbnail , product picuture and galleryimaeges to csv writer.
	 */
	private void createGalleryImagesForProduct(final String productCode, final List mediaContainerList,
			final List mediaContainersList, final List productMedias)
	{
		LOG.debug("Create Gallery Iamges method started");
		final ProductModel productModel = new ProductModel();
		try
		{
			productModel.setCode(productCode);
			productModel.setCatalogVersion(getCatalogVersion());
			productModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);
			final ProductModel product = flexibleSearchService.getModelByExample(productModel);
			product.setGalleryImages(mediaContainerList);
			modelService.save(product);
			final StringBuilder imageURL = new StringBuilder();
			for (final Object image : mediaContainersList)
			{
				imageURL.append(image + CatCoreConstants.COMMA);
			}
			if (null != product.getPicture() && null != product.getThumbnail() && null != product.getSupportMaterial())
			{
				productMedias.add(
						product.getCode() + CatCoreConstants.SEMICOLON + product.getThumbnail().getCode() + CatCoreConstants.SEMICOLON
								+ product.getPicture().getCode() + CatCoreConstants.SEMICOLON + product.getSupportMaterial().getCode()
								+ CatCoreConstants.SEMICOLON + StringUtils.stripEnd(imageURL.toString(), ","));
			}
		}
		catch (final ModelNotFoundException mnfe)
		{
			LOG.error(CatCoreConstants.MODEL_NOTFOUND_EXCEPTION + productModel.getCode(), mnfe);
		}
		catch (final ModelSavingException mse)
		{
			LOG.error(CatCoreConstants.MODEL_SAVE_EXCEPTION + productModel.getCode(), mse);
		}
		LOG.debug("Create Gallery Images method ended");

	}

	/**
	 * This method helps to create thumbnail and picture for Product.
	 *
	 * @param productCode
	 *           productCode
	 * @param mimeType
	 *           Media Type
	 * @param mediaList
	 *           This to send media list details to csv writer.
	 *
	 *
	 */
	private void createMainMediaForProduct(final String productCode, final String mainMediaeUrl, final String mimeType,
			final List mediaList)
	{
		LOG.debug("Picture Media and Thumbanial Media Method || CPC Media Service Method Started:");
		final ProductModel prodModel = new ProductModel();
		try
		{
			final List pictureMedias = new ArrayList<>();
			final List thumbnailMedias = new ArrayList<>();
			final MediaModel pictureModel = getPictureModel(productCode, mainMediaeUrl, mimeType);
			final MediaModel thumbnailModel = getThumbnailModel(productCode, mainMediaeUrl, mimeType);
			mediaList.add(pictureModel.getCode() + ";" + pictureModel.getInternalURL());
			mediaList.add(thumbnailModel.getCode() + ";" + thumbnailModel.getInternalURL());
			pictureMedias.add(pictureModel);
			thumbnailMedias.add(thumbnailModel);
			prodModel.setCode(productCode);
			prodModel.setCatalogVersion(getCatalogVersion());
			prodModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);
			final ProductModel productModel = flexibleSearchService.getModelByExample(prodModel);
			final MediaContainerModel mediaContainerPicture = createMediaContainerForPicture(pictureMedias, prodModel);
			final MediaContainerModel mediaContainerThumbnail = createMediaContainerForThumbnail(thumbnailMedias, prodModel);
			if (null != mediaContainerPicture)
			{
				pictureModel.setMediaContainer(mediaContainerPicture);
			}
			if (null != mediaContainerThumbnail)
			{
				thumbnailModel.setMediaContainer(mediaContainerThumbnail);
			}
			productModel.setPicture(pictureModel);
			productModel.setThumbnail(thumbnailModel);
			modelService.save(productModel);
			LOG.debug("Picture Media" + pictureModel.getCode() + " and Thumbanial Media" + thumbnailModel.getCode()
					+ " is assgined to product" + productCode);

		}
		catch (final ModelNotFoundException mnfe)
		{
			LOG.error(CatCoreConstants.MODEL_NOTFOUND_EXCEPTION + prodModel.getCode(), mnfe);
		}
		catch (final ModelSavingException mse)
		{
			LOG.error(CatCoreConstants.MODEL_SAVE_EXCEPTION + prodModel.getCode(), mse);
		}
		LOG.debug("Picture Media and Thumbanial Media Method || CPC Media Service Method Ended");
	}

	/**
	 * @param medias
	 *           This to create media container for picture media.
	 * @param prodModel
	 *           productModel
	 * @return container return container media model.
	 */
	private MediaContainerModel createMediaContainerForPicture(final List medias, final ProductModel prodModel)
	{
		final MediaContainerModel container = this.modelService.create(MediaContainerModel.class);
		if (CollectionUtils.isNotEmpty(medias))
		{
			try
			{
				container.setQualifier(prodModel.getCode() + "_" + "picture");
				return getMediaContainerModel(medias, container);
			}
			catch (final ModelNotFoundException mnfe)
			{
				logErrorStatus(medias, container, mnfe);
				return container;
			}
			catch (final ModelSavingException mse)
			{
				LOG.error(CatCoreConstants.MODEL_SAVE_EXCEPTION + container.getQualifier(), mse);
			}
		}
		return container;

	}

	/**
	 * @param medias
	 *           This to create media container for thumbnail Media.
	 * @param prodModel
	 *           productModel
	 * @return container return container media model.
	 */
	private MediaContainerModel createMediaContainerForThumbnail(final List medias, final ProductModel prodModel)
	{
		final MediaContainerModel container = this.modelService.create(MediaContainerModel.class);
		if (CollectionUtils.isNotEmpty(medias))
		{
			try
			{
				container.setQualifier(prodModel.getCode() + "_" + "thumbnail");
				return getMediaContainerModel(medias, container);
			}
			catch (final ModelNotFoundException mnfe)
			{
				logErrorStatus(medias, container, mnfe);
				return container;
			}
			catch (final ModelSavingException mse)
			{
				LOG.error(CatCoreConstants.MODEL_SAVE_EXCEPTION + container.getQualifier(), mse);
			}
		}
		return container;

	}

	/**
	 * Gets the media container model.
	 *
	 * @param medias
	 *           the medias
	 * @param container
	 *           the container
	 * @return the media container model
	 */
	private MediaContainerModel getMediaContainerModel(final List medias, final MediaContainerModel container)
	{
		container.setCatalogVersion(getCatalogVersion());
		final MediaContainerModel mediaContainModel = flexibleSearchService.getModelByExample(container);
		mediaContainModel.setMedias(medias);
		mediaContainModel.setCatalogVersion(getCatalogVersion());
		modelService.save(mediaContainModel);
		return mediaContainModel;
	}

	/**
	 * Log error status.
	 *
	 * @param medias
	 *           the medias
	 * @param container
	 *           the container
	 * @param mnfe
	 *           the mnfe
	 */
	private void logErrorStatus(final List medias, final MediaContainerModel container, final ModelNotFoundException mnfe)
	{
		container.setMedias(medias);
		modelService.save(container);
		LOG.error(CatCoreConstants.MODEL_NOTFOUND_EXCEPTION + container.getQualifier(), mnfe);
	}

	/**
	 * @param productCode
	 *           productCode
	 * @param thumbnailUrl
	 *           Media url value
	 * @param mimeType
	 *           media type.
	 */
	private MediaModel getThumbnailModel(final String productCode, final String thumbnailUrl, final String mimeType)
	{
		final String thumbnailCode = productCode + "_" + "thumbnail";
		LOG.debug("Thubnail Media" + thumbnailCode + "for product " + productCode + "Picture Media URL" + thumbnailUrl);
		return getMediaModelByExample(thumbnailCode, thumbnailUrl, mimeType);
	}

	/**
	 * @param productCode
	 *           productCode
	 * @param pictureUrl
	 *           media url value
	 * @param mimeType
	 *
	 */
	private MediaModel getPictureModel(final String productCode, final String pictureUrl, final String mimeType)
	{
		final String pictureCode = productCode + "_" + "picture";
		LOG.debug("Picture Media is creating:" + pictureCode + "for product " + productCode + "Picture Media URL" + pictureUrl);
		return getMediaModelByExample(pictureCode, pictureUrl, mimeType);

	}

	/**
	 * This Method helps to create MediaContainer for Media and assign media and gllary images to Product.
	 *
	 * @param mediasList
	 *           this is to have media container for galleryImages.
	 * @param productCode
	 *           product Code
	 * @param loopValue
	 *           this value is used to create media container code.
	 * @param mediaContainerList
	 *           this is add mediacontainer model details to Media Container List.
	 * @param mediaContainersList
	 *           this is to send galleryMedia container list to csv writer.
	 *
	 */
	private List createMediaContainer(final List mediasList, final String productCode, final int loopValue,
			final List mediaContainerList, final List mediaContainersList)
	{
		final MediaContainerModel container = this.modelService.create(MediaContainerModel.class);
		for (int i = 0; i < mediasList.size(); i++)
		{
			try
			{
				container.setQualifier(productCode + "_" + "galleryImage" + "-" + loopValue);
				final MediaContainerModel mediaContainModel = getMediaContainerModel(mediasList, container);
				mediaContainerList.add(mediaContainModel);
				mediaContainersList.add(mediaContainModel.getQualifier());
			}
			catch (final ModelNotFoundException mnfe)
			{
				container.setMedias(mediasList);
				modelService.save(container);
				mediaContainerList.add(container);
				mediaContainersList.add(container.getQualifier());
				LOG.error(CatCoreConstants.MODEL_NOTFOUND_EXCEPTION + container.getQualifier(), mnfe);
			}
			catch (final ModelSavingException mse)
			{
				LOG.error(CatCoreConstants.MODEL_SAVE_EXCEPTION + container.getQualifier(), mse);
			}
		}
		return mediaContainerList;
	}

	/**
	 * This Method helps to Created Media for product
	 *
	 * @param productCode
	 *           product code
	 * @param mediaUrl
	 *           media url
	 * @param mediaList
	 *           this media list is to send csv writer.
	 *
	 */
	private List createMedia(final String productCode, final String mediaUrl, final int innerTemp, final String mimeType,
			final List mediaList)
	{
		final List<MediaModel> mediasList = new ArrayList<>();
		final MediaFormatModel mediaFormatModel = getMediaFormatModel();
		final MediaModel mediaModel = modelService.create(MediaModel.class);
		try
		{
			mediaModel.setCode(productCode + "_" + "galleryImage" + "-" + innerTemp);
			mediaModel.setCatalogVersion(getCatalogVersion());
			final MediaModel media = flexibleSearchService.getModelByExample(mediaModel);
			media.setURL(mediaUrl);
			media.setMime(mimeType);
			media.setMediaFormat(mediaFormatModel);
			media.setCatalogVersion(getCatalogVersion());
			modelService.save(media);
			mediaList.add(media.getCode() + ";" + mediaUrl);
			mediasList.add(media);

		}
		catch (final ModelNotFoundException mnfe)
		{
			mediaModel.setMediaFormat(mediaFormatModel);
			mediaModel.setMime(mimeType);
			mediaModel.setRealFileName(CatCoreConstants.IMAGE);
			mediaModel.setURL(mediaUrl);
			modelService.save(mediaModel);
			mediaList.add(mediaModel.getCode() + ";" + mediaUrl);
			mediasList.add(mediaModel);
			LOG.error(CatCoreConstants.MODEL_NOTFOUND_EXCEPTION + mediaModel.getCode(), mnfe);
		}
		catch (final ModelSavingException mse)
		{
			LOG.error(CatCoreConstants.MODEL_SAVE_EXCEPTION + mediaModel.getCode(), mse);
		}
		return mediasList;

	}

	/**
	 * This Method helps to get MediaFormat(800Wx600H) Details.
	 */
	private MediaFormatModel getMediaFormatModel()
	{
		final MediaFormatModel mediaFormatModel = new MediaFormatModel();
		mediaFormatModel.setQualifier(Config.getParameter(CatCoreConstants.MEDIA_FORMAT));
		return flexibleSearchService.getModelByExample(mediaFormatModel);
	}

	/**
	 * To get Stage CatalogVersion
	 */

	protected CatalogVersionModel getCatalogVersion()
	{

		return catalogVersionService.getCatalogVersion(Config.getParameter(CatCoreConstants.CAT_PRODUCT_CATALOG),
				Config.getParameter(CatCoreConstants.CAT_PRODUCT_CATALOG_VERSION));


	}




}
