package com.cat.bcp.core.servicelayer.impl;

import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.jalo.AbortCronJobException;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.util.Config;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.exception.CatException;
import com.cat.bcp.core.job.CatAbstractAbortableCronJob;
import com.cat.bcp.core.model.AttributeInfoModel;
import com.cat.bcp.core.model.FacetInfoModel;
import com.cat.bcp.core.servicelayer.AbstractCatProductCPCService;
import com.cat.bcp.core.servicelayer.CatCSVFileService;
import com.cat.bcp.core.servicelayer.CatProductFacetService;


/**
 *
 * @author ravjonnalagadda
 *
 */
public class CatProductFacetServiceImpl extends AbstractCatProductCPCService implements CatProductFacetService
{
	private static final Logger LOG = Logger.getLogger(CatProductFacetServiceImpl.class);

	@Resource(name = "catCoreCSVFileService")
	private CatCSVFileService catCSVFileService;
	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;
	@Resource(name = "modelService")
	private ModelService modelService;
	@Resource(name = "configurationService")
	private ConfigurationService configurationService;
	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;
	@Resource(name = "catalogService")
	private CatalogService catalogService;


	/**
	 * {@inheritDoc}
	 *
	 **/
	@Override
	public void productFacetService(final CatAbstractAbortableCronJob catAbstractAbortableCronJob, final CronJobModel cronJobModel)
			throws CatException
	{
		try
		{
			final List<String> productList = new ArrayList<>();
			final Map<ProductModel, Document> productxml = fetchCPCProductXML(catAbstractAbortableCronJob, cronJobModel);
			fetchFacetData(productList, productxml);
			catCSVFileService.csvService(productList, CatCoreConstants.FACET);
		}
		catch (final MalformedURLException e)
		{
			LOG.error("URL Exception: " + e);
			throw new CatException("URL Exception in Cat product Facet Service ", e);
		}
		catch (final ParserConfigurationException e)
		{
			LOG.error("Parser Exception: " + e);
			throw new CatException("Parser Exception in Cat product Facet Service : ", e);
		}
		catch (final SAXException e)
		{
			LOG.error("SAXException: " + e);
			throw new CatException("SAXException in Cat product Facet Service : ", e);
		}
		catch (final AbortCronJobException e)
		{
			LOG.error("AbortCronJobException: " + e);
			throw new CatException("AbortCronJobException in Cat product Facet Service : ", e);
		}
		catch (final IOException e)
		{
			LOG.error("IO Exception: " + e);
			throw new CatException("IO Exception in Cat product Facet Service : ", e);
		}
		catch (final Exception e)
		{
			LOG.error("Exception Occured : " + e);
			throw new CatException("Exception has occured in Cat product Facet Service :", e);
		}
	}

	/**
	 * This method will set product facet details and attribute details in a rows.
	 *
	 * @param productList
	 * @param productxml
	 */
	private void fetchFacetData(final List<String> productList, final Map<ProductModel, Document> productxml)
	{
		if (MapUtils.isNotEmpty(productxml))
		{
			for (final Map.Entry<ProductModel, Document> cpcProductXml : productxml.entrySet())
			{
				getFacetDetailsForProducts(productList, cpcProductXml.getKey(), cpcProductXml.getValue());
			}
		}
	}

	/**
	 * This method is to fetch inactive facets attributes for products.
	 *
	 * @param facet
	 */
	private void updateOrphanedFacets(final Map<String, Set<String>> facet)
	{
		for (final Map.Entry<String, Set<String>> facets : facet.entrySet())
		{
			final ProductModel productModel = modelService.create(ProductModel.class);
			try
			{
				productModel.setCode(facets.getKey());
				productModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);
				productModel.setCatalogVersion(getCatalogVersion());
				final ProductModel model = flexibleSearchService.getModelByExample(productModel);
				final Set<AttributeInfoModel> attributeInfoModels = new HashSet<>(model.getAttributeInfo());
				findOrphanAttributeInfoModels(facets, productModel, model, attributeInfoModels);
			}
			catch (final ModelNotFoundException mnfe)
			{
				LOG.error(CatCoreConstants.MODEL_NOTFOUND_EXCEPTION + productModel.getCode(), mnfe);
			}
		}
	}

	/**
	 * @param facets
	 * @param productModel
	 * @param model
	 * @param attributeInfoModels
	 */
	private void findOrphanAttributeInfoModels(final Map.Entry<String, Set<String>> facets, final ProductModel productModel,
			final ProductModel model, final Set<AttributeInfoModel> attributeInfoModels)
	{
		for (final AttributeInfoModel attributeInfoModel : attributeInfoModels)
		{
			if (null != attributeInfoModel.getFacetInfo())
			{
				final FacetInfoModel facetInfoModel = attributeInfoModel.getFacetInfo();
				final Set<String> facetInfos = facets.getValue();

				final boolean flag = updateOrphanedFacet(facetInfoModel, facetInfos);
				if (!flag)
				{
					LOG.info(
							"FacetInfo Model :" + facetInfoModel.getCode() + "is no longer part of product:" + productModel.getCode());
					facetInfoModel.setIsMarkForDelete(Boolean.TRUE);
					modelService.save(facetInfoModel);
					modelService.refresh(model);
					LOG.info("FacetInfo Model :" + facetInfoModel.getCode() + "has been marked for delete");
				}
			}
		}
	}

	/**
	 * This method will mark inactive facets as mark for delete.
	 *
	 * @param facetInfoModel
	 * @param facetInfos
	 * @return boolean
	 */
	private boolean updateOrphanedFacet(final FacetInfoModel facetInfoModel, final Set<String> facetInfos)
	{
		final Iterator iterator = facetInfos.iterator();
		while (iterator.hasNext())
		{
			if (facetInfoModel.getCode().equals(iterator.next()))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * This method will help to find orphaned attributes for products.
	 *
	 * @param attribute
	 */
	private void updateOrphanedAttributes(final Map<String, Set<String>> attribute)
	{
		for (final Map.Entry<String, Set<String>> attributes : attribute.entrySet())
		{
			final ProductModel productModel = modelService.create(ProductModel.class);
			try
			{
				productModel.setCode(attributes.getKey());
				productModel.setApprovalStatus(ArticleApprovalStatus.APPROVED);
				productModel.setCatalogVersion(getCatalogVersion());
				final ProductModel model = flexibleSearchService.getModelByExample(productModel);
				final Set<AttributeInfoModel> attributeInfoModels = new HashSet<>(model.getAttributeInfo());
				final List attributeKey = new ArrayList<>();
				for (final AttributeInfoModel attributeInfoModel : attributeInfoModels)
				{
					attributeKey.add(attributeInfoModel.getCode());
				}
				final Set<String> attris = attributes.getValue();
				final Collection<String> similar = new HashSet<>(attributeKey);
				final Collection<String> different = new HashSet<>();
				different.addAll(attributeKey);
				different.addAll(attris);

				similar.retainAll(attris);
				different.removeAll(similar);
				if (CollectionUtils.isNotEmpty(different))
				{
					updateAttributes(model, different);
				}
			}
			catch (final ModelNotFoundException mnfe)
			{
				LOG.error(CatCoreConstants.MODEL_NOTFOUND_EXCEPTION + productModel.getCode(), mnfe);
			}
		}

	}

	/**
	 * This method will make inactive attributes as make for delete.
	 *
	 * @param productModel
	 * @param different
	 */
	private void updateAttributes(final ProductModel productModel, final Collection<String> different)
	{
		for (final String dif : different)
		{
			final AttributeInfoModel attributeInfoModel = modelService.create(AttributeInfoModel.class);
			try
			{
				LOG.info(dif);
				attributeInfoModel.setCode(dif);
				attributeInfoModel.setCatalogVersion(getCatalogVersion());
				final AttributeInfoModel attributeModel = flexibleSearchService.getModelByExample(attributeInfoModel);
				LOG.info("Attribute Model :" + attributeModel.getCode() + "is no longer part of product:" + productModel.getCode());
				attributeModel.setIsMarkForDelete(Boolean.TRUE);
				modelService.save(attributeModel);
				LOG.info("Attribute Info Model has been marked for delete" + attributeModel.getCode());
			}
			catch (final ModelNotFoundException mnfe)
			{
				LOG.error(CatCoreConstants.MODEL_NOTFOUND_EXCEPTION + attributeInfoModel.getCode(), mnfe);
			}
		}

	}

	/**
	 * This method will fetch the Facet details for products.
	 *
	 * @param productList
	 * @param productModel
	 * @param doc
	 */
	private void getFacetDetailsForProducts(final List<String> productList, final ProductModel productModel, final Document doc)
	{
		doc.getDocumentElement().normalize();
		final NodeList nodeList = doc.getElementsByTagName(CatCoreConstants.LISTOFMETADATA);
		final Set<String> facetSet = new HashSet();
		final Set<String> attributeSet = new HashSet();
		final Map<String, Set<String>> facet = new HashMap<>();
		final Map<String, Set<String>> attribute = new HashMap<>();

		for (int i = 0; i < nodeList.getLength(); i++)
		{
			final Node primaryContentNode = nodeList.item(i);
			final Element primaryContentElement = (Element) primaryContentNode;
			final NodeList facetContentNodeList = primaryContentElement.getElementsByTagName(CatCoreConstants.METADATA);
			for (int j = 0; j < facetContentNodeList.getLength(); j++)
			{
				final Node facetContentNode = facetContentNodeList.item(j);
				final Element facetContentElement = (Element) facetContentNode;
				final NodeList attributeList = facetContentNode.getChildNodes();
				final String facetKey = productModel.getCode() + CatCoreConstants.UNDERSCORE
						+ facetContentElement.getAttribute(CatCoreConstants.KEY).replaceAll("\\s+", "");
				facetSet.add(facetKey);
				facet.put(productModel.getCode(), facetSet);
				for (int k = 0; k < attributeList.getLength(); k++)
				{
					final Node attributeContentNode = attributeList.item(k);
					final Element attributeContentElement = (Element) attributeContentNode;
					final String attributeKey = facetKey + CatCoreConstants.UNDERSCORE
							+ attributeContentElement.getAttribute(CatCoreConstants.KEY).replaceAll("\\s+", "");
					LOG.info("CSV RECORD :" + productModel.getCode() + "," + facetKey + ","
							+ facetContentElement.getAttribute(CatCoreConstants.NAME) + "," + attributeKey + ","
							+ attributeContentElement.getTextContent());
					productList.add(productModel.getCode() + CatCoreConstants.SEMICOLON + facetKey + CatCoreConstants.SEMICOLON
							+ facetContentElement.getAttribute(CatCoreConstants.NAME) + CatCoreConstants.SEMICOLON
							+ facetContentElement.getAttribute(CatCoreConstants.KEY) + CatCoreConstants.SEMICOLON + attributeKey
							+ CatCoreConstants.SEMICOLON + attributeContentElement.getTextContent() + CatCoreConstants.SEMICOLON
							+ attributeContentElement.getAttribute(CatCoreConstants.KEY));

					attributeSet.add(attributeKey);
					attribute.put(productModel.getCode(), attributeSet);
				}

			}

		}
		updateOrphanedAttributes(attribute);
		updateOrphanedFacets(facet);
	}

	/**
	 * This method provide catalog version and product catalog details.
	 */

	protected CatalogVersionModel getCatalogVersion()
	{

		return catalogVersionService.getCatalogVersion(Config.getParameter(CatCoreConstants.CAT_PRODUCT_CATALOG),
				Config.getParameter(CatCoreConstants.CAT_PRODUCT_CATALOG_VERSION));


	}
}
