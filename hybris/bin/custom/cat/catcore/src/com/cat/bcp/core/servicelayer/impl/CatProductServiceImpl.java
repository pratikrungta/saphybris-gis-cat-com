/**
 *
 */
package com.cat.bcp.core.servicelayer.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.impl.DefaultProductService;

import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.CatB2BUnitDao;
import com.cat.bcp.core.dao.CatProductDao;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.InventoryReservationModel;
import com.cat.bcp.core.model.PartVariantProductModel;
import com.cat.bcp.core.servicelayer.CatProductService;


/**
 * @author bidavda
 *
 */
public class CatProductServiceImpl extends DefaultProductService implements CatProductService
{
	@Resource(name = "b2bUnitDao")
	private CatB2BUnitDao b2bUnitDao;

	@Resource(name = "catProductDao")
	private CatProductDao catProductDao;


	/**
	 * @param catProductDao
	 *           the catProductDao to set
	 */
	public void setCatProductDao(final CatProductDao catProductDao)
	{
		this.catProductDao = catProductDao;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isUtvProduct(final ProductModel productModel)
	{
		final Collection<CategoryModel> categories = productModel.getSupercategories();
		for (final CategoryModel category : categories)
		{
			if (category.getCode().equalsIgnoreCase(CatCoreConstants.UTV))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isUtvProduct(final ProductData productData)
	{
		final Collection<CategoryData> categories = productData.getCategories();
		for (final CategoryData category : categories)
		{
			if (CatCoreConstants.UTV.equalsIgnoreCase(category.getCode()))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isUtvProduct(final String category)
	{
		boolean utilityProducts = false;
		if (category.toLowerCase().contains("utility"))
		{
			utilityProducts = true;
		}
		return utilityProducts;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getProductType(final ProductModel product)
	{
		String productType = CatCoreConstants.NEW;

		if (product instanceof ConfigVariantProductModel)
		{
			if (isUtvProduct(product))
			{
				productType = CatCoreConstants.UTV;
			}
			else
			{
				productType = CatCoreConstants.NEW;
			}
		}
		else if (product instanceof BaseVariantProductModel)
		{
			productType = CatCoreConstants.USED;
		}
		else if (product instanceof PartVariantProductModel)
		{
			productType = CatCoreConstants.ACCESSORIES;
		}
		return productType;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<InventoryReservationModel> getInventoryReservationForProduct(final ProductModel productModel,
			final B2BUnitModel b2bUnitModel)
	{
		return b2bUnitDao.fetchAllInventoryReservationForProduct(productModel, b2bUnitModel);
	}


	/**
	 * @return the b2bUnitDao
	 */
	public CatB2BUnitDao getB2bUnitDao()
	{
		return b2bUnitDao;
	}


	/**
	 * @param b2bUnitDao
	 *           the b2bUnitDao to set
	 */
	public void setB2bUnitDao(final CatB2BUnitDao b2bUnitDao)
	{
		this.b2bUnitDao = b2bUnitDao;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductReferenceModel> getAccessoryData()
	{
		return catProductDao.getAccessoryData();
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductModel> getCUVModels()
	{
		return catProductDao.getCUVModels();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isAccessoryProduct(final ProductData product)
	{
		for (final CategoryData category : product.getCategories())
		{
			if (StringUtils.equals(CatCoreConstants.PARTS_CLASSIFICATION, category.getCode()))
			{
				return true;
			}
		}
		return false;
	}

}
