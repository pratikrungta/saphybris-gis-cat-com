/**
 *
 */
package com.cat.bcp.core.servicelayer.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SearchQueryPageableData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchRequest;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchResponse;
import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.SolrFacetSearchConfigSelectionStrategy;
import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.impl.DefaultSearchQueryTemplateNameResolver;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfigService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.indexer.SolrIndexedTypeCodeResolver;
import de.hybris.platform.solrfacetsearch.indexer.exceptions.IndexerException;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedTypeModel;
import de.hybris.platform.solrfacetsearch.search.impl.DefaultFacetSearchService;

import java.util.Collection;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.core.convert.converter.Converter;

import com.cat.bcp.core.enums.CatIndexedTypeEnum;


/**
 * @author bidavda
 *
 */
public class CatSolrProductSearchService<ITEM> //NOSONAR
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(CatSolrProductSearchService.class);

	private FacetSearchConfigService facetSearchConfigService;
	private SolrIndexedTypeCodeResolver solrIndexedTypeCodeResolver;
	private SolrFacetSearchConfigSelectionStrategy solrFacetSearchConfigSelectionStrategy;

	private Converter<SearchQueryPageableData<SolrSearchQueryData>, SolrSearchRequest> searchQueryPageableConverter;
	private Converter<SolrSearchRequest, SolrSearchResponse> searchRequestConverter;
	private Converter<SolrSearchResponse, ProductCategorySearchPageData<SolrSearchQueryData, ITEM, CategoryModel>> searchResponseConverter;

	@Resource(name = "facetSearchService")
	DefaultFacetSearchService facetSearchService;

	@Resource(name = "searchQueryTemplateNameResolver")
	DefaultSearchQueryTemplateNameResolver searchQueryTemplateNameResolver;

	protected Converter<SearchQueryPageableData<SolrSearchQueryData>, SolrSearchRequest> getSearchQueryPageableConverter()
	{
		return searchQueryPageableConverter;
	}

	@Required
	public void setSearchQueryPageableConverter(
			final Converter<SearchQueryPageableData<SolrSearchQueryData>, SolrSearchRequest> searchQueryPageableConverter)
	{
		this.searchQueryPageableConverter = searchQueryPageableConverter;
	}

	protected Converter<SolrSearchRequest, SolrSearchResponse> getSearchRequestConverter()
	{
		return searchRequestConverter;
	}

	@Required
	public void setSearchRequestConverter(final Converter<SolrSearchRequest, SolrSearchResponse> searchRequestConverter)
	{
		this.searchRequestConverter = searchRequestConverter;
	}

	protected Converter<SolrSearchResponse, ProductCategorySearchPageData<SolrSearchQueryData, ITEM, CategoryModel>> getSearchResponseConverter()
	{
		return searchResponseConverter;
	}

	@Required
	public void setSearchResponseConverter(
			final Converter<SolrSearchResponse, ProductCategorySearchPageData<SolrSearchQueryData, ITEM, CategoryModel>> searchResponseConverter)
	{
		this.searchResponseConverter = searchResponseConverter;
	}

	/**
	 * @return the facetSearchConfigService
	 */
	public FacetSearchConfigService getFacetSearchConfigService()
	{
		return facetSearchConfigService;
	}

	/**
	 * @param facetSearchConfigService
	 *           the facetSearchConfigService to set
	 */
	public void setFacetSearchConfigService(final FacetSearchConfigService facetSearchConfigService)
	{
		this.facetSearchConfigService = facetSearchConfigService;
	}


	/**
	 * @return the solrIndexedTypeCodeResolver
	 */
	public SolrIndexedTypeCodeResolver getSolrIndexedTypeCodeResolver()
	{
		return solrIndexedTypeCodeResolver;
	}

	/**
	 * @param solrIndexedTypeCodeResolver
	 *           the solrIndexedTypeCodeResolver to set
	 */
	public void setSolrIndexedTypeCodeResolver(final SolrIndexedTypeCodeResolver solrIndexedTypeCodeResolver)
	{
		this.solrIndexedTypeCodeResolver = solrIndexedTypeCodeResolver;
	}

	/**
	 * @return the solrFacetSearchConfigSelectionStrategy
	 */
	public SolrFacetSearchConfigSelectionStrategy getSolrFacetSearchConfigSelectionStrategy()
	{
		return solrFacetSearchConfigSelectionStrategy;
	}

	/**
	 * @param solrFacetSearchConfigSelectionStrategy
	 *           the solrFacetSearchConfigSelectionStrategy to set
	 */
	public void setSolrFacetSearchConfigSelectionStrategy(
			final SolrFacetSearchConfigSelectionStrategy solrFacetSearchConfigSelectionStrategy)
	{
		this.solrFacetSearchConfigSelectionStrategy = solrFacetSearchConfigSelectionStrategy;
	}


	// End spring inject methods
	/**
	 * Method to perform search on the values passed in search query data and index type :
	 *
	 * @param searchQueryData
	 *           the searchQueryData to be sent to doSearch method for building final Solr Query
	 * @param pageableData
	 *           the pageableData to be sent to doSearch method for setting the pagination data
	 * @param indexType
	 *           the indexType enum value whether it is a Base Variant or Config Variant Product
	 * @return ProductCategorySearchPageData
	 */
	public ProductCategorySearchPageData<SolrSearchQueryData, ITEM, CategoryModel> searchAgain(
			final SolrSearchQueryData searchQueryData, final PageableData pageableData, final CatIndexedTypeEnum indexType)
	{
		return doSearch(searchQueryData, pageableData, indexType);
	}

	/**
	 * Method which initiates all the populators registered for converting searchQueryPageableData into SolrSearchRequest
	 * SolrSearchRequest into SolrSearchResponse SolrSearchResponse into ProductCategorySearchPageData
	 *
	 * @param searchQueryData
	 *           the searchQueryData for building final Solr Query
	 * @param pageableData
	 *           the pageableData for setting the pagination data
	 * @param indexType
	 *           the indexType enum value whether it is a Base Variant or Config Variant Product
	 * @return ProductCategorySearchPageData this will be used by searchAgain method
	 */
	protected ProductCategorySearchPageData<SolrSearchQueryData, ITEM, CategoryModel> doSearch(
			final SolrSearchQueryData searchQueryData, final PageableData pageableData, final CatIndexedTypeEnum indexType)
	{
		try
		{
			validateParameterNotNull(searchQueryData, "SearchQueryData cannot be null");
			// Create the SearchQueryPageableData that contains our parameters
			final SearchQueryPageableData<SolrSearchQueryData> searchQueryPageableData = buildSearchQueryPageableData(
					searchQueryData, pageableData, indexType);
			// Build up the search request
			final SolrSearchRequest solrSearchRequest = getSearchQueryPageableConverter().convert(searchQueryPageableData);

			// Execute the search
			final SolrSearchResponse solrSearchResponse = getSearchRequestConverter().convert(solrSearchRequest);

			// Convert the response
			return getSearchResponseConverter().convert(solrSearchResponse);

		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage(), e);
		}
		return null;
	}

	/**
	 * Building the search query pageable data 1) Setting the indexed type so that the query is run on that indexed type
	 *
	 * @param searchQueryData
	 *           the searchQueryData for building final Solr Query
	 * @param pageableData
	 *           the pageableData for setting the pagination data
	 * @param indexType
	 *           the indexType enum value whether it is a Base Variant or Config Variant Product
	 * @return SearchQueryPageableData
	 */
	private SearchQueryPageableData<SolrSearchQueryData> buildSearchQueryPageableData(final SolrSearchQueryData searchQueryData,
			final PageableData pageableData, final CatIndexedTypeEnum indexType)
	{
		final SearchQueryPageableData<SolrSearchQueryData> searchQueryPageableData = createSearchQueryPageableData();
		searchQueryPageableData.setSearchQueryData(searchQueryData);
		searchQueryPageableData.setPageableData(pageableData);
		searchQueryPageableData.setIndexedType(indexType.getCode());
		return searchQueryPageableData;
	}

	protected SearchQueryPageableData<SolrSearchQueryData> buildSearchQueryPageableData(final SolrSearchQueryData searchQueryData,
			final PageableData pageableData)
	{
		final SearchQueryPageableData<SolrSearchQueryData> searchQueryPageableData = createSearchQueryPageableData();
		searchQueryPageableData.setSearchQueryData(searchQueryData);
		searchQueryPageableData.setPageableData(pageableData);
		return searchQueryPageableData;
	}

	// Create methods for data object - can be overridden in spring config

	protected SearchQueryPageableData<SolrSearchQueryData> createSearchQueryPageableData()
	{
		return new SearchQueryPageableData<>();
	}

	protected SolrSearchQueryData createSearchQueryData()
	{
		return new SolrSearchQueryData();
	}

	/**
	 * Method to find SolrIndexedTypeModel
	 *
	 * @param facetSearchConfigModel
	 *           to get the solr indexed Types
	 * @param indexedType
	 *           to compare with the solr indexed Types retrieved from facetSearchConfigModel
	 * @return SolrIndexedTypeModel instance of SolrIndexedTypeModel
	 */
	protected SolrIndexedTypeModel findIndexedTypeModel(final SolrFacetSearchConfigModel facetSearchConfigModel,
			final IndexedType indexedType) throws IndexerException
	{
		if (indexedType == null)
		{
			throw new IndexerException("indexedType is NULL!");
		}
		for (final SolrIndexedTypeModel type : facetSearchConfigModel.getSolrIndexedTypes())
		{
			if (solrIndexedTypeCodeResolver.resolveIndexedTypeCode(type).equals(indexedType.getUniqueIndexedTypeCode()))
			{
				return type;
			}
		}
		throw new IndexerException("Could not find matching model for type: " + indexedType.getCode());
	}

	/**
	 * Method to get the Indexed Type
	 *
	 * @param config
	 *           to fetch the index config
	 * @param indexedType
	 *           to compare with the indexed types present in index config
	 * @return Optional<IndexedType>
	 */
	protected Optional<IndexedType> getIndexedType(final FacetSearchConfig config, final String indexedType)
	{
		final IndexConfig indexConfig = config.getIndexConfig();
		// Strategy for working out which of the available indexed types to use
		final Collection<IndexedType> indexedTypes = indexConfig.getIndexedTypes().values();
		return indexedTypes.stream().filter(indexType -> indexType.getCode().equalsIgnoreCase(indexedType)).findFirst();
	}
}
