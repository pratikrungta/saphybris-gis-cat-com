/**
 *
 */
package com.cat.bcp.core.servicelayer.impl;

import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.model.contents.ContentCatalogModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ClassificationData;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.FeatureValueData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.Config;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.CatSpeccheckDao;
import com.cat.bcp.core.exception.CatSpeccheckException;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.servicelayer.CatSpeccheckService;




/**
 * The Class CatSpeccheckServiceImpl.
 *
 * @author asomjal This class is used for Speccheck integration
 */
public class CatSpeccheckServiceImpl implements CatSpeccheckService
{

	private static final Logger LOG = Logger.getLogger(CatSpeccheckServiceImpl.class);

	@Resource(name = "categoryService")
	private CategoryService categoryService;

	@Resource(name = "catCategoryUrlConverter")
	private Converter<CategoryModel, CategoryData> catCategoryUrlConverter;

	@Resource(name = "catalogVersionService")
	CatalogVersionService catalogVersionService;

	@Resource(name = "catSpeccheckDao")
	private CatSpeccheckDao catSpeccheckDao;

	private static final String DUMMY_VALUES = "1234";
	private static final String MACHINE_ID = "Machine_ID";
	private static final String MANUFACTURER_ID = "Manufacturer_ID";
	private static final String CAT_MANUFACTURER_ID = "5";
	private static final String MANUFACTURER = "Manufacturer";
	private static final String MODEL = "Model";
	private static final String DISCONTINUED = "Discontinued";
	private static final String DISCONTINUED_VALUE = "0";
	private static final String TIER_ID = "Tier_ID";
	private static final String CAT_TIER_ID = "5";
	private static final String SORT_DISPLAY = "Sort_Display";
	private static final String CATERPILLAR = "CATERPILLAR";
	private static final String LABEL = "Label";
	private static final String MACHINES = "MACHINES";
	private static final String DATATYPE = "DataType";
	private static final String DATATYPE_I = "I";
	private static final String MEDIA = "Media";
	private static final String DATATYPE_M = "M";
	private static final String DATATYPE_H = "H";
	private static final String DATATYPE_A = "A";
	private static final String UNIT = "Unit";
	private static final String DATA = "Data";
	private static final String COLUMN = "Column ";
	private static final String FILENAME = "Filename";
	private static final String DESCRIPTION = "Description";
	private static final String PHOTO = "Photo";
	private static final String CONTENT_TYPE = "Content_Type";
	private static final String CONTENT_TYPE_JPG = ".jpg";
	private static final String LANGUAGE_ID = "Language_ID";
	private static final String UTV_PRODUCT="UTV PRODUCT";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ResponseEntity<Object> callSpeccheckAPI(final String requestURI)
	{
		final RestTemplate restTemplate = getRestTemplate(requestURI);

		final HttpHeaders requestHeaders = new HttpHeaders();
		final HttpEntity<?> entity = new HttpEntity(requestHeaders);
		return restTemplate.exchange(requestURI, HttpMethod.GET, entity, Object.class);
	}

	/**
	 * Gets the rest template.
	 *
	 * @param requestURI
	 *           the request URI
	 * @return RestTemplate
	 */
	private RestTemplate getRestTemplate(final String requestURI)
	{
		LOG.debug("********************requestURI******************\n" + requestURI);
		RestTemplate restTemplate = new RestTemplate();
		if (Boolean.parseBoolean(Config.getParameter(CatCoreConstants.SPECCHECK_PROXYENABLED)))
		{
			final String proxyPort = Config.getParameter(CatCoreConstants.SPECCHECK_PROXYPORT);
			final String proxyUrl = Config.getParameter(CatCoreConstants.SPECCHECK_PROXYURL);
			LOG.debug(proxyPort + " - Port Number - " + Integer.parseInt(proxyPort));
			LOG.debug("proxy URL - " + proxyUrl);
			final SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
			final Proxy proxy = new Proxy(Type.HTTP, new InetSocketAddress(proxyUrl, Integer.parseInt(proxyPort)));
			requestFactory.setProxy(proxy);
			restTemplate = new RestTemplate(requestFactory);
		}
		return restTemplate;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String callSpeccheckAPIForCompetitor(final String requestURI) throws CatSpeccheckException
	{
		String jsonString;
		final RestTemplate restTemplate = getRestTemplate(requestURI);

		final HttpHeaders requestHeaders = new HttpHeaders();
		final HttpEntity<?> entity = new HttpEntity(requestHeaders);
		final ResponseEntity<String> responseEntity = restTemplate.exchange(requestURI, HttpMethod.GET, entity, String.class);
		if (responseEntity != null && responseEntity.getBody() != null)
		{
			final ObjectMapper mapper = new ObjectMapper();
			try
			{
				LOG.debug("Response entity Body - \n");
				LOG.debug(responseEntity.getBody());
				final JsonNode actualObj = mapper.readValue(responseEntity.getBody(), JsonNode.class);
				final Object json = mapper.readValue(String.valueOf(actualObj), Object.class);
				jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
			}
			catch (final Exception e)
			{
				LOG.error("Error in service layer while converting response to JSON string" + e.getMessage());
				throw new CatSpeccheckException("Error in service layer while converting response to JSON string", e);
			}
		}
		else
		{
			LOG.error("Response Entity in the service layer is null");
			throw new CatSpeccheckException("Response Entity in the service layer is null");
		}
		return jsonString;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductModel> getSalesModelForCompare(final String categoryCode)
	{
		final Iterator itr = catalogVersionService.getSessionCatalogVersions().iterator();
		String catalogId = "";
		String catalogVersion = "";
		while (itr.hasNext())
		{
			final CatalogVersionModel catalogVer = (CatalogVersionModel) itr.next();
			final CatalogModel catalogModel = catalogVer.getCatalog();
			// Condition to get Product Catalog
			if (!(catalogModel instanceof ContentCatalogModel) && !catalogModel.getDefaultCatalog().booleanValue())
			{
				catalogId = catalogModel.getId();
				catalogVersion = catalogModel.getVersion();
				break;
			}
		}
		return catSpeccheckDao.getSalesModelForCompare(categoryCode, catalogId, catalogVersion);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductModel> getSalesModelForCompare(final CategoryModel category)
	{
		final List<ProductModel> products = new ArrayList<>();
		final List<ProductModel> allProducts = category.getProducts();
		for (final ProductModel productModel : allProducts)
		{
			if (!(productModel instanceof ConfigVariantProductModel) && !(productModel instanceof BaseVariantProductModel))
			{
				products.add(productModel);
			}
		}
		return products;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getSalesModelJson(final List<ProductModel> products) throws JsonMappingException, IOException
	{
		final List<Object> salesModelJson = new ArrayList<>();

		for (final ProductModel product : products)
		{
			final JSONObject obj = new JSONObject();

			obj.put(MACHINE_ID, product.getCode());
			obj.put(MANUFACTURER_ID, CAT_MANUFACTURER_ID);
			obj.put(MANUFACTURER, CATERPILLAR);
			obj.put(MODEL, product.getCode());
			obj.put(DISCONTINUED, DISCONTINUED_VALUE);
			obj.put(TIER_ID, CAT_TIER_ID);
			obj.put(SORT_DISPLAY, DUMMY_VALUES);

			final ObjectMapper mapper = new ObjectMapper();
			final Object json = mapper.readValue(String.valueOf(obj), Object.class);
			salesModelJson.add(json);
		}
		return salesModelJson;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object getProductSpecs(final List<ProductData> products, final String uom) throws JsonMappingException, IOException
	{
		final List<Object> productSpecs = new ArrayList<>();

		final JSONObject machines = new JSONObject();

		machines.put(LABEL, MACHINES);
		machines.put(DATATYPE, DATATYPE_I);

		final JSONObject media = new JSONObject();
		media.put(LABEL, MEDIA);
		media.put(DATATYPE, DATATYPE_M);


		int i = 0;
		final JSONArray machinesDataList = new JSONArray();
		final JSONArray mediaDataList = new JSONArray();

		for (final ProductData productData : products)
		{
			populateMachineJson(productData, machinesDataList, i);
			populateMediaJson(productData, mediaDataList, i);

			media.put(DATA, mediaDataList);
			i++;
		}

		machines.put(DATA, machinesDataList);

		final ObjectMapper mapper = new ObjectMapper();

		final Object machineJson = mapper.readValue(String.valueOf(machines), Object.class);
		productSpecs.add(machineJson);

		final Object mediaJson = mapper.readValue(String.valueOf(media), Object.class);
		productSpecs.add(mediaJson);

		return populateClassificationDataJson(productSpecs, products, uom);
	}



	/**
	 * Populate machine json.
	 *
	 * @param productData
	 *           the product data
	 * @param machinesDataList
	 *           the machines data list
	 * @param coulmnIndex
	 *           the coulmnIndex
	 */
	private void populateMachineJson(final ProductData productData, final JSONArray machinesDataList, final int coulmnIndex)
	{
		final JSONObject machinesData = new JSONObject();

		machinesData.put(COLUMN, String.valueOf(coulmnIndex));

		machinesData.put(MACHINE_ID, productData.getCode());
		machinesData.put(MANUFACTURER_ID, CAT_MANUFACTURER_ID);
		machinesData.put(MANUFACTURER, CATERPILLAR);
		machinesData.put(MODEL, productData.getCode());
		machinesData.put(DISCONTINUED, DISCONTINUED_VALUE);
		machinesData.put(TIER_ID, CAT_TIER_ID);
		machinesData.put(SORT_DISPLAY, DUMMY_VALUES);

		machinesDataList.put(machinesData);

	}


	/**
	 * Populate media json.
	 *
	 * @param productData
	 *           the product data
	 * @param mediaDataList
	 *           the media data list
	 * @param coulmnIndex
	 *           the coulmn index
	 */
	private void populateMediaJson(final ProductData productData, final JSONArray mediaDataList, final int coulmnIndex)
	{
		String imageUrl = StringUtils.EMPTY;
		final JSONObject mediaData = new JSONObject();
		final JSONArray mediaList = new JSONArray();
		final JSONObject mediaImages = new JSONObject();

		if (CollectionUtils.isNotEmpty(productData.getImages()))
		{
			for (final ImageData image : productData.getImages())
			{
				imageUrl = image.getUrl();
			}
		}

		mediaImages.put(FILENAME, imageUrl);
		mediaImages.put(DESCRIPTION, PHOTO);
		mediaImages.put(CONTENT_TYPE, CONTENT_TYPE_JPG);
		mediaImages.put(LANGUAGE_ID, StringUtils.EMPTY);
		mediaList.put(mediaImages);

		mediaData.put(COLUMN + String.valueOf(coulmnIndex), mediaList);

		mediaDataList.put(mediaData);

	}



	/**
	 * Populate classification data json.
	 *
	 * @param productSpecs
	 *           the product specs
	 * @param products
	 *           the products
	 * @param uom
	 *           the uom
	 * @return JSON Object
	 * @throws JsonMappingException
	 *            the json mapping exception
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 */
	private Object populateClassificationDataJson(final List<Object> productSpecs, final List<ProductData> products,
			final String uom) throws JsonMappingException, IOException
	{

		final JSONObject header = new JSONObject();
		final ObjectMapper mapper = new ObjectMapper();

		final ProductData firstProduct = products.get(0);
		final Collection<ClassificationData> classificationData1 = firstProduct.getClassifications();
		for (final ClassificationData classification : classificationData1)
		{

			if (!(UTV_PRODUCT.equalsIgnoreCase(classification.getName())))
			{
				header.put(LABEL, classification.getName());
				header.put(DATATYPE, DATATYPE_H);

				final Object headerJson = mapper.readValue(String.valueOf(header), Object.class);
				productSpecs.add(headerJson);

				if (CollectionUtils.isNotEmpty(classification.getFeatures()))
				{
					populateFeatureData(classification, products, productSpecs, uom);
				}
			}


		}
		return productSpecs;
	}

	/**
	 * Populate feature data.
	 *
	 * @param classification
	 *           the classification
	 * @param products
	 *           the products
	 * @param productSpecs
	 *           the product specs
	 * @param uom
	 *           the uom
	 * @throws JsonMappingException
	 *            the json mapping exception
	 * @throws IOException
	 *            Signals that an I/O exception has occurred.
	 */
	private void populateFeatureData(final ClassificationData classification, final List<ProductData> products,
			final List<Object> productSpecs, final String uom) throws JsonMappingException, IOException
	{

		for (final FeatureData feature : classification.getFeatures())
		{
			if (feature.getCode().contains(uom))
			{
				final JSONObject features = new JSONObject();

				features.put(LABEL, feature.getName());
				features.put(DATATYPE, DATATYPE_A);
				features.put(UNIT, StringUtils.EMPTY);

				final JSONArray dataList = new JSONArray();

				if (CollectionUtils.isNotEmpty(feature.getFeatureValues()))
				{
					populateFeatureValues(products, feature, dataList);
				}

				features.put(DATA, dataList);

				final ObjectMapper mapper = new ObjectMapper();
				final Object featuresJson = mapper.readValue(String.valueOf(features), Object.class);

				productSpecs.add(featuresJson);
			}
		}
	}

	/**
	 * Populate feature values.
	 *
	 * @param products
	 *           the products
	 * @param feature
	 *           the feature
	 * @param dataList
	 *           the data list
	 */
	private void populateFeatureValues(final List<ProductData> products, final FeatureData feature, final JSONArray dataList)
	{
		//Product loop for p2
		int max = products.size();
		int counter = 0;
		while (max > 0)
		{
			final ProductData product = products.get(counter);
			final Collection<ClassificationData> classData = product.getClassifications();
			if (CollectionUtils.isNotEmpty(classData))
			{
				for (final ClassificationData c : classData)
				{
					final Collection<FeatureData> featureData = c.getFeatures();

					populateFeatureValueData(feature, featureData, counter, dataList);
				}
			}
			max--;
			counter++;
		}
	}

	/**
	 * Populate feature value data.
	 *
	 * @param feature
	 *           the feature
	 * @param featureData
	 *           the feature data
	 * @param counter
	 *           the counter
	 * @param dataList
	 *           the data list
	 */
	private void populateFeatureValueData(final FeatureData feature, final Collection<FeatureData> featureData, final int counter,
			final JSONArray dataList)
	{
		for (final FeatureData f : featureData)
		{
			if (StringUtils.equals(feature.getCode(), f.getCode()))
			{
				final JSONObject data = new JSONObject();
				for (final FeatureValueData fvd : f.getFeatureValues())
				{
					data.put(COLUMN + String.valueOf(counter), fvd.getValue());
				}
				dataList.put(data);
			}
		}

	}

}
