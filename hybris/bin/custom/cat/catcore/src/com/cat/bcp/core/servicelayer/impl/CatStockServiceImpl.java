/**
*
*/
package com.cat.bcp.core.servicelayer.impl;


import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;

import com.cat.bcp.core.dao.CatStockDao;
import com.cat.bcp.core.model.OrderWindowModel;
import com.cat.bcp.core.model.RestockAlertModel;
import com.cat.bcp.core.model.ShipToAfterDateModel;
import com.cat.bcp.core.servicelayer.CatStockService;
import com.cat.facades.order.CatShiptoQuantitiesData;
import com.cat.facades.product.order.ShipToAfterDateData;


/**
 * @author avaijapurkar
 *
 */
public class CatStockServiceImpl implements CatStockService
{

	private static final String WHITE_SPACE = " ";
	private static final String HYPHEN = "-";

	@Resource(name = "catStockDao")
	private CatStockDao catStockDao;

	@Autowired
	private ModelService modelService;

	@Autowired
	private FlexibleSearchService flexibleSearchService;

	@Autowired
	private Converter<ShipToAfterDateModel, ShipToAfterDateData> shipTtoAfterDateConverter;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateAlertEntry(final String alertId, final String configProduct, final String orderedQty)
	{
		catStockDao.updateAlertEntry(alertId, configProduct, orderedQty);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RestockAlertModel getStockForSalesModel(final String alertId)
	{
		return catStockDao.getStockForSalesModel(alertId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map getOrderWindowAndShipAfterDates(final List<ProductModel> prodList)
	{
		final OrderWindowModel orderWindow = catStockDao.getOrderWindowAndShipAfterDates(prodList);

		final Map orderWindowMap = new HashMap();
		if (orderWindow != null)
		{
			final List<ShipToAfterDateModel> shipAfterList = new ArrayList(orderWindow.getShipToAfterDate());

			orderWindowMap.put("orderWindow", orderWindow.getName());
			orderWindowMap.put("shipToAfterDates", shipAfterList);
		}
		return orderWindowMap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Boolean isIMAddToCartConfigProduct(final String salesModelId, final ProductModel productModel,
			final B2BCustomerModel customerModel)
	{
		final String alertId = salesModelId + "_" + customerModel.getDefaultB2BUnit().getUid();

		return catStockDao.isIMAddToCartConfigProduct(alertId, productModel);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<CatShiptoQuantitiesData> getShipToDateQty(final List<OrderEntryData> entries)
	{
		final List<CatShiptoQuantitiesData> shipToList = new ArrayList();
		for (final OrderEntryData oed : entries)
		{
			final CatShiptoQuantitiesData catShipToQtyData = new CatShiptoQuantitiesData();
			ShipToAfterDateModel shipToModel = modelService.create(ShipToAfterDateModel.class);
			shipToModel.setShipAfterDate(oed.getShipToAfterDate());
			shipToModel = flexibleSearchService.getModelByExample(shipToModel);
			catShipToQtyData.setQuantity(Integer.valueOf(oed.getQuantity().toString()));
			catShipToQtyData.setShippingWindow(shipTtoAfterDateConverter.convert(shipToModel));
			shipToList.add(catShipToQtyData);
		}
		return shipToList;
	}

	@Override
	public StringBuilder formatShipToAfterDate(final ShipToAfterDateModel shipAfterModel)
	{
		final Date shipFromDate = shipAfterModel.getShipFromDate();
		final Date shipToDate = shipAfterModel.getShipToDate();

		final int fromDatePart = shipFromDate.getDate();
		final int fromMonthPart = shipFromDate.getMonth();
		StringBuilder formattedDate = formatShipToDates(fromDatePart, fromMonthPart, new StringBuilder());
		formattedDate.append(WHITE_SPACE).append(HYPHEN).append(WHITE_SPACE);

		final int toDatePart = shipToDate.getDate();
		final int toMonthPart = shipToDate.getMonth();
		formattedDate = formatShipToDates(toDatePart, toMonthPart, formattedDate);
		return formattedDate;
	}

	public StringBuilder formatShipToDates(final int date, final int month, final StringBuilder formattedDate)
	{
		final String monthName = getMonthName(month);
		if (date % 10 == 0 || (date > 10 && date < 20))
		{
			formattedDate.append(date).append("th");
		}
		else if (date % 10 == 1)
		{
			formattedDate.append(date).append("st");
		}
		else if (date % 10 == 2)
		{
			formattedDate.append(date).append("nd");
		}
		else if (date % 10 == 3)
		{
			formattedDate.append(date).append("rd");
		}
		else if (date % 10 > 3)
		{
			formattedDate.append(date).append("th");
		}

		formattedDate.append(WHITE_SPACE).append(monthName);
		return formattedDate;
	}

	public String getMonthName(final int month)
	{
		String monthName = "";
		final DateFormatSymbols dfs = new DateFormatSymbols();
		final String[] months = dfs.getMonths();
		if (month >= 0 && month <= 11)
		{
			monthName = months[month];
		}
		return monthName;
	}
}