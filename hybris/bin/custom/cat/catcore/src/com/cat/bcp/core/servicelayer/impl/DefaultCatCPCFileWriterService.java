package com.cat.bcp.core.servicelayer.impl;


import com.cat.bcp.core.servicelayer.CatCPCFileWriterService;
import de.hybris.platform.util.CSVWriter;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
/**
 * The class contains the implementations of the @interface{CatCPCFileWriterService }
 */

public class DefaultCatCPCFileWriterService implements CatCPCFileWriterService
{
    private static final Logger LOGGER=Logger.getLogger(DefaultCatCPCFileWriterService.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public  void createCSVFile(Map<String, String> valueMap, CSVWriter csvWriter, String header) throws IOException {
        LOGGER.debug("Start:writing values to csv file");
        csvWriter.write(getCsvData(header));
        List<Map<Integer, String>> rows = new ArrayList<>();
        Map<Integer, String> rowData = null;
        int column = 0;

        for (Map.Entry<String, String> entry : valueMap.entrySet()) {
            MutableInt columnIndex = new MutableInt(0);
            column = columnIndex.intValue();
            rowData = new HashMap<>();
            rowData.put(column++, entry.getKey());
            rowData.put(column, entry.getValue());
            rows.add(rowData);
        }
        csvWriter.write(rows);
        csvWriter.close();
        LOGGER.debug("End:writing values to csv file");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createClassificationAttributeAssignmentFile(Map<String, String> classAttributeAssignmentMap, CSVWriter csvClassificationAttributeAssignmentWriter,String classificationAttributeAssignmentHeader) throws IOException {
        csvClassificationAttributeAssignmentWriter.write(getCsvData(classificationAttributeAssignmentHeader));
        final List<Map<Integer, String>> rows = new ArrayList<>();
        Map<Integer, String> rowData = null;
        int column = 0;

        for (final Map.Entry<String, String> entry : classAttributeAssignmentMap.entrySet())
        {
            final MutableInt columnIndex = new MutableInt(0);
            column = columnIndex.intValue();
            rowData = new HashMap<>();
            rowData.put(column++, entry.getValue());
            rowData.put(column, entry.getKey());
            rows.add(rowData);
        }
        csvClassificationAttributeAssignmentWriter.write(rows);
        csvClassificationAttributeAssignmentWriter.close();


    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createClassificationProductCsvFile(Map<String, Map<String, String>> productValueMap, CSVWriter productValueMapWriter, String productValueHeader) throws IOException {
        LOGGER.debug("Start:createClassificationProductCsvFile->Writing to files updating the products with attribute values");
        productValueMapWriter.write(getCsvData(productValueHeader));
        List<Map<Integer, String>> rows = new ArrayList<>();
        Map<Integer, String> rowData = null;
        int column = 0;

        for (Map.Entry<String, Map<String, String>> entry : productValueMap.entrySet()) {

            Map<String, String> classificationAttributeValueMap = entry.getValue();
            for (Map.Entry<String, String> classificationAttributeEntry : classificationAttributeValueMap.entrySet()) {
                MutableInt columnIndex = new MutableInt(0);
                column = columnIndex.intValue();
                rowData = new HashMap<>();
                rowData.put(column, entry.getKey());
                rowData.put(++column, String.valueOf("@").concat(classificationAttributeEntry.getKey().concat("*").concat(classificationAttributeEntry.getValue())));
                rows.add(rowData);
            }
        }
        productValueMapWriter.write(rows);
        productValueMapWriter.close();
        LOGGER.debug("End:createClassificationProductCsvFile->Writing to files updating the products with attribute values");

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createCSVFileForDataCleanup(List<String> classificationCleanUpDataList, CSVWriter classificationDataCleanupWriter, String classificationDataRemovalHeader) throws IOException {
        LOGGER.debug("Start:createCSVFileForDataCleanup->Writing to files for the classfication data cleanup");
        classificationDataCleanupWriter.write(getCsvData(classificationDataRemovalHeader));
        List<Map<Integer, String>> rows = new ArrayList<>();
        Map<Integer, String> rowData = null;
        int column = 0;

        for (String classificationData : classificationCleanUpDataList) {
            MutableInt columnIndex = new MutableInt(0);
            column = columnIndex.intValue();
            rowData = new HashMap<>();
            rowData.put(column, classificationData);
            rows.add(rowData);

        }
        classificationDataCleanupWriter.write(rows);
        classificationDataCleanupWriter.close();
        LOGGER.debug("End:createCSVFileForDataCleanup->Writing to files for the classfication data cleanup");

    }


    @Override
    public  Map<Integer, String> getCsvData(String headerArray) {
        Map<Integer, String> header = new HashMap<>();
        int columnIndex = 0;
        for (String headerContent : headerArray.split(Pattern.quote("|"))) {
            header.put(columnIndex++, headerContent);
        }
        return header;
    }

    @Override
    public CSVWriter getCsvWriter(String path, char separator) throws IOException {

    File file = new File(path);
    CSVWriter csvClassificationWriter = new CSVWriter(file,"UTF-8");
    csvClassificationWriter.setFieldseparator(separator);
    csvClassificationWriter.setLinebreak("\n");
    return csvClassificationWriter;
}

    @Override
    public void createUnclassifiedCSVFileForDataCleanup(List<List<String>> unclassifiedCleanUpDataList, CSVWriter unclassifiedAttributeCleanupWriter, String classificationDataRemovalHeader) throws IOException {
        LOGGER.debug("Start:createUnclassifiedCSVFileForDataCleanup->Writing to files for the unclassified attribute  cleanup");
        unclassifiedAttributeCleanupWriter.write(getCsvData(classificationDataRemovalHeader));
        List<Map<Integer, String>> rows = new ArrayList<>();
        Map<Integer, String> rowData = null;
        int column = 0;
        for (List<String> unclassifiedCleanUpData : unclassifiedCleanUpDataList) {
            MutableInt columnIndex = new MutableInt(0);
            column = columnIndex.intValue();
            rowData = new HashMap<>();
            rowData.put(column++, unclassifiedCleanUpData.get(0));
            rowData.put(column, unclassifiedCleanUpData.get(1));
            rows.add(rowData);
        }
        unclassifiedAttributeCleanupWriter.write(rows);
        unclassifiedAttributeCleanupWriter.close();
        LOGGER.debug("End:createUnclassifiedCSVFileForDataCleanup->Writing to files for the unclassified attribute  cleanup");
    }




}
