package com.cat.bcp.core.servicelayer.impl;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.CatCPCProductDao;
import com.cat.bcp.core.exception.CatCoreException;
import com.cat.bcp.core.job.CatCPCAbstractAbortableCronJob;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.CpcCronJobModel;
import com.cat.bcp.core.servicelayer.CatCPCFileWriterService;
import com.cat.bcp.core.servicelayer.CatCPCProductService;
import com.cat.bcp.core.servicelayer.CatCreateCSVWriterService;
import de.hybris.platform.classification.ClassificationService;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.jalo.AbortCronJobException;
import de.hybris.platform.util.CSVWriter;
import de.hybris.platform.util.Config;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.annotation.Resource;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URL;
import java.util.*;

import static com.cat.bcp.core.constants.CatCoreConstants.*;
import static com.cat.bcp.core.servicelayer.impl.DefaultCatCreateCSVWriterService.CSV;

/**
 * The class contains the method of Interface CatCPCProductService
 */
public class DefaultCatCPCProductService implements CatCPCProductService {

    private static final Logger LOGGER = Logger.getLogger(DefaultCatCPCProductService.class.getName());

    public static final String US_METRIC = "_US_METRIC";
    public static final String METRIC = "_METRIC";
    public static final String TITLE = "title";
    public static final String SPEC = "spec";
    public static final String ID = "id";
    public static final String SHORT_DESCRIPTION = "short_description";
    public static final String TYPE = "type";
    public static final String SPECIFICATION_TYPE_MEASUREMENT = "measurement";
    public static final String SPECIFICATION_TYPE_TEXT = "text";
    public static final String US_UNIT_OF_MEASUREMENT = "english";
    public static final String METRIC_UNIT_OF_MEASUREMENT = "metric";
    public static final String CONCATENATE_SYMBOL = "_";
    public static final String US = "_US";
    public static final String UTV_PROD = "UTV_PROD";

    @Resource(name = "catCPCProductDao")
    CatCPCProductDao catCPCProductDao;

    @Resource(name="catCreateCSVWriterService")
    CatCreateCSVWriterService catCreateCSVWriterService;

    @Resource(name="catCPCFileWriterService")
    CatCPCFileWriterService catCPCFileWriterService;

    @Resource(name = "classificationService")
    ClassificationService classificationService;

    @Resource(name="baseDirectoryCat")
    String baseDirectoryCat;

    /**
     *
     * {@inheritDoc}
     */
    @Override
    public void productSpecificationService(CatCPCAbstractAbortableCronJob catAbstractAbortableCronJob, CpcCronJobModel cpcCronJobModel)
            throws CatCoreException {

        Map<String, String> classificationClassMap = new HashMap<>();
        Map<String, String> classificationAttributeMap = new HashMap<>();
        Map<String, String> classAttributeAssignmentMap = new HashMap<>();
        Map<String, String> productDescriptionMap = new HashMap<>();
        Map<String, String> categoryRelationMap = new HashMap<>();
        Map<String, Map<String, String>> productValueMap = new HashMap<>();
        try {

            List<BaseVariantProductModel> baseVariantProductModelList =
                    catCPCProductDao.getProductforSpecificationRecords(cpcCronJobModel.getProductFamilies());
            baseVariantProductModelList = getListOfProductsWithCpcGroupID(baseVariantProductModelList);
            LOGGER.debug("total base variants with cpc group id and product id:" + baseVariantProductModelList.size());
            for (BaseVariantProductModel baseVariantProductModel : baseVariantProductModelList) {
                LOGGER.debug("CPC parsing happening for the base variant:" + baseVariantProductModel.getCode());
                catAbstractAbortableCronJob.checkIfRequestedToAbortCron(cpcCronJobModel);

                Map<String, String> productAttributeValueMap = new HashMap<>();

                Document doc = getDocumentAfterFormingUrl(baseVariantProductModel.getBaseProduct());
                doc.getDocumentElement().normalize();

                String productDescription = getProductDescription(doc);
                addDescriptionToProductDescriptionMap(productDescription, baseVariantProductModel, productDescriptionMap);

                final NodeList productNodeList = doc.getElementsByTagName(CatCoreConstants.SPECIFICATIONS);
                LOGGER.debug("total no of specification records:" + productNodeList.getLength());
                //iterating the specifications
                for (int temp = 0; temp < productNodeList.getLength(); temp++) {
                    final NodeList primaryContentNodeList = getContent(temp, productNodeList);

                    //iterating the specs
                    LOGGER.debug("End :productSpecificationService-> Creating specification record data map");
                    updateSpecsValues(classificationClassMap, classificationAttributeMap, classAttributeAssignmentMap, categoryRelationMap, productAttributeValueMap, primaryContentNodeList);
                    LOGGER.debug("End :productSpecificationService-> Creating specification record data map");

                }
                productValueMap.put(baseVariantProductModel.getCode(), productAttributeValueMap);

                clearProductFeatureValues(baseVariantProductModel);
            }
            if (!baseVariantProductModelList.isEmpty()) {
                DefaultCatCreateCSVWriterService cpcCSVFileCreator = catCreateCSVWriterService.createCSVWriter();
                CSVWriter csvClassificationWriter = cpcCSVFileCreator.getCsvClassificationWriter();
                CSVWriter categoryRelationshipsWriter = cpcCSVFileCreator.getCategoryRelationshipsWriter();
                CSVWriter csvClassificationAttributeWriter = cpcCSVFileCreator.getCsvClassificationAttributeWriter();
                CSVWriter csvClassificationAttributeAssignmentWriter = cpcCSVFileCreator.getCsvClassificationAttributeAssignmentWriter();
                CSVWriter productDescriptionWriter = cpcCSVFileCreator.getProductDescriptionWriter();
                CSVWriter productValueMapWriter = cpcCSVFileCreator.getProductValueMapWriter();
                catCPCFileWriterService.createCSVFile(classificationClassMap, csvClassificationWriter, CatCoreConstants.CLASSIFICATION_CLASS_HEADER_ARRAY);
                catCPCFileWriterService.createCSVFile(categoryRelationMap, categoryRelationshipsWriter, CatCoreConstants.CATEGORY_RELATION_HEADER);
                catCPCFileWriterService.createCSVFile(classificationAttributeMap, csvClassificationAttributeWriter, CatCoreConstants.CLASSIFICATION_CLASS_HEADER_ARRAY);
                catCPCFileWriterService.createClassificationAttributeAssignmentFile(classAttributeAssignmentMap, csvClassificationAttributeAssignmentWriter, CatCoreConstants.CLASSIFICATION_CLASS_HEADER_ARRAY);
                catCPCFileWriterService.createCSVFile(productDescriptionMap, productDescriptionWriter, CatCoreConstants.PRODUCT_DESCRIPTION_HEADER);
                catCPCFileWriterService.createClassificationProductCsvFile(productValueMap, productValueMapWriter, CatCoreConstants.PRODUCT_VALUE_HEADER);


                List<String> existingUtvNewProductClassificationClassList = catCPCProductDao.getExistingCategoryForUTV();

                LOGGER.debug("existing classification list:" + existingUtvNewProductClassificationClassList.toString());

                List<String> classificationClassRemovalList = getClassficationDataToBeCleaned(existingUtvNewProductClassificationClassList, classificationClassMap);

                Set<String> totalAttributeToBeRemoved = new HashSet<>();
                if (CollectionUtils.isNotEmpty(classificationClassRemovalList)) {

                    List<String> classificationAttributeToBeRemoved = catCPCProductDao.getListOfClassificationAttribute(classificationClassRemovalList);
                    addAttributeToRemovalSet(totalAttributeToBeRemoved, classificationAttributeToBeRemoved);
                }


                List<String> existingClassificationAttribute = catCPCProductDao.getListOfClassificationAttribute(existingUtvNewProductClassificationClassList);
                removeClassificationAttributeData(classificationAttributeMap, totalAttributeToBeRemoved, existingClassificationAttribute);
            } else {
                LOGGER.warn("No product found with cpcGroup ID and CPC Product ID");
            }
        } catch (final IOException ioException)
        {
            throw new CatCoreException("Error occurred during I/O operation while creating csv files.", ioException);
        } catch (AbortCronJobException abortCronJobException) {
                throw new CatCoreException("CPC cron job aborted due to errors.", abortCronJobException);
        }
    }



    /**
     * The method returns the list of base variant model with the valid cpc group id
     * @param baseVariantProductModelList the list containing the base variant model for the utv
     * @return List<BaseVariantProductModel>
     */
    private List<BaseVariantProductModel> getListOfProductsWithCpcGroupID(List<BaseVariantProductModel> baseVariantProductModelList) {
       List<BaseVariantProductModel> listOfProductsWithValidCPCId=new ArrayList<>();
        for(BaseVariantProductModel variant:baseVariantProductModelList){
            ProductModel product=variant.getBaseProduct();
            if(product.getCpcGroupId()!=null && product.getCpcProductId()!=null){
                listOfProductsWithValidCPCId.add(variant);
            }
        }
        return listOfProductsWithValidCPCId;
    }

    private void updateSpecsValues(Map<String, String> classificationClassMap, Map<String, String> classificationAttributeMap, Map<String, String> classAttributeAssignmentMap, Map<String, String> categoryRelationMap, Map<String, String> productAttributeValueMap, NodeList primaryContentNodeList) {
        for (int tem = 0; tem < primaryContentNodeList.getLength(); tem++) {
            final Node primaryContentNode = primaryContentNodeList.item(tem);
            final Element primaryContentElement = (Element) primaryContentNode.getChildNodes();

            createClassificationAttributeAssignment(primaryContentElement, classificationAttributeMap, productAttributeValueMap,
                    classAttributeAssignmentMap);

            //iterating the nodes of individual specs like title,name-text,value
            createClassificationaAndCategoryRelationship(classificationClassMap, categoryRelationMap, primaryContentElement);

        }

    }

    /**
     * The method creates the map for classification Classes then creates the map to create the relationship between the classification classes and categories
     * @param classificationClassMap the map containing the key value pairs for the classification class
     * @param categoryRelationMap the map containing the key value pairs to create the relationship between classificationClasses and category
     * @param primaryContentElement the current spec id element from the cpc xml
     */
    private void createClassificationaAndCategoryRelationship(Map<String, String> classificationClassMap, Map<String, String> categoryRelationMap, Element primaryContentElement) {
        final NodeList specChildNodeList = primaryContentElement.getChildNodes();
        for (int specChildNodesCounter = 0; specChildNodesCounter < specChildNodeList.getLength(); specChildNodesCounter++) {
            final String primaryNode = specChildNodeList.item(specChildNodesCounter).getNodeName();
            if (TITLE.equals(primaryNode)) {
                final Element element = (Element) specChildNodeList.item(specChildNodesCounter);
                String titleID = element.getAttributes().getNamedItem(ID).getTextContent();
                titleID = titleID.concat(CONCATENATE_SYMBOL).concat(element.getTextContent().replaceAll(" +","_"));
                classificationClassMap.put(titleID, element.getTextContent());
                categoryRelationMap.put(titleID, CatCoreConstants.UTV_CATEGORY);
            }
        }
    }

    /**
     * The method used to add the classificationAttribute to be removed from the database to @param totalAttributeToBeRemoved
     * @param totalAttributeToBeRemoved contains final set of  the classificationAttribute to be removed
     * @param classificationAttributeToBeRemoved  list containing the attributes to be removed
     */
    private static  void addAttributeToRemovalSet(Set<String> totalAttributeToBeRemoved, List<String> classificationAttributeToBeRemoved) {

        totalAttributeToBeRemoved.addAll(classificationAttributeToBeRemoved);

    }

    /**
     * The method used to add the product description from the cpc xml to the @param productDescriptionMap
     * @param productDescription the description to be added to the map for the base model
     * @param productModel the model for which the description is added
     * @param productDescriptionMap the map containing key as base model and value as description fo the product
     */

    private void addDescriptionToProductDescriptionMap(String productDescription, BaseVariantProductModel productModel, Map<String, String> productDescriptionMap) {
        if (StringUtils.isNotBlank(productDescription)) {
            productDescriptionMap.put(productModel.getCode(), productDescription);
        }
    }

    /**
     *The method returns the description of the product after parsing the document
     * @param doc the document to be parsed for getting the product description
     * @return the description corresponding to the product
     */

    private static String getProductDescription(Document doc) {

        NodeList descriptionPropertyNodelist = doc.getElementsByTagName(CatCoreConstants.PROPERTY);

        for (int propertyNodeCounter = 0; propertyNodeCounter < descriptionPropertyNodelist.getLength(); propertyNodeCounter++) {

            Element element = (Element) descriptionPropertyNodelist.item(propertyNodeCounter);
            String productDescriptionType = element.getAttributes().getNamedItem(TYPE).getTextContent();
            if (SHORT_DESCRIPTION.equals(productDescriptionType)) {
                return element.getTextContent();

            }

        }
        return StringUtils.EMPTY;
    }

    /**
     * The method updates the different classification data for spec type as text
     * @param primaryContentElement  the current specs element
     * @param classificationAttributeMap the map containing the classification attributes
     * @param productAttributeValueMap the map containing the classificationAttribute along with its value corresponding to products
     * @param classAttributeAssignmentMap the map containing the attribute assignment corresponding to classification classes
     */
    private void updateClassificationDataMapforText(Element primaryContentElement, Map<String, String> classificationAttributeMap,
                                                    Map<String, String> productAttributeValueMap, Map<String, String> classAttributeAssignmentMap) {
        NodeList specChildNodeList = primaryContentElement.getChildNodes();
        String titleID = StringUtils.EMPTY;
        String elementTitleIDValue=StringUtils.EMPTY;
        for (int specChildNodesCounter = 0; specChildNodesCounter < specChildNodeList.getLength(); specChildNodesCounter++) {
            final String primaryNode = getNodeName(specChildNodeList, specChildNodesCounter);
            final Element element = (Element) specChildNodeList.item(specChildNodesCounter);

            switch (primaryNode) {
                case TITLE:
                    titleID = getIdForElementAttribute(element);
                    elementTitleIDValue=getTextContent(element);
                    break;

                case "name":
                    classificationAttributeMap.put(getIdForElementTypeAttributes(primaryContentElement).concat("_").concat(titleID).concat(US_METRIC),element.getTextContent() );
                    classAttributeAssignmentMap.put(getIdForElementTypeAttributes(primaryContentElement).concat("_").concat(titleID).concat(US_METRIC), titleID.concat("_").concat(elementTitleIDValue.replaceAll(" +","_")));
                    break;

                case "value-text":
                    productAttributeValueMap.put(getIdForElementTypeAttributes(primaryContentElement).concat("_").concat(titleID).concat(US_METRIC), getTextContent(element));
                    break;

                default:break;

            }


        }
    }

    /**
     * the method is used to return the node name of the spec child attributes
     * @param specChildNodeList get the node corresponding to the specChildNodesCounter
     * @param specChildNodesCounter the iteration current value
     * @return the node name of the spec child attributes
     */
    private static String getNodeName(NodeList specChildNodeList, int specChildNodesCounter) {
        return specChildNodeList.item(specChildNodesCounter).getNodeName();
    }

    /**
     * The method updates the different classification data for spec type as measurement
     * @param primaryContentElement  the current specs element
     * @param classificationAttributeMap the map containing the classification attributes
     * @param productAttributeValueMap the map containing the classificationAttribute along with its value corresponding to products
     * @param classAttributeAssignmentMap the map containing the attribute assignment corresponding to classification classes
     */
    private void updateClassificationDataMapforMeasurement(Element primaryContentElement, Map<String, String> classificationAttributeMap,
                                                           Map<String, String> productAttributeValueMap, Map<String, String> classAttributeAssignmentMap) {
        NodeList specChildNodeList = primaryContentElement.getChildNodes();
        String attributeName = null;
        String titleID = StringUtils.EMPTY;
        String elementTitleIDValue=StringUtils.EMPTY;
        List<String> attributes=new ArrayList<>();
        List<Map<String, String>> classificationDataMap = getClassificationDataMap(classificationAttributeMap, productAttributeValueMap, classAttributeAssignmentMap);
        for (int specChildUnitNodesCounter = 0; specChildUnitNodesCounter < specChildNodeList.getLength(); specChildUnitNodesCounter++) {
            final String measurementNode = getNodeName(specChildNodeList, specChildUnitNodesCounter);
            final Element element = (Element) specChildNodeList.item(specChildUnitNodesCounter);

            switch (measurementNode) {
                case TITLE:
                    titleID = getIdForElementAttribute(element);
                   elementTitleIDValue=getTextContent(element);
                    break;

                case "name":
                    attributeName = getTextContent(element);
                    break;

                case "value":
                    attributes.add(attributeName);
                    attributes.add(titleID);
                    attributes.add(elementTitleIDValue);
                    updateClassificationDataMapforUnitOfMeasurement(primaryContentElement,classificationDataMap,attributes,element);
                    break;
                default:break;
            }
        }


    }

    /**
     * The method updates the classification data map for the different units of measurement like US,METRIC
     * @param primaryContentElement the current  spec id
     * @param classificationDataMap the map containing the list of classificationAttributeMap,productAttributeValueMap,classAttributeAssignmentMap
     * @param attributes the list containing the attributes "attributeName","titleID","elementTitleIDValue"
     * @param element  the child of spec ID.
     */

    private void updateClassificationDataMapforUnitOfMeasurement(Element primaryContentElement, List<Map<String, String>> classificationDataMap,List<String> attributes , Element element) {
        String unitOfMeasurement = element.getAttributes().getNamedItem(TYPE).getTextContent();
        if (US_UNIT_OF_MEASUREMENT.equals(unitOfMeasurement)) {
            updateClassificationMapForEngishUnitOfMeasurement(primaryContentElement,classificationDataMap, attributes, element);

        } else if ((METRIC_UNIT_OF_MEASUREMENT).equals(unitOfMeasurement)) {
            updateClassificationMapForMetricUnitOfMeasurement(primaryContentElement,classificationDataMap, attributes, element);
        }

    }

    /**
     * Updates the classification data map for US units of measurement
     * @param primaryContentElement the current  spec id
     * @param classificationDataMap the map containing the list of classificationAttributeMap,productAttributeValueMap,classAttributeAssignmentMap
     * @param attributes the list containing the attributes "attributeName","titleID","elementTitleIDValue"
     * @param element  the child of spec ID.
     */
    private void updateClassificationMapForMetricUnitOfMeasurement(Element primaryContentElement,List<Map<String,String>> classificationDataMap, List<String> attributes, Element element) {
        Map<String,String> classificationAttributeMap=classificationDataMap.get(0);
        Map<String,String> productAttributeValueMap=classificationDataMap.get(1);
        Map<String,String> classAttributeAssignmentMap=classificationDataMap.get(2);
        String attributeName=attributes.get(0);
        String titleID=attributes.get(1);
        String elementTitleIDValue=attributes.get(2);
        classificationAttributeMap.put(getIdForElementTypeAttributes(primaryContentElement).concat(CONCATENATE_SYMBOL).concat(titleID).concat(METRIC), attributeName);
        Element unitNode = (Element) element.getNextSibling();
        productAttributeValueMap.put(getIdForElementTypeAttributes(primaryContentElement).concat(CONCATENATE_SYMBOL).concat(titleID).concat(METRIC),
                getTextContent(element).concat(("null").equals(getTextContent(unitNode)) ? StringUtils.EMPTY : getTextContent(unitNode)));
        classAttributeAssignmentMap.put(getIdForElementTypeAttributes(primaryContentElement).concat(CONCATENATE_SYMBOL).concat(titleID).concat(METRIC),titleID.concat(CONCATENATE_SYMBOL).concat(elementTitleIDValue.replaceAll(" +","_")));
    }

    /**
     *Updates the classification data map for US units of measurement
     * @param primaryContentElement the current  spec id
     * @param classificationDataMap the map containing the list of classificationAttributeMap,productAttributeValueMap,classAttributeAssignmentMap
     * @param attributes the list containing the attributes "attributeName","titleID","elementTitleIDValue"
     * @param element  the child of spec ID.
     */
    private void updateClassificationMapForEngishUnitOfMeasurement(Element primaryContentElement,List<Map<String,String>> classificationDataMap, List<String> attributes, Element element) {
        Map<String,String> classificationAttributeMap=classificationDataMap.get(0);
        Map<String,String> productAttributeValueMap=classificationDataMap.get(1);
        Map<String,String> classAttributeAssignmentMap=classificationDataMap.get(2);
        String attributeName=attributes.get(0);
        String titleID=attributes.get(1);
        String elementTitleIDValue=attributes.get(2);
        classificationAttributeMap.put(getIdForElementTypeAttributes(primaryContentElement).concat(CONCATENATE_SYMBOL).concat(titleID).concat(US), attributeName);
        Element unitNode = (Element) element.getNextSibling();
        productAttributeValueMap.put(getIdForElementTypeAttributes(primaryContentElement).concat(CONCATENATE_SYMBOL).concat(titleID).concat(US), getTextContent(element).concat(("null").equals(getTextContent(unitNode)) ? StringUtils.EMPTY : getTextContent(unitNode)));
        classAttributeAssignmentMap.put(getIdForElementTypeAttributes(primaryContentElement).concat(CONCATENATE_SYMBOL).concat(titleID).concat(US), titleID.concat(CONCATENATE_SYMBOL).concat(elementTitleIDValue.replaceAll(" +","_")));
    }

    /**
     * The method returns the id of the element
     * @param element the element
     * @return the id corresponding to the element
     */
    private static String getIdForElementAttribute(Element element) {
        return getIdForElementTypeAttributes(element);
    }

    /**
     *
     * The method updates the different classification data for spec type as measurement
     * @param primaryContentElement  the current specs element
     * @param classificationAttributeMap the map containing the classification attributes
     * @param productAttributeValueMap the map containing the classificationAttribute along with its value corresponding to products
     * @param classAttributeAssignmentMap the map containing the attribute assignment corresponding to classification classes
     */
    private void createClassificationAttributeAssignment(Element primaryContentElement, Map<String, String> classificationAttributeMap,
                                                         Map<String, String> productAttributeValueMap, Map<String, String> classAttributeAssignmentMap) {

        String specificationType = primaryContentElement.getAttribute(TYPE);

         if(SPECIFICATION_TYPE_TEXT.equals(specificationType)) {

             updateClassificationDataMapforText(primaryContentElement, classificationAttributeMap, productAttributeValueMap, classAttributeAssignmentMap);
         }
            else if(SPECIFICATION_TYPE_MEASUREMENT.equals(specificationType)){
                updateClassificationDataMapforMeasurement(primaryContentElement, classificationAttributeMap, productAttributeValueMap, classAttributeAssignmentMap);
        }
    }

    /**
     * The method returns the value of the element
     * @param element the element
     * @return the value of the element
     */
    private static String getTextContent(Element element) {
        return element.getTextContent();
    }

    /**]
     * The method returns the value of the tag id corresponding to the element
     * @param element the element
     * @return the value of the tag id corresponding to the element
     */
    private static String getIdForElementTypeAttributes(Element element) {
        return element.getAttributes().getNamedItem(ID).getTextContent();
    }


    /**
     * The method creates the node list corresponding to spec
     * @param temp the counter
     * @param productNodeList the node list
     * @return the NodeList corresponding to spec
     */

    private static NodeList getContent(final int temp, final NodeList productNodeList)
    {
        final Node productNode = productNodeList.item(temp);
        final Element productElement = (Element) productNode;
        return productElement.getElementsByTagName(SPEC);
    }


    /**
     * The method returns the list of classification data to be removed from database.
     * @param existingClassificationDataList the existing classification data to be compared
     * @param classificationDataMap the classificationClassMap the map containing the data from the parsed xml of the product
     * @return the list of classification data to be cleaned
     */

    @Override
    public  List<String> getClassficationDataToBeCleaned(List<String> existingClassificationDataList, Map<String, String> classificationDataMap) {

        List<String> classificationDataRemovalList = new ArrayList<>();
        for (String classificationData : existingClassificationDataList) {
            boolean matched = false;
            for (String key : classificationDataMap.keySet()) {
                if (classificationData.equals(key)) {
                    matched = true;
                    break;
                }

            }

            if (!matched) {
                classificationDataRemovalList.add(classificationData);
            }
        }
        return classificationDataRemovalList;
    }


    /**
     * Returns the doc to be parsed.
     * @param productModel the product model
     * @return doc to be parsed
     * @throws ParserConfigurationException when there is error due to configuration
     * @throws IOException if there is an error during I/O operation
     * @throws SAXException there is an issue during parsing
     */

    @Override
    public Document getDocumentAfterFormingUrl(ProductModel productModel) throws CatCoreException {

        try {
            final DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();

            f.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

            final DocumentBuilder db = f.newDocumentBuilder();
            return db.parse(new URL(CatCoreConstants.CPC_XML_URL + productModel.getCpcGroupId()
                    + Config.getParameter(CatCoreConstants.CAT_PRODUCT_MEDIA_XML_SEPERATOR) + productModel.getCpcProductId()
                    + Config.getParameter(CatCoreConstants.CAT_PRODUCT_MEDIA_XML_EXTENSION)).openStream());

        } catch (final ParserConfigurationException parserConfigurationException) {
            throw new CatCoreException("getDocumentAfterFormingUrl ->Configuration issue occurred during parsing cpc xml.", parserConfigurationException);
        } catch (final SAXException saxException) {
            throw new CatCoreException("getDocumentAfterFormingUrl->Error occurred during parsing cpc xml.", saxException);
        } catch (final IOException ioException) {
            throw new CatCoreException("getDocumentAfterFormingUrl->Error occurred during I/O operation.", ioException);
        }
    }





    /**
     * The method used to clear all the classification values for the product
     * @param baseVariantProductModel  the base model for which the values needs to be cleared.
     */
    private void clearProductFeatureValues(final BaseVariantProductModel baseVariantProductModel)
    {
        LOGGER.debug("Start:clearProductFeatureValues->Started clearing the feature values");
        final FeatureList featureList = classificationService.getFeatures(baseVariantProductModel);


        if (featureList != null && !featureList.getFeatures().isEmpty())
        {
            for (final Feature feature : featureList.getFeatures())
            {
                final String featureCode = feature.getCode();
                if ((featureCode != null)
                        && !(featureCode.contains(UTV_PROD)))
                {
                    final List<FeatureValue> featureValues =  feature.getValues();

                    clearFeatureValues(featureValues);

                }
            }
        }
        classificationService.setFeatures(baseVariantProductModel, featureList);
        LOGGER.debug("END:clearProductFeatureValues->Finished clearing the feature values");

    }


    /**
     * The method is used to clear the future values
     * @param featureValues the values for the features
     */
    private void clearFeatureValues(List<FeatureValue> featureValues)
    {
        if (featureValues != null && !featureValues.isEmpty())
        {
            for (final FeatureValue featureValue : featureValues)
            {
                featureValue.setValue(StringUtils.EMPTY);
            }
        }

    }




    /**
     * The method returns the the list of classificationAttributeMap,productAttributeValueMap,classAttributeAssignmentMap
     *@param classificationAttributeMap the map containing the classification attributes
     * @param productAttributeValueMap the map containing the classificationAttribute along with its value corresponding to products
     * @param classAttributeAssignmentMap the map containing the attribute assignment corresponding to classification classes
     * @return the list of classificationAttributeMap,productAttributeValueMap,classAttributeAssignmentMap
     */

    private  static List<Map<String, String>> getClassificationDataMap(Map<String, String> classificationAttributeMap, Map<String, String> productAttributeValueMap, Map<String, String> classAttributeAssignmentMap) {
        List<Map<String,String>> classificationDataMap=new ArrayList<>();
        classificationDataMap.add(classificationAttributeMap);
        classificationDataMap.add(productAttributeValueMap);
        classificationDataMap.add(classAttributeAssignmentMap);
        return classificationDataMap;
    }


    private void removeClassificationAttributeData(Map<String, String> classificationAttributeMap, Set<String> totalAttributeToBeRemoved, List<String> existingClassificationAttribute) throws IOException {
        List<String> classificationAttributeRemovalList = getClassficationDataToBeCleaned(existingClassificationAttribute, classificationAttributeMap);
        addAttributeToRemovalSet(totalAttributeToBeRemoved, classificationAttributeRemovalList);
        if (CollectionUtils.isNotEmpty(totalAttributeToBeRemoved)) {
            List<String> totalAttributeRemovalList = new ArrayList<>();
            totalAttributeRemovalList.addAll(totalAttributeToBeRemoved);

            CSVWriter classificationAttributeRemovalWriter = catCPCFileWriterService.getCsvWriter(baseDirectoryCat.concat("/").concat(Config.getString("cat.hotfolder.classification.attribute.removal", "classification-attribute_removal")).concat(CSV), CatCoreConstants.CPCSEPARATOR);
            catCPCFileWriterService.createCSVFileForDataCleanup(totalAttributeRemovalList, classificationAttributeRemovalWriter, CatCoreConstants.CLASSIFICATION_DATA_REMOVAL_HEADER);
            List<List<String>> totalUnClassifiedAttributeRemovalList = catCPCProductDao.getListOfUnClassifiedAttribute(totalAttributeRemovalList);
            if (CollectionUtils.isNotEmpty(totalUnClassifiedAttributeRemovalList)) {
                CSVWriter unClassifiedAttributeRemovalWriter = catCPCFileWriterService.getCsvWriter(baseDirectoryCat.concat("/").concat(Config.getString("cat.hotfolder.classification.unclassified.attribute.removal", "unclassified-attribute_removal")).concat(CSV), CatCoreConstants.CPCSEPARATOR);
                catCPCFileWriterService.createUnclassifiedCSVFileForDataCleanup(totalUnClassifiedAttributeRemovalList, unClassifiedAttributeRemovalWriter, CatCoreConstants.UNCLASSIFIED_DATA_REMOVAL_HEADER);
            }

        }


    }

}
