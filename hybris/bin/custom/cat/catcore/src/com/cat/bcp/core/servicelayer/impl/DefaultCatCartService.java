/**
 *
 */
package com.cat.bcp.core.servicelayer.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.impl.DefaultCartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.search.SearchResult;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.dao.CatCartDao;
import com.cat.bcp.core.model.CatAdditionalInfoModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.FBCPartsOrderInfoModel;
import com.cat.bcp.core.servicelayer.CatCartService;
import com.cat.core.integration.fbc.request.Selection;
import com.cat.facades.order.PurchaseOrderCheck;


/**
 * @author sparkibanda This class provides implementation for the Cart interface with methods to modify/remove cart
 */
public class DefaultCatCartService extends DefaultCartService implements CatCartService
{

	private CatCartDao catCartDao;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Resource(name = "productService")
	private ProductService productService;

	private static final Logger LOG = Logger.getLogger(DefaultCatCartService.class);
	private static final String SELECTIONS = "selections";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PurchaseOrderCheck doPOListCheck(final List<String> purchaseOrderNumberList, final B2BCustomerModel customerModel)
	{
		return getCatCartDao().checkPOList(purchaseOrderNumberList, customerModel);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clearCart()
	{
		final CartModel cartModel = this.getSessionCart();
		if (null != cartModel)
		{
			if (CollectionUtils.isNotEmpty(cartModel.getEntries()))
			{
				cartModel.setEntries(Collections.emptyList());
			}
			cartModel.setLaneType(null);
			modelService.save(cartModel);
			if (sessionService.getAttribute(CatCoreConstants.REORDER_PO_LIST) != null)
			{
				sessionService.removeAttribute(CatCoreConstants.REORDER_PO_LIST);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeCarts(final List<CartModel> cartList)
	{
		for (final CartModel cart : cartList)
		{
			modelService.remove(cart);
		}
		this.removeSessionCart();
	}

	/**
	 * This method is used to fetch Cat Additional Info(Used for EPP/CSA drop-down options)
	 *
	 * @param type
	 * @return List of CatAdditionalInfoModel
	 */
	@Override
	public List<CatAdditionalInfoModel> getCatAdditionalInfo(final String type)
	{
		return catCartDao.getCatAdditionalInfo(type);
	}

	/**
	 * @return the catCartDao
	 */
	public CatCartDao getCatCartDao()
	{
		return catCartDao;
	}

	/**
	 * @param catCartDao
	 *           the catCartDao to set
	 */
	public void setCatCartDao(final CatCartDao catCartDao)
	{
		this.catCartDao = catCartDao;
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public void updateCartEntryPONumbers(final AbstractOrderEntryModel cartEntryModel, final Set<String> orderEntryPOList,
			final boolean isUtv, final boolean isTruckload)
	{
		final CartModel cartModel = getSessionCart();
		final Map purchaseOrderMap = new HashMap<>();
		final Map reorderPoList = (Map) sessionService.getAllAttributes().get(CatCoreConstants.REORDER_PO_LIST);

		purchaseOrderMap.putAll(reorderPoList);
		LOG.info("Updated purchase order number List" + orderEntryPOList + "for cart entry::" + cartEntryModel.getEntryNumber()
				+ " with product code " + cartEntryModel.getProduct().getCode());
		if (!isTruckload && isUtv)
		{
			purchaseOrderMap.put(String.valueOf(cartEntryModel.getProduct().getCode() + "__" + cartEntryModel.getTruckloadId()),
					orderEntryPOList);
		}
		else if (!isUtv && !isTruckload)
		{
			purchaseOrderMap.put(String.valueOf(cartEntryModel.getProduct().getCode() + "__" + cartEntryModel.getEntryNumber()),
					orderEntryPOList);
		}
		else
		{
			purchaseOrderMap.put(String.valueOf(cartEntryModel.getProduct().getCode() + "__" + cartEntryModel.getTruckloadId()),
					orderEntryPOList);
		}
		sessionService.setAttribute(CatCoreConstants.REORDER_PO_LIST, purchaseOrderMap);

		modelService.save(cartModel);
	}


	/**
	 * This method adds the entries into the custom FBC Parts info table
	 *
	 * @param configId
	 *           - config id
	 * @param qty
	 *           - quantity
	 *
	 */
	public void addFBCPartInfo(final String configId, final long qty)
	{
		final List<Selection> selectionList = sessionService.getAttribute(SELECTIONS);
		getCatCartDao().addFBCPartInfo(selectionList, configId, qty);
		sessionService.removeAttribute(SELECTIONS);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateCartForConfigurableProducts(final String productCode, final long qty, Integer entryNumber)
	{
		if (sessionService.getAttribute(SELECTIONS) != null)
		{
			addFBCPartInfo(productCode, qty);
		}

		final SearchResult<FBCPartsOrderInfoModel> searchResult = getCatCartDao().updateCartForConfigurableProducts();
		final CartModel cartModel = getSessionCart();
		double totalPrice = 0.00d;

		if (qty > 0 || StringUtils.isEmpty(productCode)) // When a product is added (Not Updated/modified) or from a new session
		{
			for (final AbstractOrderEntryModel orderEntry : cartModel.getEntries())
			{
				final double orderEntryTotal = 0.00d;
				if (CollectionUtils.isNotEmpty(searchResult.getResult()) && BooleanUtils.isTrue(orderEntry.getConfigurable()))
				{
					totalPrice = updateOrderEntry(searchResult, orderEntry, orderEntryTotal, totalPrice);
				}
				else if (BooleanUtils.isFalse(orderEntry.getConfigurable()))
				{
					totalPrice += orderEntry.getTotalPrice();
				}
			}
			if (entryNumber == null)
			{
				entryNumber = getSessionCart().getEntries().size() - 1;
			}
			cartModel.setTotalPrice(totalPrice);
			modelService.save(cartModel);
		}
		if (StringUtils.isNotEmpty(productCode) && sessionService.getAttribute("selections") == null)
		{
			updateFBCPartsTable(searchResult, qty, entryNumber, productCode);
		}



	}


	/**
	 * This method updates order entry
	 *
	 * @param productCode
	 *           - prod code
	 * @param qty
	 *           - qty
	 * @param entryNumber
	 *           - entry Number
	 */
	private double updateOrderEntry(final SearchResult<FBCPartsOrderInfoModel> searchResult,
			final AbstractOrderEntryModel orderEntry, final double oeTotal, final double total)
	{
		double orderEntryTotal = oeTotal;
		double totalPrice = total;
		for (final FBCPartsOrderInfoModel fBCPartsOrderInfo : searchResult.getResult())
		{
			if (orderEntry.getProduct().getCode().equals(fBCPartsOrderInfo.getConfigurationId())
					&& orderEntry.getEntryNumber().toString().equalsIgnoreCase(fBCPartsOrderInfo.getEntryId()))
			{
				orderEntryTotal += (fBCPartsOrderInfo.getPrice() * orderEntry.getQuantity());
				totalPrice += (fBCPartsOrderInfo.getPrice() * orderEntry.getQuantity());
				orderEntry.setBasePrice(orderEntryTotal / orderEntry.getQuantity());
				orderEntry.setTotalPrice(orderEntryTotal);
				modelService.save(orderEntry);
			}


		}
		return totalPrice;

	}

	/**
	 * This method updates FBC parts table
	 *
	 * @param searchResult
	 *           - search result
	 * @param qty
	 *           - qty
	 * @param entryNumber
	 *           - entry number
	 * @param productCode
	 *           - prod code
	 */
	private void updateFBCPartsTable(final SearchResult<FBCPartsOrderInfoModel> searchResult, final long qty,
			final Integer entryNumber, final String productCode)
	{
		for (final FBCPartsOrderInfoModel fBCPartsOrderInfo : searchResult.getResult())
		{
			if (fBCPartsOrderInfo.getConfigurationId().equalsIgnoreCase(productCode)
					&& fBCPartsOrderInfo.getEntryId().equalsIgnoreCase(entryNumber.toString()))
			{
				if (qty > 0)
				{
					final int modifiedQty = ((int) qty - fBCPartsOrderInfo.getQuantity());
					fBCPartsOrderInfo.setQuantity(fBCPartsOrderInfo.getQuantity() + modifiedQty);
					modelService.save(fBCPartsOrderInfo);
				}
				else
				{
					modelService.remove(fBCPartsOrderInfo);
				}
			}
		}
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public List<AbstractOrderEntryModel> getConfigVariantEntriesInCurrentCart(final CartModel cartModel)
	{
		return CollectionUtils.isNotEmpty(cartModel.getEntries()) ? cartModel.getEntries().stream()
				.filter(entry -> entry.getProduct() instanceof ConfigVariantProductModel).collect(Collectors.toList())
				: Collections.emptyList();
	}
}
