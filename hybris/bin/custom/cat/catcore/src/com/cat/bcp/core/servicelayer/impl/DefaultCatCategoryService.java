/**
 *
 */
package com.cat.bcp.core.servicelayer.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.category.impl.DefaultCategoryService;
import de.hybris.platform.category.model.CategoryModel;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.cat.bcp.core.dao.CatCategoryDao;
import com.cat.bcp.core.servicelayer.CatCategoryService;


/**
 * @author manjam
 *
 */
public class DefaultCatCategoryService extends DefaultCategoryService implements CatCategoryService
{



	private CatCategoryDao catCategoryDao;



	/*
	 * This Method Used to fetch Low Stock Products Categories.
	 *
	 * @return the List<CategoryModel>
	 */
	@Override
	public List<CategoryModel> getLowStockProductsCategories(final B2BUnitModel b2bUnitModel)
	{

		return (List<CategoryModel>) getCatCategoryDao().findLowStockProducts(b2bUnitModel);
	}



	/**
	 * @return the catCategoryDao
	 */
	public CatCategoryDao getCatCategoryDao()
	{
		return catCategoryDao;
	}



	/**
	 * @param catCategoryDao
	 *           the catCategoryDao to set
	 */
	@Required
	public void setCatCategoryDao(final CatCategoryDao catCategoryDao)
	{
		this.catCategoryDao = catCategoryDao;
	}

}
