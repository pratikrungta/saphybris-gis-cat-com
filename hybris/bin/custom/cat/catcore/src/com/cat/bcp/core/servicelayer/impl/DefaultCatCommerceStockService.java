/**
 *
 */
package com.cat.bcp.core.servicelayer.impl;

import de.hybris.platform.commerceservices.stock.impl.DefaultCommerceStockService;
import de.hybris.platform.store.BaseStoreModel;


/**
 * @author manjam
 *
 */			
public class DefaultCatCommerceStockService extends DefaultCommerceStockService
{
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hybris.platform.commerceservices.stock.impl.DefaultCommerceStockService#isStockSystemEnabled(de.hybris.platform
	 * .store.BaseStoreModel)
	 */
	@Override
	public boolean isStockSystemEnabled(final BaseStoreModel baseStore)
	{
		return false;
	}
}
