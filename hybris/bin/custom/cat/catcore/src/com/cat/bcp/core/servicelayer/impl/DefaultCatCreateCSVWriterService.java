package com.cat.bcp.core.servicelayer.impl;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.servicelayer.CatCPCFileWriterService;
import com.cat.bcp.core.servicelayer.CatCreateCSVWriterService;
import de.hybris.platform.util.CSVWriter;
import de.hybris.platform.util.Config;
import org.apache.log4j.Logger;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * The class contains the implementation of method of CatCreateCSVWriterService.
 */
public class DefaultCatCreateCSVWriterService implements CatCreateCSVWriterService {
    private static final Logger LOGGER=Logger.getLogger(DefaultCatCreateCSVWriterService.class);
    public static final String CSV = "-000.csv";
    private CSVWriter csvClassificationWriter;
    private CSVWriter csvClassificationAttributeWriter;
    private CSVWriter csvClassificationAttributeAssignmentWriter;
    private CSVWriter productDescriptionWriter;
    private CSVWriter categoryRelationshipsWriter;
    private CSVWriter productValueMapWriter;
    private CSVWriter classificationAttributeRemovalWriter;
    @Resource(name="catCPCFileWriterService")
    CatCPCFileWriterService catCPCFileWriterService;

    @Resource(name="baseDirectoryCat")
    String baseDirectoryCat;

    public CSVWriter getCsvClassificationWriter() {
        return csvClassificationWriter;
    }

    public CSVWriter getCsvClassificationAttributeWriter() {
        return csvClassificationAttributeWriter;
    }

    public CSVWriter getCsvClassificationAttributeAssignmentWriter() {
        return csvClassificationAttributeAssignmentWriter;
    }

    public CSVWriter getProductDescriptionWriter() {
        return productDescriptionWriter;
    }

    public CSVWriter getCategoryRelationshipsWriter() {
        return categoryRelationshipsWriter;
    }

    public CSVWriter getProductValueMapWriter() {
        return productValueMapWriter;
    }

    public CSVWriter getClassificationAttributeRemovalWriter() {
        return classificationAttributeRemovalWriter;
    }


    /**
     *{@inheritDoc}
     */
    @Override
    public   DefaultCatCreateCSVWriterService createCSVWriter() throws IOException {
        LOGGER.debug("base directory for the hotfolder is"+baseDirectoryCat);
        LOGGER.debug("Start:Creating csv file");

        csvClassificationWriter = catCPCFileWriterService.getCsvWriter(baseDirectoryCat.concat("/").concat(Config.getString("cat.hotfolder.classification.class","classification-class")).concat(CSV), CatCoreConstants.CPCSEPARATOR);

        csvClassificationAttributeWriter = catCPCFileWriterService.getCsvWriter(baseDirectoryCat.concat("/").concat(Config.getString("cat.hotfolder.classification.attribute","classification-attribute")).concat(CSV), CatCoreConstants.CPCSEPARATOR);

        csvClassificationAttributeAssignmentWriter = catCPCFileWriterService.getCsvWriter(baseDirectoryCat.concat("/").concat(Config.getString("cat.hotfolder.classification-attribute_assignment","classification-attribute_assignment")).concat(CSV), CatCoreConstants.CPCSEPARATOR);

        productDescriptionWriter = catCPCFileWriterService.getCsvWriter(baseDirectoryCat.concat("/").concat(Config.getString("cat.hotfolder.base.product.description","base-product_description")).concat(CSV), CatCoreConstants.CPCSEPARATOR);

        categoryRelationshipsWriter = catCPCFileWriterService.getCsvWriter(baseDirectoryCat.concat("/").concat(Config.getString("cat.hotfolder.utv.product.category.classfication","classification-category_utv_relation")).concat(CSV), CatCoreConstants.CPCSEPARATOR);

        productValueMapWriter = catCPCFileWriterService.getCsvWriter(baseDirectoryCat.concat("/").concat(Config.getString("cat.hotfolder.product.classification.value.assignment","product-classification_value_assignment")).concat(CSV), CatCoreConstants.CPCSEPARATOR);

        LOGGER.debug("END:Creating csv file");
        return this;
    }


}

