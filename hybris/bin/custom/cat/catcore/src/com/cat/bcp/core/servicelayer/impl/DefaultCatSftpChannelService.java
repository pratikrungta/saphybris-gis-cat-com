/**
 *
 */
package com.cat.bcp.core.servicelayer.impl;

import de.hybris.platform.servicelayer.util.ServicesUtil;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.servicelayer.CatSftpChannelService;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;


/**
 * @author amitsinha5
 *
 */
public class DefaultCatSftpChannelService implements CatSftpChannelService
{

	private static final Logger LOG = Logger.getLogger(DefaultCatSftpChannelService.class.getName());
	public static final String FILE_SEPARATOR = "/";

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("boxing")
	@Override
	public Session connectSessionForSftp(final String host, final String userId, final int port, final String authPassword)
			throws IOException, JSchException
	{


		ServicesUtil.validateParameterNotNull(host, "The given host is null!");
		ServicesUtil.validateParameterNotNull(userId, "The given userID is null!");
		ServicesUtil.validateParameterNotNull(port, "The given port is null!");
		ServicesUtil.validateParameterNotNull(authPassword, "The given password is null!");

		Session session = null;
		final JSch jsch = new JSch();
		session = jsch.getSession(userId, host, port);
		session.setPassword(authPassword);
		final java.util.Properties config = new java.util.Properties();
		config.put(CatCoreConstants.SFTP_HOST_KEY_CHECK, CatCoreConstants.KEY_REQUIRED);
		session.setConfig(config);
		session.connect();
		LOG.info("Started SFTP session with MFT server..");
		return session;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Channel openChannelType(final String ftpType, final Session session) throws JSchException
	{
		ServicesUtil.validateParameterNotNull(ftpType, "The given ftpType is null!");
		ServicesUtil.validateParameterNotNull(session, "The given session is null!");

		Channel channel = null;
		channel = session.openChannel(ftpType);
		channel.connect();

		return channel;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ChannelSftp changeDirectoryForSftp(final String filePath, final Channel channel) throws IOException
	{

		ServicesUtil.validateParameterNotNull(filePath, "The given filePath is null!");
		ServicesUtil.validateParameterNotNull(channel, "The given channel is null!");

		final ChannelSftp channelSftp = (ChannelSftp) channel;
		try
		{
			channelSftp.cd(filePath);
		}
		catch (final Exception e)
		{
			throw new IOException(CatCoreConstants.FTP_FILE_PATH_ERROR, e);
		}

		return channelSftp;
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public void readFilesFromMftServer(final ChannelSftp channelSftp, final int bufferByte, final LsEntry entry, final File file)
			throws IOException, SftpException
	{
		ServicesUtil.validateParameterNotNull(entry.getFilename(), "The given file name is null");

		try (OutputStream os = new FileOutputStream(file);
				final BufferedOutputStream bos = new BufferedOutputStream(os);
				final BufferedInputStream bis = new BufferedInputStream(channelSftp.get(entry.getFilename())))
		{

			final byte[] buffer = new byte[bufferByte];

			int readCount;
			while ((readCount = bis.read(buffer)) > 0)
			{
				LOG.info("Writing file into your local hotfolder location::" + file);
				bos.write(buffer, 0, readCount);
			}
		}
		catch (final IOException e)
		{
			throw new IOException(CatCoreConstants.FTP_FILE_PATH_ERROR, e);
		}
		catch (final SftpException e)
		{
			throw new SftpException(1, CatCoreConstants.FTP_CHANNEL_ERROR, e);
		}
	}





	/**
	 * {@inheritDoc}
	 */
	@Override
	public Vector<LsEntry> getFilePatternEntries(final ChannelSftp channelSftp, final String searchType) throws IOException
	{
		Vector<LsEntry> entries = null; //NOSONAR
		try
		{
			if (!StringUtils.isBlank(searchType))
			{
				entries = channelSftp.ls(searchType);
			}
			else
			{
				entries = channelSftp.ls(CatCoreConstants.LS_COMPLETE_SEARCH);
			}
		}
		catch (final SftpException e)
		{
			LOG.error("Unable to fetch LS remote files entries", e);
			throw new IOException(CatCoreConstants.FTP_FILE_PATH_ERROR);
		}
		return entries;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public File storeFileToTargetFolder(final LsEntry entry, final String path, final String site) throws IOException
	{
		ServicesUtil.validateParameterNotNull(entry.getFilename(), "The given fileName is null!");
		ServicesUtil.validateParameterNotNull(path, "The given file path is null!");
		File newFilePath = null;

		newFilePath = new File(path + getFilePath(site) + entry.getFilename());
		LOG.info("Local folder path for transferring the files from MFT:" + newFilePath);

		return newFilePath;
	}

	/**
	 * @param path
	 * @return path
	 */
	private static String getFilePath(final String path)
	{
		if (path.charAt(path.length() - 1) != FILE_SEPARATOR.charAt(0))
		{
			return path + FILE_SEPARATOR.charAt(0);
		}

		return path;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void writingFileLogs(final LsEntry entry) throws IOException
	{
		ServicesUtil.validateParameterNotNull(entry.getFilename(), "The given fileName is null!");

		LOG.info(" Started fetching file from MFT server, file info:" + entry.getFilename() + " & file Size:"
				+ entry.getFilename().length());
	}


}
