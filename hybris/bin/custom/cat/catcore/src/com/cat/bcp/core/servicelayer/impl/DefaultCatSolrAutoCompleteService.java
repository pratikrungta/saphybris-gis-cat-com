/**
 *
 */
package com.cat.bcp.core.servicelayer.impl;

import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.SolrFacetSearchConfigSelectionStrategy;
import de.hybris.platform.commerceservices.search.solrfacetsearch.strategies.exceptions.NoValidSolrConfigException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfigService;
import de.hybris.platform.solrfacetsearch.config.IndexConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.exceptions.FacetConfigServiceException;
import de.hybris.platform.solrfacetsearch.indexer.SolrIndexedTypeCodeResolver;
import de.hybris.platform.solrfacetsearch.indexer.exceptions.IndexerException;
import de.hybris.platform.solrfacetsearch.model.config.SolrFacetSearchConfigModel;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedTypeModel;
import de.hybris.platform.solrfacetsearch.suggester.SolrAutoSuggestService;
import de.hybris.platform.solrfacetsearch.suggester.SolrSuggestion;
import de.hybris.platform.solrfacetsearch.suggester.exceptions.SolrAutoSuggestException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.cat.bcp.core.enums.CatIndexedTypeEnum;
import com.cat.bcp.core.servicelayer.CatSolrAutoCompleteService;


/**
 * @author sagdhingra
 *
 */
public class DefaultCatSolrAutoCompleteService implements CatSolrAutoCompleteService
{
	private static final Logger LOG = Logger.getLogger(DefaultCatSolrAutoCompleteService.class);

	private FacetSearchConfigService facetSearchConfigService;
	private CommonI18NService commonI18NService;
	private SolrAutoSuggestService solrAutoSuggestService;
	private SolrIndexedTypeCodeResolver solrIndexedTypeCodeResolver;
	private SolrFacetSearchConfigSelectionStrategy solrFacetSearchConfigSelectionStrategy;


	protected FacetSearchConfigService getFacetSearchConfigService()
	{
		return facetSearchConfigService;
	}


	@Required
	public void setSolrIndexedTypeCodeResolver(final SolrIndexedTypeCodeResolver solrIndexedTypeCodeResolver)
	{
		this.solrIndexedTypeCodeResolver = solrIndexedTypeCodeResolver;
	}

	/**
	 * @return the solrFacetSearchConfigSelectionStrategy
	 */
	public SolrFacetSearchConfigSelectionStrategy getSolrFacetSearchConfigSelectionStrategy()
	{
		return solrFacetSearchConfigSelectionStrategy;
	}


	@Required
	public void setSolrFacetSearchConfigSelectionStrategy(
			final SolrFacetSearchConfigSelectionStrategy solrFacetSearchConfigSelectionStrategy)
	{
		this.solrFacetSearchConfigSelectionStrategy = solrFacetSearchConfigSelectionStrategy;
	}

	@Required
	public void setFacetSearchConfigService(final FacetSearchConfigService facetSearchConfigService)
	{
		this.facetSearchConfigService = facetSearchConfigService;
	}

	protected CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	@Required
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	protected SolrAutoSuggestService getSolrAutoSuggestService()
	{
		return solrAutoSuggestService;
	}

	@Required
	public void setSolrAutoSuggestService(final SolrAutoSuggestService solrAutoSuggestService)
	{
		this.solrAutoSuggestService = solrAutoSuggestService;
	}


	/**
	 * Method to get a map of auto suggestions: 1) Retrieving indexedTypeModel based on the input index type and 2) Call
	 * the auto suggest service to retrieve the suggestion map.
	 *
	 * @param input
	 * @param indexType
	 * @return Map<String, Collection<String>>
	 */
	@Override
	public Map<String, Collection<String>> getAutocompleteSuggestionsMap(final String input, final CatIndexedTypeEnum indexType)
	{
		Map<String, Collection<String>> result = new HashMap<>();
		try
		{
			final SolrFacetSearchConfigModel solrFacetSearchConfigModel = getSolrFacetSearchConfigSelectionStrategy()
					.getCurrentSolrFacetSearchConfig();

			final FacetSearchConfig facetSearchConfig = getFacetSearchConfigService()
					.getConfiguration(solrFacetSearchConfigModel.getName());
			final Optional<IndexedType> indexedType = getIndexedType(facetSearchConfig, indexType);
			if (indexedType.isPresent())
			{
				final SolrIndexedTypeModel indexedTypeModel = findIndexedTypeModel(solrFacetSearchConfigModel, indexedType.get());

				final SolrSuggestion suggestions = getSolrAutoSuggestService()
						.getAutoSuggestionsForQuery(getCommonI18NService().getCurrentLanguage(), indexedTypeModel, input);


				result = suggestions.getSuggestions();
			}
		}
		catch (final SolrAutoSuggestException | FacetConfigServiceException | IndexerException | NoValidSolrConfigException e)
		{
			LOG.warn("Error retrieving autocomplete suggestions", e);
		}

		return result;
	}

	/**
	 * Method to get the Indexed Type
	 *
	 * @param config
	 * @param indexedType
	 * @return Optional<IndexedType>
	 */
	protected Optional<IndexedType> getIndexedType(final FacetSearchConfig config, final CatIndexedTypeEnum indexedType)
	{
		final IndexConfig indexConfig = config.getIndexConfig();
		// Strategy for working out which of the available indexed types to use
		final Collection<IndexedType> indexedTypes = indexConfig.getIndexedTypes().values();
		return indexedTypes.stream().filter(indexType -> indexType.getCode().equalsIgnoreCase(indexedType.getCode())).findFirst();
	}

	/**
	 * Method to find SolrIndexedTypeModel
	 *
	 * @param facetSearchConfigModel
	 * @param indexedType
	 * @return SolrIndexedTypeModel
	 */
	protected SolrIndexedTypeModel findIndexedTypeModel(final SolrFacetSearchConfigModel facetSearchConfigModel,
			final IndexedType indexedType) throws IndexerException
	{
		if (indexedType == null)
		{
			throw new IndexerException("indexedType is NULL!");
		}
		for (final SolrIndexedTypeModel type : facetSearchConfigModel.getSolrIndexedTypes())
		{
			if (solrIndexedTypeCodeResolver.resolveIndexedTypeCode(type).equals(indexedType.getUniqueIndexedTypeCode()))
			{
				return type;
			}
		}
		throw new IndexerException("Could not find matching model for type: " + indexedType.getCode());
	}

}
