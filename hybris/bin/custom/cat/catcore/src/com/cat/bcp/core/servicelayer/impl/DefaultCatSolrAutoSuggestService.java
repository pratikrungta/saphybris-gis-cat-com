/**
 *
 */
package com.cat.bcp.core.servicelayer.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.c2l.LanguageModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.solrfacetsearch.config.FacetSearchConfig;
import de.hybris.platform.solrfacetsearch.config.IndexedType;
import de.hybris.platform.solrfacetsearch.config.exceptions.FacetConfigServiceException;
import de.hybris.platform.solrfacetsearch.model.SolrIndexModel;
import de.hybris.platform.solrfacetsearch.model.config.SolrIndexedTypeModel;
import de.hybris.platform.solrfacetsearch.solr.Index;
import de.hybris.platform.solrfacetsearch.solr.SolrSearchProvider;
import de.hybris.platform.solrfacetsearch.solr.exceptions.SolrServiceException;
import de.hybris.platform.solrfacetsearch.suggester.SolrSuggestion;
import de.hybris.platform.solrfacetsearch.suggester.exceptions.SolrAutoSuggestException;
import de.hybris.platform.solrfacetsearch.suggester.impl.DefaultSolrAutoSuggestService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.util.Config;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.SuggesterResponse;
import org.apache.solr.client.solrj.response.Suggestion;

import com.cat.bcp.core.enums.CatIndexedTypeEnum;


/**
 * @author sagdhingra
 *
 */
public class DefaultCatSolrAutoSuggestService extends DefaultSolrAutoSuggestService
{
	@Resource(name = "userService")
	UserService userService;

	private static final Logger LOG = Logger.getLogger(DefaultSolrAutoSuggestService.class);
	public static final String SUGGESTER_REQ_HANDLER = "/autoSuggest";
	public static final String IS_SUGGEST = "suggest";
	public static final String SUGGEST_DICTIONARY = "suggest.dictionary";
	public static final String SUGGEST_BUILD = "suggest.build";
	public static final String SUGGEST_WT = "wt";
	public static final String SUGGEST_COUNT = "suggest.count";
	public static final String SUGGEST_COUNT_VALUE = "suggest.count.value";
	public static final String SUGGEST_QUERY = "suggest.q";
	public static final String SUGGEST_CFQ = "suggest.cfq";
	private static final String[] SUGGEST_USED =
	{ "salesModelNameSuggester", "productFamilyNameSuggester", "serialNumberSuggester" };
	private static final String[] SUGGEST_NEW =
	{ "salesModelNameSuggester", "baseModelNameSuggester", "productFamilyNameSuggester" };

	/**
	 * Method for returning dealer warehouse
	 *
	 *
	 * @return String
	 */
	public String getDealerWarehouse()
	{
		final B2BCustomerModel customer = (B2BCustomerModel) userService.getCurrentUser();
		final B2BUnitModel unit = customer.getDefaultB2BUnit();
		WarehouseModel warehouse = null;
		final Set<PointOfServiceModel> posSet = unit.getPointsOfService();
		for (final PointOfServiceModel pos : posSet)
		{
			if (PointOfServiceTypeEnum.POS.equals(pos.getType()) && CollectionUtils.isNotEmpty(pos.getWarehouses()))
			{
				warehouse = pos.getWarehouses().get(0);
				break;
			}
		}
		return warehouse != null ? warehouse.getCode() : "";
	}

	/**
	 * Method to get the auto suggestions for the query sent to SOLR : 1) Preparing the query by adding query parameters
	 * or 2) Manipulating the suggestion map and setting it in SolrSuggestion object.
	 *
	 * @param language
	 *           language model passed
	 * @param solrIndexedType
	 *           the indexed type on which the query will be fired
	 * @param queryInput
	 *           the input passed as text
	 * @return SolrSuggestion the object containing the list of suggestions
	 */
	@Override
	public SolrSuggestion getAutoSuggestionsForQuery(final LanguageModel language, final SolrIndexedTypeModel solrIndexedType,
			final String queryInput) throws SolrAutoSuggestException
	{
		SolrClient solrClient = null;
		try
		{
			Map<String, Collection<String>> suggestionMap = new HashMap<>();
			final String e = solrIndexedType.getSolrFacetSearchConfig().getName();
			final FacetSearchConfig facetSearchConfig = this.facetSearchConfigService.getConfiguration(e);
			final IndexedType indexedType = facetSearchConfig.getIndexConfig().getIndexedTypes()
					.get(getSolrIndexedTypeCodeResolver().resolveIndexedTypeCode(solrIndexedType));
			final SolrSearchProvider solrSearchProvider = getSolrSearchProviderFactory().getSearchProvider(facetSearchConfig,
					indexedType);
			final SolrIndexModel solrIndex = getSolrIndexService().getActiveIndex(facetSearchConfig.getName(),
					indexedType.getIdentifier());
			final Index index = solrSearchProvider.resolveIndex(facetSearchConfig, indexedType, solrIndex.getQualifier());
			solrClient = solrSearchProvider.getClient(index);
			final SolrQuery query = new SolrQuery();

			query.setRequestHandler(SUGGESTER_REQ_HANDLER);
			query.set(IS_SUGGEST, "true");
			query.set(SUGGEST_COUNT, Config.getParameter(SUGGEST_COUNT_VALUE));
			query.set(SUGGEST_DICTIONARY, (CatIndexedTypeEnum.BASEVARIANTPRODUCT.getCode()).equalsIgnoreCase(indexedType.getCode())
					? SUGGEST_USED : SUGGEST_NEW);
			query.set(SUGGEST_WT, "json");
			query.set(SUGGEST_QUERY, queryInput);
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Solr Suggest Query: \n" + URLDecoder.decode(String.valueOf(query), "UTF-8"));
			}
			addLocationFilterQuery(query, indexedType);
			final QueryResponse response = solrClient.query(index.getName(), query);
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Solr Suggest Response: \n" + response);
			}

			final SuggesterResponse suggesterResponse = response.getSuggesterResponse();
			if (suggesterResponse != null && suggesterResponse.getSuggestions() != null)
			{
				suggestionMap = prepareSuggestionMap(suggesterResponse.getSuggestions());
			}
			return new SolrSuggestion(suggestionMap, new ArrayList<String>());

		}
		catch (FacetConfigServiceException | SolrServerException | IOException | SolrServiceException arg18)
		{
			throw new SolrAutoSuggestException("Error issuing suggestion query", arg18);
		}
		finally
		{
			IOUtils.closeQuietly(solrClient);
		}
	}


	/**
	 * Method for preparing suggestion map from a Map of String,List<Suggestion>
	 *
	 * @param suggestions
	 *           the list of suggestions coming from solr
	 * @return Map<String,Collection<String>> the map of String,Collection<String> is returned from Map<String,List
	 *         <Suggestion>>
	 */
	public Map<String, Collection<String>> prepareSuggestionMap(final Map<String, List<Suggestion>> suggestions)
	{
		final Map<String, Collection<String>> suggestionMap = new HashMap<>();
		suggestions.entrySet().forEach(entrySet -> {
			final List<Suggestion> suggestionList = entrySet.getValue();

			final Set<Suggestion> distinctSuggestionSet = new TreeSet<>(Comparator.comparing(p -> p.getTerm()));
			distinctSuggestionSet.addAll(suggestionList);

			if (CollectionUtils.isNotEmpty(distinctSuggestionSet))
			{
				suggestionMap.put(entrySet.getKey(), prepareSuggestionCollection(distinctSuggestionSet));
			}
		});
		return suggestionMap;
	}

	/**
	 * This method is used to restrict suggestion collection size to 5
	 *
	 * @param distinctSuggestionSet
	 * @return suggesion collection
	 */
	private Set<String> prepareSuggestionCollection(final Set<Suggestion> distinctSuggestionSet)
	{
		final Set<String> suggestionCollection = new HashSet<>();
		for (final Suggestion suggestion : distinctSuggestionSet)
		{
			if (suggestionCollection.size() < 5)
			{
				suggestionCollection.add(suggestion.getTerm());
			}
			else
			{
				break;
			}
		}
		return suggestionCollection;
	}

	/**
	 * While retrieving the data for autosuggestion in new products take care of warehouse as well
	 *
	 * @param query
	 *           solr query in which the filter query is to be added
	 * @param indexedType
	 *           to check whether it is a new or used product
	 *
	 */
	private void addLocationFilterQuery(final SolrQuery query, final IndexedType indexedType)
	{
		if (indexedType.getCode().equals(CatIndexedTypeEnum.CONFIGVARIANTPRODUCT.getCode()))
		{
			final StringBuilder warehouseLocationQuery = new StringBuilder();

			final Set<CategoryModel> categorySet = new HashSet<>();

			//Send the gateway categories in the solr query for the results in auto suggestions
			for (final UserGroupModel userGroupModel : userService.getAllUserGroupsForUser(userService.getCurrentUser()))
			{
				categorySet.addAll(userGroupModel.getAccessibleCategories().stream()
						.filter(categoryModel -> categoryModel.getGatewayCategory() != null).collect(Collectors.toList()));
			}
			if (CollectionUtils.isNotEmpty(categorySet))
			{
				categorySet.forEach(categoryModel -> warehouseLocationQuery.append("(").append(categoryModel.getCode()).append(")"));
				query.set(SUGGEST_CFQ, String.valueOf(warehouseLocationQuery));
			}
		}
	}
}