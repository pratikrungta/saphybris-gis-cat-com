/**
 *
 */
package com.cat.bcp.core.servicelayer.impl;

import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.user.impl.DefaultAddressService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.List;

import org.springframework.util.CollectionUtils;

import com.cat.bcp.core.dao.DealerAddressDao;
import com.cat.bcp.core.servicelayer.DealerAddressService;


/**
 * @author bidavda
 *
 */
public class DefaultDealerAddressService extends DefaultAddressService implements DealerAddressService
{
	DealerAddressDao dealerAddressDao;

	@Override
	public AddressModel getPosAddressByPosId(final String posId)
	{
		final List<AddressModel> addresses = getDealerAddressDao().findByPosId(posId);
		if (CollectionUtils.isEmpty(addresses))
		{
			return null;
		}
		return addresses.iterator().next();
	}

	@Override
	public PointOfServiceModel findMFUorPDCByPosId(final String posId)
	{
		final List<PointOfServiceModel> addresses = getDealerAddressDao().findMFUorPDCByPosId(posId);
		if (CollectionUtils.isEmpty(addresses))
		{
			return null;
		}
		return addresses.iterator().next();
	}

	@Override
	public WarehouseModel findWarehouseByDealerCode(final String dealerCode)
	{
		final List<WarehouseModel> wareHouses = getDealerAddressDao().findWareHouseBycode(dealerCode);
		if (CollectionUtils.isEmpty(wareHouses))
		{
			return null;
		}
		return wareHouses.iterator().next();
	}

	/**
	 * @return the dealerAddressDao
	 */
	public DealerAddressDao getDealerAddressDao()
	{
		return dealerAddressDao;
	}

	/**
	 * @param dealerAddressDao
	 *           the dealerAddressDao to set
	 */
	public void setDealerAddressDao(final DealerAddressDao dealerAddressDao)
	{
		this.dealerAddressDao = dealerAddressDao;
	}
}
