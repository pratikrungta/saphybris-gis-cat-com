/**
 *
 */
package com.cat.bcp.core.servicelayer.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

import javax.annotation.Resource;

import com.cat.bcp.core.dao.DealerSearchDao;
import com.cat.bcp.core.model.AlertProductModel;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.RestockAlertModel;
import com.cat.bcp.core.servicelayer.DealerSearchService;


/**
 * @author sankale
 *
 *         This class provides the services required for custom handlers
 */
public class DefaultDealerSearchService implements DealerSearchService
{

	@Resource(name = "dealerSearchDao")
	private DealerSearchDao dealerSearchDao;


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<BaseVariantProductModel> getBaseModels(final ProductModel productModel)
	{
		return dealerSearchDao.getBaseModels(productModel);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductModel> getSalesModels(final String categoryCode, final boolean isUsed)
	{
		// YTODO Auto-generated method stub
		return dealerSearchDao.getSalesModels(categoryCode, isUsed);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<RestockAlertModel> getLowStockProducts(final B2BUnitModel b2bUnit, final String category,
			final String salesModelId)
	{
		return dealerSearchDao.getLowStockProducts(b2bUnit, category, salesModelId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AlertProductModel> getAssociatedConfigProducts(final String alertId)
	{
		return dealerSearchDao.getAssociatedConfigProducts(alertId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ProductModel getProductByName(final String productName)
	{
		return dealerSearchDao.getProductByName(productName);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public CategoryModel getCategoryByName(final String categoryName)
	{
		return dealerSearchDao.getCategoryByName(categoryName);
	}
}
