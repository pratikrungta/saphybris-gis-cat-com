/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.core.setup;

import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;
import de.hybris.platform.core.initialization.SystemSetupParameterMethod;
import de.hybris.platform.impex.constants.ImpExConstants;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.cat.bcp.core.constants.CatCoreConstants;


/**
 * This class provides hooks into the system's initialization and update processes.
 */
@SystemSetup(extension = CatCoreConstants.EXTENSIONNAME)
public class CoreSystemSetup extends AbstractSystemSetup
{
	private static final Logger LOG = Logger.getLogger(CoreSystemSetup.class);

	public static final String IMPORT_ACCESS_RIGHTS = "accessRights";

	public static final String CAT = "NA";

	public static final String COREDATA_PATH = "/catcore/import/coredata/";


	@Resource(name = "coreDataImportService")
	private CoreDataImportService coreDataImportService;

	/**
	 * This method will be called by system creator during initialization and system update. Be sure that this method can
	 * be called repeatedly.
	 *
	 * @param context
	 *           the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.ESSENTIAL, process = Process.ALL)
	public void createEssentialData(final SystemSetupContext context)
	{
		importImpexFile(context, "/catcore/import/common/essential-data.impex");
		importImpexFile(context, "/catcore/import/common/countries.impex");
		importImpexFile(context, "/catcore/import/common/delivery-modes.impex");

		importImpexFile(context, "/catcore/import/common/themes.impex");
		importImpexFile(context, "/catcore/import/common/user-groups.impex");
	}

	/**
	 * Generates the Dropdown and Multi-select boxes for the project data import
	 */
	@Override
	@SystemSetupParameterMethod
	public List<SystemSetupParameter> getInitializationOptions()
	{
		final List<SystemSetupParameter> params = new ArrayList<>();

		params.add(createBooleanSystemSetupParameter(IMPORT_ACCESS_RIGHTS, "Import Users & Groups", true));

		return params;
	}

	/**
	 * This method will be called during the system initialization.
	 *
	 * @param context
	 *           the context provides the selected parameters and values
	 */
	@SystemSetup(type = Type.PROJECT, process = Process.INIT)
	public void createInitProjectData(final SystemSetupContext context)
	{
		importContent(context);
		importStore(context, CAT);
	}

	@SystemSetup(type = Type.PROJECT, process = Process.ALL)
	public void createProjectData(final SystemSetupContext context)
	{
		final boolean importAccessRights = getBooleanSystemSetupParameter(context, IMPORT_ACCESS_RIGHTS);

		final List<String> extensionNames = getExtensionNames();



		processCockpit(context, importAccessRights, extensionNames, "cmscockpit",
				"/catcore/import/cockpits/cmscockpit/cmscockpit-users.impex",
				"/catcore/import/cockpits/cmscockpit/cmscockpit-access-rights.impex");

		processCockpit(context, importAccessRights, extensionNames, "productcockpit",
				"/catcore/import/cockpits/productcockpit/productcockpit-users.impex",
				"/catcore/import/cockpits/productcockpit/productcockpit-access-rights.impex",
				"/catcore/import/cockpits/productcockpit/productcockpit-constraints.impex");

		processCockpit(context, importAccessRights, extensionNames, "cscockpit",
				"/catcore/import/cockpits/cscockpit/cscockpit-users.impex",
				"/catcore/import/cockpits/cscockpit/cscockpit-access-rights.impex");

		processCockpit(context, importAccessRights, extensionNames, "reportcockpit",
				"/catcore/import/cockpits/reportcockpit/reportcockpit-users.impex",
				"/catcore/import/cockpits/reportcockpit/reportcockpit-access-rights.impex");

		if (extensionNames.contains("mcc"))
		{
			importImpexFile(context, "/catcore/import/common/mcc-sites-links.impex");
		}

		importProductCatalog(context, CAT);
		importContentCatalog(context, CAT);
	}

	@SystemSetup(type = Type.ESSENTIAL, process = Process.UPDATE)
	public void createEssentialUpdateData(final SystemSetupContext context)
	{
		logInfo(context, "==============BEGIN ESSENTIAL UPDATE PROCESS==============");
		try
		{

			importContent(context);

		}
		catch (final Exception e)
		{
			this.logError(context, e.getMessage(), e.getCause());
			LOG.error("Error occurred in createEssentialUpdateData ! - ", e);
		}
		logInfo(context, "==============DONE ESSENTIAL UPDATE PROCESS==============");

	}

	protected void processCockpit(final SystemSetupContext context, final boolean importAccessRights,
			final List<String> extensionNames, final String cockpit, final String... files)
	{
		if (importAccessRights && extensionNames.contains(cockpit))
		{
			for (final String file : files)
			{
				importImpexFile(context, file);
			}
		}
	}

	protected List<String> getExtensionNames()
	{
		return Registry.getCurrentTenant().getTenantSpecificExtensionNames();
	}

	protected <T> T getBeanForName(final String name)
	{
		return (T) Registry.getApplicationContext().getBean(name);
	}

	protected void importProductCatalog(final SystemSetupContext context, final String catalogName)
	{
		logInfo(context, "Begin importing catalog [" + catalogName + "]");
		importImpexFile(context, COREDATA_PATH + catalogName + "ProductCatalog/catalog.impex", true);
		logInfo(context, "Done importing catalog [" + catalogName + "]");

		logInfo(context, "Begin importing categories [" + catalogName + "]");
		importImpexFile(context, COREDATA_PATH + catalogName + "ProductCatalog/categories.impex", true);
		logInfo(context, "Done importing catalog [" + catalogName + "]");

		logInfo(context, "Begin importing classification categories [" + catalogName + "]");
		importImpexFile(context, COREDATA_PATH + catalogName + "ProductCatalog/classifications-units.impex", true);
		importImpexFile(context, COREDATA_PATH + catalogName + "ProductCatalog/categories-classifications.impex", true);
		logInfo(context, "Done importing classification catalog [" + catalogName + "]");

		logInfo(context, "Begin importing Speccheck data [" + catalogName + "]");
		importImpexFile(context, COREDATA_PATH + catalogName + "ProductCatalog/speccheck_data.impex", true);
		logInfo(context, "Done importing Speccheck data [" + catalogName + "]");

		logInfo(context, "Begin importing CPC data [" + catalogName + "]");
		importImpexFile(context, COREDATA_PATH + catalogName + "ProductCatalog/product_cpc_data.impex", true);
		logInfo(context, "Done importing CPC data [" + catalogName + "]");

		logInfo(context, "Begin importing SalesAreaLookup data [" + catalogName + "]");
		importImpexFile(context, COREDATA_PATH + catalogName + "ProductCatalog/saleArealookup_data.impex", true);
		logInfo(context, "Done importing SalesAreaLookup data [" + catalogName + "]");

		//logInfo(context, "Begin importing CatLaneOrderingWindow data [" + catalogName + "]");
		//importImpexFile(context, COREDATA_PATH + catalogName + "ProductCatalog/catLaneOrderingWindow.impex", true);
		//logInfo(context, "Done importing CatLaneOrderingWindow data [" + catalogName + "]");

		// Read old value
		final String legacyModeBackup = Config.getParameter(ImpExConstants.Params.LEGACY_MODE_KEY);
		// Set it to true
		Config.setParameter(ImpExConstants.Params.LEGACY_MODE_KEY, "true");
		logInfo(context, "Begin importing EPP And CSA data [" + catalogName + "]");
		importImpexFile(context, COREDATA_PATH + catalogName + "ProductCatalog/additionalInfo_data.impex", true);
		logInfo(context, "Done importing EPP And CSA data [" + catalogName + "]");
		// Set it again to previous value
		Config.setParameter(ImpExConstants.Params.LEGACY_MODE_KEY, legacyModeBackup);

		logInfo(context, "Begin Creating SOLR indexer Job [" + catalogName + "]");
		importImpexFile(context, COREDATA_PATH + catalogName + "/cat_solr.impex", true);
		logInfo(context, "Done Creating SOLR indexer Job [" + catalogName + "]");

		logInfo(context, "Begin Creating Sync and update cronjob [" + catalogName + "]");
		importImpexFile(context, COREDATA_PATH + catalogName + "ProductCatalog/catalog-sync.impex", true);
		logInfo(context, "Done Creating Sync and update cronjob [" + catalogName + "]");

		logInfo(context, "Begin importing Coaching Tools Data [" + catalogName + "]");
		importImpexFile(context, COREDATA_PATH + catalogName + "ProductCatalog/coachingToolLinks.impex", true);
		logInfo(context, "Done importing Coaching Tools Data [" + catalogName + "]");

		logInfo(context, "Begin Creating Product Sync Job [" + catalogName + "]");
		createProductCatalogSyncJob(context, catalogName + "ProductCatalog");
		coreDataImportService.synchronizeProductCatalog(this, context, catalogName, true);
		logInfo(context, "Done Creating Product Sync Job [" + catalogName + "]");
	}



	protected void importContentCatalog(final SystemSetupContext context, final String catalogName)
	{
		logInfo(context, "Begin importing content catalog [" + catalogName + "]");

		importImpexFile(context, COREDATA_PATH + catalogName + "ContentCatalog/catalog.impex", true);
		importImpexFile(context, COREDATA_PATH + catalogName + "ContentCatalog/cms-content.impex", false);
		importImpexFile(context, COREDATA_PATH + catalogName + "ContentCatalog/email-content.impex", false);
		importImpexFile(context, COREDATA_PATH + catalogName + "ContentCatalog/Region_Master.impex", false);
		importImpexFile(context, COREDATA_PATH + catalogName + "ContentCatalog/cronjob.impex", false);
		logInfo(context, "Done importing content catalog [" + catalogName + "]");

		logInfo(context, "Start Sync catalog [" + catalogName + "]");
		createContentCatalogSyncJob(context, catalogName + "ContentCatalog");
		coreDataImportService.synchronizeContentCatalog(this, context, catalogName, true);
		logInfo(context, "Done Sync catalog [" + catalogName + "]");


	}

	protected void importStore(final SystemSetupContext context, final String storeName)
	{
		logInfo(context, "Begin importing store [" + storeName + "]");

		importImpexFile(context, COREDATA_PATH + storeName + "/store.impex");
		importImpexFile(context, COREDATA_PATH + storeName + "/site.impex");

		logInfo(context, "Done importing store [" + storeName + "]");
	}

	protected void importUserGroup(final SystemSetupContext context, final String storeName)
	{
		logInfo(context, "Begin importing User group [" + storeName + "]");

		importImpexFile(context, "/catcore/import/common/user-groups.impex");

		logInfo(context, "Done importing User group [" + storeName + "]");
	}

	protected void importContent(final SystemSetupContext context)
	{
		logInfo(context, "Begin importing  [ essentialdata_cmscockpit_templates Data]");

		importImpexFile(context, "/catcore/import/cockpits/cmscockpit/essentialdata_cmscockpit_templates.impex");


		logInfo(context, "Done importing  [essentialdata_cmscockpit_templates Data ]");
	}
}
