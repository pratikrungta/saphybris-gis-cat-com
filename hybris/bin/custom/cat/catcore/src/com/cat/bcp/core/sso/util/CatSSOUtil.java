/**
 *
 */
package com.cat.bcp.core.sso.util;

import de.hybris.platform.util.Config;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;


/**
 * @author rkolanupaka
 *
 */
public class CatSSOUtil
{


	private static final Logger LOG = Logger.getLogger(CatSSOUtil.class);

	/**
	 * Method to check Request has SSO cookie or not.
	 *
	 * @param request
	 * @return boolean
	 */

	public boolean isSSOCookieExists(final HttpServletRequest request)
	{
		boolean cookieExists = false;
		final Cookie[] cookies = request.getCookies();
		if (ArrayUtils.isNotEmpty(cookies))
		{
			for (final Cookie cookie : cookies)
			{
				// If SAML Cookie is present, set the SAML authentication flag to true.
				if (cookie.getName().equalsIgnoreCase(Config.getParameter("sso.cookie.name")))
				{
					LOG.debug("isSSOCookieExists :: SAML Cookie found in request.");
					cookieExists = true;
					break;
				}
			}
		}
		LOG.info("isSSOCookieExists : Leaving with cookieExists =" + cookieExists);
		return cookieExists;
	}

}
