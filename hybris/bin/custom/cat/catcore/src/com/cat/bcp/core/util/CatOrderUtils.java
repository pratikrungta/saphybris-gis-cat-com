package com.cat.bcp.core.util;

import de.hybris.platform.b2b.model.B2BCommentModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.keygenerator.KeyGenerator;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;


/**
 * @author prrungta
 *
 */
public class CatOrderUtils
{
	@Resource(name = "orderCodeGenerator")
	private KeyGenerator keyGenerator;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource
	private ModelService modelService;


	public KeyGenerator getKeyGenerator()
	{
		return keyGenerator;
	}

	/**
	 * Sets the order entries.
	 *
	 * @param orderModel
	 *           the order model
	 * @param orderEntriesList
	 *           the order entries list
	 * @param orderList
	 *           the order list
	 * @param quantityList
	 *           the quantity list
	 */
	public void setOrderEntries(final AbstractOrderModel orderModel, final List<AbstractOrderEntryModel> orderEntriesList,
			final List<OrderModel> orderList, final List<Long> quantityList)
	{
		for (final AbstractOrderEntryModel entry : orderEntriesList)
		{
			final OrderModel localOrder = modelService.create(OrderModel.class);
			setBasicAttributes(orderModel, localOrder);
			final AbstractOrderEntryModel clonedEntry = modelService.clone(entry);

			if (entry.getConfigurable() != null && BooleanUtils.isTrue(entry.getConfigurable()))
			{
				clonedEntry.setBasePrice(entry.getBasePrice());
				clonedEntry.setTotalPrice(entry.getBasePrice());
				localOrder.setTotalPrice(entry.getBasePrice());
			}
			clonedEntry.setEntryNumber(entry.getEntryNumber());
			clonedEntry.setOrder(localOrder);
			modelService.save(clonedEntry);
			modelService.save(localOrder);
			modelService.refresh(localOrder);
			quantityList.add(entry.getQuantity());
			orderList.add(localOrder);
		}
	}

	/**
	 * Sets the basic attributes.
	 *
	 * @param orderModel
	 *           the order model
	 * @param localOrder
	 *           the local order
	 */
	private void setBasicAttributes(final AbstractOrderModel orderModel, final OrderModel localOrder)
	{
		final String clonedOrderCode = String.valueOf(getKeyGenerator().generate());
		localOrder.setCode(clonedOrderCode);
		localOrder.setUser(orderModel.getUser());
		localOrder.setDate(new Date());
		localOrder.setUnit(orderModel.getUnit());
		localOrder.setStore(orderModel.getStore());
		localOrder.setSite(orderModel.getSite());
		localOrder.setCurrency(orderModel.getCurrency());
		if (baseStoreService.getCurrentBaseStore() != null)
		{
			localOrder.setStore(baseStoreService.getCurrentBaseStore());
		}
		if (baseSiteService.getCurrentBaseSite() != null)
		{
			localOrder.setSite(baseSiteService.getCurrentBaseSite());
		}
		if (orderModel.getDeliveryAddress() != null)
		{
			localOrder.setDeliveryAddress(orderModel.getDeliveryAddress());
		}
		localOrder.setPaymentAddress(orderModel.getPaymentAddress());
		setReorderComments(orderModel, localOrder);

	}

	/**
	 * Method to set re-order comment
	 *
	 * @param orderModel
	 *           - ordermodel
	 * @param localOrder
	 *           - local order
	 */
	private void setReorderComments(final AbstractOrderModel orderModel, final OrderModel localOrder)
	{
		if (CollectionUtils.isNotEmpty(orderModel.getB2bcomments()))
		{
			final Optional<B2BCommentModel> parentOrderComment = orderModel.getB2bcomments().stream().findFirst();
			if (parentOrderComment.isPresent())
			{
				final B2BCommentModel reOdercommnt = modelService.create(B2BCommentModel.class);
				reOdercommnt.setComment(parentOrderComment.get().getComment());
				modelService.save(reOdercommnt);
				localOrder.setB2bcomments(Collections.singleton(reOdercommnt));
			}
		}
	}
}
