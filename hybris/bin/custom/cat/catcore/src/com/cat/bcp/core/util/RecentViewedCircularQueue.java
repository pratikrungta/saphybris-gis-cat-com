/**
 *
 */
package com.cat.bcp.core.util;

import java.util.LinkedList;


/**
 * @author sagdhingra
 *
 */
public class RecentViewedCircularQueue<E> extends LinkedList<E>
{

	private final int limit;

	public RecentViewedCircularQueue(final int limit)
	{
		this.limit = limit;
	}


	@Override
	public boolean add(final E o)
	{
		final boolean added = super.add(o);
		while (added && size() > limit)
		{
			super.remove();
		}
		return added;
	}

}