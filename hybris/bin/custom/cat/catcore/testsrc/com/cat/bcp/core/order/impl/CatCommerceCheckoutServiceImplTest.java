/**
 *
 */
package com.cat.bcp.core.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.cat.bcp.core.dao.CatCommerceCheckoutDao;
import com.cat.bcp.core.model.TruckloadModel;
import com.cat.bcp.core.servicelayer.CatCartService;


/**
 * @author avaijapurkar
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CatCommerceCheckoutServiceImplTest
{


	CatCommerceCheckoutServiceImpl catCheckoutService;

	@Mock
	CatCommerceCheckoutDao catCommerceCheckoutDao;

	@Mock
	CatCartService cartService;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		catCheckoutService = new CatCommerceCheckoutServiceImpl();
		catCheckoutService.setCatCommerceCheckoutDao(catCommerceCheckoutDao);
	}

	/**
	 * Test method for
	 * {@link com.cat.bcp.core.order.impl.CatCommerceCheckoutServiceImpl#populateTruckloadSuggestions(int, int)}.
	 */
	@Test
	public void testPopulateTruckloadSuggestions()
	{
		final List<TruckloadModel> truckList = new ArrayList();
		final TruckloadModel truckloadModel = Mockito.mock(TruckloadModel.class);
		truckloadModel.setTwoSeaterCapacity(6);
		truckloadModel.setFiveSeaterCapacity(0);
		truckList.add(truckloadModel);

		final TruckloadModel truckloadModel2 = Mockito.mock(TruckloadModel.class);
		truckloadModel2.setTwoSeaterCapacity(0);
		truckloadModel2.setFiveSeaterCapacity(6);


		final SearchResult<TruckloadModel> searchResult = Mockito.mock(SearchResult.class);
		Mockito.when(searchResult.getResult()).thenReturn(truckList);
		Mockito.when(catCommerceCheckoutDao.getFiveSeaterSuggestion(5)).thenReturn(searchResult);
		Mockito.when(catCommerceCheckoutDao.getTwoSeaterSuggestion(1)).thenReturn(searchResult);


		Assert.assertNotNull(catCheckoutService.populateTruckloadSuggestions(1, 5));

	}

	/**
	 * Test method for
	 * {@link com.cat.bcp.core.order.impl.CatCommerceCheckoutServiceImpl#removeTruckload(java.lang.String)}.
	 *
	 * @throws CalculationException
	 */
	@Test
	public void testRemoveTruckload() throws CalculationException
	{
		final CartModel cartModel = Mockito.mock(CartModel.class);
		final AbstractOrderEntryModel oem = Mockito.mock(AbstractOrderEntryModel.class);
		oem.setTruckloadId("123");
		oem.setEntryNumber(1);
		final List<AbstractOrderEntryModel> oemList = new ArrayList();
		oemList.add(oem);
		cartModel.setEntries(oemList);
		final CatCartService cartService = Mockito.mock(CatCartService.class);
		ReflectionTestUtils.setField(catCheckoutService, "cartService", cartService);
		Mockito.when(cartService.getSessionCart()).thenReturn(cartModel);
		Mockito.when(cartModel.getEntries()).thenReturn(oemList);
		Mockito.when(oem.getTruckloadId()).thenReturn("123");
		catCheckoutService.removeTruckload("123");
	}

}
