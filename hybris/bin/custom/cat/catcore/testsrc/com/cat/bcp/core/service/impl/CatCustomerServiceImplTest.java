/**
 *
 */
package com.cat.bcp.core.service.impl;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.cat.bcp.core.dao.CatCustomerDao;
import com.cat.bcp.core.model.CatCountyModel;


/**
 * @author sagdhingra
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CatCustomerServiceImplTest
{

	CatCustomerServiceImpl catCustomerServiceImpl;

	@Mock
	private CatCustomerDao catCustomerDao;

	@Mock
	private ModelService modelService;


	@Mock
	private B2BUnitModel b2bUnitModel;

	List<CatCountyModel> catCountyList = new ArrayList<>();

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		catCustomerServiceImpl = new CatCustomerServiceImpl();
		catCustomerServiceImpl.setCatCustomerDao(catCustomerDao);
		catCustomerServiceImpl.setModelService(modelService);
		catCountyList = prepareCountyModelList();
	}


	private List<CatCountyModel> prepareCountyModelList()
	{
		final List<CatCountyModel> catCountyList = new ArrayList<>();
		final CatCountyModel catCountyModel = mock(CatCountyModel.class);
		Mockito.when(catCountyModel.getMainDealer()).thenReturn(b2bUnitModel);
		Mockito.when(catCountyModel.getBranchDealer()).thenReturn(b2bUnitModel);
		Mockito.when(catCountyModel.getIsMapped()).thenReturn(Boolean.TRUE);
		Mockito.when(catCountyModel.getCountyName()).thenReturn("Meade");
		catCountyList.add(catCountyModel);
		return catCountyList;
	}

	/**
	 * Test method for
	 * {@link com.cat.bcp.core.service.impl.CatCustomerServiceImpl#getCountyDataForDealer(java.lang.String, java.lang.String, java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	public void testGetCountyDataForDealer()
	{
		final String dealerShipTo = "testShip";
		final String dealerCode = "testCode";
		final String sortParameter = "sortParam";
		final String sortCode = "sortCode";
		Assert.assertNotNull(catCustomerServiceImpl.getCountyDataForDealer(dealerShipTo, dealerCode, sortParameter, sortCode));
	}

	/**
	 * Test method for
	 * {@link com.cat.bcp.core.service.impl.CatCustomerServiceImpl#unlinkCounty(com.cat.bcp.core.model.CatCountyModel)}.
	 */
	@Test
	public void testUnlinkCounty()
	{
		final CatCountyModel catCountyModel = mock(CatCountyModel.class);
		catCountyModel.setIsMapped(Boolean.FALSE);
		doNothing().when(modelService).save(catCountyModel);
		Assert.assertEquals(Boolean.FALSE, catCountyModel.getIsMapped());
		catCustomerServiceImpl.unlinkCounty(catCountyModel);
	}

	/**
	 * Test method for
	 * {@link com.cat.bcp.core.service.impl.CatCustomerServiceImpl#getAvailableCountyForShipTo(java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	public void testGetAvailableCountyForShipTo()
	{
		final String mainDealerCode = "E170";
		Assert.assertNotNull(catCustomerServiceImpl.getAvailableCountyForShipTo(mainDealerCode));

	}

}
