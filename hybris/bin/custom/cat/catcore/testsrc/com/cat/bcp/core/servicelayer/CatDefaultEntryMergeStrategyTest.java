/**
 *
 */
package com.cat.bcp.core.servicelayer;

import static org.mockito.Mockito.mock;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.cat.bcp.core.model.PartVariantProductModel;


/**
 * @author sagdhingra
 *
 */
public class CatDefaultEntryMergeStrategyTest
{

	CatDefaultEntryMergeStrategy catDefaultEntryMergeStrategy;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		catDefaultEntryMergeStrategy = new CatDefaultEntryMergeStrategy();
	}

	/**
	 * Test method for
	 * {@link com.cat.bcp.core.servicelayer.CatDefaultEntryMergeStrategy#getEntryToMerge(java.util.List, de.hybris.platform.core.model.order.AbstractOrderEntryModel)}.
	 */
	@Test
	public void testGetEntryToMerge()
	{
		final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();

		final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
		final PartVariantProductModel productModel = mock(PartVariantProductModel.class);
		Mockito.when(productModel.getCode()).thenReturn("545-8297");
		Mockito.when(orderEntry.getProduct()).thenReturn(productModel);
		Mockito.when(orderEntry.getCompatibleModel()).thenReturn("CUV82");
		Mockito.when(orderEntry.getConfigVariantId()).thenReturn("");
		orderEntries.add(orderEntry);
		catDefaultEntryMergeStrategy.getEntryToMerge(orderEntries, orderEntry);

	}

	@Test
	public void testGetEntryToMergeAccessoryWithNoCompatibleModel()
	{
		final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();

		final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
		final PartVariantProductModel productModel = mock(PartVariantProductModel.class);
		Mockito.when(productModel.getCode()).thenReturn("545-8297");
		Mockito.when(orderEntry.getProduct()).thenReturn(productModel);
		Mockito.when(orderEntry.getCompatibleModel()).thenReturn("");
		Mockito.when(orderEntry.getConfigVariantId()).thenReturn("");
		final AbstractOrderEntryModel orderEntry1 = mock(AbstractOrderEntryModel.class);
		final PartVariantProductModel productModel1 = mock(PartVariantProductModel.class);
		Mockito.when(productModel1.getCode()).thenReturn("545-8298");
		Mockito.when(orderEntry1.getProduct()).thenReturn(productModel);
		Mockito.when(orderEntry1.getCompatibleModel()).thenReturn("");
		Mockito.when(orderEntry1.getConfigVariantId()).thenReturn("");
		orderEntries.add(orderEntry1);
		catDefaultEntryMergeStrategy.getEntryToMerge(orderEntries, orderEntry);

	}

	@Test
	public void testGetEntryToMergeAccessoryWithSameCompatibleModelsandDiffProducts()
	{
		final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();

		final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
		final PartVariantProductModel productModel = mock(PartVariantProductModel.class);
		Mockito.when(productModel.getCode()).thenReturn("545-8297");
		Mockito.when(orderEntry.getProduct()).thenReturn(productModel);
		Mockito.when(orderEntry.getCompatibleModel()).thenReturn("CUV82");
		Mockito.when(orderEntry.getConfigVariantId()).thenReturn("");
		final AbstractOrderEntryModel orderEntry1 = mock(AbstractOrderEntryModel.class);
		final PartVariantProductModel productModel1 = mock(PartVariantProductModel.class);
		Mockito.when(productModel1.getCode()).thenReturn("545-8298");
		Mockito.when(orderEntry1.getProduct()).thenReturn(productModel);
		Mockito.when(orderEntry1.getCompatibleModel()).thenReturn("CUV82");
		Mockito.when(orderEntry1.getConfigVariantId()).thenReturn("");
		orderEntries.add(orderEntry1);
		catDefaultEntryMergeStrategy.getEntryToMerge(orderEntries, orderEntry);
	}

	@Test
	public void testGetEntryToMergeAccessoryWithDiffCompatibleModelsAndSameProducts()
	{
		final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
		final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
		final PartVariantProductModel productModel = mock(PartVariantProductModel.class);
		Mockito.when(productModel.getCode()).thenReturn("545-8297");
		Mockito.when(orderEntry.getProduct()).thenReturn(productModel);
		Mockito.when(orderEntry.getCompatibleModel()).thenReturn("");
		Mockito.when(orderEntry.getConfigVariantId()).thenReturn("");
		final AbstractOrderEntryModel orderEntry1 = mock(AbstractOrderEntryModel.class);
		final PartVariantProductModel productModel1 = mock(PartVariantProductModel.class);
		Mockito.when(productModel1.getCode()).thenReturn("545-8297");
		Mockito.when(orderEntry1.getProduct()).thenReturn(productModel);
		Mockito.when(orderEntry1.getCompatibleModel()).thenReturn("CUV82");
		Mockito.when(orderEntry1.getConfigVariantId()).thenReturn("");
		orderEntries.add(orderEntry1);
		catDefaultEntryMergeStrategy.getEntryToMerge(orderEntries, orderEntry);
	}


	@Test
	public void testGetEntryToMergeIfEntriesNull()
	{
		final List<AbstractOrderEntryModel> entries = null;
		final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
		catDefaultEntryMergeStrategy.getEntryToMerge(entries, orderEntry);

	}

	@Test
	public void testGetEntryToMergeWithConfigIdPresent()
	{
		final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();

		final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
		final PartVariantProductModel productModel = mock(PartVariantProductModel.class);
		Mockito.when(productModel.getCode()).thenReturn("545-8297");
		Mockito.when(orderEntry.getProduct()).thenReturn(productModel);
		Mockito.when(orderEntry.getCompatibleModel()).thenReturn("CUV82");
		Mockito.when(orderEntry.getConfigVariantId()).thenReturn("XYZ");
		orderEntries.add(orderEntry);
		catDefaultEntryMergeStrategy.getEntryToMerge(orderEntries, orderEntry);
	}

	@Test
	public void testGetEntryToMergeWithSameConfigIdPresent()
	{
		final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();
		final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
		final PartVariantProductModel productModel = mock(PartVariantProductModel.class);
		Mockito.when(productModel.getCode()).thenReturn("545-8297");
		Mockito.when(orderEntry.getProduct()).thenReturn(productModel);
		Mockito.when(orderEntry.getCompatibleModel()).thenReturn("");
		Mockito.when(orderEntry.getConfigVariantId()).thenReturn("XYZ");
		final AbstractOrderEntryModel orderEntry1 = mock(AbstractOrderEntryModel.class);
		final PartVariantProductModel productModel1 = mock(PartVariantProductModel.class);
		Mockito.when(productModel1.getCode()).thenReturn("545-8297");
		Mockito.when(orderEntry1.getProduct()).thenReturn(productModel);
		Mockito.when(orderEntry1.getCompatibleModel()).thenReturn("");
		Mockito.when(orderEntry1.getConfigVariantId()).thenReturn("XYZ");
		orderEntries.add(orderEntry1);
		catDefaultEntryMergeStrategy.getEntryToMerge(orderEntries, orderEntry);
	}


	@Test
	public void testGetEntryToMergeWithEpp()
	{
		final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();

		final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
		final ProductModel productModel = mock(ProductModel.class);
		Mockito.when(productModel.getCode()).thenReturn("EPPProduct");
		Mockito.when(orderEntry.getProduct()).thenReturn(productModel);
		Mockito.when(orderEntry.getCompatibleModel()).thenReturn("");
		Mockito.when(orderEntry.getConfigVariantId()).thenReturn("XYZ");
		orderEntries.add(orderEntry);
		catDefaultEntryMergeStrategy.getEntryToMerge(orderEntries, orderEntry);
	}

	@Test
	public void testGetEntryToMergeWithCSA()
	{
		final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();

		final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
		final ProductModel productModel = mock(ProductModel.class);
		Mockito.when(productModel.getCode()).thenReturn("CSAProduct");
		Mockito.when(orderEntry.getProduct()).thenReturn(productModel);
		Mockito.when(orderEntry.getCompatibleModel()).thenReturn("");
		Mockito.when(orderEntry.getConfigVariantId()).thenReturn("XYZ");
		orderEntries.add(orderEntry);
		catDefaultEntryMergeStrategy.getEntryToMerge(orderEntries, orderEntry);
	}

	@Test
	public void testGetEntryToMergeWithConfigVariantProduct()
	{
		final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();

		final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
		final ProductModel productModel = mock(ProductModel.class);
		Mockito.when(productModel.getCode()).thenReturn("XYZ");
		Mockito.when(orderEntry.getProduct()).thenReturn(productModel);
		Mockito.when(orderEntry.getCompatibleModel()).thenReturn("");
		Mockito.when(orderEntry.getConfigVariantId()).thenReturn("");
		orderEntries.add(orderEntry);
		catDefaultEntryMergeStrategy.getEntryToMerge(orderEntries, orderEntry);
	}

	@Test
	public void testGetEntryToMergeWithNoProductInEntry()
	{
		final List<AbstractOrderEntryModel> orderEntries = new ArrayList<>();

		final AbstractOrderEntryModel orderEntry = mock(AbstractOrderEntryModel.class);
		final ProductModel productModel = null;
		Mockito.when(orderEntry.getProduct()).thenReturn(productModel);
		Mockito.when(orderEntry.getCompatibleModel()).thenReturn("");
		Mockito.when(orderEntry.getConfigVariantId()).thenReturn("");
		orderEntries.add(orderEntry);
		catDefaultEntryMergeStrategy.getEntryToMerge(orderEntries, orderEntry);
	}
}
