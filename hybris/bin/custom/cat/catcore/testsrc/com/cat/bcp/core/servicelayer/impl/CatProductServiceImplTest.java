/**
 *
 */
package com.cat.bcp.core.servicelayer.impl;

import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.Assert;

import com.cat.bcp.core.dao.CatProductDao;


/**
 * @author sagdhingra
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CatProductServiceImplTest
{
	@Mock
	private CatProductDao catProductDao;

	CatProductServiceImpl catProductServiceImpl;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		catProductServiceImpl = new CatProductServiceImpl();
		catProductServiceImpl.setCatProductDao(catProductDao);
	}

	/**
	 * Test method for {@link com.cat.bcp.core.servicelayer.impl.CatProductServiceImpl#getAccessoryData()}.
	 */
	@Test
	public void testGetAccessoryData()
	{
		final List<ProductReferenceModel> productRefList = new ArrayList<>();
		final ProductReferenceModel rfModel = mock(ProductReferenceModel.class);
		productRefList.add(rfModel);
		final ProductReferenceModel refModel = mock(ProductReferenceModel.class);
		productRefList.add(refModel);
		final ProductReferenceModel refModel3 = mock(ProductReferenceModel.class);
		productRefList.add(refModel3);
		Mockito.when(catProductDao.getAccessoryData()).thenReturn(productRefList);
		Assert.notNull(catProductServiceImpl.getAccessoryData());
	}

	/**
	 * Test method for {@link com.cat.bcp.core.servicelayer.impl.CatProductServiceImpl#getCUVModels()}.
	 */
	@Test
	public void testGetCUVModels()
	{
		final List<ProductModel> productModelList = new ArrayList<>();
		final ProductModel mockProduct1 = mock(ProductModel.class);
		mockProduct1.setCode("CUV82");
		final ProductModel mockProduct2 = mock(ProductModel.class);
		mockProduct2.setCode("CUV85");
		final ProductModel mockProduct3 = mock(ProductModel.class);
		mockProduct3.setCode("CUV102");
		final ProductModel mockProduct4 = mock(ProductModel.class);
		mockProduct4.setCode("CUV105");
		productModelList.add(mockProduct1);
		productModelList.add(mockProduct2);
		productModelList.add(mockProduct3);
		productModelList.add(mockProduct4);
		Mockito.when(catProductDao.getCUVModels()).thenReturn(productModelList);
		Assert.notNull(catProductServiceImpl.getCUVModels());
	}

}
