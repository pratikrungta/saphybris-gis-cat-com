/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at May 15, 2018 9:31:57 PM                     ---
 * ----------------------------------------------------------------
 */
package com.cat.bcp.facades.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedCatFacadesConstants
{
	public static final String EXTENSIONNAME = "catfacades";
	
	protected GeneratedCatFacadesConstants()
	{
		// private constructor
	}
	
	
}
