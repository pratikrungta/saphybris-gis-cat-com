/**
 *
 */
package com.cat.bcp.facade.export;

import de.hybris.platform.commercefacades.quote.data.QuoteData;
import de.hybris.platform.commercefacades.user.data.CustomerData;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.util.Map;


/**
 * The Interface CatQuoteExportFacade.
 *
 * @author amitsinha5
 */
public interface CatQuoteExportFacade
{


    /**
     * Gets the export file stream.
     *
     * @param filePath
     *           the file path
     * @return the export file stream
     */
     FileInputStream getExportFileStream(final String filePath);

    /**
     * Prepare export document.
     *
     * @param fileStream
     *           the file stream
     * @param quoteData
     *           the quote data
     * @param customerdata
     *           the customerdata
     * @param localeMap
     *           the locale map
     * @param hostURL
     *           the host URL
     */
     void prepareExportDocument(FileInputStream fileStream, QuoteData quoteData, CustomerData customerdata,
            Map<String, String> localeMap, String hostURL);


    /**
     * Prints the export word.
     *
     * @param file
     *           the file
     * @param response
     *           the response
     * @param quoteData
     *           the quote data
     */
     void printExportWord(final File file, final HttpServletResponse response, final QuoteData quoteData);



}




