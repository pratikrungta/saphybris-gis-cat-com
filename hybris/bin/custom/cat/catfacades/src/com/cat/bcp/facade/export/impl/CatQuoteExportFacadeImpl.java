/**
 *
 */
package com.cat.bcp.facade.export.impl;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.quote.export.CatQuoteExportStrategy;
import com.cat.bcp.facade.export.CatQuoteExportFacade;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.quote.data.QuoteData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * @author amitsinha5
 *
 */
public class CatQuoteExportFacadeImpl implements CatQuoteExportFacade
{
    private static final Logger LOG = Logger.getLogger(CatQuoteExportFacadeImpl.class);

    private CatQuoteExportStrategy catQuoteExportStrategy;

    private ProductFacade productFacade;

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public FileInputStream getExportFileStream(final String filePath)
    {
        FileInputStream fileStream = null;
        final ClassLoader classLoader = getClass().getClassLoader();
        if (classLoader.getResource(CatCoreConstants.EXPORT_QUOTE_FILE) != null)
        {
            try
            {
                fileStream = new FileInputStream(classLoader.getResource(filePath).getFile());
            }
            catch (final IOException ex)
            {
                LOG.error("Export file is not found or file is not available on given absolute path ", ex);
            }
            catch (final Exception ex)
            {
                LOG.error("Unable to process file & export for given template file due to unexpected error", ex);
            }
        }

        return fileStream;
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public void prepareExportDocument(final FileInputStream fileStream, final QuoteData quoteData, final CustomerData customerdata,
            final Map<String, String> localeMap, final String hostURL)
    {
        final List<ProductData> productList = getProductList(quoteData);

        catQuoteExportStrategy.prepareExportDocument(fileStream, quoteData, customerdata, productList, localeMap, hostURL);
    }


    /**
     * {@inheritDoc}
     *
     */
    @Override
    public void printExportWord(final File file, final HttpServletResponse response, final QuoteData quoteData)
    {
        response.setContentType("application/msword");
        response.setContentLength((int) file.length());

        response.addHeader("Content-Disposition", "inline; filename=" + CatCoreConstants.QUOTE_EXPORT_NAME_PREFIX
                + quoteData.getName() + CatCoreConstants.QUOTE_EXPORT_FILE_EXT);

        try (FileInputStream fileInputStream = new FileInputStream(file))
        {
            final OutputStream responseOutputStream = response.getOutputStream();
            int bytes;
            while ((bytes = fileInputStream.read()) != -1)
            {
                responseOutputStream.write(bytes);
            }

        }

        catch (final IOException ex)
        {
            LOG.error("Export file is not found or file is not available on given absolute path ", ex);
        }
    }

    /**
     * Gets the product list.
     *
     * @param quoteData
     *           the quote data
     * @return the product list
     */
    private List<ProductData> getProductList(final QuoteData quoteData)
    {
        final List<ProductData> productList = new ArrayList<ProductData>();

        final List<ProductOption> options = new ArrayList<>(Arrays.asList(ProductOption.BASIC, ProductOption.URL,
                ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION, ProductOption.GALLERY,
                ProductOption.CATEGORIES, ProductOption.REVIEW, ProductOption.PROMOTIONS, ProductOption.CLASSIFICATION,
                ProductOption.VARIANT_FULL, ProductOption.STOCK, ProductOption.VOLUME_PRICES, ProductOption.PRICE_RANGE,
                ProductOption.DELIVERY_MODE_AVAILABILITY, ProductOption.REFERENCES, ProductOption.VARIANT_MATRIX_BASE,
                ProductOption.VARIANT_MATRIX_URL, ProductOption.VARIANT_MATRIX_MEDIA));

        if (CollectionUtils.isNotEmpty(quoteData.getEntries()))
        {

            for (final OrderEntryData orderEntry : quoteData.getEntries())
            {

                if (orderEntry.getProduct() != null)
                {

                    final ProductData productData = productFacade.getProductForCodeAndOptions(orderEntry.getProduct().getCode(),
                            options);

                    productList.add(productData);
                }
            }

        }
        return productList;
    }

    /**
     * @return the catQuoteExportStrategy
     */
    public CatQuoteExportStrategy getCatQuoteExportStrategy()
    {
        return catQuoteExportStrategy;
    }

    /**
     * @param catQuoteExportStrategy
     *           the catQuoteExportStrategy to set
     */
    @Required
    public void setCatQuoteExportStrategy(final CatQuoteExportStrategy catQuoteExportStrategy)
    {
        this.catQuoteExportStrategy = catQuoteExportStrategy;
    }

    /**
     * @return the productFacade
     */
    protected ProductFacade getProductFacade()
    {
        return productFacade;
    }

    /**
     * @param productFacade
     *           the productFacade to set
     */
    @Required
    public void setProductFacade(final ProductFacade productFacade)
    {
        this.productFacade = productFacade;
    }
}