/**
 *
 */
package com.cat.bcp.facade.product;

import de.hybris.platform.cmsfacades.data.MediaData;
import de.hybris.platform.commercefacades.product.ProductFacade;

import java.util.List;
import java.util.Map;

import com.cat.facades.order.CatLaneSpecificDataForPDP;
import com.cat.facades.product.CatAccessoryData;


/**
 * @author sagdhingra
 *
 */
public interface CatProductFacade extends ProductFacade
{
	/**
	 *
	 * This method will return accessory data for utility vehicles
	 *
	 * @return Map<String, List<CatAccessoryData>>
	 */
	Map<String, List<CatAccessoryData>> getAccessoryData();

	/**
	 * This method will return cuv models for utility vehicles
	 *
	 * @return List>String
	 */
	List<String> getCUVModels();

	/**
	 * This method is used to get Coaching Tools Data for product.
	 *
	 * @param productCode
	 *           product Code
	 * @return List of MediaData Object
	 */
	List<MediaData> getCoachingToolsData(final String productCode);

	/**
	 * This method is used to get Lane Specific Data for PDP
	 *
	 * @param productCode
	 *           configuration productCode
	 * @param salesModel
	 *           sales Model code
	 * @return CatLaneSpecificData Object
	 */
	CatLaneSpecificDataForPDP getLaneSpecificData(String productCode, String salesModel);
}
