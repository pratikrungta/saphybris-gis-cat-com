/**
 *
 */
package com.cat.bcp.facade.product;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.cat.bcp.core.model.RestockAlertModel;
import com.cat.facades.product.InventoryAgeData;


/**
 * The Interface CatStockFacade.
 *
 * @author bidavda
 */
public interface CatStockFacade
{

	/**
	 * Populates stock for product.
	 *
	 * @param productModel
	 *           Product Model
	 * @return List of StockData
	 */
	List<StockData> populateStockForProduct(final ProductModel productModel);

	/**
	 * This method will update alert entry after the reorder is placed.
	 *
	 * @param alertId
	 *           Alert Id
	 * @param configProduct
	 *           Config Variant Product
	 * @param orderedQty
	 *           Ordered quantity
	 */
	void updateAlertEntry(String alertId, String configProduct, String orderedQty);


	/**
	 * This method will get stock for sales model.
	 *
	 * @param alertId
	 *           Alert Id
	 * @return RestockAlertModel
	 */
	RestockAlertModel getStockForSalesModel(String alertId);

	/**
	 * Gets the stock level for dealer warehouse.
	 *
	 * @param productModel
	 *           the product model
	 * @return the stock level for dealer warehouse
	 */
	Long getStockLevelForDealerWarehouse(final ProductModel productModel);

	/**
	 * Method to check if the associated config product exist based on alert id.
	 *
	 * @param salesModelId
	 *           Sales Model Id
	 * @param productModel
	 *           ProductModel Object
	 * @param customerModel
	 *           Object
	 * @return Boolean alert products model
	 */
	Boolean isIMAddToCartConfigProduct(final String salesModelId, final ProductModel productModel,
			final B2BCustomerModel customerModel);

	/**
	 * Gets the inventory age for product.
	 *
	 * @param productModel
	 *           the product model
	 * @return the inventory age for product
	 */
	List<InventoryAgeData> getInventoryAgeForProduct(final ProductModel productModel);

	/**
	 * This method return Order window for product
	 *
	 * @param prodList
	 *           - prod List
	 * @return Order window - map
	 */
	Map getOrderWindowAndShipAfterDates(List<ProductModel> prodList);

	/**
	 * This method returns product list for ship after date
	 *
	 * @param date
	 *           - ship after date
	 * @return - list of Products
	 */
	List<ProductData> getProductsForShipAfterDate(Date date);
}
