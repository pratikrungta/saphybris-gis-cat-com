/**
 *
 */
package com.cat.bcp.facade.product.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.cmsfacades.data.MediaData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.impl.DefaultProductFacade;
import de.hybris.platform.core.model.product.ProductModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.enums.LaneTypeEnum;
import com.cat.bcp.core.model.AlertProductModel;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.PartVariantProductModel;
import com.cat.bcp.core.model.ProductLaneInfoModel;
import com.cat.bcp.core.model.RestockAlertModel;
import com.cat.bcp.core.order.CatReorderService;
import com.cat.bcp.core.servicelayer.CatOrderLaneProductService;
import com.cat.bcp.core.servicelayer.CatProductService;
import com.cat.bcp.core.servicelayer.impl.CatOrderWindowCalculationUtility;
import com.cat.bcp.facade.product.CatProductFacade;
import com.cat.bcp.facade.product.CatStockFacade;
import com.cat.bcp.facades.comparator.CatProductNameComparator;
import com.cat.bcp.facades.comparator.CatProductSeatCapacityComparator;
import com.cat.bcp.facades.order.CatCartFacade;
import com.cat.bcp.facades.order.CatReorderFacade;
import com.cat.bcp.facades.populators.CatProductDetailsPopulator;
import com.cat.facades.order.CatErrorData;
import com.cat.facades.order.CatLaneOneData;
import com.cat.facades.order.CatLaneSpecificDataForPDP;
import com.cat.facades.order.CatLaneThreeData;
import com.cat.facades.order.CatLaneTwoData;
import com.cat.facades.order.CatStockingRecommendationData;
import com.cat.facades.product.CatAccessoryData;
import com.cat.facades.product.CatCartAccessoryData;


/**
 * @author sagdhingra
 *
 */
public class CatProductFacadeImpl extends DefaultProductFacade implements CatProductFacade
{
	private static final Logger LOG = Logger.getLogger(CatProductFacadeImpl.class);
	private static final String RECOMMENDED = "recommended";
	private static final String NAME = "name";
	private static final String LANE3 = "lane3";
	private static final String LANE2 = "lane2";
	private static final String CLOSED = "closed";
	private static final String MONTH_YEAR_FORMAT = "MMMM yyyy";

	@Resource(name = "catProductService")
	private CatProductService catProductService;

	@Resource(name = "cartFacade")
	private CatCartFacade cartFacade;

	@Resource(name = "productPopulator")
	private CatProductDetailsPopulator productPopulator;

	@Resource(name = "catReorderFacade")
	private CatReorderFacade catReorderFacade;

	@Resource(name = "catStockFacade")
	private CatStockFacade catStockFacade;

	@Resource(name = "catOrderLaneProductService")
	private CatOrderLaneProductService catOrderLaneProductService;

	@Resource(name = "catOrderWindowCalculationUtility")
	private CatOrderWindowCalculationUtility catOrderWindowCalculationUtility;

	@Resource(name = "catReorderService")
	private CatReorderService catReorderService;

	/**
	 * @param cartFacade
	 *           the cartFacade to set
	 */
	public void setCartFacade(final CatCartFacade cartFacade)
	{
		this.cartFacade = cartFacade;
	}

	/**
	 * @param catProductService
	 *           the catProductService to set
	 */
	public void setCatProductService(final CatProductService catProductService)
	{
		this.catProductService = catProductService;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCUVModels()
	{
		final List<ProductModel> productModelList = catProductService.getCUVModels();
		final List<ProductData> prodDataList = getProductConverter().convertAll(productModelList);
		Collections.sort(prodDataList, new CatProductNameComparator());
		Collections.reverse(prodDataList);
		Collections.sort(prodDataList, new CatProductSeatCapacityComparator());
		return prodDataList.stream().map(productData -> productData.getCode()).collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<MediaData> getCoachingToolsData(final String productCode)
	{
		final ProductData productData = new ProductData();
		final ProductModel productModel = getProductService().getProductForCode(productCode);
		productPopulator.populate(productModel, productData);
		return productData.getCoachingToolLinks();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, List<CatAccessoryData>> getAccessoryData()
	{
		final Map<String, List<CatAccessoryData>> catAccessoryDataMap = new LinkedHashMap<>();
		final List<ProductReferenceModel> productModelList = catProductService.getAccessoryData();
		if (CollectionUtils.isNotEmpty(productModelList))
		{
			final Map<String, List<ProductReferenceModel>> accessryDataMap = productModelList.stream()
					.collect(Collectors.groupingBy(ProductReferenceModel::getDescription));
			for (final Entry<String, List<ProductReferenceModel>> entry : accessryDataMap.entrySet())
			{
				final Map<PartVariantProductModel, List<String>> combineCompatibleModel = new LinkedHashMap<>();
				combineCompatibleModelWithPartVariant(combineCompatibleModel, entry);
				catAccessoryDataMap.put(entry.getKey(), prepareListOfCatAccessoryData(combineCompatibleModel));
			}
			sortAccessoryMapByRecommendedFlagOrName(catAccessoryDataMap, NAME);
			sortAccessoryMapByRecommendedFlagOrName(catAccessoryDataMap, RECOMMENDED);
		}
		return catAccessoryDataMap;
	}

	/**
	 * Method to combine compatible model with part variant
	 *
	 * @param combineCompatibleModel
	 * @param entry
	 */
	private void combineCompatibleModelWithPartVariant(final Map<PartVariantProductModel, List<String>> combineCompatibleModel,
			final Entry<String, List<ProductReferenceModel>> entry)
	{
		for (final ProductReferenceModel productRef : entry.getValue())
		{
			final List<String> sourceProductList = new ArrayList<>();
			final PartVariantProductModel product = (PartVariantProductModel) productRef.getTarget();
			if (combineCompatibleModel.containsKey(product))
			{
				combineCompatibleModel.get(product)
						.add(((BaseVariantProductModel) productRef.getSource()).getBaseProduct().getCode());
			}
			else
			{
				sourceProductList.add(((BaseVariantProductModel) productRef.getSource()).getBaseProduct().getCode());
				combineCompatibleModel.put(product, sourceProductList);
			}
		}
	}

	/**
	 * Method to sort the list of Product Variant Data based on the recommended and name
	 *
	 * @param accessryDataMap
	 *           get the list out of the map and sort it
	 *
	 * @param parameter
	 *           name or recommended flag
	 */
	private void sortAccessoryMapByRecommendedFlagOrName(final Map<String, List<CatAccessoryData>> accessryDataMap,
			final String parameter)
	{
		for (final Entry<String, List<CatAccessoryData>> entry : accessryDataMap.entrySet())
		{
			if ((RECOMMENDED).equalsIgnoreCase(parameter))
			{
				Collections.sort(entry.getValue(), (val1, val2) -> Boolean.compare(val2.getIsRecommended().booleanValue(),
						val1.getIsRecommended().booleanValue()));
			}
			else if ((NAME).equalsIgnoreCase(parameter))
			{
				Collections.sort(entry.getValue(), (val1, val2) -> StringUtils.compare(val1.getName(), val2.getName()));
			}
		}
	}

	/**
	 * Method to return the list of cat accessory data
	 *
	 * @param prepareVariantList
	 *           Map to prepare a list of accessory data
	 * @return List<CatAccessoryData> list of cat accessory data
	 */
	private List<CatAccessoryData> prepareListOfCatAccessoryData(
			final Map<PartVariantProductModel, List<String>> prepareVariantList)
	{
		final List<CatAccessoryData> catAccessoryDataList = new ArrayList<>();
		final Map<String, List<OrderEntryData>> cartDataMap = getCartDataMap();
		for (final Entry<PartVariantProductModel, List<String>> entry : prepareVariantList.entrySet())
		{
			final CatAccessoryData catAccessoryData = new CatAccessoryData();
			getProductConverter().convert(entry.getKey(), catAccessoryData);
			catAccessoryData.setIsRecommended(entry.getKey().getRecommendedFlag());
			catAccessoryData.setCompatibleModels(entry.getValue());
			populateCartItemsWithAccessoryData(cartDataMap, entry, catAccessoryData);
			catAccessoryData.setMaxQuantity(Integer.valueOf(catAccessoryData.getCompatibleModels().size()));
			catAccessoryDataList.add(catAccessoryData);
		}
		return catAccessoryDataList;
	}

	/**
	 * Method to return cart data map if not empty
	 *
	 *
	 * @return Map<String, List<OrderEntryData>> map with key as Product Code and value as list of entries
	 */
	private Map<String, List<OrderEntryData>> getCartDataMap()
	{
		final Map<String, List<OrderEntryData>> cartDataMap = new HashMap<>();
		final CartData cartData = cartFacade.getSessionCart();
		if (null != cartData && CollectionUtils.isNotEmpty(cartData.getEntries()))
		{
			for (final OrderEntryData entryData : cartData.getEntries())
			{
				List<OrderEntryData> orderEntryList = new ArrayList<>();
				if (cartDataMap.containsKey(entryData.getProduct().getCode()))
				{
					orderEntryList = cartDataMap.get(entryData.getProduct().getCode());
					orderEntryList.add(entryData);
				}
				else
				{
					orderEntryList.add(entryData);
				}
				cartDataMap.put(entryData.getProduct().getCode(), orderEntryList);
			}
		}
		return cartDataMap;
	}

	/**
	 * Method to populate cart data along with the accessory data
	 *
	 * @param cartDataMap
	 *           Map of string and list of entries
	 * @param entry
	 *           entry of the map containing part variant product model and compatible models
	 *
	 *
	 * @param catAccessoryData
	 *           catAccessoryData to be populated
	 */
	private void populateCartItemsWithAccessoryData(final Map<String, List<OrderEntryData>> cartDataMap,
			final Entry<PartVariantProductModel, List<String>> entry, final CatAccessoryData catAccessoryData)
	{
		if (MapUtils.isNotEmpty(cartDataMap))
		{
			List<OrderEntryData> orderEntryData = new ArrayList<>();
			for (final Entry<String, List<OrderEntryData>> productEntry : cartDataMap.entrySet())
			{
				if (productEntry.getKey().equalsIgnoreCase((entry.getKey().getCode())))
				{
					orderEntryData = productEntry.getValue();
				}
			}
			final List<CatCartAccessoryData> catCartAccessoryList = new ArrayList<>();
			for (final OrderEntryData orderEntry : orderEntryData)
			{
				final CatCartAccessoryData catCartData = new CatCartAccessoryData();
				catCartData.setQuantity(orderEntry.getQuantity());
				final PriceData priceData = orderEntry.getTotalPrice();
				catCartData.setTotalPrice(Double.valueOf(priceData.getValue().doubleValue()));
				catCartData.setFormattedTotalPrice(priceData.getFormattedValue());
				catCartData.setModelNumber(orderEntry.getCompatibleModel());
				catCartAccessoryList.add(catCartData);
			}
			catAccessoryData.setLineItems(catCartAccessoryList);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CatLaneSpecificDataForPDP getLaneSpecificData(final String productCode, final String salesModel)
	{
		final CatLaneSpecificDataForPDP catLaneSpecificDataForPDP = new CatLaneSpecificDataForPDP();
		catLaneSpecificDataForPDP.setErrorData(new ArrayList());
		final B2BCustomerModel user = (B2BCustomerModel) getUserService().getCurrentUser();
		final boolean isAlertProductPresent = setStockingRecommendation(productCode, salesModel, catLaneSpecificDataForPDP, user);
		final String alertId = salesModel + CatCoreConstants.UNDERSCORE + user.getDefaultB2BUnit().getUid();
		final RestockAlertModel restockAlert = catStockFacade.getStockForSalesModel(alertId);
		final boolean isRestockAlertPresent = (null != restockAlert) ? true : false;
		catLaneSpecificDataForPDP.setHideAddToCart(Boolean.FALSE);
		final CartData cartData = cartFacade.getSessionCart();
		if (null != cartData)
		{
			catLaneSpecificDataForPDP
					.setCartLaneType(null != cartData.getLaneType() ? cartData.getLaneType().getCode() : StringUtils.EMPTY);
			setCartQuantity(productCode, cartData, catLaneSpecificDataForPDP);
		}
		final List<ConfigVariantProductModel> configVariantLane1 = catReorderService.getCompatibleConfigProducts(null,
				LaneTypeEnum.LANE1.getCode(), productCode);
		if (CollectionUtils.isNotEmpty(configVariantLane1))
		{
			setLaneSpecificDataForLane1(catLaneSpecificDataForPDP, false, isAlertProductPresent);
		}

		final boolean isProductCompatibleToOrderForLane2 = isProductCompatibleToOrder(catLaneSpecificDataForPDP, productCode,
				LANE2);
		final boolean isProductCompatibleToOrderForLane3 = isProductCompatibleToOrder(catLaneSpecificDataForPDP, productCode,
				LANE3);
		final Map<String, Object> orderingMessageWindow = catOrderWindowCalculationUtility.getOrderingWindowMessage();
		final List<ProductLaneInfoModel> productLaneInfoModelList = catOrderLaneProductService.getLaneInfoForSalesModel(salesModel,
				user.getDefaultB2BUnit());
		if (CollectionUtils.isNotEmpty(productLaneInfoModelList))
		{
			for (final ProductLaneInfoModel productLaneInfoModel : productLaneInfoModelList)
			{
				setLaneSpecificData(orderingMessageWindow, catLaneSpecificDataForPDP, productLaneInfoModel,
						isProductCompatibleToOrderForLane2, isProductCompatibleToOrderForLane3, isRestockAlertPresent);
			}
		}
		populateDummyDataForLanesInCaseOfError(catLaneSpecificDataForPDP, orderingMessageWindow, isProductCompatibleToOrderForLane2,
				isProductCompatibleToOrderForLane3, isRestockAlertPresent);
		setAdditionalAttributes(productCode, catLaneSpecificDataForPDP);
		hideAddToCartIfLanesDataNotAvailable(catLaneSpecificDataForPDP);
		return catLaneSpecificDataForPDP;
	}

	/**
	 * This method is used to hide Add To Cart If Lane Data not available for any Lane.
	 *
	 * @param catLaneSpecificDataForPDP
	 *           catLaneSpecificDataForPDP
	 */
	private void hideAddToCartIfLanesDataNotAvailable(final CatLaneSpecificDataForPDP catLaneSpecificDataForPDP)
	{
		if (catLaneSpecificDataForPDP.getLane1() == null && catLaneSpecificDataForPDP.getLane2() == null
				&& catLaneSpecificDataForPDP.getLane3() == null)
		{
			catLaneSpecificDataForPDP.setHideAddToCart(Boolean.TRUE);
			final List<CatErrorData> errorDataList = catLaneSpecificDataForPDP.getErrorData();
			final CatErrorData catErrorData = new CatErrorData();
			catErrorData.setErrorMessage("Data unavailable for all the lanes.");
			errorDataList.add(catErrorData);
			catLaneSpecificDataForPDP.setErrorData(errorDataList);
		}
	}

	/**
	 * This method is used to set Cart Quantity for configuration.
	 *
	 * @param productCode
	 *           productCode
	 * @param cartData
	 *           cartData
	 * @param catLaneSpecificDataForPDP
	 *           catLaneSpecificDataForPDP
	 */
	private void setCartQuantity(final String productCode, final CartData cartData,
			final CatLaneSpecificDataForPDP catLaneSpecificDataForPDP)
	{
		final List<OrderEntryData> orderEntyDataList = cartData.getEntries();
		if (CollectionUtils.isNotEmpty(orderEntyDataList))
		{
			for (final OrderEntryData orderEntryData : orderEntyDataList)
			{
				if (orderEntryData.getProduct().getCode().equalsIgnoreCase(productCode))
				{
					catLaneSpecificDataForPDP.setCartProductQuantity(orderEntryData.getQuantity());
				}
			}
		}
	}

	/**
	 * Add Dummy Data for Lane2 and Lane3 in case of error Scenarios.
	 *
	 * @param catLaneSpecificDataForPDP
	 *           catLaneSpecificDataForPDP
	 * @param orderingMessageWindow
	 *           orderingMessageWindow
	 * @param isProductCompatibleToOrderForLane2
	 *           isProductCompatibleToOrderForLane2
	 * @param isProductCompatibleToOrderForLane3
	 *           isProductCompatibleToOrderForLane3
	 * @param isRestockAlertPresent
	 */
	private void populateDummyDataForLanesInCaseOfError(final CatLaneSpecificDataForPDP catLaneSpecificDataForPDP,
			final Map<String, Object> orderingMessageWindow, final boolean isProductCompatibleToOrderForLane2,
			final boolean isProductCompatibleToOrderForLane3, final boolean isRestockAlertPresent)
	{
		if (isProductCompatibleToOrderForLane2 && catLaneSpecificDataForPDP.getLane2() == null)
		{
			addDummyDataForLanesIfEntryNotPresent(orderingMessageWindow, LaneTypeEnum.LANE2, catLaneSpecificDataForPDP,
					isRestockAlertPresent);
			LOG.debug("Populating dummy data for Lane 2.");
		}
		if (isProductCompatibleToOrderForLane3 && catLaneSpecificDataForPDP.getLane3() == null)
		{
			addDummyDataForLanesIfEntryNotPresent(orderingMessageWindow, LaneTypeEnum.LANE3, catLaneSpecificDataForPDP,
					isRestockAlertPresent);
			LOG.debug("Populating dummy data for Lane 3.");
		}
	}

	/**
	 * This method is used to set Lane Specific Data.
	 *
	 * @param orderingMessageWindow
	 *           orderingMessageWindow
	 * @param catLaneSpecificDataForPDP
	 *           catLaneSpecificDataForPDP
	 * @param productLaneInfoModel
	 *           productLaneInfoModel
	 * @param isProductCompatibleToOrderForLane2
	 *           isProductCompatibleToOrderForLane2
	 * @param isProductCompatibleToOrderForLane3
	 *           isProductCompatibleToOrderForLane3
	 * @param isRestockAlertPresent
	 */
	private void setLaneSpecificData(final Map<String, Object> orderingMessageWindow,
			final CatLaneSpecificDataForPDP catLaneSpecificDataForPDP, final ProductLaneInfoModel productLaneInfoModel,
			final boolean isProductCompatibleToOrderForLane2, final boolean isProductCompatibleToOrderForLane3,
			final boolean isRestockAlertPresent)
	{
		if (orderingMessageWindow != null && productLaneInfoModel.getLaneInfo() != null)
		{
			if (productLaneInfoModel.getLaneInfo().getCode().equals(LaneTypeEnum.LANE2.getCode())
					&& isProductCompatibleToOrderForLane2)
			{
				setLaneSpecificDataForLane2(catLaneSpecificDataForPDP, productLaneInfoModel, orderingMessageWindow,
						isRestockAlertPresent);
			}
			if (productLaneInfoModel.getLaneInfo().getCode().equals(LaneTypeEnum.LANE3.getCode())
					&& isProductCompatibleToOrderForLane3)
			{
				setLaneSpecificDataForLane3(catLaneSpecificDataForPDP, productLaneInfoModel, orderingMessageWindow,
						isRestockAlertPresent);
			}
		}
		else
		{
			LOG.debug("ording Message Window is not present for Lane2 and Lane3.");
		}
	}

	/**
	 * This method is used to set Lane Specific Data for Lane3
	 *
	 * @param catLaneSpecificDataForPDP
	 *           catLaneSpecificDataForPDP
	 * @param productLaneInfoModel
	 *           productLaneInfoModel
	 * @param orderingMessageWindow
	 *           orderingMessageWindow
	 * @param isRestockAlertPresent
	 *           restock alert present
	 */
	private void setLaneSpecificDataForLane3(final CatLaneSpecificDataForPDP catLaneSpecificDataForPDP,
			final ProductLaneInfoModel productLaneInfoModel, final Map<String, Object> orderingMessageWindow,
			final boolean isRestockAlertPresent)
	{
		final CatLaneThreeData lane3 = new CatLaneThreeData();
		setOrderingWindowDataForLane3(orderingMessageWindow, lane3);
		lane3.setAllocationCount(Integer.valueOf(getOrderableQuantityCountLeft(productLaneInfoModel)));
		lane3.setMaximumCount(productLaneInfoModel.getMaximumOrderableQuantity());
		try
		{
			final Date date = new SimpleDateFormat("MM-yyyy").parse(productLaneInfoModel.getMonthYear());
			final SimpleDateFormat sdf = new SimpleDateFormat(MONTH_YEAR_FORMAT);
			lane3.setMonth_Year(sdf.format(date));
		}
		catch (final ParseException e)
		{
			LOG.error("Error while parsing Date", e);
		}
		lane3.setAddToCartDisabled(Boolean.valueOf(isAddToCartDisabled(productLaneInfoModel, lane3, LANE3)));
		lane3.setDisplayAddToCartButton(Boolean.valueOf(isRestockAlertPresent));
		catLaneSpecificDataForPDP.setLane3(lane3);
	}

	/**
	 * This method is used to set Ordering Window Data for Lane 3.
	 *
	 * @param orderingMessageWindow
	 *           orderingMessageWindow map
	 * @param lane3
	 *           CatLaneThreeData Object
	 */
	private void setOrderingWindowDataForLane3(final Map<String, Object> orderingMessageWindow, final CatLaneThreeData lane3)
	{
		if (orderingMessageWindow != null)
		{
			final Object lane2ForOrderingWindow = orderingMessageWindow.get(LANE3);
			if (lane2ForOrderingWindow instanceof CatLaneThreeData)
			{
				lane3.setLeftDays(((CatLaneThreeData) lane2ForOrderingWindow).getLeftDays());
				lane3.setOrderWindowStatus(((CatLaneThreeData) lane2ForOrderingWindow).getOrderWindowStatus());
			}
			else
			{
				lane3.setOrderWindowStatus(CLOSED);
				lane3.setLeftDays(Integer.valueOf(0));
			}
		}
		else
		{
			lane3.setOrderWindowStatus(CLOSED);
			lane3.setLeftDays(Integer.valueOf(0));
		}
	}

	/**
	 * This method is used to set Lane Specific Data for Lane2
	 *
	 * @param catLaneSpecificDataForPDP
	 *           catLaneSpecificDataForPDP
	 * @param productLaneInfoModel
	 *           productLaneInfoModel
	 * @param orderingMessageWindow
	 *           orderingMessageWindow
	 * @param isRestockAlertPresent
	 *           restockAlert present
	 */
	private void setLaneSpecificDataForLane2(final CatLaneSpecificDataForPDP catLaneSpecificDataForPDP,
			final ProductLaneInfoModel productLaneInfoModel, final Map<String, Object> orderingMessageWindow,
			final boolean isRestockAlertPresent)
	{
		final CatLaneTwoData lane2 = new CatLaneTwoData();
		setOrderingWindowDataForLane2(orderingMessageWindow, lane2);
		lane2.setCommittedCount(Integer.valueOf(getOrderableQuantityCountLeft(productLaneInfoModel)));
		lane2.setMaximumCount(productLaneInfoModel.getMaximumOrderableQuantity());
		try
		{
			final Date date = new SimpleDateFormat("MM-yyyy").parse(productLaneInfoModel.getMonthYear());
			final SimpleDateFormat sdf = new SimpleDateFormat(MONTH_YEAR_FORMAT);
			lane2.setMonth_Year(sdf.format(date));
		}
		catch (final ParseException e)
		{
			LOG.error("Error while parsing Date", e);
		}
		lane2.setAddToCartDisabled(Boolean.valueOf(isAddToCartDisabled(productLaneInfoModel, lane2, LANE2)));
		lane2.setDisplayAddToCartButton(Boolean.valueOf(isRestockAlertPresent));
		catLaneSpecificDataForPDP.setLane2(lane2);
	}

	/**
	 * This method is used to set Ordering Window Data for Lane 2.
	 *
	 * @param orderingMessageWindow
	 *           orderingMessageWindow map
	 * @param lane2
	 *           CatLaneTwoData
	 */
	private void setOrderingWindowDataForLane2(final Map<String, Object> orderingMessageWindow, final CatLaneTwoData lane2)
	{
		if (orderingMessageWindow != null)
		{
			final Object lane2ForOrderingWindow = orderingMessageWindow.get(LANE2);
			if (lane2ForOrderingWindow instanceof CatLaneTwoData)
			{
				lane2.setLeftDays(((CatLaneTwoData) lane2ForOrderingWindow).getLeftDays());
				lane2.setOrderWindowStatus(((CatLaneTwoData) lane2ForOrderingWindow).getOrderWindowStatus());
			}
			else
			{
				lane2.setOrderWindowStatus(CLOSED);
				lane2.setLeftDays(Integer.valueOf(0));
			}
		}
		else
		{
			lane2.setOrderWindowStatus(CLOSED);
			lane2.setLeftDays(Integer.valueOf(0));
		}
	}

	/**
	 * This method is used to calculate add To cart disabled
	 *
	 * @param productLaneInfoModel
	 *           productLaneInfoModel
	 * @param catLaneData
	 *           Object
	 * @param lane
	 *           lane
	 * @return addToCartDisabled flag
	 */
	private boolean isAddToCartDisabled(final ProductLaneInfoModel productLaneInfoModel, final Object catLaneData,
			final String lane)
	{
		boolean addToCartDisabled = false;
		if (catLaneData != null && StringUtils.isNotBlank(lane))
		{
			if (lane.equalsIgnoreCase(LANE2))
			{
				final CatLaneTwoData catLaneTwoData = (CatLaneTwoData) catLaneData;
				if (catLaneTwoData.getOrderWindowStatus().equalsIgnoreCase(CLOSED))
				{
					addToCartDisabled = true;
				}
			}
			else
			{
				final CatLaneThreeData catLaneThreeData = (CatLaneThreeData) catLaneData;
				if (catLaneThreeData.getOrderWindowStatus().equalsIgnoreCase(CLOSED))
				{
					addToCartDisabled = true;
				}
			}
			if (getOrderableQuantityCountLeft(productLaneInfoModel) <= 0)
			{
				addToCartDisabled = true;
			}
		}
		return addToCartDisabled;
	}

	/**
	 * Method to check the compatibility of a product
	 *
	 * @param catLaneSpecificDataForPDP
	 *           CatLaneSpecificDataForPDP Object
	 * @param productCode
	 *           productCode
	 * @param lane
	 *           lane
	 * @return isProductCompatibleToOrder boolean value
	 */
	private boolean isProductCompatibleToOrder(final CatLaneSpecificDataForPDP catLaneSpecificDataForPDP, final String productCode,
			final String lane)
	{
		try
		{
			if (!catOrderLaneProductService.isProductOrderableForLane(
					(lane.equalsIgnoreCase(LANE2) ? LaneTypeEnum.LANE2.getCode() : LaneTypeEnum.LANE3.getCode()), null, productCode,
					false, false, true, false))
			{
				final List<CatErrorData> catErrorDataList = catLaneSpecificDataForPDP.getErrorData();
				final CatErrorData catErrorData = new CatErrorData();
				catErrorData.setErrorMessage("This configuration is not compatible for "
						+ (lane.equalsIgnoreCase(LANE2) ? LaneTypeEnum.LANE2 : LaneTypeEnum.LANE3));
				catErrorDataList.add(catErrorData);
				catLaneSpecificDataForPDP.setErrorData(catErrorDataList);
				return false;
			}
		}
		catch (final ParseException e)
		{
			LOG.error(e.getMessage(), e);
		}
		return true;
	}

	/**
	 * This method is used to calculate orderable Quantity Count Left.
	 *
	 * @param productLaneInfoModel
	 *           productLaneInfoModel
	 * @return count
	 */
	private int getOrderableQuantityCountLeft(final ProductLaneInfoModel productLaneInfoModel)
	{
		return (productLaneInfoModel.getMaximumOrderableQuantity() != null
				? productLaneInfoModel.getMaximumOrderableQuantity().intValue()
				: 0)
				- (productLaneInfoModel.getReorderedQuantity() != null ? productLaneInfoModel.getReorderedQuantity().intValue() : 0);
	}

	/**
	 * This method is used to set Lane Specific Data for Lane1
	 *
	 * @param catLaneSpecificDataForPDP
	 *           catLaneSpecificDataForPDP
	 * @param addToCartDisabled
	 *           addToCartDisabled
	 * @param isAlertProductPresent
	 *           AlertProduct is present or not
	 */
	private void setLaneSpecificDataForLane1(final CatLaneSpecificDataForPDP catLaneSpecificDataForPDP,
			final boolean addToCartDisabled, final boolean isAlertProductPresent)
	{
		final CatLaneOneData lane1 = new CatLaneOneData();
		lane1.setAddToCartDisabled(Boolean.valueOf(addToCartDisabled));
		lane1.setDisplayAddToCartButton(Boolean.valueOf(isAlertProductPresent));
		catLaneSpecificDataForPDP.setLane1(lane1);
	}

	/**
	 * This method is used to set Stocking Recommendation.
	 *
	 * @param productCode
	 *           productCode
	 * @param salesModel
	 *           salesModel
	 * @param catLaneSpecificDataForPDP
	 *           catLaneSpecificDataForPDP
	 * @param user
	 *           user
	 * @return Entry present in RestockAlertModel Or Not.
	 */
	private boolean setStockingRecommendation(final String productCode, final String salesModel,
			final CatLaneSpecificDataForPDP catLaneSpecificDataForPDP, final B2BCustomerModel user)
	{
		boolean isAlertProductPresent = false;
		final String alertId = salesModel + CatCoreConstants.UNDERSCORE + user.getDefaultB2BUnit().getUid();
		final RestockAlertModel restockAlert = catStockFacade.getStockForSalesModel(alertId);
		if (null != restockAlert && StringUtils.isNotBlank(productCode))
		{
			final CatStockingRecommendationData catStockingRecommendationData = new CatStockingRecommendationData();
			catStockingRecommendationData.setCurrent(restockAlert.getCurrentStockLevel());
			catStockingRecommendationData.setMinimum(restockAlert.getMinimumStockLevel());
			catStockingRecommendationData.setRecommended(restockAlert.getRecommendedStockLevel());
			catLaneSpecificDataForPDP.setStockingRecommendation(catStockingRecommendationData);
			final List<AlertProductModel> alertProductsList = catReorderFacade.getAssociatedConfigProducts(alertId);
			if (CollectionUtils.isNotEmpty(alertProductsList))
			{
				final Optional<AlertProductModel> restockAlertProduct = alertProductsList.stream()
						.filter(alertProduct -> alertProduct.getProduct().getCode().equalsIgnoreCase(productCode)).findAny();
				if (restockAlertProduct.isPresent())
				{
					isAlertProductPresent = true;
				}
			}
		}
		return isAlertProductPresent;
	}


	/**
	 * This method is used to se Additional Attributes.
	 *
	 * @param productCode
	 *           productCode
	 * @param catLaneSpecificDataForPDP
	 *           catLaneSpecificDataForPDP
	 */
	private void setAdditionalAttributes(final String productCode, final CatLaneSpecificDataForPDP catLaneSpecificDataForPDP)
	{
		BaseVariantProductModel baseVariantProductModel = null;
		final ConfigVariantProductModel configVariantProductModel = (ConfigVariantProductModel) catProductService
				.getProductForCode(productCode);
		if (configVariantProductModel != null)
		{
			baseVariantProductModel = (BaseVariantProductModel) configVariantProductModel.getBaseProduct();
		}
		else
		{
			LOG.info("Config Variant is null");
		}
		if (baseVariantProductModel != null)
		{
			setFbcMustSelectFlag(baseVariantProductModel, catLaneSpecificDataForPDP);
			setDCACode(baseVariantProductModel, catLaneSpecificDataForPDP);
			setPrimaryImageUrl(baseVariantProductModel, catLaneSpecificDataForPDP);
		}
		else
		{
			LOG.info("Base Variant is null");
		}
	}


	/**
	 * This method is used to set FBC must select flag.
	 *
	 * @param baseVariantProductModel
	 *           baseVariant
	 * @param catLaneSpecificDataForPDP
	 *           laneSpecificData
	 */
	private void setFbcMustSelectFlag(final BaseVariantProductModel baseVariantProductModel,
			final CatLaneSpecificDataForPDP catLaneSpecificDataForPDP)
	{
		final Boolean fbcMustSelectFlag = baseVariantProductModel.getFbcMustSelectFlag();
		catLaneSpecificDataForPDP.setFbcMustSelectFlag(fbcMustSelectFlag);
	}

	/**
	 * This method is used to set DCA code.
	 *
	 * @param baseVariantProductModel
	 *           baseVariant
	 * @param catLaneSpecificDataForPDP
	 *           lanespecificdata
	 */
	private void setDCACode(final BaseVariantProductModel baseVariantProductModel,
			final CatLaneSpecificDataForPDP catLaneSpecificDataForPDP)
	{
		catLaneSpecificDataForPDP.setDcaCode(baseVariantProductModel.getCode());
	}

	/**
	 * This method is used to set Primary Image URL.
	 *
	 * @param baseVariantProductModel
	 *           baseVariant
	 * @param catLaneSpecificDataForPDP
	 *           lanespecificdata
	 */
	private void setPrimaryImageUrl(final BaseVariantProductModel baseVariantProductModel,
			final CatLaneSpecificDataForPDP catLaneSpecificDataForPDP)
	{
		final ProductModel salesModelProductModel = baseVariantProductModel.getBaseProduct();
		if (salesModelProductModel != null && salesModelProductModel.getPicture() != null)
		{
			final String primaryImageUrl = null != salesModelProductModel.getPicture() ? salesModelProductModel.getPicture().getURL()
					: StringUtils.EMPTY;
			catLaneSpecificDataForPDP.setPrimaryImageUrl(primaryImageUrl);
		}
	}

	/**
	 * Add Dummy Data for Lane2 and Lane3 in case of error Scenarios.
	 *
	 * @param orderingMessageWindow
	 *           orderingMessageWindow
	 * @param laneTypeEnum
	 *           laneTypeEnum
	 * @param catLaneSpecificDataForPDP
	 *           catLaneSpecificDataForPDP
	 * @param isRestockAlertPresent
	 * @return CatLaneSpecificDataForPDP Object
	 */
	private CatLaneSpecificDataForPDP addDummyDataForLanesIfEntryNotPresent(final Map<String, Object> orderingMessageWindow,
			final LaneTypeEnum laneTypeEnum, final CatLaneSpecificDataForPDP catLaneSpecificDataForPDP,
			final boolean isRestockAlertPresent)
	{
		if (laneTypeEnum != null && laneTypeEnum.equals(LaneTypeEnum.LANE2))
		{
			final CatLaneTwoData lane2 = new CatLaneTwoData();
			setOrderingWindowDataForLane2(orderingMessageWindow, lane2);
			lane2.setCommittedCount(Integer.valueOf(0));
			lane2.setMaximumCount(Integer.valueOf(0));
			final Date date = new Date();
			final SimpleDateFormat sdf = new SimpleDateFormat(MONTH_YEAR_FORMAT);
			lane2.setMonth_Year(sdf.format(date));
			lane2.setAddToCartDisabled(Boolean.TRUE);
			lane2.setDisplayAddToCartButton(Boolean.valueOf(isRestockAlertPresent));
			catLaneSpecificDataForPDP.setLane2(lane2);
		}
		else
		{
			final CatLaneThreeData lane3 = new CatLaneThreeData();
			setOrderingWindowDataForLane3(orderingMessageWindow, lane3);
			lane3.setAllocationCount(Integer.valueOf(0));
			lane3.setMaximumCount(Integer.valueOf(0));
			final Date date = new Date();
			final SimpleDateFormat sdf = new SimpleDateFormat(MONTH_YEAR_FORMAT);
			lane3.setMonth_Year(sdf.format(date));
			lane3.setAddToCartDisabled(Boolean.TRUE);
			lane3.setDisplayAddToCartButton(Boolean.valueOf(isRestockAlertPresent));
			catLaneSpecificDataForPDP.setLane3(lane3);
		}
		return catLaneSpecificDataForPDP;
	}

}
