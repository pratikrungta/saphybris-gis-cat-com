/**
 *
 */
package com.cat.bcp.facade.product.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.stock.StockService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.model.InventoryReservationModel;
import com.cat.bcp.core.model.OrderWindowModel;
import com.cat.bcp.core.model.RestockAlertModel;
import com.cat.bcp.core.model.ShipToAfterDateModel;
import com.cat.bcp.core.servicelayer.CatProductService;
import com.cat.bcp.core.servicelayer.CatStockService;
import com.cat.bcp.facade.product.CatStockFacade;
import com.cat.bcp.facades.customer.CatCustomerFacade;
import com.cat.facades.product.InventoryAgeData;


/**
 * @author bidavda
 *
 */
public class CatStockFacadeImpl implements CatStockFacade
{
	private static final Logger LOG = Logger.getLogger(CatStockFacadeImpl.class);

	@Resource(name = "dealerStockConverter")
	private Converter<ProductModel, List<StockData>> dealerStockConverter;

	@Resource(name = "catStockService")
	private CatStockService catStockService;

	@Resource(name = "catCustomerFacade")
	private CatCustomerFacade catCustomerFacade;

	@Resource(name = "stockService")
	private StockService stockService;

	@Resource(name = "catProductService")
	private CatProductService catProductService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Resource(name = "productConverter")
	private Converter<ProductModel, ProductData> productConverter;


	@Resource(name = "catInventoryAgeConverter")
	private Converter<InventoryReservationModel, InventoryAgeData> catInventoryAgeConverter;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<StockData> populateStockForProduct(final ProductModel productModel)
	{
		final List<StockData> stockData = dealerStockConverter.convert(productModel);
		if (!stockData.isEmpty())
		{
			return stockData;
		}
		else
		{
			LOG.debug("No stock found for product : " + productModel.getCode());
			return Collections.emptyList();
		}
	}

	/**
	 *
	 * {@inheritDoc}`
	 */
	@Override
	public void updateAlertEntry(final String alertId, final String configProduct, final String orderedQty)
	{
		catStockService.updateAlertEntry(alertId, configProduct, orderedQty);

	}


	/**
	 *
	 * {@inheritDoc}`
	 */
	@Override
	public RestockAlertModel getStockForSalesModel(final String alertId)
	{
		return catStockService.getStockForSalesModel(alertId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map getOrderWindowAndShipAfterDates(final List<ProductModel> prodList)
	{
		return catStockService.getOrderWindowAndShipAfterDates(prodList);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long getStockLevelForDealerWarehouse(final ProductModel productModel)
	{
		final List<WarehouseModel> dealerWarehouses = catCustomerFacade.getCurrentDealerWarehouse(productModel);

		if (!dealerWarehouses.isEmpty())
		{
			final Map<WarehouseModel, Integer> dealerStockMap = stockService.getAvailability(dealerWarehouses, productModel, null);
			final List<StockData> dealerStock = populateStockData(dealerStockMap);
			if (!dealerStock.isEmpty())
			{
				// As there is only one dealer ware house so only one stock level for particular product
				final StockData stockData = dealerStock.get(0);
				return stockData.getStockLevel();

			}
		}
		// No dealer Ware house found
		return Long.valueOf(0);

	}

	/**
	 * populate the StockData for pdc, mfu and dealer warehouse.
	 *
	 * @param stockMap
	 *           the stock map
	 * @param dealerFlag
	 *           the dealer flag
	 * @return List of StockData
	 */
	private List<StockData> populateStockData(final Map<WarehouseModel, Integer> stockMap)
	{
		final List<StockData> stockDataList = new ArrayList<>();

		stockMap.forEach((warehouse, stockValue) -> {
			final StockData stock = new StockData();
			final Integer stockLevel = stockValue;
			stock.setStockLevel(Long.valueOf(stockLevel.longValue()));
			stock.setWarehouseName(warehouse.getName());
			stock.setWarehouseType(warehouse.getWarehouseType().toString());
			stockDataList.add(stock);
		});

		return stockDataList;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Boolean isIMAddToCartConfigProduct(final String salesModelId, final ProductModel productModel,
			final B2BCustomerModel customerModel)
	{
		return catStockService.isIMAddToCartConfigProduct(salesModelId, productModel, customerModel);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<InventoryAgeData> getInventoryAgeForProduct(final ProductModel productModel)
	{
		final B2BCustomerModel b2bCustomerModel = (B2BCustomerModel) userService.getCurrentUser();
		final List<InventoryAgeData> inventoryAgeDatas = new ArrayList<>();
		final List<InventoryReservationModel> inventoryReservationModels = catProductService
				.getInventoryReservationForProduct(productModel, b2bCustomerModel.getDefaultB2BUnit());
		InventoryAgeData inventoryAgeData = null;
		if (CollectionUtils.isNotEmpty(inventoryReservationModels))
		{
			for (final InventoryReservationModel inventoryReservationModel : inventoryReservationModels)
			{
				inventoryAgeData = getCatInventoryAgeConverter().convert(inventoryReservationModel);
				inventoryAgeDatas.add(inventoryAgeData);
			}
		}
		return inventoryAgeDatas;
	}

	/**
	 * @return the dealerStockConverter
	 */
	public Converter<ProductModel, List<StockData>> getDealerStockConverter()
	{
		return dealerStockConverter;
	}

	/**
	 * @param dealerStockConverter
	 *           the dealerStockConverter to set
	 */
	public void setDealerStockConverter(final Converter<ProductModel, List<StockData>> dealerStockConverter)
	{
		this.dealerStockConverter = dealerStockConverter;
	}

	/**
	 * @return the catStockService
	 */
	public CatStockService getCatStockService()
	{
		return catStockService;
	}

	/**
	 * @param catStockService
	 *           the catStockService to set
	 */
	public void setCatStockService(final CatStockService catStockService)
	{
		this.catStockService = catStockService;
	}

	/**
	 * @return the catCustomerFacade
	 */
	public CatCustomerFacade getCatCustomerFacade()
	{
		return catCustomerFacade;
	}

	/**
	 * @param catCustomerFacade
	 *           the catCustomerFacade to set
	 */
	public void setCatCustomerFacade(final CatCustomerFacade catCustomerFacade)
	{
		this.catCustomerFacade = catCustomerFacade;
	}

	/**
	 * @return the stockService
	 */
	public StockService getStockService()
	{
		return stockService;
	}

	/**
	 * @param stockService
	 *           the stockService to set
	 */
	public void setStockService(final StockService stockService)
	{
		this.stockService = stockService;
	}

	/**
	 * @return the catProductService
	 */
	public CatProductService getCatProductService()
	{
		return catProductService;
	}

	/**
	 * @param catProductService
	 *           the catProductService to set
	 */
	public void setCatProductService(final CatProductService catProductService)
	{
		this.catProductService = catProductService;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * @return the catInventoryAgeConverter
	 */
	public Converter<InventoryReservationModel, InventoryAgeData> getCatInventoryAgeConverter()
	{
		return catInventoryAgeConverter;
	}

	/**
	 * @param catInventoryAgeConverter
	 *           the catInventoryAgeConverter to set
	 */
	public void setCatInventoryAgeConverter(final Converter<InventoryReservationModel, InventoryAgeData> catInventoryAgeConverter)
	{
		this.catInventoryAgeConverter = catInventoryAgeConverter;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductData> getProductsForShipAfterDate(final Date date)
	{
		ShipToAfterDateModel shipToAfterModel = new ShipToAfterDateModel();
		shipToAfterModel.setShipAfterDate(date);
		shipToAfterModel = flexibleSearchService.getModelByExample(shipToAfterModel);

		final OrderWindowModel owm = shipToAfterModel.getOrderWindow();
		return productConverter.convertAll(owm.getProduct());


	}



}
