/**
 *
 */
package com.cat.bcp.facades.comparator;

import de.hybris.platform.commercefacades.product.data.ProductData;

import java.io.Serializable;
import java.util.Comparator;


/**
 * @author avaijapurkar
 *
 */
public class CatProductNameComparator implements Comparator<ProductData>, Serializable
{

	/*
	 * Compare on product name
	 *
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(final ProductData product1, final ProductData product2)
	{

		return product1.getName().compareTo(product2.getName());
	}

}
