/**
 *
 */
package com.cat.bcp.facades.comparator;

import de.hybris.platform.commercefacades.product.data.ProductData;

import java.io.Serializable;
import java.util.Comparator;


/**
 * @author avaijapurkar
 *
 */
public class CatProductSeatCapacityComparator implements Comparator<ProductData>, Serializable
{

	/*
	 * Compare on seat capacity
	 *
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(final ProductData product1, final ProductData product2)
	{
		final int p1SeatCapacity = Integer.parseInt(product1.getSeatCapacity());
		final int p2SeatCapacity = Integer.parseInt(product2.getSeatCapacity());
		return p1SeatCapacity - p2SeatCapacity;
	}

}
