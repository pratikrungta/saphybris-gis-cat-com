/**
 *
 */
package com.cat.bcp.facades.comparator;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

import com.cat.bcp.core.model.ShipToAfterDateModel;


/**
 * @author avaijapurkar
 *
 */
public class CatShipToAfterDateComparator implements Comparator<ShipToAfterDateModel>, Serializable
{

	/*
	 * Compare on seat capacity
	 *
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(final ShipToAfterDateModel ship1, final ShipToAfterDateModel ship2)
	{
		final Date s1Date = ship1.getShipAfterDate();
		final Date s2Date = ship2.getShipAfterDate();
		return s1Date.getTime() > s2Date.getTime() ? 1 : -1;
	}

}
