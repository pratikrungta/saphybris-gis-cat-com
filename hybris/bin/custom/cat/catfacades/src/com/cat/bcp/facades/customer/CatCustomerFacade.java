/**
 *
 */
package com.cat.bcp.facades.customer;


import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.cat.bcp.core.enums.CatCountryEnum;
import com.cat.facades.order.LaneTypeData;
import com.cat.facades.user.CatCountyData;
import com.cat.facades.user.CatCountyMappingData;
import com.cat.facades.user.CatCountyMasterData;


/**
 * @author avaijapurkar
 *
 */
public interface CatCustomerFacade extends CustomerFacade
{

	/**
	 * Get Dealer Warehouse.
	 *
	 * @return String
	 */
	String getDealerWarehouse();

	/**
	 * Get Current Dealer Warehouse.
	 *
	 * @param productModel
	 *           Product Model
	 * @return List<WarehouseModel>
	 */
	List<WarehouseModel> getCurrentDealerWarehouse(ProductModel productModel);

	/**
	 * Get Current Dealer.
	 *
	 * @return B2BCustomerModel
	 */
	B2BCustomerModel getCurrentDealer();

	/**
	 * Check if User is IM.
	 *
	 * @param userGroups
	 *           User Groups
	 * @return List<WarehouseModel>
	 */
	boolean isUserIM(Set<PrincipalGroupModel> userGroups);

	/**
	 * Get PDC Warehouses.
	 *
	 * @param productModel
	 *           Product Model
	 * @return List<WarehouseModel>
	 */
	List<WarehouseModel> getPDCWarehouses(ProductModel productModel);

	/**
	 * Get MFU Warehouses.
	 *
	 * @param productModel
	 *           Product Model
	 * @return List<WarehouseModel>
	 */
	List<WarehouseModel> getMFUWarehouses(ProductModel productModel);

	/**
	 * Get Product Available Warehouses.
	 *
	 * @param list
	 *           Facet Data List
	 * @return String
	 */
	String getProductAvailableWarehouses(List<FacetData<SearchStateData>> list);

	/**
	 * Update Favorite Products In Result.
	 *
	 * @param results
	 *           Product Data List
	 * @return List<ProductData>
	 */
	List<ProductData> updateFavoriteProductsInResult(List<ProductData> results);

	/**
	 * Get Customer Favorite Products.
	 *
	 * @return List<ProductData>
	 */
	List<ProductData> getCustomerFavoriteProducts();

	/**
	 * Toggle Favorite.
	 *
	 * @param favProdList
	 *           Favorite Product List
	 * @param favProductSet
	 *           Favorite Product set
	 * @param product
	 *           Product Model
	 * @return Map
	 */
	Map toggleFavorite(Set<ProductModel> favProdList, Set favProductSet, ProductModel product);

	/**
	 * Get User Type.
	 *
	 * @return String
	 */
	String getUserType();

	/**
	 * Get Dealer ShipTos.
	 *
	 * @param customerModel
	 *           UID
	 * @param sortParameter
	 *           added this parameter to reuse the code and sort on the basis of country
	 * @return List<AddressData>
	 */

	List<AddressData> getDealerShipTos(B2BCustomerModel customerModel, String sortParameter);

	/**
	 * Get Customer Name Suggestions
	 *
	 * @param name
	 *           The input passed based on which customer data is returned.
	 * @return List<String> this will be returned to the jsp
	 */
	List<String> getCustomerNameSuggestions(String name);

	/**
	 * Fetch Existing Customer Details By Name
	 *
	 * @param name
	 *           The input passed based on which customer data is returned.
	 * @return CustomerData this will be returned to the jsp
	 */
	CustomerData fetchExistingCustomerDetailsByName(String name);

	/**
	 * Get Customer Email Suggestions
	 *
	 * @param email
	 *           The input passed based on which customer data is returned.
	 * @return List<String> this will be returned to the jsp
	 */
	List<String> getCustomerEmailSuggestions(String email);

	/**
	 * Fetch Existing Customer Details By Email
	 *
	 * @param email
	 *           The input passed based on which customer data is returned.
	 * @return CustomerData this will be returned to the jsp
	 */
	CustomerData fetchExistingCustomerDetailsByEmail(String email);

	/**
	 *
	 * Method to return a list of dynamic enum values
	 *
	 * @return List<CatCountryEnum> list of catcountryenum values
	 */
	List<CatCountryEnum> getListOfCountries();

	/**
	 * Method to pass logged in user details and request URI to service layer
	 *
	 * @param requestURI
	 *           Request URI
	 * @param userAgent
	 *           User Agent
	 */

	void persistUserDetails(String requestURI, String userAgent);

	/**
	 * Gets the dealer warehouse name.
	 *
	 * @return the dealer warehouse name
	 */
	String getDealerWarehouseName();

	/**
	 * Removes the unauthorized favorite products
	 */
	void removeUnauthorizedFavoriteAndRecentProducts();

	/**
	 * Method to get the county data for a dealer based on sort param and order
	 *
	 * @param shipToAddressPk
	 *           pk on the basis of which owner will be calculated
	 * @param sortOrder
	 *           sort Order i.e ASC or DESC
	 * @param sortParameter
	 *           sort Parameter i.e CountyName or Country or State
	 *
	 * @return List<CatCountyData> list of counties present for that ship to
	 */
	List<CatCountyData> getCountyDataForDealer(String shipToAddressPk, String sortParameter, String sortOrder);

	/**
	 * This method is used to hide ship to address and delink the linked counties for the same.
	 *
	 * @param shipToPkForHide
	 *           shipToPkForHide
	 * @param sortParameter
	 *           sortParameter
	 * @param sortOrder
	 *           sortOrder
	 */
	void hideShipTo(final String shipToPkForHide, final String sortParameter, final String sortOrder);

	/**
	 * This method is used to un-hide ship to address
	 *
	 * @param shipToPkForUnHide
	 *           shipToPkForUnHide
	 */
	void unHideShipTo(final String shipToPkForUnHide);

	/**
	 * Method to unlink county from a ship to
	 *
	 * @param countyPk
	 *           pk on the basis of which a county model will be fetched
	 * @return List<CatCountyData> list of county data
	 */
	List<CatCountyData> unlinkCounty(String countyPk);

	/**
	 * Method to get the available county for shipto's which can be linked
	 *
	 * @param shipToAddressPk
	 *           pk against which county list will be retrieved
	 * @return CatCountyMasterData that contains List<CatCountyData> list of county data, and address data
	 */
	CatCountyMasterData getAvailableCountyForShipTo(String shipToAddressPk);

	/**
	 * Method to link county to a ship to
	 *
	 * @param catCountyMappingData
	 *           Data object that holds list of County PKs and ship to address PK
	 */
	void linkCounty(CatCountyMappingData catCountyMappingData);

	/**
	 * Method to check if there are any unlinked counties present
	 *
	 * @param shipToPk
	 *           ship to pk on the basis of which counties will be fetched
	 * @return Boolean true or false
	 */
	Boolean checkIfUnlinkedCountiesPresent(String shipToPk);

	/**
	 * Used to loop through the addresses and concated the different elements of address into one
	 *
	 * @param shippingAddressList
	 *           the shipping address data list used for concatenation
	 * @return List<String> the list containing the address in the required format
	 */
	List<String> getConcatenatedShippingAddressList(List<AddressData> shippingAddressList);


	/**
	 * Gets the lane types for customer orders.
	 *
	 * @return the lane types for customer orders
	 */
	List<LaneTypeData> getLaneTypesForCustomerOrders();
}
