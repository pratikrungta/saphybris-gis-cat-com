/**
 *
 */
package com.cat.bcp.facades.customer.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.basecommerce.enums.PointOfServiceTypeEnum;
import de.hybris.platform.commercefacades.customer.impl.DefaultCustomerFacade;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetValueData;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.ordersplitting.daos.impl.DefaultWarehouseDao;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.enums.CatCountryEnum;
import com.cat.bcp.core.enums.LaneTypeEnum;
import com.cat.bcp.core.enums.ProductConditionEnum;
import com.cat.bcp.core.enums.WarehouseTypeEnum;
import com.cat.bcp.core.event.RecentViewedItemsCollection;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.CatCountyModel;
import com.cat.bcp.core.service.CatCustomerService;
import com.cat.bcp.core.servicelayer.CatB2BUnitService;
import com.cat.bcp.facades.customer.CatCustomerFacade;
import com.cat.facades.order.LaneTypeData;
import com.cat.facades.user.CatCountyData;
import com.cat.facades.user.CatCountyMappingData;
import com.cat.facades.user.CatCountyMasterData;


/**
 * @author avaijapurkar
 *
 *         This class contains methods to 1- Get dealer warehouse 2- Build the solr search query 3- Get Current dealer
 */
public class CatCustomerFacadeImpl extends DefaultCustomerFacade implements CatCustomerFacade
{
	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "warehouseDao")
	private DefaultWarehouseDao warehouseDao;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "productConverter")
	private Converter<ProductModel, ProductData> productConverter;

	@Resource(name = "sessionService")
	SessionService sessionService;

	@Resource(name = "b2bUnitService")
	CatB2BUnitService b2bUnitService;

	@Resource(name = "catCustomerService")
	CatCustomerService catCustomerService;

	@Resource(name = "customerConverter")
	private Converter<CustomerModel, CustomerData> customerConverter;

	@Resource(name = "catCountyMappingConverter")
	private Converter<CatCountyModel, CatCountyData> catCountyMappingConverter;

	@Resource(name = "catLaneTypeConverter")
	private Converter<LaneTypeEnum, LaneTypeData> catLaneTypeConverter;

	@Resource(name = "enumerationService")
	EnumerationService enumerationService;


	private static final Logger LOG = Logger.getLogger(CatCustomerFacadeImpl.class);

	private static final boolean IS_LOG_DEBUG_ENABLED = LOG.isDebugEnabled();

	private static final String VISITED_PRODUCTS_KEY = "RECENT_VIEWED_ITEMS";

	private static final String VISITED_USED_PRODUCTS_SESSION_KEY = "RECENT_VIEWED_USED_ITEMS";
	private static final String ADD_SUCCESS = "addSuccess";
	private static final String REMOVE_SUCCESS = "removeSuccess";
	private static final String UNDERSCO = "_";

	private static final String TOWN = "town";


	public void setCatCustomerService(final CatCustomerService catCustomerService)
	{
		this.catCustomerService = catCustomerService;
	}


	public void setCatCountyMappingConverter(final Converter<CatCountyModel, CatCountyData> catCountyMappingConverter)
	{
		this.catCountyMappingConverter = catCountyMappingConverter;
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getDealerWarehouse()
	{

		final B2BCustomerModel customer = getCurrentDealer();
		if (IS_LOG_DEBUG_ENABLED)
		{
			LOG.debug("Entered #getDealerWarehouse() method in Facade for dealer : " + customer.getUid());
		}
		final WarehouseModel warehouse = getDealerWarehouseByCustomer(customer);
		if (IS_LOG_DEBUG_ENABLED)
		{
			LOG.debug("Exit #getDealerWarehouse() method in DefaultCatCustomerFacade" + customer.getUid());
		}
		return warehouse != null ? warehouse.getCode() : "";
	}

	/**
	 * @param customer
	 * @return warehouse
	 */
	private WarehouseModel getDealerWarehouseByCustomer(final B2BCustomerModel customer)
	{
		final B2BUnitModel unit = customer.getDefaultB2BUnit();
		WarehouseModel warehouse = null;
		final Set<PointOfServiceModel> posSet = unit.getPointsOfService();
		for (final PointOfServiceModel pos : posSet)
		{
			if (PointOfServiceTypeEnum.POS.equals(pos.getType()) && CollectionUtils.isNotEmpty(pos.getWarehouses()))
			{
				warehouse = pos.getWarehouses().get(0);
				break;
			}
		}
		return warehouse;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getDealerWarehouseName()
	{
		final B2BCustomerModel customer = getCurrentDealer();
		if (IS_LOG_DEBUG_ENABLED)
		{
			LOG.debug("Entered #getDealerWarehouseName() method in Facade for dealer : " + customer.getUid());
		}
		final WarehouseModel warehouse = getDealerWarehouseByCustomer(customer);

		return warehouse != null ? warehouse.getName() : "";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<WarehouseModel> getCurrentDealerWarehouse(final ProductModel productModel)
	{
		if (IS_LOG_DEBUG_ENABLED)
		{
			LOG.debug(
					"Entered #getCurrentDealerWarehouse() method in DefaultCatCustomerFacade for product : " + productModel.getCode());
		}
		final String dealerWarehouseCode = getDealerWarehouse();

		final List<WarehouseModel> dealerWarehouses = new ArrayList<>();

		final List<WarehouseModel> warehouses = warehouseDao.getWarehouses(productModel.getCode());
		if (!warehouses.isEmpty())
		{
			for (final WarehouseModel warehouse : warehouses)
			{
				if (WarehouseTypeEnum.WAREHOUSE.equals(warehouse.getWarehouseType())
						&& warehouse.getCode().equals(dealerWarehouseCode))
				{
					dealerWarehouses.add(warehouse);
				}
			}
		}
		if (IS_LOG_DEBUG_ENABLED)
		{
			LOG.debug(
					"Exit #getCurrentDealerWarehouse() method in DefaultCatCustomerFacade for product : " + productModel.getCode());
		}
		return dealerWarehouses;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public B2BCustomerModel getCurrentDealer()
	{
		B2BCustomerModel currentDealer = null;
		final UserModel user = userService.getCurrentUser();
		if (!"anonymous".equalsIgnoreCase(user.getUid()))
		{
			currentDealer = (B2BCustomerModel) user;
		}
		return currentDealer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<WarehouseModel> getPDCWarehouses(final ProductModel productModel)
	{
		if (IS_LOG_DEBUG_ENABLED)
		{
			LOG.debug("Entered #getPDCWarehouses() method in DefaultCatCustomerFacade for product : " + productModel.getCode());
		}
		final List<WarehouseModel> pdcWarehouses = new ArrayList<>();

		final List<WarehouseModel> warehouses = warehouseDao.getWarehouses(productModel.getCode());
		if (!warehouses.isEmpty())
		{
			for (final WarehouseModel warehouse : warehouses)
			{
				if (WarehouseTypeEnum.PDC.equals(warehouse.getWarehouseType()))
				{
					pdcWarehouses.add(warehouse);
				}
			}
		}
		if (IS_LOG_DEBUG_ENABLED)
		{
			LOG.debug("Exit #getPDCWarehouses() method in DefaultCatCustomerFacade for product : " + productModel.getCode());
		}
		return pdcWarehouses;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<WarehouseModel> getMFUWarehouses(final ProductModel productModel)
	{
		if (IS_LOG_DEBUG_ENABLED)
		{
			LOG.debug("Entered #getMFUWarehouses() method in DefaultCatCustomerFacade for product : " + productModel.getCode());
		}
		final List<WarehouseModel> warehouses = warehouseDao.getWarehouses(productModel.getCode());
		final List<WarehouseModel> mfuWarehouses = new ArrayList<>();

		if (!warehouses.isEmpty())
		{
			for (final WarehouseModel warehouse : warehouses)
			{
				if (WarehouseTypeEnum.MFU.equals(warehouse.getWarehouseType()))
				{
					mfuWarehouses.add(warehouse);
				}
			}
		}
		if (IS_LOG_DEBUG_ENABLED)
		{
			LOG.debug("Exit #getMFUWarehouses() method in DefaultCatCustomerFacade for product : " + productModel.getCode());
		}
		return mfuWarehouses;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getProductAvailableWarehouses(final List<FacetData<SearchStateData>> facetDataList)
	{
		String flags = StringUtils.EMPTY;
		if (facetDataList != null)
		{
			for (final FacetData<SearchStateData> searchData : facetDataList)
			{
				if ("availableInWarehouse".equalsIgnoreCase(searchData.getCode()))
				{
					flags = setWsFlags(searchData);
				}
			}
		}
		return flags;
	}

	/**
	 * @param searchData
	 * @return WSFlag String
	 */
	private String setWsFlags(final FacetData<SearchStateData> searchData)
	{
		boolean inPDC = false;
		boolean inMFU = false;
		boolean inDealerWS = false;
		for (final FacetValueData<SearchStateData> facetData : searchData.getValues())
		{
			if (CatCoreConstants.PDC.equalsIgnoreCase(facetData.getCode()))
			{
				inPDC = true;
			}
			if (CatCoreConstants.MFU.equalsIgnoreCase(facetData.getCode()))
			{
				inMFU = true;
			}
			final String warehouseCode = getDealerWarehouse();
			if (facetData.getCode().equalsIgnoreCase(warehouseCode))
			{
				inDealerWS = true;
			}
			if (inPDC && inMFU && inDealerWS)
			{
				break;
			}
		}
		return inPDC + ";" + inMFU + ";" + inDealerWS;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isUserIM(final Set<PrincipalGroupModel> userGroups)
	{
		for (final PrincipalGroupModel pgm : userGroups)
		{
			if (CatCoreConstants.INVENTORY_MANAGER_GRP.equalsIgnoreCase(pgm.getUid())
					|| CatCoreConstants.INVENTORY_MANAGER_UTV_GRP.equalsIgnoreCase(pgm.getUid()))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductData> updateFavoriteProductsInResult(final List<ProductData> results)
	{
		LOG.debug("Entered #updateFavoriteProductsInResult() method in DefaultCatCustomerFacade");
		final List<ProductData> newProdList = new ArrayList();
		final Set<ProductModel> favList = getCurrentDealer().getFavoriteProducts();

		final List<String> codeList = new ArrayList();
		for (final ProductModel pd : favList)
		{
			codeList.add(pd.getCode());
		}
		for (final ProductData product : results)
		{
			if (codeList.contains(product.getCode()))
			{
				product.setFavorite(Boolean.TRUE);
			}
			newProdList.add(product);
		}
		LOG.debug("Exit #updateFavoriteProductsInResult() method in DefaultCatCustomerFacade");
		return newProdList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductData> getCustomerFavoriteProducts()
	{
		final Set<ProductModel> prodModelsList = getCurrentDealer().getFavoriteProducts();
		List<ProductData> favProdList = new ArrayList();
		if (CollectionUtils.isNotEmpty(prodModelsList))
		{
			favProdList = productConverter.convertAll(prodModelsList);
		}
		return favProdList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map toggleFavorite(final Set<ProductModel> favProdList, final Set favProductSet, final ProductModel product)
	{
		if (IS_LOG_DEBUG_ENABLED)
		{
			LOG.debug("Entered #toggleFavorite() method in DefaultCatCustomerFacade for product : " + product.getCode());
		}
		final Map resultMap = new HashMap();
		boolean addFlag = false;
		final B2BCustomerModel customerModel = getCurrentDealer();
		if (CollectionUtils.isNotEmpty(favProdList))
		{
			for (final ProductModel pm : favProdList)
			{
				favProductSet.add(pm);
			}
		}
		if (checkFavListLimit(product, favProdList))
		{
			if (favProductSet.contains(product))
			{
				favProductSet.remove(product);
				customerModel.setFavoriteProducts(favProductSet);
				getModelService().save(customerModel);

				resultMap.put(ADD_SUCCESS, Boolean.FALSE);
				resultMap.put(REMOVE_SUCCESS, Boolean.TRUE);
			}
			else
			{
				resultMap.put(ADD_SUCCESS, Boolean.FALSE);
				resultMap.put(REMOVE_SUCCESS, Boolean.FALSE);
			}
		}
		else
		{
			if (!favProductSet.contains(product))
			{
				favProductSet.add(product);
				customerModel.setFavoriteProducts(favProductSet);
				getModelService().save(customerModel);
				addFlag = true;
				resultMap.put(ADD_SUCCESS, Boolean.TRUE);
				resultMap.put(REMOVE_SUCCESS, Boolean.FALSE);
			}
			else
			{
				favProductSet.remove(product);
				customerModel.setFavoriteProducts(favProductSet);
				getModelService().save(customerModel);

				resultMap.put(ADD_SUCCESS, Boolean.FALSE);
				resultMap.put(REMOVE_SUCCESS, Boolean.TRUE);
			}
		}
		RecentViewedItemsCollection collection;
		if (isUsedProduct(product))
		{
			collection = sessionService.getOrLoadAttribute(VISITED_USED_PRODUCTS_SESSION_KEY,
					() -> new RecentViewedItemsCollection(Config.getInt("cat.maxRecentItems", 6)));
			updateRecentlyViewedFavProducts(collection, product.getCode(), addFlag);
			sessionService.setAttribute(VISITED_USED_PRODUCTS_SESSION_KEY, collection);
		}
		else
		{
			collection = sessionService.getOrLoadAttribute(VISITED_PRODUCTS_KEY,
					() -> new RecentViewedItemsCollection(Config.getInt("cat.maxRecentItems", 6)));
			updateRecentlyViewedFavProducts(collection, product.getCode(), addFlag);
			sessionService.setAttribute(VISITED_PRODUCTS_KEY, collection);
		}
		if (IS_LOG_DEBUG_ENABLED)
		{
			LOG.debug("Exit #toggleFavorite() method in DefaultCatCustomerFacade for product : " + product.getCode());
		}
		return resultMap;
	}

	/**
	 * Method to update favorite product in recently viewed product list
	 *
	 * @param collection
	 * @param productCode
	 * @param addFlag
	 */
	private void updateRecentlyViewedFavProducts(final RecentViewedItemsCollection collection, final String productCode,
			final boolean addFlag)
	{
		final List<String> codeList = new ArrayList();
		for (final ProductData productData : collection.getProductCodes())
		{
			codeList.add(productData.getCode());
		}

		if (codeList.contains(productCode))
		{
			for (final ProductData productData : collection.getProductCodes())
			{
				setFavoriteProduct(productCode, addFlag, productData);
			}
		}
	}

	/**
	 * @param productCode
	 * @param addFlag
	 * @param productData
	 */
	private void setFavoriteProduct(final String productCode, final boolean addFlag, final ProductData productData)
	{
		if (productData.getCode().equals(productCode))
		{
			if (addFlag)
			{
				productData.setFavorite(Boolean.TRUE);
			}
			else
			{
				productData.setFavorite(Boolean.FALSE);
			}
		}
	}

	/**
	 * Method to get count of Used favorite product .
	 *
	 * @param favProdList
	 * @return count
	 */
	private int getUsedFavoriteProductCount(final Set<ProductModel> favProdList)
	{
		int usedProductCount = 0;
		for (final ProductModel favProd : favProdList)
		{
			if (isUsedProduct(favProd))
			{
				usedProductCount++;
			}
		}
		return usedProductCount;
	}

	/**
	 * Method to check product is of type Used.
	 *
	 * @return boolean
	 */
	private boolean isUsedProduct(final ProductModel productModel)
	{
		if (productModel instanceof BaseVariantProductModel)
		{
			final BaseVariantProductModel baseVariantProductModel = (BaseVariantProductModel) productModel;
			if (baseVariantProductModel.getProductCondition() != null
					&& baseVariantProductModel.getProductCondition().equals(ProductConditionEnum.USED))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Method to evaluate favorite product list limit
	 *
	 * @param favProd
	 * @param favProdList
	 * @return boolean
	 */
	private boolean checkFavListLimit(final ProductModel favProd, final Set<ProductModel> favProdList)
	{
		if (CollectionUtils.isNotEmpty(favProdList))
		{
			final int usedFavoriteProdCount = getUsedFavoriteProductCount(favProdList);
			if (isUsedProduct(favProd))
			{
				if (Config.getParameter("used.favorite.product.limit") != null
						&& usedFavoriteProdCount >= Integer.parseInt(Config.getParameter("used.favorite.product.limit")))
				{
					return true;
				}
			}
			else if (Config.getParameter("user.favorite.limit") != null
					&& (favProdList.size() - usedFavoriteProdCount) >= Integer.parseInt(Config.getParameter("user.favorite.limit")))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUserType()
	{
		final CustomerData customerData = getCurrentCustomer();
		String userType = customerData.getUserRoleUid();
		if (IS_LOG_DEBUG_ENABLED)
		{
			LOG.debug("Entered #getUserType() method in DefaultCatCustomerFacade for the customer code : " + customerData.getUid());
		}
		if (CatCoreConstants.INVENTORY_MANAGER_UTV_GRP.equalsIgnoreCase(customerData.getUserRoleUid()))
		{
			userType = CatCoreConstants.UTV;
		}
		else if (CatCoreConstants.INVENTORY_MANAGER_GRP.equalsIgnoreCase(customerData.getUserRoleUid()))
		{
			userType = CatCoreConstants.IM;
		}
		else if (CatCoreConstants.SALES_PERSON_GRP.equalsIgnoreCase(customerData.getUserRoleUid()))
		{
			userType = CatCoreConstants.SP;
		}
		if (IS_LOG_DEBUG_ENABLED)
		{
			LOG.debug("Exit #getUserType() method in DefaultCatCustomerFacade for Customer code : " + customerData.getUid());
		}
		return userType;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override


	public List<AddressData> getDealerShipTos(final B2BCustomerModel customerModel, final String sortParameter)

	{
		if (IS_LOG_DEBUG_ENABLED)
		{
			LOG.debug("Entered #getDealerShipTos() method in DefaultCatCustomerFacade for customer with UID : "
					+ customerModel.getDefaultB2BUnit().getUid());
		}
		final List<B2BUnitModel> shipsTOUnits = b2bUnitService.getShipsToUnitsForUid(customerModel);
		final List<AddressModel> shipToAddress = new ArrayList();
		List<AddressData> shipToAddressDataList = new ArrayList();

		if (CollectionUtils.isNotEmpty(shipsTOUnits))
		{
			for (final B2BUnitModel b2bUnit : shipsTOUnits)
			{
				setShipToAddress(shipToAddress, b2bUnit);
			}
			if (StringUtils.isNotEmpty(sortParameter) && TOWN.equalsIgnoreCase(sortParameter))
			{
				shipToAddress.sort(Comparator.comparing(AddressModel::getTown));
			}
			else
			{
				shipToAddress.sort(Comparator.comparing(AddressModel::getModifiedtime).reversed());
			}
			shipToAddressDataList = getAddressConverter().convertAll(shipToAddress);
		}
		final Iterator<AddressData> shipToAddressDataListIterator = shipToAddressDataList.iterator();
		while (shipToAddressDataListIterator.hasNext())
		{
			final AddressData addressData = shipToAddressDataListIterator.next();
			if (addressData.getShipToCode() != null && addressData.getShipToCode().length() > 6)
			{
				shipToAddressDataListIterator.remove();
			}
		}
		if (IS_LOG_DEBUG_ENABLED)
		{
			LOG.debug("Exit #getDealerShipTos() method in DefaultCatCustomerFacade for customer with UID : "
					+ customerModel.getDefaultB2BUnit().getUid());
		}
		return shipToAddressDataList;
	}

	/**
	 * @param shipToAddress
	 * @param b2bUnit
	 */
	private void setShipToAddress(final List<AddressModel> shipToAddress, final B2BUnitModel b2bUnit)
	{
		if (CollectionUtils.isNotEmpty(b2bUnit.getAddresses()))
		{
			for (final AddressModel address : b2bUnit.getAddresses())
			{
				shipToAddress.add(address);
			}
		}
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public CustomerData fetchExistingCustomerDetailsByName(final String name)
	{
		final CustomerModel customer = catCustomerService.fetchExistingCustomerDetailsByName(name);
		if (null != customer)
		{
			final CustomerData customerData = customerConverter.convert(customer);
			customerData.setEmail(customer.getCatCustomerEmailId());
			return customerData;
		}
		return null;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCustomerEmailSuggestions(final String email)
	{
		return catCustomerService.getCustomerEmailSuggestions(email);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public CustomerData fetchExistingCustomerDetailsByEmail(final String email)
	{
		final CustomerModel customer = catCustomerService.fetchExistingCustomerDetailsByEmail(email);
		if (null != customer)
		{
			final CustomerData customerData = customerConverter.convert(customer);
			customerData.setEmail(customer.getCatCustomerEmailId());
			return customerData;
		}
		return null;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getCustomerNameSuggestions(final String name)
	{
		return catCustomerService.getCustomerNameSuggestions(name);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CatCountryEnum> getListOfCountries()
	{
		return enumerationService.getEnumerationValues(CatCountryEnum.class);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

	/**
	 * @return the warehouseDao
	 */
	public DefaultWarehouseDao getWarehouseDao()
	{
		return warehouseDao;
	}

	/**
	 * @param warehouseDao
	 *           the warehouseDao to set
	 */
	public void setWarehouseDao(final DefaultWarehouseDao warehouseDao)
	{
		this.warehouseDao = warehouseDao;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void persistUserDetails(final String requestURI, final String userAgent)
	{
		final String userIdentifier = getCurrentCustomer().getUid() + UNDERSCO + getCurrentCustomer().getName();
		catCustomerService.persistUserDetails(requestURI, userIdentifier, getUserType(), userAgent);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeUnauthorizedFavoriteAndRecentProducts()
	{
		final B2BCustomerModel customer = (B2BCustomerModel) getCurrentUser();
		catCustomerService.removeUnauthorizedFavoriteAndRecentProducts(customer);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CatCountyData> getCountyDataForDealer(final String shipToAddressPk, final String sortParameter,
			final String sortOrder)
	{
		final B2BCustomerModel customer = (B2BCustomerModel) getCurrentUser();
		final AddressModel addressModel = (AddressModel) getModelService().get(PK.parse(shipToAddressPk));
		final B2BUnitModel shipToB2BUnit = (B2BUnitModel) addressModel.getOwner();
		final List<CatCountyModel> catCountyList = catCustomerService.getCountyDataForDealer(shipToB2BUnit.getUid(),
				customer.getDefaultB2BUnit().getUid(), sortParameter, sortOrder);
		return catCountyMappingConverter.convertAll(catCountyList);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void hideShipTo(final String shipToPkForHide, final String sortParameter, final String sortOrder)
	{
		final B2BCustomerModel customer = (B2BCustomerModel) getCurrentUser();
		final AddressModel addressModel = (AddressModel) getModelService().get(PK.parse(shipToPkForHide));
		final B2BUnitModel shipToB2BUnit = (B2BUnitModel) addressModel.getOwner();
		final List<CatCountyModel> catCountyList = catCustomerService.getCountyDataForDealer(shipToB2BUnit.getUid(),
				customer.getDefaultB2BUnit().getUid(), sortParameter, sortOrder);
		addressModel.setVisibleInAddressBook(Boolean.FALSE);
		getModelService().save(addressModel);
		if (CollectionUtils.isNotEmpty(catCountyList))
		{
			for (final CatCountyModel catCountyModel : catCountyList)
			{
				catCountyModel.setIsMapped(Boolean.FALSE);
				getModelService().save(catCountyModel);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void unHideShipTo(final String shipToPkForUnHide)
	{
		final AddressModel addressModel = (AddressModel) getModelService().get(PK.parse(shipToPkForUnHide));
		addressModel.setVisibleInAddressBook(Boolean.TRUE);
		getModelService().save(addressModel);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CatCountyData> unlinkCounty(final String countyPk)
	{
		final CatCountyModel countyModel = getModelService().get(PK.parse(countyPk));
		final String branchDealerCode = countyModel.getBranchDealer().getUid();
		final String mainDealerCode = countyModel.getMainDealer().getUid();
		catCustomerService.unlinkCounty(countyModel);
		final List<CatCountyModel> catCountyList = catCustomerService.getCountyDataForDealer(branchDealerCode, mainDealerCode,
				"countyName", "ASC");
		return catCountyMappingConverter.convertAll(catCountyList);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CatCountyMasterData getAvailableCountyForShipTo(final String shipToAddressPk)
	{
		final CatCountyMasterData countyMasterData = new CatCountyMasterData();
		final B2BCustomerModel customer = (B2BCustomerModel) getCurrentUser();
		final String mainDealerCode = customer.getDefaultB2BUnit().getUid();

		final AddressModel addressModel = getModelService().get(PK.parse(shipToAddressPk));
		final List<CatCountyModel> catCountyList = catCustomerService.getAvailableCountyForShipTo(mainDealerCode);

		countyMasterData.setCountyData(catCountyMappingConverter.convertAll(catCountyList));
		countyMasterData.setAddressData(getAddressConverter().convert(addressModel));
		return countyMasterData;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void linkCounty(final CatCountyMappingData catCountyMappingData)
	{
		final AddressModel addressModel = getModelService().get(PK.parse(catCountyMappingData.getShipToPk()));
		final B2BUnitModel shipToUnitModel = (B2BUnitModel) addressModel.getOwner();

		final List<String> countyIdPKList = catCountyMappingData.getCountyIdList();
		for (final String countyPK : countyIdPKList)
		{
			final CatCountyModel catCountyModel = getModelService().get(PK.parse(countyPK));
			catCountyModel.setBranchDealer(shipToUnitModel);
			catCountyModel.setIsMapped(Boolean.TRUE);
			getModelService().save(catCountyModel);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Boolean checkIfUnlinkedCountiesPresent(final String shipToPk)
	{
		return Boolean.valueOf((CollectionUtils.isNotEmpty((getAvailableCountyForShipTo(shipToPk)).getCountyData())));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getConcatenatedShippingAddressList(final List<AddressData> shippingAddressList)
	{
		final List<String> addressList = new ArrayList<>();
		for (final AddressData address : shippingAddressList)
		{
			final String getTownRegionConcatenatedAddress = catCustomerService.getTownRegionConcatenatedAddress(address);
			final String getIsoCodePostalConcatAddress = catCustomerService.getIsoCodePostalConcatenatedAddress(address);
			final String concatenatedAddresss = catCustomerService.getAddressString(address.getLine3(), " ")
					.concat(catCustomerService.getAddressString(address.getLine1(), ", ")).concat(getTownRegionConcatenatedAddress)
					.concat(getIsoCodePostalConcatAddress);
			addressList.add(concatenatedAddresss);
		}
		return addressList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<LaneTypeData> getLaneTypesForCustomerOrders()
	{
		final List<LaneTypeEnum> laneTypeEnumList = enumerationService.getEnumerationValues(LaneTypeEnum.class);
		return catLaneTypeConverter.convertAll(laneTypeEnumList);
	}
}