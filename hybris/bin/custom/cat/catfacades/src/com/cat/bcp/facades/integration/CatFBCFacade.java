/**
 *
 */
package com.cat.bcp.facades.integration;

/**
 * The Facade class for a FBC Implementation
 *
 * @author ravjonnalagadda
 */
public interface CatFBCFacade
{


	/**
	 * This method returns response for the SelectAPI response
	 * 
	 * @param dca
	 * @param laneCode
	 * @return parts for the initiated request.
	 * @throws Exception
	 */
	String initiateFBCAPIRequest(final String dca, final String laneCode) throws Exception;


	/**
	 * @param configId
	 * @param featureCode
	 * @param productCode
	 * @return JSON Formatted data : returns JSON Formatted data
	 */
	String getFBCSelectAPIResponse(String configId, String featureCode, String productCode);

}
