/**
 *
 */
package com.cat.bcp.facades.integration;

import com.cat.bcp.core.exception.CatException;
import com.cat.facades.edh.CatEDHInventoryDetailsData;


/**
 * The Facade class for a EDH Implementation
 *
 * @author ravjonnalagadda
 */
public interface CatIntegrationFacade
{

	/**
	 * This method is to get InventoryDetails from EDH.
	 *
	 * @param inventoryDetailsData
	 *           : Object it holds request resource parameters list.
	 * @return : it will return data for inventoryData.
	 * @throws CatException
	 *            : throws generic CatException.
	 */
	String getInventoryDetails(CatEDHInventoryDetailsData inventoryDetailsData) throws CatException;

	/**
	 * This method is to get InventoryCount from EDH.
	 *
	 * @param partyId
	 *           : Code which identifies a given PDC, manufacturing facility or dealer.
	 * @param config
	 *           : A configuration ID associated with a given machine
	 * @param getDescendantNodes
	 *           : A boolean flag ('true', 'false') for whether children objects should be included
	 * @return : it return count data
	 * @throws CatException
	 *            : throws generic CatException.
	 */
	public String getInventoryCountByPartyId(String partyId, String config, String getDescendantNodes) throws CatException;


}
