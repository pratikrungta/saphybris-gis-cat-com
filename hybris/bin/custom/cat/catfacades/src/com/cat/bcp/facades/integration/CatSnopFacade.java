/**
 *
 */
package com.cat.bcp.facades.integration;

import com.cat.core.integration.snop.response.CatSnopResponseMapper;


/**
 * @author avaijapurkar
 *
 */
public interface CatSnopFacade
{

	/**
	 * @return CatSnopResponseMapper - response mapper
	 */
	CatSnopResponseMapper validateSnopRequest();
}
