/**
 *
 */
package com.cat.bcp.facades.integration.impl;


import javax.annotation.Resource;

import com.cat.bcp.core.integration.service.CatFBCService;
import com.cat.bcp.facades.integration.CatFBCFacade;



/**
 * The Facade Impl class for a FBC Implementation
 *
 * @author ravjonnalagadda
 */

public class CatFBCFacadeImpl implements CatFBCFacade
{

	@Resource(name = "catFBCService")
	CatFBCService catFBCService;

	/**
	 * {@inheritDoc}
	 *
	 * @throws Exception
	 *
	 */
	@Override
	public String initiateFBCAPIRequest(final String dca, final String laneCode) throws Exception
	{
		return catFBCService.initiateFBCAPIRequest(dca, laneCode);
	}

	/**
	 * {@inheritDoc}
	 *
	 */
	@Override
	public String getFBCSelectAPIResponse(final String configId, final String featureCode, final String productCode)
	{
		return catFBCService.getSelectNextResponse(configId, featureCode, productCode);
	}

}
