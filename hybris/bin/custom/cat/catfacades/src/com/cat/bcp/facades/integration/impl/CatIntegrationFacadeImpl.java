/**
 *
 */
package com.cat.bcp.facades.integration.impl;

import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import com.cat.bcp.core.exception.CatException;
import com.cat.bcp.core.integration.jobs.service.CatIntegrationService;
import com.cat.bcp.facades.integration.CatIntegrationFacade;
import com.cat.core.integration.edh.CatEDHInventoryDetailsDataList;
import com.cat.facades.edh.CatEDHInventoryDetailsData;



/**
 * The Facade Impl class for a EDH Implementation
 *
 * @author ravjonnalagadda
 */

public class CatIntegrationFacadeImpl implements CatIntegrationFacade
{

	@Resource(name = "catIntegrationService")
	CatIntegrationService catIntegrationService;

	@Resource(name = "catEDHInventoryDetailsConverter")
	private Converter<CatEDHInventoryDetailsData, CatEDHInventoryDetailsDataList> catEDHInventoryDetailsConverter;


	/**
	 * @return the catEDHInventoryDetailsConverter
	 */
	public Converter<CatEDHInventoryDetailsData, CatEDHInventoryDetailsDataList> getCatEDHInventoryDetailsConverter()
	{
		return catEDHInventoryDetailsConverter;
	}

	/**
	 * @param catEDHInventoryDetailsConverter
	 *           the catEDHInventoryDetailsConverter to set
	 */
	public void setCatEDHInventoryDetailsConverter(
			final Converter<CatEDHInventoryDetailsData, CatEDHInventoryDetailsDataList> catEDHInventoryDetailsConverter)
	{
		this.catEDHInventoryDetailsConverter = catEDHInventoryDetailsConverter;
	}

	/**
	 * {@inheritDoc}
	 *
	 */
	@Override
	public String getInventoryDetails(final CatEDHInventoryDetailsData inventoryDetailsData) throws CatException
	{
		final CatEDHInventoryDetailsDataList edhInventoryDetailsDataList = catEDHInventoryDetailsConverter
				.convert(inventoryDetailsData);

		return catIntegrationService.getInventoryDetails(edhInventoryDetailsDataList);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getInventoryCountByPartyId(final String partyId, final String config, final String getDescendantNodes)
			throws CatException
	{
		return catIntegrationService.getInventoryCountByPartyId(partyId, config, getDescendantNodes);
	}



}
