/**
 *
 */
package com.cat.bcp.facades.integration.impl;


import de.hybris.platform.commercefacades.order.CartFacade;

import javax.annotation.Resource;

import com.cat.bcp.core.integration.service.CatSnopService;
import com.cat.bcp.facades.integration.CatSnopFacade;
import com.cat.core.integration.snop.response.CatSnopResponseMapper;



/**
 * The Facade Impl class for a FBC Implementation
 *
 * @author ravjonnalagadda
 */

public class CatSnopFacadeImpl implements CatSnopFacade
{

	@Resource(name = "catSnopService")
	CatSnopService catSnopService;

	@Resource(name = "cartFacade")
	CartFacade cartFacade;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CatSnopResponseMapper validateSnopRequest()
	{
		return catSnopService.validateSnopRequest(cartFacade.getSessionCart());

	}



}
