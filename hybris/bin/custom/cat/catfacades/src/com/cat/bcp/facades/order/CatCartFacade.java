/**
 *
 */
package com.cat.bcp.facades.order;

import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.CatAdditionalInfoData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.cat.bcp.core.enums.LaneTypeEnum;
import com.cat.bcp.facades.utility.CatCartDataErrorMap;
import com.cat.facades.order.PurchaseOrderNumberData;
import com.cat.facades.product.CatAccessoryData;


/**
 * @author vjagannadharaotel
 *
 */
public interface CatCartFacade extends CartFacade
{

	/**
	 * This method is used to clear cart entries from the session cart.
	 */
	void clearCart();


	/**
	 * This method is used to update the cart for configurable product
	 *
	 * @param productCode
	 *           - prod code
	 * @param qty
	 *           - quantity
	 * @param entryNumber
	 *           - entry Number
	 */
	void updateCartForConfigurableProducts(String productCode, long qty, final Integer entryNumber);


	/**
	 * This method is used to update purchase order number list for cart entries.
	 *
	 * @param cartEntryList
	 *           List of OrderEntyData
	 * @param purchaseOrderNumberDataSet
	 *           List of PurchaseOrderNumberData
	 * @param isTruckload
	 *           Value is true when its truckload
	 * @return List<PurchaseOrderNumberData>
	 */
	List<PurchaseOrderNumberData> updateCartEntyPOList(List<OrderEntryData> cartEntryList,
			List<PurchaseOrderNumberData> purchaseOrderNumberDataSet, boolean isTruckload);

	/**
	 * This method is used to update re-order cart entry.
	 *
	 * @param entryNumber
	 *           Entry Number
	 * @param quantity
	 *           Quantity
	 * @return CartModificationData CartModificationData Object
	 * @throws CommerceCartModificationException
	 *            CommerceCartModification Exception
	 */
	CartModificationData updateReOrderCartEntry(long entryNumber, long quantity) throws CommerceCartModificationException;

	/**
	 * This method is used to load review re order data.
	 *
	 * @param reviewReorderMap
	 *           review ReOrder Map
	 * @param orderEntryDataList
	 *           List of OrderEntryData
	 * @return Map.
	 */
	Map getReOrderData(final Map reviewReorderMap, List<OrderEntryData> orderEntryDataList);


	/**
	 * This method is used to get the cart type.
	 *
	 * @param productCode
	 *           the product code
	 * @return the cart type
	 */
	Map<String, Object> getCartType(String productCode);

	/**
	 * This method used to remove un-approved prdoucts from list.
	 *
	 * @param abstractOrderEntryModelList
	 *           order entry list
	 */
	void removeUnapprovedProductsFromCart(final List<AbstractOrderEntryModel> abstractOrderEntryModelList);

	/**
	 * This method adds product to a cart
	 *
	 * @param code
	 *           code of product to add
	 * @param quantity
	 *           the quantity of the product
	 * @param shipToAfterDate
	 *           shipToAfterDate value for a cart entry
	 * @param uniqueTruckloadId
	 *           the truckload id
	 * @param imageUrl
	 *           the truckload image url
	 * @return the cart modification data that includes a statusCode and the actual quantity added to the cart
	 * @throws CommerceCartModificationException
	 *            if the cart cannot be modified
	 */
	CartModificationData addToCart(String code, int quantity, Date shipToAfterDate, String uniqueTruckloadId, String imageUrl)
			throws CommerceCartModificationException;


	/**
	 * This method is used for updating or deleting cart entries.
	 *
	 * @param entryNumber
	 *           entry number which is to be updated or deleted
	 * @param quantity
	 *           quantity which is to be updated
	 * @return List of CartModificationData
	 * @throws CommerceCartModificationException
	 *            CommerceCartModification Exception
	 */
	List<CartModificationData> catUpdateCartEntry(long entryNumber, long quantity) throws CommerceCartModificationException;


	/**
	 * This method is used to add New EPP Product To cart.
	 *
	 * @param addToCartParams
	 *           addToCart params includes csahours, years, csa/epp option
	 * @return CartModificationData
	 * @throws CommerceCartModificationException
	 *            -CommerceCartModification Exception
	 */
	CartModificationData addNewAdditionalProductToCart(AddToCartParams addToCartParams) throws CommerceCartModificationException;


	/**
	 * This method is used to update EPP Cart Entry.
	 *
	 * @param valueOf
	 *           valueOf
	 * @param addToCartParams
	 *           addTCartParams includes parameters such as eppOption,csaOption,csaHours and CsaYear
	 * @return CartModificationData CartModificationData Object
	 *
	 * @throws CommerceCartModificationException
	 *            CommerceCartModificationException
	 */
	CartModificationData updateAdditionalCartEntry(AddToCartParams addToCartParams, long valueOf)
			throws CommerceCartModificationException;


	/**
	 * This method redirects to appropriate Review order page
	 *
	 * @param userType
	 *           - user type
	 * @param productCode
	 *           - product code
	 * @param orderQuantity
	 *           - order order quantity
	 * @param selectedModel
	 *           selected Model
	 * @return redirectUrl - redirectUrl
	 */
	String redirectIfNotSP(String userType, String productCode, int orderQuantity, String selectedModel);


	/**
	 * This method is used to fetch Cat Additional Info(Used for EPP/CSA drop-down options)
	 *
	 * @param type
	 *           type is CSA or EPP
	 * @return CatAdditionalInfoData
	 */
	CatAdditionalInfoData getCatAdditionalInfo(String type);

	/**
	 * Method to add accessories to cart
	 *
	 * @param accessories
	 *           List of CatAccessoryData Object
	 * @throws CommerceCartModificationException
	 *            CommerceCartModificationException
	 */
	void addAccessoriesToCart(List<CatAccessoryData> accessories) throws CommerceCartModificationException;

	/**
	 * Get Duplicate PO numbers for Accessores.
	 *
	 * @param purchaseOrderNumberDataList
	 *           purchaseOrderNumberDataList
	 * @return list of PurchaseOrderNumberData
	 */
	List<PurchaseOrderNumberData> getDuplicatePosForAccessories(final List<PurchaseOrderNumberData> purchaseOrderNumberDataList);

	/**
	 * This method is used to validate and Update Purchase Order number for Accessories.
	 *
	 * @param purchaseOrderNumberDataList
	 *           purchase Order Number DataList
	 * @return List of PurchaseOrderNumberData
	 */
	List<PurchaseOrderNumberData> validateAndUpdatePONumbersForAccessories(
			List<PurchaseOrderNumberData> purchaseOrderNumberDataList);

	/**
	 * This method is used to get Cart Model for Current User.
	 *
	 * @return CartModel Object
	 */
	CartModel getCartModelForCurrentUser();



	/**
	 * This method checks if existing cart contains accessories.
	 *
	 * @param cartData
	 *           the cart data
	 * @return true if accessories product is present in cart else return false
	 */
	Boolean isAccessoriesCart(CartData cartData);

	/**
	 * This method is used to clear cart Based on order window and other validations.
	 *
	 * @param laneValidationMap
	 *           the lane validation map
	 * @return the cart validation map
	 */
	CatCartDataErrorMap getCartValidationMap(Map<LaneTypeEnum, String> laneValidationMap);


	/**
	 * Method to return lane validation map.
	 *
	 * @return the lane validation map
	 */
	Map<LaneTypeEnum, String> getLaneValidationMap();


	/**
	 * Clear and recalculate cart.
	 */
	void clearAndRecalculateCart();



	/**
	 * Sets the cart lane type on cart.
	 *
	 * @param laneType
	 *           the new cart lane type on cart
	 */
	void setCartLaneTypeOnCart(String laneType);


	/**
	 * Adds the to cart.
	 *
	 * @param productCodePost
	 *           - product Code
	 * @param qty
	 *           - quantity
	 * @param configurable
	 *           - is Configurable
	 * @param laneType
	 *           the lane type
	 * @return the cart modification data
	 * @throws CommerceCartModificationException
	 *            the commerce cart modification exception
	 */
	CartModificationData addToCart(String productCodePost, long qty, boolean configurable, String laneType)
			throws CommerceCartModificationException;

}
