/**
 *
 */
package com.cat.bcp.facades.order;

import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.exceptions.BusinessException;

import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.cat.bcp.core.model.ShipToAfterDateModel;
import com.cat.bcp.facades.address.CatReviewOrderData;
import com.cat.core.integration.snop.response.CatSnopResponseMapper;
import com.cat.facades.order.CATOrderReviewData;
import com.cat.facades.order.PurchaseOrderCheck;


/**
 * @author manjam
 *
 */
public interface CatCheckoutFacade extends CheckoutFacade
{

	/**
	 * This method takes review order data from controller and creates an order by calling respective service methods and
	 * returns the order list.
	 *
	 * @param reviewOrderData
	 *           The review order data details that we receive from controller
	 * @return List of OrderData
	 * @throws BusinessException
	 *            Business Exception
	 */
	OrderData placeCatOrder(CATOrderReviewData reviewOrderData) throws BusinessException;


	/**
	 * This method is used to load Review-Order Data.
	 *
	 * @param productCode
	 *           Product Code
	 * @param salesModelCode
	 *           Sales Model Code
	 * @param reOrderProductQuantity
	 *           ReOrder Product Quantity
	 * @return CatReviewOrderData
	 */
	CatReviewOrderData getReviewOrderData(final String productCode, final String salesModelCode, final int reOrderProductQuantity);

	/**
	 * Add products to cart for ReOrder.
	 *
	 * @param productCode
	 *           Product Code
	 * @param reOrderProductQuantity
	 *           ReOrder Product Quantity
	 */
	void addToCart(final String productCode, final int reOrderProductQuantity);

	/**
	 * This method is to check if the given PO number exist against a customer. This method calls the corresponding
	 * service method with a list of PO numbers which needs to be validated.
	 *
	 * @param purchaseOrderNumberList
	 *           Purchase Order Number List
	 * @return PurchaseOrderCheck
	 */
	PurchaseOrderCheck checkPOList(List<String> purchaseOrderNumberList);

	/**
	 * Method to get list of shipping addresses
	 *
	 * @param addressModels
	 *           Address Models
	 * @return List of AddressData
	 */
	List<AddressData> getDealerShippingAddress(Collection<AddressModel> addressModels);


	/**
	 * This method will call the service which will trigger business process inorder to split the parent Order based on
	 * list of purchase order number
	 *
	 * @param orderCode
	 *           Parent Order Code
	 * @param poList
	 *           list of PO Numbers
	 */

	void splitOrderByPO(String orderCode, List<String> poList);


	/**
	 * This method is used to split Accessories Order.
	 *
	 * @param orderCode
	 *           order Code
	 */
	void splitOrderForAccessories(String orderCode);

	/**
	 * This method will place re-order for the session cart.
	 *
	 * @param comments
	 *           Comments
	 * @param poNumbersList
	 *           PO Numbers List
	 * @param selectedShippingAddressPK
	 *           selected Shipping Address PK
	 * @return OrderData
	 */
	OrderData placeCatReOrder(String comments, List<String> poNumbersList, String selectedShippingAddressPK);

	/**
	 * This method is used to Place Order for Accessories.
	 *
	 * @param comments
	 *           comments
	 * @param selectedShippingAddressPK
	 *           PK for shipping Address
	 * @return Order Data Object
	 */
	OrderData placeCatOrderForAccessories(String comments, String selectedShippingAddressPK);

	/**
	 * This method will used to check product of type utv.
	 *
	 * @param productData
	 *           productData Object
	 * @return boolean
	 */
	boolean checkUTVProduct(final ProductData productData);

	/**
	 * This method is to create order additional info table for an order with given order code.
	 *
	 * @param orderCode
	 *           Order Code.
	 * @param productCode
	 *           Product Code
	 */
	void createAdditionalInfo(final String orderCode, final String productCode);

	/**
	 * Gets the config products from category.
	 *
	 * @param categoryCode
	 *           the category code
	 * @return the config products from category
	 */
	List<ProductData> getConfigProductsFromCategory(String categoryCode);


	/**
	 * @return List of truckload data
	 */
	List<TruckloadData> getTruckloadCombinationList();


	/**
	 * This method validates truckload
	 *
	 * @param twoSeaterQtyArray
	 *           - 2 seaters
	 * @param fiveSeaterQtyArray
	 *           - 5 seaters
	 * @return boolean - is truckload valid or not
	 */
	boolean validateTruckload(int[] twoSeaterQtyArray, int[] fiveSeaterQtyArray);


	/**
	 * Get list of Formatted ship after dates
	 *
	 * @param shipToDates
	 *           - the ship to date
	 * @return Map - Map of Date and Formatted date string
	 */
	Map<Date, String> getListOfFormattedShipToDates(List<ShipToAfterDateModel> shipToDates);


	/**
	 * Populate truckload recommendation
	 *
	 * @param twoSeaterQtyArray
	 *           - 2 seaters
	 * @param fiveSeaterQtyArray
	 *           - 5 seaters
	 * @return List - Truckload data list
	 */
	List<TruckloadData> populateTruckloadSuggestions(int[] twoSeaterQtyArray, int[] fiveSeaterQtyArray);



	/**
	 * Gets the appropriate truckload image
	 *
	 * @param twoSeaterQtyArray
	 *           - 2 seater
	 * @param fiveSeaterQtyArray
	 *           - 5 seater
	 * @return String - url
	 */
	String getTruckloadImage(int[] twoSeaterQtyArray, int[] fiveSeaterQtyArray);

	/**
	 * This method removes the cart entry with given truckload id.
	 *
	 * @param truckloadId
	 *           the truckload id
	 * @throws CalculationException
	 *            the calculation exception
	 */
	void removeTruckload(String truckloadId) throws CalculationException;

	/**
	 * This method retains all the user added truckloads
	 *
	 * @param cartData
	 *           - Cart data
	 * @return Map- Map of truckload related data
	 */
	Map<Object, Object> retainTruckloads(CartData cartData);


	/**
	 * This method calculates total 2 seaters
	 *
	 * @param twoSeaterQtyArr
	 *           - Two seaters qty
	 * @return int- total 2 seaters
	 */
	int calculateTwoSeaters(int[] twoSeaterQtyArr);

	/**
	 * This method calculates total 5 seaters
	 *
	 * @param fiveSeaterQtyArr
	 *           - Two seaters qty
	 * @return int- total 5 seaters
	 */
	int calculateFiveSeaters(int[] fiveSeaterQtyArr);



	/**
	 * This method sets the suggestion list and The error message
	 *
	 * @param resultMap
	 *           - Result map
	 * @param twoSeaterQtyArray
	 *           - total 2 seaters
	 * @param fiveSeaterQtyArray
	 *           - total 5 seaters
	 * @return map- result map
	 */
	Map<String, Object> setSuggestionAndMessage(Map<String, Object> resultMap, int[] twoSeaterQtyArray, int[] fiveSeaterQtyArray);



	/**
	 * This methods performs add to cart
	 *
	 * @param quantities
	 *           -qty
	 * @param productCodes
	 *           -prod code
	 * @param productNames
	 *           - prod name
	 * @param shipAfterDate
	 *           - ship after date
	 * @param resultMap
	 *           - result map
	 * @param imageUrl
	 *           - truckload image url
	 * @return map - result map
	 * @throws CommerceCartModificationException
	 *            the commerce cart modification exception
	 * @throws ParseException
	 *            the parse exception
	 */
	Map<String, Object> doAddToCart(int[] quantities, String[] productCodes, String[] productNames, String shipAfterDate,
			Map<String, Object> resultMap, String imageUrl) throws CommerceCartModificationException, ParseException;

	/**
	 * This method is used to send Order Confirmation Email.
	 *
	 * @param orderCode
	 *           Order Code
	 */
	void sendCatOrderConfirmationEmail(final String orderCode);

	/**
	 * This method is used to update ReOrdered Quantity in ProductLaneInfo Table.
	 *
	 * @param parentOrder
	 *           parentOrder
	 */
	void updateReOrderedQuantityForProductLaneInfo(final OrderModel parentOrder);


	/**
	 * This method populates Snop Response
	 * 
	 * @param catSnopResponseMapper
	 *           - response
	 * @param laneType
	 *           - lane
	 * @return
	 * @throws ParseException
	 */
	List<Map<String, Object>> populateResponse(CatSnopResponseMapper catSnopResponseMapper, String laneType) throws ParseException;
}
