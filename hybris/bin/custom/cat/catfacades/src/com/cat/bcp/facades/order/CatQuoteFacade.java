/**
 *
 */
package com.cat.bcp.facades.order;

import de.hybris.platform.commercefacades.order.QuoteFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CatReserveQuoteData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.quote.data.QuoteData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.enums.DiscountType;
import de.hybris.platform.core.enums.QuoteState;

import java.util.List;
import java.util.Map;

import com.cat.bcp.core.enums.CatQuoteLostReasonEnum;
import com.cat.facades.quote.CatQuoteVETData;


/**
 * The Interface CatQuoteFacade.
 *
 * @author prrungta
 */
public interface CatQuoteFacade extends QuoteFacade
{

	/**
	 * Apply quote discount entry level.
	 *
	 * @param entryNumber
	 *           the entry number for which discount is to be applied
	 * @param discountRate
	 *           the discount to be applied to the entry
	 * @param discountType
	 *           the discount type i.e % or $
	 */
	void applyQuoteDiscountEntryLevel(long entryNumber, double discountRate, DiscountType discountType);


	/**
	 * remove existing discount values on cart level.
	 */
	void refreshCartData();

	/**
	 * Get the enum values from CatQuotationLostReasonEnum.
	 *
	 * @return Map<String, CatQuoteLostReasonEnum> so that can be set in a model attribute
	 */
	Map<String, CatQuoteLostReasonEnum> getQuotationLostReasons();


	/**
	 * Get the enum values from QuoteStateEnum based on the sales person role.
	 *
	 * @return Map<String, QuoteState> so that can be set in a model attribute
	 */
	Map<String, QuoteState> getQuoteStatusesForSalesPerson();


	/**
	 * Delete the quote when a user navigates away from the quote creation page as quote is already created.
	 *
	 * @param quoteCode
	 *           The quote will be fetched on the basis of this code only
	 */
	void deleteQuote(String quoteCode);

	/**
	 * Save Quote Details.
	 *
	 * @param quoteData
	 *           The data which needs to be converted to quoteModel
	 * @param quoteCode
	 *           fetching the quote model on the basis of quote code
	 * @return Quote Data object.
	 */
	QuoteData saveQuoteDetails(QuoteData quoteData, String quoteCode);

	/**
	 * Update quote status.
	 *
	 * @param quoteCode
	 *           the quote code
	 * @param submittedToDSU
	 *           the submitted to DSU
	 * @throws Exception
	 *            the exception
	 *
	 */
	void updateQuoteStatus(String quoteCode, boolean submittedToDSU) throws Exception;


	/**
	 * Gets the order additional info data.
	 *
	 * @return the order additional info data
	 */
	List<String> getOrderAdditionalInfoData();

	/**
	 * This method is to get list of products with no stock for a quote.
	 *
	 * @param quoteCode
	 *           Quote which needs to be verified for stock level
	 *
	 * @return list of quote entries with no stock.
	 */
	List<ProductData> getQuoteItemsWithNoStock(final String quoteCode);


	/**
	 * This method is used to get Reservation Quote Data.
	 *
	 * @param productCode
	 *           product Code
	 * @return List of CatReserveQuoteData
	 */
	List<CatReserveQuoteData> getReservationQuoteData(String productCode);

	/**
	 *
	 * This method is used to update product with less stock in the replicated quote. 1. Remove product with no stock. 2.
	 * Update quantity to 1 when less stock available.
	 *
	 */
	void updateProductWithLessStock();

	/**
	 * This method is used to set Error Data for replicate quote.
	 *
	 * @param cartData
	 *           CartData Object
	 */
	void setCatQuoteErrorData(final CartData cartData);

	/**
	 * This method is used to update Quote Address.
	 *
	 * @param quoteCode
	 *           - quote code
	 * @param shippingAddressData
	 *           - address data
	 */
	void updateQuoteAddress(String quoteCode, AddressData shippingAddressData);

	/**
	 * This method is used to set error message For Cart used on Quote Page for apply Discount section.
	 *
	 * @param cartData
	 *           Cart Data
	 * @param entryNumber
	 *           Entry Number
	 * @param errorMessage
	 *           Error Message
	 * @return CartData Object
	 */
	CartData setErrorDataForCart(final CartData cartData, final long entryNumber, final String errorMessage);

	/**
	 * Activate quote and set it to edit mode. Also update quote with updated list of mandatory parts.
	 *
	 * @param quoteCode
	 *           the code of the quote to process
	 */
	void enableQuoteReplicate(final String quoteCode);


	/**
	 * Save reservation quote data.
	 *
	 * @param quoteCode
	 *           the quote code
	 * @param catReserveQuoteDataList
	 *           the cat reserve quote data list
	 */
	void saveReservationQuoteData(String quoteCode, List<CatReserveQuoteData> catReserveQuoteDataList);

	/**
	 * Adds the value estimation for quote entry.
	 *
	 * @param vetLink
	 *           the vet link
	 * @param vetProduct
	 *           the vet product
	 * @param entryNumber
	 *           the entry number
	 */
	void addValueEstimationForQuoteEntry(String vetLink, String vetProduct, long entryNumber);

	/**
	 * Removes the value estimation for quote entry.
	 *
	 * @param vetLink
	 *           the vet link
	 * @param vetProduct
	 *           the vet product
	 * @param entryNumber
	 *           the entry number
	 */
	void removeValueEstimationForQuoteEntry(String vetLink, String vetProduct, long entryNumber);

	/**
	 * Edits the value estimation for quote entry.
	 *
	 * @param catQuoteVETDatas
	 *           the cat quote VET datas
	 * @param entryNumber
	 *           the entry number
	 */
	void editValueEstimationForQuoteEntry(List<CatQuoteVETData> catQuoteVETDatas, long entryNumber);
}

