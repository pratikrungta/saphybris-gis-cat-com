/**
 *
 */
package com.cat.bcp.facades.order;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.List;

import com.cat.bcp.core.model.AlertProductModel;
import com.cat.bcp.core.model.RestockAlertModel;


/**
 * Class to seperate the reorder logic from Search Page Controller
 *
 * @author sagdhingra
 *
 */
public interface CatReorderFacade
{

	/**
	 * Method to get associated config products
	 *
	 * @param alertId
	 *           get alert products based on alert id
	 * @return List<AlertProductModel>
	 */
	List<AlertProductModel> getAssociatedConfigProducts(String alertId);

	/**
	 * Method to get low stock products
	 *
	 * @param defaultB2BUnit
	 *           dealer specific restrictions
	 * @param category
	 *           category code to be fetch the products
	 * @param salesModelId
	 *           fetch product based on sales model id
	 * @return List<RestockAlertModel>
	 */
	List<RestockAlertModel> getLowStockProducts(B2BUnitModel defaultB2BUnit, String category, String salesModelId);



	/**
	 * Method to get the pageable reorder data
	 *
	 * @param alertEntries
	 *           list of alert entries
	 * @param page
	 *           page number
	 * @param numOfProdsPerPage
	 *           number of products per page
	 * @return List<ProductData>
	 */
	List<ProductData> getPageableData(List<RestockAlertModel> alertEntries, int page, int numOfProdsPerPage);

	/**
	 * Method to get the reorder data
	 *
	 * @param alertEntries
	 *           list of alert entries
	 * @param defaultB2BUnit
	 *           dealer specific restriction
	 * @return List<ProductData>
	 */
	List<ProductData> getReorderProducts(final List<RestockAlertModel> alertEntries, B2BUnitModel defaultB2BUnit);


	/**
	 * Fetch Low Stock Products Categories based on AlertProduct Data.
	 *
	 * @param b2bUnitModel
	 *           for adding dealer specific restriction
	 * @return List<CategoryData>
	 */
	List<CategoryData> getLowStockProductsCategories(B2BUnitModel b2bUnitModel);
}
