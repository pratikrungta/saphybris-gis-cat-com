/**
 *
 */
package com.cat.bcp.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;


/**
 * @author vjagannadharaotel
 *
 */
public class CatCommerceCartParameterPopulator implements Populator<AddToCartParams, CommerceCartParameter>
{

	/**
	 * {@inheritDoc}
	 *
	 */
	@Override
	public void populate(final AddToCartParams source, final CommerceCartParameter target) throws ConversionException
	{
		target.setShipToAfterDate(source.getShipToAfterDate());
		target.setMandatoryProduct(source.getMandatoryProduct());
		target.setConfigVariantId(source.getConfigVariantId());
		target.setBasePrice(source.getBasePrice());
		target.setEppOption(source.getEppOption());
		target.setCsaOption(source.getCsaOption());
		target.setCsaHours(source.getCsaHours());
		target.setCsaYear(source.getCsaYear());
		target.setTruckloadId(source.getTruckloadId());
		target.setTruckloadImageUrl(source.getTruckloadImageUrl());
		target.setConfigurable(source.getConfigurable());
		if (StringUtils.isNotBlank(source.getCompatibleModel()))
		{
			target.setCompatibleModel(source.getCompatibleModel());
		}
		if (source.getShipToAfterDate() != null || BooleanUtils.isTrue(source.getConfigurable()))
		{
			//This is to create a new entry everytime we add quantities to cart.
			target.setCreateNewEntry(true);
		}
		else
		{
			target.setCreateNewEntry(false);
		}
		target.setLaneType(source.getLaneType());
	}

}
