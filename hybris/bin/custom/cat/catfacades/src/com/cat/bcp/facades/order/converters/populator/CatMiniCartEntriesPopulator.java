/**
 *
 */
package com.cat.bcp.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.converters.populator.AbstractOrderPopulator;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.CartModel;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author vjagannadharaotel
 *
 */
public class CatMiniCartEntriesPopulator extends AbstractOrderPopulator<CartModel, CartData>
{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void populate(final CartModel source, final CartData target)
	{
		if (source != null && CollectionUtils.isNotEmpty(source.getEntries()))
		{
			target.setEntries(getOrderEntryConverter().convertAll(source.getEntries()));
		}
	}

}
