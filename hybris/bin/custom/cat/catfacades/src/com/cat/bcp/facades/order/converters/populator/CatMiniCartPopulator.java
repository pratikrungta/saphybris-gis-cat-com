/**
 *
 */
package com.cat.bcp.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.converters.populator.MiniCartPopulator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;


/**
 * @author vjagannadharaotel
 *
 */
public class CatMiniCartPopulator extends MiniCartPopulator
{

	/**
	 * This method is used to calculate unit count for Config Variant Product for Mini Cart.
	 *
	 * @param source
	 *           AbstractOrderModel
	 * @return Total Unit Count
	 */
	@Override
	protected Integer calcTotalUnitCount(final AbstractOrderModel source)
	{
		int totalUnitCount = 0;
		if (source != null && (source.getEntries() != null))
		{
			final List<AbstractOrderEntryModel> configVariantList = source.getEntries().stream()
					.filter(entry -> !entry.getMandatoryProduct().booleanValue() && StringUtils.isBlank(entry.getConfigVariantId()))
					.collect(Collectors.toList());

			for (final AbstractOrderEntryModel orderEntryModel : configVariantList)
			{
				totalUnitCount += orderEntryModel.getQuantity().intValue();
			}
		}
		return Integer.valueOf(totalUnitCount);
	}

}
