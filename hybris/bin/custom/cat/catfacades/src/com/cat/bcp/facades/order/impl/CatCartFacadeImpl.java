/**
 *
 */
package com.cat.bcp.facades.order.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.CartRestorationData;
import de.hybris.platform.commercefacades.order.data.CatAdditionalInfoData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.order.impl.DefaultCartFacade;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartRestorationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.order.impl.DefaultCommerceCartCalculationStrategy;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.util.Config;

import java.text.ParseException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.enums.LaneTypeEnum;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.CatAdditionalInfoModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.ProductLaneInfoModel;
import com.cat.bcp.core.model.RestockAlertModel;
import com.cat.bcp.core.servicelayer.CatCartService;
import com.cat.bcp.core.servicelayer.CatConfigurableProductAddToCartMethodHook;
import com.cat.bcp.core.servicelayer.CatOrderLaneProductService;
import com.cat.bcp.core.servicelayer.CatProductService;
import com.cat.bcp.core.servicelayer.impl.CatOrderWindowCalculationUtility;
import com.cat.bcp.facade.product.CatStockFacade;
import com.cat.bcp.facades.customer.CatCustomerFacade;
import com.cat.bcp.facades.order.CatCartFacade;
import com.cat.bcp.facades.order.CatCheckoutFacade;
import com.cat.bcp.facades.order.CatReorderFacade;
import com.cat.bcp.facades.search.DealerSearchFacade;
import com.cat.bcp.facades.utility.CatCartDataErrorMap;
import com.cat.facades.order.PurchaseOrderCheck;
import com.cat.facades.order.PurchaseOrderNumberData;
import com.cat.facades.product.CatAccessoryData;


/**
 * @author vjagannadharaotel
 *
 */
public class CatCartFacadeImpl extends DefaultCartFacade implements CatCartFacade
{

	private static final Logger LOG = Logger.getLogger(CatCartFacadeImpl.class);

	private static final String UNAPPROVED = "unapproved";

	private static final String REMOVEUNAPPROVEDPRODUCTSWHILELOGIN = "remove.unapproved.productus.afterLogin";

	private static final String CART_PRODUCT_TYPE = "cartProductType";

	private static final String UPDATING_PO = "Updating PO numbers list for Config Id ::";

	private static final String PO_AS = "with PO numbers as ::";

	private static final String ORDER_WINDOW = "orderWindowValidation";

	private static final String COMPATIBLE_CHECK = "compatibilityValidation";

	private static final String STOCK_CHECK = "stockValidation";

	private static final String UNAPPROVED_CHECK = "unapprovedValidation";

	private static final String REORDERABLE_CHECK = "reorderableValidation";

	private static final String REORDERABLE = "reorderable";

	private static final String RESTOCK_CHECK = "restockValidation";

	private static final String RESTOCK = "restock";

	@Resource(name = "cartService")
	private CatCartService cartService;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "modelService")
	ModelService modelService;

	@Resource(name = "commerceCartCalculationStrategy")
	DefaultCommerceCartCalculationStrategy defaultCommerceCartCalculationStrategy;

	@Resource(name = "configurableProductAddToCartMethodHook")
	CatConfigurableProductAddToCartMethodHook catConfigurableProductAddToCartMethodHook;

	@Resource(name = "checkoutFacade")
	private CatCheckoutFacade checkoutFacade;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "catCustomerFacade")
	private CatCustomerFacade catCustomerFacade;

	@Resource(name = "dealerSearchFacade")
	private DealerSearchFacade dealerSearchFacade;

	@Resource(name = "catStockFacade")
	private CatStockFacade catStockFacade;

	/** The product facade. */
	@Resource(name = "productFacade")
	private ProductFacade productFacade;

	@Resource(name = "catProductService")
	private CatProductService catProductService;

	@Resource(name = "commerceCartService")
	private CommerceCartService commerceCartService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSiteService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "catOrderWindowCalculationUtility")
	private CatOrderWindowCalculationUtility catOrderWindowCalculationUtility;

	@Resource(name = "catOrderLaneProductService")
	private CatOrderLaneProductService catOrderLaneProductService;

	@Resource(name = "catReorderFacade")
	private CatReorderFacade catReorderFacade;

	@Resource(name = "orderEntryConverter")
	private Converter<AbstractOrderEntryModel, OrderEntryData> orderEntryConverter;


	boolean truckloadRedirect = false;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CartModificationData addToCart(final String code, final int quantity, final Date shipToAfterDate,
			final String uniqueTruckloadId, final String truckloadImageUrl) throws CommerceCartModificationException
	{
		final AddToCartParams params = new AddToCartParams();
		params.setProductCode(code);
		params.setQuantity(quantity);
		params.setTruckloadId(uniqueTruckloadId);
		params.setShipToAfterDate(shipToAfterDate);
		params.setMandatoryProduct(Boolean.FALSE);
		params.setConfigVariantId(StringUtils.EMPTY);
		params.setTruckloadImageUrl(truckloadImageUrl);
		return addToCart(params);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clearCart()
	{
		cartService.clearCart();
		final CartModel cartModel = cartService.getSessionCart();
		cartModel.setLaneType(null);
	}

	/**
	 * This method is used for adding Config Variant Product along with it's Part Variant Products to the Cart.
	 *
	 * @param code
	 *           product which is to be added
	 * @param quantity
	 *           quantity which is to be added
	 * @return CartModificationData
	 */
	@Override
	public CartModificationData addToCart(final String code, final long quantity) throws CommerceCartModificationException
	{
		final List<AddToCartParams> paramsList = new ArrayList();
		final AddToCartParams params = new AddToCartParams();
		params.setProductCode(code);
		params.setQuantity(quantity);
		params.setMandatoryProduct(Boolean.FALSE);
		params.setConfigVariantId("");
		paramsList.add(params);
		final ProductModel productModel = productService.getProductForCode(code);
		if (productModel instanceof ConfigVariantProductModel)
		{
			final ConfigVariantProductModel configVariantProductModel = (ConfigVariantProductModel) productModel;
			final BaseVariantProductModel baseProduct = (BaseVariantProductModel) configVariantProductModel.getBaseProduct();
			if (baseProduct != null)
			{
				final boolean configurable = baseProduct.getFbcMustSelectFlag() != null
						? baseProduct.getFbcMustSelectFlag().booleanValue() : false;
				if (!configurable)
				{
					final Collection<ProductReferenceModel> productReferenceCollection = configVariantProductModel
							.getProductReferences();
					addProductReferenceParams(configVariantProductModel, quantity, paramsList, productReferenceCollection);
				}
			}
		}
		final CartModificationData cartModificationData = catAddToCart(paramsList);
		final CartModel cartModel = cartService.getSessionCart();
		boolean isConfigurable = false;
		for (final AbstractOrderEntryModel entry : cartModel.getEntries())
		{
			if (BooleanUtils.isTrue(entry.getConfigurable()))
			{
				isConfigurable = true;
				break;
			}
		}
		if (isConfigurable)
		{
			updateCartForConfigurableProducts(code, quantity, null);
		}
		return cartModificationData;
	}

	/**
	 * This method is used to prepare AddToCartParams list by adding Part Variant Products to it.
	 *
	 * @param configVariantProductModel
	 *           configVariant product model
	 * @param paramsList
	 *           parameters list
	 * @param productReferenceCollection
	 *           product references against a product
	 * @return List of AddToCartParams
	 */
	public List<AddToCartParams> addProductReferenceParams(final ConfigVariantProductModel configVariantProductModel,
			final long quantity, final List<AddToCartParams> paramsList,
			final Collection<ProductReferenceModel> productReferenceCollection)
	{
		if (CollectionUtils.isNotEmpty(productReferenceCollection))
		{
			for (final ProductReferenceModel productReferenceModel : productReferenceCollection)
			{
				if (productReferenceModel.getTarget() != null)
				{
					LOG.debug(productReferenceModel.getTarget().getCode());
					final AddToCartParams paramsForReference = new AddToCartParams();
					paramsForReference.setProductCode(productReferenceModel.getTarget().getCode());
					paramsForReference.setQuantity(quantity);
					paramsForReference.setMandatoryProduct(Boolean.TRUE);
					paramsForReference.setConfigVariantId(configVariantProductModel.getCode());
					paramsList.add(paramsForReference);
				}
			}
		}
		return paramsList;
	}

	/**
	 * This method is used for adding Config Variant Product along with it's Part Variant Products to the Cart.
	 *
	 * @param addToCartParamsList
	 *           parameters to be added against the entry
	 * @return CartModificationData
	 * @throws CommerceCartModificationException
	 */
	public CartModificationData catAddToCart(final List<AddToCartParams> addToCartParamsList)
			throws CommerceCartModificationException
	{
		CommerceCartModification modification = new CommerceCartModification();
		for (final AddToCartParams addToCartParams : addToCartParamsList)
		{
			LOG.info("productCode - " + addToCartParams.getProductCode());
			final CommerceCartParameter parameter = getCommerceCartParameterConverter().convert(addToCartParams);
			modification = getCommerceCartService().addToCart(parameter);
		}
		return getCartModificationConverter().convert(modification);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CartModificationData> catUpdateCartEntry(final long entryNumber, final long quantity)
			throws CommerceCartModificationException
	{
		final AddToCartParams dto = new AddToCartParams();
		dto.setQuantity(quantity);
		final CommerceCartParameter parameter = getCommerceCartParameterConverter().convert(dto);
		parameter.setEnableHooks(true);
		parameter.setEnableHooks(true);
		parameter.setEntryNumber(entryNumber);

		final CartModel cartModel = parameter.getCart();
		final AbstractOrderEntryModel abstractOrderEntryForConfig = getEntryForNumber(cartModel, (int) entryNumber);
		updatecarEntryPOList(quantity, abstractOrderEntryForConfig);


		final List<AbstractOrderEntryModel> abstractOrderEntryModelList = abstractOrderEntryForConfig.getOrder().getEntries();

		final List<CommerceCartParameter> commerceCartParameterListForUpdate = new ArrayList();
		abstractOrderEntryForConfig.setDiscountValues(new ArrayList<>());
		modelService.save(abstractOrderEntryForConfig);
		modelService.save(cartModel);
		modelService.refresh(cartModel);

		final String prodCode = abstractOrderEntryForConfig.getProduct().getCode();

		boolean isConfigurable = false;
		for (final AbstractOrderEntryModel entry : cartService.getSessionCart().getEntries())
		{
			if (BooleanUtils.isTrue(entry.getConfigurable()))
			{
				isConfigurable = true;
				break;
			}
		}

		for (final AbstractOrderEntryModel abstractOrderEntryModel : abstractOrderEntryModelList)
		{
			if (StringUtils.isNotBlank(abstractOrderEntryModel.getConfigVariantId()) && abstractOrderEntryModel.getConfigVariantId()
					.equalsIgnoreCase(abstractOrderEntryForConfig.getProduct().getCode()))
			{
				if (quantity == 0)
				{
					modelService.remove(abstractOrderEntryModel);
				}
				else if (BooleanUtils.isTrue(abstractOrderEntryModel.getMandatoryProduct()))
				{
					final CommerceCartParameter parameterForPart = getCommerceCartParameterConverter().convert(dto);
					parameterForPart.setEnableHooks(true);
					parameterForPart.setEnableHooks(true);
					parameterForPart.setEntryNumber(abstractOrderEntryModel.getEntryNumber().longValue());
					commerceCartParameterListForUpdate.add(parameterForPart);
				}
			}
		}
		if (quantity == 0)
		{
			modelService.remove(abstractOrderEntryForConfig);
			saveAndReCalculateCart(cartModel);
		}
		else
		{
			commerceCartParameterListForUpdate.add(parameter);
		}
		return updateQuantityForConfigAndPartsEntries(commerceCartParameterListForUpdate, quantity, prodCode, isConfigurable,
				Long.valueOf(entryNumber));
	}

	/**
	 * This method is used to update Quantity For Cart Entry.
	 *
	 * @param commerceCartParameterListForUpdate
	 *           List of CommerceCartParameter
	 * @param abstractOrderEntryForConfig
	 *           - order entry
	 * @param quantity
	 *           - qty
	 * @param isConfigurable
	 * @param prodCode
	 * @param entryNumber
	 * @return List of CartModificationData
	 * @throws CommerceCartModificationException
	 *            CommerceCartModification Exception
	 */
	private List<CartModificationData> updateQuantityForConfigAndPartsEntries(
			final List<CommerceCartParameter> commerceCartParameterListForUpdate, final long quantity, final String prodCode,
			final boolean isConfigurable, final Long entryNumber) throws CommerceCartModificationException
	{
		final List<CartModificationData> cartModificationDataList = new ArrayList();
		if (CollectionUtils.isNotEmpty(commerceCartParameterListForUpdate))
		{
			for (final CommerceCartParameter commerceCartParameter : commerceCartParameterListForUpdate)
			{

				final CommerceCartModification commerceCartModification = getCommerceCartService()
						.updateQuantityForCartEntry(commerceCartParameter);
				cartModificationDataList.add(getCartModificationConverter().convert(commerceCartModification));
			}
		}
		if (isConfigurable)
		{
			updateCartForConfigurableProducts(prodCode, quantity, Integer.valueOf(entryNumber.toString()));
		}
		return cartModificationDataList;
	}


	/**
	 * This method is used to Save Cart Model Object and ReCalculate Cart
	 *
	 * @param cartModel
	 *           CartModel Object
	 */
	public void saveAndReCalculateCart(final CartModel cartModel)
	{
		if (cartModel != null)
		{
			modelService.save(cartModel);
			modelService.refresh(cartModel);
			defaultCommerceCartCalculationStrategy.recalculateCart(cartModel);
		}
	}

	/**
	 * This method is used to get AbstractOrderEntryModel for given entry number
	 *
	 * @param order
	 *           to get the entries
	 * @param number
	 *           requested entry number
	 * @return AbstractOrderEntryModel
	 */
	protected AbstractOrderEntryModel getEntryForNumber(final AbstractOrderModel order, final int number)
	{
		final List<AbstractOrderEntryModel> entries = order.getEntries();
		if (entries != null && !entries.isEmpty())
		{
			final Integer requestedEntryNumber = Integer.valueOf(number);
			for (final AbstractOrderEntryModel entry : entries)
			{
				if (entry != null && requestedEntryNumber.equals(entry.getEntryNumber()))
				{
					return entry;
				}
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CartModificationData addNewAdditionalProductToCart(final AddToCartParams addToCartParams)
			throws CommerceCartModificationException
	{
		if (validateAdditionalProductQty(addToCartParams, null))
		{
			if (StringUtils.isNotEmpty(addToCartParams.getEppOption()))
			{
				addToCartParams.setProductCode(CatCoreConstants.EPPPRODUCT);
			}
			else
			{
				addToCartParams.setProductCode(CatCoreConstants.CSAPRODUCT);
			}
			addToCartParams.setMandatoryProduct(Boolean.FALSE);
			addToCartParams.setConfigVariantId(addToCartParams.getConfigVariantId());
			return addToCart(addToCartParams);
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CartModificationData updateAdditionalCartEntry(final AddToCartParams addToCartParams, final long entryNumber)
			throws CommerceCartModificationException
	{
		if (validateAdditionalProductQty(addToCartParams, Integer.valueOf((int) entryNumber)))
		{
			final AddToCartParams dto = new AddToCartParams();
			dto.setQuantity(addToCartParams.getQuantity());
			final CommerceCartParameter parameter = getCommerceCartParameterConverter().convert(dto);
			parameter.setEnableHooks(true);
			parameter.setEnableHooks(true);
			parameter.setEntryNumber(entryNumber);

			final CommerceCartModification modification = getCommerceCartService().updateQuantityForCartEntry(parameter);

			parameter.setProduct(modification.getEntry().getProduct());
			parameter.setBasePrice(addToCartParams.getBasePrice());
			if (modification.getEntry().getProduct().getCode().equalsIgnoreCase(CatCoreConstants.EPPPRODUCT))
			{
				parameter.setEppOption(addToCartParams.getEppOption());
			}
			else
			{
				parameter.setCsaOption(addToCartParams.getCsaOption());
				parameter.setCsaHours(addToCartParams.getCsaHours());
				parameter.setCsaYear(addToCartParams.getCsaYear());
			}
			catConfigurableProductAddToCartMethodHook.catAfterAddToCart(parameter, modification);
			defaultCommerceCartCalculationStrategy.recalculateCart(parameter.getCart());
			return getCartModificationConverter().convert(modification);
		}
		return null;
	}

	/**
	 * Validate if EPP/CSA can be added to cart based on the total quantity.
	 *
	 * @param addToCartParams
	 *           addTCartParams includes parameters such as eppOption,csaOption,csaHours and CsaYear
	 * @return true/false
	 *
	 *         Return true when EPP/CSA can be added to cart or otherwise false.
	 *
	 */
	private boolean validateAdditionalProductQty(final AddToCartParams addToCartParams, final Integer entryNumber)
	{
		boolean addToCart = false;
		final CartModel cart = getCartService().getSessionCart();

		int totalQty = (int) addToCartParams.getQuantity();

		if (null == entryNumber)
		{
			if (StringUtils.isNotEmpty(addToCartParams.getEppOption()))
			{
				totalQty += cart.getEntries().stream()
						.filter(entry -> (entry.getProduct().getCode().equalsIgnoreCase(CatCoreConstants.EPPPRODUCT)
								&& entry.getConfigVariantId().equalsIgnoreCase(addToCartParams.getConfigVariantId())))
						.mapToInt(entry -> entry.getQuantity().intValue()).sum();
			}
			else
			{
				totalQty += cart.getEntries().stream()
						.filter(entry -> (entry.getProduct().getCode().equalsIgnoreCase(CatCoreConstants.CSAPRODUCT)
								&& entry.getConfigVariantId().equalsIgnoreCase(addToCartParams.getConfigVariantId())))
						.mapToInt(entry -> entry.getQuantity().intValue()).sum();
			}
		}
		else
		{
			if (StringUtils.isNotEmpty(addToCartParams.getEppOption()))
			{
				totalQty += cart.getEntries().stream()
						.filter(entry -> (entry.getProduct().getCode().equalsIgnoreCase(CatCoreConstants.EPPPRODUCT)
								&& entry.getConfigVariantId().equalsIgnoreCase(addToCartParams.getConfigVariantId())
								&& !entryNumber.equals(entry.getEntryNumber())))
						.mapToInt(entry -> entry.getQuantity().intValue()).sum();
			}
			else
			{
				totalQty += cart.getEntries().stream()
						.filter(entry -> (entry.getProduct().getCode().equalsIgnoreCase(CatCoreConstants.CSAPRODUCT)
								&& entry.getConfigVariantId().equalsIgnoreCase(addToCartParams.getConfigVariantId())
								&& !entryNumber.equals(entry.getEntryNumber())))
						.mapToInt(entry -> entry.getQuantity().intValue()).sum();
			}
		}

		final Optional<AbstractOrderEntryModel> configVariant = cart.getEntries().stream()
				.filter(entry -> entry.getProduct().getCode().equalsIgnoreCase(addToCartParams.getConfigVariantId())).findFirst();

		if (configVariant.isPresent() && configVariant.get().getQuantity().intValue() >= totalQty)
		{
			addToCart = true;
		}

		return addToCart;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CatAdditionalInfoData getCatAdditionalInfo(final String type)
	{
		final List<String> catAdditionalInfoDataList = new ArrayList();
		final CatAdditionalInfoData catAdditionalInfoData = new CatAdditionalInfoData();
		final List<CatAdditionalInfoModel> catAdditionalInfoModelList = cartService.getCatAdditionalInfo(type);
		if (CollectionUtils.isNotEmpty(catAdditionalInfoModelList))
		{
			for (final CatAdditionalInfoModel catAdditionalInfoModel : catAdditionalInfoModelList)
			{
				catAdditionalInfoDataList.add(catAdditionalInfoModel.getAdditionalInfoOption());
			}
			catAdditionalInfoData.setOptions(catAdditionalInfoDataList);
		}
		return catAdditionalInfoData;
	}


	/**
	 * @param cartEntryList
	 * @param purchaseOrderNumberDataSet
	 * @return List<PurchaseOrderNumberData>
	 */
	@Override
	public List<PurchaseOrderNumberData> updateCartEntyPOList(final List<OrderEntryData> cartEntryList,
			final List<PurchaseOrderNumberData> purchaseOrderNumberDataSet, final boolean isTruckload)
	{
		final List<PurchaseOrderNumberData> duplicatePoList = validatePurchaseOrderNumberList(purchaseOrderNumberDataSet,
				isTruckload);
		if (CollectionUtils.isEmpty(duplicatePoList))
		{
			final Map<String, Set<String>> reorderPoListMap = new HashMap<>();
			sessionService.setAttribute(CatCoreConstants.REORDER_PO_LIST, reorderPoListMap);
			if (isTruckload)
			{
				final Map<String, List<OrderEntryData>> groupByTruckloadIdMap = cartEntryList.stream()
						.collect(Collectors.groupingBy(OrderEntryData::getTruckloadId));
				saveCartEntryPOList(purchaseOrderNumberDataSet, groupByTruckloadIdMap, isTruckload);
			}
			else
			{
				final Map<String, List<OrderEntryData>> groupBySalesModelMap = new HashMap();

				for (final OrderEntryData oed : cartEntryList)
				{
					populateSalesModelMap(oed, groupBySalesModelMap);
				}
				saveCartEntryPOList(purchaseOrderNumberDataSet, groupBySalesModelMap, isTruckload);
			}

		}
		return duplicatePoList;
	}

	/**
	 * @param oed
	 *           - order entry
	 * @param groupBySalesModelMap
	 *           - sales model map
	 */
	private void populateSalesModelMap(final OrderEntryData oed, final Map<String, List<OrderEntryData>> groupBySalesModelMap)
	{
		final List<OrderEntryData> reviewOrderDataObjectList = new ArrayList();
		reviewOrderDataObjectList.add(oed);
		if (StringUtils.isEmpty(oed.getTruckloadId()))
		{
			groupBySalesModelMap.put(oed.getSalesModelId() + "__" + oed.getEntryNumber(), reviewOrderDataObjectList);
		}
		else
		{
			groupBySalesModelMap.put(oed.getSalesModelId() + "__" + oed.getTruckloadId(), reviewOrderDataObjectList);
		}

	}

	/**
	 * Method to update sales model's cart entries purchase order Number.
	 *
	 * @param purchaseOrderNumberDataSet
	 *           - po set
	 * @param cartEntryGroup
	 *           - list of order entry data
	 * @param isTruckload
	 *           - is truckload flow
	 */
	private void saveCartEntryPOList(final List<PurchaseOrderNumberData> purchaseOrderNumberDataSet,
			final Map<String, List<OrderEntryData>> cartEntryGroup, final boolean isTruckload)
	{
		/*
		 * Iterating over sales models in the cart
		 */
		if (!isTruckload)
		{
			saveNonTruckloadPO(cartEntryGroup, purchaseOrderNumberDataSet);
		}
		else
		{
			saveTruckloadPO(cartEntryGroup, purchaseOrderNumberDataSet);

		}
	}

	/**
	 * @param cartEntryGroup
	 *           - order data list
	 * @param purchaseOrderNumberDataSet
	 *           - PO set
	 */
	private void saveTruckloadPO(final Map<String, List<OrderEntryData>> cartEntryGroup,
			final List<PurchaseOrderNumberData> purchaseOrderNumberDataSet)
	{
		cartEntryGroup.forEach((mixMatchTruckloadId, cartEntries) -> {
			final List<PurchaseOrderNumberData> purchaseOrderNumberList = purchaseOrderNumberDataSet.stream()
					.filter(purchaseOrderNumberData -> mixMatchTruckloadId.equals(purchaseOrderNumberData.getMixMatchTruckloadId()))
					.collect(Collectors.toList());
			LOG.info(" Updating Purchase Order Data List for cart entries of salesModel :: " + mixMatchTruckloadId);
			final List<PurchaseOrderNumberData> poDataList = new ArrayList();
			poDataList.addAll(setPODataList(purchaseOrderNumberList, mixMatchTruckloadId));
			final Iterator<PurchaseOrderNumberData> iterator = poDataList.iterator();
			cartEntries.stream().forEach(cartEntryData -> {
				final AbstractOrderEntryModel cartEntryModel = getEntryForNumber(getCartService().getSessionCart(),
						cartEntryData.getEntryNumber().intValue());
				final boolean isUtv = catProductService.isUtvProduct(cartEntryModel.getProduct().getCode());
				if (iterator.hasNext())
				{

					final Set<String> cartEntryPOList = setPoList(iterator.next().getPurchaseOrderNumberConfigIdMap(), cartEntryModel)
							.stream().map(String::toUpperCase).collect(Collectors.toSet());
					cartService.updateCartEntryPONumbers(cartEntryModel, cartEntryPOList, isUtv, true);
				}
			});
		});

	}

	/**
	 * @param purchaseOrderNumberList
	 *           - purchase order data
	 * @param mixMatchTruckloadId
	 *           - truckload id
	 */
	private List<PurchaseOrderNumberData> setPODataList(final List<PurchaseOrderNumberData> purchaseOrderNumberList,
			final String mixMatchTruckloadId)
	{
		final Map<String, Map<String, Set<String>>> tlPoMap = purchaseOrderNumberList.get(0)
				.getTruckloadPurchaseOrderNumberConfigIdMap();
		final Map<String, Set<String>> poMap = tlPoMap.get(mixMatchTruckloadId);
		final Iterator<Entry<String, Set<String>>> poIterator = poMap.entrySet().iterator();
		final Map<String, Set<String>> tempMap = new HashMap();
		final List<PurchaseOrderNumberData> poDataList = new ArrayList();
		while (poIterator.hasNext())
		{
			final Entry<String, Set<String>> prodCode = poIterator.next();
			final PurchaseOrderNumberData poData = new PurchaseOrderNumberData();
			tempMap.put(prodCode.getKey(), prodCode.getValue());
			poData.setPurchaseOrderNumberConfigIdMap(tempMap);
			poDataList.add(poData);
		}
		return poDataList;
	}

	/**
	 * @param cartEntryGroup
	 *           - order data list
	 * @param purchaseOrderNumberDataSet
	 *           - po set
	 */
	private void saveNonTruckloadPO(final Map<String, List<OrderEntryData>> cartEntryGroup,
			final List<PurchaseOrderNumberData> purchaseOrderNumberDataSet)
	{
		cartEntryGroup.forEach((salesModelId, cartEntries) -> {

			final List<PurchaseOrderNumberData> purchaseOrderNumberList = purchaseOrderNumberDataSet.stream()
					.filter(purchaseOrderNumberData -> salesModelId.equals(purchaseOrderNumberData.getSalesModelId()))
					.collect(Collectors.toList());
			LOG.info(" Updating Purchase Order Data List for cart entries of salesModel :: " + salesModelId);
			final Iterator<PurchaseOrderNumberData> iterator = purchaseOrderNumberList.iterator();
			if (CollectionUtils.isNotEmpty(purchaseOrderNumberList))
			{
				final Map<String, Set<String>> purchaseOrderNumberConfigIdMap = purchaseOrderNumberList.get(0)
						.getPurchaseOrderNumberConfigIdMap();
				cartEntries.stream().forEach(cartEntryData -> {
					final AbstractOrderEntryModel cartEntryModel = getEntryForNumber(getCartService().getSessionCart(),
							cartEntryData.getEntryNumber().intValue());
					final boolean isUtv = catProductService.isUtvProduct(cartEntryModel.getProduct());
					updateOnUtvFlag(isUtv, purchaseOrderNumberConfigIdMap, cartEntryModel, iterator);

				});
			}
		});

	}


	/**
	 * @param isUtv
	 *           - utv flag
	 * @param purchaseOrderNumberConfigIdMap
	 *           - po map
	 * @param cartEntryModel
	 *           - order entry list
	 * @param iterator
	 */
	private void updateOnUtvFlag(final boolean isUtv, final Map<String, Set<String>> purchaseOrderNumberConfigIdMap,
			final AbstractOrderEntryModel cartEntryModel, final Iterator<PurchaseOrderNumberData> iterator)
	{
		if (!isUtv)
		{
			updateBCPCartEntry(isUtv, purchaseOrderNumberConfigIdMap, cartEntryModel);

		}
		else if (iterator.hasNext())
		{

			final Map<String, Set<String>> purchaseOrderNumberConfigIdMapCuv = iterator.next().getPurchaseOrderNumberConfigIdMap();
			updateCUVCartEntry(isUtv, purchaseOrderNumberConfigIdMapCuv, cartEntryModel);

		}

	}

	/**
	 * @param isUtv
	 *           - utv flag
	 * @param purchaseOrderNumberConfigIdMap
	 *           - config map
	 * @param cartEntryModel
	 *           - order entry list
	 */
	private void updateCUVCartEntry(final boolean isUtv, final Map<String, Set<String>> purchaseOrderNumberConfigIdMapCuv,
			final AbstractOrderEntryModel cartEntryModel)
	{
		Set<String> cartEntryPOList = setPoList(purchaseOrderNumberConfigIdMapCuv, cartEntryModel);
		cartEntryPOList = cartEntryPOList.stream().map(String::toUpperCase).collect(Collectors.toSet());
		LOG.info(UPDATING_PO + cartEntryModel.getProduct().getCode() + PO_AS + cartEntryPOList);
		cartService.updateCartEntryPONumbers(cartEntryModel, cartEntryPOList, isUtv, false);

	}

	/**
	 * @param isUtv
	 *           - utv flag
	 * @param purchaseOrderNumberConfigIdMap
	 *           - config map
	 * @param cartEntryModel
	 *           - order entry list
	 */
	private void updateBCPCartEntry(final boolean isUtv, final Map<String, Set<String>> purchaseOrderNumberConfigIdMap,
			final AbstractOrderEntryModel cartEntryModel)
	{
		Set<String> cartEntryPOList = setPoList(purchaseOrderNumberConfigIdMap, cartEntryModel);
		cartEntryPOList = cartEntryPOList.stream().map(String::toUpperCase).collect(Collectors.toSet());
		LOG.info(UPDATING_PO + cartEntryModel.getProduct().getCode() + PO_AS + cartEntryPOList);
		cartService.updateCartEntryPONumbers(cartEntryModel, cartEntryPOList, isUtv, false);
	}

	/**
	 * This method sets PO List
	 *
	 * @param purchaseOrderNumberConfigIdMap
	 *           - PO Map
	 * @param isUtv
	 *           - is UTV
	 * @param cartEntryModel
	 *           - The cart entry
	 * @return Set of PO numbers
	 */
	private Set<String> setPoList(final Map<String, Set<String>> purchaseOrderNumberConfigIdMap,
			final AbstractOrderEntryModel cartEntryModel)
	{
		Set<String> cartEntryPOList;
		if (StringUtils.isNotEmpty(cartEntryModel.getTruckloadId()))
		{
			cartEntryPOList = purchaseOrderNumberConfigIdMap
					.get(cartEntryModel.getProduct().getCode() + "__" + cartEntryModel.getTruckloadId());
		}
		else
		{
			cartEntryPOList = purchaseOrderNumberConfigIdMap
					.get(cartEntryModel.getProduct().getCode() + "__" + cartEntryModel.getEntryNumber());
		}
		return cartEntryPOList == null ? new HashSet() : cartEntryPOList;
	}

	/**
	 * Method to check duplicate purchased order number.
	 *
	 * @param isTruckload
	 *
	 * @param purchaseOrderNumberDataSet
	 * @return List<PurchaseOrderNumberData>
	 */
	private List<PurchaseOrderNumberData> validatePurchaseOrderNumberList(
			final List<PurchaseOrderNumberData> purchaseOrderNumberDataList, final boolean isTruckload)
	{
		LOG.debug("start of  purchase Order Number List validation ");
		final List<PurchaseOrderNumberData> duplicatePOList = new ArrayList<>();
		purchaseOrderNumberDataList.forEach(purchaseOrderNumberData -> {
			final PurchaseOrderCheck result = checkoutFacade
					.checkPOList(purchaseOrderNumberData.getPurchaseOrderNumberSet().stream().collect(Collectors.toList()));
			LOG.info("Duplicate Purchase Order Number list" + result.getListOfErrorPO());
			populateDuplicatePOList(isTruckload, result, purchaseOrderNumberData, duplicatePOList);
		});
		LOG.debug("end of  purchase Order Number List Validation");
		return duplicatePOList;
	}

	/**
	 * @param isTruckload
	 *           - truckload flag
	 * @param result
	 *           - result
	 * @param purchaseOrderNumberData
	 *           - po data
	 * @param duplicatePOList
	 *           - po list
	 */
	private void populateDuplicatePOList(final boolean isTruckload, final PurchaseOrderCheck result,
			final PurchaseOrderNumberData purchaseOrderNumberData, final List<PurchaseOrderNumberData> duplicatePOList)
	{
		if (!isTruckload && result.getPurchaseOrderPresent().equals(Boolean.FALSE))
		{
			final PurchaseOrderNumberData duplicatePOData = new PurchaseOrderNumberData();
			duplicatePOList
					.addAll(setDuplicatePOData(duplicatePOData, result, new StringBuilder(purchaseOrderNumberData.getSalesModelId())));
		}
		else if (isTruckload && result.getPurchaseOrderPresent().equals(Boolean.FALSE))
		{
			purchaseOrderNumberData.getTruckloadPurchaseOrderNumberConfigIdMap().entrySet().forEach(truckloadConfigData -> {
				final PurchaseOrderNumberData duplicatePOData = new PurchaseOrderNumberData();
				final Map<String, Set<String>> truckloadMapEntry = purchaseOrderNumberData
						.getTruckloadPurchaseOrderNumberConfigIdMap().get(truckloadConfigData.getKey());
				final Set<String> salesModelIdList = truckloadMapEntry.keySet();
				final StringBuilder salesModelId = formSalesModelId(salesModelIdList);
				duplicatePOData.setMixMatchTruckloadId(truckloadConfigData.getKey());
				duplicatePOList.addAll(setDuplicatePOData(duplicatePOData, result, salesModelId));
			});
		}

	}

	/**
	 * @param salesModelIdList
	 *           - list of sales model Id
	 * @return String - comma separated sales model id
	 */
	private StringBuilder formSalesModelId(final Set<String> salesModelIdList)
	{
		final StringBuilder salesModelId = new StringBuilder();
		for (final String prodCode : salesModelIdList)
		{
			if (StringUtils.isNotEmpty(salesModelId))
			{
				salesModelId.append(",");
			}
			salesModelId.append(prodCode);
		}
		return salesModelId;
	}

	/**
	 * @param duplicatePOData
	 *           - duplicate po data
	 * @param result
	 *           - result
	 * @param salesModelId
	 *           - sales model id
	 * @param truckloadConfigData
	 *           - truckload config data
	 * @return list- duplicate po list
	 */
	private List<PurchaseOrderNumberData> setDuplicatePOData(final PurchaseOrderNumberData duplicatePOData,
			final PurchaseOrderCheck result, final StringBuilder salesModelId)
	{
		final List<PurchaseOrderNumberData> duplicatePOList = new ArrayList<>();
		duplicatePOData.setPurchaseOrderNumberSet(result.getListOfErrorPO().stream().collect(Collectors.toSet()));
		duplicatePOData.setIsValid(result.getPurchaseOrderPresent());
		duplicatePOData.setSalesModelId(salesModelId.toString());
		duplicatePOData.setDuplicatePONumbers(result.getDuplicatePONumbers());
		duplicatePOList.add(duplicatePOData);
		return duplicatePOList;
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public CartModificationData updateReOrderCartEntry(final long entryNumber, final long quantity)
			throws CommerceCartModificationException
	{
		final AddToCartParams dto = new AddToCartParams();
		dto.setQuantity(quantity);
		final CommerceCartParameter parameter = getCommerceCartParameterConverter().convert(dto);
		parameter.setEnableHooks(true);
		parameter.setEnableHooks(true);
		parameter.setEntryNumber(entryNumber);
		final CommerceCartModification modification = getCommerceCartService().updateQuantityForCartEntry(parameter);
		final AbstractOrderEntryModel orderEntry = modification.getEntry();
		updatecarEntryPOList(quantity, orderEntry);
		return getCartModificationConverter().convert(modification);
	}

	/**
	 * Method to update cart entry purchase order Number list based on cart entry quantity.
	 *
	 * @param quantity
	 * @param orderEntry
	 */
	private void updatecarEntryPOList(final long quantity, final AbstractOrderEntryModel orderEntry)
	{
		final Map currentSessionPOList = (Map) sessionService.getAllAttributes().get(CatCoreConstants.REORDER_PO_LIST);
		if (null != currentSessionPOList && !currentSessionPOList.isEmpty()
				&& currentSessionPOList.containsKey(orderEntry.getProduct().getCode()))
		{
			final Map purchaseOrderMap = new HashMap<>();
			purchaseOrderMap.putAll(currentSessionPOList);
			final Set cartEntryPoSet = (Set) currentSessionPOList.get(orderEntry.getProduct().getCode());
			if (quantity > 0)
			{
				final Set modifiedCartEntryPoSet = new HashSet<String>(cartEntryPoSet);
				final Long poListSize = Long.valueOf(modifiedCartEntryPoSet.size());
				final int difference = poListSize.compareTo(orderEntry.getQuantity());
				if (difference > 0)
				{
					final Set<String> differencePoSet = (Set<String>) modifiedCartEntryPoSet.stream().limit(difference)
							.collect(Collectors.toSet());
					modifiedCartEntryPoSet.removeAll(differencePoSet);
					LOG.info("Removed purchase order numbers" + differencePoSet + "from Cart Entry" + orderEntry.getEntryNumber()
							+ " with product code::" + orderEntry.getProduct().getCode());
					purchaseOrderMap.put(orderEntry.getProduct().getCode(), modifiedCartEntryPoSet);
					LOG.info("Updated purchase order number list" + modifiedCartEntryPoSet + "for Cart Entry"
							+ orderEntry.getEntryNumber() + " with product code::" + orderEntry.getProduct().getCode());
					sessionService.setAttribute(CatCoreConstants.REORDER_PO_LIST, purchaseOrderMap);
				}
			}
			else if (quantity == 0)
			{
				purchaseOrderMap.remove(orderEntry.getProduct().getCode(), cartEntryPoSet);
				sessionService.removeAttribute(CatCoreConstants.REORDER_PO_LIST);
				sessionService.setAttribute(CatCoreConstants.REORDER_PO_LIST, purchaseOrderMap);
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map getReOrderData(final Map reviewReorderMap, final List<OrderEntryData> reorderCartEntryDataList)
	{
		final Map<String, Object> reviewOrderDataMap = new HashMap<>();
		final B2BCustomerModel customerModel = catCustomerFacade.getCurrentDealer();
		final B2BUnitModel b2bUnitModel = customerModel.getDefaultB2BUnit();
		if (CollectionUtils.isNotEmpty(reorderCartEntryDataList) && null != b2bUnitModel)
		{
			setReviewOrderCartData(reviewReorderMap, reorderCartEntryDataList, reviewOrderDataMap, b2bUnitModel);
			setDealerData(reviewReorderMap, b2bUnitModel);
		}
		return reviewReorderMap;
	}

	/**
	 * @param reviewReorderMap
	 * @param b2bUnitModel
	 */
	private void setDealerData(final Map reviewReorderMap, final B2BUnitModel b2bUnitModel)
	{
		reviewReorderMap.put("dealerName", b2bUnitModel.getLocName());
		final List<AddressData> shippingAddressList = catCustomerFacade.getDealerShipTos(catCustomerFacade.getCurrentDealer(), "");

		if (CollectionUtils.isNotEmpty(shippingAddressList))
		{
			reviewReorderMap.put("shippingAddressList", shippingAddressList);
		}
	}

	/**
	 * @param reviewReorderMap
	 * @param reorderCartEntryDataList
	 * @param reviewOrderDataMap
	 * @param b2bUnitModel
	 */
	private void setReviewOrderCartData(final Map reviewReorderMap, final List<OrderEntryData> reorderCartEntryDataList,
			final Map<String, Object> reviewOrderDataMap, final B2BUnitModel b2bUnitModel)
	{
		for (final OrderEntryData oed : reorderCartEntryDataList)
		{
			final List tempList = new ArrayList();
			tempList.add(oed);
			final List reviewOrderDataObjectList = new ArrayList<>();
			reviewOrderDataObjectList.add(tempList);

			final RestockAlertModel restockAlertModel = getRestockAlert(oed.getSalesModelId(), b2bUnitModel);
			if (null != restockAlertModel)
			{
				reviewOrderDataObjectList
						.add((new AbstractMap.SimpleEntry("projectedInventory", restockAlertModel.getCurrentStockLevel())));
			}
			reviewOrderDataMap.put(oed.getSalesModelId() + "__" + oed.getEntryNumber(), reviewOrderDataObjectList);
		}
		reviewReorderMap.put("salesModelEntries", reviewOrderDataMap);
	}

	/**
	 *
	 * @param salesModelCode
	 *           - sales model
	 * @param b2bUnitModel
	 *           - b2b unit
	 * @return RestockAlertModel - restock alert model
	 */
	private RestockAlertModel getRestockAlert(final String salesModelCode, final PrincipalModel b2bUnitModel)
	{
		final String alertId = salesModelCode + "_" + b2bUnitModel.getUid();
		return catStockFacade.getStockForSalesModel(alertId);
	}

	/*
	 * This method is used to restore Saved Cart.
	 *
	 * @param guid GUID
	 *
	 * @return CartRestorationData
	 *
	 * @throws CommerceCartRestorationException
	 */
	@Override
	public CartRestorationData restoreSavedCart(final String guid) throws CommerceCartRestorationException
	{
		if (!hasEntries() && !hasEntryGroups())
		{
			getCartService().setSessionCart(null);
		}

		final CommerceCartParameter parameter = new CommerceCartParameter();
		parameter.setEnableHooks(true);
		final CartModel cartForGuidAndSiteAndUser = getCommerceCartService().getCartForGuidAndSiteAndUser(guid,
				getBaseSiteService().getCurrentBaseSite(), getUserService().getCurrentUser());
		if (cartForGuidAndSiteAndUser != null && (Boolean.parseBoolean(Config.getParameter(REMOVEUNAPPROVEDPRODUCTSWHILELOGIN))))
		{
			getCartService().setSessionCart(cartForGuidAndSiteAndUser);
			final List<AbstractOrderEntryModel> abstractOrderEntryModelList = cartForGuidAndSiteAndUser.getEntries();
			removeUnapprovedProductsFromCart(abstractOrderEntryModelList);
		}
		parameter.setCart(cartForGuidAndSiteAndUser);

		return getCartRestorationConverter().convert(getCommerceCartService().restoreCart(parameter));
	}

	/**
	 * This method is used to remove UnApproved Products From Cart on log in.
	 *
	 * @param abstractOrderEntryModelList
	 *           abstractOrderEntryModelList
	 */
	@Override
	public void removeUnapprovedProductsFromCart(final List<AbstractOrderEntryModel> abstractOrderEntryModelList)
	{
		if (CollectionUtils.isNotEmpty(abstractOrderEntryModelList))
		{
			final List<AbstractOrderEntryModel> configVariantList = abstractOrderEntryModelList.stream()
					.filter(entry -> !entry.getMandatoryProduct().booleanValue() && StringUtils.isBlank(entry.getConfigVariantId()))
					.collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(configVariantList))
			{
				removeConfigurationFromCart(configVariantList);
			}
		}
	}

	/**
	 * This method is used to remove UnApproved Products From Cart on log in.
	 *
	 * @param configVariantList
	 *           configVariantList
	 */
	private void removeConfigurationFromCart(final List<AbstractOrderEntryModel> configVariantList)
	{
		for (final AbstractOrderEntryModel configVariant : configVariantList)
		{
			if ((configVariant.getProduct().getApprovalStatus().getCode()).equalsIgnoreCase(UNAPPROVED))
			{
				try
				{
					catUpdateCartEntry(configVariant.getEntryNumber().longValue(), 0);
				}
				catch (final CommerceCartModificationException e)
				{
					LOG.error(e.getMessage(), e);
				}
			}
		}
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, Object> getCartType(final String productCode)
	{
		final Map<String, Object> cartType = new HashMap<>();
		Boolean isUTVCart = Boolean.FALSE;
		if (hasEntries())
		{
			final OrderEntryData cartEntry = getSessionCart().getEntries().iterator().next();

			final ProductData cartProduct = cartEntry.getProduct();
			final boolean cartProductIsAccessory = catProductService.isAccessoryProduct(cartProduct);
			final boolean cartProductIsUtv = catProductService.isUtvProduct(cartProduct);

			if (StringUtils.isNotBlank(productCode))
			{
				final ProductData productData = productFacade.getProductForCodeAndOptions(productCode,
						Arrays.asList(ProductOption.CATEGORIES, ProductOption.VARIANT_MATRIX_BASE, ProductOption.VARIANT_MATRIX_URL,
								ProductOption.VARIANT_MATRIX_MEDIA));
				if ((catProductService.isUtvProduct(productData) != catProductService.isUtvProduct(cartProduct))
						|| cartProductIsAccessory)
				{
					clearCart();
				}
			}
			else
			{
				isUTVCart = getUserCartType(cartProductIsUtv, cartProductIsAccessory, cartType);
			}


			final String truckId = cartEntry.getTruckloadId();
			if (StringUtils.isNotEmpty(truckId) && StringUtils.contains(truckId, CatCoreConstants.MIX_AND_MATCH_TRUCK))
			{
				cartType.put(CART_PRODUCT_TYPE, "cartProductIsUtvTruck");
			}
		}
		else
		{
			cartType.put(CART_PRODUCT_TYPE, "emptyCart");
		}
		if (isUTVCart.booleanValue())
		{
			cartType.put(CatCoreConstants.IS_UTV_CART, Boolean.TRUE);
		}
		else
		{
			cartType.put(CatCoreConstants.IS_UTV_CART, Boolean.FALSE);
		}

		return cartType;
	}

	/**
	 * @param cartProductIsUtv
	 *           - utv flag
	 * @param cartProductIsAccessory
	 *           - accessory flag
	 * @param cartType
	 *           - cart type
	 */
	private Boolean getUserCartType(final boolean cartProductIsUtv, final boolean cartProductIsAccessory,
			final Map<String, Object> cartType)
	{
		Boolean isUTVCart;
		if (cartProductIsUtv)
		{
			isUTVCart = Boolean.TRUE;
			cartType.put(CART_PRODUCT_TYPE, "cartProductIsUtv");
		}
		else if (cartProductIsAccessory)
		{
			isUTVCart = Boolean.TRUE;
			cartType.put(CART_PRODUCT_TYPE, "cartProductIsAccessory");
		}
		else
		{
			isUTVCart = Boolean.FALSE;
			cartType.put(CART_PRODUCT_TYPE, "cartProductIsBCP");
		}
		return isUTVCart;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CartData getSessionCart()
	{
		final CartData cartData;
		if (hasSessionCart())
		{
			final CartModel cart = getCartService().getSessionCart();
			removeUnapprovedProductsFromCart(cart.getEntries());
			cartData = getCartConverter().convert(cart);
		}
		else
		{
			cartData = createEmptyCart();
		}
		return cartData;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CartData getMiniCart()
	{
		final CartData cartData;
		if (hasSessionCart())
		{
			final CartModel cart = getCartService().getSessionCart();
			removeUnapprovedProductsFromCart(cart.getEntries());
			cartData = getMiniCartConverter().convert(cart);
		}
		else
		{
			cartData = createEmptyCart();
		}
		return cartData;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String redirectIfNotSP(final String userType, final String productCode, final int orderQuantity,
			final String selectedModel)
	{
		final CartData cartData = getSessionCart();
		String redirectUrl = "/cart/reOrderCart";
		if (CollectionUtils.isNotEmpty(cartData.getEntries()))
		{
			final ProductData sampleProdData = cartData.getEntries().get(0).getProduct();
			populateReviewOrderRedirectFlag(cartData, sampleProdData);

			String prodCode = productCode;
			int orderQty = orderQuantity;
			if (StringUtils.isEmpty(prodCode))
			{
				prodCode = sampleProdData.getCode();
			}

			if (orderQty == 0)
			{
				orderQty = cartData.getTotalItems().intValue();
			}

			if (catProductService.isUtvProduct(sampleProdData))
			{
				if (CatCoreConstants.UTV.equalsIgnoreCase(userType) && !truckloadRedirect)
				{
					redirectUrl = "/checkout/reviewUTV/" + prodCode + "?productCode=" + prodCode + "&orderQuantity=" + orderQty;

				}
				if (CatCoreConstants.UTV.equalsIgnoreCase(userType) && truckloadRedirect)
				{
					redirectUrl = "/truckload/reviewTruckload/?productCode=" + prodCode;
				}
			}
			redirectUrl = createURLForAccessories(selectedModel, cartData, redirectUrl);

		}
		return "redirect:" + redirectUrl;
	}

	/**
	 * This method is used to create Redirect URL for Accessories.
	 *
	 * @param selectedModel
	 *           selectedModel
	 * @param cartData
	 *           cartData
	 * @param redirectUrl
	 *           redirectUrl
	 * @return redirectUrl
	 */
	private String createURLForAccessories(final String selectedModel, final CartData cartData, final String redirectUrl)
	{
		final String redirectURL = redirectUrl;
		if (CollectionUtils.isNotEmpty(cartData.getEntries())
				&& StringUtils.isNotBlank(cartData.getEntries().get(0).getCompatibleModel()))
		{
			return StringUtils.isNotEmpty(selectedModel) ? "/accessories?selecteModel=" + selectedModel : "/accessories";
		}
		return redirectURL;
	}

	/**
	 * This method populates the review order redirect flag for utv products. It will either redirect to review truckload
	 * page or utv review order page.
	 *
	 * @param cartData
	 *           the cart data
	 * @param product
	 *           the product
	 */
	private void populateReviewOrderRedirectFlag(final CartData cartData, final ProductData product)
	{
		if (catProductService.isUtvProduct(product))
		{
			LOG.debug("UTV Products Exist adding redirect attribute for review order page ::: ");
			for (final OrderEntryData cartEntry : cartData.getEntries())
			{
				if (StringUtils.contains(cartEntry.getTruckloadId(), CatCoreConstants.MIX_AND_MATCH_TRUCK))
				{
					this.truckloadRedirect = true;
					break;
				}
				else
				{
					this.truckloadRedirect = false;
				}
			}
		}
	}


	/**
	 * {@inheritDoc}
	 *
	 *
	 */
	@Override
	public void addAccessoriesToCart(final List<CatAccessoryData> accessories) throws CommerceCartModificationException
	{
		final List<AddToCartParams> paramsList = new ArrayList<>();
		prepareParamsListForAccessories(accessories, paramsList);
		catAddToCart(paramsList);
	}


	/**
	 * Method to prepare list of add params based on accessory data
	 *
	 * @param accessories
	 *           the accessory data sent in the request
	 * @param paramsList
	 *           The list to be populated and passed to the strategies
	 */
	private void prepareParamsListForAccessories(final List<CatAccessoryData> accessories, final List<AddToCartParams> paramsList)
	{
		for (final CatAccessoryData accessory : accessories)
		{
			final AddToCartParams paramsForReference = new AddToCartParams();
			paramsForReference.setProductCode(accessory.getCode());
			paramsForReference.setQuantity(accessory.getQuantity().longValue());
			paramsForReference.setMandatoryProduct(Boolean.FALSE);
			paramsForReference.setConfigVariantId("");
			paramsForReference.setCompatibleModel(accessory.getSelectedCompatibleModel());
			paramsList.add(paramsForReference);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PurchaseOrderNumberData> getDuplicatePosForAccessories(
			final List<PurchaseOrderNumberData> purchaseOrderNumberDataList)
	{
		final List<PurchaseOrderNumberData> purchaseOrderNumberDataListError = new ArrayList();
		final List<String> purchaseOrderNumberList = new ArrayList();
		purchaseOrderNumberDataList.forEach(purchaseOrderNumberData -> {
			if (CollectionUtils.isNotEmpty(purchaseOrderNumberData.getPurchaseOrderNumberSet()))
			{
				purchaseOrderNumberList
						.addAll(purchaseOrderNumberData.getPurchaseOrderNumberSet().stream().collect(Collectors.toList()));
			}
		});
		final Set<String> duplicatePos = new HashSet();
		final Set tempSet = new HashSet();
		for (final String purchaseOrderNbr : purchaseOrderNumberList)
		{
			if (!tempSet.add(purchaseOrderNbr))
			{
				duplicatePos.add(purchaseOrderNbr.toUpperCase());
			}
		}
		if (CollectionUtils.isNotEmpty(duplicatePos))
		{
			final PurchaseOrderNumberData purchaseOrderNumberData = new PurchaseOrderNumberData();
			purchaseOrderNumberData.setIsValid(Boolean.FALSE);
			purchaseOrderNumberData.setPurchaseOrderNumberSet(duplicatePos);
			purchaseOrderNumberData.setDuplicatePONumbers(Boolean.TRUE);
			purchaseOrderNumberDataListError.add(purchaseOrderNumberData);
		}
		return purchaseOrderNumberDataListError;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PurchaseOrderNumberData> validateAndUpdatePONumbersForAccessories(
			final List<PurchaseOrderNumberData> purchaseOrderNumberDataList)
	{
		final CartModel cartModel = getCartModelForCurrentUser();
		final List<PurchaseOrderNumberData> duplicatePoList = validatePurchaseOrderNumberList(purchaseOrderNumberDataList, false);
		if (CollectionUtils.isEmpty(duplicatePoList) && cartModel != null && CollectionUtils.isNotEmpty(cartModel.getEntries()))
		{
			final Map<String, List<AbstractOrderEntryModel>> compatibleModelAbstractOrderEntryModelMap = cartModel.getEntries()
					.stream().collect(Collectors.groupingBy(AbstractOrderEntryModel::getCompatibleModel));
			savePONumberEntryForAccessories(purchaseOrderNumberDataList, cartModel, compatibleModelAbstractOrderEntryModelMap);
		}
		return duplicatePoList;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public CartModel getCartModelForCurrentUser()
	{
		CartModel cartModel = null;
		if (hasSessionCart())
		{
			cartModel = cartService.getSessionCart();
		}
		else
		{
			final List<CartModel> cartModelList = commerceCartService.getCartsForSiteAndUser(baseSiteService.getCurrentBaseSite(),
					userService.getCurrentUser());
			final Optional<CartModel> cartModelOption = cartModelList.stream().findFirst();
			if (cartModelOption.isPresent())
			{
				cartModel = cartModelOption.get();
			}
		}
		return cartModel;
	}


	/**
	 * This method is used to save Purchase Order Number for Accessories.
	 *
	 * @param purchaseOrderNumberDataList
	 *           purchase Order Number Data List
	 * @param cartModel
	 *           Cart model
	 * @param compatibleModelAbstractOrderEntryModelMap
	 *           Map of compatible models and Abstract Order Entry Model List
	 */
	private void savePONumberEntryForAccessories(final List<PurchaseOrderNumberData> purchaseOrderNumberDataList,
			final CartModel cartModel, final Map<String, List<AbstractOrderEntryModel>> compatibleModelAbstractOrderEntryModelMap)
	{
		LOG.info("compatibleModelOrderEntryDataMap - " + compatibleModelAbstractOrderEntryModelMap);
		for (final Map.Entry<String, List<AbstractOrderEntryModel>> compatibleModelAbstractOrderEntryModelMapEntry : compatibleModelAbstractOrderEntryModelMap
				.entrySet())
		{
			LOG.info(compatibleModelAbstractOrderEntryModelMapEntry.getKey());
			final List<PurchaseOrderNumberData> purchaseOrderDataEntry = purchaseOrderNumberDataList.stream()
					.filter(purchaseOrderNumberDataEntry -> purchaseOrderNumberDataEntry.getSalesModelId()
							.equalsIgnoreCase(compatibleModelAbstractOrderEntryModelMapEntry.getKey()))
					.collect(Collectors.toList());
			final PurchaseOrderNumberData purchaseOrderNumberData = purchaseOrderDataEntry.iterator().next();
			if (CollectionUtils.isNotEmpty(purchaseOrderNumberData.getPurchaseOrderNumberSet()))
			{
				final List<AbstractOrderEntryModel> abstractOrderEntryModelList = compatibleModelAbstractOrderEntryModelMap
						.get(compatibleModelAbstractOrderEntryModelMapEntry.getKey());
				for (final AbstractOrderEntryModel abstractOrderEntryModel : abstractOrderEntryModelList)
				{
					abstractOrderEntryModel.setAccessoriesPONumber(
							(purchaseOrderNumberData.getPurchaseOrderNumberSet().iterator().next()).toUpperCase());
					modelService.save(abstractOrderEntryModel);
				}
			}
		}
		modelService.save(cartModel);
		modelService.refresh(cartModel);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Boolean isAccessoriesCart(final CartData cartData)
	{
		if (hasEntries())
		{
			final List<OrderEntryData> cartEntries = getSessionCart().getEntries();
			for (final OrderEntryData cartEntry : cartEntries)
			{
				final ProductData cartProduct = cartEntry.getProduct();
				if (!catProductService.isAccessoryProduct(cartProduct))
				{
					return Boolean.FALSE;
				}
			}
			return Boolean.TRUE;
		}
		return Boolean.TRUE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<LaneTypeEnum, String> getLaneValidationMap()
	{
		final Map<LaneTypeEnum, String> laneValidationMap = new HashMap<>();
		laneValidationMap.put(LaneTypeEnum.LANE1, UNAPPROVED + "," + RESTOCK + "," + REORDERABLE);
		laneValidationMap.put(LaneTypeEnum.LANE2,
				ORDER_WINDOW + "," + UNAPPROVED + "," + RESTOCK + "," + REORDERABLE + "," + COMPATIBLE_CHECK + "," + STOCK_CHECK);
		laneValidationMap.put(LaneTypeEnum.LANE3,
				ORDER_WINDOW + "," + UNAPPROVED + "," + RESTOCK + "," + REORDERABLE + "," + COMPATIBLE_CHECK + "," + STOCK_CHECK);
		return laneValidationMap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CatCartDataErrorMap getCartValidationMap(final Map<LaneTypeEnum, String> laneValidationMap)
	{
		final CatCartDataErrorMap errorMap = new CatCartDataErrorMap();
		final Map<String, List<OrderEntryData>> errorEntryMap = new HashMap();
		if (hasSessionCart())
		{
			final CartModel cartModel = getCartService().getSessionCart();

			final LaneTypeEnum laneTypeEnumForCart = cartModel.getLaneType();
			if (null != laneTypeEnumForCart)
			{
				errorMap.setLaneType(laneTypeEnumForCart);
				checkPossibleValidationScenarios(laneValidationMap, errorEntryMap, cartModel, laneTypeEnumForCart);
				errorMap.setErrorEntryMap(errorEntryMap);
			}
		}
		return errorMap;
	}

	/**
	 * Check possible validation scenarios.
	 *
	 * @param laneValidationMap
	 *           the lane validation map
	 * @param errorEntryMap
	 *           the error entry map
	 * @param cartModel
	 *           the cart model
	 * @param laneTypeEnumForCart
	 *           the lane type enum for cart
	 */
	private void checkPossibleValidationScenarios(final Map<LaneTypeEnum, String> laneValidationMap,
			final Map<String, List<OrderEntryData>> errorEntryMap, final CartModel cartModel, final LaneTypeEnum laneTypeEnumForCart)
	{
		try
		{
			String validationScenarios = laneValidationMap.get(laneTypeEnumForCart);
			final List<AbstractOrderEntryModel> abstractOrderEntryList = cartService.getConfigVariantEntriesInCurrentCart(cartModel);
			validationScenarios = skipScenariosIfCartModelEmpty(abstractOrderEntryList, validationScenarios);

			for (final String scenario : validationScenarios.split(","))
			{
				switch (scenario)
				{
					case ORDER_WINDOW:
						orderWindowScenario(errorEntryMap, laneTypeEnumForCart);
						break;
					case UNAPPROVED:
						unapprovedScenario(errorEntryMap, laneTypeEnumForCart, abstractOrderEntryList);
						break;
					case REORDERABLE:
						reorderableScenario(errorEntryMap, laneTypeEnumForCart, abstractOrderEntryList);
						break;
					case COMPATIBLE_CHECK:
						compatibleScenario(errorEntryMap, laneTypeEnumForCart, abstractOrderEntryList);
						break;
					case STOCK_CHECK:
						stockScenario(errorEntryMap, laneTypeEnumForCart, abstractOrderEntryList);
						break;
					case RESTOCK_CHECK:
						restockAlertScenario(errorEntryMap, abstractOrderEntryList);
						break;
					default:
						break;
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage(), e);
		}
	}

	/**
	 * Restock alert scenario.
	 *
	 * @param errorEntryMap
	 *           the error entry map
	 * @param abstractOrderEntryList
	 *           the abstract order entry list
	 */
	private void restockAlertScenario(final Map<String, List<OrderEntryData>> errorEntryMap,
			final List<AbstractOrderEntryModel> abstractOrderEntryList)
	{
		final List<AbstractOrderEntryModel> configForWhichRestockAlertIsNotPresent = getConfigForWhichRestockAlertNotPresent(
				abstractOrderEntryList);
		if (CollectionUtils.isNotEmpty(configForWhichRestockAlertIsNotPresent))
		{
			errorEntryMap.put(STOCK_CHECK, orderEntryConverter.convertAll(configForWhichRestockAlertIsNotPresent));
		}
	}

	/**
	 * Stock scenario.
	 *
	 * @param errorEntryMap
	 *           the error entry map
	 * @param laneTypeEnumForCart
	 *           the lane type enum for cart
	 * @param abstractOrderEntryList
	 *           the abstract order entry list
	 */
	private void stockScenario(final Map<String, List<OrderEntryData>> errorEntryMap, final LaneTypeEnum laneTypeEnumForCart,
			final List<AbstractOrderEntryModel> abstractOrderEntryList)
	{
		final List<AbstractOrderEntryModel> configForWhichStockIsExceeded = getConfigForWhichStockIsExceeded(laneTypeEnumForCart,
				abstractOrderEntryList);
		if (CollectionUtils.isNotEmpty(configForWhichStockIsExceeded))
		{
			errorEntryMap.put(STOCK_CHECK, orderEntryConverter.convertAll(configForWhichStockIsExceeded));
		}
	}

	/**
	 * Compatible scenario.
	 *
	 * @param errorEntryMap
	 *           the error entry map
	 * @param laneTypeEnumForCart
	 *           the lane type enum for cart
	 * @param abstractOrderEntryList
	 *           the abstract order entry list
	 */
	private void compatibleScenario(final Map<String, List<OrderEntryData>> errorEntryMap, final LaneTypeEnum laneTypeEnumForCart,
			final List<AbstractOrderEntryModel> abstractOrderEntryList)
	{
		final List<AbstractOrderEntryModel> nonCompatibleConfigList = getNonCompatibleConfigVariants(laneTypeEnumForCart,
				abstractOrderEntryList);
		if (CollectionUtils.isNotEmpty(nonCompatibleConfigList))
		{
			errorEntryMap.put(COMPATIBLE_CHECK, orderEntryConverter.convertAll(nonCompatibleConfigList));
		}
	}

	/**
	 * Reorderable scenario.
	 *
	 * @param errorEntryMap
	 *           the error entry map
	 * @param laneTypeEnumForCart
	 *           the lane type enum for cart
	 * @param abstractOrderEntryList
	 *           the abstract order entry list
	 */
	private void reorderableScenario(final Map<String, List<OrderEntryData>> errorEntryMap, final LaneTypeEnum laneTypeEnumForCart,
			final List<AbstractOrderEntryModel> abstractOrderEntryList)
	{
		final List<AbstractOrderEntryModel> nonReorderableConfigVariants = getNonReorderableConfigVariants(abstractOrderEntryList);
		if (CollectionUtils.isNotEmpty(nonReorderableConfigVariants))
		{
			errorEntryMap.put(REORDERABLE_CHECK, orderEntryConverter.convertAll(nonReorderableConfigVariants));
		}
	}

	/**
	 * Unapproved scenario.
	 *
	 * @param errorEntryMap
	 *           the error entry map
	 * @param laneTypeEnumForCart
	 *           the lane type enum for cart
	 * @param abstractOrderEntryList
	 *           the abstract order entry list
	 */
	private void unapprovedScenario(final Map<String, List<OrderEntryData>> errorEntryMap, final LaneTypeEnum laneTypeEnumForCart,
			final List<AbstractOrderEntryModel> abstractOrderEntryList)
	{
		final List<AbstractOrderEntryModel> unapprovedConfigVariants = getUnapprovedConfigVariantProducts(laneTypeEnumForCart,
				abstractOrderEntryList);
		if (CollectionUtils.isNotEmpty(unapprovedConfigVariants))
		{
			errorEntryMap.put(UNAPPROVED_CHECK, orderEntryConverter.convertAll(unapprovedConfigVariants));
		}
	}

	/**
	 * Order window scenario.
	 *
	 * @param errorEntryMap
	 *           the error entry map
	 * @param laneTypeEnumForCart
	 *           the lane type enum for cart
	 * @throws ParseException
	 *            the parse exception
	 */
	private void orderWindowScenario(final Map<String, List<OrderEntryData>> errorEntryMap, final LaneTypeEnum laneTypeEnumForCart)
			throws ParseException
	{
		final boolean isProductOrderableInOrderWindow = catOrderLaneProductService
				.isProductOrderableForLane(laneTypeEnumForCart.getCode(), null, null, true, false, false, false);
		if (BooleanUtils.isFalse(Boolean.valueOf(isProductOrderableInOrderWindow)))
		{
			errorEntryMap.put(ORDER_WINDOW, new ArrayList<>());
		}
	}

	/**
	 * Gets the config for which restock alert not present.
	 *
	 * @param abstractOrderEntryList
	 *           the abstract order entry list
	 * @return the config for which restock alert not present
	 */
	private List<AbstractOrderEntryModel> getConfigForWhichRestockAlertNotPresent(
			final List<AbstractOrderEntryModel> abstractOrderEntryList)
	{
		final List<AbstractOrderEntryModel> nonLowStockProducts = abstractOrderEntryList.stream()
				.filter(abstractEntry -> BooleanUtils.isFalse(isProductPresentInRestockAlert(abstractEntry)))
				.collect(Collectors.toList());
		return nonLowStockProducts;
	}


	/**
	 * Checks if is product present in restock alert.
	 *
	 * @param abstractEntry
	 *           the abstract entry
	 * @return the boolean
	 */
	private Boolean isProductPresentInRestockAlert(final AbstractOrderEntryModel abstractEntry)
	{
		final List<RestockAlertModel> restockAlert = catReorderFacade.getLowStockProducts(
				((B2BCustomerModel) getUserService().getCurrentUser()).getDefaultB2BUnit(), null,
				getSalesModel(abstractEntry.getProduct()).getCode());
		return Boolean.valueOf(restockAlert.stream().anyMatch(
				restock -> catReorderFacade.getAssociatedConfigProducts(restock.getAlertId()).contains(abstractEntry.getProduct())));
	}

	/**
	 * Gets the non reorderable config variants.
	 *
	 * @param abstractOrderEntryList
	 *           the abstract order entry list
	 * @return the non reorderable config variants
	 */
	private List<AbstractOrderEntryModel> getNonReorderableConfigVariants(
			final List<AbstractOrderEntryModel> abstractOrderEntryList)
	{
		final List<AbstractOrderEntryModel> nonReorderableConfigVariants = abstractOrderEntryList.stream()
				.filter(abstractEntry -> BooleanUtils.isFalse(isProductReorderable(abstractEntry))).collect(Collectors.toList());
		return nonReorderableConfigVariants;
	}

	/**
	 * Checks if is product reorderable.
	 *
	 * @param abstractEntry
	 *           the abstract entry
	 * @return the boolean
	 */
	private Boolean isProductReorderable(final AbstractOrderEntryModel abstractEntry)
	{
		final ConfigVariantProductModel configVariant = (ConfigVariantProductModel) abstractEntry.getProduct();
		final BaseVariantProductModel baseVariantProductModel = (BaseVariantProductModel) configVariant.getBaseProduct();
		if (BooleanUtils.isFalse(baseVariantProductModel.getIsReorderable()))
		{
			return Boolean.FALSE;
		}
		final ProductModel salesProductModel = baseVariantProductModel.getBaseProduct();
		if (BooleanUtils.isFalse(salesProductModel.getIsReorderable()))
		{
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}


	/**
	 * Skip scenarios if cart model empty.
	 *
	 * @param abstractOrderEntryList
	 *           the abstract order entry list
	 * @param validationScenarios
	 *           the validation scenarios
	 * @return the string
	 */
	private String skipScenariosIfCartModelEmpty(final List<AbstractOrderEntryModel> abstractOrderEntryList,
			final String validationScenarios)
	{
		if (CollectionUtils.isEmpty(abstractOrderEntryList) && validationScenarios.contains(ORDER_WINDOW))
		{
			return validationScenarios
					.replace(validationScenarios.substring(validationScenarios.indexOf(','), validationScenarios.length() - 1), "");
		}
		return validationScenarios;
	}




	/**
	 * Gets the unapproved config variant products.
	 *
	 * @param laneTypeEnumForCart
	 *           the lane type enum for cart
	 * @param abstractOrderEntryList
	 *           the abstract order entry list
	 * @return the unapproved config variant products
	 */
	private List<AbstractOrderEntryModel> getUnapprovedConfigVariantProducts(final LaneTypeEnum laneTypeEnumForCart,
			final List<AbstractOrderEntryModel> abstractOrderEntryList)
	{
		final List<AbstractOrderEntryModel> unapprovedConfigList = abstractOrderEntryList.stream()
				.filter(abstractEntry -> BooleanUtils.isFalse(isProductApproved(abstractEntry))).collect(Collectors.toList());
		return unapprovedConfigList;

	}

	/**
	 * Checks if is product approved.
	 *
	 * @param abstractEntry
	 *           the abstract entry
	 * @return the boolean
	 */
	private Boolean isProductApproved(final AbstractOrderEntryModel abstractEntry)
	{
		final ConfigVariantProductModel configVariant = (ConfigVariantProductModel) abstractEntry.getProduct();
		if (BooleanUtils.isFalse(Boolean.valueOf(configVariant.getApprovalStatus().equals(ArticleApprovalStatus.APPROVED))))
		{
			return Boolean.FALSE;
		}
		final BaseVariantProductModel baseVariantProductModel = (BaseVariantProductModel) configVariant.getBaseProduct();
		if (BooleanUtils
				.isFalse(Boolean.valueOf(baseVariantProductModel.getApprovalStatus().equals(ArticleApprovalStatus.APPROVED))))
		{
			return Boolean.FALSE;
		}
		final ProductModel salesProductModel = baseVariantProductModel.getBaseProduct();
		if (BooleanUtils.isFalse(Boolean.valueOf(salesProductModel.getApprovalStatus().equals(ArticleApprovalStatus.APPROVED))))
		{
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clearAndRecalculateCart()
	{
		final CartModel cartModel = getCartService().getSessionCart();
		clearCart();
		cartModel.setLaneType(null);
		saveAndReCalculateCart(cartModel);
	}

	/**
	 * Gets the config for which stock is exceeded.
	 *
	 * @param laneTypeEnumForCart
	 *           the lane type enum for cart
	 * @param abstractOrderEntryList
	 *           the abstract order entry list
	 * @return boolean
	 */
	private List<AbstractOrderEntryModel> getConfigForWhichStockIsExceeded(final LaneTypeEnum laneTypeEnumForCart,
			final List<AbstractOrderEntryModel> abstractOrderEntryList)
	{
		final List<AbstractOrderEntryModel> stockExceededForSalesModel = new ArrayList<>();
		final Map<String, List<AbstractOrderEntryModel>> combinedSalesModelMap = abstractOrderEntryList.stream()
				.collect(Collectors.groupingBy(abstractOrderEntry -> getSalesModel(abstractOrderEntry.getProduct()).getCode()));
		for (final Entry<String, List<AbstractOrderEntryModel>> entry : combinedSalesModelMap.entrySet())
		{
			final long orderedQty = entry.getValue().stream().mapToLong(entryValue -> entryValue.getQuantity().longValue()).sum();
			final List<ProductLaneInfoModel> productLaneInfoList = catOrderLaneProductService.getLaneInfoForOrderedQuantity(
					entry.getKey(), ((B2BCustomerModel) getUserService().getCurrentUser()).getDefaultB2BUnit(), (int) orderedQty);

			final Optional<ProductLaneInfoModel> productInfoModel = productLaneInfoList.stream()
					.filter(productLaneInfo -> productLaneInfo.getLaneInfo().equals(laneTypeEnumForCart)).findFirst();
			if (!productInfoModel.isPresent())
			{
				stockExceededForSalesModel.addAll(entry.getValue());
			}
		}
		return stockExceededForSalesModel;
	}

	/**
	 * Gets the non compatible config variants.
	 *
	 * @param laneTypeEnumForCart
	 *           the lane type enum for cart
	 * @param abstractOrderEntryList
	 *           the abstract order entry list
	 * @return the non compatible config variants
	 */
	private List<AbstractOrderEntryModel> getNonCompatibleConfigVariants(final LaneTypeEnum laneTypeEnumForCart,
			final List<AbstractOrderEntryModel> abstractOrderEntryList)
	{
		final List<AbstractOrderEntryModel> nonCompatibleList = abstractOrderEntryList.stream().filter(abstractEntry -> {
			try
			{
				return BooleanUtils
						.isFalse(Boolean.valueOf(catOrderLaneProductService.isProductOrderableForLane(laneTypeEnumForCart.getCode(),
								null, abstractEntry.getProduct().getCode(), false, false, true, false)));
			}
			catch (final ParseException e)
			{
				LOG.error(e.getMessage(), e);
			}
			return false;
		}).collect(Collectors.toList());

		return nonCompatibleList;
	}


	/**
	 * Gets the sales model.
	 *
	 * @param product
	 *           the product
	 * @return the sales model
	 */
	private ProductModel getSalesModel(final ProductModel product)
	{
		final BaseVariantProductModel baseVariantProduct = (BaseVariantProductModel) ((ConfigVariantProductModel) product)
				.getBaseProduct();
		return baseVariantProduct.getBaseProduct();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCartLaneTypeOnCart(final String laneType)
	{
		final CartModel cartModel = cartService.getSessionCart();
		if (StringUtils.isNotEmpty(laneType))
		{
			cartModel.setLaneType(LaneTypeEnum.valueOf(laneType));
			modelService.save(cartModel);
			modelService.refresh(cartModel);
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param entryNumber
	 */
	@Override
	public void updateCartForConfigurableProducts(final String productCode, final long qty, final Integer entryNumber)
	{
		cartService.updateCartForConfigurableProducts(productCode, qty, entryNumber);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CartModificationData addToCart(final String productCodePost, final long qty, final boolean configurable,
			final String laneType) throws CommerceCartModificationException
	{
		final AddToCartParams params = new AddToCartParams();
		params.setProductCode(productCodePost);
		params.setQuantity(qty);
		params.setConfigurable(Boolean.valueOf(configurable));
		params.setLaneType(LaneTypeEnum.valueOf(laneType));
		params.setMandatoryProduct(Boolean.FALSE);
		params.setConfigVariantId(StringUtils.EMPTY);
		return addToCart(params);

	}
}
