/**
 *
 */
package com.cat.bcp.facades.order.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.order.impl.DefaultCheckoutFacade;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.OrderService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.exceptions.BusinessException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.util.Config;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.enums.LaneTypeEnum;
import com.cat.bcp.core.event.CatOrderConfirmationEmailEvent;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.RestockAlertModel;
import com.cat.bcp.core.model.ShipToAfterDateModel;
import com.cat.bcp.core.model.TruckloadModel;
import com.cat.bcp.core.order.CatCommerceCheckoutService;
import com.cat.bcp.core.servicelayer.CatCartService;
import com.cat.bcp.core.servicelayer.CatOrderLaneProductService;
import com.cat.bcp.core.servicelayer.CatProductService;
import com.cat.bcp.core.servicelayer.CatStockService;
import com.cat.bcp.facade.product.CatStockFacade;
import com.cat.bcp.facades.address.CatReviewOrderData;
import com.cat.bcp.facades.customer.CatCustomerFacade;
import com.cat.bcp.facades.order.CatCartFacade;
import com.cat.bcp.facades.order.CatCheckoutFacade;
import com.cat.bcp.facades.order.TruckloadData;
import com.cat.core.integration.snop.response.CatSnopResponseMapper;
import com.cat.core.integration.snop.response.CatSnopSubResponse;
import com.cat.facades.order.CATOrderReviewData;
import com.cat.facades.order.PurchaseOrderCheck;



/**
 * The Class DefaultCatDealerCheckoutFacade.
 */
/**
 * @author manjam
 *
 */
public class CatCheckoutFacadeImpl extends DefaultCheckoutFacade implements CatCheckoutFacade
{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultCheckoutFacade.class);

	private static final String WHITE_SPACE = " ";
	private static final String HYPHEN = "-";
	private static final String QTY_GREATER_THAN_TRUCKLOAD = "GREATER";
	private static final String QTY_LESSER_THAN_TRUCKLOAD = "LESSER";
	private static final String TRUCKLOAD_ID = "truckloadId";
	private static final String TRUCKLOAD_IMG_URL = "truckloadImageUrl";
	private static final String SHIP_AFTER_DATE = "shipAfterDate";
	private static final String PRODUCT_LIST = "productList";
	private static final String TRUCKLOAD_PRICE = "truckload_price";
	private static final String TOTAL_TRUCK_QTY = "totalQuantity";
	private static final String TOTAL_CART_QTY = "totalCartQuantity";
	private static final String PROD_QTY_MAP = "prodQtyMap";
	private static final String ERROR_MSG = "errorMessage";
	private static final String CAT_BCP_ORDER_CONFIRMATION_TEMPLATE = "CatBCPOrderConfirmationEmailTemplate";

	private static final String ORDER_BOM_SW = "ORDER_BOM_SW";
	private static final String ORDER_LMT_SW = "ORDER_LMT_SW";
	private static final String ORDER_LIMIT = "ORDER_LIMIT";
	private static final String ORDER_LIMIT_USED = "ORDER_LIMIT_USED";
	private static final String SUCCESS = "SUCCESS";
	private static final String PRODUCT_CODE = "PRODUCT_CODE";
	private static final String INVALID_DATE = "INVALID_DATE";
	private static final String IS_RESPONSE_NULL = "isResponseNull";
	private static final String IS_CONNECTION_ERROR = "isConnectionError";
	private static final String CONNECTION_ERROR = "CONNECTION ERROR";



	@Resource(name = "eventService")
	private EventService eventService;

	/** The cat customer facade. */
	@Resource(name = "catCustomerFacade")
	private CatCustomerFacade catCustomerFacade;

	/** The address converter. */
	@Resource(name = "addressConverter")
	private Converter<AddressModel, AddressData> addressConverter;

	/** The address reverse converter. */
	@Resource(name = "addressReverseConverter")
	private Converter<AddressData, AddressModel> addressReverseConverter;

	/** The cart facade. */
	@Resource(name = "cartFacade")
	private CatCartFacade cartFacade;

	/** The model service. */
	@Resource(name = "modelService")
	private ModelService modelService;

	/** The cart service. */
	@Resource(name = "cartService")
	private CatCartService cartService;

	/** The order service. */
	@Resource(name = "orderService")
	private OrderService orderService;

	/** The i 18 N facade. */
	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	/** The product facade. */
	@Resource(name = "productFacade")
	private ProductFacade productFacade;

	/** The product service. */
	@Resource(name = "productService")
	private CatProductService productService;

	@Resource(name = "catStockFacade")
	private CatStockFacade catStockFacade;

	@Resource(name = "calculationService")
	private CalculationService calculationService;

	@Resource(name = "checkoutService")
	private CatCommerceCheckoutService checkoutService;

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	@Resource(name = "catalogVersionService")
	private CatalogVersionService catalogVersionService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "customerAccountService")
	private CustomerAccountService customerAccountService;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Autowired
	private Converter<OrderModel, OrderData> orderConverter;

	@Autowired
	private CatStockService catStockService;

	@Resource(name = "commerceCategoryService")
	private CommerceCategoryService commerceCategoryService;

	@Resource(name = "productConverter")
	private Converter<ProductModel, ProductData> productConverter;

	@Resource(name = "catTruckloadConverter")
	private Converter<TruckloadModel, TruckloadData> catTruckloadConverter;

	@Resource(name = "catOrderLaneProductService")
	private CatOrderLaneProductService catOrderLaneProductService;

	/** The extra options. */
	final List<ProductOption> extraOptions = Arrays.asList(ProductOption.VARIANT_MATRIX_BASE, ProductOption.VARIANT_MATRIX_URL,
			ProductOption.VARIANT_MATRIX_MEDIA, ProductOption.CATEGORIES);


	/**
	 * {@inheritDoc}
	 */
	@Override
	public CatReviewOrderData getReviewOrderData(final String productCode, final String salesModelCode,
			final int reOrderProductQuantity)
	{
		final CatReviewOrderData reviewOrderData = new CatReviewOrderData();
		final B2BCustomerModel customerModel = catCustomerFacade.getCurrentDealer();
		final B2BUnitModel b2bUnitModel = customerModel.getDefaultB2BUnit();
		if (CollectionUtils.isNotEmpty(b2bUnitModel.getBillingAddresses()))
		{
			final AddressData billingAddress = addressConverter.convert(b2bUnitModel.getBillingAddresses().iterator().next());
			reviewOrderData.setBillingAddress(billingAddress);
		}
		reviewOrderData.setDealerName(b2bUnitModel.getLocName());

		final ProductModel productModel = productService.getProductForCode(productCode);
		if (null != productModel)
		{
			reviewOrderData.setConfigModelCode(productModel.getCode());
			reviewOrderData.setConfigName(productModel.getName());
			reviewOrderData.setOrderedQty(Integer.valueOf(reOrderProductQuantity));
		}

		final ProductData productData = productFacade.getProductForCodeAndOptions(productCode, extraOptions);
		final ProductData baseProductData = productFacade.getProductForCodeAndOptions(productData.getBaseProduct(), extraOptions);
		reviewOrderData.setSalesModelName(baseProductData.getBaseProduct());
		final boolean isUtvProduct = checkUTVProduct(productData);
		if (productModel instanceof ConfigVariantProductModel && !isUtvProduct)
		{
			final String alertId = salesModelCode + "_" + b2bUnitModel.getUid();
			final RestockAlertModel restockAlert = catStockFacade.getStockForSalesModel(alertId);
			if (null != restockAlert)

			{
				reviewOrderData.setSalesModelCode(salesModelCode);
				reviewOrderData.setSalesModelName(restockAlert.getProduct().getName());
				reviewOrderData.setCurrentStock(restockAlert.getCurrentStockLevel());
			}
		}

		if (isUtvProduct)
		{
			reviewOrderData.setShipAfterQuantities(catStockService.getShipToDateQty(cartFacade.getSessionCart().getEntries()));
		}
		reviewOrderData.setOrderedQty(Integer.valueOf(reOrderProductQuantity));
		reviewOrderData.setConfigModelCode(productData.getCode());
		reviewOrderData.setConfigName(productData.getName());

		return reviewOrderData;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean checkUTVProduct(final ProductData productData)
	{
		final Collection<CategoryData> categoryList = productData.getCategories();
		String category = StringUtils.EMPTY;
		if (CollectionUtils.isNotEmpty(categoryList) && categoryList.iterator().next() != null)
		{
			category = categoryList.iterator().next().getName();
		}
		return productService.isUtvProduct(category);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addToCart(final String productCode, final int reOrderProductQuantity)
	{
		try
		{
			cartFacade.clearCart();
			cartFacade.addToCart(productCode, reOrderProductQuantity);
		}
		catch (final CommerceCartModificationException e)
		{
			LOG.error(" Reorder Add to Cart failed: ", e);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OrderData placeCatOrder(final CATOrderReviewData reviewOrderData) throws BusinessException
	{
		if (reviewOrderData.getVariantConfigProductCode() != null)
		{
			final ProductData productData = productFacade.getProductForCodeAndOptions(reviewOrderData.getVariantConfigProductCode(),
					extraOptions);
			final boolean isUtvProduct = checkUTVProduct(productData);
			if (null != reviewOrderData.getOrderedQty() && !isUtvProduct)
			{
				checkoutService.updateCartQuantity(Long.valueOf(reviewOrderData.getOrderedQty().longValue()));
			}
		}
		final Map<String, List<String>> poMap = sessionService.getAttribute(CatCoreConstants.REORDER_PO_LIST);
		OrderModel order = null;
		if (CollectionUtils.isEmpty(reviewOrderData.getShippingAddressList()))
		{
			order = checkoutService.placeCatOrder(reviewOrderData.getComments(), reviewOrderData.getPurchaseOrder(),
					reviewOrderData.getSelectedShippingAddressPK());
		}
		else
		{
			order = checkoutService.placeTruckloadOrder(reviewOrderData);
		}
		order.setOrderGroupId(order.getCode());

		order.setPoNumberMap(poMap);
		modelService.save(order);

		return orderConverter.convert(order);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PurchaseOrderCheck checkPOList(final List<String> purchaseOrderNumberList)
	{
		final B2BCustomerModel customerModel = catCustomerFacade.getCurrentDealer();
		return cartService.doPOListCheck(purchaseOrderNumberList, customerModel);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AddressData> getDealerShippingAddress(final Collection<AddressModel> addressModels)
	{
		return addressConverter.convertAll(addressModels);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void splitOrderByPO(final String orderCode, final List<String> purchaseOrderNumberList)
	{
		LOG.debug(" Entered Split Order Facade ");
		checkoutService.splitOrderByPO(orderCode, purchaseOrderNumberList);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void splitOrderForAccessories(final String orderCode)
	{
		LOG.debug(" Entered Split Order Facade For Accessories");
		checkoutService.splitOrderForAccessories(orderCode);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OrderData placeCatReOrder(final String comments, final List<String> poNumbersList,
			final String selectedShippingAddressPK)
	{
		OrderModel order = null;
		try
		{
			final Map<String, List<String>> poMap = sessionService.getAttribute(CatCoreConstants.REORDER_PO_LIST);
			order = checkoutService.placeCatOrder(comments, poNumbersList, selectedShippingAddressPK);
			order.setPoNumberMap(poMap);
			modelService.save(order);
			updateReOrderedQuantityForProductLaneInfo(order);
		}
		catch (final BusinessException e)
		{
			LOG.error(" Reorder submit order failed: ", e);
		}
		return orderConverter.convert(order);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OrderData placeCatOrderForAccessories(final String comments, final String selectedShippingAddressPK)
	{
		OrderModel order = null;
		try
		{
			order = checkoutService.placeCatOrderForAccessories(comments, selectedShippingAddressPK);
		}
		catch (final BusinessException e)
		{
			LOG.error(" Reorder submit order failed: ", e);
		}
		return orderConverter.convert(order);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateReOrderedQuantityForProductLaneInfo(final OrderModel parentOrder)
	{
		catOrderLaneProductService.updateReOrderedQuantityForProductLaneInfo(parentOrder);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void sendCatOrderConfirmationEmail(final String orderCode)
	{
		final OrderModel orderModel = customerAccountService.getOrderForCode(orderCode, baseStoreService.getCurrentBaseStore());
		eventService.publishEvent(new CatOrderConfirmationEmailEvent(orderModel, CAT_BCP_ORDER_CONFIRMATION_TEMPLATE, false));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createAdditionalInfo(final String orderCode, final String productCode)
	{
		final OrderModel localOrder = customerAccountService.getOrderForCode(orderCode, baseStoreService.getCurrentBaseStore());
		checkoutService.createAdditionalInfo(localOrder, productCode);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductData> getConfigProductsFromCategory(final String categoryCode)
	{
		final CategoryModel categoryModel = commerceCategoryService.getCategoryForCode(categoryCode);

		final List<ProductModel> productList = productService.getProductsForCategory(categoryModel);
		final List<ProductModel> configProductList = new ArrayList();
		for (final ProductModel product : productList)
		{
			if (product instanceof ConfigVariantProductModel)
			{
				configProductList.add(product);
			}
		}
		return productConverter.convertAll(configProductList);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<TruckloadData> getTruckloadCombinationList()
	{
		final List<TruckloadModel> truckloadModels = checkoutService.getTruckloadCombinations();
		if (CollectionUtils.isNotEmpty(truckloadModels))
		{
			return catTruckloadConverter.convertAll(truckloadModels);
		}
		return Collections.emptyList();
	}

	/**
	 * {@inheritDoc}
	 *
	 */
	@Override
	public boolean validateTruckload(final int[] twoSeaterQtyArray, final int[] fiveSeaterQtyArray)
	{
		final int twoSeaterQty = calculateTwoSeaters(twoSeaterQtyArray);
		final int fiveSeaterQty = calculateFiveSeaters(fiveSeaterQtyArray);
		return checkoutService.validateTruckload(twoSeaterQty, fiveSeaterQty);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<Date, String> getListOfFormattedShipToDates(final List<ShipToAfterDateModel> shipToDates)
	{
		final Map<Date, String> formattedDateString = new HashMap();

		for (final ShipToAfterDateModel shipAfterModel : shipToDates)
		{
			final Date shipFromDate = shipAfterModel.getShipFromDate();
			final Date shipToDate = shipAfterModel.getShipToDate();

			final int fromDatePart = shipFromDate.getDate();
			final int fromMonthPart = shipFromDate.getMonth();
			StringBuilder formattedDate = formatShipToDates(fromDatePart, fromMonthPart, new StringBuilder());
			formattedDate.append(WHITE_SPACE).append(HYPHEN).append(WHITE_SPACE);

			final int toDatePart = shipToDate.getDate();
			final int toMonthPart = shipToDate.getMonth();
			formattedDate = formatShipToDates(toDatePart, toMonthPart, formattedDate);
			formattedDateString.put(shipAfterModel.getShipAfterDate(), formattedDate.toString());

		}
		return new TreeMap<>(formattedDateString);
	}

	public StringBuilder formatShipToDates(final int date, final int month, final StringBuilder formattedDate)
	{
		final String monthName = getMonthName(month);
		if (date % 10 == 0 || (date > 10 && date < 20))
		{
			formattedDate.append(date).append("th");
		}
		else if (date % 10 == 1)
		{
			formattedDate.append(date).append("st");
		}
		else if (date % 10 == 2)
		{
			formattedDate.append(date).append("nd");
		}
		else if (date % 10 == 3)
		{
			formattedDate.append(date).append("rd");
		}
		else if (date % 10 > 3)
		{
			formattedDate.append(date).append("th");
		}

		formattedDate.append(WHITE_SPACE).append(monthName);
		return formattedDate;
	}

	public String getMonthName(final int month)
	{
		String monthName = "";
		final DateFormatSymbols dfs = new DateFormatSymbols();
		final String[] months = dfs.getMonths();
		if (month >= 0 && month <= 11)
		{
			monthName = months[month];
		}
		return monthName;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<TruckloadData> populateTruckloadSuggestions(final int[] twoSeaterQtyArray, final int[] fiveSeaterQtyArray)
	{
		final int twoSeaterQty = calculateTwoSeaters(twoSeaterQtyArray);
		final int fiveSeaterQty = calculateFiveSeaters(fiveSeaterQtyArray);
		return catTruckloadConverter.convertAll(checkoutService.populateTruckloadSuggestions(twoSeaterQty, fiveSeaterQty));
	}

	/**
	 * {@inheritDoc}
	 *
	 */
	@Override
	public String getTruckloadImage(final int[] twoSeaterQtyArray, final int[] fiveSeaterQtyArray)
	{
		final int twoSeaterQty = calculateTwoSeaters(twoSeaterQtyArray);
		final int fiveSeaterQty = calculateFiveSeaters(fiveSeaterQtyArray);
		MediaModel mediaModel = modelService.create(MediaModel.class);
		mediaModel.setCode("truckload_" + twoSeaterQty + "_" + fiveSeaterQty);
		mediaModel.setCatalogVersion(getCatalogVersion());
		try
		{
			mediaModel = flexibleSearchService.getModelByExample(mediaModel);
			if (null != mediaModel)
			{
				return mediaModel.getURL();
			}
			return StringUtils.EMPTY;
		}
		catch (final Exception e)
		{
			LOG.error("Error while retrieving truckload image", e);
			return StringUtils.EMPTY;
		}
	}

	/**
	 * This method gets the Catalog version
	 */
	private CatalogVersionModel getCatalogVersion()
	{
		return catalogVersionService.getCatalogVersion(Config.getParameter(CatCoreConstants.CAT_CONTENT_CATALOG),
				Config.getParameter(CatCoreConstants.CAT_CONTENT_CATALOG_VERSION));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeTruckload(final String truckloadId) throws CalculationException
	{
		checkoutService.removeTruckload(truckloadId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<Object, Object> retainTruckloads(final CartData cartData)
	{
		final Map<Object, Object> truckloadMap = new LinkedHashMap();

		Map<String, Object> prodQtyMap = new LinkedHashMap();
		Map<String, Object> tempProdQtyMap = new LinkedHashMap();

		int truckCount = 0;
		int cartEntries = 0;
		double truckloadPrice = 0.0d;

		String truckloadId = StringUtils.EMPTY;

		for (final OrderEntryData orderEntry : cartData.getEntries())
		{
			cartEntries++;
			if (StringUtils.isEmpty(truckloadId) || StringUtils.equalsIgnoreCase(truckloadId, orderEntry.getTruckloadId()))
			{
				prodQtyMap.put(orderEntry.getProduct().getName(), orderEntry.getQuantity());
				tempProdQtyMap.put(orderEntry.getProduct().getName(), orderEntry.getQuantity());
				populateProductQtyMap(prodQtyMap, orderEntry, tempProdQtyMap);
				truckloadPrice = truckloadPrice + orderEntry.getTotalPrice().getValue().doubleValue();
			}
			else
			{
				prodQtyMap.put(TRUCKLOAD_PRICE, Double.valueOf(truckloadPrice));
				truckloadMap.put(Integer.valueOf(truckCount), prodQtyMap);
				truckCount++;

				truckloadPrice = 0.0d;
				prodQtyMap = new LinkedHashMap();
				tempProdQtyMap = new LinkedHashMap();
				prodQtyMap.put(orderEntry.getProduct().getName(), orderEntry.getQuantity());
				tempProdQtyMap.put(orderEntry.getProduct().getName(), orderEntry.getQuantity());
				populateProductQtyMap(prodQtyMap, orderEntry, tempProdQtyMap);
				truckloadPrice = truckloadPrice + orderEntry.getTotalPrice().getValue().doubleValue();
			}
			if (cartEntries == cartData.getEntries().size())
			{
				prodQtyMap.put(TRUCKLOAD_PRICE, Double.valueOf(truckloadPrice));
				truckloadMap.put(Integer.valueOf(truckCount), prodQtyMap);
			}
			truckloadId = orderEntry.getTruckloadId();
		}
		return truckloadMap;
	}

	/**
	 * Populate product quantity map.
	 *
	 * @param prodQtyMap
	 *           the product quantity map
	 * @param orderEntry
	 *           the order entry
	 * @param tempProdQtyMap
	 *           - prod qty temp map
	 */
	private void populateProductQtyMap(final Map<String, Object> prodQtyMap, final OrderEntryData orderEntry,
			final Map<String, Object> tempProdQtyMap)
	{
		prodQtyMap.put(PRODUCT_LIST, tempProdQtyMap);
		prodQtyMap.put(SHIP_AFTER_DATE, orderEntry.getShipToAfterDate());
		prodQtyMap.put(TRUCKLOAD_ID, orderEntry.getTruckloadId());
		prodQtyMap.put(TRUCKLOAD_IMG_URL,
				StringUtils.isNotEmpty(orderEntry.getTruckloadImageUrl()) ? orderEntry.getTruckloadImageUrl() : StringUtils.EMPTY);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int calculateTwoSeaters(final int[] twoSeaterQtyArr)
	{
		int twoSeaterQty = 0;
		if (ArrayUtils.isNotEmpty(twoSeaterQtyArr))
		{

			for (final int twoSeaterCount : twoSeaterQtyArr)
			{
				twoSeaterQty = twoSeaterQty + twoSeaterCount;
			}
		}
		return twoSeaterQty;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int calculateFiveSeaters(final int[] fiveSeaterQtyArr)
	{
		int fiveSeaterQty = 0;
		if (ArrayUtils.isNotEmpty(fiveSeaterQtyArr))
		{

			for (final int fiveSeaterCount : fiveSeaterQtyArr)
			{
				fiveSeaterQty = fiveSeaterQty + fiveSeaterCount;
			}
		}
		return fiveSeaterQty;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, Object> setSuggestionAndMessage(final Map<String, Object> resultMap, final int[] twoSeaterQtyArray,
			final int[] fiveSeaterQtyArray)
	{
		final List<TruckloadData> suggestedTruckloadList = populateTruckloadSuggestions(twoSeaterQtyArray, fiveSeaterQtyArray);
		resultMap.put("suggestions", suggestedTruckloadList);
		final TruckloadData truckloadData = suggestedTruckloadList.get(0);

		final int totalRequestedQty = calculateTwoSeaters(twoSeaterQtyArray) + calculateFiveSeaters(fiveSeaterQtyArray);

		int maxAllowedQty = 0;

		if (null != truckloadData.getFiveSeaterCapacity() && null != truckloadData.getTwoSeaterCapacity())
		{
			maxAllowedQty = truckloadData.getFiveSeaterCapacity().intValue() + truckloadData.getTwoSeaterCapacity().intValue();
		}

		if (totalRequestedQty > maxAllowedQty)
		{
			resultMap.put(ERROR_MSG, QTY_GREATER_THAN_TRUCKLOAD);
		}
		else
		{
			resultMap.put(ERROR_MSG, QTY_LESSER_THAN_TRUCKLOAD);
		}
		return resultMap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, Object> doAddToCart(final int[] quantities, final String[] productCodes, final String[] productNames,
			final String shipAfterDate, final Map<String, Object> resultMap, final String imageUrl)
			throws CommerceCartModificationException, ParseException
	{
		int totalQuantity = 0;
		final Map<String, Integer> productQtyMap = new HashMap();
		final String b2bUnit = catCustomerFacade.getCurrentCustomer().getUnit().getUid();
		final String uniqueTruckloadId = CatCoreConstants.MIX_AND_MATCH_TRUCK + b2bUnit + System.currentTimeMillis();
		for (int count = 0; count < productCodes.length; count++)
		{
			final DateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.US);
			final Date date = formatter.parse(shipAfterDate);
			if (quantities[count] > 0)
			{
				cartFacade.addToCart(productCodes[count], quantities[count], date, uniqueTruckloadId, imageUrl);
				productQtyMap.put(productNames[count], Integer.valueOf(quantities[count]));
				totalQuantity = totalQuantity + quantities[count];
			}
		}
		resultMap.put(PROD_QTY_MAP, productQtyMap);
		resultMap.put(TOTAL_TRUCK_QTY, Integer.valueOf(totalQuantity));
		resultMap.put(TOTAL_CART_QTY, cartFacade.getSessionCart().getTotalUnitCount());
		resultMap.put(TRUCKLOAD_ID, uniqueTruckloadId);
		return resultMap;
	}

	/**
	 * @return the catTruckloadConverter
	 */
	public Converter<TruckloadModel, TruckloadData> getCatTruckloadConverter()
	{
		return catTruckloadConverter;
	}

	/**
	 * @param catTruckloadConverter
	 *           the catTruckloadConverter to set
	 */
	public void setCatTruckloadConverter(final Converter<TruckloadModel, TruckloadData> catTruckloadConverter)
	{
		this.catTruckloadConverter = catTruckloadConverter;
	}

	/**
	 * @param checkoutService
	 *           the checkoutService to set
	 */
	public void setCheckoutService(final CatCommerceCheckoutService checkoutService)
	{
		this.checkoutService = checkoutService;
	}

	/**
	 * @param commerceCategoryService
	 *           the commerceCategoryService to set
	 */
	public void setCommerceCategoryService(final CommerceCategoryService commerceCategoryService)
	{
		this.commerceCategoryService = commerceCategoryService;
	}

	/**
	 * @param productService
	 *           the productService to set
	 */
	public void setProductService(final CatProductService productService)
	{
		this.productService = productService;
	}

	/**
	 * @param productConverter
	 *           the productConverter to set
	 */
	public void setProductConverter(final Converter<ProductModel, ProductData> productConverter)
	{
		this.productConverter = productConverter;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Map<String, Object>> populateResponse(final CatSnopResponseMapper catSnopResponseMapper, final String laneType)
			throws ParseException
	{
		final List<Map<String, Object>> resultMapList = new ArrayList();
		if (catSnopResponseMapper != null && null != catSnopResponseMapper.getMessageArea())
		{

			populateUIResponse(resultMapList, catSnopResponseMapper, laneType);

		}
		else
		{
			final Map<String, Object> resultMap = new HashMap();
			resultMap.put(IS_RESPONSE_NULL, Boolean.TRUE);
			resultMap.put(IS_CONNECTION_ERROR, Boolean.FALSE);
			resultMapList.add(resultMap);
		}
		return resultMapList;
	}

	/**
	 * @param resultMapList
	 *           - result list
	 * @param catSnopResponseMapper
	 *           - response mapper
	 * @param laneType
	 *           - lane type
	 * @throws ParseException
	 */
	private void populateUIResponse(final List<Map<String, Object>> resultMapList,
			final CatSnopResponseMapper catSnopResponseMapper, final String laneType) throws ParseException
	{
		for (final CatSnopSubResponse catSnopSubResponse : catSnopResponseMapper.getMessageArea())
		{
			try
			{
				if (catSnopSubResponse != null && StringUtils.isNotEmpty(catSnopSubResponse.getDca()))
				{
					final Map<String, Object> resultMap = new HashMap();
					resultMap.put(IS_RESPONSE_NULL, Boolean.FALSE);
					resultMap.put(IS_CONNECTION_ERROR, Boolean.FALSE);
					if ((CONNECTION_ERROR).equalsIgnoreCase(catSnopResponseMapper.getIibTransactionId()))
					{
						resultMap.put(SUCCESS, Boolean.FALSE);
						resultMap.put(IS_CONNECTION_ERROR, Boolean.TRUE);
						resultMapList.add(resultMap);
						break;
					}
					populateBomSopFromResponse(laneType, catSnopSubResponse, resultMap);
					resultMapList.add(resultMap);
				}
			}
			catch (final Exception e)
			{
				LOG.error("Exception in populating SNOP UI Response", e);
				final Map<String, Object> resultMap = new HashMap();
				resultMap.put("SUCCESS", Boolean.TRUE);
				resultMapList.add(resultMap);
			}
		}


	}

	/**
	 * This method populates BOM / SOP response
	 *
	 * @param laneType
	 *           - lane type
	 * @param catSnopSubResponse
	 *           - sub response
	 * @param resultMap
	 *           - result map
	 * @throws ParseException
	 */
	private void populateBomSopFromResponse(final String laneType, final CatSnopSubResponse catSnopSubResponse,
			final Map<String, Object> resultMap) throws ParseException
	{
		BaseVariantProductModel baseProduct = null;
		String genericProdCode = "";
		if (StringUtils.isNotEmpty(catSnopSubResponse.getDca()))
		{
			baseProduct = (BaseVariantProductModel) productService.getProduct(catSnopSubResponse.getDca());
			genericProdCode = baseProduct.getBaseProduct().getGenericProductCode();
		}

		if (LaneTypeEnum.LANE2.toString().equals(laneType) && "Y".equalsIgnoreCase(catSnopSubResponse.getOrderBomSW()))
		{
			populateBomResponse(catSnopSubResponse, genericProdCode, resultMap);
		}
		else if ("Y".equalsIgnoreCase(catSnopSubResponse.getOrderLimitSw()))
		{
			populateSopResponse(catSnopSubResponse, genericProdCode, resultMap);
		}
		else if ("N".equalsIgnoreCase(catSnopSubResponse.getOrderLimitSw()))
		{
			resultMap.put(SUCCESS, Boolean.TRUE);
			resultMap.put(PRODUCT_CODE, genericProdCode);
			resultMap.put(ORDER_LIMIT, Integer.valueOf(catSnopSubResponse.getOrderLimitOut()));
			resultMap.put(ORDER_LIMIT_USED, Integer.valueOf(catSnopSubResponse.getOrderLimitUsed()));
			resultMap.put(IS_RESPONSE_NULL, Boolean.FALSE);
		}

	}

	/**
	 * This method popuates SOP SW related response
	 *
	 * @param catSnopSubResponse
	 *           - sub response
	 * @param genericProdCode
	 *           - product code
	 * @param resultMap
	 *           - result map
	 * @throws ParseException
	 */
	private void populateSopResponse(final CatSnopSubResponse catSnopSubResponse, final String genericProdCode,
			final Map<String, Object> resultMap) throws ParseException
	{
		final int sopOrderLimit = Integer.parseInt(catSnopSubResponse.getOrderLimitOut());
		final int sopOrderused = Integer.parseInt(catSnopSubResponse.getOrderLimitUsed());
		resultMap.put(ORDER_LIMIT, Integer.valueOf(catSnopSubResponse.getOrderLimitOut()));
		resultMap.put(ORDER_LIMIT_USED, Integer.valueOf(catSnopSubResponse.getOrderLimitUsed()));
		if (sopOrderLimit - sopOrderused >= 0)
		{
			resultMap.put(SUCCESS, Boolean.TRUE);
			if ("Y".equalsIgnoreCase(catSnopSubResponse.getSopIndicator())
					&& StringUtils.isNotBlank(catSnopSubResponse.getSopAccountingDays()))
			{
				final String dt = catSnopSubResponse.getSopAccountingDays();
				final DateFormat formatter = new SimpleDateFormat("MMddyy", Locale.US);
				final Date date = formatter.parse(dt);

				Date todayDate = new Date();
				final String fdate = formatter.format(todayDate);
				todayDate = formatter.parse(fdate);

				if (date.before(todayDate))
				{
					resultMap.put(SUCCESS, Boolean.FALSE);
				}
			}
			else
			{
				resultMap.put(SUCCESS, Boolean.TRUE);

			}
		}
		else
		{
			resultMap.put(SUCCESS, Boolean.FALSE);
		}
		resultMap.put(PRODUCT_CODE, genericProdCode);

	}

	/**
	 * This method populates BOM response
	 *
	 * @param catSnopSubResponse
	 *           - sub response
	 * @param genericProdCode
	 *           - prod code
	 * @param resultMap
	 *           - result map
	 * @throws ParseException
	 */
	private void populateBomResponse(final CatSnopSubResponse catSnopSubResponse, final String genericProdCode,
			final Map<String, Object> resultMap) throws ParseException
	{
		if (StringUtils.isNotBlank(catSnopSubResponse.getBomAccountingDays()))
		{
			final String dt = catSnopSubResponse.getBomAccountingDays();
			final DateFormat formatter = new SimpleDateFormat("MMddyy", Locale.US);
			final Date date = formatter.parse(dt);

			Date todayDate = new Date();
			final String fdate = formatter.format(todayDate);
			todayDate = formatter.parse(fdate);

			if (date.before(todayDate))
			{
				resultMap.put(SUCCESS, Boolean.FALSE);
				resultMap.put(PRODUCT_CODE, genericProdCode);
				resultMap.put(INVALID_DATE, Boolean.TRUE);
			}
			else
			{
				populateSuccessBomResponse(catSnopSubResponse, genericProdCode, resultMap);
			}
		}
		else
		{
			populateSuccessBomResponse(catSnopSubResponse, genericProdCode, resultMap);
		}
	}

	/**
	 * This method populates BOM success response.
	 *
	 * @param catSnopSubResponse
	 *           - sub response
	 * @param genericProdCode
	 *           - prod code
	 * @param resultMap
	 *           - result map
	 */
	private void populateSuccessBomResponse(final CatSnopSubResponse catSnopSubResponse, final String genericProdCode,
			final Map<String, Object> resultMap)
	{
		final int bomOrderLimit = Integer.parseInt(catSnopSubResponse.getOrderBomOut());
		final int bomOrderused = Integer.parseInt(catSnopSubResponse.getOrderBomUsed());

		resultMap.put(ORDER_BOM_SW, Boolean.TRUE);
		resultMap.put(ORDER_LMT_SW, Boolean.FALSE);
		resultMap.put(ORDER_LIMIT, Integer.valueOf(bomOrderLimit));
		resultMap.put(ORDER_LIMIT_USED, Integer.valueOf(bomOrderused));
		resultMap.put(PRODUCT_CODE, genericProdCode);
		if (bomOrderLimit - bomOrderused >= 0)
		{
			resultMap.put(SUCCESS, Boolean.TRUE);
		}
		else
		{
			resultMap.put(SUCCESS, Boolean.FALSE);
		}
	}
}
