/**
 *
 */
package com.cat.bcp.facades.order.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CatReserveQuoteData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.order.impl.DefaultQuoteFacade;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.quote.data.QuoteData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.enums.DiscountType;
import de.hybris.platform.commerceservices.enums.QuoteAction;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.strategies.QuoteStateSelectionStrategy;
import de.hybris.platform.commerceservices.order.strategies.QuoteUserIdentificationStrategy;
import de.hybris.platform.commerceservices.util.CommerceQuoteUtils;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.enums.QuoteState;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;
import de.hybris.platform.util.DiscountValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.enums.CatQuoteLostReasonEnum;
import com.cat.bcp.core.exception.CatException;
import com.cat.bcp.core.integration.jobs.service.CatIntegrationService;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.InventoryReservationModel;
import com.cat.bcp.core.order.CatCommerceQuoteService;
import com.cat.bcp.core.service.CatCustomerService;
import com.cat.bcp.facades.order.CatQuoteFacade;
import com.cat.bcp.facades.quote.error.CatQuoteErrorData;
import com.cat.facades.quote.CatQuoteVETData;


/**
 * The Class CatQuoteFacadeImpl.
 *
 * @author prrungta
 */
public class CatQuoteFacadeImpl extends DefaultQuoteFacade implements CatQuoteFacade
{
	private static final Logger LOG = Logger.getLogger(CatQuoteFacadeImpl.class);
	private static final String QUOTE_STATUS_CODE = "quote.status.code.";
	private static final String QUOTE_REASON_LOST = "quote.reason.lost.";

	private CartService cartService;
	private QuoteUserIdentificationStrategy quoteUserIdentificationStrategy;

	@Resource(name = "catCommerceQuoteService")
	private CatCommerceQuoteService catCommerceQuoteService;

	@Resource(name = "commerceQuoteUtils")
	private CommerceQuoteUtils commerceQuoteUtils;

	@Resource(name = "calculationService")
	private CalculationService calculationService;

	@Resource
	private UserService userService;

	@Resource(name = "enumerationService")
	private EnumerationService enumerationService;

	@Resource(name = "quoteStateSelectionStrategy")
	QuoteStateSelectionStrategy quoteStateSelectionStrategy;

	@Resource(name = "catQuoteReverseConverter")
	private Converter<QuoteData, QuoteModel> quoteReverseConverter;

	@Resource(name = "quoteConverter")
	private Converter<QuoteModel, QuoteData> quoteConverter;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "cartFacade")
	CatCartFacadeImpl cartFacade;

	@Resource(name = "productConverter")
	private Converter<ProductModel, ProductData> productConverter;

	@Resource(name = "addressReversePopulator")
	private Populator<AddressData, AddressModel> addressReversePopulator;

	@Resource(name = "catCustomerService")
	private CatCustomerService catCustomerService;

	@Resource(name = "catIntegrationService")
	CatIntegrationService catIntegrationService;


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void applyQuoteDiscountEntryLevel(final long entryNumber, final double discountRate, final DiscountType discountType)
	{
		catCommerceQuoteService.applyQuoteDiscountEntryLevel(getCartService().getSessionCart(), entryNumber,
				getQuoteUserIdentificationStrategy().getCurrentQuoteUser(), discountRate, discountType);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void refreshCartData()
	{
		final AbstractOrderModel abstractOrderModel = getCartService().getSessionCart();
		for (final AbstractOrderEntryModel entry : abstractOrderModel.getEntries())
		{
			if (CollectionUtils.isNotEmpty(entry.getDiscountValues()))
			{
				final List<DiscountValue> discountValues = new ArrayList<>();
				entry.setDiscountValues(discountValues);
				getModelService().save(entry);
			}
		}
		try
		{
			calculationService.recalculate(abstractOrderModel);
		}
		catch (final CalculationException e)
		{
			LOG.error("Error while calculating cart", e);
		}
		getModelService().save(abstractOrderModel);
	}




	/**
	 * {@inheritDoc}
	 */
	@Override
	public QuoteData saveQuoteDetails(final QuoteData quoteData, final String quoteCode)
	{
		final QuoteModel quoteModel = getQuoteModelForCode(quoteCode);
		quoteReverseConverter.convert(quoteData, quoteModel);
		catCommerceQuoteService.saveQuoteDetails(quoteModel);
		return quoteConverter.convert(quoteModel);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void submitQuote(final String quoteCode)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Validating Quote");
		}
		validateParameterNotNullStandardMessage("quoteCode", quoteCode);
		final QuoteModel quoteModel = getQuoteModelForCode(quoteCode);
		final UserModel userModel = getQuoteUserIdentificationStrategy().getCurrentQuoteUser();
		final CartModel sessionCart = getCartService().getSessionCart();

		catCommerceQuoteService.validateQuoteThreshold(quoteModel, userModel, sessionCart);
		catCommerceQuoteService.unassignQuote(quoteModel, userModel);
		catCommerceQuoteService.submitQuote(quoteModel, userModel);
		catCommerceQuoteService.clearPreviousReservedSerialNumbers(quoteModel);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void enableQuoteEdit(final String quoteCode)
	{
		final QuoteModel quoteModel = getQuoteModelForCode(quoteCode);
		final UserModel currentQuoteUser = getQuoteUserIdentificationStrategy().getCurrentQuoteUser();
		catCommerceQuoteService.assignQuoteToUser(quoteModel, currentQuoteUser, currentQuoteUser);
		final CartModel cartModel = catCommerceQuoteService.loadQuoteAsSessionCart(quoteModel, currentQuoteUser);
		getModelService().saveAll(cartModel, quoteModel);
		getModelService().refresh(cartModel);
		getCartService().setSessionCart(cartModel);
		cartFacade.removeUnapprovedProductsFromCart(getCartService().getSessionCart().getEntries());
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void enableQuoteReplicate(final String quoteCode)
	{
		enableQuoteEdit(quoteCode);
		final CartModel cartModel = getCartService().getSessionCart();

		//Remove all old Mandatory Parts from Cart.
		if (CollectionUtils.isNotEmpty(cartModel.getEntries()))
		{
			final List<AbstractOrderEntryModel> partVariantList = getCartService().getSessionCart().getEntries().stream()
					.filter(entry -> entry.getMandatoryProduct().booleanValue() && StringUtils.isNotBlank(entry.getConfigVariantId()))
					.collect(Collectors.toList());
			removeMandatoryParts(partVariantList);

			//Add all new Mandatory Parts to Cart.
			for (final AbstractOrderEntryModel entry : getCartService().getSessionCart().getEntries())
			{
				final List<AddToCartParams> paramsList = new ArrayList();
				final ProductModel productModel = entry.getProduct();
				addMandatoryParts(entry, paramsList, productModel);
				try
				{
					cartFacade.catAddToCart(paramsList);
				}
				catch (final CommerceCartModificationException e)
				{
					LOG.error(e.getMessage(), e);
				}
			}
		}
		cartFacade.saveAndReCalculateCart(cartModel);
	}


	/**
	 * @param entry
	 * @param paramsList
	 * @param productModel
	 */
	private void addMandatoryParts(final AbstractOrderEntryModel entry, final List<AddToCartParams> paramsList,
			final ProductModel productModel)
	{
		if (productModel instanceof ConfigVariantProductModel)
		{
			final ConfigVariantProductModel configVariantProductModel = (ConfigVariantProductModel) productModel;
			final BaseVariantProductModel baseProduct = (BaseVariantProductModel) configVariantProductModel.getBaseProduct();
			if (baseProduct != null)
			{
				final boolean configurable = baseProduct.getFbcMustSelectFlag() != null
						? baseProduct.getFbcMustSelectFlag().booleanValue() : false;
				if (!configurable)
				{
					final Collection<ProductReferenceModel> productReferenceCollection = configVariantProductModel
							.getProductReferences();
					cartFacade.addProductReferenceParams(configVariantProductModel, entry.getQuantity().longValue(), paramsList,
							productReferenceCollection);
				}
			}
		}
	}


	/**
	 * @param partVariantList
	 */
	private void removeMandatoryParts(final List<AbstractOrderEntryModel> partVariantList)
	{
		if (CollectionUtils.isNotEmpty(partVariantList))
		{

			for (final AbstractOrderEntryModel partVariant : partVariantList)
			{
				try
				{
					cartFacade.catUpdateCartEntry(partVariant.getEntryNumber().longValue(), 0);
				}
				catch (final CommerceCartModificationException e)
				{
					LOG.error(e.getMessage(), e);
				}
			}

		}
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, CatQuoteLostReasonEnum> getQuotationLostReasons()
	{
		final List<CatQuoteLostReasonEnum> catQuoteLostReason = enumerationService
				.getEnumerationValues(CatQuoteLostReasonEnum.class);

		return catQuoteLostReason.stream()
				.collect(Collectors.toMap(catQuoteLostEnum -> Config.getParameter(QUOTE_REASON_LOST + catQuoteLostEnum.getCode()),
						catQuoteLostEnum -> catQuoteLostEnum));
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, QuoteState> getQuoteStatusesForSalesPerson()
	{
		final UserModel quoteUserModel = getUserService().getCurrentUser();
		final Set<QuoteState> quoteStateSet = quoteStateSelectionStrategy.getAllowedStatesForAction(QuoteAction.VIEW,
				quoteUserModel);
		return quoteStateSet.stream()
				.filter(quoteStates -> !quoteStates.getCode().equalsIgnoreCase(CatCoreConstants.CREATED_STATE)
						&& !quoteStates.getCode().equalsIgnoreCase(CatCoreConstants.DRAFTED_STATE))
				.collect(Collectors.toMap(quoteState -> Config.getParameter(QUOTE_STATUS_CODE + quoteState.getCode()),
						quoteState -> quoteState));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteQuote(final String quoteCode)
	{
		final QuoteModel quoteModel = getQuoteModelForCode(quoteCode);
		catCommerceQuoteService.deleteQuote(quoteModel);
	}


	/**
	 * This method is used to get List of Serial Numbers that can be reserved for given Config Variant Product.
	 *
	 * @param productCode
	 * @return List of CatReserveQuoteData.
	 */
	@Override
	public List<CatReserveQuoteData> getReservationQuoteData(final String productCode)
	{
		final List<CatReserveQuoteData> catReserveQuoteDataList = new ArrayList();
		final B2BCustomerModel b2bCustomerModel = (B2BCustomerModel) userService.getCurrentUser();

		final List<InventoryReservationModel> inventoryReservationModelList = catCommerceQuoteService
				.getReservationQuoteData(productService.getProduct(productCode), b2bCustomerModel.getDefaultB2BUnit());

		if (CollectionUtils.isNotEmpty(inventoryReservationModelList))
		{
			final CatReserveQuoteData catReserveQuoteData = new CatReserveQuoteData();
			final List<String> serialNumbers = new ArrayList();
			for (final InventoryReservationModel inventoryReservationModel : inventoryReservationModelList)
			{
				serialNumbers.add(inventoryReservationModel.getSerialNumber());
			}
			catReserveQuoteData.setSerialNumbers(serialNumbers);
			catReserveQuoteDataList.add(catReserveQuoteData);
		}

		return catReserveQuoteDataList;
	}

	/**
	 * This method is used to reserve Serial Numbers for Config Variant Products on Quote Page.
	 *
	 * @param quoteCode
	 * @param catReserveQuoteDataList
	 */
	@Override
	public void saveReservationQuoteData(final String quoteCode, final List<CatReserveQuoteData> catReserveQuoteDataList)
	{

		validateParameterNotNullStandardMessage("quoteCode", quoteCode);
		final QuoteModel quoteModel = getQuoteModelForCode(quoteCode);
		final B2BCustomerModel b2bCustomerModel = (B2BCustomerModel) userService.getCurrentUser();
		if (CollectionUtils.isNotEmpty(catReserveQuoteDataList))
		{
			for (final CatReserveQuoteData catReserveQuoteData : catReserveQuoteDataList)
			{
				final ProductModel productModel = productService
						.getProductForCode(catReserveQuoteData.getConfigVariantProduct().getCode());
				for (final String serialNmuber : catReserveQuoteData.getSerialNumbers())
				{
					catCommerceQuoteService.saveReservedSerialNumbersForConfiguration(quoteModel, b2bCustomerModel,
							catReserveQuoteData.getEndDate(), productModel, serialNmuber);
				}
			}
		}
	}


	/**
	 * @return the cartService
	 */
	@Override
	public CartService getCartService()
	{
		return cartService;
	}

	/**
	 * @param cartService
	 *           the cartService to set
	 */
	@Override
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	/**
	 * @return the quoteUserIdentificationStrategy
	 */
	@Override
	public QuoteUserIdentificationStrategy getQuoteUserIdentificationStrategy()
	{
		return quoteUserIdentificationStrategy;
	}

	/**
	 * @param quoteUserIdentificationStrategy
	 *           the quoteUserIdentificationStrategy to set
	 */
	@Override
	public void setQuoteUserIdentificationStrategy(final QuoteUserIdentificationStrategy quoteUserIdentificationStrategy)
	{
		this.quoteUserIdentificationStrategy = quoteUserIdentificationStrategy;
	}

	/**
	 * @return the catCommerceQuoteService
	 */
	public CatCommerceQuoteService getCatCommerceQuoteService()
	{
		return catCommerceQuoteService;
	}

	/**
	 * @param catCommerceQuoteService
	 *           the catCommerceQuoteService to set
	 */
	public void setCatCommerceQuoteService(final CatCommerceQuoteService catCommerceQuoteService)
	{
		this.catCommerceQuoteService = catCommerceQuoteService;
	}

	@Override
	public void updateQuoteStatus(final String quoteCode, final boolean submittedToDSU) throws Exception
	{
		catCommerceQuoteService.updateQuote(quoteCode, submittedToDSU);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> getOrderAdditionalInfoData()
	{

		return catCommerceQuoteService.getProductFamilies();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductData> getQuoteItemsWithNoStock(final String quoteCode)
	{
		final List<ProductModel> productsWithNoStock = catCommerceQuoteService.getQuoteItemsWithNoStock(quoteCode);
		if (null != productsWithNoStock)
		{
			return productConverter.convertAll(productsWithNoStock);
		}
		//All products in the quote are out of stock.
		return null; // NOSONAR
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateProductWithLessStock()
	{
		final CartModel cart = cartService.getSessionCart();

		if (null != cart)
		{
			for (final AbstractOrderEntryModel entry : cart.getEntries())
			{
				final ProductModel product = entry.getProduct();
				if (product instanceof ConfigVariantProductModel)
				{
					updateQuoteProductQty(entry);
				}
			}
		}
	}


	/**
	 * Update Quote Product Quantity
	 *
	 * @param entry
	 *           AbstractOrderEntryModel
	 */
	private void updateQuoteProductQty(final AbstractOrderEntryModel entry)
	{
		final B2BCustomerModel b2bCustomer = (B2BCustomerModel) userService.getCurrentUser();
		if (b2bCustomer.getDefaultB2BUnit() != null)
		{
			final long stock = catCommerceQuoteService.calculateStock(entry.getProduct(), b2bCustomer);

			try
			{
				if (stock == 0)
				{
					//remove product entry from the quote when no stock.
					cartFacade.catUpdateCartEntry(entry.getEntryNumber().longValue(), 0);
				}
				else if (entry.getQuantity().longValue() > stock)
				{
					//reduce quantity to 1 when quote product quantity is more than current stock.
					cartFacade.catUpdateCartEntry(entry.getEntryNumber().longValue(), 1);
					entry.setQuantityUpdated(Boolean.TRUE);
					getModelService().save(entry);
				}
			}
			catch (final CommerceCartModificationException e)
			{
				LOG.error("Error updating cart quantity:: ", e);
			}

		}
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setCatQuoteErrorData(final CartData cartData)
	{
		if (null != cartData.getQuoteData())
		{
			final Map<String, Boolean> productCodeErroDataMap = new LinkedHashMap();
			for (final OrderEntryData entry : cartData.getEntries())
			{
				poulateErrorDataMap(productCodeErroDataMap, entry);
			}
			populateErrorDataForEditQuotePage(cartData, productCodeErroDataMap);
		}
	}


	/**
	 * This helper method is created to resolve sonar complexity issue
	 *
	 * @param productCodeErroDataMap
	 * @param entry
	 */
	private void poulateErrorDataMap(final Map<String, Boolean> productCodeErroDataMap, final OrderEntryData entry)
	{
		final CatQuoteErrorData errorData = new CatQuoteErrorData();

		final ProductModel product = productService.getProductForCode(entry.getProduct().getCode());
		if (product instanceof ConfigVariantProductModel)
		{
			try
			{
				setReservationErrorData(errorData, product);
			}
			catch (final CatException e)
			{
				LOG.error("Error while fetching serial number data", e);
			}
			entry.setErrorData(errorData);
			productCodeErroDataMap.put(product.getCode(), errorData.getDisableReserveBtn());
		}
	}


	/**
	 * This method is used to populate error Data in QuoteData entries in cart data response. This is used on Edit Quote
	 * Page.
	 *
	 * @param cartData
	 *           CartData Object
	 * @param productCodeErroDataMap
	 *           Map(key-ProductCode and value-BooleanValue_disableReserveButton)
	 */
	private void populateErrorDataForEditQuotePage(final CartData cartData, final Map<String, Boolean> productCodeErroDataMap)
	{
		for (final OrderEntryData entry : cartData.getQuoteData().getEntries())
		{
			if (productCodeErroDataMap.containsKey(entry.getProduct().getCode()))
			{
				final Boolean disableReserveBtn = productCodeErroDataMap.get(entry.getProduct().getCode());
				if (BooleanUtils.isTrue(disableReserveBtn))
				{
					populateErrorDataBasedOnSerialNumbers(entry);
				}
			}
		}
	}


	/**
	 * This method is used to populate error Data in QuoteData entries in cart data response. This is used on Edit Quote
	 * Page.
	 *
	 * @param entry
	 *           OrderEntryData Object
	 */
	private void populateErrorDataBasedOnSerialNumbers(final OrderEntryData entry)
	{

		final CatQuoteErrorData errorData = new CatQuoteErrorData();
		errorData.setDisableReserveBtn(Boolean.TRUE);
		errorData.setReserveBtnErrorMsg("All available units are reserved");
		entry.setErrorData(errorData);

	}


	/**
	 * Set Reservation Error Data
	 *
	 * @param errorData
	 * @param product
	 * @throws CatException
	 */
	private void setReservationErrorData(final CatQuoteErrorData errorData, final ProductModel product) throws CatException
	{
		//check for serial number.
		final List<CatReserveQuoteData> catReserveQuoteDataList = new ArrayList();
		final B2BCustomerModel b2bCustomerModel = (B2BCustomerModel) userService.getCurrentUser();
		final CatReserveQuoteData catReserveQuoteData = catIntegrationService.getSerialNumbersData(product.getCode(),
				b2bCustomerModel.getDefaultB2BUnit().getUid());
		if (null != catReserveQuoteData && CollectionUtils.isNotEmpty(catReserveQuoteData.getSerialNumbers()))
		{
			catReserveQuoteDataList.add(catReserveQuoteData);
		}
		if (CollectionUtils.isEmpty(catReserveQuoteDataList))
		{
			errorData.setDisableReserveBtn(Boolean.TRUE);
			errorData.setReserveBtnErrorMsg("All available units are reserved");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateQuoteAddress(final String quoteCode, final AddressData addressData)
	{
		final QuoteModel quoteModel = getQuoteModelForCode(quoteCode);
		final AddressModel newAddress = getModelService().create(AddressModel.class);
		addressReversePopulator.populate(addressData, newAddress);
		final CustomerModel currentCustomer = catCustomerService.findUserByCustomerEmail(addressData.getEmail());
		newAddress.setOwner(currentCustomer);
		if (addressData.isBillingAddress())
		{
			quoteModel.setPaymentAddress(newAddress);
			getModelService().save(quoteModel);
		}
		if (addressData.isShippingAddress())
		{
			quoteModel.setDeliveryAddress(newAddress);
			getModelService().save(quoteModel);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public QuoteData requote(final String quoteCode)
	{
		final QuoteModel quoteModel = getQuoteModelForCode(quoteCode);
		final UserModel userModel = getQuoteUserIdentificationStrategy().getCurrentQuoteUser();
		final QuoteModel newQuoteModel = catCommerceQuoteService.requote(quoteModel, userModel);
		return quoteConverter.convert(newQuoteModel);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CartData setErrorDataForCart(final CartData cartData, final long entryNumber, final String errorMessage)
	{
		final List<OrderEntryData> orderEntryDataList = cartData.getEntries();
		if (CollectionUtils.isNotEmpty(orderEntryDataList) && Long.valueOf(entryNumber) != null)
		{
			for (final OrderEntryData orderEntryData : orderEntryDataList)
			{
				final Integer requestedEntryNumber = Integer.valueOf((int) entryNumber);
				if (orderEntryData.getEntryNumber().equals(requestedEntryNumber))
				{
					orderEntryData.setErrorMessage(errorMessage);
				}
			}
		}
		return cartData;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addValueEstimationForQuoteEntry(final String vetLink, final String vetProduct, final long entryNumber)
	{
		if (null != vetLink && null != vetProduct)
		{
			catCommerceQuoteService.addValueEstimationEntryLevel(getCartService().getSessionCart(), entryNumber, vetLink,
					vetProduct);
		}
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeValueEstimationForQuoteEntry(final String vetLink, final String vetProduct, final long entryNumber)
	{
		catCommerceQuoteService.removeValueEstimationEntryLevel(getCartService().getSessionCart(), entryNumber, vetLink,
				vetProduct);

	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public void editValueEstimationForQuoteEntry(final List<CatQuoteVETData> catQuoteVETDatas, final long entryNumber)
	{
		catCommerceQuoteService.editValueEstimationEntryLevel(getCartService().getSessionCart(), entryNumber, catQuoteVETDatas);

	}
}
