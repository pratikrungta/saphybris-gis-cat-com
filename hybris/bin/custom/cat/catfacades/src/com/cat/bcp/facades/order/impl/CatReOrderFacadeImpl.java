/**
 *
 */
package com.cat.bcp.facades.order.impl;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.enums.LaneTypeEnum;
import com.cat.bcp.core.model.AlertProductModel;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.ProductLaneInfoModel;
import com.cat.bcp.core.model.RestockAlertModel;
import com.cat.bcp.core.order.CatReorderService;
import com.cat.bcp.core.servicelayer.CatCategoryService;
import com.cat.bcp.core.servicelayer.CatOrderLaneProductService;
import com.cat.bcp.core.servicelayer.impl.CatOrderWindowCalculationUtility;
import com.cat.bcp.facades.customer.CatCustomerFacade;
import com.cat.bcp.facades.order.CatCartFacade;
import com.cat.bcp.facades.order.CatReorderFacade;
import com.cat.facades.order.CatLaneOneData;
import com.cat.facades.order.CatLaneSpecificData;
import com.cat.facades.order.CatLaneThreeData;
import com.cat.facades.order.CatLaneTwoData;
import com.cat.facades.order.CatStockingRecommendationData;


/**
 * Facade having the reorder logic
 *
 * @author sagdhingra
 *
 */
public class CatReOrderFacadeImpl implements CatReorderFacade
{

	@Resource(name = "catOrderLaneProductService")
	private CatOrderLaneProductService catOrderLaneProductService;

	@Resource(name = "catOrderWindowCalculationUtility")
	private CatOrderWindowCalculationUtility catOrderWindowCalculationUtility;

	@Resource(name = "categoryService")
	private CatCategoryService categoryService;

	@Resource(name = "catCustomerFacade")
	private CatCustomerFacade catCustomerFacade;

	@Resource(name = "catReorderService")
	private CatReorderService catReorderService;

	@Resource(name = "productConverter")
	private Converter<ProductModel, ProductData> productConverter;

	@Resource(name = "cartFacade")
	private CatCartFacade cartFacade;

	@Resource(name = "categoryConverter")
	private Converter<CategoryModel, CategoryData> categoryConverter;

	private static final String LANE2 = "lane2";
	private static final String LANE3 = "lane3";

	private static final String DATEFORMAT = "MMMM yyyy";

	private static final Logger LOG = Logger.getLogger(CatReOrderFacadeImpl.class);


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<RestockAlertModel> getLowStockProducts(final B2BUnitModel b2bUnit, final String category,
			final String salesModelId)
	{
		return catReorderService.getLowStockProducts(b2bUnit, category, salesModelId);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AlertProductModel> getAssociatedConfigProducts(final String alertId)
	{
		final List<AlertProductModel> alertProducts = catReorderService.getAssociatedConfigProducts(alertId);

		//Approved list of products.
		final List<AlertProductModel> newAlertProducts = new ArrayList();
		if (CollectionUtils.isNotEmpty(alertProducts))
		{
			for (final AlertProductModel alertProduct : alertProducts)
			{
				final ProductModel productModel = alertProduct.getProduct();
				if (null != productModel
						&& ArticleApprovalStatus.APPROVED.toString().equalsIgnoreCase(productModel.getApprovalStatus().getCode()))
				{
					newAlertProducts.add(alertProduct);
				}
			}
		}
		return newAlertProducts;
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductData> getReorderProducts(final List<RestockAlertModel> alertEntries, final B2BUnitModel defaultB2BUnit)
	{
		final List<ProductData> productList = new ArrayList();

		try
		{

			for (final RestockAlertModel restockEntry : alertEntries)
			{
				final ProductData product = productConverter.convert(restockEntry.getProduct());
				final CatLaneSpecificData laneSpecificData = new CatLaneSpecificData();

				final Map<String, Object> orderWindowMap = catOrderWindowCalculationUtility.getOrderingWindowMessage();


				final CatLaneTwoData catLaneTwoData = (CatLaneTwoData) orderWindowMap.get(LANE2);

				final CatLaneThreeData catLaneThreeData = (CatLaneThreeData) orderWindowMap.get(LANE3);

				prepareLaneOneData(defaultB2BUnit, laneSpecificData, restockEntry);

				prepareStockingRecommendationData(restockEntry, product);

				setEssentialDataForLaneTwo(catLaneTwoData, restockEntry, product);
				setEssentialDataForLaneThree(catLaneThreeData, restockEntry, product);

				setAddToCartOptionLaneWise(catLaneTwoData, catLaneThreeData, product);

				laneSpecificData.setLane2(catLaneTwoData);
				laneSpecificData.setLane3(catLaneThreeData);

				product.setConfigProductsList(laneSpecificData);
				productList.add(product);
			}

		}
		catch (final ParseException e)
		{
			LOG.error(e.getMessage(), e);
		}

		return productList;
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductData> getPageableData(final List<RestockAlertModel> alertEntries, final int page,
			final int numOfProductsPerPage)
	{
		final List<ProductData> productList = new ArrayList();
		final int startIndex = (page * numOfProductsPerPage) - numOfProductsPerPage;
		int i = 0;
		for (final RestockAlertModel restockEntry : alertEntries)
		{

			if (i >= startIndex && i < page * numOfProductsPerPage)
			{
				final ProductData product = productConverter.convert(restockEntry.getProduct());
				product.setMaximumStockLevel(restockEntry.getMaximumStockLevel());
				product.setMinimumStockLevel(restockEntry.getMinimumStockLevel());
				product.setRecommendedStockLevel(restockEntry.getRecommendedStockLevel());
				product.setCurrentStockLevel(restockEntry.getCurrentStockLevel());
				productList.add(product);
				if (i == page * numOfProductsPerPage)
				{
					break;
				}
			}
			i++;
		}
		return productList;
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CategoryData> getLowStockProductsCategories(final B2BUnitModel b2bUnitModel)
	{
		return this.categoryConverter.convertAll(categoryService.getLowStockProductsCategories(b2bUnitModel));
	}


	/**
	 * Sets the add to cart option lane wise.
	 *
	 * @param catLaneTwoData
	 *           the cat lane two data
	 * @param catLaneThreeData
	 *           the cat lane three data
	 * @param product
	 *           the product
	 * @throws ParseException
	 *            the parse exception
	 */
	private void setAddToCartOptionLaneWise(final CatLaneTwoData catLaneTwoData, final CatLaneThreeData catLaneThreeData,
			final ProductData product) throws ParseException
	{
		//1st-orderingWindowValidation 2nd dateValidation 3rd compatibility 4th Stock
		catLaneTwoData.setAddToCartDisabled(Boolean.valueOf(!catOrderLaneProductService
				.isProductOrderableForLane(LaneTypeEnum.LANE2.getCode(), product.getCode(), null, true, true, false, true)));
		catLaneThreeData.setAddToCartDisabled(Boolean.valueOf(!catOrderLaneProductService
				.isProductOrderableForLane(LaneTypeEnum.LANE3.getCode(), product.getCode(), null, true, true, false, true)));
	}


	/**
	 * Sets the essential data for lane three.
	 *
	 * @param catLaneThreeData
	 *           the cat lane three data
	 * @param restockEntry
	 *           the restock entry
	 * @param product
	 *           the product
	 */
	private void setEssentialDataForLaneThree(final CatLaneThreeData catLaneThreeData, final RestockAlertModel restockEntry,
			final ProductData product)
	{
		final ProductLaneInfoModel productLaneInfoModelForLane3 = catOrderLaneProductService.getLaneInfoForLaneType(
				LaneTypeEnum.LANE3.getCode(), product.getCode(), catCustomerFacade.getCurrentDealer().getDefaultB2BUnit());
		int maximumOrderableQty = 0;
		int reOrderableQty = 0;
		if (null != productLaneInfoModelForLane3)
		{
			maximumOrderableQty = null != productLaneInfoModelForLane3.getMaximumOrderableQuantity()
					? productLaneInfoModelForLane3.getMaximumOrderableQuantity().intValue() : 0;
			reOrderableQty = null != productLaneInfoModelForLane3.getReorderedQuantity()
					? productLaneInfoModelForLane3.getReorderedQuantity().intValue() : 0;

			catLaneThreeData.setAllocationCount(Integer.valueOf(maximumOrderableQty - reOrderableQty));
			catLaneThreeData.setMonth_Year(setMonth_YearLaneWise(productLaneInfoModelForLane3.getMonthYear()));
			catLaneThreeData.setMaximumCount(productLaneInfoModelForLane3.getMaximumOrderableQuantity());
		}
		else
		{
			catLaneThreeData.setAllocationCount(Integer.valueOf(maximumOrderableQty - reOrderableQty));
			catLaneThreeData.setMaximumCount(Integer.valueOf(maximumOrderableQty));
			final SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
			catLaneThreeData.setMonth_Year(sdf.format(new Date()));
		}
		catLaneThreeData.setConfigVariantList(getConfigProductsLaneWise(restockEntry, LaneTypeEnum.LANE3.getCode()));
	}




	/**
	 * Sets the month year lane wise.
	 *
	 * @param monthYear
	 *           the month year
	 * @return the date
	 */
	private String setMonth_YearLaneWise(final String monthYear)
	{
		final SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
		try
		{
			final Date date = new SimpleDateFormat("MM-yyyy").parse(monthYear);
			return sdf.format(date);
		}
		catch (final ParseException e)
		{
			LOG.error("Error while parsing Date", e);
		}
		return sdf.format(new Date());
	}


	/**
	 * Sets the essential data for lane two.
	 *
	 * @param catLaneTwoData
	 *           the cat lane two data
	 * @param restockEntry
	 *           the restock entry
	 * @param product
	 *           the product
	 */
	private void setEssentialDataForLaneTwo(final CatLaneTwoData catLaneTwoData, final RestockAlertModel restockEntry,
			final ProductData product)
	{
		final ProductLaneInfoModel productLaneInfoModel = catOrderLaneProductService.getLaneInfoForLaneType(
				LaneTypeEnum.LANE2.getCode(), product.getCode(), catCustomerFacade.getCurrentDealer().getDefaultB2BUnit());

		catLaneTwoData.setConfigVariantList(getConfigProductsLaneWise(restockEntry, LaneTypeEnum.LANE2.getCode()));
		int maximumOrderableQty = 0;
		int reOrderableQty = 0;
		if (null != productLaneInfoModel)
		{
			maximumOrderableQty = null != productLaneInfoModel.getMaximumOrderableQuantity()
					? productLaneInfoModel.getMaximumOrderableQuantity().intValue() : 0;
			reOrderableQty = null != productLaneInfoModel.getReorderedQuantity()
					? productLaneInfoModel.getReorderedQuantity().intValue() : 0;

			catLaneTwoData.setCommittedCount(Integer.valueOf(maximumOrderableQty - reOrderableQty));
			catLaneTwoData.setMonth_Year(setMonth_YearLaneWise(productLaneInfoModel.getMonthYear()));
			catLaneTwoData.setMaximumCount(productLaneInfoModel.getMaximumOrderableQuantity());
		}
		else
		{
			catLaneTwoData.setCommittedCount(Integer.valueOf(maximumOrderableQty - reOrderableQty));
			catLaneTwoData.setMaximumCount(Integer.valueOf(maximumOrderableQty));
			final SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
			catLaneTwoData.setMonth_Year(sdf.format(new Date()));
		}
	}



	/**
	 * Prepare lane one data.
	 *
	 * @param defaultB2BUnit
	 *           the default B 2 B unit
	 * @param laneSpecificData
	 *           the lane specific data
	 * @param restockEntry
	 *           the restock entry
	 */
	private void prepareLaneOneData(final B2BUnitModel defaultB2BUnit, final CatLaneSpecificData laneSpecificData,
			final RestockAlertModel restockEntry)
	{
		final CatLaneOneData catLaneOneData = new CatLaneOneData();
		final List<ProductData> configProducts = getConfigProductsforAlert(restockEntry, defaultB2BUnit);
		catLaneOneData.setAddToCartDisabled(Boolean.FALSE);
		catLaneOneData.setConfigVariantList(configProducts);
		laneSpecificData.setLane1(catLaneOneData);
	}



	/**
	 * Prepare stocking recommendation data.
	 *
	 * @param restockEntry
	 *           the restock entry
	 * @param product
	 *           the product
	 */
	private void prepareStockingRecommendationData(final RestockAlertModel restockEntry, final ProductData product)
	{
		final CatStockingRecommendationData stockingRecommendationData = new CatStockingRecommendationData();
		stockingRecommendationData.setMinimum(restockEntry.getMinimumStockLevel());
		stockingRecommendationData.setCurrent(restockEntry.getCurrentStockLevel());
		stockingRecommendationData.setRecommended(restockEntry.getRecommendedStockLevel());
		product.setStockingRecommendation(stockingRecommendationData);
	}


	/**
	 * Gets the config products for alert.
	 *
	 * @param alert
	 *           the alert
	 * @param defaultB2BUnit
	 *           the default B 2 B unit
	 * @return the config productsfor alert
	 */
	private List<ProductData> getConfigProductsforAlert(final RestockAlertModel alert, final B2BUnitModel defaultB2BUnit)
	{
		final String salesModelCode = alert.getProduct().getCode();
		final String alertId = salesModelCode + "_" + defaultB2BUnit.getUid();

		final List<AlertProductModel> alertProducts = getAssociatedConfigProducts(alertId);
		final List<ConfigVariantProductModel> configVariantProductModels = alertProducts.stream()
				.map(alertProduct -> (ConfigVariantProductModel) alertProduct.getProduct()).collect(Collectors.toList());
		final List<ProductData> configProducts = new ArrayList();
		if (CollectionUtils.isNotEmpty(alertProducts))
		{
			for (final AlertProductModel alertProduct : alertProducts)
			{
				if (isOrderable(alertProduct.getProduct()))
				{
					final ProductData configProduct = productConverter.convert(alertProduct.getProduct());
					configProduct
							.setOrderedQty(Integer.valueOf(getOrderedQuantity(configProduct.getCode(), LaneTypeEnum.LANE1).intValue()));
					configProducts.add(configProduct);
				}
			}
		}
		setEssentialDataforConfigProducts(configVariantProductModels, configProducts);
		return configProducts;
	}


	/**
	 * Checks if is orderable.
	 *
	 * @param product
	 *           the product
	 * @return true, if is orderable
	 */
	private boolean isOrderable(final ProductModel product)
	{
		final ConfigVariantProductModel configVariant = (ConfigVariantProductModel) product;
		if (null != configVariant.getBaseProduct())
		{
			final BaseVariantProductModel baseVariantProductModel = (BaseVariantProductModel) configVariant.getBaseProduct();
			if (BooleanUtils.isFalse(baseVariantProductModel.getIsReorderable())
					|| baseVariantProductModel.getApprovalStatus().equals(ArticleApprovalStatus.UNAPPROVED))
			{
				return false;
			}
		}
		else
		{
			return false;
		}
		return true;
	}


	/**
	 * Gets the config products lane wise.
	 *
	 * @param alert
	 *           the alert
	 * @param laneType
	 *           the lane type
	 * @return the config products lane wise
	 */
	private List<ProductData> getConfigProductsLaneWise(final RestockAlertModel alert, final String laneType)
	{
		final String salesModelCode = alert.getProduct().getCode();

		final List<ConfigVariantProductModel> configProducts = catReorderService.getCompatibleConfigProducts(salesModelCode,
				laneType, null);
		List<ProductData> configProductList = new ArrayList();
		if (CollectionUtils.isNotEmpty(configProducts))
		{
			final List<ConfigVariantProductModel> validConfigProducts = configProducts.stream().filter(config -> isOrderable(config))
					.collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(validConfigProducts))
			{
				configProductList = validConfigProducts.stream().map(configProd -> productConverter.convert(configProd))
						.collect(Collectors.toList());
				configProductList.stream().forEach(configList -> configList.setOrderedQty(
						Integer.valueOf(getOrderedQuantity(configList.getCode(), LaneTypeEnum.valueOf(laneType)).intValue())));
			}
		}
		setEssentialDataforConfigProducts(configProducts, configProductList);
		return configProductList;
	}

	public Long getOrderedQuantity(final String productCode, final LaneTypeEnum laneType)
	{

		final CartData cartData = cartFacade.getSessionCart();
		final LaneTypeEnum laneTypeEnum = (null != cartData && null != cartData.getLaneType()) ? cartData.getLaneType()
				: LaneTypeEnum.LANE1;
		if (null != cartData && CollectionUtils.isNotEmpty(cartData.getEntries()) && laneTypeEnum.equals(laneType))
		{
			final Optional<OrderEntryData> orderEntry = cartData.getEntries().stream()
					.filter(cartObj -> cartObj.getProduct().getCode().equals(productCode)).findFirst();
			return orderEntry.isPresent() ? orderEntry.get().getQuantity() : Long.valueOf(0);
		}
		return Long.valueOf(0);
	}

	/**
	 * This method sets the attributes FbcMustSelectFlag, DCACode and ImageUrl for ConfigVariants
	 *
	 * @param configVariantProductModels
	 *           List of configvariant models
	 * @param configProductsList
	 *           Updated Product data list of Config Variants
	 */
	private void setEssentialDataforConfigProducts(final List<ConfigVariantProductModel> configVariantProductModels,
			final List<ProductData> configProductsList)
	{
		for (int i = 0; i < configVariantProductModels.size() && i < configProductsList.size(); i++)
		{
			final ConfigVariantProductModel configVariantProductModel = configVariantProductModels.get(i);
			final ProductData configProduct = configProductsList.get(i);
			if (configVariantProductModel != null)
			{
				final BaseVariantProductModel baseVariantProductModel = (BaseVariantProductModel) configVariantProductModel
						.getBaseProduct();
				setFbcMustSelectFlag(baseVariantProductModel, configProduct);
				setDcaCode(baseVariantProductModel, configProduct);
				setPrimaryImageUrl(baseVariantProductModel, configProduct);
			}
		}
	}

	/**
	 * This method sets the FbcMustSelectOptions flag
	 *
	 * @param baseVariantProductModel
	 *           the base variant model
	 * @param configProduct
	 *           the productdata for config variant
	 */
	private void setFbcMustSelectFlag(final BaseVariantProductModel baseVariantProductModel, final ProductData configProduct)
	{
		if (baseVariantProductModel != null && configProduct != null)
		{
			configProduct.setFbcMustSelectFlag(baseVariantProductModel.getFbcMustSelectFlag());
		}
	}

	/**
	 * This method sets the DCA Code i.e. AlertId
	 *
	 * @param baseVariantProductModel
	 *           the base variant model
	 * @param configProduct
	 *           the productdata for config variant
	 */
	private void setDcaCode(final BaseVariantProductModel baseVariantProductModel, final ProductData configProduct)
	{
		if (baseVariantProductModel != null && configProduct != null)
		{
			configProduct.setBaseProduct(baseVariantProductModel.getCode());
		}
	}

	/**
	 * This method sets the primary image url i.e. sales model image url for the config product
	 *
	 * @param baseVariantProductModel
	 *           the base variant model
	 * @param configProduct
	 *           the productdata for config variant
	 */
	private void setPrimaryImageUrl(final BaseVariantProductModel baseVariantProductModel, final ProductData configProduct)
	{
		if (baseVariantProductModel != null && configProduct != null)
		{
			final ProductModel salesModelProductModel = baseVariantProductModel.getBaseProduct();
			if (salesModelProductModel != null)
			{
				final MediaModel picture = salesModelProductModel.getPicture();
				if (picture != null)
				{
					configProduct.setPrimaryImageUrl(picture.getURL());
				}
			}
		}
	}


}
