/**
 *
 */
package com.cat.bcp.facades.populators;


import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.converters.Populator;


/**
 * @author sankale
 *
 *         This populator is used for populating category models into category data
 *
 */
public class CatCategoryDataPopulator implements Populator<CategoryModel, CategoryData>
{

	@Override
	public void populate(final CategoryModel source, final CategoryData target)
	{
		// Skid Steer Loaders
		target.setSpecCheckProdFamilyRangeId("42");



	}

}
