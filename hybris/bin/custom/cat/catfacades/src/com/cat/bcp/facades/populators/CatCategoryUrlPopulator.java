/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.converters.populator.CategoryUrlPopulator;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;


/**
 * @author avaijapurkar
 *
 */
public class CatCategoryUrlPopulator extends CategoryUrlPopulator
{

	@Resource(name = "categoryUrlConverter")
	private Converter<CategoryModel, CategoryData> categoryUrlConverter;

	@Override
	public void populate(final CategoryModel source, final CategoryData target)
	{
		super.populate(source, target);

		CategoryData categoryData = null;

		List<CategoryData> catDataList = new ArrayList<>();

		for (final CategoryModel catMod : source.getSupercategories())
		{
			categoryData = categoryUrlConverter.convert(catMod);
			catDataList.add(categoryData);

		}
		target.setSuperCategories(catDataList);

		catDataList = new ArrayList<>();
		for (final CategoryModel catMod : source.getAllSubcategories())
		{
			categoryData = categoryUrlConverter.convert(catMod);
			catDataList.add(categoryData);

		}
		sortSubCategories(catDataList);

		target.setSubCategories(catDataList);
	}

	protected void sortSubCategories(final List<CategoryData> categoryDataList)
	{

		categoryDataList.sort((subCat1, subCat2) -> subCat1.getName().compareTo(subCat2.getName()));
	}


}
