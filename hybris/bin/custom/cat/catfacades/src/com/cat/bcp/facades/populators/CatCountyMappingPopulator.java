/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.text.SimpleDateFormat;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.cat.bcp.core.model.CatCountyModel;
import com.cat.facades.user.CatCountyData;


/**
 * @author sagdhingra
 *
 */
public class CatCountyMappingPopulator implements Populator<CatCountyModel, CatCountyData>
{

	@Resource(name = "regionConverter")
	private Converter<RegionModel, RegionData> regionConverter;

	@Resource(name = "countryConverter")
	private Converter<CountryModel, CountryData> countryConverter;

	/**
	 * populate the County Data from CatCountyModel.
	 *
	 * @param source
	 *           the CatCountyModel
	 * @param target
	 *           the CatCountyData
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final CatCountyModel source, final CatCountyData target) throws ConversionException
	{
		final SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-YYYY");
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setCountyName(source.getCountyName());
		target.setSellCountry(countryConverter.convert(source.getSellCountry()));
		target.setLastModifiedTime(sdf.format(source.getModifiedtime()));
		target.setSellState(regionConverter.convert(source.getSellState()));
		target.setId(String.valueOf(source.getPk()));
	}

}
