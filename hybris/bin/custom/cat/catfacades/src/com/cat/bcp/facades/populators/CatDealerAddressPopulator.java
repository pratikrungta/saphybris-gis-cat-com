/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.user.converters.populator.AddressPopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;

import org.apache.commons.lang.StringUtils;


/**
 * @author bidavda
 *
 */
public class CatDealerAddressPopulator extends AddressPopulator
{
	@Override
	public void populate(final AddressModel source, final AddressData target)
	{
		super.populate(source, target);
		if (StringUtils.isNotBlank(source.getLine3()))
		{
			target.setLine3(source.getLine3());
		}
		if (null != source.getOwner() && source.getOwner() instanceof B2BUnitModel)
		{
			target.setShipToCode(((B2BUnitModel) source.getOwner()).getUid());
		}
		else
		{
			target.setShipToCode(source.getSapCustomerID());
		}
		target.setPublicKey(source.getPublicKey());
	}
}
