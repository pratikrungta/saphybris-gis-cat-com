/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.commercefacades.user.converters.populator.AddressReversePopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.lang.StringUtils;


/**
 * @author manjam
 *
 */
public class CatDealerAddressReversePopulator extends AddressReversePopulator
{

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * de.hybris.platform.commercefacades.user.converters.populator.AddressReversePopulator#populate(de.hybris.platform.
	 * commercefacades.user.data.AddressData, de.hybris.platform.core.model.user.AddressModel)
	 */
	@Override
	public void populate(final AddressData addressData, final AddressModel addressModel) throws ConversionException
	{
		super.populate(addressData, addressModel);
		addressModel.setStreetname(addressData.getLine1());
		if (StringUtils.isNotBlank(addressData.getLine3()))
		{
			final String[] data = addressData.getLine3().split("\\s+");
			if (null != data)
			{
				if (data.length > 0 && StringUtils.isNotBlank(data[0]))
				{
					addressModel.setAppartment(data[0]);
				}
				if (data.length > 1 && StringUtils.isNotBlank(data[1]))
				{
					addressModel.setBuilding(data[1]);
				}
			}
		}
	}
}
