/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bcommercefacades.company.converters.populators.B2BUnitPopulator;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;


/**
 * This class has methods to populate the b2bunit data.
 *
 * @author bidavda
 *
 */
public class CatDealerB2BUnitPopulator extends B2BUnitPopulator
{
	/**
	 * populate the B2BUnitData from B2BUnitModel
	 *
	 * @param source
	 * @param target
	 */
	@Override
	public void populate(final B2BUnitModel source, final B2BUnitData target)
	{
		super.populate(source, target);

		target.setDescription(source.getDescription());
	}


}
