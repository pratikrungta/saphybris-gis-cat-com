package com.cat.bcp.facades.populators;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.commercefacades.user.converters.populator.CustomerPopulator;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Required;

import com.cat.bcp.core.constants.CatCoreConstants;


/**
 * This class has methods to populate the customer data.
 *
 */
public class CatDealerCustomerPopulator extends CustomerPopulator
{

	private Converter<AddressModel, AddressData> addressConverter;

	protected Converter<AddressModel, AddressData> getAddressConverter()
	{
		return addressConverter;
	}

	@Required
	public void setAddressConverter(final Converter<AddressModel, AddressData> addressConverter)
	{
		this.addressConverter = addressConverter;
	}

	/**
	 * populate the CustomerData from CustomerModel
	 *
	 * @param source
	 * @param target
	 */
	@Override
	public void populate(final CustomerModel source, final CustomerData target)
	{
		super.populate(source, target);

		if (source instanceof B2BCustomerModel)
		{
			populateEmail(source, target);
			populateUserRole(source, target);
		}
		else
		{
			final AddressModel billingAddress = source.getDefaultPaymentAddress();
			final AddressModel shippingAddress = source.getDefaultShipmentAddress();
			if (null != billingAddress)
			{
				target.setDefaultBillingAddress(getAddressConverter().convert(billingAddress));
			}
			if (null != shippingAddress)
			{
				target.setDefaultShippingAddress(getAddressConverter().convert(shippingAddress));
			}
			if (null != billingAddress && null != shippingAddress && billingAddress.equals(shippingAddress))
			{
				target.setShippingSameAsBilling(Boolean.TRUE);
			}
			else
			{
				target.setShippingSameAsBilling(Boolean.FALSE);
			}
		}
	}

	/**
	 * populate the email from CustomerModel
	 *
	 * @param source
	 * @param target
	 */
	protected void populateEmail(final CustomerModel source, final CustomerData target)
	{
		final String email = ((B2BCustomerModel) source).getEmail();
		if (StringUtils.isNotBlank(email))
		{
			target.setEmail(email);
		}
	}

	/**
	 * populate the user role from CustomerModel
	 *
	 * @param source
	 * @param target
	 */
	protected void populateUserRole(final CustomerModel source, final CustomerData target)
	{
		final Set<PrincipalGroupModel> allGroups = source.getGroups();
		for (final PrincipalGroupModel group : allGroups)
		{
			if (group.getUid().equalsIgnoreCase(CatCoreConstants.INVENTORY_MANAGER_GRP)
					|| group.getUid().equalsIgnoreCase(CatCoreConstants.SALES_PERSON_GRP)
					|| group.getUid().equalsIgnoreCase(CatCoreConstants.INVENTORY_MANAGER_UTV_GRP))
			{
				target.setUserRole(group.getDisplayName());
				target.setUserRoleUid(group.getUid());
			}
		}
	}
}
