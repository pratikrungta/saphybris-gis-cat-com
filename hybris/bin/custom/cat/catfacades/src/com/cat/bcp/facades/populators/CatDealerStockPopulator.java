/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.ordersplitting.model.WarehouseModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.stock.StockService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;

import com.cat.bcp.facades.customer.CatCustomerFacade;


/**
 * This class has methods to populate the stock data for products based on pdc, mfu or dealer warehouse along with the
 * warehouse address.
 *
 * @author bidavda
 *
 */
public class CatDealerStockPopulator implements Populator<ProductModel, List<StockData>>
{
	@Resource(name = "stockService")
	private StockService stockService;

	@Resource(name = "catCustomerFacade")
	private CatCustomerFacade catCustomerFacade;

	@Resource(name = "addressConverter")
	private Converter<AddressModel, AddressData> addressConverter;


	/**
	 * populate the StockData from ProductModel for pdc, mfu and dealer warehouse
	 *
	 * @param productModel
	 * @param catStock
	 */
	@Override
	public void populate(final ProductModel productModel, final List<StockData> catStock)
	{
		final List<WarehouseModel> pdcWarehouses = catCustomerFacade.getPDCWarehouses(productModel);
		final List<WarehouseModel> mfuWarehouses = catCustomerFacade.getMFUWarehouses(productModel);
		final List<WarehouseModel> dealerWarehouses = catCustomerFacade.getCurrentDealerWarehouse(productModel);

		final Map<WarehouseModel, Integer> pdcStockMap = stockService.getAvailability(pdcWarehouses, productModel, null);
		final List<StockData> pdcStock = populateStockData(pdcStockMap, false);
		catStock.addAll(pdcStock);

		final Map<WarehouseModel, Integer> mfuStockMap = stockService.getAvailability(mfuWarehouses, productModel, null);
		final List<StockData> mfuStock = populateStockData(mfuStockMap, false);
		catStock.addAll(mfuStock);

		final Map<WarehouseModel, Integer> dealerStockMap = stockService.getAvailability(dealerWarehouses, productModel, null);
		final List<StockData> dealerStock = populateStockData(dealerStockMap, true);
		catStock.addAll(dealerStock);

	}

	/**
	 * populate the StockData for pdc, mfu and dealer warehouse
	 *
	 * @param stockMap
	 * @param dealerFlag
	 *
	 * @return List of StockData
	 */
	protected List<StockData> populateStockData(final Map<WarehouseModel, Integer> stockMap, final boolean dealerFlag)
	{
		final List<StockData> stockDataList = new ArrayList<>();

		stockMap.forEach((warehouse, stockValue) -> {
			final StockData stock = new StockData();
			stock.setStockLevel(Long.valueOf(stockValue.longValue()));
			stock.setWarehouseName(warehouse != null ? warehouse.getName() : StringUtils.EMPTY);
			stock.setWarehouseType(warehouse != null ? warehouse.getWarehouseType().toString() : StringUtils.EMPTY);
			final Iterator<PointOfServiceModel> posItr = warehouse.getPointsOfService().iterator();
			if (posItr.hasNext() && !dealerFlag){
				final PointOfServiceModel pos = posItr.next();
				final AddressModel address = pos.getAddress();
				if (address != null){
					stock.setWarehouseAddress(populateAddressData(address));
				}
			}
			else if (dealerFlag)
			{
				populateDealerAddress(stock);
			}
			stockDataList.add(stock);
		});

		return stockDataList;

	}

	/**
	 * populate the dealer address for dealer warehouse
	 *
	 * @param stock
	 */
	protected void populateDealerAddress(final StockData stock)
	{
		final B2BCustomerModel currentDealer = catCustomerFacade.getCurrentDealer();
		if (currentDealer != null)
		{
			final B2BUnitModel unit = currentDealer.getDefaultB2BUnit();
			final Collection<AddressModel> unitAddresses = unit.getAddresses();
			unitAddresses.forEach(address -> {
				if (address.getShippingAddress().booleanValue())
				{
					stock.setWarehouseAddress(populateAddressData(address));
				}
			});
		}
	}

	/**
	 * convert AddressModel to AddressData
	 *
	 * @param address
	 *
	 * @return AddressData
	 */
	protected AddressData populateAddressData(final AddressModel address)
	{
		return addressConverter.convert(address);
	}


	/**
	 * @return the stockService
	 */
	public StockService getStockService()
	{
		return stockService;
	}

	/**
	 * @param stockService
	 *           the stockService to set
	 */
	public void setStockService(final StockService stockService)
	{
		this.stockService = stockService;
	}

	/**
	 * @return the catCustomerFacade
	 */
	public CatCustomerFacade getCatCustomerFacade()
	{
		return catCustomerFacade;
	}

	/**
	 * @param catCustomerFacade
	 *           the catCustomerFacade to set
	 */
	public void setCatCustomerFacade(final CatCustomerFacade catCustomerFacade)
	{
		this.catCustomerFacade = catCustomerFacade;
	}

	/**
	 * @return the addressConverter
	 */
	public Converter<AddressModel, AddressData> getAddressConverter()
	{
		return addressConverter;
	}

	/**
	 * @param addressConverter
	 *           the addressConverter to set
	 */
	public void setAddressConverter(final Converter<AddressModel, AddressData> addressConverter)
	{
		this.addressConverter = addressConverter;
	}

}
