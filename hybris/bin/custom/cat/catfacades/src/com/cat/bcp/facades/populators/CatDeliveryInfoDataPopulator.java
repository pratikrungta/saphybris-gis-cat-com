package com.cat.bcp.facades.populators;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.springframework.util.Assert;

import com.cat.bcp.core.model.DeliveryInfoModel;
import com.cat.facades.deliveryinfo.data.DeliveryInfoData;


/**
 * Populator to populate DeliveryInfoData from DeliveryInfoModel
 *
 * @author megverma
 *
 */
public class CatDeliveryInfoDataPopulator implements Populator<DeliveryInfoModel, DeliveryInfoData>
{

	@Resource(name = "b2bUnitConverter")
	private Converter<B2BUnitModel, B2BUnitData> b2bUnitConverter;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final DeliveryInfoModel source, final DeliveryInfoData target) throws ConversionException
	{

		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setConfirmedShippingDate(source.getConfirmedShippingDate());

		target.setEstimatedDeliveryDate(source.getEstimatedDeliveryDate());

		target.setInTransitStatus(source.getInTransitStatus());

		target.setMsoNumber(source.getMsoNumber());

		target.setPurchasedOrderNumber(source.getPurchasedOrderNumber());

		target.setSerialNumber(source.getSerialNumber());

		target.setSourceCode(source.getSourceCode());

	}

}
