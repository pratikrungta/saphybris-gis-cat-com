/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.cat.core.integration.edh.CatEDHInventoryDetailsDataList;
import com.cat.facades.edh.CatEDHInventoryDetailsData;


/*
 * This Populator is to populate the InventoryDetailsDataList from InventoryDetailsData
 * @author ravjonnalagadda
 *
 */
public class CatEDHInventoryDetailsPopulator implements Populator<CatEDHInventoryDetailsData, CatEDHInventoryDetailsDataList>
{

	/**
	 * populate the InventoryDetailsDataList from InventoryDetailsData
	 *
	 * @param source
	 * @param target
	 */
	@Override
	public void populate(final CatEDHInventoryDetailsData source, final CatEDHInventoryDetailsDataList target)
			throws ConversionException
	{
		if (CollectionUtils.isNotEmpty(source.getBaseSalesModelList()))
		{
			target.setBaseSalesModelList(source.getBaseSalesModelList());
		}
		if (CollectionUtils.isNotEmpty(source.getConfigIdList()))
		{
			target.setConfigIdList(source.getConfigIdList());
		}
		if (CollectionUtils.isNotEmpty(source.getPartyIdList()))
		{
			target.setPartyIdList(source.getPartyIdList());
		}
		if (CollectionUtils.isNotEmpty(source.getStageList()))
		{
			target.setStageList(source.getStageList());
		}

		if (StringUtils.isNotBlank(source.getTokenId()))
		{
			target.setTokenId(source.getTokenId());
		}

	}
}
