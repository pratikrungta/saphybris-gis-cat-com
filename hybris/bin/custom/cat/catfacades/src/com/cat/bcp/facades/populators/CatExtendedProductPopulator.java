/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.catalog.model.ProductFeatureModel;
import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.cat.bcp.core.constants.CatCoreConstants;


/**
 *
 * @author asomjal : This populator is for classification attributes. And added to productConverter list.
 */
public class CatExtendedProductPopulator implements Populator<ProductModel, ProductData>
{

	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		final List<ProductFeatureModel> productFeaterModelList = source.getFeatures();
		if (CollectionUtils.isNotEmpty(productFeaterModelList))
		{
			final Iterator iter = productFeaterModelList.iterator();
			while (iter.hasNext())
			{
				final ProductFeatureModel productFeatureModel = (ProductFeatureModel) iter.next();
				populateFeature(productFeatureModel, target);
			}
		}
	}

	/**
	 * This method is to get ClassificationAttributeModel from productFeatureModel
	 *
	 * @param productFeatureModel
	 * @param target
	 */
	private void populateFeature(final ProductFeatureModel productFeatureModel, final ProductData target)
	{
		final ClassAttributeAssignmentModel classAttributeAssignmentModel = productFeatureModel
				.getClassificationAttributeAssignment();
		if (null != classAttributeAssignmentModel)
		{
			final ClassificationAttributeModel classificationAttributeModel = classAttributeAssignmentModel
					.getClassificationAttribute();
			if (null != classificationAttributeModel)
			{
				populateClassificationAttributes(productFeatureModel, target, classificationAttributeModel);
			}
		}
	}

	/**
	 * This method is to populate classification attributes.
	 *
	 * @param productFeatureModel
	 * @param target
	 * @param classificationAttributeModel
	 */
	private void populateClassificationAttributes(final ProductFeatureModel productFeatureModel, final ProductData target,
			final ClassificationAttributeModel classificationAttributeModel)
	{
		switch (classificationAttributeModel.getCode())
		{
			case CatCoreConstants.ENGINE_MODEL:
				target.setFeatureEngineModel(
						null != productFeatureModel.getValue() ? String.valueOf(productFeatureModel.getValue()) : "");
				break;

			case CatCoreConstants.NET_POWER:
				target.setFeatureNetPower(
						null != productFeatureModel.getValue() ? String.valueOf(productFeatureModel.getValue()) : "");
				break;

			case CatCoreConstants.OPERATING_WEIGHT:
				target.setFeatureOperatingWeight(
						null != productFeatureModel.getValue() ? String.valueOf(productFeatureModel.getValue()) : "");
				break;

			case CatCoreConstants.SERIAL_NUMBER_CLASSIFICATION:
				target.setSerialNumber(null != productFeatureModel.getValue() ? String.valueOf(productFeatureModel.getValue()) : "");
				break;
			default:
		}

		populateUTVClassificationAttributes(productFeatureModel, target, classificationAttributeModel);

	}

	/**
	 * @param productFeatureModel
	 * @param target
	 * @param classificationAttributeModel
	 */
	private void populateUTVClassificationAttributes(final ProductFeatureModel productFeatureModel, final ProductData target,
			final ClassificationAttributeModel classificationAttributeModel)
	{
		switch (classificationAttributeModel.getCode())
		{
			case CatCoreConstants.SEAT_CAPACITY:
				target.setSeatCapacity(null != productFeatureModel.getValue() ? String.valueOf(productFeatureModel.getValue()) : "");
				break;
			case CatCoreConstants.FUEL_TYPE:
				target.setFuelType(null != productFeatureModel.getValue() ? String.valueOf(productFeatureModel.getValue()) : "");
				break;
			case CatCoreConstants.MAX_SPEED:
				target.setMaxSpeed(null != productFeatureModel.getValue() ? String.valueOf(productFeatureModel.getValue()) : "");
				break;
			default:
		}
	}
}
