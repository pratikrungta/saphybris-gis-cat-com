/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;
import de.hybris.platform.solrfacetsearch.search.impl.populators.FacetSearchQueryBasicPopulator;

import org.apache.solr.client.solrj.SolrQuery;


/**
 * @author sankale
 *
 */
public class CatFacetSearchQueryBasicPopulator extends FacetSearchQueryBasicPopulator
{

	@Override
	public void populate(final SearchQueryConverterData source, final SolrQuery target)
	{
		String userQuery = source.getSearchQuery().getUserQuery();

		if (userQuery != null)
		{
			target.setQuery(userQuery.replace("$", ":"));
		}
		else
		{
			userQuery = "*:*";
			target.setQuery(userQuery);
		}

	}


}




