/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.solrfacetsearch.search.impl.SearchQueryConverterData;
import de.hybris.platform.solrfacetsearch.search.impl.populators.FacetSearchQueryFacetsPopulator;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.List;

import org.apache.solr.client.solrj.SolrQuery;

import com.cat.bcp.core.constants.CatCoreConstants;


/**
 * @author sagdhingra
 *
 */
public class CatFacetSearchQueryFacetsPopulator extends FacetSearchQueryFacetsPopulator
{
	private static final String CAT_FACET_LIMIT = "cat.facet.limit.default";

	@Override
	public void populate(final SearchQueryConverterData source, final SolrQuery target) throws ConversionException
	{
		super.populate(source, target);
		final List<String> filterQueryList = new ArrayList<>();
		if (target.getFilterQueries() != null)
		{
			for (String filterQuery : target.getFilterQueries())
			{
				if (filterQuery.contains(CatCoreConstants.HOURS_USED) || filterQuery.contains(CatCoreConstants.MANUFACTURING_YEAR)
						|| filterQuery.contains(CatCoreConstants.PRICE))
				{
					filterQuery = filterQuery.replaceAll("\\\\", "");
				}
				filterQueryList.add(filterQuery);
			}
			String[] filterQueryArr = new String[filterQueryList.size()];
			filterQueryArr = filterQueryList.toArray(filterQueryArr);
			target.setFilterQueries(filterQueryArr);
			target.setFacetLimit(Integer.parseInt(Config.getParameter(CAT_FACET_LIMIT)));
		}
	}
}
