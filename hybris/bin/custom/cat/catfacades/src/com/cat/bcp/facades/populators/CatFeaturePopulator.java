/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.catalog.model.classification.ClassAttributeAssignmentModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeUnitModel;
import de.hybris.platform.catalog.model.classification.ClassificationAttributeValueModel;
import de.hybris.platform.classification.features.Feature;
import de.hybris.platform.classification.features.FeatureValue;
import de.hybris.platform.commercefacades.product.converters.populator.FeaturePopulator;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.FeatureUnitData;
import de.hybris.platform.commercefacades.product.data.FeatureValueData;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;

import com.cat.bcp.core.constants.CatCoreConstants;


/**
 * This class is used to populate features/classification attributes for Used Product.
 *
 * @author asomjal
 *
 */
public class CatFeaturePopulator extends FeaturePopulator
{
	/**
	 * This method is used to populate features/classification attributes for Used Product.
	 *
	 * @param source
	 *           Feature Object
	 * @param target
	 *           FeatureData Object
	 */
	@Override
	public void populate(final Feature source, final FeatureData target)
	{
		final ClassAttributeAssignmentModel classAttributeAssignment = source.getClassAttributeAssignment();

		// Create the feature
		target.setCode(source.getCode());
		target.setComparable(Boolean.TRUE.equals(classAttributeAssignment.getComparable()));
		target.setDescription(classAttributeAssignment.getDescription());
		target.setName(source.getName());
		target.setRange(Boolean.TRUE.equals(classAttributeAssignment.getRange()));

		final ClassificationAttributeUnitModel unit = classAttributeAssignment.getUnit();
		if (unit != null)
		{
			final FeatureUnitData featureUnitData = new FeatureUnitData();
			featureUnitData.setName(unit.getName());
			featureUnitData.setSymbol(unit.getSymbol());
			featureUnitData.setUnitType(unit.getUnitType());
			target.setFeatureUnit(featureUnitData);
		}

		// Create the feature data items
		final List<FeatureValueData> featureValueDataList = new ArrayList();
		for (final FeatureValue featureValue : source.getValues())
		{
			final FeatureValueData featureValueData = new FeatureValueData();
			final Object value = featureValue.getValue();
			if (value instanceof ClassificationAttributeValueModel)
			{
				featureValueData.setValue(((ClassificationAttributeValueModel) value).getName());
			}
			else if (NumberUtils.isNumber(String.valueOf(value)))
			{
				if (source.getName().equalsIgnoreCase(CatCoreConstants.PRICE)
						|| source.getName().equalsIgnoreCase(CatCoreConstants.HOURS_USED))
				{
					final double featureVal = Double.parseDouble(String.valueOf(value));
					featureValueData.setValue(String.valueOf(featureVal));
				}
				else
				{
					featureValueData.setValue(String.valueOf(value).replaceAll("\\.0*$", ""));
				}
			}
			else
			{
				featureValueData.setValue(String.valueOf(value));
			}

			featureValueDataList.add(featureValueData);
		}
		target.setFeatureValues(featureValueDataList);
	}
}
