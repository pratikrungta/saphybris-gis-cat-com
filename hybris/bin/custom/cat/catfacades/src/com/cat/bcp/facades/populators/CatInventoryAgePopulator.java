/**
 *
 */
package com.cat.bcp.facades.populators;


import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import org.springframework.util.Assert;

import com.cat.bcp.core.model.InventoryReservationModel;
import com.cat.facades.product.InventoryAgeData;


/**
 * @author prrungta
 *
 */
public class CatInventoryAgePopulator implements Populator<InventoryReservationModel, InventoryAgeData>
{

	private static final String INVENTORY_AGE_UNIT = "Days";

	/**
	 * populate the Inventory Age Data from InventoryReservationModel.
	 *
	 * @param source
	 *           the InventoryReservationModel
	 * @param target
	 *           the InventoryAgeData
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final InventoryReservationModel source, final InventoryAgeData target) throws ConversionException
	{
		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		target.setSerialNumber(source.getSerialNumber());
		String inventoryAge;
		if (null != source.getInventoryAge() && source.getInventoryAge().intValue() != 0)
		{
			inventoryAge = source.getInventoryAge() + " " + INVENTORY_AGE_UNIT;
		}
		else
		{
			inventoryAge = "Not Available";
		}
		target.setInventoryAge(inventoryAge);
	}

}
