/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.converters.Populator;

import org.apache.commons.lang.StringUtils;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.enums.LaneTypeEnum;
import com.cat.facades.order.LaneTypeData;


/**
 * @author bidavda
 *
 */
public class CatLaneTypeDataPopulator implements Populator<LaneTypeEnum, LaneTypeData>
{

	@Override
	public void populate(final LaneTypeEnum source, final LaneTypeData target)
	{
		if (StringUtils.isNotEmpty(source.getCode()))
		{
			target.setCode(source.getCode());
			switch (source.getCode())
			{
				case "LANE1":
					target.setName(CatCoreConstants.LANE1_NAME);
					break;
				case "LANE2":
					target.setName(CatCoreConstants.LANE2_NAME);
					break;
				case "LANE3":
					target.setName(CatCoreConstants.LANE3_NAME);
					break;
				default:
					target.setCode(StringUtils.EMPTY);
					target.setName(StringUtils.EMPTY);
					break;
			}
		}
		else
		{
			target.setCode(StringUtils.EMPTY);
			target.setName(StringUtils.EMPTY);
		}
	}

}
