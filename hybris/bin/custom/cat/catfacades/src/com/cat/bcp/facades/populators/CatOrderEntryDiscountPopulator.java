/**
 *
 */
package com.cat.bcp.facades.populators;


import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;


/**
 * @author ridjain
 *
 *         This populator is used for populating discount value into order entry data for quotes.
 *
 */
public class CatOrderEntryDiscountPopulator implements Populator<AbstractOrderEntryModel, OrderEntryData>
{

	private PriceDataFactory priceDataFactory;

	/**
	 * @return the priceDataFactory
	 */
	public PriceDataFactory getPriceDataFactory()
	{
		return priceDataFactory;
	}

	/**
	 * @param priceDataFactory
	 *           the priceDataFactory to set
	 */
	public void setPriceDataFactory(final PriceDataFactory priceDataFactory)
	{
		this.priceDataFactory = priceDataFactory;
	}

	@Override
	public void populate(final AbstractOrderEntryModel source, final OrderEntryData target)
	{
		if (CollectionUtils.isNotEmpty(source.getDiscountValues()))
		{
			PriceData quoteDiscount = new PriceData();
			final List<DiscountValue> discountValueList = source.getDiscountValues();
			final Optional<DiscountValue> firstDiscountValue = discountValueList.stream().findFirst();
			final DiscountValue discVal = firstDiscountValue.isPresent() ? firstDiscountValue.get() : null;
			if (null != discVal && !discVal.isAbsolute())
			{
				quoteDiscount = getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(discVal.getValue()),
						source.getOrder().getCurrency());
			}
			else if (null != discVal)
			{
				quoteDiscount = getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(discVal.getAppliedValue()),
						source.getOrder().getCurrency());
			}
			target.setDiscount(quoteDiscount);
		}
	}

}
