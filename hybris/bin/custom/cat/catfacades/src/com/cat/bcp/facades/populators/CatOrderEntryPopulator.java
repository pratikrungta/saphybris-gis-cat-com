/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.OrderEntryPopulator;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.session.SessionService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.InventoryReservationModel;
import com.cat.bcp.core.order.CatCommerceQuoteService;
import com.cat.bcp.core.servicelayer.CatProductService;
import com.cat.facades.quote.CatQuoteVETData;


/**
 * This class is used to populate OrderEntryData Object for Cart Page.
 *
 * @author asomjal
 *
 */
public class CatOrderEntryPopulator extends OrderEntryPopulator
{
	@Resource(name = "catCommerceQuoteService")
	private CatCommerceQuoteService catCommerceQuoteService;

	@Resource(name = "sessionService")
	private SessionService sessionService;
	private static final String DATE_FORMAT = "MM-dd-yyyy";

	@Resource(name = "addressConverter")
	private Converter<AddressModel, AddressData> addressConverter;

	@Resource(name = "catProductService")
	private CatProductService catProductService;

	@Resource(name = "cartService")
	private CartService cartService;

	/**
	 * This method is used to populate OrderEntryData Object for Cart Page.
	 *
	 * @param AbstractOrderEntryModel
	 * @param OrderEntryData
	 */
	@Override
	protected void addCommon(final AbstractOrderEntryModel orderEntry, final OrderEntryData entry)
	{
		entry.setMandatoryProduct(orderEntry.getMandatoryProduct());
		entry.setConfigVariantId(orderEntry.getConfigVariantId());
		entry.setEntryNumber(orderEntry.getEntryNumber());
		entry.setQuantity(orderEntry.getQuantity());
		entry.setCompatibleModel(orderEntry.getCompatibleModel());
		addValueAnalysis(orderEntry, entry);
		entry.setQuantityUpdated(orderEntry.getQuantityUpdated());
		final Map reorderPOMap = (Map) sessionService.getAllAttributes().get(CatCoreConstants.REORDER_PO_LIST);
		final boolean isUtv = catProductService.isUtvProduct(orderEntry.getProduct());
		setPoList(isUtv, reorderPOMap, orderEntry, entry);
		if (orderEntry.getOrder() instanceof QuoteModel)
		{
			final List<InventoryReservationModel> inventoryReservationModels = catCommerceQuoteService
					.getReservedSerialNumbers(orderEntry.getProduct(), (QuoteModel) orderEntry.getOrder());
			if (CollectionUtils.isNotEmpty(inventoryReservationModels))
			{
				final String timeStamp = new SimpleDateFormat(DATE_FORMAT)
						.format(inventoryReservationModels.get(0).getReservationEndDate());

				entry.setEndDate(timeStamp);
				final List<String> serialNumbers = new ArrayList<>();
				for (final InventoryReservationModel inventoryReservationModel : inventoryReservationModels)
				{
					serialNumbers.add(inventoryReservationModel.getSerialNumber());
				}
				entry.setSerialNumbers(serialNumbers);
			}
		}
		entry.setShipToAfterDate(orderEntry.getShipToAfter());
		entry.setTruckloadId(orderEntry.getTruckloadId());
		entry.setTruckloadImageUrl(orderEntry.getTruckloadImageUrl());
		if (orderEntry.getConfigurable() == null)
		{
			orderEntry.setConfigurable(Boolean.FALSE);
		}
		entry.setConfigurable(orderEntry.getConfigurable());
		if (orderEntry.getShippingAddress() != null)
		{
			entry.setShippingAddress(addressConverter.convert((orderEntry.getShippingAddress())));
		}
		adjustUpdateable(entry, orderEntry);
	}

	/**
	 * This method sets the PO list
	 *
	 * @param reorderPOMap
	 *           - the PO Map
	 * @param isUtv
	 *           - the UTV flag
	 * @param orderEntry
	 *           - the order entry
	 * @param entry
	 *           - the Order entry Data
	 */
	private void setPoList(final boolean isUtv, final Map reorderPOMap, final AbstractOrderEntryModel orderEntry,
			final OrderEntryData entry)
	{
		if (isUtv)
		{
			if (null != reorderPOMap
					&& reorderPOMap.containsKey(orderEntry.getProduct().getCode() + "__" + orderEntry.getTruckloadId()))
			{
				entry.setPurchaseOrderNumberList(
						(Set) reorderPOMap.get(orderEntry.getProduct().getCode() + "__" + orderEntry.getTruckloadId()));
			}
		}
		else
		{
			if (null != reorderPOMap
					&& reorderPOMap.containsKey(orderEntry.getProduct().getCode() + "__" + orderEntry.getEntryNumber()))
			{
				entry.setPurchaseOrderNumberList(
						(Set) reorderPOMap.get(orderEntry.getProduct().getCode() + "__" + orderEntry.getEntryNumber()));
			}
			else if (null != reorderPOMap && reorderPOMap.containsKey(orderEntry.getProduct().getCode()))
			{
				entry.setPurchaseOrderNumberList((Set) reorderPOMap.get(orderEntry.getProduct().getCode()));
			}
		}
	}

	/**
	 * @param orderEntry
	 *           - order entry model
	 * @param entry
	 *           - order entry data
	 */
	private void addValueAnalysis(final AbstractOrderEntryModel orderEntry, final OrderEntryData entry)
	{
		// VET Integration Starts //
		if (orderEntry.getProduct() instanceof ConfigVariantProductModel)
		{
			final ConfigVariantProductModel configVariantProductModel = (ConfigVariantProductModel) orderEntry.getProduct();
			final BaseVariantProductModel baseVariantProduct = (BaseVariantProductModel) configVariantProductModel.getBaseProduct();
			final ProductModel salesModel = baseVariantProduct.getBaseProduct();
			entry.setBaseVETLink(salesModel.getVETLink());
		}
		if (null != orderEntry.getVETLinkMap() && !orderEntry.getVETLinkMap().isEmpty())
		{
			final Map<String, String> vetLinkMap = orderEntry.getVETLinkMap();

			final List<CatQuoteVETData> catQuoteVETDatas = new ArrayList<>();
			final Iterator<String> vetLinksIterator = vetLinkMap.keySet().iterator();
			final Iterator<String> vetProdIterator = vetLinkMap.values().iterator();

			while (vetLinksIterator.hasNext() && vetProdIterator.hasNext())
			{
				final CatQuoteVETData catQuoteVETData = new CatQuoteVETData();
				catQuoteVETData.setVetLink(vetLinksIterator.next());
				catQuoteVETData.setVetProduct(vetProdIterator.next());
				catQuoteVETDatas.add(catQuoteVETData);

			}
			entry.setValueAnalysis(catQuoteVETDatas);

		}
	}

	/**
	 * @param orderEntry
	 * @param entry
	 */
	@Override
	protected void addProduct(final AbstractOrderEntryModel orderEntry, final OrderEntryData entry)
	{
		super.addProduct(orderEntry, entry);
		if (null != entry.getProduct())
		{
			entry.setSalesModelId(entry.getProduct().getSalesModelId());
			final String baseProductCode = entry.getProduct().getBaseProduct();
			if (StringUtils.isNotEmpty(baseProductCode) && orderEntry.getProduct() instanceof ConfigVariantProductModel)
			{
				entry.getProduct().setBaseProduct(baseProductCode.replaceFirst("(.{3})(?!$)", "$1-"));
			}
		}

	}
}
