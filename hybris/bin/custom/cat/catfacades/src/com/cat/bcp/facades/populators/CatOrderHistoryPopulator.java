/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.OrderHistoryPopulator;
import de.hybris.platform.commercefacades.order.data.OrderHistoryData;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import com.cat.bcp.core.enums.LaneTypeEnum;
import com.cat.bcp.core.model.DeliveryInfoModel;
import com.cat.bcp.core.service.CatDeliveryInfoService;
import com.cat.facades.order.LaneTypeData;


/**
 * @author sparkibanda
 *
 */
public class CatOrderHistoryPopulator extends OrderHistoryPopulator
{
	@Resource(name = "catDeliveryInfoService")
	private CatDeliveryInfoService catDeliveryInfoService;

	@Resource(name = "catLaneTypeConverter")
	private Converter<LaneTypeEnum, LaneTypeData> catLaneTypeConverter;

	@Override
	public void populate(final OrderModel source, final OrderHistoryData target)
	{
		DeliveryInfoModel orderDeliveryInfo = null;

		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");
		target.setCode(source.getCode());
		target.setGuid(source.getGuid());
		target.setPlaced(source.getDate());
		target.setStatus(source.getStatus());

		if (StringUtils.isNotEmpty(source.getMso()))
		{
			target.setMso(source.getMso());
		}
		if (null != source.getOrderPromiseDate())
		{
			target.setOrderPromiseDate(source.getOrderPromiseDate());
		}

		if (null != source.getShipToAfter())
		{
			target.setShipAfterDate(source.getShipToAfter());
		}

		orderDeliveryInfo = catDeliveryInfoService.retrieveDeliveryInfoByMSO(source.getMso());

		if (null != orderDeliveryInfo)
		{

			if (StringUtils.isNotEmpty(orderDeliveryInfo.getSerialNumber()))
			{
				target.setSerialNumber(orderDeliveryInfo.getSerialNumber());
			}

			if (null != orderDeliveryInfo.getEstimatedDeliveryDate())
			{
				target.setEstimatedDelivery(orderDeliveryInfo.getEstimatedDeliveryDate());
			}

			if (StringUtils.isNotEmpty(orderDeliveryInfo.getInTransitStatus()))
			{
				target.setInTransitStatus(orderDeliveryInfo.getInTransitStatus());
			}

		}

		populateLaneTypeData(source, target);

	}

	/**
	 * Populate lane type data.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	private void populateLaneTypeData(final OrderModel source, final OrderHistoryData target)
	{
		if (null != source.getLaneType())
		{
			target.setLaneTypeData(catLaneTypeConverter.convert(source.getLaneType()));
		}
	}

}
