/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.b2b.model.B2BCommentModel;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BCommentData;
import de.hybris.platform.commercefacades.order.converters.populator.OrderPopulator;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import com.cat.bcp.core.enums.LaneTypeEnum;
import com.cat.bcp.core.model.DeliveryInfoModel;
import com.cat.bcp.core.service.CatDeliveryInfoService;
import com.cat.facades.order.LaneTypeData;


/**
 * The Class CatOrderPopulator.
 *
 * @author rrajranjan
 */
public class CatOrderPopulator extends OrderPopulator
{

	@Resource(name = "catDeliveryInfoService")
	private CatDeliveryInfoService catDeliveryInfoService;

	@Resource(name = "catLaneTypeConverter")
	private Converter<LaneTypeEnum, LaneTypeData> catLaneTypeConverter;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void populate(final OrderModel source, final OrderData target)
	{
		DeliveryInfoModel orderDeliveryInfo = null;

		super.populate(source, target);

		if (StringUtils.isNotEmpty(source.getPurchaseOrderNumber()))
		{
			target.setPurchaseOrderNumber(source.getPurchaseOrderNumber());
		}
		if (StringUtils.isNotEmpty(source.getMso()))
		{
			target.setMso(source.getMso());
		}
		if (null != source.getOrderPromiseDate())
		{
			target.setOrderPromiseDate(source.getOrderPromiseDate());
		}


		orderDeliveryInfo = catDeliveryInfoService.retrieveDeliveryInfoByMSO(source.getMso());

		if (null != orderDeliveryInfo)
		{

			if (StringUtils.isNotEmpty(orderDeliveryInfo.getSerialNumber()))
			{
				target.setSerialNumber(orderDeliveryInfo.getSerialNumber());
			}

			if (null != orderDeliveryInfo.getEstimatedDeliveryDate())
			{
				target.setEstimatedDelivery(orderDeliveryInfo.getEstimatedDeliveryDate());
			}

			if (StringUtils.isNotEmpty(orderDeliveryInfo.getInTransitStatus()))
			{
				target.setInTransitStatus(orderDeliveryInfo.getInTransitStatus());
			}

		}

		addComment(source, target);
		addBillingAddress(source, target);
		addEntriesForCat(source, target);
		target.setPurchaseOrderForAccessories(source.getPurchaseOrderNumber());
		target.setCompatibleModel(source.getCompatibleModel());

		populateLaneTypeData(source, target);
	}



	/**
	 * Populate lane type data.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	private void populateLaneTypeData(final OrderModel source, final OrderData target)
	{
		if (null != source.getLaneType())
		{
			target.setLaneTypeData(catLaneTypeConverter.convert(source.getLaneType()));
		}
	}


	/**
	 * Adds the comment.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	private void addComment(final OrderModel source, final OrderData target)
	{
		final List<B2BCommentData> b2bCommentDataList = new ArrayList<>();
		final B2BCommentData b2bCommentData = new B2BCommentData();
		for (final B2BCommentModel b2bcomment : source.getB2bcomments())
		{
			b2bCommentData.setComment(b2bcomment.getComment());
			b2bCommentDataList.add(b2bCommentData);

		}
		target.setB2bCommentData(b2bCommentDataList);
	}

	/**
	 * Adds the billing address.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	private void addBillingAddress(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (source.getPaymentAddress() != null)
		{
			final CCPaymentInfoData ccPaymentInfoData = new CCPaymentInfoData();
			ccPaymentInfoData.setBillingAddress(getAddressConverter().convert(source.getPaymentAddress()));
			target.setPaymentInfo(ccPaymentInfoData);
		}
	}

	/**
	 * This method is used to exclude mandatory products from the DTO.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	private void addEntriesForCat(final AbstractOrderModel source, final AbstractOrderData target)
	{
		if (CollectionUtils.isNotEmpty(source.getEntries()))
		{
			final List<OrderEntryData> orderEntryDataForTarget = new ArrayList();
			final List<OrderEntryData> orderEntryDataList = getOrderEntryConverter().convertAll(source.getEntries());
			for (final OrderEntryData orderEntryData : orderEntryDataList)
			{
				if (BooleanUtils.isNotTrue(orderEntryData.getMandatoryProduct()))
				{
					orderEntryDataForTarget.add(orderEntryData);
				}
			}
			target.setEntries(orderEntryDataForTarget);
		}
	}
}
