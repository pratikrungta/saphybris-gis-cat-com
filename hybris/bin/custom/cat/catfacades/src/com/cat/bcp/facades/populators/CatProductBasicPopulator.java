/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.ProductBasicPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.Collection;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;

import com.cat.bcp.core.enums.ProductConditionEnum;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.SpeccheckInfoModel;


/**
 * @author manjam
 *
 */
public class CatProductBasicPopulator extends ProductBasicPopulator<ProductModel, ProductData>
{


	/**
	 * This method is used to populate product model to product data.
	 *
	 * @param productModel
	 *           the product model
	 * @param productData
	 *           the product data
	 * @throws ConversionException
	 *            the conversion exception
	 */
	@Override
	public void populate(final ProductModel productModel, final ProductData productData) throws ConversionException
	{
		super.populate(productModel, productData);
		if (productModel instanceof BaseVariantProductModel)
		{
			final BaseVariantProductModel baseVariantProductModel = (BaseVariantProductModel) productModel;
			if (baseVariantProductModel.getProductCondition() != null)
			{
				productData.setProductCondition(baseVariantProductModel.getProductCondition());
			}
			else if (productModel instanceof ConfigVariantProductModel)
			{
				productData.setProductCondition(ProductConditionEnum.NEW);
			}
		}
		final Collection<SpeccheckInfoModel> speccheckInfoModelCollection = productModel.getSpeccheckInfo();
		if (CollectionUtils.isNotEmpty(speccheckInfoModelCollection))
		{
			final Optional<SpeccheckInfoModel> speccheckInfo = speccheckInfoModelCollection.stream().findFirst();
			if (speccheckInfo.isPresent())
			{
				productData.setMachineId(speccheckInfo.get().getMachineId());
			}
		}
		if (productModel instanceof ConfigVariantProductModel)
		{
			final ConfigVariantProductModel variantProduct = (ConfigVariantProductModel) productModel;
			if (variantProduct.getBaseProduct() != null)
			{
				productData.setPrimaryOfferingName(
						variantProduct.getBaseProduct() != null ? variantProduct.getBaseProduct().getName() : null);
				productData.setPrimaryOfferingId(variantProduct.getBaseProduct().getCode());
				setReorderableStatus(productData, variantProduct);
			}
		}
		productData.setApprovalStatus(productModel.getApprovalStatus());
		productData.setCreationTime(productModel.getCreationtime());
	}

	/**
	 * @param productData
	 * @param variantProduct
	 *           This Method used to set Re-orderable status for product.
	 */
	private void setReorderableStatus(final ProductData productData, final ConfigVariantProductModel variantProduct)
	{
		if (variantProduct.getBaseProduct() != null && variantProduct.getBaseProduct() instanceof BaseVariantProductModel)
		{
			final BaseVariantProductModel primaryOffering = (BaseVariantProductModel) variantProduct.getBaseProduct();

			if (null != primaryOffering.getBaseProduct())
			{
				productData.setSalesModelId(primaryOffering.getBaseProduct().getCode());
				productData.setSalesModelName(primaryOffering.getBaseProduct().getName());
			}

			if (BooleanUtils.isFalse(primaryOffering.getIsReorderable()))
			{
				productData.setIsReorderable(Boolean.FALSE);
			}
			else if (primaryOffering.getBaseProduct() != null
					&& BooleanUtils.isFalse(primaryOffering.getBaseProduct().getIsReorderable()))
			{
				productData.setIsReorderable(Boolean.FALSE);
			}
			else
			{
				productData.setIsReorderable(Boolean.TRUE);
			}
		}
	}
}
