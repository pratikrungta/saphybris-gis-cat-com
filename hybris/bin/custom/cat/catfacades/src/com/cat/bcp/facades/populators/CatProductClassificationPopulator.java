/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.classification.features.FeatureList;
import de.hybris.platform.commercefacades.product.converters.populator.ProductClassificationPopulator;
import de.hybris.platform.commercefacades.product.data.ClassificationData;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.FeatureValueData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Collection;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.servicelayer.CatProductService;


/**
 * Populate the product data with the products classification data
 */
public class CatProductClassificationPopulator extends ProductClassificationPopulator
{
	private static final String SEAT_CAPACITY = "SEAT_CAPACITY";

	@Resource(name = "catProductService")
	private CatProductService catProductService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		if (source instanceof ConfigVariantProductModel && catProductService.isUtvProduct(source))
		{
			final BaseVariantProductModel baseProduct = (BaseVariantProductModel) ((ConfigVariantProductModel) source)
					.getBaseProduct();
			final FeatureList featureList = getClassificationService().getFeatures(baseProduct);
			if (featureList != null && !featureList.getFeatures().isEmpty())
			{
				getProductFeatureListPopulator().populate(featureList, target);
			}

			target.setSeatCapacity(populateSeatCapacity(target));

		}
		else
		{
			super.populate(source, target);
		}

	}



	/**
	 * This method populates seat capacity.
	 *
	 * @param target
	 *           the target
	 * @return the seatCapacity
	 */
	private String populateSeatCapacity(final ProductData target)
	{
		final Collection<ClassificationData> classifications = target.getClassifications();
		if (CollectionUtils.isNotEmpty(classifications))
		{
			for (final ClassificationData classificationData : classifications)
			{
				final Collection<FeatureData> classificationFeatures = classificationData.getFeatures();

				if (CollectionUtils.isNotEmpty(classificationFeatures))
				{
					return getSeatCapacity(classificationFeatures);
				}
			}
		}
		return StringUtils.EMPTY;

	}


	/**
	 * This method gets the seat capacity.
	 *
	 * @param classificationFeatures
	 *           the classification features
	 * @return the seat capacity
	 */
	private String getSeatCapacity(final Collection<FeatureData> classificationFeatures)
	{
		for (final FeatureData featureData : classificationFeatures)
		{
			if (StringUtils.equals(SEAT_CAPACITY, featureData.getName()))
			{
				final Collection<FeatureValueData> featureValues = featureData.getFeatureValues();
				return CollectionUtils.isNotEmpty(featureValues) ? featureValues.iterator().next().getValue() : StringUtils.EMPTY;
			}
		}
		return StringUtils.EMPTY;
	}
}
