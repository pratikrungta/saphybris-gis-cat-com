/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.ProductDescriptionPopulator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import com.cat.bcp.core.enums.ProductConditionEnum;
import com.cat.bcp.core.model.BaseVariantProductModel;


/**
 * @author sagdhingra
 *
 *         Overriden the class for not populating description of a used product if not present at product level
 *
 */
public class CatProductDescriptionPopulator extends ProductDescriptionPopulator
{

	/**
	 * Method to get the values only for used product and not the base models.
	 *
	 * @param productModel
	 *           product model from which the value is to be fetched
	 * @param attribute
	 *           attribute values to be fetched
	 */
	@Override
	protected Object getProductAttribute(final ProductModel productModel, final String attribute)
	{
		Object value = null;
		if (null != productModel)
		{
			value = getModelService().getAttributeValue(productModel, attribute);
			if (value == null && productModel instanceof VariantProductModel)
			{
				value = checkBaseProductScenario(productModel, attribute);
			}
			return value;
		}
		return value;
	}

	/**
	 * Perform the default action when the product is new
	 *
	 * @param productModel
	 *           product model from which the value is to be fetched
	 * @param attribute
	 *           attribute for which value is to be returned
	 * @param value
	 *           the object which will be returned in case there is no base product
	 * @return Object value that will be returned
	 */
	private Object checkBaseProductScenario(final ProductModel productModel, final String attribute)
	{
		if (productModel instanceof BaseVariantProductModel
				&& !(ProductConditionEnum.USED).equals(((BaseVariantProductModel) productModel).getProductCondition()))
		{
			final ProductModel baseProduct = ((VariantProductModel) productModel).getBaseProduct();
			if (baseProduct != null)
			{
				return getProductAttribute(baseProduct, attribute);
			}
		}
		return null;
	}

}
