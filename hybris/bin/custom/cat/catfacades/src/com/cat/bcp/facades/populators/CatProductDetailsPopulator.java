/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.cmsfacades.data.MediaData;
import de.hybris.platform.commercefacades.product.converters.populator.ProductPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.CatUsedProductFeatureModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.DeliveryInfoModel;
import com.cat.bcp.core.service.CatDeliveryInfoService;
import com.cat.bcp.core.servicelayer.CatProductService;
import com.cat.bcp.facades.product.data.CatProductFeatureData;
import com.cat.facades.deliveryinfo.data.DeliveryInfoData;


/**
 * @author rbairwa
 *
 */
public class CatProductDetailsPopulator extends ProductPopulator
{
	private static final Logger LOG = Logger.getLogger(CatProductDetailsPopulator.class);

	@Resource(name = "catProductService")
	private CatProductService catProductService;

	@Resource(name = "productReferenceConverter")
	private Converter<ProductReferenceModel, ProductReferenceData> productReferenceConverter;

	@Resource(name = "catDeliveryInfoConverter")
	private Converter<DeliveryInfoModel, DeliveryInfoData> catDeliveryInfoConverter;

	@Resource(name = "catDeliveryInfoService")
	private CatDeliveryInfoService catDeliveryInfoService;

	@Resource(name = "catProductFeatureConverter")
	private Converter<CatUsedProductFeatureModel, CatProductFeatureData> catProductFeatureConverter;

	/**
	 * populate the ProductData from ProductModel
	 *
	 * @param source
	 * @param target
	 */
	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		super.populate(source, target);

		if (StringUtils.isNotEmpty(source.getDescription()))
		{
			target.setDescription(source.getDescription().trim());
		}

		/* Check if the Product condition is New. */
		if (source instanceof ConfigVariantProductModel)
		{
			populateConfigVariantProduct(source, target);

			/* Call the method to populate ProductData with DeliveryInfo. */
			populateProductDeliveryInfo(source, target);
		}
		else if (source instanceof BaseVariantProductModel)
		{
			populateBaseVariantProduct(source, target);
		}
	}

	/**
	 * populate the ProductData from ProductModel for BaseVariantProduct
	 *
	 * @param source
	 * @param target
	 */
	private void populateBaseVariantProduct(final ProductModel source, final ProductData target)
	{
		final ProductModel product = ((BaseVariantProductModel) source).getBaseProduct();
		populateBaseVariantProductDetails(target, product);
		if (((BaseVariantProductModel) source).getSpecificationOverview() != null)
		{
			target.setSpecificationOverview(((BaseVariantProductModel) source).getSpecificationOverview());
		}

		CatProductFeatureData catProductFeatureData = null;

		if (CollectionUtils.isNotEmpty(source.getCatUsedProductFeature()))
		{
			final List<CatProductFeatureData> catProductFeatureList = new ArrayList<>();

			for (final CatUsedProductFeatureModel catProductFeature : source.getCatUsedProductFeature())
			{
				catProductFeatureData = catProductFeatureConverter.convert(catProductFeature);

				if (null != catProductFeatureData)
				{
					catProductFeatureList.add(catProductFeatureData);
				}
			}
			target.setCatProductFeatures(catProductFeatureList);
		}



	}

	/**
	 * @param target
	 * @param product
	 */
	private void populateBaseVariantProductDetails(final ProductData target, final ProductModel product)
	{
		if (product.getSupportMaterial() != null)
		{
			target.setSupportMaterialLink(product.getSupportMaterial().getURL());
		}
		if (CollectionUtils.isNotEmpty(product.getCoachingTools()))
		{
			final List<MediaData> mediaDataList = new ArrayList<>();
			for (final MediaModel model : product.getCoachingTools())
			{
				final MediaData mediaData = new MediaData();
				mediaData.setUrl(model.getURL());
				mediaData.setCode(model.getRealFileName());
				mediaDataList.add(mediaData);
			}
			target.setCoachingToolLinks(mediaDataList);
		}
		else
		{
			target.setCoachingToolLinks(Collections.emptyList());
		}
	}

	/**
	 * Method to populate ProductData with DeliveryInfo if the Product condition is New.
	 *
	 * @param source
	 * @param target
	 */
	private void populateProductDeliveryInfo(final ProductModel source, final ProductData target)
	{

		List<DeliveryInfoModel> productDeliveryInfos = null;
		List<DeliveryInfoData> productDeliveryInfoData = null;

		if (null != source)
		{

			productDeliveryInfos = catDeliveryInfoService.getProductDeliveryInfos(source);

			if (CollectionUtils.isNotEmpty(productDeliveryInfos))
			{
				productDeliveryInfoData = new ArrayList<>();

				if (LOG.isDebugEnabled())
				{
					LOG.debug("No. of In Transit Machines : " + productDeliveryInfoData.size());
				}

				getProductDeliveryInfoData(productDeliveryInfos, productDeliveryInfoData);

				target.setInTransitInfos(productDeliveryInfoData);

			}
		}
	}

	private void getProductDeliveryInfoData(List<DeliveryInfoModel> productDeliveryInfos, List<DeliveryInfoData> productDeliveryInfoData) {
		DeliveryInfoData deliveryInfoData;
		for (final DeliveryInfoModel productDeliveryInfo : productDeliveryInfos)
        {
            deliveryInfoData = catDeliveryInfoConverter.convert(productDeliveryInfo);

            if (null != deliveryInfoData)
            {
                productDeliveryInfoData.add(deliveryInfoData);
            }
        }
	}


	/**
	 * populate the ProductData from ProductModel for ConfigVariantProduct
	 *
	 * @param source
	 * @param target
	 */
	public void populateConfigVariantProduct(final ProductModel source, final ProductData target)
	{
		final BaseVariantProductModel baseProduct = (BaseVariantProductModel) ((ConfigVariantProductModel) source).getBaseProduct();
		final ProductModel product = baseProduct.getBaseProduct();
		if (!catProductService.isUtvProduct(source))
		{
			populateBaseVariantProductDetails(target, product);
		}
		else
		{
			if (baseProduct.getSpecificationOverview() != null)
			{
				target.setSpecificationOverview(baseProduct.getSpecificationOverview());
			}

			if (baseProduct.getProductReferences() != null)
			{
				final List<ProductReferenceData> productReferences = new ArrayList();
				for (final ProductReferenceModel ref : baseProduct.getProductReferences())
				{
					productReferences.add(productReferenceConverter.convert(ref));
				}

				target.setProductReferences(productReferences);
			}
			target.setShippingFactor(baseProduct.getShippingFactor());
		}

	}
}




