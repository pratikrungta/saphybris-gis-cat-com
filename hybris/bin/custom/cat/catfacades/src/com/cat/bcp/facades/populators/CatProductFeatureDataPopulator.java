/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import com.cat.bcp.core.model.CatUsedProductFeatureModel;
import com.cat.bcp.facades.product.data.CatProductFeatureData;


/**
 * Populator to populate CatUsedProductFeatureModel from CatProductFeatureModel
 *
 * @author megverma
 *
 */
public class CatProductFeatureDataPopulator implements Populator<CatUsedProductFeatureModel, CatProductFeatureData>
{

	@Resource(name = "b2bUnitConverter")
	private Converter<B2BUnitModel, B2BUnitData> b2bUnitConverter;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */

	@Override
	public void populate(final CatUsedProductFeatureModel source, final CatProductFeatureData target)
	{

		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");

		if (StringUtils.isNotEmpty(source.getUsedProductFeature()))
		{
			target.setProductFeature(source.getUsedProductFeature());
		}

	}


}
