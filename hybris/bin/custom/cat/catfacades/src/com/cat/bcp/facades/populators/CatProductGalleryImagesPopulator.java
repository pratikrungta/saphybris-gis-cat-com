/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.ProductGalleryImagesPopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.media.MediaContainerModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.List;

import javax.annotation.Resource;

import com.cat.bcp.core.enums.ProductConditionEnum;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.facades.utility.CatProductImageHelper;


/**
 *
 * @author sagdhingra
 *
 *         Overriden the class for not populating gallery images of a used product if not present at product level
 */
public class CatProductGalleryImagesPopulator extends ProductGalleryImagesPopulator<ProductModel, ProductData>
{
	@Resource(name = "catProductImageHelper")
	CatProductImageHelper catProductImageHelper;

	/**
	 * Method to get the values only for used product and not the base models.
	 *
	 * @param productModel
	 *           product model from which the value is to be fetched
	 * @param attribute
	 *           attribute values to be fetched
	 */
	@Override
	protected Object getProductAttribute(final ProductModel productModel, final String attribute)
	{
		return catProductImageHelper.getProductAttribute(productModel, attribute);
	}





	/**
	 * Method to collect media containers for a product in case the product is used product it should not fetch the media
	 * container from base product.
	 *
	 * @param productModel
	 *           product model from which the value is to be fetched
	 * @param list
	 *           media container list to be populated
	 */
	@Override
	protected void collectMediaContainers(final ProductModel productModel, final List<MediaContainerModel> list)
	{
		if (null != productModel)
		{
			final List<MediaContainerModel> galleryImages = (List<MediaContainerModel>) getProductAttribute(productModel,
					ProductModel.GALLERYIMAGES);
			if (galleryImages != null)
			{
				populateMediaContainerList(list, galleryImages);
				if (galleryImages.isEmpty() && productModel instanceof VariantProductModel)
				{
					collectMediaContainersForBaseProductScenario(productModel, list);
				}
			}
		}
	}

	/**
	 * Method for populating media container list
	 *
	 * @param list
	 *           list of media container to be populated
	 * @param galleryImages
	 *           for checking whether the image is present in the list or not
	 */
	private void populateMediaContainerList(final List<MediaContainerModel> list, final List<MediaContainerModel> galleryImages)
	{
		for (final MediaContainerModel galleryImage : galleryImages)
		{
			if (!list.contains(galleryImage))
			{
				list.add(galleryImage);
			}
		}
	}

	/**
	 * Collect media containers for base products in case of new products
	 *
	 * @param productModel
	 *           to get the base product
	 * @param list
	 *           media container list to populate the media values for base products if any
	 */
	private void collectMediaContainersForBaseProductScenario(final ProductModel productModel,
			final List<MediaContainerModel> list)
	{
		if (null != productModel && productModel instanceof BaseVariantProductModel
				&& !(ProductConditionEnum.USED).equals(((BaseVariantProductModel) productModel).getProductCondition()))
		{
			collectMediaContainers(((VariantProductModel) productModel).getBaseProduct(), list);
		}
	}
}
