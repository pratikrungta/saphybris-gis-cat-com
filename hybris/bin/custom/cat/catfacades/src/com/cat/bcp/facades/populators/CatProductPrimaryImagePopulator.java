/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.commercefacades.product.converters.populator.ProductPrimaryImagePopulator;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;

import javax.annotation.Resource;

import com.cat.bcp.facades.utility.CatProductImageHelper;


/**
 * Overridden the class for not populating primary images of a used product if not present at product level
 *
 * @author sagdhingra
 *
 */
public class CatProductPrimaryImagePopulator extends ProductPrimaryImagePopulator<ProductModel, ProductData>
{
	@Resource(name = "catProductImageHelper")
	CatProductImageHelper catProductImageHelper;

	/**
	 * Method to get the values only for used product and not the base models.
	 *
	 * @param productModel
	 *           product model from which the value is to be fetched
	 * @param attribute
	 *           attribute values to be fetched
	 */
	@Override
	protected Object getProductAttribute(final ProductModel productModel, final String attribute)
	{
		return catProductImageHelper.getProductAttribute(productModel, attribute);
	}

}
