/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.converters.populator.ProductReferencesPopulator;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.servicelayer.CatProductService;


/**
 * Populates product references (List of {@link ProductReferenceData}) for product ({@link ProductData}).
 */
public class CatProductReferencesPopulator extends ProductReferencesPopulator
{
	@Resource(name = "catProductService")
	private CatProductService catProductService;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	@Resource(name = "baseStoreService")
	private BaseStoreService baseStoreService;

	@Override
	public void populate(final ProductModel source, final ProductData target)
	{
		if (source instanceof ConfigVariantProductModel)
		{
			final List<ProductReferenceData> baseProductReferenceDataList = getBaseProductReferences(source);
			if (catProductService.isUtvProduct(source))
			{
				target.setProductReferences(baseProductReferenceDataList);
			}
			else
			{
				super.populate(source, target);
				target.setBaseProductReferences(baseProductReferenceDataList);
				calculatePriceForConfiguration(source, target);
			}
		}
		else
		{
			super.populate(source, target);
		}

	}

	/**
	 * This method is used to calculate price for Config-variant Product
	 *
	 * @param source
	 *           ProductModel Object
	 * @param target
	 *           ProductData Object
	 */
	private void calculatePriceForConfiguration(final ProductModel source, final ProductData target)
	{
		final ConfigVariantProductModel configVariantProductModel = (ConfigVariantProductModel) source;
		if (configVariantProductModel.getBaseProduct() != null)
		{
			final BaseVariantProductModel baseProduct = (BaseVariantProductModel) configVariantProductModel.getBaseProduct();
			if (baseProduct != null)
			{
				final boolean configurable = baseProduct.getFbcMustSelectFlag() != null
						? baseProduct.getFbcMustSelectFlag().booleanValue() : false;
				if (!configurable)
				{
					calculatePriceForTarget(target, target.getProductReferences());
				}
			}
		}
	}

	/**
	 * This method is used to calculate Price for ConfigVariant Products - (Used on PDP Page)
	 *
	 * @param target
	 *           ProductData Object
	 * @param productReferenceDataList
	 *           List of ProductReferenceData
	 */
	private void calculatePriceForTarget(final ProductData target, final List<ProductReferenceData> productReferenceDataList)
	{
		final PriceData priceData = target.getPrice();

		if (CollectionUtils.isNotEmpty(productReferenceDataList))
		{
			final CurrencyModel currencyIso = baseStoreService.getCurrentBaseStore().getDefaultCurrency();
			double price = priceData == null ? 0 : priceData.getValue().doubleValue();
			for (final ProductReferenceData productReferenceData : productReferenceDataList)
			{
				final ProductData partProducts = productReferenceData.getTarget();
				if (partProducts != null && partProducts.getPrice() != null)
				{
					price = price + partProducts.getPrice().getValue().doubleValue();
				}
			}
			target.setPrice(createPrice(currencyIso.getIsocode(), BigDecimal.valueOf(price)));
		}
	}

	/**
	 * This method is used to get List of Part Variant Products associated with given Configurations.
	 *
	 * @param source
	 *           ProductModel Object
	 * @return List of ProductReferenceData
	 */
	private List<ProductReferenceData> getBaseProductReferences(final ProductModel source)
	{
		final List<ProductReferenceData> productReferences = new ArrayList();
		final BaseVariantProductModel baseProduct = (BaseVariantProductModel) ((ConfigVariantProductModel) source).getBaseProduct();
		if (baseProduct.getProductReferences() != null)
		{
			for (final ProductReferenceModel ref : baseProduct.getProductReferences())
			{
				final ProductReferenceData productRefData = getProductReferenceConverter().convert(ref);
				if (productRefData.getTarget() != null && productRefData.getTarget().getPrice() != null)
				{
					productReferences.add(productRefData);
				}
			}
		}
		return productReferences;
	}

	/**
	 * This method is used to create PriceData Object from given price value.
	 *
	 * @param currencyIso
	 *           currencyIso
	 * @param val
	 *           BigDecimal value
	 * @return PriceData PriceData Object
	 */
	protected PriceData createPrice(final String currencyIso, final BigDecimal val)
	{
		return priceDataFactory.create(PriceDataType.BUY, val, currencyIso);
	}
}
