/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.cat.bcp.core.service.CatCustomerService;


/**
 * Populator for converting quote data to quote model
 *
 * @author sagdhingra
 *
 */
public class CatProductReversePopulator implements Populator<ProductData, ProductModel>
{
	private final Logger log = Logger.getLogger(CatProductReversePopulator.class);
	ModelService modelService;

	UserService userService;

	@Resource(name = "catCustomerService")
	private CatCustomerService catCustomerService;

	@Resource(name = "flexibleSearchService")
	private FlexibleSearchService flexibleSearchService;

	/**
	 * This method converts product data to product model
	 */
	@Override
	public void populate(final ProductData prodData, ProductModel prodModel) throws ConversionException
	{
		prodModel.setCode(prodData.getCode());
		prodModel = flexibleSearchService.getModelByExample(prodModel);
		log.debug("Converted Product " + prodModel.getCode());
	}


	/**
	 * Populate additional info data.
	 *
	 * @param prodData
	 *           the quote data
	 * @param prodModel
	 *           the quote model
	 */

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}


}
