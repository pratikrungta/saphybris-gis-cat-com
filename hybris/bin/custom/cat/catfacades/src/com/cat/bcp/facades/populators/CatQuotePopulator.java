/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.QuotePopulator;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.quote.data.QuoteData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.enums.DiscountType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.promotions.result.PromotionOrderResults;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.DiscountValue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.cat.bcp.core.constants.CatCoreConstants;


/**
 * @author ridjain
 *
 */
public class CatQuotePopulator extends QuotePopulator
{
	@Resource(name = "customerConverter")
	private Converter<CustomerModel, CustomerData> customerConverter;

	@Resource(name = "addressConverter")
	private Converter<AddressModel, AddressData> addressConverter;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void populate(final QuoteModel source, final QuoteData target)
	{
		super.populate(source, target);
		final CustomerModel customer = (CustomerModel) source.getQuoteCustomer();
		if (null != customer)
		{
			final CustomerData customerData = customerConverter.convert(customer);
			target.setQuoteCustomer(customerData);
			target.setCustomerName(customerData.getName());
			target.setCustomerEmail(customer.getCatCustomerEmailId());

		}
		target.setSubmittedToCustomerDate(source.getSubmittedToCustomerDate());
		target.setAcceptedByCustomerDate(source.getAcceptedByCustomerDate());
		target.setQuoteLostReason(source.getQuoteLostReason());
		target.setQuoteComments(source.getQuoteComments());
		if (source.getCsaAdditionalService() != null)
		{
			target.setCsaAdditionalServices(source.getCsaAdditionalService().getDescription());
		}
		if (source.getEppAdditionalService() != null)
		{
			target.setEppAdditionalServices(source.getEppAdditionalService().getDescription());
		}
		target.setQuoteCreatedTime(source.getQuoteCreatedTime());
		if (source.getPaymentAddress() != null)
		{
			target.setPaymentAddress(addressConverter.convert(source.getPaymentAddress()));
		}
		if (source.getDeliveryAddress() != null)
		{
			target.setDeliveryAddress(addressConverter.convert(source.getDeliveryAddress()));
		}
		if (source.getDeliveryAddress() != null && source.getPaymentAddress() != null
				&& source.getDeliveryAddress().equals(source.getPaymentAddress()))
		{
			target.setIsShippingSameAsBilling(Boolean.TRUE);
		}
		else
		{
			target.setIsShippingSameAsBilling(Boolean.FALSE);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void addPromotions(final AbstractOrderModel source, final PromotionOrderResults promoOrderResults,
			final AbstractOrderData prototype)
	{
		final double quoteDiscountsAmount = getQuoteDiscountsAmount(source);
		prototype.setQuoteDiscounts(createPrice(source, Double.valueOf(quoteDiscountsAmount)));

		final Pair<DiscountType, Double> quoteDiscountsTypeAndRate = getQuoteDiscountsTypeAndRate(source);

		DiscountType discountType = DiscountType.ABSOLUTE;//initially $ should be selected on front end
		final List<AbstractOrderEntryModel> abstractOrderEntryList = source.getEntries();
		if (CollectionUtils.isNotEmpty(source.getEntries()))
		{
			final List<DiscountValue> discountList = new ArrayList<>();
			for (final AbstractOrderEntryModel orderEntry : abstractOrderEntryList)
			{
				if (CollectionUtils.isNotEmpty(orderEntry.getDiscountValues()))
				{
					discountList.addAll(orderEntry.getDiscountValues());
				}
			}
			if (CollectionUtils.isNotEmpty(discountList))
			{
				discountType = discountList.get(0).isAbsolute() ? DiscountType.ABSOLUTE : DiscountType.PERCENT;//get the discount type from the first discount entry
			}
		}
		prototype.setQuoteDiscountsType(discountType.getCode());
		prototype.setQuoteDiscountsRate(quoteDiscountsTypeAndRate.getValue());

		if (promoOrderResults != null)
		{
			final double productsDiscountsAmount = getProductsDiscountsAmount(source);
			final double orderDiscountsAmount = getOrderDiscountsAmount(source);

			prototype.setProductDiscounts(createPrice(source, Double.valueOf(productsDiscountsAmount)));
			prototype.setOrderDiscounts(createPrice(source, Double.valueOf(orderDiscountsAmount)));
			prototype.setTotalDiscounts(createPrice(source, Double.valueOf(productsDiscountsAmount + orderDiscountsAmount)));
			prototype.setTotalDiscountsWithQuoteDiscounts(
					createPrice(source, Double.valueOf(productsDiscountsAmount + orderDiscountsAmount + quoteDiscountsAmount)));
			prototype.setAppliedOrderPromotions(getPromotions(promoOrderResults.getAppliedOrderPromotions()));
			prototype.setAppliedProductPromotions(getPromotions(promoOrderResults.getAppliedProductPromotions()));
		}
	}

	/**
	 * This method is used to populate AbstractOrderData entries from AbstractOrderModel.
	 *
	 * @param source
	 *           abstract order model
	 * @param prototype
	 *           abstract order data
	 */
	@Override
	protected void addEntries(final AbstractOrderModel source, final AbstractOrderData prototype)
	{
		final List<OrderEntryData> orderEntryList = setSpecifiedEntriesOnly(
				getOrderEntryConverter().convertAll(source.getEntries()));
		prototype.setEntries(orderEntryList);
	}

	/**
	 * This method is used to calculate Price Data for Config Variant Products.
	 *
	 * @param list
	 *           collection of entries which is to be manipulated
	 * @return OrderEntryData
	 */
	private List<OrderEntryData> setSpecifiedEntriesOnly(final List<OrderEntryData> list)
	{
		final List<OrderEntryData> configVariantList = list.stream()
				.filter(entry -> !entry.getMandatoryProduct().booleanValue() && StringUtils.isBlank(entry.getConfigVariantId()))
				.collect(Collectors.toList());
		final List<OrderEntryData> partVariantList = list.stream().filter(entry -> entry.getMandatoryProduct().booleanValue())
				.collect(Collectors.toList());
		final List<OrderEntryData> additionalProductList = list.stream()
				.filter(entry -> !entry.getMandatoryProduct().booleanValue() && StringUtils.isNotBlank(entry.getConfigVariantId()))
				.collect(Collectors.toList());

		Map<String, List<OrderEntryData>> configVarAdditionalProductMap = null;

		if (CollectionUtils.isNotEmpty(configVariantList))
		{
			if (CollectionUtils.isNotEmpty(additionalProductList))
			{
				configVarAdditionalProductMap = additionalProductList.stream()
						.collect(Collectors.groupingBy(OrderEntryData::getConfigVariantId));
			}

			for (final OrderEntryData configOrderEntryData : configVariantList)
			{
				setConfigVariantPriceData(partVariantList, configOrderEntryData);

				/* Call the method to prepare and set EPP and CSA entries for ConfigVariant */
				prepareConfigVarEppCsaEntries(configOrderEntryData, configVarAdditionalProductMap);

			}
		}
		return configVariantList;
	}

	/**
	 * This method is used to calculate Price Data for Config Variant Products.
	 *
	 * @param partVariantList
	 *           list of partvariants for a config entry
	 * @param configOrderEntryData
	 *           config order entry data
	 */
	private void setConfigVariantPriceData(final List<OrderEntryData> partVariantList, final OrderEntryData configOrderEntryData)
	{
		double entryBasePrice = (configOrderEntryData.getBasePrice() != null)
				? configOrderEntryData.getBasePrice().getValue().doubleValue() : 0;
		double entryTotalPrice = (configOrderEntryData.getTotalPrice() != null)
				? configOrderEntryData.getTotalPrice().getValue().doubleValue() : 0;
		for (final OrderEntryData partOrderEntryData : partVariantList)
		{
			if (partOrderEntryData.getConfigVariantId().equalsIgnoreCase(configOrderEntryData.getProduct().getCode()))
			{
				entryBasePrice = entryBasePrice + ((partOrderEntryData.getBasePrice()) != null
						? partOrderEntryData.getBasePrice().getValue().doubleValue() : 0);
				entryTotalPrice = entryTotalPrice + ((partOrderEntryData.getTotalPrice()) != null
						? partOrderEntryData.getTotalPrice().getValue().doubleValue() : 0);
			}
		}
		configOrderEntryData.setBasePrice(catCreatePrice(configOrderEntryData, Double.valueOf(entryBasePrice)));
		configOrderEntryData.setTotalPrice(catCreatePrice(configOrderEntryData, Double.valueOf(entryTotalPrice)));
	}

	/**
	 * This method is used to create PriceData Object from given price value.
	 *
	 * @param orderEntry
	 *           order entry for which price data is to be called
	 * @param val
	 *           price value
	 * @return PriceData
	 */
	protected PriceData catCreatePrice(final OrderEntryData orderEntry, final Double val)
	{
		return getPriceDataFactory().create(PriceDataType.BUY, BigDecimal.valueOf(val.doubleValue()),
				orderEntry.getBasePrice().getCurrencyIso());
	}

	/**
	 * Method to prepare EPP CSA entries for Config Variant in an OrderEntry
	 *
	 * @param configOrderEntryData
	 * @param configVarAdditionalProductMap
	 */
	private void prepareConfigVarEppCsaEntries(final OrderEntryData configOrderEntryData,
			final Map<String, List<OrderEntryData>> configVarAdditionalProductMap)
	{
		List<OrderEntryData> configVarAdditionalProducts = new ArrayList();

		if (null != configOrderEntryData)
		{
			if (MapUtils.isNotEmpty(configVarAdditionalProductMap))
			{
				/* Get Additional Products for the Config Variant from the map */
				configVarAdditionalProducts.addAll(configVarAdditionalProductMap.get(configOrderEntryData.getProduct().getCode()));
			}

			if (CollectionUtils.isNotEmpty(configVarAdditionalProducts))
			{
				/* Filter and set EPP entries for the Config Variant from the configVarAdditionalProducts list */
				configOrderEntryData.setEppEntries(configVarAdditionalProducts.stream()
						.filter(entry -> entry.getProduct().getCode().equalsIgnoreCase(CatCoreConstants.EPPPRODUCT))
						.collect(Collectors.toList()));

				/* Filter and set CSA entries for the Config Variant from the configVarAdditionalProducts list */
				configOrderEntryData.setCsaEntries(configVarAdditionalProducts.stream()
						.filter(entry -> entry.getProduct().getCode().equalsIgnoreCase(CatCoreConstants.CSAPRODUCT))
						.collect(Collectors.toList()));
			}
		}
	}
}
