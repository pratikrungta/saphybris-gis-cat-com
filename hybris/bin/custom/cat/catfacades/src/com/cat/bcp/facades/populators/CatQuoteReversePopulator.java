/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.commercefacades.quote.data.QuoteData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.enums.QuoteState;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.Date;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.cat.bcp.core.model.CatCsaAdditionalServiceModel;
import com.cat.bcp.core.model.CatEppAdditionalServiceModel;
import com.cat.bcp.core.service.CatCustomerService;


/**
 * Populator for converting quote data to quote model
 *
 * @author sagdhingra
 *
 */
public class CatQuoteReversePopulator implements Populator<QuoteData, QuoteModel>
{
	ModelService modelService;

	UserService userService;

	@Resource(name = "catCustomerService")
	private CatCustomerService catCustomerService;

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final QuoteData quoteData, final QuoteModel quoteModel) throws ConversionException
	{
		quoteModel.setName(quoteData.getName());
		quoteModel.setDescription(quoteData.getDescription());
		quoteModel.setExpirationTime(quoteData.getExpirationTime());

		populateAdditionalInfoData(quoteData, quoteModel);

		//Quote comments
		quoteModel.setQuoteComments(quoteData.getQuoteComments());

		//Updating Quote State depending upon the DSM Approval
		if (null != quoteData.getState())
		{
			quoteModel.setState(quoteData.getState());
		}
		else
		{
			quoteModel.setState(QuoteState.BUYER_OFFER);
			quoteModel.setSubmittedToCustomerDate(new Date());
		}

		if (StringUtils.isNotEmpty(quoteData.getCustomerEmail()))
		{
			final CustomerModel customer = catCustomerService.findUserByCustomerEmail(quoteData.getCustomerEmail());
			quoteModel.setQuoteCustomer(customer);
		}
		if (null != quoteData.getSubmittedToCustomerDate())
		{
			quoteModel.setSubmittedToCustomerDate(quoteData.getSubmittedToCustomerDate());
		}
		if (null != quoteData.getAcceptedByCustomerDate()
				&& (quoteData.getState().equals(QuoteState.BUYER_REJECTED) || quoteData.getState().equals(QuoteState.BUYER_ACCEPTED)))
		{
			quoteModel.setAcceptedByCustomerDate(quoteData.getAcceptedByCustomerDate());
		}
		if (null != quoteData.getQuoteLostReason() && quoteData.getState().equals(QuoteState.BUYER_REJECTED))
		{
			quoteModel.setQuoteLostReason(quoteData.getQuoteLostReason());
		}
	}

	
	/**
	 * Populate additional info data.
	 *
	 * @param quoteData the quote data
	 * @param quoteModel the quote model
	 */
	private void populateAdditionalInfoData(final QuoteData quoteData, final QuoteModel quoteModel)
	{
		// Adding Additional Services
		if (StringUtils.isNotBlank(quoteData.getCsaAdditionalServices()))
		{
			final CatCsaAdditionalServiceModel csaAdditionalServiceModel = getModelService()
					.create(CatCsaAdditionalServiceModel.class);
			csaAdditionalServiceModel.setDescription(quoteData.getCsaAdditionalServices());
			getModelService().save(csaAdditionalServiceModel);
			quoteModel.setCsaAdditionalService(csaAdditionalServiceModel);

		}

		if (StringUtils.isNotBlank(quoteData.getEppAdditionalServices()))
		{
			final CatEppAdditionalServiceModel eppAdditionalServiceModel = getModelService()
					.create(CatEppAdditionalServiceModel.class);
			eppAdditionalServiceModel.setDescription(quoteData.getEppAdditionalServices());
			getModelService().save(eppAdditionalServiceModel);
			quoteModel.setEppAdditionalService(eppAdditionalServiceModel);
		}
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}


}
