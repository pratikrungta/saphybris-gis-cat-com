/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.b2b.services.B2BUnitService;
import de.hybris.platform.catalog.model.CompanyModel;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.converters.populator.SearchResultVariantProductPopulator;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.search.restriction.SearchRestrictionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.enums.LaneTypeEnum;


/**
 * @author avaijapurkar
 *
 */
public class CatSearchResultProductPopulator extends SearchResultVariantProductPopulator
{

	@Resource(name = "b2bUnitService")
	B2BUnitService b2bUnitService;

	@Resource(name = "searchRestrictionService")
	SearchRestrictionService searchRestrictionService;

	@Resource(name = "userService")
	UserService userService;



	@Override
	public void populate(final SearchResultValueData source, final ProductData target)
	{
		super.populate(source, target);
		if (MapUtils.isNotEmpty(source.getValues()))
		{
			if (null != source.getValues().get(CatCoreConstants.AVAILABLE_WAREHOUSES))
			{
				target.setAvailableInWarehouse((List<String>) source.getValues().get(CatCoreConstants.AVAILABLE_WAREHOUSES));
			}
			if (null != source.getValues().get(CatCoreConstants.PRIMARY_IMAGE_URL))
			{
				target.setPrimaryImageUrl(String.valueOf(source.getValues().get(CatCoreConstants.PRIMARY_IMAGE_URL)));
			}
			if (null != source.getValues().get(CatCoreConstants.IS_REORDERABLE))
			{
				target.setIsReorderable((Boolean) source.getValues().get(CatCoreConstants.IS_REORDERABLE));
			}

			if (userService.getAllUserGroupsForUser(userService.getCurrentUser()).stream()
					.anyMatch(group -> !group.getUid().equalsIgnoreCase(CatCoreConstants.SP_USER)))
			{
				setLaneInformationOnProduct(source, target);
			}
			populateProductHierarchy(source, target);
			populateNewClassificationAttributes(source, target);
			populateUsedClassificationAttributes(source, target);
			populateUTVAttributes(source, target);
			populateInTransitFacetAttributes(source, target);
		}
	}


	/**
	 * Sets the lane information on product.
	 *
	 * @param source
	 *           the source
	 * @param target
	 *           the target
	 */
	private void setLaneInformationOnProduct(final SearchResultValueData source, final ProductData target)
	{
		if (null != source.getValues().get(CatCoreConstants.LANE_TYPE))
		{
			final List<String> laneTypeArr = (List<String>) source.getValues().get(CatCoreConstants.LANE_TYPE);
			target.setLane1(Boolean
					.valueOf(laneTypeArr.stream().anyMatch(laneType -> laneType.equalsIgnoreCase(LaneTypeEnum.LANE1.getCode()))));
			target.setLane2(Boolean
					.valueOf(laneTypeArr.stream().anyMatch(laneType -> laneType.equalsIgnoreCase(LaneTypeEnum.LANE2.getCode()))));
			target.setLane3(Boolean
					.valueOf(laneTypeArr.stream().anyMatch(laneType -> laneType.equalsIgnoreCase(LaneTypeEnum.LANE3.getCode()))));
		}
	}




	/**
	 * Method to populate product hierarchy
	 *
	 * @param source
	 * @param target
	 */
	private void populateProductHierarchy(final SearchResultValueData source, final ProductData target)
	{
		target.setPrimaryOfferingId(null != source.getValues().get(CatCoreConstants.PRIMARY_OFFERING_ID)
				? String.valueOf(source.getValues().get(CatCoreConstants.PRIMARY_OFFERING_ID))
				: String.valueOf(""));

		target.setPrimaryOfferingName(null != source.getValues().get(CatCoreConstants.PRIMARY_OFFERING_NAME)
				? String.valueOf(source.getValues().get(CatCoreConstants.PRIMARY_OFFERING_NAME))
				: String.valueOf(""));

		target.setSalesModelId(null != source.getValues().get(CatCoreConstants.SALES_MODEL_ID)
				? String.valueOf(source.getValues().get(CatCoreConstants.SALES_MODEL_ID))
				: String.valueOf(""));

		target.setSalesModelName(null != source.getValues().get(CatCoreConstants.SALES_MODEL_NAME)
				? String.valueOf(source.getValues().get(CatCoreConstants.SALES_MODEL_NAME))
				: String.valueOf(""));

		if (null != source.getValues().get(CatCoreConstants.CATEGORY))
		{
			target.setCategory((List<String>) source.getValues().get(CatCoreConstants.CATEGORY));
		}

		if (null != source.getValues().get(CatCoreConstants.CATEGORY_NAME))
		{
			target.setCategoryName((List<String>) source.getValues().get(CatCoreConstants.CATEGORY_NAME));
		}
	}


	/**
	 * Method to populate used classification attributes
	 *
	 * @param source
	 * @param target
	 */
	private void populateUsedClassificationAttributes(final SearchResultValueData source, final ProductData target)
	{
		//Classification attributed for used products
		target.setBranchLocation(null != source.getValues().get(CatCoreConstants.BRANCH_LOCATION)
				? String.valueOf(source.getValues().get(CatCoreConstants.BRANCH_LOCATION))
				: String.valueOf(""));

		target.setManufacturingYear(null != source.getValues().get(CatCoreConstants.MANUFACTURING_YEAR)
				? String.valueOf(source.getValues().get(CatCoreConstants.MANUFACTURING_YEAR))
				: String.valueOf(""));

		target.setHoursUsed(null != source.getValues().get(CatCoreConstants.HOURS_USED)
				? String.valueOf(source.getValues().get(CatCoreConstants.HOURS_USED))
				: String.valueOf(""));

		target.setPriceClassification(null != source.getValues().get(CatCoreConstants.PRICE)
				? String.valueOf(source.getValues().get(CatCoreConstants.PRICE))
				: String.valueOf(""));

		target.setDealerCertified(null != source.getValues().get(CatCoreConstants.DEALER_CERTIFIED)
				? String.valueOf(source.getValues().get(CatCoreConstants.DEALER_CERTIFIED)).toLowerCase()
				: String.valueOf(""));

		if (null != source.getValues().get(CatCoreConstants.DEALER_CODE))
		{
			final String dealerCode = String.valueOf(source.getValues().get(CatCoreConstants.DEALER_CODE));
			if (StringUtils.isNotEmpty(dealerCode))
			{
				searchRestrictionService.disableSearchRestrictions();
				final CompanyModel b2bUnitModel = b2bUnitService.getUnitForUid(dealerCode);
				searchRestrictionService.enableSearchRestrictions();
				target.setDealerName(null != b2bUnitModel ? b2bUnitModel.getName() : String.valueOf(""));
			}
		}

		target.setSerialNumber(null != source.getValues().get(CatCoreConstants.SERIAL_NUMBER_CLASSIFICATION)
				? String.valueOf(source.getValues().get(CatCoreConstants.SERIAL_NUMBER_CLASSIFICATION))
				: String.valueOf(""));
	}


	/**
	 * Method to populate new classification attributes
	 *
	 * @param source
	 * @param target
	 */
	private void populateNewClassificationAttributes(final SearchResultValueData source, final ProductData target)
	{
		target.setFeatureEngineModel(null != source.getValues().get(CatCoreConstants.ENGINE_MODEL)
				? String.valueOf(source.getValues().get(CatCoreConstants.ENGINE_MODEL))
				: String.valueOf(""));
		target.setFeatureNetPower(null != source.getValues().get(CatCoreConstants.NET_POWER)
				? String.valueOf(source.getValues().get(CatCoreConstants.NET_POWER))
				: String.valueOf(""));
		target.setFeatureOperatingWeight(null != source.getValues().get(CatCoreConstants.OPERATING_WEIGHT)
				? String.valueOf(source.getValues().get(CatCoreConstants.OPERATING_WEIGHT))
				: String.valueOf(""));
	}

	/**
	 * Method to populate utv specific attributes
	 *
	 * @param source
	 * @param target
	 */
	public void populateUTVAttributes(final SearchResultValueData source, final ProductData target)
	{
		target.setFuelType(null != source.getValues().get(CatCoreConstants.FUEL_TYPE)
				? String.valueOf(source.getValues().get(CatCoreConstants.FUEL_TYPE))
				: String.valueOf(""));
		target.setSeatCapacity(null != source.getValues().get(CatCoreConstants.SEAT_CAPACITY)
				? String.valueOf(source.getValues().get(CatCoreConstants.SEAT_CAPACITY))
				: String.valueOf(""));
		target.setMaxSpeed(null != source.getValues().get(CatCoreConstants.MAX_SPEED)
				? String.valueOf(source.getValues().get(CatCoreConstants.MAX_SPEED))
				: String.valueOf(""));
	}

	/**
	 * @param source
	 * @param target
	 */
	private void populateInTransitFacetAttributes(final SearchResultValueData source, final ProductData target)
	{
		if (source.getValues().get(CatCoreConstants.IS_CONFIGURATION_IN_TRANSIT) != null)
		{
			target.setIsConfigurationInTransit((Boolean) source.getValues().get(CatCoreConstants.IS_CONFIGURATION_IN_TRANSIT));
		}
		if (source.getValues().get(CatCoreConstants.STOCK_IN_TRANSIT) != null)
		{
			target.setStockInTransit((Integer) source.getValues().get(CatCoreConstants.STOCK_IN_TRANSIT));
		}

	}
}




