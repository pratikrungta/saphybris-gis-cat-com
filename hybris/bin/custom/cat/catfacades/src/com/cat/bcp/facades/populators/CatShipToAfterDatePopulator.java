/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.cat.bcp.core.model.ShipToAfterDateModel;
import com.cat.facades.product.order.ShipToAfterDateData;


/**
 * @author avaijapurkar
 *
 */
public class CatShipToAfterDatePopulator implements Populator<ShipToAfterDateModel, ShipToAfterDateData>
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final ShipToAfterDateModel source, final ShipToAfterDateData target) throws ConversionException
	{
		target.setShipAfterDate(source.getShipAfterDate());
		target.setShipAfterName(source.getShipAfterName());
		target.setShipFromDate(source.getShipFromDate());
		target.setShipToDate(source.getShipToDate());

	}

}
