/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.address.converters.populator.SingleLineAddressFormatPopulator;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;


/**
 * @author manjam
 *
 */
public class CatSingleLineAddressFormatPopulator extends SingleLineAddressFormatPopulator
{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void populate(final AddressModel addressModel, final StringBuilder addressLine) throws ConversionException
	{
		if (null != addressModel.getOwner() && addressModel.getOwner() instanceof B2BUnitModel)
		{
			addressLine.append(((B2BUnitModel) addressModel.getOwner()).getUid());
			addressLine.append(",");
		}
		super.populate(addressModel, addressLine);

	}
}
