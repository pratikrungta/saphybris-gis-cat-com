/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryTermData;
import de.hybris.platform.converters.Populator;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.facades.customer.CatCustomerFacade;


/**
 * Converter implementation for {@link de.hybris.platform.commercefacades.search.data.SearchQueryData} as source and
 * {@link de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData} as target type.
 */
public class CatSolrSearchQueryDecoderPopulator implements Populator<SearchQueryData, SolrSearchQueryData>
{
	private static final Logger LOG = Logger.getLogger(CatSolrSearchQueryDecoderPopulator.class);

	@Resource(name = "catCustomerFacade")
	private CatCustomerFacade catCustomerFacade;

	@Override
	public void populate(final SearchQueryData source, final SolrSearchQueryData target)
	{
		if (source != null && source.getValue() != null && !source.getValue().isEmpty())
		{
			final String warehouseCode = catCustomerFacade.getDealerWarehouse();
			final String[] split = source.getValue().split(":");

			if (split.length > 0)
			{
				target.setFreeTextSearch(split[0]);
			}
			if (split.length > 1)
			{
				target.setSort(split[split.length - 1]);
			}

			setWareHouseParamValue(split, warehouseCode, target);
		}
	}

	/**
	 * @param split
	 * @param warehouseCode
	 * @param target
	 * @param terms
	 */
	private void setWareHouseParamValue(final String[] split, final String warehouseCode, final SolrSearchQueryData target)
	{
		final List<SolrSearchQueryTermData> terms = new ArrayList<>();
		for (int i = 1; (i + 1) < split.length; i += 2)
		{
			final SolrSearchQueryTermData termData = new SolrSearchQueryTermData();
			termData.setKey(split[i]);
			try
			{
				if (CatCoreConstants.WAREHOUSE_PARAM.contains(split[i]) && "true".equals(split[i + 1]))
				{
					termData.setValue(URLDecoder.decode(split[i + 1].replace("true", warehouseCode), "UTF-8"));
				}
				else
				{
					termData.setValue(URLDecoder.decode(split[i + 1], "UTF-8"));
				}
			}
			catch (final UnsupportedEncodingException e)
			{
				// UTF-8 is supported encoding, so it shouldn't come here
				LOG.error("Solr search query URLdecoding failed.", e);
			}
			terms.add(termData);
		}
		target.setFilterTerms(terms);

	}
}
