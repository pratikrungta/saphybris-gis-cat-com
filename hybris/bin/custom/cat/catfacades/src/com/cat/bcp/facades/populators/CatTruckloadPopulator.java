/**
 *
 */
package com.cat.bcp.facades.populators;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import com.cat.bcp.core.model.TruckloadModel;
import com.cat.bcp.facades.order.TruckloadData;


/**
 * Populates TruckloadData from TruckloadModel
 *
 * @author avaijapurkar
 *
 */
public class CatTruckloadPopulator implements Populator<TruckloadModel, TruckloadData>
{

	/**
	 * Populates TruckloadData from TruckloadModel
	 *
	 */
	@Override
	public void populate(final TruckloadModel source, final TruckloadData target) throws ConversionException
	{
		//
		target.setCode(source.getCode());
		target.setTwoSeaterCapacity(source.getTwoSeaterCapacity());
		target.setFiveSeaterCapacity(source.getFiveSeaterCapacity());
		target.setIsFull(source.getIsFull());
	}

}
