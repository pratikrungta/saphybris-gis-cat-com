/**
 *
 */
package com.cat.bcp.facades.search;

import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.core.model.product.ProductModel;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.cat.bcp.core.enums.CatIndexedTypeEnum;
import com.cat.bcp.core.model.AlertProductModel;
import com.cat.bcp.core.model.RestockAlertModel;



/**
 * @author bidavda
 *
 */
public interface DealerSearchFacade<ITEM>
{
	/**
	 * Method to get the sales models
	 *
	 * @param categoryCode
	 *           sales model is fetched through category code.
	 * @param isUsedFlag
	 *           isUsed flag to select sales model specific to used products
	 * @return List<ProductData>
	 */
	List<ProductData> getSalesModels(String categoryCode, boolean isUsedFlag);

	/**
	 * Method to get the base models
	 *
	 * @param productCode
	 *           base models is fetched through product code.
	 * @return List<ProductData>
	 */
	List<ProductData> getBaseModels(String productCode);


	/**
	 * Method to build the search query.
	 *
	 * @param category
	 *           The category code if present
	 * @param salesModelId
	 *           sales model id if present
	 * @param primaryOfferingId
	 *           primary offering id if present
	 * @param searchQuery
	 *           search query
	 * @param isUsed
	 *           boolean flag for isUsed
	 * @param usedFacetMap
	 *           map for used facets
	 * @param selectedBranchLocation
	 *           branch location which is selected
	 * @param inTransitFlag
	 *           in transit flag
	 * @param laneType
	 *           the lane type
	 * @return String
	 */
	String buildSearchQuery(String category, String salesModelId, String primaryOfferingId, String searchQuery, boolean isUsed,
			Map<String, String> usedFacetMap, String[] selectedBranchLocation, boolean inTransitFlag, String laneType);

	/**
	 * Method to set the sales models
	 *
	 * @param products
	 *           products through which sales model is to be retrieved.
	 * @return List<Map<String, Object>>
	 */
	List<Map<String, Object>> setSalesModels(List<ProductData> products);

	/**
	 * Method to set the base models
	 *
	 * @param products
	 *           products through which base model is to be retrieved.
	 * @return List<Map<String, Object>>
	 */
	List<Map<String, Object>> setBaseModels(List<ProductData> products);

	/**
	 * Method to set the product families
	 *
	 * @param products
	 *           products through which product family is to be retrieved.
	 * @return List<Map<String, Object>>
	 */
	List<Map<String, Object>> setProductFamily(List<ProductData> products);

	/**
	 * Method to build category search query
	 *
	 * @param query
	 *           query which will be formed after evaluation of flags
	 * @return String
	 */
	String buildCategorySearchQuery(String query);

	/**
	 * Method to build category search query
	 *
	 * @param query
	 *           query which will be formed after evaluation of flags
	 * @return String
	 */
	String buildBaseModelSearchQuery(String query);

	/**
	 * Method to build sales model search query
	 *
	 * @param query
	 *           query which will be formed after evaluation of flags
	 * @return String
	 */
	String buildSalesModelSearchQuery(String query);

	/**
	 * Method to get associated config products
	 *
	 * @param alertId
	 *           get alert products based on alert id
	 * @return List<AlertProductModel>
	 */
	List<AlertProductModel> getAssociatedConfigProducts(String alertId);

	/**
	 * Method to get low stock products
	 *
	 * @param defaultB2BUnit
	 *           dealer specific restrictions
	 * @param category
	 *           category code to be fetch the products
	 * @param salesModelId
	 *           fetch product based on sales model id
	 * @return List<RestockAlertModel>
	 */
	List<RestockAlertModel> getLowStockProducts(B2BUnitModel defaultB2BUnit, String category, String salesModelId);



	/**
	 * Method to get the pageable reorder data
	 *
	 * @param alertEntries
	 *           list of alert entries
	 * @param page
	 *           page number
	 * @param numOfProdsPerPage
	 *           number of products per page
	 * @return List<ProductData>
	 */
	List<ProductData> getPageableData(List<RestockAlertModel> alertEntries, int page, int numOfProdsPerPage);

	/**
	 * Method to get the reorder data
	 *
	 * @param alertEntries
	 *           list of alert entries
	 * @param defaultB2BUnit
	 *           dealer specific restrictions
	 * @return List<ProductData>
	 */
	List<ProductData> getReorderProducts(final List<RestockAlertModel> alertEntries, B2BUnitModel defaultB2BUnit);


	/**
	 * Method to get auto complete suggestions
	 *
	 * @param searchQuery
	 *           search query on the basis of which suggestions are to be retrieved
	 * @param indexedType
	 *           suggestions on the basis of indexed type
	 * @return Map<String, Collection<String>>
	 */
	Map<String, Collection<String>> getAutocompleteSuggestions(String searchQuery, CatIndexedTypeEnum indexedType);

	/**
	 * Method to search the product based on the text input provided.
	 *
	 * @param searchState
	 *           SearchState which is used to search
	 * @param pageableData
	 *           pageable data which has page data
	 * @param indexType
	 *           indexed type to filter out solr results
	 * @return ProductSearchPageData
	 */
	ProductSearchPageData<SearchStateData, ITEM> textSearch(SearchStateData searchState, PageableData pageableData,
			final CatIndexedTypeEnum indexType);

	/**
	 * Method to fetch product model by name
	 *
	 * @param productName
	 *           product name for which product is to be retrieved
	 * @return ProductModel
	 */
	ProductModel getProductByName(String productName);

	/**
	 * Method to fetch category model by name
	 *
	 * @param categoryCode
	 *           category name for which category is to be retrieved
	 * @return CategoryModel
	 */
	CategoryModel getCategoryByName(String categoryCode);


	/**
	 * Fetch Low Stock Products Categories based on AlertProduct Data.
	 *
	 * @param b2bUnitModel
	 *           for adding dealer specific restriction
	 * @return List<CategoryData>
	 */
	List<CategoryData> getLowStockProductsCategories(B2BUnitModel b2bUnitModel);

	/**
	 * Method for checking if product has a gateway category attached
	 *
	 * @param productModel
	 *           product for which category is to be checked
	 * @return boolean
	 */
	boolean isGatewayProduct(ProductModel productModel);


}
