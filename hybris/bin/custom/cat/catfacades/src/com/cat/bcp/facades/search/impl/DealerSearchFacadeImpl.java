/**
 *
 */
package com.cat.bcp.facades.search.impl;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.catalog.constants.GeneratedCatalogConstants.Enumerations.ArticleApprovalStatus;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.facetdata.ProductCategorySearchPageData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.resultdata.SearchResultValueData;
import de.hybris.platform.commerceservices.search.solrfacetsearch.data.SolrSearchQueryData;
import de.hybris.platform.commerceservices.threadcontext.ThreadContextService;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.enums.CatIndexedTypeEnum;
import com.cat.bcp.core.enums.LaneTypeEnum;
import com.cat.bcp.core.model.AlertProductModel;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.RestockAlertModel;
import com.cat.bcp.core.servicelayer.CatCategoryService;
import com.cat.bcp.core.servicelayer.DealerSearchService;
import com.cat.bcp.core.servicelayer.impl.CatSolrProductSearchService;
import com.cat.bcp.core.servicelayer.impl.DefaultCatSolrAutoCompleteService;
import com.cat.bcp.facades.customer.CatCustomerFacade;
import com.cat.bcp.facades.search.DealerSearchFacade;


/**
 * @author bidavda
 *
 */
public class DealerSearchFacadeImpl<ITEM extends ProductData> implements DealerSearchFacade<ITEM>
{

	@Resource(name = "categoryService")
	private CatCategoryService categoryService;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "productConverter")
	private Converter<ProductModel, ProductData> productConverter;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "dealerSearchService")
	private DealerSearchService dealerSearchService;

	@Resource(name = "catCustomerFacade")
	private CatCustomerFacade catCustomerFacade;

	@Resource(name = "catSolrAutoCompleteService")
	private DefaultCatSolrAutoCompleteService catSolrAutoCompleteService;

	@Resource(name = "threadContextService")
	private ThreadContextService threadContextService;

	@Resource(name = "catSolrProductSearchService")
	private CatSolrProductSearchService catSolrProductSearchService;


	private Converter<CategoryModel, CategoryData> categoryConverter;


	private Converter<SearchQueryData, SolrSearchQueryData> searchQueryDecoder;
	private Converter<ProductCategorySearchPageData<SolrSearchQueryData, SearchResultValueData, CategoryModel>, ProductCategorySearchPageData<SearchStateData, ITEM, CategoryData>> productCategorySearchPageConverter;

	private static final Logger LOG = Logger.getLogger(DealerSearchFacadeImpl.class);

	private static final String SORT = ":salesname-asc:";

	private static final String UTV_TEXT = "utility";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductData> getSalesModels(final String categoryCode, final boolean isUsed)
	{
		final List<ProductData> salesModels = new ArrayList<>();

		final List<ProductModel> products = dealerSearchService.getSalesModels(categoryCode, isUsed);
		for (final ProductModel productModel : products)
		{
			if (!(productModel instanceof BaseVariantProductModel) && !(productModel instanceof ConfigVariantProductModel))
			{
				final ProductData product = productConverter.convert(productModel);
				salesModels.add(product);
			}
		}
		return sortData(salesModels);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductData> getBaseModels(final String productCode)
	{
		final List<ProductData> baseModels = new ArrayList<>();
		final ProductModel productModel = productService.getProductForCode(productCode);

		final List<BaseVariantProductModel> variants = dealerSearchService.getBaseModels(productModel);

		for (final BaseVariantProductModel primaryOffering : variants)
		{
			final ProductData product = productConverter.convert(primaryOffering);
			baseModels.add(product);
		}
		return sortData(baseModels);
	}

	protected List<ProductData> sortData(final List<ProductData> productDataList)
	{
		productDataList.sort((productData1, productData2) -> productData1.getName().compareTo(productData2.getName()));
		return productDataList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String buildSearchQuery(final String category, final String salesModelId, final String primaryOfferingId,
			final String query, final boolean isUsedFlag, final Map<String, String> usedFacetMap,
			final String[] selectedBranchLocation, final boolean inTransitFlag, final String laneType)
	{
		final StringBuilder searchQuery = new StringBuilder();

		if (StringUtils.isNotEmpty(query))
		{
			searchQuery.append(query);
		}

		if (StringUtils.isNotEmpty(primaryOfferingId))
		{
			searchQuery.append(CatCoreConstants.PRIMARY_OFFERING_NAME_STRING).append('"' + primaryOfferingId + '"');
		}
		else if (StringUtils.isNotEmpty(salesModelId))
		{
			searchQuery.append(CatCoreConstants.SALESMODEL_NAME_STRING).append('"' + salesModelId + '"');
		}
		else if (StringUtils.isNotEmpty(category))
		{
			searchQuery.append(CatCoreConstants.CATEGORY_NAME_STRING).append('"' + category + '"');
		}

		populateUsedAndUtvQuery(category, searchQuery, isUsedFlag, inTransitFlag);


		populateUsedFacetMapQuery(searchQuery, usedFacetMap, selectedBranchLocation);

		if (!isUsedFlag && StringUtils.isNotEmpty(laneType) && !laneType.equalsIgnoreCase(CatCoreConstants.ALL_LANES))
		{
			if (laneType.equalsIgnoreCase(CatCoreConstants.LANE2))
			{
				searchQuery.append(CatCoreConstants.LANE_TYPE_PARAM).append(LaneTypeEnum.LANE2.getCode());
			}
			else if (laneType.equalsIgnoreCase(CatCoreConstants.LANE3))
			{
				searchQuery.append(CatCoreConstants.LANE_TYPE_PARAM).append(LaneTypeEnum.LANE3.getCode());
			}
			else if (laneType.equalsIgnoreCase(CatCoreConstants.LANE1))
			{
				searchQuery.append(CatCoreConstants.LANE_TYPE_PARAM).append(LaneTypeEnum.LANE1.getCode());
			}
		}

		LOG.debug("DefaultCatCustomerFacade solr query:" + searchQuery);

		searchQuery.append(SORT);
		return String.valueOf(searchQuery);

	}


	/**
	 * @param category
	 *           - the category
	 * @param searchQuery
	 *           - search query
	 * @param isUsedFlag
	 *           - is Used flag
	 * @param inTransitFlag
	 *           - in Transit Flag
	 * @param wsFlag
	 *           - warehouse flag
	 * @param pdcFlag
	 *           - pdc plag
	 * @param mfuFlag
	 *           - mfu flag
	 */
	private void populateUsedAndUtvQuery(final String category, final StringBuilder searchQuery, final boolean isUsedFlag,
			final boolean inTransitFlag)
	{
		if ((StringUtils.isNotEmpty(category) && !category.contains(UTV_TEXT)) && !isUsedFlag)
		{
			if (inTransitFlag)
			{
				searchQuery.append(CatCoreConstants.IS_CONFIGURATION_IN_TRANSIT_STRING).append(inTransitFlag);
			}
		}
		else if (isUsedFlag)
		{
			final B2BCustomerModel b2bCustomerModel = (B2BCustomerModel) userService.getCurrentUser();
			searchQuery.append(" AND ");
			searchQuery.append(CatCoreConstants.CATEGORY_DEALER_STRING).append("((")
					.append(b2bCustomerModel.getDefaultB2BUnit().getUid()).append(")^10000").append(" OR ").append("(*)^10)");
		}

	}

	/**
	 * @param searchQuery
	 *           - the search query
	 * @param usedFacetMap
	 *           - used facet map
	 * @param selectedBranchLocation
	 *           - list of selected branches
	 */
	private void populateUsedFacetMapQuery(final StringBuilder searchQuery, final Map<String, String> usedFacetMap,
			final String[] selectedBranchLocation)
	{
		if (MapUtils.isNotEmpty(usedFacetMap))
		{
			if (selectedBranchLocation != null)
			{
				for (final String selectBranch : selectedBranchLocation)
				{
					searchQuery.append(CatCoreConstants.SOLR_BRANCH_LOCATION).append(selectBranch);
				}
			}
			searchQuery.append(buildUsedFacetQuery(usedFacetMap));
		}

	}

	/**
	 * Method for building the used facet query
	 *
	 * @param usedFacetMap
	 * @return String
	 */
	private String buildUsedFacetQuery(final Map<String, String> usedFacetMap)
	{
		final StringBuilder usedFacetQuery = new StringBuilder("");
		usedFacetMap.entrySet().forEach(usedFacet -> usedFacetQuery.append(usedFacet.getKey()).append(usedFacet.getValue()));
		return String.valueOf(usedFacetQuery);
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public String buildCategorySearchQuery(final String query)
	{
		final StringBuilder searchQuery = new StringBuilder();

		if (StringUtils.isNotEmpty(query))
		{
			searchQuery.append(CatCoreConstants.CATEGORY_NAME_STRING).append(query);
		}
		searchQuery.append(CatCoreConstants.SORT);

		return String.valueOf(searchQuery);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String buildSalesModelSearchQuery(final String query)
	{
		final StringBuilder searchQuery = new StringBuilder();

		if (StringUtils.isNotEmpty(query))
		{
			searchQuery.append(CatCoreConstants.SALESMODEL_NAME_STRING).append(query);
		}
		searchQuery.append(CatCoreConstants.SORT);

		return String.valueOf(searchQuery);

	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public String buildBaseModelSearchQuery(final String query)
	{
		final StringBuilder searchQuery = new StringBuilder();

		if (StringUtils.isNotEmpty(query))
		{
			searchQuery.append(CatCoreConstants.PRIMARY_OFFERING_NAME_STRING).append(query);
		}
		searchQuery.append(CatCoreConstants.SORT);

		return String.valueOf(searchQuery);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<RestockAlertModel> getLowStockProducts(final B2BUnitModel b2bUnit, final String category,
			final String salesModelId)
	{
		final List<RestockAlertModel> alertEntries = dealerSearchService.getLowStockProducts(b2bUnit, category, salesModelId);

		final List<RestockAlertModel> newAlertEntries = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(alertEntries))
		{
			for (final RestockAlertModel restockEntry : alertEntries)
			{
				final String salesModelCode = restockEntry.getProduct().getCode();
				final String alertId = salesModelCode + "_" + b2bUnit.getUid();
				final List<AlertProductModel> alertProducts = getAssociatedConfigProducts(alertId);
				if (CollectionUtils.isNotEmpty(alertProducts))
				{
					newAlertEntries.add(restockEntry);
				}
			}
		}
		return newAlertEntries;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Map<String, Object>> setProductFamily(final List<ProductData> products)
	{
		final List<Map<String, Object>> productFamily = new ArrayList<>();

		for (final ProductData product : products)
		{
			final Map<String, Object> productFamilyMap = new HashMap<>();
			productFamilyMap.put(CatCoreConstants.CATEGORY_CODE, filterCategory(product.getCategory()));
			productFamilyMap.put(CatCoreConstants.CATEGORY_NAME_VAL, filterCategory(product.getCategoryName()));
			productFamily.add(productFamilyMap);
		}

		return removeDuplicates(productFamily);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Map<String, Object>> setSalesModels(final List<ProductData> products)
	{
		final List<Map<String, Object>> salesModels = new ArrayList<>();

		for (final ProductData product : products)
		{
			final Map<String, Object> salesModelsMap = new HashMap<>();
			salesModelsMap.put(CatCoreConstants.SALES_MODEL_ID, product.getSalesModelId());
			salesModelsMap.put(CatCoreConstants.SALES_MODEL_NAME, product.getSalesModelName());
			salesModelsMap.put(CatCoreConstants.CATEGORY_CODE, filterCategory(product.getCategory()));
			salesModelsMap.put(CatCoreConstants.CATEGORY_NAME_VAL, filterCategory(product.getCategoryName()));
			salesModels.add(salesModelsMap);
		}

		return removeDuplicates(salesModels);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Map<String, Object>> setBaseModels(final List<ProductData> products)
	{
		final List<Map<String, Object>> baseModels = new ArrayList<>();
		for (final ProductData product : products)
		{
			final Map<String, Object> baseModelsMap = new HashMap<>();
			baseModelsMap.put("primaryOfferingId", product.getPrimaryOfferingId());
			baseModelsMap.put("primaryOfferingName", product.getPrimaryOfferingName());
			baseModelsMap.put("salesModelId", product.getSalesModelId());
			baseModelsMap.put("salesModelName", product.getSalesModelName());
			baseModelsMap.put("categoryCode", filterCategory(product.getCategory()));
			baseModelsMap.put("categoryName", filterCategory(product.getCategoryName()));
			baseModels.add(baseModelsMap);
		}
		return removeDuplicates(baseModels);
	}

	public String filterCategory(final List<String> categoryList)
	{
		final List<String> removeCategories = new ArrayList<>(Arrays.asList("equipment", "1", "Products"));

		categoryList.removeIf(x -> removeCategories.contains(x));

		if (categoryList.isEmpty())
		{
			return "";
		}

		return categoryList.get(0);
	}

	public List<Map<String, Object>> removeDuplicates(final List<Map<String, Object>> autoSuggestResultsMap)
	{
		final List<Map<String, Object>> listWithoutDuplicates = autoSuggestResultsMap.stream().distinct()
				.collect(Collectors.toList());
		//Limit to maximum of 5 results
		if (listWithoutDuplicates.size() > 5)
		{
			return listWithoutDuplicates.subList(0, 5);
		}
		return listWithoutDuplicates;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AlertProductModel> getAssociatedConfigProducts(final String alertId)
	{
		final List<AlertProductModel> alertProducts = dealerSearchService.getAssociatedConfigProducts(alertId);

		//Approved list of products.
		final List<AlertProductModel> newAlertProducts = new ArrayList();
		if (CollectionUtils.isNotEmpty(alertProducts))
		{
			for (final AlertProductModel alertProduct : alertProducts)
			{
				final ProductModel productModel = alertProduct.getProduct();
				if (null != productModel
						&& ArticleApprovalStatus.APPROVED.toString().equalsIgnoreCase(productModel.getApprovalStatus().getCode()))
				{
					newAlertProducts.add(alertProduct);
				}
			}
		}
		return newAlertProducts;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductData> getPageableData(final List<RestockAlertModel> alertEntries, final int page,
			final int numOfProductsPerPage)
	{
		final List<ProductData> productList = new ArrayList();
		final int startIndex = (page * numOfProductsPerPage) - numOfProductsPerPage;
		int i = 0;
		for (final RestockAlertModel restockEntry : alertEntries)
		{

			if (i >= startIndex && i < page * numOfProductsPerPage)
			{


				final ProductData product = productConverter.convert(restockEntry.getProduct());
				product.setMaximumStockLevel(restockEntry.getMaximumStockLevel());
				product.setMinimumStockLevel(restockEntry.getMinimumStockLevel());
				product.setRecommendedStockLevel(restockEntry.getRecommendedStockLevel());
				product.setCurrentStockLevel(restockEntry.getCurrentStockLevel());
				productList.add(product);
				if (i == page * numOfProductsPerPage)
				{
					break;
				}
			}
			i++;
		}
		return productList;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductData> getReorderProducts(final List<RestockAlertModel> alertEntries, final B2BUnitModel defaultB2BUnit)
	{
		final List<ProductData> productList = new ArrayList();
		for (final RestockAlertModel restockEntry : alertEntries)
		{
			final ProductData product = productConverter.convert(restockEntry.getProduct());

			product.setMaximumStockLevel(restockEntry.getMaximumStockLevel());
			product.setMinimumStockLevel(restockEntry.getMinimumStockLevel());
			product.setRecommendedStockLevel(restockEntry.getRecommendedStockLevel());
			product.setCurrentStockLevel(restockEntry.getCurrentStockLevel());

			final List<ProductData> configProducts = getConfigProductsforAlert(restockEntry, defaultB2BUnit);
			product.setConfigProducts(configProducts);

			productList.add(product);
		}
		return productList;
	}

	/**
	 * @param restockEntry
	 * @return List to config product associated with the alert.
	 */
	private List<ProductData> getConfigProductsforAlert(final RestockAlertModel alert, final B2BUnitModel defaultB2BUnit)
	{
		final String salesModelCode = alert.getProduct().getCode();
		final String alertId = salesModelCode + "_" + defaultB2BUnit.getUid();

		final List<AlertProductModel> alertProducts = getAssociatedConfigProducts(alertId);
		final List<ProductData> configProducts = new ArrayList();
		if (CollectionUtils.isNotEmpty(alertProducts))
		{
			for (final AlertProductModel alertProduct : alertProducts)
			{
				final ProductData configProduct = productConverter.convert(alertProduct.getProduct());
				configProducts.add(configProduct);
			}
		}
		return configProducts;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Map<String, Collection<String>> getAutocompleteSuggestions(final String searchQuery,
			final CatIndexedTypeEnum indexedType)
	{
		return threadContextService
				.executeInContext(new ThreadContextService.Executor<Map<String, Collection<String>>, ThreadContextService.Nothing>()
				{
					@Override
					public Map<String, Collection<String>> execute()
					{
						return catSolrAutoCompleteService.getAutocompleteSuggestionsMap(searchQuery, indexedType);
					}
				});
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ProductSearchPageData<SearchStateData, ITEM> textSearch(final SearchStateData searchState,
			final PageableData pageableData, final CatIndexedTypeEnum indexType)
	{
		Assert.notNull(searchState, "SearchStateData must not be null.");

		return threadContextService.executeInContext(
				new ThreadContextService.Executor<ProductSearchPageData<SearchStateData, ITEM>, ThreadContextService.Nothing>()
				{
					@Override
					public ProductSearchPageData<SearchStateData, ITEM> execute()
					{
						return getProductCategorySearchPageConverter().convert(
								catSolrProductSearchService.searchAgain(decodeState(searchState, null), pageableData, indexType));
					}
				});
	}

	protected SolrSearchQueryData decodeState(final SearchStateData searchState, final String categoryCode)
	{
		final SolrSearchQueryData searchQueryData = getSearchQueryDecoder().convert(searchState.getQuery());
		if (categoryCode != null)
		{
			searchQueryData.setCategoryCode(categoryCode);
		}

		return searchQueryData;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ProductModel getProductByName(final String productName)
	{
		return dealerSearchService.getProductByName(productName);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CategoryModel getCategoryByName(final String categoryName)
	{
		return dealerSearchService.getCategoryByName(categoryName);
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isGatewayProduct(final ProductModel productModel)
	{
		final Collection<CategoryModel> categories = productModel.getSupercategories();
		for (final CategoryModel category : categories)
		{
			if (BooleanUtils.isTrue(category.getGatewayCategory()))
			{
				return true;
			}
		}
		return false;
	}


	/**
	 * @return the searchQueryDecoder
	 */
	public Converter<SearchQueryData, SolrSearchQueryData> getSearchQueryDecoder()
	{
		return searchQueryDecoder;
	}

	/**
	 * @param searchQueryDecoder
	 *           the searchQueryDecoder to set
	 */
	public void setSearchQueryDecoder(final Converter<SearchQueryData, SolrSearchQueryData> searchQueryDecoder)
	{
		this.searchQueryDecoder = searchQueryDecoder;
	}

	/**
	 * @return the productCategorySearchPageConverter
	 */
	public Converter<ProductCategorySearchPageData<SolrSearchQueryData, SearchResultValueData, CategoryModel>, ProductCategorySearchPageData<SearchStateData, ITEM, CategoryData>> getProductCategorySearchPageConverter()
	{
		return productCategorySearchPageConverter;
	}

	/**
	 * @param productCategorySearchPageConverter
	 *           the productCategorySearchPageConverter to set
	 */
	public void setProductCategorySearchPageConverter(
			final Converter<ProductCategorySearchPageData<SolrSearchQueryData, SearchResultValueData, CategoryModel>, ProductCategorySearchPageData<SearchStateData, ITEM, CategoryData>> productCategorySearchPageConverter)
	{
		this.productCategorySearchPageConverter = productCategorySearchPageConverter;
	}


	/**
	 * Fetch Low Stock Products Categories based on AlertProduct Data.
	 *
	 * @return List<CategoryData>
	 */
	@Override
	public List<CategoryData> getLowStockProductsCategories(final B2BUnitModel b2bUnitModel)
	{
		return this.categoryConverter.convertAll(categoryService.getLowStockProductsCategories(b2bUnitModel));


	}

	/**
	 * @return the categoryConverter
	 */
	public Converter<CategoryModel, CategoryData> getCategoryConverter()
	{
		return categoryConverter;
	}

	/**
	 * @param categoryConverter
	 *           the categoryConverter to set
	 */
	public void setCategoryConverter(final Converter<CategoryModel, CategoryData> categoryConverter)
	{
		this.categoryConverter = categoryConverter;
	}


}
