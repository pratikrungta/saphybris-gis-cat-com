/**
 *
 */
package com.cat.bcp.facades.speccheck;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.Collection;
import java.util.List;

import com.cat.bcp.core.exception.CatSpeccheckException;


/**
 * @author asomjal
 *
 */
public interface CatSpeccheckFacade
{

	/**
	 * This method is used to fetch category data for 'equipment' category This data is used on the landing page to show
	 * all the families(sub categories of equipment category) in the drop down.
	 *
	 * @param category
	 *           Category Model
	 * @return Collection of Category Data
	 */
	Collection<CategoryData> getCategoryData(CategoryModel category);

	/**
	 * When user clicks on compare button on the comparison page this handler is used to return the comparison data from
	 * Speccheck Handler 6. Also we can use this to get results from speccheck Handler 2,5.
	 *
	 * @param mids
	 *           machine Ids
	 * @param fid
	 *           FID
	 * @param rid
	 *           region Id
	 * @param handlerNumber
	 *           Handler Number
	 * @param uom
	 *           Unit Of Measurement
	 * @return speccheckResponse the comparison data from Speccheck Handler 6
	 * @throws CatSpeccheckException
	 *            CatSpeccheck Exception
	 */
	Object getSpeccheckResponse(final String mids, final String fid, final String rid, final String handlerNumber, String uom)
			throws CatSpeccheckException;

	/**
	 * This method is used to get all the competitor products for given sales Model. we hit Speccheck Handler 5 twice to
	 * get competitor products first with Caterpillar as manufacture and second with all manufacturer.
	 *
	 * @param salesModel
	 *           Sales Model
	 * @return JSON Response from SpeccheckHandler
	 * @throws CatSpeccheckException
	 *            CatSpeccheck Exception
	 */
	Object getCompetitorProducts(final String salesModel) throws CatSpeccheckException;

	/**
	 * This method is called on load of 'Compare Products' Page This is used to get category and media data for the
	 * selected sales model.
	 *
	 * @param salesModel
	 *           Sales Model
	 * @return competitorProducts returns productData from productFacade.
	 */
	ProductData getProductData(final String salesModel);

	/**
	 * This method is used to extract first non empty media URL for the salesModel selected on PLP page.
	 *
	 * @param productData
	 *           Product Data Object
	 * @return media URL for the salesModel selected on PLP page
	 */
	String getImageURL(ProductData productData);

	/**
	 * This method returns Sales Models for comparison against selected Category
	 *
	 * @param category
	 *           Category Model
	 * @return Returns Sales Models for comparison against selected Category
	 */
	List<ProductData> getSalesModelForCompare(CategoryModel category);


	/**
	 * This method returns Sales Models for comparison against selected Category
	 *
	 * @param category
	 *           Category Model
	 * @param salesModel
	 *           Sales Model
	 * @return JSON Object
	 */
	Object getUTVProductsForCompare(CategoryModel category, String salesModel);

	/**
	 * This method returns product specification data for comparison against selected sales models for UTV.
	 *
	 * @param salesModels
	 *           sales Model
	 * @param uom
	 *           Unit of measurement
	 * @return JSON Object
	 */
	Object getProductSpecs(String salesModels, String uom);
}
