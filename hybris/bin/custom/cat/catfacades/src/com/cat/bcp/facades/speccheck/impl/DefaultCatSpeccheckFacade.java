/**
 *
 */
package com.cat.bcp.facades.speccheck.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.util.Config;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.http.ResponseEntity;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.exception.CatSpeccheckException;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.SpeccheckInfoModel;
import com.cat.bcp.core.servicelayer.CatSpeccheckService;
import com.cat.bcp.facades.customer.CatCustomerFacade;
import com.cat.bcp.facades.speccheck.CatSpeccheckFacade;


/**
 * @author asomjal This class is used for Speccheck integration.
 *
 */
public class DefaultCatSpeccheckFacade implements CatSpeccheckFacade
{

	private static final Logger LOG = Logger.getLogger(DefaultCatSpeccheckFacade.class);

	private static final String UNIT_US = "_us";
	private static final String UNIT_METRIC = "_metric";

	private static final String ERROR = "Error occured during fetching product specifications";

	@Resource(name = "catSpeccheckService")
	private CatSpeccheckService catSpeccheckService;

	@Resource(name = "catCategoryUrlConverter")
	private Converter<CategoryModel, CategoryData> catCategoryUrlConverter;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "productConverter")
	private Converter<ProductModel, ProductData> productConverter;

	@Resource(name = "productVariantFacade")
	private ProductFacade productFacade;

	@Resource(name = "catCustomerFacade")
	private CatCustomerFacade catCustomerFacade;

	/**
	 * This method is used to extract first non empty media URL for the salesModel selected on PLP page.
	 */
	@Override
	public String getImageURL(final ProductData productData)
	{
		final Collection<ImageData> imageDataCollection = productData.getImages();
		if (CollectionUtils.isNotEmpty(imageDataCollection))
		{
			final Iterator iter = imageDataCollection.iterator();
			while (iter.hasNext())
			{
				return ((ImageData) iter.next()).getUrl();
			}
		}
		return "";
	}

	/**
	 * This method is called on load of 'Compare Products' Page This is used to get category and media data for the
	 * selected sales model.
	 */
	@Override
	public ProductData getProductData(final String salesModel)
	{
		final List<ProductOption> extraOptions = Arrays.asList(ProductOption.BASIC, ProductOption.GALLERY,
				ProductOption.CATEGORIES);
		return productFacade.getProductForCodeAndOptions(salesModel, extraOptions);
	}

	/**
	 * This method is used to fetch category data for 'equipment' category This data is used on the landing page to show
	 * all the families(sub categories of equipment category) in the drop down.
	 */
	@Override
	public Collection<CategoryData> getCategoryData(final CategoryModel category)
	{
		final String userType = catCustomerFacade.getUserType();

		final Collection<CategoryModel> categoryModelCollection = category.getAllSubcategories();

		if (CatCoreConstants.UTV.equalsIgnoreCase(userType))
		{
			return catCategoryUrlConverter.convertAll(categoryModelCollection);
		}
		else
		{
			List<CategoryModel> categoryList = new ArrayList<>();
			if (CollectionUtils.isNotEmpty(categoryModelCollection))
			{
				categoryList = categoryModelCollection.stream()
						.filter(categoryModel -> BooleanUtils.isFalse(categoryModel.getGatewayCategory())).collect(Collectors.toList());
			}
			return catCategoryUrlConverter.convertAll(categoryList);
		}
	}

	/**
	 * When user clicks on compare button on the comparison page this handler is used to return the comparison data from
	 * Speccheck Handler 6. Also we can use this to get results from speccheck Handler 2,5.
	 *
	 * @param mids
	 * @param fid
	 * @param rid
	 * @param handler
	 * @return JSON response from speccheck
	 */
	@Override
	public Object getSpeccheckResponse(final String mids, final String fid, final String rid, final String handler,
			final String uom) throws CatSpeccheckException
	{
		LOG.debug("mids - " + mids);
		LOG.debug("rid - " + rid);
		LOG.debug("handlerNumber - " + handler);
		final String requestURIConfig = Config.getParameter(CatCoreConstants.SPECCHECK_REQUESTURI);
		LOG.debug("requestURI - " + requestURIConfig);
		final StringBuilder requestURI = new StringBuilder(requestURIConfig);
		if ((CatCoreConstants.SPECCHECK_RANGESHANDLER).equalsIgnoreCase(handler))
		{
			requestURI.append(CatCoreConstants.SPECCHECK_RANGESHANDLERURI).append(CatCoreConstants.SPECCHECK_FID).append(fid);
		}
		else if ((CatCoreConstants.SPECCHECK_MACHINESHANDLER).equalsIgnoreCase(handler))
		{
			requestURI.append(CatCoreConstants.SPECCHECK_MACHINESHANDLERURI).append(CatCoreConstants.SPECCHECK_RID).append(rid);
		}
		else if ((CatCoreConstants.SPECCHECK_COMPAREHANDLER).equalsIgnoreCase(handler))
		{
			requestURI.append(CatCoreConstants.SPECCHECK_COMPAREHANDLERURI).append(CatCoreConstants.SPECCHECK_MIDS).append(mids)
					.append(CatCoreConstants.SPECCHECK_UOM).append(uom).append(CatCoreConstants.SPECCHECK_RGIDS)
					.append(Config.getParameter(CatCoreConstants.SPECCHECK_RGIDSFORCOMPARE));
			if (Boolean.parseBoolean(Config.getParameter(CatCoreConstants.SPECCHECK_GETCOMPAREMEDIAENABLED)))
			{
				requestURI.append(CatCoreConstants.SPECCHECK_MED);
			}
		}
		else
		{
			throw new CatSpeccheckException("Please provide handler in the request param");
		}
		ResponseEntity<Object> speccheckResponse = null;
		try
		{
			speccheckResponse = catSpeccheckService.callSpeccheckAPI(requestURI.toString());
		}
		catch (final Exception e)
		{
			LOG.error(CatCoreConstants.SPECCHECK_ERRORMESSAGE + e.getMessage());
			throw new CatSpeccheckException(CatCoreConstants.SPECCHECK_ERRORMESSAGE, e);
		}
		this.logResponseFromSpecCheck(speccheckResponse);
		Object obj = null;
		if (speccheckResponse != null)
		{
			obj = speccheckResponse.getBody();
		}
		return obj;
	}

	/**
	 * This method is used to get all the competitor products for given sales Model. we hit Speccheck Handler 5 twice to
	 * get competitor products first with Caterpillar as manufacture and second with all manufacturer.
	 *
	 * @return JSON response
	 */
	@Override
	public Object getCompetitorProducts(final String salesModel) throws CatSpeccheckException
	{
		final String manufactureId = CatCoreConstants.SPECCHECK_MANUFACTURERID;
		final String requestURI = Config.getParameter(CatCoreConstants.SPECCHECK_REQUESTURI);
		Object response = null;
		final ProductModel productModel = productService.getProductForCode(salesModel);
		final Collection<SpeccheckInfoModel> speccheckInfoModelCollection = productModel.getSpeccheckInfo();
		if (CollectionUtils.isNotEmpty(speccheckInfoModelCollection))
		{
			final Iterator iter = speccheckInfoModelCollection.iterator();
			while (iter.hasNext())
			{
				final SpeccheckInfoModel speccheckInfoModel = (SpeccheckInfoModel) iter.next();
				final String machineId = speccheckInfoModel.getMachineId();
				final String rangeId = speccheckInfoModel.getRangeId();
				final String tierId = speccheckInfoModel.getTierId();
				LOG.debug("Machine Id - " + machineId + " - Range Id  - " + rangeId + " - Tier Id" + tierId);
				final String catCompetitorJSON = getCatCompetitors(requestURI, machineId, rangeId, manufactureId, tierId);
				final String allCompetitorJSON = getAllCompetitors(requestURI, machineId, rangeId, tierId);

				if (StringUtils.isNotBlank(allCompetitorJSON) && StringUtils.isNotBlank(catCompetitorJSON))
				{
					final ArrayList<Map<String, Object>> responseList = addCatCompetitorToAllCompetitor(catCompetitorJSON,
							allCompetitorJSON);
					response = getJSONResponseForCompetitors(responseList);
				}
				else if (StringUtils.isNotBlank(allCompetitorJSON) && StringUtils.isBlank(catCompetitorJSON))
				{
					response = convertResponseToJSONCompetitor(allCompetitorJSON);
				}
				else if (StringUtils.isBlank(allCompetitorJSON) && StringUtils.isNotBlank(catCompetitorJSON))
				{
					response = convertResponseToJSONCompetitor(catCompetitorJSON);
				}
				else
				{
					throw new CatSpeccheckException("JSON response is blank for all the manufacturers");
				}
			}
		}
		return response;
	}

	/**
	 * This method is used to get JSON Object from competitorJSON
	 *
	 * @param response
	 * @param allCompetitorJSON
	 * @return
	 * @throws CatSpeccheckException
	 */
	private Object convertResponseToJSONCompetitor(final String competitorJSON) throws CatSpeccheckException
	{
		Object obj;
		final ObjectMapper mapper = new ObjectMapper();
		ArrayList<Map<String, Object>> responseList;
		try
		{
			responseList = mapper.readValue(competitorJSON, new TypeReference<ArrayList<Map<String, Object>>>()
			{/* not implemented */});

			LOG.debug("response List ******************************- \n" + responseList);
			final ObjectMapper objectMapper = new ObjectMapper();

			final ObjectWriter ow = objectMapper.writer().withDefaultPrettyPrinter();
			obj = objectMapper.readValue(String.valueOf(ow.writeValueAsString(responseList)), Object[].class);
		}
		catch (final Exception e)
		{
			LOG.error("Error in mapping response to JSON************" + e.getMessage());
			throw new CatSpeccheckException("Error in mapping response to JSON", e);
		}
		return obj;
	}

	/**
	 * This method is used to get response Object from sorted list of Competitor Products.
	 *
	 * @param response
	 * @param responseList
	 * @return
	 * @throws CatSpeccheckException
	 */
	private Object getJSONResponseForCompetitors(final ArrayList<Map<String, Object>> responseList) throws CatSpeccheckException
	{
		Object obj;
		try
		{
			final ObjectMapper objectMapper = new ObjectMapper();
			final ObjectWriter ow = objectMapper.writer().withDefaultPrettyPrinter();
			obj = objectMapper.readValue(String.valueOf(ow.writeValueAsString(responseList)), Object[].class);
		}
		catch (final Exception e)
		{
			LOG.error("Error in mapping response to JSON************" + e.getMessage());
			throw new CatSpeccheckException("Error in mapping response to JSON", e);
		}
		return obj;
	}

	/**
	 * This method is called when JSON response of competitors for given sales model is not blank. Also this method is
	 * used to prioritize Competitors with manufacturerID:5 i.e. Caterpillar at the top. This method concatenates both
	 * the JSON responses by removing the duplicates.
	 *
	 * @param catCompetitorJSON
	 * @param allCompetitorJSON
	 * @return
	 */
	private ArrayList<Map<String, Object>> addCatCompetitorToAllCompetitor(final String catCompetitorJSON,
			final String allCompetitorJSON) throws CatSpeccheckException
	{
		ArrayList<Map<String, Object>> allCompetitorList;
		ArrayList<Map<String, Object>> catCompetitorList;
		final ObjectMapper mapper = new ObjectMapper();
		try
		{
			allCompetitorList = mapper.readValue(allCompetitorJSON, new TypeReference<ArrayList<Map<String, Object>>>()
			{/* not implemented */});
			catCompetitorList = mapper.readValue(catCompetitorJSON, new TypeReference<ArrayList<Map<String, Object>>>()
			{/* not implemented */});
		}
		catch (final Exception e)
		{
			LOG.error("Error in converting JSON response to List facade Layer  - ObjectMapper" + e.getMessage());
			throw new CatSpeccheckException("Error in converting JSON response to List facade Layer- ObjectMapper", e);
		}
		final ArrayList<Map<String, Object>> finalCompetitorList = new ArrayList();
		finalCompetitorList.addAll(catCompetitorList);
		if (CollectionUtils.isNotEmpty(allCompetitorList))
		{
			for (final Map<String, Object> allCompetitor : allCompetitorList)
			{
				final String machineIdValue = String.valueOf(allCompetitor.get("Machine_ID"));
				if (!catCompetitorJSON.contains(machineIdValue))
				{
					finalCompetitorList.add(allCompetitor);
				}
			}
		}
		return finalCompetitorList;
	}

	/**
	 *
	 * This method returns competitors with 'Caterpillar' manufacturer for given sales model.
	 *
	 * @param requestURI
	 * @param response
	 * @param machineId
	 * @param rangeId
	 * @param manufacturerId
	 * @return
	 */
	private String getCatCompetitors(final String requestURI, final String machineId, final String rangeId,
			final String manufacturerId, final String tierId) throws CatSpeccheckException
	{
		String jsonResponse = null;
		//call Competitor API to get List of Competitor for given Sales Model - speccheck Handler No:5
		final String competitorURI = requestURI + CatCoreConstants.SPECCHECK_MACHINESHANDLERURI + CatCoreConstants.SPECCHECK_RID
				+ rangeId + CatCoreConstants.SPECCHECK_MID + machineId + CatCoreConstants.SPECCHECK_MANIDS + manufacturerId
				+ CatCoreConstants.SPECCHECK_LIMIT + Config.getParameter(CatCoreConstants.SPECCHECK_LIMITFORCATCOMP)
				+ CatCoreConstants.SPECCHECK_TIDS + tierId + CatCoreConstants.SPECCHECK_RGIDS
				+ Config.getParameter(CatCoreConstants.SPECCHECK_RGIDSFORCOMPARE);
		try
		{
			jsonResponse = catSpeccheckService.callSpeccheckAPIForCompetitor(competitorURI);
			LOG.debug("JSON response From Service - \n" + jsonResponse);
		}
		catch (final Exception e)
		{
			LOG.error(CatCoreConstants.SPECCHECK_ERRORMESSAGE + e.getMessage());
			throw new CatSpeccheckException(CatCoreConstants.SPECCHECK_ERRORMESSAGE, e);
		}

		return jsonResponse;
	}

	/**
	 * This method returns all the competitors for given sales model.
	 *
	 * @param requestURI
	 * @param response
	 * @param machineId
	 * @param rangeId
	 * @return
	 */
	private String getAllCompetitors(final String requestURI, final String machineId, final String rangeId, final String tierId)
			throws CatSpeccheckException
	{
		String jsonResponse = null;
		//call Competitor API to get List of Competitor for given Sales Model - speccheck Handler No:5
		final String competitorURI = requestURI + CatCoreConstants.SPECCHECK_MACHINESHANDLERURI + CatCoreConstants.SPECCHECK_RID
				+ rangeId + CatCoreConstants.SPECCHECK_MID + machineId + CatCoreConstants.SPECCHECK_LIMIT
				+ Config.getParameter(CatCoreConstants.SPECCHECK_LIMITFORCATCOMP) + CatCoreConstants.SPECCHECK_TIDS + tierId
				+ CatCoreConstants.SPECCHECK_RGIDS + Config.getParameter(CatCoreConstants.SPECCHECK_RGIDSFORCOMPARE);
		try
		{
			jsonResponse = catSpeccheckService.callSpeccheckAPIForCompetitor(competitorURI);
			LOG.debug("JSON response From Service - \n" + jsonResponse);
		}
		catch (final Exception e)
		{
			LOG.error(CatCoreConstants.SPECCHECK_ERRORMESSAGE + e.getMessage());
			throw new CatSpeccheckException(CatCoreConstants.SPECCHECK_ERRORMESSAGE, e);
		}

		return jsonResponse;
	}

	/**
	 * This method is to log response from Speccheck
	 *
	 * @param speccheckResponse
	 *           response to be fetched from Speccheck response object
	 *
	 */
	public void logResponseFromSpecCheck(final ResponseEntity<Object> speccheckResponse)
	{
		if (null != speccheckResponse)
		{
			if (null != speccheckResponse.getStatusCode())
			{
				LOG.debug("GET Service Response - " + speccheckResponse.getStatusCode());
			}
			LOG.debug("!! GET Service Response Body - \n");
			final Object responseBody = speccheckResponse.getBody();
			LOG.debug(responseBody);
		}
		else
		{
			LOG.debug("!! GET Service Response NULL !!");
		}
	}

	/**
	 * This method returns Sales Models for comparison against selected Category
	 *
	 * @param category
	 *
	 * @see com.cat.bcp.facades.speccheck.CatSpeccheckFacade#getSalesModelForCompare(java.lang.String)
	 */
	@Override
	public List<ProductData> getSalesModelForCompare(final CategoryModel category)
	{
		final String categoryCode = category.getCode();
		final List<ProductData> salesModels = new ArrayList<>();
		List<ProductModel> products;
		if (categoryCode.equalsIgnoreCase(CatCoreConstants.UTV))
		{
			products = catSpeccheckService.getSalesModelForCompare(category);
		}
		else
		{
			products = catSpeccheckService.getSalesModelForCompare(categoryCode);
		}
		for (final ProductModel productModel : products)
		{
			final ProductData product = productConverter.convert(productModel);
			salesModels.add(product);
		}
		return salesModels;
	}

	/**
	 * This method returns Sales Models for comparison against selected Category
	 *
	 * @param category
	 * @return JSON Object
	 */
	@Override
	public Object getUTVProductsForCompare(final CategoryModel category, final String salesModel)
	{
		final List<ProductModel> products = new ArrayList<>();
		final List<ProductModel> allProducts = category.getProducts();
		for (final ProductModel productModel : allProducts)
		{
			if (!(productModel instanceof ConfigVariantProductModel) && !(productModel instanceof BaseVariantProductModel)
					&& !(productModel.getCode().equalsIgnoreCase(salesModel)))
			{
				products.add(productModel);
			}
		}

		try
		{
			return catSpeccheckService.getSalesModelJson(products);
		}
		catch (final Exception e)
		{
			LOG.error(ERROR, e);
		}
		return CatCoreConstants.SPECCHECK_EMPLTYLIST;
	}

	/**
	 * This method returns product specification data for comparison against selected sales models for UTV.
	 *
	 * @param salesModels
	 * @param uom
	 * @return JSON Object
	 */
	@Override
	public Object getProductSpecs(final String salesModels, final String uom)
	{
		String unit;
		if ("2".equals(uom))
		{
			unit = UNIT_US;
		}
		else
		{
			unit = UNIT_METRIC;
		}
		final List<ProductData> products = getProductDataForUTV(salesModels);

		try
		{
			return catSpeccheckService.getProductSpecs(products, unit);
		}
		catch (final Exception e)
		{
			LOG.error(ERROR, e);
		}

		return CatCoreConstants.SPECCHECK_EMPLTYLIST;
	}

	/**
	 *
	 * This method returns the list of ProductData for the sales models selected for comapre.
	 *
	 * @param salesModels
	 *           sales Models
	 * @return List<ProductData>
	 */
	public List<ProductData> getProductDataForUTV(final String salesModels)
	{
		final List<ProductData> products = new ArrayList<>();

		final String[] salesModelList = salesModels.split(",");

		for (final String salesModel : salesModelList)
		{
			populateBaseVariantProducts(products, salesModel);
		}
		return products;
	}

	/**
	 * This method is used to get Base Variant Products
	 *
	 * @param products
	 *           List of ProductData
	 * @param salesModel
	 *           salesModel
	 */
	private void populateBaseVariantProducts(final List<ProductData> products, final String salesModel)
	{
		final ProductModel product = productService.getProductForCode(salesModel);
		final Collection<VariantProductModel> productVariants = product.getVariants();
		if (!productVariants.isEmpty())
		{
			for (final VariantProductModel variant : productVariants)
			{
				if (variant instanceof BaseVariantProductModel)
				{
					final BaseVariantProductModel baseModel = (BaseVariantProductModel) variant;
					final ProductData productData = productFacade.getProductForCodeAndOptions(baseModel.getCode(),
							Arrays.asList(ProductOption.CLASSIFICATION));
					products.add(productData);
				}
			}
		}
	}
}
