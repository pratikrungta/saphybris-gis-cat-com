/**
 *
 */
package com.cat.bcp.facades.user;

import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;


/**
 * The Interface CatUserFacade.
 *
 * @author vjagannadharaotel
 */
public interface CatUserFacade extends UserFacade
{

	/**
	 * This method is to create a new customer with the given uid and name.
	 *
	 * @param customerName
	 *           the customer name
	 * @param customerUid
	 *           the customer uid
	 */
	void createCustomer(final String customerName, final String customerUid);

	/**
	 * This method is to add address to the given customer uid.
	 *
	 * @param addressData
	 *           the address data
	 * @param customerUid
	 *           the customer uid
	 */
	void addAddress(final AddressData addressData, final String customerUid);

	/**
	 * This method is to edit the address of a given customer uid.
	 *
	 * @param addressData
	 *           the address data
	 * @param customerUid
	 *           the customer uid
	 */
	void editAddress(AddressData addressData, final String customerUid);

	/**
	 * This method is to update the customer data.
	 *
	 * @param customerName
	 *           the customer name
	 * @param customerEmail
	 *           the customer email
	 * @param customerUid
	 *           the customer uid
	 */
	void updateCustomer(String customerName, String customerEmail, String customerUid);

}
