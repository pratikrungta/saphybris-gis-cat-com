/**
 *
 */
package com.cat.bcp.facades.user.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.impl.DefaultUserFacade;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import com.cat.bcp.core.customer.CatCustomerAccountService;
import com.cat.bcp.core.service.CatCustomerService;
import com.cat.bcp.facades.user.CatUserFacade;


/**
 * @author vjagannadharaotel
 *
 */
public class CatUserFacadeImpl extends DefaultUserFacade implements CatUserFacade
{

	@Resource(name = "customerAccountService")
	private CatCustomerAccountService customerAccountService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "catCustomerService")
	private CatCustomerService catCustomerService;

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public void createCustomer(final String customerName, final String customerUid)
	{
		customerAccountService.createCustomer(customerName, customerUid);

	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public void updateCustomer(final String customerName, final String customerEmail, final String customerUid)
	{
		final CustomerModel currentCustomer = (CustomerModel) userService.getUserForUID(customerUid);
		if (!currentCustomer.getName().equalsIgnoreCase(customerName))
		{
			currentCustomer.setName(customerName);
		}
		if (!currentCustomer.getCatCustomerEmailId().equalsIgnoreCase(customerEmail))
		{
			currentCustomer.setCatCustomerEmailId(customerEmail);
		}
		modelService.save(currentCustomer);
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public void addAddress(final AddressData addressData, final String customerEmail)
	{

		validateParameterNotNullStandardMessage("addressData", addressData);

		final CustomerModel currentCustomer = catCustomerService.findUserByCustomerEmail(customerEmail);

		final boolean makeThisAddressTheDefault = addressData.isDefaultAddress();

		// Create the new address model
		final AddressModel newAddress = getModelService().create(AddressModel.class);
		getAddressReversePopulator().populate(addressData, newAddress);

		// Store the address against the user
		customerAccountService.saveAddressEntry(currentCustomer, newAddress);

		// Update the address ID in the newly created address
		addressData.setId(newAddress.getPk().toString());

		if (makeThisAddressTheDefault)
		{
			customerAccountService.setDefaultAddressEntry(currentCustomer, newAddress);
		}
		else if (addressData.isBillingAddress())
		{
			currentCustomer.setDefaultPaymentAddress(newAddress);
			modelService.save(currentCustomer);
		}
		else if (addressData.isShippingAddress())
		{
			currentCustomer.setDefaultShipmentAddress(newAddress);
			modelService.save(currentCustomer);
		}
	}

	/**
	 *
	 * {@inheritDoc}
	 */
	@Override
	public void editAddress(final AddressData addressData, final String customerEmail)
	{
		validateParameterNotNullStandardMessage("addressData", addressData);

		final CustomerModel currentCustomer = catCustomerService.findUserByCustomerEmail(customerEmail);
		final AddressModel addressModel;
		if (currentCustomer.getDefaultShipmentAddress().equals(currentCustomer.getDefaultPaymentAddress())
				&& addressData.isBillingAddress())
		{
			addressData.setId(null);
			addressModel = null;
		}
		else
		{
			addressModel = getCustomerAccountService().getAddressForCode(currentCustomer, addressData.getId());
		}
		final boolean makeThisAddressTheDefault = addressData.isDefaultAddress();
		if (addressModel != null)
		{
			addressModel.setRegion(null);
			getAddressReversePopulator().populate(addressData, addressModel);
			getCustomerAccountService().saveAddressEntry(currentCustomer, addressModel);
			if (makeThisAddressTheDefault)
			{
				customerAccountService.setDefaultAddressEntry(currentCustomer, addressModel);
			}
			else if (addressData.isBillingAddress())
			{
				currentCustomer.setDefaultPaymentAddress(addressModel);
				modelService.save(currentCustomer);
			}
			else if (addressData.isShippingAddress())
			{
				currentCustomer.setDefaultShipmentAddress(addressModel);
				modelService.save(currentCustomer);
			}
		}
		else
		{
			this.addAddress(addressData, customerEmail);
		}
	}

}
