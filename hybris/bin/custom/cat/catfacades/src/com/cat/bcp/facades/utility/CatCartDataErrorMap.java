/**
 *
 */
package com.cat.bcp.facades.utility;

import de.hybris.platform.commercefacades.order.data.OrderEntryData;

import java.util.List;
import java.util.Map;

import com.cat.bcp.core.enums.LaneTypeEnum;


/**
 * @author sagdhingra
 *
 */
public class CatCartDataErrorMap
{

	LaneTypeEnum laneType;


	Map<String, List<OrderEntryData>> errorEntryMap;


	/**
	 * @return the laneType
	 */
	public LaneTypeEnum getLaneType()
	{
		return laneType;
	}


	/**
	 * @param laneType
	 *           the laneType to set
	 */
	public void setLaneType(final LaneTypeEnum laneType)
	{
		this.laneType = laneType;
	}


	/**
	 * @return the errorEntryMap
	 */
	public Map<String, List<OrderEntryData>> getErrorEntryMap()
	{
		return errorEntryMap;
	}


	/**
	 * @param errorEntryMap
	 *           the errorEntryMap to set
	 */
	public void setErrorEntryMap(final Map<String, List<OrderEntryData>> errorEntryMap)
	{
		this.errorEntryMap = errorEntryMap;
	}




}
