/**
 *
 */
package com.cat.bcp.facades.utility;


import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.variants.model.VariantProductModel;

import javax.annotation.Resource;

import com.cat.bcp.core.enums.ProductConditionEnum;
import com.cat.bcp.core.model.BaseVariantProductModel;


/**
 * This class is used as helper for populating Cat Image related attributes
 *
 * @author sankale
 *
 */
public class CatProductImageHelper
{

	@Resource(name = "modelService")
	ModelService modelService;

	/**
	 * Method to get the values only for used product and not the base models.
	 *
	 * @param productModel
	 *           product model from which the value is to be fetched
	 * @param attribute
	 *           attribute values to be fetched
	 */
	public Object getProductAttribute(final ProductModel productModel, final String attribute)
	{
		Object value = null;
		if (null != productModel)
		{
			value = modelService.getAttributeValue(productModel, attribute);
			if (value == null && productModel instanceof VariantProductModel)
			{
				value = checkBaseProductScenario(productModel, attribute);
			}
		}
		return value;
	}


	/**
	 * Perform the default action when the product is new
	 *
	 * @param productModel
	 *           product model from which the value is to be fetched
	 * @param attribute
	 *           attribute for which value is to be returned
	 * @return Object value that will be returned
	 */
	public Object checkBaseProductScenario(final ProductModel productModel, final String attribute)
	{
		if (productModel instanceof BaseVariantProductModel
				&& !(ProductConditionEnum.USED).equals(((BaseVariantProductModel) productModel).getProductCondition()))
		{
			final ProductModel baseProduct = ((VariantProductModel) productModel).getBaseProduct();
			if (baseProduct != null)
			{
				return getProductAttribute(baseProduct, attribute);
			}
		}
		return null;
	}
}
