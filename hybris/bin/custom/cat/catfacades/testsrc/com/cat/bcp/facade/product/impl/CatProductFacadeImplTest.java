/**
 *
 */
package com.cat.bcp.facade.product.impl;

import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.ProductReferenceModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.PartVariantProductModel;
import com.cat.bcp.core.servicelayer.CatProductService;
import com.cat.bcp.facades.order.CatCartFacade;



/**
 * @author sagdhingra
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CatProductFacadeImplTest
{
	@Mock
	CatProductService catProductService;

	@Mock
	CatCartFacade cartFacade;

	@Mock
	private Converter<ProductModel, ProductData> productConverter;

	CatProductFacadeImpl catProductFacadeImpl;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		catProductFacadeImpl = new CatProductFacadeImpl();
		catProductFacadeImpl.setCatProductService(catProductService);
		catProductFacadeImpl.setProductConverter(productConverter);
		catProductFacadeImpl.setCartFacade(cartFacade);
	}

	/**
	 * Test method for {@link com.cat.bcp.facade.product.impl.CatProductFacadeImpl#getCUVModels()}.
	 */
	@Test
	public void testGetCUVModels()
	{
		final List<ProductModel> productModelList = new ArrayList<>();
		final ProductModel mockProduct1 = mock(ProductModel.class);
		mockProduct1.setCode("CUV82");
		final ProductModel mockProduct2 = mock(ProductModel.class);
		mockProduct2.setCode("CUV85");
		final ProductModel mockProduct3 = mock(ProductModel.class);
		mockProduct3.setCode("CUV102");
		final ProductModel mockProduct4 = mock(ProductModel.class);
		mockProduct4.setCode("CUV105");
		productModelList.add(mockProduct1);
		productModelList.add(mockProduct2);
		productModelList.add(mockProduct3);
		productModelList.add(mockProduct4);
		Mockito.when(catProductService.getCUVModels()).thenReturn(productModelList);
		Assert.assertNotNull(catProductFacadeImpl.getCUVModels());
	}

	/**
	 * Test method for {@link com.cat.bcp.facade.product.impl.CatProductFacadeImpl#getAccessoryData()}.
	 */
	@Test
	public void testGetAccessoryData()
	{
		final List<ProductReferenceModel> productRefList = new ArrayList<>();
		final ProductReferenceModel rfModel = mock(ProductReferenceModel.class);
		final BaseVariantProductModel mockProduct1 = mock(BaseVariantProductModel.class);
		mockProduct1.setCode("529-7215");
		final ProductModel mockproductModel = mock(ProductModel.class);
		final PartVariantProductModel partVariant1 = mock(PartVariantProductModel.class);
		Mockito.when(partVariant1.getCode()).thenReturn("545-8297");
		Mockito.when(partVariant1.getName()).thenReturn("Light Electicals");
		Mockito.when(rfModel.getSource()).thenReturn(mockProduct1);
		Mockito.when(mockProduct1.getBaseProduct()).thenReturn(mockproductModel);
		Mockito.when(mockproductModel.getCode()).thenReturn("CUV82");
		Mockito.when(rfModel.getTarget()).thenReturn(partVariant1);
		Mockito.when(partVariant1.getRecommendedFlag()).thenReturn(Boolean.TRUE);
		Mockito.when(rfModel.getDescription()).thenReturn("ELECTRICAL");
		productRefList.add(rfModel);

		final ProductReferenceModel refModel = mock(ProductReferenceModel.class);
		final ProductModel mockproductModel2 = mock(ProductModel.class);
		final BaseVariantProductModel mockProduct2 = mock(BaseVariantProductModel.class);
		mockProduct2.setCode("529-7215");
		Mockito.when(refModel.getSource()).thenReturn(mockProduct2);
		Mockito.when(mockProduct2.getBaseProduct()).thenReturn(mockproductModel2);
		Mockito.when(mockproductModel2.getCode()).thenReturn("CUV102");
		Mockito.when(refModel.getTarget()).thenReturn(partVariant1);
		Mockito.when(refModel.getDescription()).thenReturn("ELECTRICAL");
		productRefList.add(refModel);

		final ProductReferenceModel refModel3 = mock(ProductReferenceModel.class);
		final ProductModel mockproductModel3 = mock(ProductModel.class);
		final BaseVariantProductModel mockProduct3 = mock(BaseVariantProductModel.class);
		mockProduct2.setCode("529-7215");
		final PartVariantProductModel partVariant2 = mock(PartVariantProductModel.class);
		Mockito.when(partVariant2.getCode()).thenReturn("548-9157");
		Mockito.when(partVariant2.getName()).thenReturn("Heavy Electicals");
		Mockito.when(refModel3.getSource()).thenReturn(mockProduct3);
		Mockito.when(mockProduct3.getBaseProduct()).thenReturn(mockproductModel3);
		Mockito.when(mockproductModel3.getCode()).thenReturn("CUV82");
		Mockito.when(refModel3.getTarget()).thenReturn(partVariant2);
		Mockito.when(partVariant2.getRecommendedFlag()).thenReturn(Boolean.FALSE);
		Mockito.when(refModel3.getDescription()).thenReturn("ELECTRICAL");
		productRefList.add(refModel3);


		final CartData cartData = mock(CartData.class);
		final List<OrderEntryData> orderEntries = new ArrayList<>();
		final OrderEntryData orderEntryData = mock(OrderEntryData.class);
		final ProductData productData = mock(ProductData.class);
		Mockito.when(productData.getCode()).thenReturn("548-9157");
		Mockito.when(orderEntryData.getProduct()).thenReturn(productData);
		Mockito.when(orderEntryData.getQuantity()).thenReturn(Long.valueOf(1));
		final PriceData priceData = mock(PriceData.class);
		Mockito.when(priceData.getValue()).thenReturn(BigDecimal.valueOf(3499));
		Mockito.when(priceData.getFormattedValue()).thenReturn("$3499");
		Mockito.when(orderEntryData.getTotalPrice()).thenReturn(priceData);

		final OrderEntryData orderEntryData1 = mock(OrderEntryData.class);
		Mockito.when(orderEntryData1.getProduct()).thenReturn(productData);
		Mockito.when(orderEntryData1.getQuantity()).thenReturn(Long.valueOf(1));
		Mockito.when(orderEntryData1.getTotalPrice()).thenReturn(priceData);
		orderEntries.add(orderEntryData);
		orderEntries.add(orderEntryData1);
		Mockito.when(cartData.getEntries()).thenReturn(orderEntries);
		Mockito.when(cartFacade.getSessionCart()).thenReturn(cartData);

		Mockito.when(catProductService.getAccessoryData()).thenReturn(productRefList);
		Assert.assertNotNull(catProductFacadeImpl.getAccessoryData());
	}

	@Test
	public void testGetAccessoryDataWithEmptyList()
	{
		final List<ProductReferenceModel> productRefList = new ArrayList<>();
		Mockito.when(catProductService.getAccessoryData()).thenReturn(productRefList);
		Assert.assertNotNull(catProductFacadeImpl.getAccessoryData());
	}


}
