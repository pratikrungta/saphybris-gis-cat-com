/**
 *
 */
package com.cat.bcp.facades.customer.impl;

import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2b.model.B2BUnitModel;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.cat.bcp.core.model.CatCountyModel;
import com.cat.bcp.core.service.impl.CatCustomerServiceImpl;
import com.cat.facades.user.CatCountyData;
import com.cat.facades.user.CatCountyMappingData;
import com.cat.facades.user.CatCountyMasterData;


/**
 * @author sagdhingra
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CatCustomerFacadeImplTest
{

	/**
	 * @throws java.lang.Exception
	 */

	@Mock
	CatCustomerServiceImpl catCustomerService;

	@Mock
	ModelService modelService;

	@Mock
	UserService userService;


	@Mock
	private Converter<CatCountyModel, CatCountyData> catCountyMappingConverter;

	@Mock
	private Converter<AddressModel, AddressData> addressConverter;


	private CatCustomerFacadeImpl catCustomerFacadeImpl;

	@Mock
	private AddressModel addressModel;

	@Mock
	private B2BUnitModel b2bUnitModel;

	List<CatCountyModel> catCountyList = new ArrayList<>();

	List<CatCountyData> catCountyDataList = new ArrayList<>();

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		catCustomerFacadeImpl = new CatCustomerFacadeImpl();
		catCustomerFacadeImpl.setModelService(modelService);
		catCustomerFacadeImpl.setUserService(userService);
		catCustomerFacadeImpl.setCatCustomerService(catCustomerService);
		catCustomerFacadeImpl.setCatCountyMappingConverter(catCountyMappingConverter);
		catCustomerFacadeImpl.setAddressConverter(addressConverter);
		setB2BUnitModelMockData(b2bUnitModel);
		setAddressModelMockData(addressModel);
		catCountyList = prepareCountyModelList();
		catCountyDataList = prepareCountyDataList();

	}

	/**
	 * @param addressModel2
	 */
	private void setAddressModelMockData(final AddressModel addressModel2)
	{
		Mockito.when(addressModel.getPk()).thenReturn(PK.BIG_PK);
		Mockito.when(addressModel.getOwner()).thenReturn(b2bUnitModel);
	}

	/**
	 * @param b2bUnitModel2
	 */
	private void setB2BUnitModelMockData(final B2BUnitModel b2bUnitModel2)
	{
		Mockito.when(b2bUnitModel.getUid()).thenReturn("B170");
	}

	/**
	 * @return
	 */
	private List<CatCountyData> prepareCountyDataList()
	{
		final List<CatCountyData> catCountyDataList = new ArrayList<>();
		final CatCountyData catCountyData = new CatCountyData();
		catCountyData.setCountyName("Meade");
		catCountyDataList.add(catCountyData);
		return catCountyDataList;
	}

	/**
	 * @return
	 */
	private List<CatCountyModel> prepareCountyModelList()
	{
		final List<CatCountyModel> catCountyList = new ArrayList<>();
		final CatCountyModel catCountyModel = mock(CatCountyModel.class);
		Mockito.when(catCountyModel.getMainDealer()).thenReturn(b2bUnitModel);
		Mockito.when(catCountyModel.getBranchDealer()).thenReturn(b2bUnitModel);
		Mockito.when(catCountyModel.getIsMapped()).thenReturn(Boolean.TRUE);
		Mockito.when(catCountyModel.getCountyName()).thenReturn("Meade");
		catCountyList.add(catCountyModel);
		return catCountyList;
	}


	/**
	 * Test method for
	 * {@link com.cat.bcp.facades.customer.impl.CatCustomerFacadeImpl#getCountyDataForDealer(java.lang.String, java.lang.String, java.lang.String)}
	 * .
	 */
	@Test
	public void testGetCountyDataForDealer()
	{
		final B2BCustomerModel b2bCustomerModel = mock(B2BCustomerModel.class);
		Mockito.when(userService.getCurrentUser()).thenReturn(b2bCustomerModel);
		Mockito.when(b2bCustomerModel.getDefaultB2BUnit()).thenReturn(b2bUnitModel);
		Mockito.when(modelService.get(Mockito.any())).thenReturn(addressModel);
		final String sortParameter = "countyName";
		final String sortOrder = "ASC";
		Mockito.when(catCustomerService.getCountyDataForDealer(Mockito.isA(String.class), Mockito.isA(String.class),
				Mockito.isA(String.class), Mockito.isA(String.class))).thenReturn(catCountyList);
		Mockito.when(catCountyMappingConverter.convertAll(Mockito.anyList())).thenReturn(catCountyDataList);
		Assert.assertNotNull(
				catCustomerFacadeImpl.getCountyDataForDealer(addressModel.getPk().toString(), sortParameter, sortOrder));
	}

	/**
	 * Test method for {@link com.cat.bcp.facades.customer.impl.CatCustomerFacadeImpl#unlinkCounty(java.lang.String)}.
	 */
	@Test
	public void testUnlinkCounty()
	{
		final CatCountyModel catCountyModel = mock(CatCountyModel.class);
		Mockito.when(modelService.get(Mockito.any())).thenReturn(catCountyModel);
		Mockito.when(catCountyModel.getPk()).thenReturn(PK.BIG_PK);
		Mockito.when(catCountyModel.getMainDealer()).thenReturn(b2bUnitModel);
		Mockito.when(catCountyModel.getBranchDealer()).thenReturn(b2bUnitModel);
		Mockito.when(catCountyModel.getIsMapped()).thenReturn(Boolean.TRUE);
		Mockito.when(catCountyModel.getCountyName()).thenReturn("Meade");
		final List<CatCountyModel> catCountyList = new ArrayList<>();
		catCountyList.add(catCountyModel);
		Mockito.when(catCustomerService.getCountyDataForDealer(Mockito.isA(String.class), Mockito.isA(String.class),
				Mockito.isA(String.class), Mockito.isA(String.class))).thenReturn(catCountyList);
		Mockito.when(catCountyMappingConverter.convertAll(Mockito.anyList())).thenReturn(catCountyDataList);
		Assert.assertNotNull(catCustomerFacadeImpl.unlinkCounty(catCountyModel.getPk().toString()));
	}

	/**
	 * Test method for
	 * {@link com.cat.bcp.facades.customer.impl.CatCustomerFacadeImpl#getAvailableCountyForShipTo(java.lang.String)}.
	 */
	@Test
	public void testGetAvailableCountyForShipTo()
	{
		final CatCountyModel catCountyModel = mock(CatCountyModel.class);
		final AddressData addressData = mock(AddressData.class);
		final CatCountyMasterData catCountyMasterData = new CatCountyMasterData();
		Mockito.when(modelService.get(Mockito.any())).thenReturn(catCountyModel);
		Mockito.when(catCountyModel.getPk()).thenReturn(PK.BIG_PK);
		Mockito.when(catCountyModel.getMainDealer()).thenReturn(b2bUnitModel);
		Mockito.when(catCountyModel.getBranchDealer()).thenReturn(b2bUnitModel);
		Mockito.when(catCountyModel.getIsMapped()).thenReturn(Boolean.TRUE);
		Mockito.when(catCountyModel.getCountyName()).thenReturn("Meade");
		final List<CatCountyModel> catCountyList = new ArrayList<>();
		catCountyList.add(catCountyModel);
		final String shipToPk = PK.BIG_PK.toString();
		final B2BCustomerModel b2bCustomerModel = mock(B2BCustomerModel.class);
		Mockito.when(userService.getCurrentUser()).thenReturn(b2bCustomerModel);
		Mockito.when(b2bCustomerModel.getDefaultB2BUnit()).thenReturn(b2bUnitModel);
		Mockito.when(addressModel.getOwner()).thenReturn(b2bUnitModel);
		Mockito.when(b2bUnitModel.getUid()).thenReturn("B170");
		Mockito.when(modelService.get(Mockito.any())).thenReturn(addressModel);
		Mockito.when(catCustomerService.getAvailableCountyForShipTo(Mockito.isA(String.class))).thenReturn(catCountyList);
		Mockito.when(addressConverter.convert(addressModel)).thenReturn(addressData);
		catCountyMasterData.setAddressData(addressData);
		catCountyMasterData.setCountyData(catCountyDataList);
		Assert.assertNotNull(catCustomerFacadeImpl.getAvailableCountyForShipTo(shipToPk));

	}

	/**
	 * Test method for
	 * {@link com.cat.bcp.facades.customer.impl.CatCustomerFacadeImpl#linkCounty(com.cat.facades.user.CatCountyMappingData)}
	 * .
	 */
	@Test
	public void testLinkCounty()
	{
		final List<String> countyPkList = new ArrayList<>();
		countyPkList.add("12343434");
		final CatCountyMappingData catCountyMappingData = mock(CatCountyMappingData.class);
		Mockito.when(catCountyMappingData.getShipToPk()).thenReturn(PK.BIG_PK.toString());
		Mockito.when(modelService.get(Mockito.any())).thenReturn(addressModel);
		Mockito.when(addressModel.getOwner()).thenReturn(b2bUnitModel);
		Mockito.when(catCountyMappingData.getCountyIdList()).thenReturn(countyPkList);
		final CatCountyModel catCountyModel = mock(CatCountyModel.class);
		Mockito.when(modelService.get(PK.parse(countyPkList.get(0)))).thenReturn(catCountyModel);
		catCustomerFacadeImpl.linkCounty(catCountyMappingData);
	}

	/**
	 * Test method for
	 * {@link com.cat.bcp.facades.customer.impl.CatCustomerFacadeImpl#checkIfUnlinkedCountiesPresent(java.lang.String)}.
	 */
	@Test
	public void testCheckIfUnlinkedCountiesPresent()
	{
		final CatCountyModel catCountyModel = mock(CatCountyModel.class);
		final AddressData addressData = mock(AddressData.class);
		Mockito.when(modelService.get(Mockito.any())).thenReturn(catCountyModel);
		Mockito.when(catCountyModel.getPk()).thenReturn(PK.BIG_PK);
		Mockito.when(catCountyModel.getMainDealer()).thenReturn(b2bUnitModel);
		Mockito.when(catCountyModel.getBranchDealer()).thenReturn(b2bUnitModel);
		Mockito.when(catCountyModel.getIsMapped()).thenReturn(Boolean.TRUE);
		Mockito.when(catCountyModel.getCountyName()).thenReturn("Meade");
		final List<CatCountyModel> catCountyList = new ArrayList<>();
		catCountyList.add(catCountyModel);
		final String shipToPk = PK.BIG_PK.toString();
		final B2BCustomerModel b2bCustomerModel = mock(B2BCustomerModel.class);
		Mockito.when(userService.getCurrentUser()).thenReturn(b2bCustomerModel);
		Mockito.when(b2bCustomerModel.getDefaultB2BUnit()).thenReturn(b2bUnitModel);
		Mockito.when(addressModel.getOwner()).thenReturn(b2bUnitModel);
		Mockito.when(b2bUnitModel.getUid()).thenReturn("B170");
		Mockito.when(modelService.get(Mockito.any())).thenReturn(addressModel);
		Mockito.when(catCustomerService.getAvailableCountyForShipTo(Mockito.isA(String.class))).thenReturn(catCountyList);
		Mockito.when(catCountyMappingConverter.convertAll(Mockito.anyList())).thenReturn(catCountyDataList);
		Mockito.when(addressConverter.convert(addressModel)).thenReturn(addressData);
		Assert.assertNotNull(catCustomerFacadeImpl.checkIfUnlinkedCountiesPresent(shipToPk));
	}

	@Test
	public void testGetConcatenatedShippingAddressList()
	{
		final List<AddressData> addressDataList = new ArrayList();

		final AddressData add1 = new AddressData();
		add1.setLine1("Line 1, test");
		add1.setLine2("Line 2");
		add1.setLine3("Line 3");

		addressDataList.add(add1);

		Mockito.when(catCustomerService.getTownRegionConcatenatedAddress(add1)).thenReturn("RegionConcatenated");
		Mockito.when(catCustomerService.getIsoCodePostalConcatenatedAddress(add1)).thenReturn("Iso-postalConcatenated");
		Mockito.when(catCustomerService.getAddressString(add1.getLine3(), " ")).thenReturn(add1.getLine3());
		Mockito.when(catCustomerService.getAddressString(add1.getLine1(), ", ")).thenReturn(add1.getLine1());

		Assert.assertNotNull(catCustomerFacadeImpl.getConcatenatedShippingAddressList(addressDataList));
	}

}
