/**
 *
 */
package com.cat.bcp.facades.order.impl;

import static org.mockito.Mockito.mock;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.commerceservices.order.CommerceCartService;
import de.hybris.platform.commerceservices.service.data.CommerceCartParameter;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import com.cat.facades.product.CatAccessoryData;


/**
 * @author sagdhingra
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CatCartFacadeImplTest
{
	@Mock
	Converter<AddToCartParams, CommerceCartParameter> commerceCartParameterConverter;

	@Mock
	CommerceCartService commerceCartService;

	@Mock
	Converter<CommerceCartModification, CartModificationData> cartModificationConverter;

	CatCartFacadeImpl catCartFacadeImpl;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		catCartFacadeImpl = new CatCartFacadeImpl();
		catCartFacadeImpl.setCommerceCartParameterConverter(commerceCartParameterConverter);
		catCartFacadeImpl.setCommerceCartService(commerceCartService);
		catCartFacadeImpl.setCartModificationConverter(cartModificationConverter);
	}

	/**
	 * Test method for {@link com.cat.bcp.facades.order.impl.CatCartFacadeImpl#addAccessoriesToCart(java.util.List)}.
	 *
	 * @throws CommerceCartModificationException
	 */
	@Test
	public void testAddAccessoriesToCart() throws CommerceCartModificationException
	{
		final List<CatAccessoryData> catAccessoryDataList = new ArrayList<>();
		final CatAccessoryData catAccessoryData = mock(CatAccessoryData.class);
		Mockito.when(catAccessoryData.getCode()).thenReturn("545-8297");
		Mockito.when(catAccessoryData.getQuantity()).thenReturn(Integer.valueOf(1));
		Mockito.when(catAccessoryData.getSelectedCompatibleModel()).thenReturn("CUV82");
		catAccessoryDataList.add(catAccessoryData);
		catCartFacadeImpl.addAccessoriesToCart(catAccessoryDataList);
	}

}
