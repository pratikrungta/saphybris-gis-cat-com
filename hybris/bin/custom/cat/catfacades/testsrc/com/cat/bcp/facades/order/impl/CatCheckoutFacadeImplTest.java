/**
 *
 */
package com.cat.bcp.facades.order.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.b2bcommercefacades.company.data.B2BUnitData;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.category.CommerceCategoryService;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.ShipToAfterDateModel;
import com.cat.bcp.core.model.TruckloadModel;
import com.cat.bcp.core.order.CatCommerceCheckoutService;
import com.cat.bcp.core.servicelayer.CatProductService;
import com.cat.bcp.facades.customer.CatCustomerFacade;
import com.cat.bcp.facades.order.CatCartFacade;
import com.cat.bcp.facades.order.TruckloadData;

import junit.framework.Assert;


/**
 * @author bidavda
 *
 */
@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CatCheckoutFacadeImplTest
{

	List<ProductData> productDataList;

	CatCheckoutFacadeImpl catCheckoutFacade;

	@Mock
	private CatCommerceCheckoutService checkoutService;

	@Mock
	private Converter<TruckloadModel, TruckloadData> catTruckloadConverter;

	@Mock
	private CommerceCategoryService commerceCategoryService;

	@Mock
	private CatProductService productService;

	@Mock
	private Converter<ProductModel, ProductData> productConverter;



	@Mock
	private ModelService modelService;


	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);

		productDataList = new ArrayList<>();

		final ProductData product1 = new ProductData();
		product1.setCode("CUV82");
		productDataList.add(product1);
		final ProductData product2 = new ProductData();
		product2.setCode("CUV102D");
		productDataList.add(product2);
		final ProductData product3 = Mockito.mock(ProductData.class);
		product3.setCode("CUV85");
		productDataList.add(product3);

		catCheckoutFacade = new CatCheckoutFacadeImpl();
		catCheckoutFacade.setCommerceCategoryService(commerceCategoryService);
		catCheckoutFacade.setProductService(productService);
		catCheckoutFacade.setProductConverter(productConverter);
		catCheckoutFacade.setCheckoutService(checkoutService);
		catCheckoutFacade.setCatTruckloadConverter(catTruckloadConverter);
		catCheckoutFacade.setModelService(modelService);
	}

	/**
	 * Test method for
	 * {@link com.cat.bcp.facades.order.impl.CatCheckoutFacadeImpl#getConfigProductsFromCategory(java.lang.String)}.
	 */
	@Test
	public void testGetConfigProductsFromCategory()
	{
		final String CATEGORY_CODE = "utv";
		final CategoryModel category = Mockito.mock(CategoryModel.class);
		category.setCode(CATEGORY_CODE);
		Mockito.when(commerceCategoryService.getCategoryForCode(CATEGORY_CODE)).thenReturn(category);

		final List<ProductModel> productList = Mockito.mock(ArrayList.class);
		Mockito.when(productService.getProductsForCategory(category)).thenReturn(productList);

		final Iterator<ProductModel> productItr = Mockito.mock(Iterator.class);
		Mockito.when(productList.iterator()).thenReturn(productItr);

		Mockito.when(Boolean.valueOf(productItr.hasNext())).thenReturn(Boolean.TRUE, Boolean.FALSE);

		final ConfigVariantProductModel product = Mockito.mock(ConfigVariantProductModel.class);
		Mockito.when(productItr.next()).thenReturn(product);

		final List<ProductModel> configProductList = Mockito.mock(ArrayList.class);

		Mockito.when(productConverter.convertAll(configProductList)).thenReturn(productDataList);


		Assert.assertNotNull(catCheckoutFacade.getConfigProductsFromCategory(CATEGORY_CODE));

	}

	@Test
	public void testGetConfigProductsFromCategoryIf()
	{
		final String CATEGORY_CODE = "utv";
		final CategoryModel category = Mockito.mock(CategoryModel.class);
		category.setCode(CATEGORY_CODE);
		Mockito.when(commerceCategoryService.getCategoryForCode(CATEGORY_CODE)).thenReturn(category);

		final List<ProductModel> productList = Mockito.mock(ArrayList.class);
		Mockito.when(productService.getProductsForCategory(category)).thenReturn(productList);

		final Iterator<ProductModel> productItr = Mockito.mock(Iterator.class);
		Mockito.when(productList.iterator()).thenReturn(productItr);

		Mockito.when(Boolean.valueOf(productItr.hasNext())).thenReturn(Boolean.TRUE, Boolean.FALSE);

		final BaseVariantProductModel product = Mockito.mock(BaseVariantProductModel.class);
		Mockito.when(productItr.next()).thenReturn(product);

		final List<ProductModel> configProductList = Mockito.mock(ArrayList.class);

		Mockito.when(productConverter.convertAll(configProductList)).thenReturn(productDataList);

		Assert.assertNotNull(catCheckoutFacade.getConfigProductsFromCategory(CATEGORY_CODE));
	}


	@Test
	public void testGetTruckloadCombinationList()
	{
		final List<TruckloadModel> truckloadList = Mockito.mock(ArrayList.class);
		final TruckloadModel t1 = new TruckloadModel();
		t1.setCode("T1");
		truckloadList.add(t1);

		final TruckloadModel t2 = new TruckloadModel();
		t2.setCode("T2");
		truckloadList.add(t2);

		final List<TruckloadData> truckDataList = Mockito.mock(ArrayList.class);

		final TruckloadData td1 = new TruckloadData();
		t1.setCode("TD1");
		truckDataList.add(td1);

		final TruckloadData td2 = new TruckloadData();
		t2.setCode("TD2");
		truckDataList.add(td2);

		Mockito.when(checkoutService.getTruckloadCombinations()).thenReturn(truckloadList);
		Mockito.when(catTruckloadConverter.convertAll(truckloadList)).thenReturn(truckDataList);

		Assert.assertNotNull(catCheckoutFacade.getTruckloadCombinationList());
	}

	@Test
	public void testGetTruckloadCombinationListEmptyList()
	{
		final List<TruckloadModel> truckloadList = Collections.emptyList();

		Mockito.when(checkoutService.getTruckloadCombinations()).thenReturn(truckloadList);

		Assert.assertNotNull(catCheckoutFacade.getTruckloadCombinationList());
	}


	@Test
	public void testGetListOfFormattedShipToDates()
	{
		final List<ShipToAfterDateModel> shipToDates = Mockito.mock(ArrayList.class);

		final Iterator<ShipToAfterDateModel> shipToDatesItr = Mockito.mock(Iterator.class);
		Mockito.when(shipToDates.iterator()).thenReturn(shipToDatesItr);

		Mockito.when(Boolean.valueOf(shipToDatesItr.hasNext())).thenReturn(Boolean.TRUE, Boolean.FALSE);
		final ShipToAfterDateModel shipAfterModel = Mockito.mock(ShipToAfterDateModel.class);
		shipAfterModel.setShipAfterDate(new Date());
		Mockito.when(shipToDatesItr.next()).thenReturn(shipAfterModel);

		final Date shipFromDate = Mockito.mock(Date.class);
		Mockito.when(shipAfterModel.getShipFromDate()).thenReturn(shipFromDate);

		final Date shipToDate = Mockito.mock(Date.class);
		Mockito.when(shipAfterModel.getShipToDate()).thenReturn(shipToDate);

		final int fromDatePart = Integer.valueOf(23).intValue();
		Mockito.when(Integer.valueOf(shipFromDate.getDate())).thenReturn(Integer.valueOf(fromDatePart));

		final int fromMonthPart = Integer.valueOf(3).intValue();
		Mockito.when(Integer.valueOf(shipFromDate.getMonth())).thenReturn(Integer.valueOf(fromMonthPart));

		final Date shipAfterDate = Mockito.mock(Date.class);
		Mockito.when(shipAfterModel.getShipAfterDate()).thenReturn(shipAfterDate);

		final TreeMap m = Mockito.mock(TreeMap.class);
		//Mockito.when(catCheckoutFacade.getListOfFormattedShipToDates(shipToDates)).thenReturn(m);
		Assert.assertNotNull((catCheckoutFacade.getListOfFormattedShipToDates(shipToDates)));
	}


	@Test
	public void testGetListOfFormattedShipToDatesMod1()
	{
		final List<ShipToAfterDateModel> shipToDates = Mockito.mock(ArrayList.class);

		final Iterator<ShipToAfterDateModel> shipToDatesItr = Mockito.mock(Iterator.class);
		Mockito.when(shipToDates.iterator()).thenReturn(shipToDatesItr);

		Mockito.when(Boolean.valueOf(shipToDatesItr.hasNext())).thenReturn(Boolean.TRUE, Boolean.FALSE);
		final ShipToAfterDateModel shipAfterModel = Mockito.mock(ShipToAfterDateModel.class);
		shipAfterModel.setShipAfterDate(new Date());
		Mockito.when(shipToDatesItr.next()).thenReturn(shipAfterModel);

		final Date shipFromDate = Mockito.mock(Date.class);
		Mockito.when(shipAfterModel.getShipFromDate()).thenReturn(shipFromDate);

		final Date shipToDate = Mockito.mock(Date.class);
		Mockito.when(shipAfterModel.getShipToDate()).thenReturn(shipToDate);

		final int fromDatePart = Integer.valueOf(21).intValue();
		Mockito.when(Integer.valueOf(shipFromDate.getDate())).thenReturn(Integer.valueOf(fromDatePart));

		final int fromMonthPart = Integer.valueOf(3).intValue();
		Mockito.when(Integer.valueOf(shipFromDate.getMonth())).thenReturn(Integer.valueOf(fromMonthPart));

		final Date shipAfterDate = Mockito.mock(Date.class);
		Mockito.when(shipAfterModel.getShipAfterDate()).thenReturn(shipAfterDate);

		final TreeMap m = Mockito.mock(TreeMap.class);
		//Mockito.when(catCheckoutFacade.getListOfFormattedShipToDates(shipToDates)).thenReturn(m);
		Assert.assertNotNull((catCheckoutFacade.getListOfFormattedShipToDates(shipToDates)));
	}

	@Test
	public void testGetListOfFormattedShipToDatesMod2()
	{
		final List<ShipToAfterDateModel> shipToDates = Mockito.mock(ArrayList.class);

		final Iterator<ShipToAfterDateModel> shipToDatesItr = Mockito.mock(Iterator.class);
		Mockito.when(shipToDates.iterator()).thenReturn(shipToDatesItr);

		Mockito.when(Boolean.valueOf(shipToDatesItr.hasNext())).thenReturn(Boolean.TRUE, Boolean.FALSE);
		final ShipToAfterDateModel shipAfterModel = Mockito.mock(ShipToAfterDateModel.class);
		shipAfterModel.setShipAfterDate(new Date());
		Mockito.when(shipToDatesItr.next()).thenReturn(shipAfterModel);

		final Date shipFromDate = Mockito.mock(Date.class);
		Mockito.when(shipAfterModel.getShipFromDate()).thenReturn(shipFromDate);

		final Date shipToDate = Mockito.mock(Date.class);
		Mockito.when(shipAfterModel.getShipToDate()).thenReturn(shipToDate);

		final int fromDatePart = Integer.valueOf(22).intValue();
		Mockito.when(Integer.valueOf(shipFromDate.getDate())).thenReturn(Integer.valueOf(fromDatePart));

		final int fromMonthPart = Integer.valueOf(3).intValue();
		Mockito.when(Integer.valueOf(shipFromDate.getMonth())).thenReturn(Integer.valueOf(fromMonthPart));

		final Date shipAfterDate = Mockito.mock(Date.class);
		Mockito.when(shipAfterModel.getShipAfterDate()).thenReturn(shipAfterDate);

		final TreeMap m = Mockito.mock(TreeMap.class);
		//Mockito.when(catCheckoutFacade.getListOfFormattedShipToDates(shipToDates)).thenReturn(m);
		Assert.assertNotNull((catCheckoutFacade.getListOfFormattedShipToDates(shipToDates)));
	}

	@Test
	public void testGetListOfFormattedShipToDatesMod4()
	{
		final List<ShipToAfterDateModel> shipToDates = Mockito.mock(ArrayList.class);

		final Iterator<ShipToAfterDateModel> shipToDatesItr = Mockito.mock(Iterator.class);
		Mockito.when(shipToDates.iterator()).thenReturn(shipToDatesItr);

		Mockito.when(Boolean.valueOf(shipToDatesItr.hasNext())).thenReturn(Boolean.TRUE, Boolean.FALSE);
		final ShipToAfterDateModel shipAfterModel = Mockito.mock(ShipToAfterDateModel.class);
		shipAfterModel.setShipAfterDate(new Date());
		Mockito.when(shipToDatesItr.next()).thenReturn(shipAfterModel);

		final Date shipFromDate = Mockito.mock(Date.class);
		Mockito.when(shipAfterModel.getShipFromDate()).thenReturn(shipFromDate);

		final Date shipToDate = Mockito.mock(Date.class);
		Mockito.when(shipAfterModel.getShipToDate()).thenReturn(shipToDate);

		final int fromDatePart = Integer.valueOf(24).intValue();
		Mockito.when(Integer.valueOf(shipFromDate.getDate())).thenReturn(Integer.valueOf(fromDatePart));

		final int fromMonthPart = Integer.valueOf(3).intValue();
		Mockito.when(Integer.valueOf(shipFromDate.getMonth())).thenReturn(Integer.valueOf(fromMonthPart));

		final Date shipAfterDate = Mockito.mock(Date.class);
		Mockito.when(shipAfterModel.getShipAfterDate()).thenReturn(shipAfterDate);

		Assert.assertNotNull((catCheckoutFacade.getListOfFormattedShipToDates(shipToDates)));
	}

	@Test
	public void testRetainTruckloads()
	{
		final List<OrderEntryData> oedList = new ArrayList();
		final OrderEntryData oed = new OrderEntryData();
		ProductData prodData = new ProductData();
		prodData.setName("CUV82");
		oed.setProduct(prodData);
		oed.setQuantity(Long.valueOf(8));
		oed.setTruckloadId("123");
		oedList.add(oed);

		prodData = new ProductData();
		prodData.setName("CUV102");
		oed.setProduct(prodData);
		oed.setQuantity(Long.valueOf(6));
		oed.setTruckloadId("456");
		oedList.add(oed);

		prodData = new ProductData();
		prodData.setName("CUV85");
		oed.setProduct(prodData);
		oed.setQuantity(Long.valueOf(6));
		oed.setTruckloadId("456");
		oedList.add(oed);

		final CartData cartData = Mockito.mock(CartData.class);

		Mockito.when(cartData.getEntries()).thenReturn(oedList);

		Assert.assertNotNull(catCheckoutFacade.retainTruckloads(cartData));
	}

	@Test
	public void testRetainTruckloadsIf()
	{
		final List<OrderEntryData> oedList = new ArrayList();
		final OrderEntryData oed = new OrderEntryData();
		ProductData prodData = new ProductData();
		prodData = new ProductData();
		prodData.setName("CUV1022");
		oed.setProduct(prodData);
		oed.setQuantity(Long.valueOf(6));
		oed.setTruckloadId(StringUtils.EMPTY);
		oedList.add(oed);

		final CartData cartData = Mockito.mock(CartData.class);

		Mockito.when(cartData.getEntries()).thenReturn(oedList);

		Assert.assertNotNull(catCheckoutFacade.retainTruckloads(cartData));
	}

	@Test
	public void testRetainTruckloadsElse()
	{
		final List<OrderEntryData> oedList = new ArrayList();
		OrderEntryData oed = new OrderEntryData();
		ProductData prodData = new ProductData();
		prodData.setName("CUV82");
		oed.setProduct(prodData);
		oed.setQuantity(Long.valueOf(8));
		oed.setTruckloadId("123");
		oedList.add(oed);

		oed = new OrderEntryData();
		prodData = new ProductData();
		prodData.setName("CUV102");
		oed.setProduct(prodData);
		oed.setQuantity(Long.valueOf(6));
		oed.setTruckloadId("456");
		oedList.add(oed);

		oed = new OrderEntryData();
		prodData = new ProductData();
		prodData.setName("CUV85");
		oed.setProduct(prodData);
		oed.setQuantity(Long.valueOf(6));
		oed.setTruckloadId("456");
		oedList.add(oed);

		final CartData cartData = Mockito.mock(CartData.class);

		Mockito.when(cartData.getEntries()).thenReturn(oedList);

		Assert.assertNotNull(catCheckoutFacade.retainTruckloads(cartData));
	}

	@Test
	public void testGetTruckloadImage()
	{
		final FlexibleSearchService flexibleSearchService = Mockito.mock(FlexibleSearchService.class);
		final ModelService modelService = Mockito.mock(ModelService.class);
		final MediaModel mediaModel = Mockito.mock(MediaModel.class);
		mediaModel.setCode("truckload_0_6");
		mediaModel.setCatalogVersion(null);
		mediaModel.setUrl("testurl");
		Mockito.when(modelService.create(MediaModel.class)).thenReturn(mediaModel);
		Mockito.when(flexibleSearchService.getModelByExample(mediaModel)).thenReturn(mediaModel);
		//	Assert.assertNotNull(catCheckoutFacade.getTruckloadImage(2, 5));
	}

	@Test
	public void testPopulateTruckloadSuggestions()
	{
		final List<TruckloadModel> truckModelList = new ArrayList();
		final List<TruckloadData> truckDataList = new ArrayList();
		final int[] twoSeaterQtyArr =
		{ 5, 3 };
		final int[] fiveSeaterQtyArr =
		{ 5, 1 };
		Mockito.when(checkoutService.populateTruckloadSuggestions(8, 5)).thenReturn(truckModelList);
		Mockito.when(catTruckloadConverter.convertAll(checkoutService.populateTruckloadSuggestions(2, 5)))
				.thenReturn(truckDataList);
		Assert.assertNotNull(catCheckoutFacade.populateTruckloadSuggestions(twoSeaterQtyArr, fiveSeaterQtyArr));
	}

	@Test
	public void testCalculateTwoSeaters()
	{
		final int[] twoSeaterQtyArr =
		{ 5, 3 };
		Assert.assertEquals(catCheckoutFacade.calculateTwoSeaters(twoSeaterQtyArr), 8);
	}

	@Test
	public void testCalculateTwoSeatersEmpty()
	{
		final int[] twoSeaterQtyArr = {};
		Assert.assertEquals(catCheckoutFacade.calculateTwoSeaters(twoSeaterQtyArr), 0);
	}

	@Test
	public void testCalculateFiveSeaters()
	{
		final int[] fiveSeaterQtyArr =
		{ 5, 1 };
		Assert.assertEquals(catCheckoutFacade.calculateFiveSeaters(fiveSeaterQtyArr), 6);
	}

	@Test
	public void testCalculateFiveSeatersEmpty()
	{
		final int[] fiveSeaterQtyArr = {};
		Assert.assertEquals(catCheckoutFacade.calculateFiveSeaters(fiveSeaterQtyArr), 0);
	}


	@Test
	public void testSetSuggestionMessage()
	{
		final Map<String, Object> resultMap = new HashMap();
		final TruckloadData truckData = new TruckloadData();
		truckData.setFiveSeaterCapacity(Integer.valueOf(3));
		truckData.setTwoSeaterCapacity(Integer.valueOf(4));
		final int[] twoSeaterQtyArr =
		{ 4 };
		final int[] fiveSeaterQtyArr =
		{ 3 };
		final List<TruckloadData> suggestionList = new ArrayList();
		suggestionList.add(truckData);
		Mockito.when(catCheckoutFacade.populateTruckloadSuggestions(twoSeaterQtyArr, fiveSeaterQtyArr)).thenReturn(suggestionList);
		Assert.assertNotNull(catCheckoutFacade.setSuggestionAndMessage(resultMap, twoSeaterQtyArr, fiveSeaterQtyArr));
		//Assert.assertNotNull(catCheckoutFacade.setSuggestionAndMessage(resultMap, twoSeaterQtyArr, fiveSeaterQtyArr));
	}

	@Test
	public void testSetSuggestionMessageNull()
	{
		final Map<String, Object> resultMap = new HashMap();
		final TruckloadData truckData = new TruckloadData();
		truckData.setFiveSeaterCapacity(null);
		truckData.setTwoSeaterCapacity(null);
		final int[] twoSeaterQtyArr =
		{ 5 };
		final int[] fiveSeaterQtyArr =
		{ 3 };

		final List<TruckloadData> suggestionList = new ArrayList();
		suggestionList.add(truckData);
		Mockito.when(catCheckoutFacade.populateTruckloadSuggestions(twoSeaterQtyArr, fiveSeaterQtyArr)).thenReturn(suggestionList);
		Assert.assertEquals(catCheckoutFacade.setSuggestionAndMessage(resultMap, twoSeaterQtyArr, fiveSeaterQtyArr), resultMap);
	}

	@Test
	public void doAddToCart() throws CommerceCartModificationException, ParseException, NoSuchFieldException, SecurityException,
			IllegalArgumentException, IllegalAccessException
	{
		final Map<String, Object> resultMap = new HashMap();
		int[] quantities =
		{ 5, 1 };
		String[] productCodes =
		{ "CUV82", "CUV85" };

		String[] productNames =
		{ "CUV82", "CUV85" };

		final String shipAfterDate = new Date().toString();
		final CatCartFacade cartFacade = Mockito.mock(CatCartFacade.class);
		ReflectionTestUtils.setField(catCheckoutFacade, "cartFacade", cartFacade);
		Mockito.when(cartFacade.getSessionCart()).thenReturn(new CartData());


		final CatCustomerFacade customerFacade = Mockito.mock(CatCustomerFacade.class);
		ReflectionTestUtils.setField(catCheckoutFacade, "catCustomerFacade", customerFacade);


		final CustomerData customerData = new CustomerData();
		final B2BUnitData unit = new B2BUnitData();
		unit.setUid("E140");
		customerData.setUnit(unit);
		Mockito.when(customerFacade.getCurrentCustomer()).thenReturn(customerData);

		Mockito.when(cartFacade.getSessionCart()).thenReturn(new CartData());
		Assert.assertNotNull(
				catCheckoutFacade.doAddToCart(quantities, productCodes, productNames, shipAfterDate, resultMap, "/testImageURL"));

		quantities = new int[]
		{ -1 };
		productCodes = new String[]
		{ "CUV82" };

		productNames = new String[]
		{ "CUV82" };
		Assert.assertNotNull(
				catCheckoutFacade.doAddToCart(quantities, productCodes, productNames, shipAfterDate, resultMap, "/testImageURL"));
	}
}
