/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jun 30, 2017 4:29:12 PM                     ---
 * ----------------------------------------------------------------
 */
package com.cat.bcp.fulfilmentprocess.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedCatFulfilmentProcessConstants
{
	public static final String EXTENSIONNAME = "catfulfilmentprocess";
	public static class Attributes
	{
		public static class ConsignmentProcess
		{
			public static final String DONE = "done".intern();
			public static final String WAITINGFORCONSIGNMENT = "waitingForConsignment".intern();
			public static final String WAREHOUSECONSIGNMENTSTATE = "warehouseConsignmentState".intern();
		}
	}
	
	protected GeneratedCatFulfilmentProcessConstants()
	{
		// private constructor
	}
	
	
}
