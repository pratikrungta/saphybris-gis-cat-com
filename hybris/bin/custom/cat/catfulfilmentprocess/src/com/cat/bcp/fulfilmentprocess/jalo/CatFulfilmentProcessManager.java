/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.cat.bcp.fulfilmentprocess.constants.CatFulfilmentProcessConstants;

@SuppressWarnings("PMD")
public class CatFulfilmentProcessManager extends GeneratedCatFulfilmentProcessManager
{
	public static final CatFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (CatFulfilmentProcessManager) em.getExtension(CatFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
