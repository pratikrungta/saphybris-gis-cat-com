/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jun 30, 2017 4:29:12 PM                     ---
 * ----------------------------------------------------------------
 */
package com.cat.bcp.initialdata.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedCatInitialDataConstants
{
	public static final String EXTENSIONNAME = "catinitialdata";
	
	protected GeneratedCatInitialDataConstants()
	{
		// private constructor
	}
	
	
}
