##Below is script which needs to be run in groovy before importing utv_products.impex (from codebase)
##This script removes the product references for all utv accessories

import de.hybris.platform.cronjob.enums.*
import de.hybris.platform.servicelayer.cronjob.PerformResult
import de.hybris.platform.servicelayer.search.*
import de.hybris.platform.servicelayer.model.*
  
searchService = spring.getBean("flexibleSearchService")
modelService = spring.getBean("modelService")
query = new FlexibleSearchQuery("SELECT {ref.pk} FROM {ProductReference as ref JOIN Product as product on {product.pk}={ref.source} JOIN PartVariantProduct as part on {part.pk}={ref.target}}, {category as cat}, {CategoryProductRelation as rel} WHERE {cat.code} = 'utv' AND {cat.pk}={rel.source} AND {product.pk}={rel.target} AND {ref.referenceType} in ({{ select {pk} from {ProductReferenceTypeEnum} where {code}='ACCESSORIES' }})");
searchService.search(query).getResult().each {
  modelService.remove(it)
}
