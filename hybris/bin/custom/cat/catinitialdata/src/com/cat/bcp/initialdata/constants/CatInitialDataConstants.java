/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.initialdata.constants;

/**
 * Global class for all CatInitialData constants.
 */
public final class CatInitialDataConstants extends GeneratedCatInitialDataConstants
{
	public static final String EXTENSIONNAME = "catinitialdata";

	private CatInitialDataConstants()
	{
		//empty
	}
}
