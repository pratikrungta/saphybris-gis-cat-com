package com.cat.core.integration;

import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.product.ProductModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;


public class CatIntegrationUtils
{

    private CatIntegrationUtils()
    {
        throw new IllegalStateException("No instance of Utility class should be create");
    }

    public static String getdateStringFormat(final Date date)
    {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        return simpleDateFormat.format(date);
    }



    public static ProductModel getProduct(AbstractOrderEntryModel orderEntry) {

        final ConfigVariantProductModel configVariantProductModel = (ConfigVariantProductModel) orderEntry.getProduct();
        final BaseVariantProductModel baseVariantProductModel = (BaseVariantProductModel) configVariantProductModel
                .getBaseProduct();
        return baseVariantProductModel.getBaseProduct();
    }

    /**
     *
     * @param value the string containg the lsit of concatenated dealercode and shipment terms
     * @return Map<String,String>
     *              key is dealer code
     *              Value is payment terms value corresponding to dealer code
     */
    public static Map<String,String> getvaluesMap(String value) {
        Map<String,String> valueMap=new HashMap<>();
        String [] dealer =value.split("&&");
        for(int dealerinfo = 0; dealerinfo< dealer.length; dealerinfo++ ){
                String [] info= dealer[dealerinfo].split(Pattern.quote("|"));
            valueMap.put(info[0],info[1]);

        }
        return valueMap;

    }
}
