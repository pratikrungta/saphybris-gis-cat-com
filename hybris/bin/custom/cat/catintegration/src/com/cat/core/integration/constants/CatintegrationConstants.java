package com.cat.core.integration.constants;

@SuppressWarnings("PMD")
public class CatintegrationConstants extends GeneratedCatintegrationConstants
{
	//FBC Constants.
	public static final String EXTENSIONNAME = "catintegration";
	
	public static final String WS_BASE_URL = "cat.ws.base.url";
	public static final String WS_CREATE_URL = "cat.ws.base.url.create";
	public static final String WS_SELECT_URL = "cat.ws.base.url.select";
	public static final String WS_NEXTSELECT_URL = "cat.ws.base.url.nextselect";
	public static final String WS_FEATUREID_URL = "cat.ws.base.url.featureidurl";

	public static final String WS_ACCESSTOKENURI = "cat.ws.base.url.accesstokenuri";
	public static final String WS_CLIENTID = "cat.ws.base.url.clientid";
	public static final String WS_CLIENTSECRET = "cat.ws.base.url.clientsecret";
	public static final String WS_GRANTTYPE = "cat.ws.base.url.granttype";
	public static final String LANE1_MSG = "DCA does not belong to LANE1.";
	public static final String FBC_URI_MSG = "Endpoint is not available.Please check if it is configured ";
	public static final String EXCEPTION_OCCURED = "Exception occured :";
	public static final String LANE1="LANE1";
	public static final String FBC_PROXYURL = "fbc.proxy.url";
	public static final String FBC_PROXYENABLED = "fbc.proxy.enabled";
	public static final String FBC_PROXYPORT = "fbc.proxy.port";
	
	public static final String STATUS_URL = "https://apistage.catdevservices.com/template/status";
	public static final String PING_URL ="https://apistage.catdevservices.com/template/ping" ;
	public static final String INVENTORY_COUNT ="http://localhost:8080/finishedgoodsinventory/counts?";
	public static final String PARTYIDS ="partyIds=" ;
	public static final String SERIALNUMBERS="serialNumbers=";
	public static final String SERIALNUMBERPREFIXES="serialNumberPrefixes=";
	public static final String GENERALARRANGMENTS="generalArrangements=";
	public static final String SALESMODELS="salesModels=";
	public static final String BASESALESMODELS = "baseSalesModels=";
	public static final String SALESMODELSUFFIXES = "salesModelSuffixes=";
	public static final String SALESMODELSERIES = "salesModelSeries=";
	public static final String SALESMODELVARIANCE = "salesModelVarianceClasses=";
	public static final String PRODUCTLINES = "productLines=";
	public static final String PRODUCTDIVISIONS = "productDivisions=";
	public static final String SOURCEFACILITIES = "sourceFacilities=";
	public static final String COUNTRIES = "countries=";
	public static final String STAGES = "stages=";
	public static final String CONFIGIDS = "configIds=";
	public static final String GET_DESCENDANT = "&getDescendantNodes=";
	public static final String CONFIG_ID = "&configId=";
	public static final String DESCENDANTNODES="getDescendantNodes=";
	public static final String TRUE ="true";
	
	public static final String EDH_INVENTORY_DETAILS_URI="cat.ws.edh.base.url.details";
	public static final String EDH_BASE_URL="cat.ws.edh.base.url";
	public static final String EDH_PING_URI="cat.ws.edh.base.url.ping";
	public static final String EDH_STATUS_URI="cat.ws.edh.base.url.status";
	public static final String EDH_COUNT_URI="cat.ws.edh.base.url.count";
	public static final String EDH_TOKEN="cat.ws.edh.base.url.token";
	public static final String LEFT_BRACKET="\\[";
	public static final String RIGHT_BRACKET="\\]";
	public static final String AND_SYMBOL="&";
	public static final String OPENID_ACCESS_TOKEN = "OIDAT";
	public static final String EDH_INVENTORY_STAGES="cat.ws.edh.base.url.statges";
	public static final String NO_RESULTS = "cat.ws.edh.noresults.msg";
	public static final String ERROR_MSG = "cat.ws.edh.error.msg";
	public static final String ERROR_MSG_MAILID="cat.ws.edh.error.msg.mailid";
	
	
	
	
	
	private CatintegrationConstants()
	{
		//empty
	}
	
	
}
