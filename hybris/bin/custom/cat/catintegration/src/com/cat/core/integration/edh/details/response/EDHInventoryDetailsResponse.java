
package com.cat.core.integration.edh.details.response;

import java.util.HashMap;
import java.util.Map;

import com.cat.core.integration.edh.response.EDHError;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "serialNumber",
    "serialNumberPrefix",
    "reserved",
    "salesModel",
    "mso",
    "dealerOrderNumber",
    "generalArrangementNumber",
    "productLineCode",
    "productLineDescription",
    "productDivision",
    "productFamily",
    "sourceFacilityCode",
    "currentLocation",
    "party",
    "commitmentStatus",
    "tradeLevel",
    "contact",
    "stage",
    "age",
    "currentStageAge",
    "manufacturingModelYear",
    "configId",
    "estimatedDeliveryDate",
    "lane",
    "marketingRegion",
    "edhError"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class EDHInventoryDetailsResponse {

    @JsonProperty("serialNumber")
    private String serialNumber;
    @JsonProperty("serialNumberPrefix")
    private String serialNumberPrefix;
    @JsonProperty("reserved")
    private String  reserved;
    
    @JsonProperty("edhError")
    private EDHError edhError; 
    /**
     * @return the edhError
     */
    public EDHError getEdhError() {
        return edhError;
    }

    /**
     * @param edhError the edhError to set
     */
    public void setEdhError(EDHError edhError) {
        this.edhError = edhError;
    }

    /**
     * @return the reserved
     */
    public String getReserved() {
        return reserved;
    }

    /**
     * @param reserved the reserved to set
     */
    public void setReserved(String reserved) {
        this.reserved = reserved;
    }

    @JsonProperty("salesModel")
    private SalesModel salesModel;
    @JsonProperty("mso")
    private String mso;
    @JsonProperty("dealerOrderNumber")
    private Object dealerOrderNumber;
    @JsonProperty("generalArrangementNumber")
    private String generalArrangementNumber;
    @JsonProperty("productLineCode")
    private String productLineCode;
    @JsonProperty("productLineDescription")
    private String productLineDescription;
    @JsonProperty("productDivision")
    private Object productDivision;
    @JsonProperty("productFamily")
    private String productFamily;
    @JsonProperty("sourceFacilityCode")
    private String sourceFacilityCode;
    @JsonProperty("currentLocation")
    private CurrentLocation currentLocation;
    @JsonProperty("party")
    private Party party;
    @JsonProperty("commitmentStatus")
    private Object commitmentStatus;
    @JsonProperty("tradeLevel")
    private Object tradeLevel;
    @JsonProperty("contact")
    private Contact contact;
    @JsonProperty("stage")
    private Stage stage;
    @JsonProperty("age")
    private Object age;
    @JsonProperty("currentStageAge")
    private Double currentStageAge;
    @JsonProperty("manufacturingModelYear")
    private Object manufacturingModelYear;
    @JsonProperty("configId")
    private String configId;
    @JsonProperty("estimatedDeliveryDate")
    private Object estimatedDeliveryDate;
    @JsonProperty("lane")
    private String lane;
    @JsonProperty("marketingRegion")
    private Object marketingRegion;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("serialNumber")
    public String getSerialNumber() {
        return serialNumber;
    }

    @JsonProperty("serialNumber")
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @JsonProperty("serialNumberPrefix")
    public String getSerialNumberPrefix() {
        return serialNumberPrefix;
    }

    @JsonProperty("serialNumberPrefix")
    public void setSerialNumberPrefix(String serialNumberPrefix) {
        this.serialNumberPrefix = serialNumberPrefix;
    }

    @JsonProperty("salesModel")
    public SalesModel getSalesModel() {
        return salesModel;
    }

    @JsonProperty("salesModel")
    public void setSalesModel(SalesModel salesModel) {
        this.salesModel = salesModel;
    }

    @JsonProperty("mso")
    public String getMso() {
        return mso;
    }

    @JsonProperty("mso")
    public void setMso(String mso) {
        this.mso = mso;
    }

    @JsonProperty("dealerOrderNumber")
    public Object getDealerOrderNumber() {
        return dealerOrderNumber;
    }

    @JsonProperty("dealerOrderNumber")
    public void setDealerOrderNumber(Object dealerOrderNumber) {
        this.dealerOrderNumber = dealerOrderNumber;
    }

    @JsonProperty("generalArrangementNumber")
    public String getGeneralArrangementNumber() {
        return generalArrangementNumber;
    }

    @JsonProperty("generalArrangementNumber")
    public void setGeneralArrangementNumber(String generalArrangementNumber) {
        this.generalArrangementNumber = generalArrangementNumber;
    }

    @JsonProperty("productLineCode")
    public String getProductLineCode() {
        return productLineCode;
    }

    @JsonProperty("productLineCode")
    public void setProductLineCode(String productLineCode) {
        this.productLineCode = productLineCode;
    }

    @JsonProperty("productLineDescription")
    public String getProductLineDescription() {
        return productLineDescription;
    }

    @JsonProperty("productLineDescription")
    public void setProductLineDescription(String productLineDescription) {
        this.productLineDescription = productLineDescription;
    }

    @JsonProperty("productDivision")
    public Object getProductDivision() {
        return productDivision;
    }

    @JsonProperty("productDivision")
    public void setProductDivision(Object productDivision) {
        this.productDivision = productDivision;
    }

    @JsonProperty("productFamily")
    public String getProductFamily() {
        return productFamily;
    }

    @JsonProperty("productFamily")
    public void setProductFamily(String productFamily) {
        this.productFamily = productFamily;
    }

    @JsonProperty("sourceFacilityCode")
    public String getSourceFacilityCode() {
        return sourceFacilityCode;
    }

    @JsonProperty("sourceFacilityCode")
    public void setSourceFacilityCode(String sourceFacilityCode) {
        this.sourceFacilityCode = sourceFacilityCode;
    }

    @JsonProperty("currentLocation")
    public CurrentLocation getCurrentLocation() {
        return currentLocation;
    }

    @JsonProperty("currentLocation")
    public void setCurrentLocation(CurrentLocation currentLocation) {
        this.currentLocation = currentLocation;
    }

    @JsonProperty("party")
    public Party getParty() {
        return party;
    }

    @JsonProperty("party")
    public void setParty(Party party) {
        this.party = party;
    }

    @JsonProperty("commitmentStatus")
    public Object getCommitmentStatus() {
        return commitmentStatus;
    }

    @JsonProperty("commitmentStatus")
    public void setCommitmentStatus(Object commitmentStatus) {
        this.commitmentStatus = commitmentStatus;
    }

    @JsonProperty("tradeLevel")
    public Object getTradeLevel() {
        return tradeLevel;
    }

    @JsonProperty("tradeLevel")
    public void setTradeLevel(Object tradeLevel) {
        this.tradeLevel = tradeLevel;
    }

    @JsonProperty("contact")
    public Contact getContact() {
        return contact;
    }

    @JsonProperty("contact")
    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @JsonProperty("stage")
    public Stage getStage() {
        return stage;
    }

    @JsonProperty("stage")
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @JsonProperty("age")
    public Object getAge() {
        return age;
    }

    @JsonProperty("age")
    public void setAge(Object age) {
        this.age = age;
    }

    @JsonProperty("currentStageAge")
    public Double getCurrentStageAge() {
        return currentStageAge;
    }

    @JsonProperty("currentStageAge")
    public void setCurrentStageAge(Double currentStageAge) {
        this.currentStageAge = currentStageAge;
    }

    @JsonProperty("manufacturingModelYear")
    public Object getManufacturingModelYear() {
        return manufacturingModelYear;
    }

    @JsonProperty("manufacturingModelYear")
    public void setManufacturingModelYear(Object manufacturingModelYear) {
        this.manufacturingModelYear = manufacturingModelYear;
    }

    @JsonProperty("configId")
    public String getConfigId() {
        return configId;
    }

    @JsonProperty("configId")
    public void setConfigId(String configId) {
        this.configId = configId;
    }

    @JsonProperty("estimatedDeliveryDate")
    public Object getEstimatedDeliveryDate() {
        return estimatedDeliveryDate;
    }

    @JsonProperty("estimatedDeliveryDate")
    public void setEstimatedDeliveryDate(Object estimatedDeliveryDate) {
        this.estimatedDeliveryDate = estimatedDeliveryDate;
    }

    @JsonProperty("lane")
    public String getLane() {
        return lane;
    }

    @JsonProperty("lane")
    public void setLane(String lane) {
        this.lane = lane;
    }

    @JsonProperty("marketingRegion")
    public Object getMarketingRegion() {
        return marketingRegion;
    }

    @JsonProperty("marketingRegion")
    public void setMarketingRegion(Object marketingRegion) {
        this.marketingRegion = marketingRegion;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
