
package com.cat.core.integration.edh.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "serialNumber",
    "serialNumberPrefix",
    "salesModel",
    "mso",
    "dealerOrderNumber",
    "generalArrangementNumber",
    "productLine",
    "productDivision",
    "productFamily",
    "sourceFacilityCode",
    "currentLocation",
    "party",
    "commitmentStatus",
    "tradeLevel",
    "contact",
    "totalChainStage",
    "age",
    "manufacturingModelYear",
    "configId",
    "estimatedDeliveryDate"
})
public class FinishedGoodsInventoryDetails {

    @JsonProperty("serialNumber")
    private String serialNumber;
    @JsonProperty("serialNumberPrefix")
    private String serialNumberPrefix;
    @JsonProperty("salesModel")
    private SalesModel salesModel;
    @JsonProperty("mso")
    private String mso;
    @JsonProperty("dealerOrderNumber")
    private String dealerOrderNumber;
    @JsonProperty("generalArrangementNumber")
    private String generalArrangementNumber;
    @JsonProperty("productLine")
    private String productLine;
    @JsonProperty("productDivision")
    private String productDivision;
    @JsonProperty("productFamily")
    private String productFamily;
    @JsonProperty("sourceFacilityCode")
    private String sourceFacilityCode;
    @JsonProperty("currentLocation")
    private CurrentLocation currentLocation;
    @JsonProperty("party")
    private Party party;
    @JsonProperty("commitmentStatus")
    private String commitmentStatus;
    @JsonProperty("tradeLevel")
    private String tradeLevel;
    @JsonProperty("contact")
    private Contact contact;
    @JsonProperty("totalChainStage")
    private TotalChainStage totalChainStage;
    @JsonProperty("age")
    private Integer age;
    @JsonProperty("manufacturingModelYear")
    private String manufacturingModelYear;
    @JsonProperty("configId")
    private String configId;
    @JsonProperty("estimatedDeliveryDate")
    private String estimatedDeliveryDate;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("serialNumber")
    public String getSerialNumber() {
        return serialNumber;
    }

    @JsonProperty("serialNumber")
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @JsonProperty("serialNumberPrefix")
    public String getSerialNumberPrefix() {
        return serialNumberPrefix;
    }

    @JsonProperty("serialNumberPrefix")
    public void setSerialNumberPrefix(String serialNumberPrefix) {
        this.serialNumberPrefix = serialNumberPrefix;
    }

    @JsonProperty("salesModel")
    public SalesModel getSalesModel() {
        return salesModel;
    }

    @JsonProperty("salesModel")
    public void setSalesModel(SalesModel salesModel) {
        this.salesModel = salesModel;
    }

    @JsonProperty("mso")
    public String getMso() {
        return mso;
    }

    @JsonProperty("mso")
    public void setMso(String mso) {
        this.mso = mso;
    }

    @JsonProperty("dealerOrderNumber")
    public String getDealerOrderNumber() {
        return dealerOrderNumber;
    }

    @JsonProperty("dealerOrderNumber")
    public void setDealerOrderNumber(String dealerOrderNumber) {
        this.dealerOrderNumber = dealerOrderNumber;
    }

    @JsonProperty("generalArrangementNumber")
    public String getGeneralArrangementNumber() {
        return generalArrangementNumber;
    }

    @JsonProperty("generalArrangementNumber")
    public void setGeneralArrangementNumber(String generalArrangementNumber) {
        this.generalArrangementNumber = generalArrangementNumber;
    }

    @JsonProperty("productLine")
    public String getProductLine() {
        return productLine;
    }

    @JsonProperty("productLine")
    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }

    @JsonProperty("productDivision")
    public String getProductDivision() {
        return productDivision;
    }

    @JsonProperty("productDivision")
    public void setProductDivision(String productDivision) {
        this.productDivision = productDivision;
    }

    @JsonProperty("productFamily")
    public String getProductFamily() {
        return productFamily;
    }

    @JsonProperty("productFamily")
    public void setProductFamily(String productFamily) {
        this.productFamily = productFamily;
    }

    @JsonProperty("sourceFacilityCode")
    public String getSourceFacilityCode() {
        return sourceFacilityCode;
    }

    @JsonProperty("sourceFacilityCode")
    public void setSourceFacilityCode(String sourceFacilityCode) {
        this.sourceFacilityCode = sourceFacilityCode;
    }

    @JsonProperty("currentLocation")
    public CurrentLocation getCurrentLocation() {
        return currentLocation;
    }

    @JsonProperty("currentLocation")
    public void setCurrentLocation(CurrentLocation currentLocation) {
        this.currentLocation = currentLocation;
    }

    @JsonProperty("party")
    public Party getParty() {
        return party;
    }

    @JsonProperty("party")
    public void setParty(Party party) {
        this.party = party;
    }

    @JsonProperty("commitmentStatus")
    public String getCommitmentStatus() {
        return commitmentStatus;
    }

    @JsonProperty("commitmentStatus")
    public void setCommitmentStatus(String commitmentStatus) {
        this.commitmentStatus = commitmentStatus;
    }

    @JsonProperty("tradeLevel")
    public String getTradeLevel() {
        return tradeLevel;
    }

    @JsonProperty("tradeLevel")
    public void setTradeLevel(String tradeLevel) {
        this.tradeLevel = tradeLevel;
    }

    @JsonProperty("contact")
    public Contact getContact() {
        return contact;
    }

    @JsonProperty("contact")
    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @JsonProperty("totalChainStage")
    public TotalChainStage getTotalChainStage() {
        return totalChainStage;
    }

    @JsonProperty("totalChainStage")
    public void setTotalChainStage(TotalChainStage totalChainStage) {
        this.totalChainStage = totalChainStage;
    }

    @JsonProperty("age")
    public Integer getAge() {
        return age;
    }

    @JsonProperty("age")
    public void setAge(Integer age) {
        this.age = age;
    }

    @JsonProperty("manufacturingModelYear")
    public String getManufacturingModelYear() {
        return manufacturingModelYear;
    }

    @JsonProperty("manufacturingModelYear")
    public void setManufacturingModelYear(String manufacturingModelYear) {
        this.manufacturingModelYear = manufacturingModelYear;
    }

    @JsonProperty("configId")
    public String getConfigId() {
        return configId;
    }

    @JsonProperty("configId")
    public void setConfigId(String configId) {
        this.configId = configId;
    }

    @JsonProperty("estimatedDeliveryDate")
    public String getEstimatedDeliveryDate() {
        return estimatedDeliveryDate;
    }

    @JsonProperty("estimatedDeliveryDate")
    public void setEstimatedDeliveryDate(String estimatedDeliveryDate) {
        this.estimatedDeliveryDate = estimatedDeliveryDate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
