
package com.cat.core.integration.edh.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "host",
    "environment",
    "apiproxy",
    "basepath",
    "client",
    "time",
    "latency",
    "message"
})
public class PingResponse {

    @JsonProperty("host")
    private String host;
    @JsonProperty("environment")
    private String environment;
    @JsonProperty("apiproxy")
    private String apiproxy;
    @JsonProperty("basepath")
    private String basepath;
    @JsonProperty("client")
    private String client;
    @JsonProperty("time")
    private String time;
    @JsonProperty("latency")
    private Double latency;
    @JsonProperty("message")
    private String message;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("host")
    public String getHost() {
        return host;
    }

    @JsonProperty("host")
    public void setHost(String host) {
        this.host = host;
    }

    @JsonProperty("environment")
    public String getEnvironment() {
        return environment;
    }

    @JsonProperty("environment")
    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    @JsonProperty("apiproxy")
    public String getApiproxy() {
        return apiproxy;
    }

    @JsonProperty("apiproxy")
    public void setApiproxy(String apiproxy) {
        this.apiproxy = apiproxy;
    }

    @JsonProperty("basepath")
    public String getBasepath() {
        return basepath;
    }

    @JsonProperty("basepath")
    public void setBasepath(String basepath) {
        this.basepath = basepath;
    }

    @JsonProperty("client")
    public String getClient() {
        return client;
    }

    @JsonProperty("client")
    public void setClient(String client) {
        this.client = client;
    }

    @JsonProperty("time")
    public String getTime() {
        return time;
    }

    @JsonProperty("time")
    public void setTime(String time) {
        this.time = time;
    }

    @JsonProperty("latency")
    public Double getLatency() {
        return latency;
    }

    @JsonProperty("latency")
    public void setLatency(Double latency) {
        this.latency = latency;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
