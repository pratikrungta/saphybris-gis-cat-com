
package com.cat.core.integration.edh.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "salesModelAndModifier",
    "baseSalesModel",
    "salesModelSuffix",
    "salesModelSeries",
    "salesModelVarianceClass"
})
public class SalesModel {

    @JsonProperty("salesModelAndModifier")
    private String salesModelAndModifier;
    @JsonProperty("baseSalesModel")
    private String baseSalesModel;
    @JsonProperty("salesModelSuffix")
    private String salesModelSuffix;
    @JsonProperty("salesModelSeries")
    private String salesModelSeries;
    @JsonProperty("salesModelVarianceClass")
    private String salesModelVarianceClass;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("salesModelAndModifier")
    public String getSalesModelAndModifier() {
        return salesModelAndModifier;
    }

    @JsonProperty("salesModelAndModifier")
    public void setSalesModelAndModifier(String salesModelAndModifier) {
        this.salesModelAndModifier = salesModelAndModifier;
    }

    @JsonProperty("baseSalesModel")
    public String getBaseSalesModel() {
        return baseSalesModel;
    }

    @JsonProperty("baseSalesModel")
    public void setBaseSalesModel(String baseSalesModel) {
        this.baseSalesModel = baseSalesModel;
    }

    @JsonProperty("salesModelSuffix")
    public String getSalesModelSuffix() {
        return salesModelSuffix;
    }

    @JsonProperty("salesModelSuffix")
    public void setSalesModelSuffix(String salesModelSuffix) {
        this.salesModelSuffix = salesModelSuffix;
    }

    @JsonProperty("salesModelSeries")
    public String getSalesModelSeries() {
        return salesModelSeries;
    }

    @JsonProperty("salesModelSeries")
    public void setSalesModelSeries(String salesModelSeries) {
        this.salesModelSeries = salesModelSeries;
    }

    @JsonProperty("salesModelVarianceClass")
    public String getSalesModelVarianceClass() {
        return salesModelVarianceClass;
    }

    @JsonProperty("salesModelVarianceClass")
    public void setSalesModelVarianceClass(String salesModelVarianceClass) {
        this.salesModelVarianceClass = salesModelVarianceClass;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
