
package com.cat.core.integration.edh.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "environment",
    "apiproxy",
    "basepath",
    "suffix",
    "client",
    "time",
    "proxyLatency",
    "targetLatency",
    "latency",
    "message",
    "backendMessage"
})
public class StatusResponse {

    @JsonProperty("environment")
    private String environment;
    @JsonProperty("apiproxy")
    private String apiproxy;
    @JsonProperty("basepath")
    private String basepath;
    @JsonProperty("suffix")
    private String suffix;
    @JsonProperty("client")
    private String client;
    @JsonProperty("time")
    private String time;
    @JsonProperty("proxyLatency")
    private Double proxyLatency;
    @JsonProperty("targetLatency")
    private Double targetLatency;
    @JsonProperty("latency")
    private Double latency;
    @JsonProperty("message")
    private String message;
    @JsonProperty("backendMessage")
    private BackendMessage backendMessage;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("environment")
    public String getEnvironment() {
        return environment;
    }

    @JsonProperty("environment")
    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    @JsonProperty("apiproxy")
    public String getApiproxy() {
        return apiproxy;
    }

    @JsonProperty("apiproxy")
    public void setApiproxy(String apiproxy) {
        this.apiproxy = apiproxy;
    }

    @JsonProperty("basepath")
    public String getBasepath() {
        return basepath;
    }

    @JsonProperty("basepath")
    public void setBasepath(String basepath) {
        this.basepath = basepath;
    }

    @JsonProperty("suffix")
    public String getSuffix() {
        return suffix;
    }

    @JsonProperty("suffix")
    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    @JsonProperty("client")
    public String getClient() {
        return client;
    }

    @JsonProperty("client")
    public void setClient(String client) {
        this.client = client;
    }

    @JsonProperty("time")
    public String getTime() {
        return time;
    }

    @JsonProperty("time")
    public void setTime(String time) {
        this.time = time;
    }

    @JsonProperty("proxyLatency")
    public Double getProxyLatency() {
        return proxyLatency;
    }

    @JsonProperty("proxyLatency")
    public void setProxyLatency(Double proxyLatency) {
        this.proxyLatency = proxyLatency;
    }

    @JsonProperty("targetLatency")
    public Double getTargetLatency() {
        return targetLatency;
    }

    @JsonProperty("targetLatency")
    public void setTargetLatency(Double targetLatency) {
        this.targetLatency = targetLatency;
    }

    @JsonProperty("latency")
    public Double getLatency() {
        return latency;
    }

    @JsonProperty("latency")
    public void setLatency(Double latency) {
        this.latency = latency;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("backendMessage")
    public BackendMessage getBackendMessage() {
        return backendMessage;
    }

    @JsonProperty("backendMessage")
    public void setBackendMessage(BackendMessage backendMessage) {
        this.backendMessage = backendMessage;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
