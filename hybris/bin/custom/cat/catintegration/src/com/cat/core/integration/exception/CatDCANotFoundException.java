package com.cat.core.integration.exception;

public class CatDCANotFoundException extends Exception {

		/**
	 * 
	 */
	private static final long serialVersionUID = 6823284280771918401L;

		public CatDCANotFoundException(final String message)
	    {
	        super(message);
	    }

	    public CatDCANotFoundException(final Throwable cause)
	    {
	        super(cause);
	    }

	    public CatDCANotFoundException(final String message, final Throwable cause)
	    {
	        super(message, cause);
	    }
}
