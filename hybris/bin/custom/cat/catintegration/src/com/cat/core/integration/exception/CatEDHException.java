package com.cat.core.integration.exception;

public class CatEDHException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CatEDHException(final String message)
	{
		super(message);
	}

	public CatEDHException(final Throwable cause)
	{
		super(cause);
	}

	public CatEDHException(final String message, final Throwable cause)
	{
		super(message, cause);
	}
}
