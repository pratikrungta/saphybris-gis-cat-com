package com.cat.core.integration.exception;


/**
 * @author sjeedula
 *
 */
public class CatRecordNotFoundException extends CatEDHException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8074911161699642517L;

	public CatRecordNotFoundException(final String message)
    {
        super(message);
    }

    public CatRecordNotFoundException(final Throwable cause)
    {
        super(cause);
    }

    public CatRecordNotFoundException(final String message, final Throwable cause)
    {
        super(message, cause);
    }

}
