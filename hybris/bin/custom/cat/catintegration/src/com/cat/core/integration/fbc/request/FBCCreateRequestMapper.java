
package com.cat.core.integration.fbc.request;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "featureFileId",
    "marketingCompany",
    "dealerCode",
    "mode"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class FBCCreateRequestMapper {

    @JsonProperty("featureFileId")
    private String featureFileId;
    @JsonProperty("marketingCompany")
    private String marketingCompany;
    @JsonProperty("dealerCode")
    private String dealerCode;
    @JsonProperty("mode")
    private Integer mode;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();//NOSONAR

    @JsonProperty("featureFileId")
    public String getFeatureFileId() {
        return featureFileId;
    }

    @JsonProperty("featureFileId")
    public void setFeatureFileId(String featureFileId) {
        this.featureFileId = featureFileId;
    }

    @JsonProperty("marketingCompany")
    public String getMarketingCompany() {
        return marketingCompany;
    }

    @JsonProperty("marketingCompany")
    public void setMarketingCompany(String marketingCompany) {
        this.marketingCompany = marketingCompany;
    }

    @JsonProperty("dealerCode")
    public String getDealerCode() {
        return dealerCode;
    }

    @JsonProperty("dealerCode")
    public void setDealerCode(String dealerCode) {
        this.dealerCode = dealerCode;
    }

    @JsonProperty("mode")
    public Integer getMode() {
        return mode;
    }

    @JsonProperty("mode")
    public void setMode(Integer mode) {
        this.mode = mode;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
