
package com.cat.core.integration.fbc.request;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "selected",
    "isRequired",
    "selectionText",
    "featureCode",
    "quantity",
    "assemblyIndicator",
    "shipLessIndicator",
    "sourceBuilding",
    "description",
    "referenceNumber",
    "listPrice",
    "isNet",
    "extendedDescription",
    "shipWeight",
    "extendedListPrice",
    "featureSet"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Feature //NOSONAR
{

    @JsonProperty("selected")
    private Boolean selected;
    @JsonProperty("isRequired")
    private Boolean isRequired;
    @JsonProperty("selectionText")
    private String selectionText;
    @JsonProperty("featureCode")
    private String featureCode;
    @JsonProperty("quantity")
    private Integer quantity;
    @JsonProperty("assemblyIndicator")
    private String assemblyIndicator;
    @JsonProperty("shipLessIndicator")
    private String shipLessIndicator;
    @JsonProperty("sourceBuilding")
    private String sourceBuilding;
    @JsonProperty("description")
    private String description;
    @JsonProperty("referenceNumber")
    private String referenceNumber;
    @JsonProperty("listPrice")
    private Integer listPrice;
    @JsonProperty("isNet")
    private Boolean isNet;
    @JsonProperty("extendedDescription")
    private String extendedDescription;
    @JsonProperty("shipWeight")
    private Integer shipWeight;
    @JsonProperty("extendedListPrice")
    private Integer extendedListPrice;
    @JsonProperty("featureSet")
    private Object featureSet;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();//NOSONAR

    @JsonProperty("selected")
    public Boolean getSelected() {
        return selected;
    }

    @JsonProperty("selected")
    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    @JsonProperty("isRequired")
    public Boolean getIsRequired() {
        return isRequired;
    }

    @JsonProperty("isRequired")
    public void setIsRequired(Boolean isRequired) {
        this.isRequired = isRequired;
    }

    @JsonProperty("selectionText")
    public String getSelectionText() {
        return selectionText;
    }

    @JsonProperty("selectionText")
    public void setSelectionText(String selectionText) {
        this.selectionText = selectionText;
    }

    @JsonProperty("featureCode")
    public String getFeatureCode() {
        return featureCode;
    }

    @JsonProperty("featureCode")
    public void setFeatureCode(String featureCode) {
        this.featureCode = featureCode;
    }

    @JsonProperty("quantity")
    public Integer getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("assemblyIndicator")
    public String getAssemblyIndicator() {
        return assemblyIndicator;
    }

    @JsonProperty("assemblyIndicator")
    public void setAssemblyIndicator(String assemblyIndicator) {
        this.assemblyIndicator = assemblyIndicator;
    }

    @JsonProperty("shipLessIndicator")
    public String getShipLessIndicator() {
        return shipLessIndicator;
    }

    @JsonProperty("shipLessIndicator")
    public void setShipLessIndicator(String shipLessIndicator) {
        this.shipLessIndicator = shipLessIndicator;
    }

    @JsonProperty("sourceBuilding")
    public String getSourceBuilding() {
        return sourceBuilding;
    }

    @JsonProperty("sourceBuilding")
    public void setSourceBuilding(String sourceBuilding) {
        this.sourceBuilding = sourceBuilding;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("referenceNumber")
    public String getReferenceNumber() {
        return referenceNumber;
    }

    @JsonProperty("referenceNumber")
    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    @JsonProperty("listPrice")
    public Integer getListPrice() {
        return listPrice;
    }

    @JsonProperty("listPrice")
    public void setListPrice(Integer listPrice) {
        this.listPrice = listPrice;
    }

    @JsonProperty("isNet")
    public Boolean getIsNet() {
        return isNet;
    }

    @JsonProperty("isNet")
    public void setIsNet(Boolean isNet) {
        this.isNet = isNet;
    }

    @JsonProperty("extendedDescription")
    public String getExtendedDescription() {
        return extendedDescription;
    }

    @JsonProperty("extendedDescription")
    public void setExtendedDescription(String extendedDescription) {
        this.extendedDescription = extendedDescription;
    }

    @JsonProperty("shipWeight")
    public Integer getShipWeight() {
        return shipWeight;
    }

    @JsonProperty("shipWeight")
    public void setShipWeight(Integer shipWeight) {
        this.shipWeight = shipWeight;
    }

    @JsonProperty("extendedListPrice")
    public Integer getExtendedListPrice() {
        return extendedListPrice;
    }

    @JsonProperty("extendedListPrice")
    public void setExtendedListPrice(Integer extendedListPrice) {
        this.extendedListPrice = extendedListPrice;
    }

    @JsonProperty("featureSet")
    public Object getFeatureSet() {
        return featureSet;
    }

    @JsonProperty("featureSet")
    public void setFeatureSet(Object featureSet) {
        this.featureSet = featureSet;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    @Override
   	public String toString()
   	{
       	 return this.referenceNumber;
   	}
}
