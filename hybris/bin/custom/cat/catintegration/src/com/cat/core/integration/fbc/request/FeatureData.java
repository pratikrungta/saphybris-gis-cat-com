package com.cat.core.integration.fbc.request;
public class FeatureData {
	String salesModel;
	String firstFeatureSet;
	String marketingCompany;
	String delearCode;
	String configId;
	String featureCode;
	
	public String getSalesModel() {
		return salesModel;
	}
	public void setSalesModel(String salesModel) {
		this.salesModel = salesModel;
	}
	public String getFirstFeatureSet() {
		return firstFeatureSet;
	}
	public void setFirstFeatureSet(String firstFeatureSet) {
		this.firstFeatureSet = firstFeatureSet;
	}
	public String getMarketingCompany() {
		return marketingCompany;
	}
	public void setMarketingCompany(String marketingCompany) {
		this.marketingCompany = marketingCompany;
	}
	public String getDelearCode() {
		return delearCode;
	}
	public void setDelearCode(String delearCode) {
		this.delearCode = delearCode;
	}
	public String getConfigId() {
		return configId;
	}
	public void setConfigId(String configId) {
		this.configId = configId;
	}
	public String getFeatureCode() {
		return featureCode;
	}
	public void setFeatureCode(String featureCode) {
		this.featureCode = featureCode;
	}
	
}
