
package com.cat.core.integration.fbc.request;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "code",
    "description",
    "selectionCriteria",
    "isRequired",
    "isMultiSelect",
    "previousSetAvailable",
    "nextSetAvailable",
    "selectedItem",
    "features"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class FeatureSet {

    @JsonProperty("code")
    private String code;
    @JsonProperty("description")
    private String description;
    @JsonProperty("selectionCriteria")
    private String selectionCriteria;
    @JsonProperty("isRequired")
    private Boolean isRequired;
    @JsonProperty("isMultiSelect")
    private Boolean isMultiSelect;
    @JsonProperty("previousSetAvailable")
    private Boolean previousSetAvailable;
    @JsonProperty("nextSetAvailable")
    private Boolean nextSetAvailable;
    @JsonProperty("selectedItem")
    private Object selectedItem;
    @JsonProperty("features")
    private List<Feature> features = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();//NOSONAR

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("selectionCriteria")
    public String getSelectionCriteria() {
        return selectionCriteria;
    }

    @JsonProperty("selectionCriteria")
    public void setSelectionCriteria(String selectionCriteria) {
        this.selectionCriteria = selectionCriteria;
    }

    @JsonProperty("isRequired")
    public Boolean getIsRequired() {
        return isRequired;
    }

    @JsonProperty("isRequired")
    public void setIsRequired(Boolean isRequired) {
        this.isRequired = isRequired;
    }

    @JsonProperty("isMultiSelect")
    public Boolean getIsMultiSelect() {
        return isMultiSelect;
    }

    @JsonProperty("isMultiSelect")
    public void setIsMultiSelect(Boolean isMultiSelect) {
        this.isMultiSelect = isMultiSelect;
    }

    @JsonProperty("previousSetAvailable")
    public Boolean getPreviousSetAvailable() {
        return previousSetAvailable;
    }

    @JsonProperty("previousSetAvailable")
    public void setPreviousSetAvailable(Boolean previousSetAvailable) {
        this.previousSetAvailable = previousSetAvailable;
    }

    @JsonProperty("nextSetAvailable")
    public Boolean getNextSetAvailable() {
        return nextSetAvailable;
    }

    @JsonProperty("nextSetAvailable")
    public void setNextSetAvailable(Boolean nextSetAvailable) {
        this.nextSetAvailable = nextSetAvailable;
    }

    @JsonProperty("selectedItem")
    public Object getSelectedItem() {
        return selectedItem;
    }

    @JsonProperty("selectedItem")
    public void setSelectedItem(Object selectedItem) {
        this.selectedItem = selectedItem;
    }

    @JsonProperty("features")
    public List<Feature> getFeatures() {
        return features;
    }

    @JsonProperty("features")
    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
