
package com.cat.core.integration.fbc.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.cat.core.integration.edh.response.EDHError;
import com.cat.core.integration.fbc.request.FeatureSet;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "configuratorId",
    "model",
    "featureSet",
    "selections",
    "edhError"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class FBCConfigResponse {

    @JsonProperty("configuratorId")
    private String configuratorId;
    @JsonProperty("model")
    private Model model;
    @JsonProperty("featureSet")
    private FeatureSet featureSet;
    @JsonProperty("selections")
    private List<Object> selections = null;
    
    @JsonProperty("edhError")
    private EDHError edhError; 
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();//NOSONAR

    @JsonProperty("configuratorId")
    public String getConfiguratorId() {
        return configuratorId;
    }

    @JsonProperty("configuratorId")
    public void setConfiguratorId(String configuratorId) {
        this.configuratorId = configuratorId;
    }

    @JsonProperty("model")
    public Model getModel() {
        return model;
    }

    @JsonProperty("model")
    public void setModel(Model model) {
        this.model = model;
    }

    @JsonProperty("featureSet")
    public FeatureSet getFeatureSet() {
        return featureSet;
    }

    @JsonProperty("featureSet")
    public void setFeatureSet(FeatureSet featureSet) {
        this.featureSet = featureSet;
    }

    @JsonProperty("selections")
    public List<Object> getSelections() {
        return selections;
    }

    @JsonProperty("selections")
    public void setSelections(List<Object> selections) {
        this.selections = selections;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    public EDHError getEdhError() {
		return edhError;
	}

	public void setEdhError(EDHError edhError) {
		this.edhError = edhError;
	}
}
