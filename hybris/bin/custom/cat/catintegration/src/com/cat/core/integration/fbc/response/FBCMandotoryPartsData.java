package com.cat.core.integration.fbc.response;
import org.apache.commons.lang.builder.ToStringBuilder;


public class FBCMandotoryPartsData {
	    private String configId;
	    private Boolean selected;
	    private Boolean isRequired;
	    private String selectionText;
	    private String featureCode;
	    private Integer quantity;
	    private String assemblyIndicator;
	    private String shipLessIndicator;
	    private String sourceBuilding;
	    private String description;
	    private String referenceNumber;
	    private Integer listPrice;
	    private Boolean isNet;
	    private String extendedDescription;
	    private Integer shipWeight;
	    private Integer extendedListPrice;
	    private String selectionCriteria;
		public Boolean getSelected() {
			return selected;
		}
		public void setSelected(Boolean selected) {
			this.selected = selected;
		}
		public Boolean getIsRequired() {
			return isRequired;
		}
		public void setIsRequired(Boolean isRequired) {
			this.isRequired = isRequired;
		}
		public String getSelectionText() {
			return selectionText;
		}
		public void setSelectionText(String selectionText) {
			this.selectionText = selectionText;
		}
		public String getFeatureCode() {
			return featureCode;
		}
		public void setFeatureCode(String featureCode) {
			this.featureCode = featureCode;
		}
		public Integer getQuantity() {
			return quantity;
		}
		public void setQuantity(Integer quantity) {
			this.quantity = quantity;
		}
		public String getAssemblyIndicator() {
			return assemblyIndicator;
		}
		public void setAssemblyIndicator(String assemblyIndicator) {
			this.assemblyIndicator = assemblyIndicator;
		}
		public String getShipLessIndicator() {
			return shipLessIndicator;
		}
		public void setShipLessIndicator(String shipLessIndicator) {
			this.shipLessIndicator = shipLessIndicator;
		}
		public String getSourceBuilding() {
			return sourceBuilding;
		}
		public void setSourceBuilding(String sourceBuilding) {
			this.sourceBuilding = sourceBuilding;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getReferenceNumber() {
			return referenceNumber;
		}
		public void setReferenceNumber(String referenceNumber) {
			this.referenceNumber = referenceNumber;
		}
		public Integer getListPrice() {
			return listPrice;
		}
		public void setListPrice(Integer listPrice) {
			this.listPrice = listPrice;
		}
		public Boolean getIsNet() {
			return isNet;
		}
		public void setIsNet(Boolean isNet) {
			this.isNet = isNet;
		}
		public String getExtendedDescription() {
			return extendedDescription;
		}
		public void setExtendedDescription(String extendedDescription) {
			this.extendedDescription = extendedDescription;
		}
		public Integer getShipWeight() {
			return shipWeight;
		}
		public void setShipWeight(Integer shipWeight) {
			this.shipWeight = shipWeight;
		}
		public Integer getExtendedListPrice() {
			return extendedListPrice;
		}
		public void setExtendedListPrice(Integer extendedListPrice) {
			this.extendedListPrice = extendedListPrice;
		}
		public String getConfigId() {
			return configId;
		}
		public void setConfigId(String configId) {
			this.configId = configId;
		}
		public String getSelectionCriteria() {
			return selectionCriteria;
		}
		public void setSelectionCriteria(String selectionCriteria) {
			this.selectionCriteria = selectionCriteria;
		}
		
		@Override
		public String toString()
		{
			return ToStringBuilder.reflectionToString(this);
		}
}
