
package com.cat.core.integration.fbc.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cat.core.integration.edh.response.EDHError;
import com.cat.core.integration.fbc.request.FeatureSet;
import com.cat.core.integration.fbc.request.Selection;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "featureSet",
    "selections"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class FBCResponseMapper {

    @JsonProperty("featureSet")
    private FeatureSet featureSet;
    @JsonProperty("selections")
    private List<Selection> selections = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("edhError")
    private EDHError edhError; 
    
    @JsonProperty("featureSet")
    public FeatureSet getFeatureSet() {
        return featureSet;
    }

    @JsonProperty("featureSet")
    public void setFeatureSet(FeatureSet featureSet) {
        this.featureSet = featureSet;
    }

    @JsonProperty("selections")
    public List<Selection> getSelections() {
        return selections;
    }

    @JsonProperty("selections")
    public void setSelections(List<Selection> selections) {
        this.selections = selections;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    public EDHError getEdhError() {
  		return edhError;
  	}

  	public void setEdhError(EDHError edhError) {
  		this.edhError = edhError;
  	}
}
