
package com.cat.core.integration.fbc.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "fileName",
    "model",
    "sourceBuilding",
    "salesArea",
    "type",
    "firstFeatureSet",
    "firstReferenceSet",
    "availableDate",
    "activityDate",
    "description",
    "updateInquiry",
    "language",
    "units",
    "currencyCode",
    "priceListDate",
    "priceListId",
    "priceListDealer",
    "status",
    "imageUrl",
    "productGroupId"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class FetchFBCFeatureFileResponseMapper {

    @JsonProperty("fileName")
    private String fileName;
    @JsonProperty("model")
    private String model;
    @JsonProperty("sourceBuilding")
    private String sourceBuilding;
    @JsonProperty("salesArea")
    private String salesArea;
    @JsonProperty("type")
    private Integer type;
    @JsonProperty("firstFeatureSet")
    private String firstFeatureSet;
    @JsonProperty("firstReferenceSet")
    private String firstReferenceSet;
    @JsonProperty("availableDate")
    private String availableDate;
    @JsonProperty("activityDate")
    private String activityDate;
    @JsonProperty("description")
    private String description;
    @JsonProperty("updateInquiry")
    private Boolean updateInquiry;
    @JsonProperty("language")
    private Integer language;
    @JsonProperty("units")
    private Integer units;
    @JsonProperty("currencyCode")
    private String currencyCode;
    @JsonProperty("priceListDate")
    private String priceListDate;
    @JsonProperty("priceListId")
    private String priceListId;
    @JsonProperty("priceListDealer")
    private String priceListDealer;
    @JsonProperty("status")
    private String status;
    @JsonProperty("imageUrl")
    private String imageUrl;
    @JsonProperty("productGroupId")
    private String productGroupId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();//NOSONAR

    @JsonProperty("fileName")
    public String getFileName() {
        return fileName;
    }

    @JsonProperty("fileName")
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @JsonProperty("model")
    public String getModel() {
        return model;
    }

    @JsonProperty("model")
    public void setModel(String model) {
        this.model = model;
    }

    @JsonProperty("sourceBuilding")
    public String getSourceBuilding() {
        return sourceBuilding;
    }

    @JsonProperty("sourceBuilding")
    public void setSourceBuilding(String sourceBuilding) {
        this.sourceBuilding = sourceBuilding;
    }

    @JsonProperty("salesArea")
    public String getSalesArea() {
        return salesArea;
    }

    @JsonProperty("salesArea")
    public void setSalesArea(String salesArea) {
        this.salesArea = salesArea;
    }

    @JsonProperty("type")
    public Integer getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(Integer type) {
        this.type = type;
    }

    @JsonProperty("firstFeatureSet")
    public String getFirstFeatureSet() {
        return firstFeatureSet;
    }

    @JsonProperty("firstFeatureSet")
    public void setFirstFeatureSet(String firstFeatureSet) {
        this.firstFeatureSet = firstFeatureSet;
    }

    @JsonProperty("firstReferenceSet")
    public String getFirstReferenceSet() {
        return firstReferenceSet;
    }

    @JsonProperty("firstReferenceSet")
    public void setFirstReferenceSet(String firstReferenceSet) {
        this.firstReferenceSet = firstReferenceSet;
    }

    @JsonProperty("availableDate")
    public String getAvailableDate() {
        return availableDate;
    }

    @JsonProperty("availableDate")
    public void setAvailableDate(String availableDate) {
        this.availableDate = availableDate;
    }

    @JsonProperty("activityDate")
    public String getActivityDate() {
        return activityDate;
    }

    @JsonProperty("activityDate")
    public void setActivityDate(String activityDate) {
        this.activityDate = activityDate;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("updateInquiry")
    public Boolean getUpdateInquiry() {
        return updateInquiry;
    }

    @JsonProperty("updateInquiry")
    public void setUpdateInquiry(Boolean updateInquiry) {
        this.updateInquiry = updateInquiry;
    }

    @JsonProperty("language")
    public Integer getLanguage() {
        return language;
    }

    @JsonProperty("language")
    public void setLanguage(Integer language) {
        this.language = language;
    }

    @JsonProperty("units")
    public Integer getUnits() {
        return units;
    }

    @JsonProperty("units")
    public void setUnits(Integer units) {
        this.units = units;
    }

    @JsonProperty("currencyCode")
    public String getCurrencyCode() {
        return currencyCode;
    }

    @JsonProperty("currencyCode")
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @JsonProperty("priceListDate")
    public String getPriceListDate() {
        return priceListDate;
    }

    @JsonProperty("priceListDate")
    public void setPriceListDate(String priceListDate) {
        this.priceListDate = priceListDate;
    }

    @JsonProperty("priceListId")
    public String getPriceListId() {
        return priceListId;
    }

    @JsonProperty("priceListId")
    public void setPriceListId(String priceListId) {
        this.priceListId = priceListId;
    }

    @JsonProperty("priceListDealer")
    public String getPriceListDealer() {
        return priceListDealer;
    }

    @JsonProperty("priceListDealer")
    public void setPriceListDealer(String priceListDealer) {
        this.priceListDealer = priceListDealer;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("imageUrl")
    public String getImageUrl() {
        return imageUrl;
    }

    @JsonProperty("imageUrl")
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @JsonProperty("productGroupId")
    public String getProductGroupId() {
        return productGroupId;
    }

    @JsonProperty("productGroupId")
    public void setProductGroupId(String productGroupId) {
        this.productGroupId = productGroupId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
