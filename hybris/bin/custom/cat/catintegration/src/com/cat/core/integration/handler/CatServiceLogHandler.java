package com.cat.core.integration.handler;

import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.io.StringWriter;
import java.util.Collections;
import java.util.Set;

import javax.annotation.Resource;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.apache.log4j.Logger;

import com.cat.core.integration.beans.AuthenticationHeader;


public class CatServiceLogHandler implements SOAPHandler<SOAPMessageContext>
{

	private static final Logger LOGGER = Logger.getLogger(CatServiceLogHandler.class);
	private static final String HEADER_AUTHENTICATION = "Password";

	@Resource
	ConfigurationService configurationService;

	@Resource
	ModelService modelService;


	@Resource
	AuthenticationHeader authenticationHeader;



	@Override
	public Set<QName> getHeaders()
	{
		return Collections.emptySet();
	}

	@Override
	public boolean handleMessage(final SOAPMessageContext context)
	{
		final SOAPMessage message = context.getMessage();


		final String userName = configurationService.getConfiguration().getString("orderdirector.webservice.username", "");
		final String password = configurationService.getConfiguration().getString("orderdirector.webservice.password", "");
		final boolean isOutboundMessage = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

		try
		{
			if (isOutboundMessage)
			{
				LOGGER.info("OUTBOUND MESSAGE\n");
				final SOAPHeader header = message.getSOAPHeader();
				final SOAPElement security = header.addChildElement("Security", "wsse",
						"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
				final SOAPElement usernameToken = security.addChildElement("UsernameToken", "wsse");
				usernameToken.addAttribute(new QName("wsu:Id"), "UsernameToken-1");
				usernameToken.addAttribute(new QName("xmlns:wsu"),
						"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
				final SOAPElement username = usernameToken.addChildElement("Username", "wsse");
				username.addTextNode(userName);

				final SOAPElement pass = usernameToken.addChildElement(HEADER_AUTHENTICATION, "wsse");
				pass.setAttribute("Type",
						"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
				pass.addTextNode(password);
				message.saveChanges();
				getSoapMessage(message);

			}
			else
			{
				LOGGER.info("INBOUND MESSAGE\n");
				getSoapMessage(message);
			}

		}
		catch (final SOAPException e)
		{
			LOGGER.error("Exception while submitting order", e);
		}
		catch (final TransformerConfigurationException e)
		{
			LOGGER.error("Exception while creating the xml String", e);
		}
		catch (final TransformerException e)
		{
			LOGGER.error("Exception while creating the xml ", e);
		}


		return true;
	}

	private static String getSoapMessage(final SOAPMessage message) throws SOAPException, TransformerException
	{
		final Source src = message.getSOAPPart().getContent();

		// Transform the Source into a StreamResult to get the XML
		final Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "no");
		final StreamResult result = new StreamResult(new StringWriter());
		transformer.transform(src, result);
		LOGGER.info(result.getWriter().toString());
		return result.getWriter().toString();


	}


	@Override
	public boolean handleFault(final SOAPMessageContext context)
	{

		//can be implemented later based on requirement
		return false;
	}

	@Override
	public void close(final MessageContext context)
	{

		//Need to be implemented later
	}
}
