package com.cat.core.integration.jobs.test;

import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.cat.core.integration.fbc.request.FBCCreateRequestMapper;
import com.cat.core.integration.fbc.request.FeatureData;
import com.cat.core.integration.fbc.request.Selection;
import com.cat.core.integration.service.impl.DefaultCatFBCConfiguratorService;

import de.hybris.platform.core.Registry;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.cronjob.model.CronJobModel;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.util.Config;

//This job is only for testing FBC in all environments.  


@SuppressWarnings("rawtypes")
public class FBCTestCronJob extends AbstractJobPerformable {//NO SONAR

	private static final Logger LOG = Logger.getLogger(FBCTestCronJob.class.getName());
	@Override
	public PerformResult perform(final CronJobModel arg0) {

		LOG.info("FBCTestCronJob started");
		LOG.info("Processing....");

		final DefaultCatFBCConfiguratorService t =(DefaultCatFBCConfiguratorService) Registry.getApplicationContext().getBean("catFBCConfiguratorService");

		final FBCCreateRequestMapper fbcCreateRequestMapper = new FBCCreateRequestMapper();

		final FeatureData featureData = new FeatureData();
		featureData.setMarketingCompany(Config.getParameter("fbc.test.cronjob.marketingcompany"));
		featureData.setDelearCode(Config.getParameter("fbc.test.cronjob.delearcode"));
		featureData.setSalesModel(Config.getParameter("fbc.test.cronjob.salesmodel"));//Have to get this from hybris
		featureData.setFirstFeatureSet(Config.getParameter("fbc.test.cronjob.cfg"));//Only get CFG from for defined sales model from list

		String featureFileIDForSalesModel = t.getFeatureFileIDForSalesModel(featureData);
		LOG.info(featureFileIDForSalesModel+":::::filename");

		fbcCreateRequestMapper.setFeatureFileId(featureFileIDForSalesModel);
		fbcCreateRequestMapper.setMarketingCompany(Config.getParameter("fbc.test.cronjob.marketingcompany"));
		fbcCreateRequestMapper.setDealerCode(Config.getParameter("fbc.test.cronjob.delearcode"));
		fbcCreateRequestMapper.setMode(2);

		List<Selection> invokeFBCRequest;
		try {
			invokeFBCRequest = t.getFBCComaptibleMandatoryMaterialsRequest(fbcCreateRequestMapper, Config.getParameter("fbc.test.cronjob.dca"));
			if(null!=invokeFBCRequest){
				for(final Selection selection : invokeFBCRequest){
					LOG.info(selection.getReferenceNumber()+"\t\t"+selection.getDescription()+"\t\t"+selection.getQuantity()+"\t\t"+selection.getAssemblyIndicator());
				}
			}
		} catch (Exception e) {
			LOG.log(Level.ERROR, "Exception Occured :"+e.getMessage(), e);//NOSONAR
		}

	//	List<Selection> invokeFBCRequest = t.getFBCMandotoryPartsRequest("5225372","00152424","E140");//NOSONAR
	//	List<Selection> invokeFBCRequest = t.getFBCMandotoryPartsRequest("5227439","00129874","E140");//NOSONAR

	//	List<Selection> invokeFBCRequest = t.getFBCMandotoryPartsRequest("5227443","00129876","E140");//NOSONAR

	//	List<Selection> invokeFBCRequest = t.getFBCMandotoryPartsRequest("4938171","00145340","E140");//NOSONAR

		return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

	}

}
