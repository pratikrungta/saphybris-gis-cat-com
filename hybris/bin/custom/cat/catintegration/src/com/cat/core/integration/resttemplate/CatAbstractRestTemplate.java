package com.cat.core.integration.resttemplate;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

import com.cat.core.integration.constants.CatintegrationConstants;

import de.hybris.platform.core.Registry;
import de.hybris.platform.util.Config;

public abstract class CatAbstractRestTemplate {
	private static final Logger LOG = Logger.getLogger(CatAbstractRestTemplate.class.getName());
	protected HttpHeaders getHttpHeaders(OAuth2AccessToken oAuth2AccessToken) {  //NOSONAR
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Bearer " + oAuth2AccessToken.getValue());
		return headers;
	}

	
	protected List<HttpMessageConverter<?>> getMessageConverter() //NOSONAR
	{
		final List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
		final MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setSupportedMediaTypes(Arrays.asList(MediaType.ALL));
		messageConverters.add(converter);

		return messageConverters;
	}
	
	protected abstract OAuth2RestTemplate getCatRestTemplate();
	
	protected HttpEntity<?> getHttpEntity(HttpHeaders headers) //NOSONAR
	{
		return new HttpEntity<>(headers);
	}
	protected ClientCredentialsResourceDetails getClientCredentialsResourceDetails() {
		return (ClientCredentialsResourceDetails) Registry.getApplicationContext().getBean("clientCredentialsResourceDetails");
	}
	protected DefaultOAuth2ClientContext getDefaultOAuth2ClientContext() {
		return (DefaultOAuth2ClientContext) Registry.getApplicationContext().getBean("defaultOAuth2ClientContext");
	}
	
	protected SimpleClientHttpRequestFactory getProxyDetails(){
		SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
		if (Config.getBoolean(CatintegrationConstants.FBC_PROXYENABLED, false))
		{
			final String proxyPort = Config.getParameter(CatintegrationConstants.FBC_PROXYPORT);
			final String proxyUrl = Config.getParameter(CatintegrationConstants.FBC_PROXYURL);
			if(LOG.isDebugEnabled()){
				LOG.debug(proxyPort + " - Port Number - " + Integer.parseInt(proxyPort));
			    LOG.debug("proxy URL - " + proxyUrl);
			}
			
			final Proxy proxy = new Proxy(Type.HTTP, new InetSocketAddress(proxyUrl, Integer.parseInt(proxyPort)));
			requestFactory.setProxy(proxy);
		}
		
		return requestFactory;
	}
	
}
