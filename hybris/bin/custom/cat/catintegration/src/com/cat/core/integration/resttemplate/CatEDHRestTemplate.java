package com.cat.core.integration.resttemplate;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

import com.cat.core.integration.constants.CatintegrationConstants;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.util.Config;

public class CatEDHRestTemplate extends CatAbstractRestTemplate{

	@Override
	public OAuth2RestTemplate getCatRestTemplate() {
		ClientCredentialsResourceDetails resource = getClientCredentialsResourceDetails();
		resource.setAccessTokenUri(Config.getParameter(CatintegrationConstants.WS_ACCESSTOKENURI));
		resource.setClientId(Config.getParameter(CatintegrationConstants.WS_CLIENTID));
		resource.setClientSecret(Config.getParameter(CatintegrationConstants.WS_CLIENTSECRET));
		resource.setGrantType(Config.getParameter(CatintegrationConstants.WS_GRANTTYPE));
		OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resource, getDefaultOAuth2ClientContext());
		restTemplate.setRequestFactory(getProxyDetails());
		restTemplate.setMessageConverters(getMessageConverter());
		return restTemplate;
	}

	protected HttpHeaders getHttpHeaders() {
		OAuth2AccessToken oAuth2AccessToken = getAccessToken();
		String tokenId = oAuth2AccessToken.getValue();
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Bearer " + tokenId);
		return headers;
	}
	public OAuth2AccessToken getAccessToken()
	{
		return (OAuth2AccessToken) JaloSession.getCurrentSession().getAttribute(CatintegrationConstants.OPENID_ACCESS_TOKEN);
	}
}
