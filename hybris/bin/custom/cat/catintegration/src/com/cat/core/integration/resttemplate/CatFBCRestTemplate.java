package com.cat.core.integration.resttemplate;

import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;

import com.cat.core.integration.constants.CatintegrationConstants;

import de.hybris.platform.util.Config;

public class CatFBCRestTemplate extends CatAbstractRestTemplate {

	@Override
	protected OAuth2RestTemplate getCatRestTemplate() {
		ClientCredentialsResourceDetails resource = getClientCredentialsResourceDetails();
		resource.setAccessTokenUri(Config.getParameter(CatintegrationConstants.WS_ACCESSTOKENURI));
		resource.setClientId(Config.getParameter(CatintegrationConstants.WS_CLIENTID));
		resource.setClientSecret(Config.getParameter(CatintegrationConstants.WS_CLIENTSECRET));
		resource.setGrantType(Config.getParameter(CatintegrationConstants.WS_GRANTTYPE));
		OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resource, getDefaultOAuth2ClientContext());
		restTemplate.setRequestFactory(getProxyDetails());
		restTemplate.setMessageConverters(getMessageConverter());
		return restTemplate;
	}

}
