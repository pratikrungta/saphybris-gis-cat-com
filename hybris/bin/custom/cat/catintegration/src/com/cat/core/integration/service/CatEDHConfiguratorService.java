package com.cat.core.integration.service;

import com.cat.core.integration.edh.CatEDHInventoryDetailsDataList;

import com.cat.core.integration.exception.CatEDHException;

 /**
 * The Interface EDHConfiguratorService.
 * @author ravjonnalagadda
 *
 */
public interface CatEDHConfiguratorService {
	
	/**
	 * This method is to get Inventory Details for EDH.
	 *
	 * @param inventoryDetailsDatalist
	 *               : Object it holds URL parameter list.
	 * @throws CatEDHException 
	 *               :  throws CatEDHException exception.
	 *  @return : returns Response .
	 */
	public String getInventoryDetails(CatEDHInventoryDetailsDataList inventoryDetailsDatalist) throws CatEDHException;
	
	
	/**
	 * This Method is to getInventoryCount by party id from EDH.
	 * 
	 * @param partyId : Code which identifies a given PDC, manufacturing facility or dealer.
	 * @param config : A configuration ID associated with a given machine
	 * @param getDescendantNodes : A boolean flag ('true', 'false') for whether children objects should be included
	 * @return JSON response
	 * @throws CatEDHException : throws CatEDHException exception.
	 */
	public String getInventoryCountByPartyId(String partyId,String config,String getDescendantNodes) throws CatEDHException;
	
	
}
