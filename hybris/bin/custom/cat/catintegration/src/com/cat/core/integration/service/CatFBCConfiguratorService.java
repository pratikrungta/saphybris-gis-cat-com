package com.cat.core.integration.service;

import java.util.List;

import com.cat.core.integration.fbc.request.FBCCreateRequestMapper;
import com.cat.core.integration.fbc.request.FeatureData;
import com.cat.core.integration.fbc.request.FeatureSet;
import com.cat.core.integration.fbc.request.Selection;
import com.cat.core.integration.fbc.response.FBCConfigResponse;
import com.cat.core.integration.fbc.response.FBCResponseMapper;
import com.cat.core.integration.fbc.response.FetchFBCFeatureFileResponseMapper;
/**
 * This interface is for FBC service
 * @author sjeedula
 */
public interface CatFBCConfiguratorService {

	/**
	 *  gets Mandatory parts by invoking FBC rest API.
	 * @param fbcCreateRequestMapper : Set the request parameters.
	 * @param productReference : set the DCA.
	 * @param salesModel : Set the sales model.
	 * @return List<Selection> : returns list of Selection. 
	 * @throws Exception : Exception 
	 */
	public List<Selection> getFBCComaptibleMandatoryMaterialsRequest(FBCCreateRequestMapper fbcCreateRequestMapper,String dca) throws Exception; //NOSONAR
	
	/**
	 * gets featureID by invoking FBC rest API.
	 * 
	 * @param featureData : set the input parameters.
	 * @return List of FetchFBCFeatureFileResponseMapper
	 */
	public String getFeatureFileIDForSalesModel(FeatureData featureData);
	
	/**
	 * Method create session and sends response along with session ID.
	 * 
	 * @param fbcCreateRequestMapper : create request mapper
	 * @return FBCConfigResponse : FBCConfigResponse
	 */
	public FBCConfigResponse getFBCCreateAPIResource(FBCCreateRequestMapper fbcCreateRequestMapper) throws Exception;
	
	/**
	 * Method returns response for SelectAPI Resource.
	 * 
	 * @param configId    : input configId
	 * @param featureCode : input featureCode
	 * @return FBCResponseMapper : returns FBCResponseMapper
	 * @throws Exception 
	 */
	public FBCResponseMapper getFBCSelectAPIResource(String configId,String featureCode) throws Exception;
	
	/**
	 * Method returns response for SelectAPI Resource.
	 * 
	 * @param configId    : input configId
	 * @param featureCode : input featureCode
	 * @return JSON Formatted data : returns JSON Formatted data
	 * */
	public String getFBCSelectAPIResponse(String configId,String featureCode);
	
	
	/**
	 * Method returns response for SelectNextAPI Resource. 
	 * 
	 * @param configId : inpput configId
	 * @return FBCResponseMapper : returns FBCResponseMapper
	 */
	public FBCResponseMapper getFBCSelectNextAPIResource(String configId);
	
	/** This method returns response for SelectAPI request
	 * @param fbcCreateRequestMapper
	 * @param dca
	 * @param laneCode
	 * @return parts for the initiated request.
	 * @throws Exception
	 */
	String initiateFBCSelectAPIRequest(FBCCreateRequestMapper fbcCreateRequestMapper,String dca,String laneCode) throws Exception;
	
	/**This method is used to get the next select options response
	 * @param configId
	 * @param featureCode
	 * @return FBCResponseMapper : returns FBCResponseMapper
	 */
	FBCResponseMapper getSelectNextResponse(String configId, String featureCode);

}
