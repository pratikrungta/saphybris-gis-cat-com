package com.cat.core.integration.service;

import java.util.List;

import com.cat.core.integration.fbc.request.FBCCreateRequestMapper;
import com.cat.core.integration.fbc.request.FeatureData;
import com.cat.core.integration.fbc.request.FeatureSet;
import com.cat.core.integration.fbc.request.Selection;
import com.cat.core.integration.fbc.response.FBCConfigResponse;
import com.cat.core.integration.fbc.response.FBCResponseMapper;
import com.cat.core.integration.fbc.response.FetchFBCFeatureFileResponseMapper;
import com.cat.core.integration.snop.request.CatSnopRequestMapper;
import com.cat.core.integration.snop.response.CatSnopResponseMapper;
/**
 * This interface is for FBC service
 * @author sjeedula
 */
public interface CatSnopValidatorService {


	/**
	 * This method validates snop request
	 * @param catSnopRequestMapper - snop request mapper
	 * @return catSnopRequestMapper : returns catSnopRequestMapper
	 */
	CatSnopResponseMapper validateSnopRequest(CatSnopRequestMapper catSnopRequestMapper);

}
