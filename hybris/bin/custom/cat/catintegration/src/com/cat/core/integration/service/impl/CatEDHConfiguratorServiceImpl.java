package com.cat.core.integration.service.impl;

import org.apache.commons.collections.CollectionUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import com.cat.core.integration.constants.CatintegrationConstants;
import com.cat.core.integration.edh.CatEDHInventoryDetailsDataList;
import com.cat.core.integration.edh.details.response.EDHInventoryDetailsResponse;
import com.cat.core.integration.edh.response.EDHError;
import com.cat.core.integration.edh.response.EDHInventoryResponse;
import com.cat.core.integration.exception.CatEDHException;
import com.cat.core.integration.exception.CatRecordNotFoundException;
import com.cat.core.integration.resttemplate.CatEDHRestTemplate;
import com.cat.core.integration.service.CatEDHConfiguratorService;
import com.cat.core.integration.utils.CatRestClientUtil;

import de.hybris.platform.util.Config;

	
/**
 *  EDH SerivceImpl Class for a EDH Implementation
 * @author ravjonnalagadda
 *
 */
public class CatEDHConfiguratorServiceImpl extends CatEDHRestTemplate implements CatEDHConfiguratorService {
	private static final Logger LOG = Logger.getLogger(CatEDHConfiguratorServiceImpl.class);
	private static final String MOCKDATA_USE = "mockdata.use";
	public static final String YES = "Yes";
	/**
	 *{@inheritDoc}
	 * 
	 */
	@Override
	public String getInventoryCountByPartyId(String partyId,String configId,String getDescendantNodes) throws CatEDHException {
		CatRestClientUtil.isValidData(partyId, configId);
		ResponseEntity<Object> responseEntity=null;
		String jsonResponse=null;
		final String mockData = Config.getParameter(MOCKDATA_USE);
		if (YES.equalsIgnoreCase(mockData))
		{
			jsonResponse = checkEDHInvenotryResponse();
		}else{
		try {
			StringBuilder url =new  StringBuilder(Config.getParameter(CatintegrationConstants.EDH_BASE_URL)).append(Config.getParameter(CatintegrationConstants.EDH_COUNT_URI)).append("partyId="+partyId).append(CatintegrationConstants.CONFIG_ID+configId);
			if(StringUtils.isNotBlank(getDescendantNodes)){
				url.append(CatintegrationConstants.GET_DESCENDANT+Boolean.parseBoolean(getDescendantNodes));//NOSONAR
			}
			final HttpEntity<Object> entity = new HttpEntity<>(getHttpHeaders()); 
			responseEntity = getCatRestTemplate().exchange(url.toString(),HttpMethod.GET, entity, Object.class);
			jsonResponse = processResponse(responseEntity, jsonResponse,"getInventoryCountByPartyId");
		} catch (Exception e) {
			EDHError edhError = CatRestClientUtil.processException(e);
			EDHInventoryResponse[] responseArray = new EDHInventoryResponse[1];
			responseArray[0] = new EDHInventoryResponse();
			responseArray[0].setEdhError(edhError);
			jsonResponse = CatRestClientUtil.getJSONResponse(responseArray);
		
		}
		}
		return jsonResponse;
	}

	/**
	 * This Method is to process the response.
	 * @param responseEntity :  response data
	 * @param jsonResponse : JSON Response from EDH Service
	 * @param msg : Message body
	 * @return : return JSON response 
	 * @throws CatRecordNotFoundException : returns if records are not avaiable.
	 */
	private static String processResponse(ResponseEntity<Object> responseEntity, String jsonResponse,String msg)
	                throws CatRecordNotFoundException {
	            if(responseEntity!=null && null!=responseEntity.getBody()){
	                CatRestClientUtil.logResponse(responseEntity.getBody(), msg);
	                jsonResponse = CatRestClientUtil.getJSONResponse(responseEntity.getBody());
	                if("[null]".equals(jsonResponse)||"[]".equals(jsonResponse)) {
	                    throw new CatRecordNotFoundException("No Results Found");
	                }
	            }
	            return jsonResponse;
	      } 
	
	/**
	 * {@inheritDoc}
	 *
	 **/
	@Override
	public String getInventoryDetails(CatEDHInventoryDetailsDataList inventoryDetailsDataList) throws CatEDHException{
		ResponseEntity<Object> responseEntity=null;
		String jsonResponse=null;
		  final String mockData = Config.getParameter(MOCKDATA_USE);
	        if (YES.equalsIgnoreCase(mockData))
	        {
	            jsonResponse = checkEDHInvenotryDetailsResponse();
	        }else{
			try {
				StringBuilder urlParameters =getParametersForInventoryDetailsURL(inventoryDetailsDataList);
				LOG.debug("Basic URL "+CatintegrationConstants.EDH_INVENTORY_DETAILS_URI);
				StringBuilder inventoryDetailsUrl =new  StringBuilder(Config.getParameter(CatintegrationConstants.EDH_BASE_URL)).append(Config.getParameter(CatintegrationConstants.EDH_INVENTORY_DETAILS_URI)).append(urlParameters);
				String inventoryUrl =StringUtils.stripEnd(inventoryDetailsUrl.toString(), CatintegrationConstants.AND_SYMBOL);
				inventoryUrl=inventoryUrl.replaceAll(CatintegrationConstants.LEFT_BRACKET, "").replaceAll(CatintegrationConstants.RIGHT_BRACKET,"");
				LOG.debug("URL"+inventoryUrl);
				final HttpEntity<Object> entity = new HttpEntity<>(getHttpHeaders()); 
				responseEntity = getCatRestTemplate().exchange(inventoryUrl,HttpMethod.GET, entity, Object.class);
				jsonResponse = processResponse(responseEntity, jsonResponse,"getInventoryDetails");
				} catch (Exception e) {
				    EDHError edhError = CatRestClientUtil.processException(e);
				    EDHInventoryDetailsResponse[] responseArray = new EDHInventoryDetailsResponse[1];
		            responseArray[0] = new EDHInventoryDetailsResponse();
		            responseArray[0].setEdhError(edhError);
		            jsonResponse = CatRestClientUtil.getJSONResponse(responseArray);
				} 
	        }
				return jsonResponse;
	}
	 
	 
	/**
	 * @return JSON Response
	 */
	private String checkEDHInvenotryDetailsResponse() {
	    String jsonResponse;
        EDHInventoryDetailsResponse[] response = CatRestClientUtil.getMockData(EDHInventoryDetailsResponse[].class);
         jsonResponse = CatRestClientUtil.getJSONResponse(response);
        return jsonResponse;
	    
    }

    /**
	 * This method is to get parameters for Inventory Details URL.
	 *
	 * @param inventoryDetailsData
	 *               Object it holds URL parameter list.
	 * @return : return URL based on inputs
	 */
	private StringBuilder getParametersForInventoryDetailsURL(CatEDHInventoryDetailsDataList inventoryDetailsData) //NOSONAR
	{
	
		StringBuilder urlParameters = new StringBuilder();
		if(CollectionUtils.isNotEmpty(inventoryDetailsData.getPartyIdList())){
		     urlParameters.append(CatintegrationConstants.PARTYIDS).append(inventoryDetailsData.getPartyIdList()).append(CatintegrationConstants.AND_SYMBOL);
		}
		if(CollectionUtils.isNotEmpty(inventoryDetailsData.getSalesModelList())){
		     urlParameters.append(CatintegrationConstants.SALESMODELS).append(inventoryDetailsData.getSalesModelList()).append(CatintegrationConstants.AND_SYMBOL);
		}
		if(CollectionUtils.isNotEmpty(inventoryDetailsData.getBaseSalesModelList())){
		     urlParameters.append(CatintegrationConstants.BASESALESMODELS).append(inventoryDetailsData.getBaseSalesModelList()).append(CatintegrationConstants.AND_SYMBOL);
		}
		if(CollectionUtils.isNotEmpty(inventoryDetailsData.getProductLineList())){
		     urlParameters.append(CatintegrationConstants.PRODUCTLINES).append(inventoryDetailsData.getProductLineList()).append(CatintegrationConstants.AND_SYMBOL);
		}
		if(CollectionUtils.isNotEmpty(inventoryDetailsData.getSourceFacilitieList())){
		     urlParameters.append(CatintegrationConstants.SOURCEFACILITIES).append(inventoryDetailsData.getSourceFacilitieList()).append(CatintegrationConstants.AND_SYMBOL);
		}
		if(CollectionUtils.isNotEmpty(inventoryDetailsData.getCountrieList())){
		     urlParameters.append(CatintegrationConstants.COUNTRIES).append(inventoryDetailsData.getCountrieList()).append(CatintegrationConstants.AND_SYMBOL);
		}
		if(CollectionUtils.isNotEmpty(inventoryDetailsData.getConfigIdList())){
		     urlParameters.append(CatintegrationConstants.CONFIGIDS).append(inventoryDetailsData.getConfigIdList()).append(CatintegrationConstants.AND_SYMBOL);
		}
	    if(StringUtils.isNotBlank(Config.getParameter(CatintegrationConstants.EDH_INVENTORY_STAGES))) {
		urlParameters.append(CatintegrationConstants.STAGES).append(Config.getParameter(CatintegrationConstants.EDH_INVENTORY_STAGES)).append(CatintegrationConstants.AND_SYMBOL);
		}
	    urlParameters.append(CatintegrationConstants.DESCENDANTNODES).append(CatintegrationConstants.TRUE);
		
	    return urlParameters;
	}

	 /**
     * @return Json Response 
     */
    private String checkEDHInvenotryResponse() {
        String jsonResponse;
        EDHInventoryResponse[] response = CatRestClientUtil.getMockData(EDHInventoryResponse[].class);
         jsonResponse = CatRestClientUtil.getJSONResponse(response);
        return jsonResponse;
    }

}	
	
