package com.cat.core.integration.service.impl;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.oauth2.client.resource.UserRedirectRequiredException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.cat.core.integration.constants.CatintegrationConstants;
import com.cat.core.integration.edh.response.EDHError;
import com.cat.core.integration.exception.CatDCANotFoundException;
import com.cat.core.integration.exception.CatRecordNotFoundException;
import com.cat.core.integration.fbc.request.FBCCreateRequestMapper;
import com.cat.core.integration.fbc.request.Feature;
import com.cat.core.integration.fbc.request.FeatureData;
import com.cat.core.integration.fbc.request.FeatureSet;
import com.cat.core.integration.fbc.request.Selection;
import com.cat.core.integration.fbc.response.FBCConfigResponse;
import com.cat.core.integration.fbc.response.FBCResponseMapper;
import com.cat.core.integration.fbc.response.FetchFBCFeatureFileResponseMapper;
import com.cat.core.integration.service.CatFBCConfiguratorService;
import com.cat.core.integration.service.CatSnopValidatorService;
import com.cat.core.integration.snop.request.CatSnopRequestMapper;
import com.cat.core.integration.snop.response.CatSnopResponseMapper;
import com.cat.core.integration.snop.response.CatSnopSubResponse;
import com.cat.core.integration.utils.CatRestClientUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;
import sun.misc.BASE64Encoder;


/**
 * This class is used to get mandatory parts from FBC API.
 * @author sjeedula
 * 
 */
public class CatSnopValidatorServiceImpl implements CatSnopValidatorService{
	private static final Logger LOG = Logger.getLogger(CatSnopValidatorServiceImpl.class);
	private static final String AUTHORIZATION = "Authorization";
	private static final String BASIC = "Basic ";
	private static final String CONNECTION_ERROR="CONNECTION ERROR";
	
	@Resource(name="sessionService")
	private SessionService sessionService;
	
	@Resource(name="userService")
	private UserService userService;
	
	@Resource(name="cartService")
	private CartService cartService;
	
	/**
	 *{@inheritDoc}
	 * @throws Exception 
	 */
	@Override
	public CatSnopResponseMapper validateSnopRequest(CatSnopRequestMapper catSnopRequestMapper) {
		CatSnopResponseMapper catSnopResponseMapper = new CatSnopResponseMapper();
		LOG.info("Processing CatSnopValidatorServiceImpl");
		String snopUrl =Config.getString("cat.snop.api.url", "");
		
		String username = Config.getParameter("cat.snop.username");
		String password = Config.getParameter("cat.snop.password");
        String authString = username + ":" + password;
        String authStringEnc = new BASE64Encoder().encode(authString.getBytes());
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add(AUTHORIZATION, BASIC + authStringEnc);
		
		final ObjectMapper mapper = new ObjectMapper();
		HttpEntity<String> httpEntity=null;
		try{
		String jsonInString = mapper.writeValueAsString(catSnopRequestMapper);
			httpEntity=new HttpEntity<>(jsonInString, headers);
			B2BCustomerModel customer=(B2BCustomerModel) userService.getCurrentUser();
			LOG.info(" SNOP :: BEFORE SNOP REQUEST :: USER - "+customer.getDefaultB2BUnit().getUid()+ " & CART ID - "+cartService.getSessionCart().getCode() + " & REQUEST SIZE - "+catSnopRequestMapper.getMessageArea().size());
			final ResponseEntity<CatSnopResponseMapper> responseEntity = new RestTemplate().exchange(snopUrl,HttpMethod.POST,httpEntity,CatSnopResponseMapper.class);
			if(responseEntity!=null && responseEntity.getBody()!=null){
				LOG.info(" SNOP :: AFTER SNOP REQUEST :: IIB TRANSACTION ID - "+responseEntity.getBody().getIibTransactionId()+ " & MESSAGE RESPONSE SIZE - "+responseEntity.getBody().getMessageArea().size());
				CatRestClientUtil.logResponse(responseEntity.getBody(), "SNOP response");
				catSnopResponseMapper = responseEntity.getBody();
			}
			else
			{
				catSnopResponseMapper.setMessageArea(null);
			}
			}
			catch(HttpMessageNotReadableException e)
			{	
				LOG.error("SNOP MSG NOT READABLE : "+e.getMessage(),e);
				final ResponseEntity<String> responseEntity = new RestTemplate().exchange(snopUrl,HttpMethod.POST,httpEntity,String.class);
				String response = responseEntity.getBody();
				response= response.replaceFirst("\"MESSAGE_AREA_OUT\"", "M_AREA_OUT").replace("\"MESSAGE_AREA_OUT\":", "").replace(":{", ":[{").replace("M_AREA_OUT", "\"MESSAGE_AREA_OUT\"").concat("#").replace("}#", "]}").replace("\\", "");
				
				CatRestClientUtil.logResponse(response, "SNOP response");
				
				final ObjectMapper objectMapper = new ObjectMapper();
				try{
				catSnopResponseMapper = objectMapper.readValue(response, CatSnopResponseMapper.class);
				}
				catch(Exception ex)
				{
					LOG.error("ERROR IN JSON MAPPING : "+ex.getMessage(),ex);
				}
			}
			catch(Exception e)
			{
				LOG.error("SNOP :: Error Connecting to S&OP Service : "+e.getMessage(), e);
				catSnopResponseMapper = new CatSnopResponseMapper();
				catSnopResponseMapper.setIibTransactionId(CONNECTION_ERROR);
				List<CatSnopSubResponse> catSnopRespList=new ArrayList();
				CatSnopSubResponse resp = new CatSnopSubResponse();
				resp.setSopIndicator("N");
				resp.setSalesModel("N");
				resp.setDca("N");
				catSnopRespList.add(resp);
				catSnopResponseMapper.setMessageArea(catSnopRespList);
			}
		
		
		
		
			
		return catSnopResponseMapper;
	}
}