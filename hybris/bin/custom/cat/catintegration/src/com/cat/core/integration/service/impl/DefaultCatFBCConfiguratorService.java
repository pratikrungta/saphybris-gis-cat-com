package com.cat.core.integration.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.resource.UserRedirectRequiredException;
import org.springframework.web.client.RestClientException;

import com.cat.core.integration.constants.CatintegrationConstants;
import com.cat.core.integration.edh.response.EDHError;
import com.cat.core.integration.exception.CatDCANotFoundException;
import com.cat.core.integration.exception.CatRecordNotFoundException;
import com.cat.core.integration.fbc.request.FBCCreateRequestMapper;
import com.cat.core.integration.fbc.request.Feature;
import com.cat.core.integration.fbc.request.FeatureData;
import com.cat.core.integration.fbc.request.FeatureSet;
import com.cat.core.integration.fbc.request.Selection;
import com.cat.core.integration.fbc.response.FBCConfigResponse;
import com.cat.core.integration.fbc.response.FBCResponseMapper;
import com.cat.core.integration.fbc.response.FetchFBCFeatureFileResponseMapper;
import com.cat.core.integration.resttemplate.CatFBCRestTemplate;
import com.cat.core.integration.service.CatFBCConfiguratorService;
import com.cat.core.integration.utils.CatRestClientUtil;

import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.util.Config;


/**
 * This class is used to get mandatory parts from FBC API.
 * @author sjeedula
 * 
 */
public class DefaultCatFBCConfiguratorService extends CatFBCRestTemplate implements CatFBCConfiguratorService{
	private static final Logger LOG = Logger.getLogger(DefaultCatFBCConfiguratorService.class);
	private String configIdLocal;
	
	@Resource(name="sessionService")
	private SessionService sessionService;
	
	/**
	 *{@inheritDoc}
	 */
	@Override
	public List<Selection> getFBCComaptibleMandatoryMaterialsRequest(FBCCreateRequestMapper fbcCreateRequestMapper,
			String dca) throws Exception {
		LOG.info("Processing DefaultCatFBCConfiguratorService");
		CatRestClientUtil.isValidData(fbcCreateRequestMapper.getMarketingCompany(),fbcCreateRequestMapper.getDealerCode(),dca);
		List<Selection> mandatoryParts =new ArrayList<>();
		FeatureData featureCodeByDCA = getFeatureCodeByDCA(fbcCreateRequestMapper,dca);
		FBCResponseMapper response = getFBCSelectAPIResource(featureCodeByDCA.getConfigId(),featureCodeByDCA.getFeatureCode());
		mandatoryParts = processResponse(mandatoryParts, featureCodeByDCA, response);
		LOG.info("Processed DefaultCatFBCConfiguratorService");
		return mandatoryParts;
	}

	private List<Selection> processResponse(List<Selection> mandatoryParts, FeatureData featureCodeByDCA,
			FBCResponseMapper response) throws Exception {
		if(BooleanUtils.isTrue(response.getFeatureSet().getIsRequired())){//if True, Lane1
			List<Feature> lane1Features = getLane1Features(response);
			if(!lane1Features.isEmpty()){
				response = getFBCSelectAPIResource(featureCodeByDCA.getConfigId(),lane1Features.get(0).getFeatureCode());
				if(null==response.getFeatureSet()){
					
					return response.getSelections();
				}
				if(response.getFeatureSet().getIsRequired().equals(Boolean.FALSE)){
					mandatoryParts = getSelectNextAPISelections(featureCodeByDCA.getConfigId());
				}
			}else{
				if(CollectionUtils.isNotEmpty(response.getSelections())) {
					return response.getSelections();
				}
			}
		
		}else if(BooleanUtils.isFalse(response.getFeatureSet().getIsRequired())){
			
				mandatoryParts = getSelectNextAPISelections(featureCodeByDCA.getConfigId());
		}
		if (null!=mandatoryParts && mandatoryParts.isEmpty()) {
			throw new CatRecordNotFoundException("No Mandotory Parts availble");
		}
		return mandatoryParts;
	}
	
	/**
	 *{@inheritDoc}
	 * @throws CatFBCException 
	 */
	@Override
	public FBCConfigResponse getFBCCreateAPIResource(FBCCreateRequestMapper fbcCreateRequestMapper) throws Exception {
		ResponseEntity<FBCConfigResponse> responseEntity = null;
		try {
			final String fbccreateurl = Config.getParameter(CatintegrationConstants.WS_BASE_URL)
					+ Config.getParameter(CatintegrationConstants.WS_CREATE_URL);

			if (StringUtils.isBlank(fbccreateurl)) {
				throw new IllegalStateException("FBC Create"+CatintegrationConstants.FBC_URI_MSG);
			}
			final HttpEntity<FBCCreateRequestMapper> entity = new HttpEntity<>(fbcCreateRequestMapper,
					getHttpHeaders(this.getCatRestTemplate().getAccessToken()));
				responseEntity = this.getCatRestTemplate().exchange(fbccreateurl,
						HttpMethod.POST, entity, FBCConfigResponse.class);
		} catch (Exception e) {
		 throw new Exception(e);
		} 
			return responseEntity.getBody();
	}
	
	/**
	 *{@inheritDoc}
	 */
	@Override
	public FBCResponseMapper getFBCSelectAPIResource(String configId,String featureCode) throws Exception{ 
		FBCResponseMapper fbcResponseMapper = null;
		try {
			String fbcSelectURL = Config.getParameter(CatintegrationConstants.WS_BASE_URL)
					+ Config.getParameter(CatintegrationConstants.WS_SELECT_URL) + configId + "/"
					+ featureCode;

			if (StringUtils.isBlank(fbcSelectURL)) {
				throw new IllegalStateException(
						"FBC Select "+CatintegrationConstants.FBC_URI_MSG);
			}
				final ResponseEntity<FBCResponseMapper> responseEntity = this.getCatRestTemplate().exchange(fbcSelectURL,
						HttpMethod.POST, getHttpEntity(getHttpHeaders(this.getCatRestTemplate().getAccessToken())), FBCResponseMapper.class);
				
				if(responseEntity!=null){
					CatRestClientUtil.logResponse(responseEntity.getBody(), "FBC Select");
					fbcResponseMapper = responseEntity.getBody();
				}
		} catch (Exception e) {
			throw new Exception(e);
		} 
			return fbcResponseMapper;
	}

	/**
	 *{@inheritDoc}
	 */
	@Override
	public FBCResponseMapper getFBCSelectNextAPIResource(String configId){
		FBCResponseMapper fbcResponseMapper=null;
		try {
			final String fbcNextSelectURL = Config.getParameter(CatintegrationConstants.WS_BASE_URL) + Config.getParameter(CatintegrationConstants.WS_NEXTSELECT_URL) + configId;

			if (StringUtils.isBlank(fbcNextSelectURL)) {
				throw new IllegalStateException(
						"FBC SelectNext "+CatintegrationConstants.FBC_URI_MSG);

			}
				ResponseEntity<FBCResponseMapper> responseEntity = this.getCatRestTemplate().exchange(fbcNextSelectURL, HttpMethod.POST, getHttpEntity(getHttpHeaders(this.getCatRestTemplate().getAccessToken())),
						FBCResponseMapper.class);
				if(responseEntity!=null){
					CatRestClientUtil.logResponse(responseEntity.getBody(), "Select Next");
					fbcResponseMapper = responseEntity.getBody();
				}
		} catch (RestClientException e) {
			LOG.log(Level.ERROR, CatintegrationConstants.EXCEPTION_OCCURED + e.getMessage(), e);
		} catch (UserRedirectRequiredException e) {
			LOG.log(Level.ERROR, CatintegrationConstants.EXCEPTION_OCCURED + e.getMessage(), e);
		}
			
			return fbcResponseMapper;
	}
	
	private List<Selection> getSelectNextAPISelections(String configId) throws Exception{
		List<Selection> selections = null;
		FBCResponseMapper processFBCSelectNextAPIRequest = getFBCSelectNextAPIResource(configId);
		if(null==processFBCSelectNextAPIRequest.getFeatureSet()){
			selections=processFBCSelectNextAPIRequest.getSelections();
		}else {
			FeatureData featureData = new FeatureData();
			featureData.setConfigId(configId);
			selections=processResponse(processFBCSelectNextAPIRequest.getSelections(),featureData, processFBCSelectNextAPIRequest);
		}
		return selections;
	}
	
	@SuppressWarnings("unused")
	private List<Selection> getSelectAPISelections(String configId,String featureCode) throws Exception{//NOSONAR
		List<Selection> selections = null;
		FBCResponseMapper processFBCSelectAPIResponse = getFBCSelectAPIResource(configId,featureCode);
		if(null==processFBCSelectAPIResponse.getFeatureSet()){
			selections=processFBCSelectAPIResponse.getSelections();
		}else{
			FeatureData featureData = new FeatureData();
			featureData.setConfigId(configId);
			featureData.setFeatureCode(featureCode);
			selections=processResponse(processFBCSelectAPIResponse.getSelections(),featureData, processFBCSelectAPIResponse);
		}
		return selections;
	}
	
	private FeatureData getFeatureCodeByDCA(FBCCreateRequestMapper fbcCreateRequestMapper,String dca) throws Exception{
		FBCConfigResponse processCreateFBCAPIRequest = getFBCCreateAPIResource(fbcCreateRequestMapper);
		List<Feature> features = processCreateFBCAPIRequest.getFeatureSet().getFeatures();
		List<Feature> dcaFeature = features.stream().filter(feature -> dca.equals(feature.getReferenceNumber())).collect(Collectors.toList());
		if (dcaFeature.isEmpty()) {
			throw new CatDCANotFoundException(
					"Unable to find DCA " + dca + " in FBC DCA's API List : " + features);
		}
		FeatureData featureData = new FeatureData();
		featureData.setConfigId(processCreateFBCAPIRequest.getConfiguratorId());
		featureData.setFeatureCode(dcaFeature.get(0).getFeatureCode());
		return featureData;
	}

	/**
	 *{@inheritDoc}
	 */
	@Override
	public String getFeatureFileIDForSalesModel(FeatureData featureData) {
		CatRestClientUtil.isValidData(featureData.getMarketingCompany(),featureData.getFirstFeatureSet(),featureData.getSalesModel());
		final List<FetchFBCFeatureFileResponseMapper> result = getFeatureFileNamesResource(featureData).stream()
				.filter(featureFileId -> featureFileId.getModel().equals(featureData.getSalesModel())
						&& featureFileId.getFirstFeatureSet().equals(featureData.getFirstFeatureSet()))
				.collect(Collectors.toList());
		if (result.isEmpty()) {
			throw new IllegalArgumentException("Unable to find feature file ID for the "+featureData.getSalesModel());
		}
		
		return result.get(0).getFileName();

	}
	private List<FetchFBCFeatureFileResponseMapper> getFeatureFileNamesResource(FeatureData featureData) {
		List<FetchFBCFeatureFileResponseMapper> featureResponse = new ArrayList<>();
		String endpoint = Config.getParameter(CatintegrationConstants.WS_FEATUREID_URL);
		
		if (StringUtils.isBlank(endpoint)) {
			throw new IllegalStateException(
					"FBC FeatureID "+CatintegrationConstants.FBC_URI_MSG);
		}
		
		String params = "marketingCompany="+ featureData.getMarketingCompany() + "&dealerCode=" + featureData.getDelearCode()+"&modelName="+featureData.getSalesModel();
		
		StringBuilder url = new StringBuilder();
		url.append(endpoint);
		url.append(params);
		
		try {
			ResponseEntity<FetchFBCFeatureFileResponseMapper[]> responseEntity = this.getCatRestTemplate().exchange(url.toString(), HttpMethod.GET, getHttpEntity(getHttpHeaders(this.getCatRestTemplate().getAccessToken())),
					FetchFBCFeatureFileResponseMapper[].class);
			FetchFBCFeatureFileResponseMapper[]	response = responseEntity.getBody();
			 featureResponse = Arrays.asList(response);

		} catch (final Exception e) {
			LOG.log(Level.ERROR, CatintegrationConstants.EXCEPTION_OCCURED+e.getMessage(), e);
		}
		return featureResponse;
	}
	
	private List<Feature> getLane1Features(FBCResponseMapper processSelectFBCAPIRequest) //NOSONAR
	{ 
			List<Feature> features = processSelectFBCAPIRequest.getFeatureSet().getFeatures();
			return features.stream().filter(feature -> CatintegrationConstants.LANE1.equalsIgnoreCase(feature.getFeatureCode())).collect(Collectors.toList());
	}
	
	
	/**
	 * {@inheritDoc}
	 *
	 */
	@Override
	public String initiateFBCSelectAPIRequest(FBCCreateRequestMapper fbcCreateRequestMapper,
			String dca,final String laneCode) {
		String jsonResponse = null;
		try {
			LOG.info("Processing DefaultCatFBCConfiguratorService");
			CatRestClientUtil.isValidData(fbcCreateRequestMapper.getMarketingCompany(),fbcCreateRequestMapper.getDealerCode(),dca);
			FBCConfigResponse fbcCreateAPIResource = this.getFBCCreateAPIResource(fbcCreateRequestMapper);
			FeatureData featuresByDCA = this.getFeaturesByDCA(fbcCreateAPIResource.getFeatureSet(),dca,fbcCreateAPIResource.getConfiguratorId());
			FBCResponseMapper response = getFBCSelectAPIResource(fbcCreateAPIResource.getConfiguratorId(),featuresByDCA.getFeatureCode());
			configIdLocal = fbcCreateAPIResource.getConfiguratorId();
			response = verifyLaneData(response,configIdLocal,laneCode);
			if(null!=response&&null!=response.getFeatureSet()&&(!response.getFeatureSet().getIsRequired())) {
					response = getFeatureSets(configIdLocal);
			}
			jsonResponse=CatRestClientUtil.getJSONFormattedString(response, jsonResponse,"getFBCSelectAPIResponse");
			if (null!=response && null == response.getFeatureSet() && null == response.getEdhError())
			{
				sessionService.setAttribute("selections", response.getSelections());
			}
			LOG.info("Processed DefaultCatFBCConfiguratorService");
		} catch (Exception e) {
			    EDHError edhError = CatRestClientUtil.processException(e);
				FBCConfigResponse fbcConfigResponse = new FBCConfigResponse();
				fbcConfigResponse.setEdhError(edhError);
				jsonResponse = CatRestClientUtil.getJSONResponse(fbcConfigResponse);
			
		
		}
		return jsonResponse;
	}
	
	private FeatureData getFeaturesByDCA(FeatureSet featureSet,String dca,String configID) throws CatDCANotFoundException{
		FeatureData featureData = null;
		if(null!=featureSet && BooleanUtils.isTrue(featureSet.getIsRequired())) {
				List<Feature> features = featureSet.getFeatures();
				List<Feature> dcaFeature = features.stream().filter(feature -> dca.equals(feature.getReferenceNumber())).collect(Collectors.toList());
				if (dcaFeature.isEmpty()) {
					throw new CatDCANotFoundException(
							"Unable to find DCA " + dca + " in FBC DCA's API List : " + features);
				}
				featureData = new FeatureData();
				featureData.setConfigId(configID);
				featureData.setFeatureCode(dcaFeature.get(0).getFeatureCode());
			}
			

		
		return featureData;
	}

	/**
	 *{@inheritDoc}
	 */
	@Override
	public String getFBCSelectAPIResponse(String configId, String featureCode) {
		String jsonResponse=null;
		String fbcSelectURL = Config.getParameter(CatintegrationConstants.WS_BASE_URL)
				+ Config.getParameter(CatintegrationConstants.WS_SELECT_URL) + configId + "/"
				+ featureCode;

		if (StringUtils.isBlank(fbcSelectURL)) {
			throw new IllegalStateException(
					"FBC Select "+CatintegrationConstants.FBC_URI_MSG);
		}
			final ResponseEntity<Object> responseEntity = this.getCatRestTemplate().exchange(fbcSelectURL,
					HttpMethod.POST, getHttpEntity(getHttpHeaders(this.getCatRestTemplate().getAccessToken())), Object.class);
			jsonResponse = CatRestClientUtil.getJSONFormattedString(responseEntity, jsonResponse,"getFBCSelectAPIResponse");
		return jsonResponse;
	}
	
	/**
	 * {@inheritDoc}
	 *
	 */
	@Override
	public FBCResponseMapper getSelectNextResponse(String configId, String featureCode) {
		FBCResponseMapper fbcResponseMapper =null;
		try {
			fbcResponseMapper = processResponseForFeatureSet(configIdLocal, featureCode);
		} catch (Exception e) {
			fbcResponseMapper = new FBCResponseMapper();
			EDHError edhError = CatRestClientUtil.processException(e);
			fbcResponseMapper.setEdhError(edhError);
		}
		return fbcResponseMapper;
	}
	
	private FBCResponseMapper processResponseForFeatureSet(String configId, String featureCode) throws Exception{
		FBCResponseMapper fbcSelectAPIResourceLoc = null;
		FBCResponseMapper fbcSelectAPIResource = getFBCSelectAPIResource(configId,featureCode);
		if(null==fbcSelectAPIResource.getFeatureSet()) {
			return fbcSelectAPIResource;
		}
			if(fbcSelectAPIResource.getFeatureSet().getIsRequired()){
				fbcSelectAPIResourceLoc = fbcSelectAPIResource;
			}
			else if(!fbcSelectAPIResource.getFeatureSet().getIsRequired()){
				fbcSelectAPIResourceLoc = getFeatureSets(configId);
			}
		return fbcSelectAPIResourceLoc;	
	}

	/**
	 * @param configId
	 * @throws CatRecordNotFoundException
	 */
	private FBCResponseMapper getFeatureSets(String configId){
		FBCResponseMapper processFBCSelectNextAPIRequest = getFBCSelectNextAPIResource(configId);
		if(null!=processFBCSelectNextAPIRequest.getFeatureSet() && !processFBCSelectNextAPIRequest.getFeatureSet().getIsRequired()) {
			return getFeatureSets(configId);
		}
		return processFBCSelectNextAPIRequest;
	}
	//Filters LANES
	private FBCResponseMapper verifyLaneData(FBCResponseMapper processSelectFBCAPIRequest,final String configId,final String laneCode) throws  Exception//NOSONAR
	{ 
		if(StringUtils.isNotBlank(laneCode)) {
			try {
				List<Feature> features = processSelectFBCAPIRequest.getFeatureSet().getFeatures();
				processSelectFBCAPIRequest = setProcessSelectFBCAPIRequest(processSelectFBCAPIRequest, configId,
						laneCode, features);
			} catch (Exception e) {
				throw new Exception(e);
			}
		}
			return processSelectFBCAPIRequest;
	}

	/**
	 * This method iterates over the feature set to set the FBCSelectAPIResource
	 * @param processSelectFBCAPIRequest The object to be set
	 * @param configId Config id
	 * @param laneCode Lane code
	 * @param features set of features
	 * @return FBCResponseMapper the selectapiresource object
	 * @throws Exception
	 */
	private FBCResponseMapper setProcessSelectFBCAPIRequest(FBCResponseMapper processSelectFBCAPIRequest,
			final String configId, final String laneCode, List<Feature> features) throws Exception {
		for(Feature feature : features) {
			if(laneCode.equals(feature.getFeatureCode())) {
				processSelectFBCAPIRequest = getFBCSelectAPIResource(configId,feature.getFeatureCode());
				break;
			}
		}
		return processSelectFBCAPIRequest;
	}
	
}
