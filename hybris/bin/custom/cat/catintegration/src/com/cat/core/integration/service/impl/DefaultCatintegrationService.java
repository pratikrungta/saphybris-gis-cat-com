/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.core.integration.service.impl;

import de.hybris.platform.catalog.model.CatalogUnawareMediaModel;
import de.hybris.platform.core.model.media.MediaModel;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.media.MediaService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.io.InputStream;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.cat.core.integration.service.CatintegrationService;


public class DefaultCatintegrationService implements CatintegrationService //NOSONAR
{
    private static final Logger LOG = Logger.getLogger(DefaultCatintegrationService.class);

    private  static final String FIND_LOGO_QUERY = "SELECT {" + CatalogUnawareMediaModel.PK + "} FROM {"
            + CatalogUnawareMediaModel._TYPECODE + "} WHERE {" + CatalogUnawareMediaModel.CODE + "}=?code";

    private MediaService mediaService;
    private ModelService modelService;
    private FlexibleSearchService flexibleSearchService;



    @Override
    public String getHybrisLogoUrl(final String logoCode)
    {
        final MediaModel media = mediaService.getMedia(logoCode);

        // Keep in mind that with Slf4j you don't need to check if debug is enabled, it is done under the hood.
        LOG.debug("Found media [code: {}]"+ media.getCode());

        return media.getURL();
    }

    @Override
    public void createLogo(final String logoCode)
    {
        final Optional<CatalogUnawareMediaModel> existingLogo = findExistingLogo(logoCode);

        final CatalogUnawareMediaModel media = existingLogo.isPresent() ? existingLogo.get()
                : modelService.create(CatalogUnawareMediaModel.class);
        media.setCode(logoCode);
        media.setRealFileName("sap-hybris-platform.png");
        modelService.save(media);

        mediaService.setStreamForMedia(media, getImageStream());
    }



    private Optional<CatalogUnawareMediaModel> findExistingLogo(final String logoCode)
    {
        final FlexibleSearchQuery fQuery = new FlexibleSearchQuery(FIND_LOGO_QUERY);
        fQuery.addQueryParameter("code", logoCode);

        try
        {
            return Optional.of(flexibleSearchService.searchUnique(fQuery));
        }
        catch (final SystemException e)
        {
            LOG.error("Exception in DefaultCatintegrationService.findExistingLogo() :",e);
            return Optional.empty();
        }
    }

    private static InputStream getImageStream()
    {
        return DefaultCatintegrationService.class.getResourceAsStream("/catintegration/sap-hybris-platform.png");
    }

    @Required
    public void setMediaService(final MediaService mediaService)
    {
        this.mediaService = mediaService;
    }

    @Required
    public void setModelService(final ModelService modelService)
    {
        this.modelService = modelService;
    }

    @Required
    public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
    {
        this.flexibleSearchService = flexibleSearchService;
    }
}
