/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.core.integration.setup;


import de.hybris.platform.core.initialization.SystemSetup;



import com.cat.core.integration.constants.CatintegrationConstants;
import com.cat.core.integration.service.CatintegrationService;


@SystemSetup(extension = CatintegrationConstants.EXTENSIONNAME)
public class CatintegrationSystemSetup
{
	public static final String PLATFORM_LOGO_CODE = "catintegrationPlatformLogo";
	
    private final CatintegrationService catintegrationService;

    public CatintegrationSystemSetup(final CatintegrationService catintegrationService)
    {
        this.catintegrationService = catintegrationService;
    }

    @SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
    public void createEssentialData()
    {
        catintegrationService.createLogo(PLATFORM_LOGO_CODE);
    }
}
