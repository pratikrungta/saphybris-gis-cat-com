package com.cat.core.integration.snop.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "TRAN_CODE_IN",
    "MESSAGE_AREA_IN"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class CatSnopRequestMapper {

	/**
	 * @return the transactionCode
	 */
	@JsonProperty("TRAN_CODE_IN")
	public String getTransactionCode() {
		return transactionCode;
	}
	/**
	 * @param transactionCode the transactionCode to set
	 */
	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}
	/**
	 * @return the messageArea
	 */
	@JsonProperty("MESSAGE_AREA_IN")
	public List<CatSnopSubRequest> getMessageArea() {
		return messageArea;
	}
	/**
	 * @param messageArea the messageArea to set
	 */
	public void setMessageArea(List<CatSnopSubRequest> messageArea) {
		this.messageArea = messageArea;
	}
	
	@JsonProperty("TRAN_CODE_IN")
	private String transactionCode;
	
	@JsonProperty("MESSAGE_AREA_IN")
	private List<CatSnopSubRequest> messageArea;
}
