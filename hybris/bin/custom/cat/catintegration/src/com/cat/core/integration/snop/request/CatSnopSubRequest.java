package com.cat.core.integration.snop.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ORD_QTY_IN",
    "TOP_LVL_IN",
    "DLR_CODE_IN",
    "LANE_IN"
})
public class CatSnopSubRequest {

	@JsonProperty("ORD_QTY_IN")
    private String orderedQty;

	@JsonProperty("TOP_LVL_IN")
    private String topLevelPartNumber;

	@JsonProperty("DLR_CODE_IN")
    private String dealerCode;
	
	@JsonProperty("LANE_IN")
    private String laneType;
	

    /**
	 * @return the laneType
	 */
	@JsonProperty("LANE_IN")
	public String getLaneType() {
		return laneType;
	}
	/**
	 * @param laneType the laneType to set
	 */
	public void setLaneType(String laneType) {
		this.laneType = laneType;
	}
	/**
	 * @return the orderedQty
	 */
	@JsonProperty("ORD_QTY_IN")
	public String getOrderedQty() {
		return orderedQty;
	}
	/**
	 * @param orderedQty the orderedQty to set
	 */
	public void setOrderedQty(String orderedQty) {
		this.orderedQty = orderedQty;
	}
	/**
	 * @return the dca
	 */
	@JsonProperty("TOP_LVL_IN")
	public String getTopLevelPartNumber() {
		return topLevelPartNumber;
	}
	/**
	 * @param dca the dca to set
	 */
	public void setTopLevelPartNumber(String topLevelPartNumber) {
		this.topLevelPartNumber = topLevelPartNumber;
	}
	/**
	 * @return the dealerCode
	 */
	@JsonProperty("DLR_CODE_IN")
	public String getDealerCode() {
		return dealerCode;
	}
	/**
	 * @param dealerCode the dealerCode to set
	 */
	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}

}
