package com.cat.core.integration.snop.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "LL_OUT",
    "ZZ_OUT",
    "IIB_TransactionID",
    "MESSAGE_AREA_OUT"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class CatSnopResponseMapper {

	@JsonProperty("LL_OUT")
	private String llOut;
	@JsonProperty("IIB_TransactionID")
	private String iibTransactionId;
	
	/**
	 * @return the iibTransactionId
	 */
	@JsonProperty("IIB_TransactionID")
	public String getIibTransactionId() {
		return iibTransactionId;
	}
	/**
	 * @param iibTransactionId the iibTransactionId to set
	 */
	public void setIibTransactionId(String iibTransactionId) {
		this.iibTransactionId = iibTransactionId;
	}
	@JsonProperty("ZZ_OUT")
	private String zzOut;
	
	@JsonProperty("MESSAGE_AREA_OUT")
	private List<CatSnopSubResponse> messageArea;
	/**
	 * @return the llOut
	 */
	@JsonProperty("LL_OUT")
	public String getLlOut() {
		return llOut;
	}
	/**
	 * @param llOut the llOut to set
	 */
	public void setLlOut(String llOut) {
		this.llOut = llOut;
	}
	/**
	 * @return the zzOut
	 */
	@JsonProperty("ZZ_OUT")
	public String getZzOut() {
		return zzOut;
	}
	/**
	 * @param zzOut the zzOut to set
	 */
	public void setZzOut(String zzOut) {
		this.zzOut = zzOut;
	}
	/**
	 * @return the messageArea
	 */
	@JsonProperty("MESSAGE_AREA_OUT")
	public List<CatSnopSubResponse> getMessageArea() {
		return messageArea;
	}
	/**
	 * @param messageArea the messageArea to set
	 */
	public void setMessageArea(List<CatSnopSubResponse> messageArea) {
		this.messageArea = messageArea;
	}
}
