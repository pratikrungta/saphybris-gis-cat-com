package com.cat.core.integration.snop.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "orderedQty",
    "dca",
    "dealerCode",
    "lane",
    "source",
    "salesModel",
    "orderLimitSw",
    "orderLimitOut",
    "orderLimitUsed",
    "orderBomSW",
    "orderBomOut",
    "orderBomUsed",
    "sopIndicator",
    "bomAccountingDays",
    "sopAccountingDays"    
})
public class CatSnopSubResponse {

	@JsonProperty("ORD_QTY_OUT")
    private String orderedQty;
    /**
	 * @return the orderedQty
	 */
	@JsonProperty("ORD_QTY_OUT")
	public String getOrderedQty() {
		return orderedQty;
	}
	/**
	 * @param orderedQty the orderedQty to set
	 */
	public void setOrderedQty(String orderedQty) {
		this.orderedQty = orderedQty;
	}
	/**
	 * @return the dca
	 */
	@JsonProperty("TOP_LVL_OUT")
	public String getDca() {
		return dca;
	}
	/**
	 * @param dca the dca to set
	 */
	public void setDca(String dca) {
		this.dca = dca;
	}
	/**
	 * @return the dealerCode
	 */
	@JsonProperty("DLR_CODE_OUT")
	public String getDealerCode() {
		return dealerCode;
	}
	/**
	 * @param dealerCode the dealerCode to set
	 */
	public void setDealerCode(String dealerCode) {
		this.dealerCode = dealerCode;
	}
	@JsonProperty("TOP_LVL_OUT")
    private String dca;
	
    @JsonProperty("DLR_CODE_OUT")
    private String dealerCode;
    
    @JsonProperty("LANE_OUT")
    private String lane;
    
    @JsonProperty("SOURCE_OUT")
    private String source;
    
    @JsonProperty("SLS_MDL_OUT")
    private String salesModel;
    
    @JsonProperty("ORD_LMT_SW")
    private String orderLimitSw;
    
    @JsonProperty("ORD_LMT_OUT")
    private String orderLimitOut;
    
    @JsonProperty("ORD_LMT_USED_OUT")
    private String orderLimitUsed;
    
    @JsonProperty("ORD_BOM_SW")
    private String orderBomSW;
    
    @JsonProperty("ORD_BOM_OUT")
    private String orderBomOut;
    
    @JsonProperty("ORD_BOM_USED_OUT")
    private String orderBomUsed;
    
    @JsonProperty("SOP_IND_OUT")
    private String sopIndicator;
    
    @JsonProperty("BOM_ACCTG_DAY_OUT")
    private String bomAccountingDays;
    
    @JsonProperty("SOP_ACCTG_DAY_OUT")
    private String sopAccountingDays;
	/**
	 * @return the lane
	 */
    @JsonProperty("LANE_OUT")
	public String getLane() {
		return lane;
	}
	/**
	 * @param lane the lane to set
	 */
	public void setLane(String lane) {
		this.lane = lane;
	}
	/**
	 * @return the source
	 */
	 @JsonProperty("SOURCE_OUT")
	public String getSource() {
		return source;
	}
	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}
	/**
	 * @return the salesModel
	 */
	 @JsonProperty("SLS_MDL_OUT")
	public String getSalesModel() {
		return salesModel;
	}
	/**
	 * @param salesModel the salesModel to set
	 */
	public void setSalesModel(String salesModel) {
		this.salesModel = salesModel;
	}
	/**
	 * @return the orderLimitSw
	 */
	 @JsonProperty("ORD_LMT_SW")
	public String getOrderLimitSw() {
		return orderLimitSw;
	}
	/**
	 * @param orderLimitSw the orderLimitSw to set
	 */
	public void setOrderLimitSw(String orderLimitSw) {
		this.orderLimitSw = orderLimitSw;
	}
	/**
	 * @return the orderLimitOut
	 */
	 @JsonProperty("ORD_LMT_OUT")
	public String getOrderLimitOut() {
		return orderLimitOut;
	}
	/**
	 * @param orderLimitOut the orderLimitOut to set
	 */
	public void setOrderLimitOut(String orderLimitOut) {
		this.orderLimitOut = orderLimitOut;
	}
	/**
	 * @return the orderLimitUsed
	 */
	 @JsonProperty("ORD_LMT_USED_OUT")
	public String getOrderLimitUsed() {
		return orderLimitUsed;
	}
	/**
	 * @param orderLimitUsed the orderLimitUsed to set
	 */
	public void setOrderLimitUsed(String orderLimitUsed) {
		this.orderLimitUsed = orderLimitUsed;
	}
	/**
	 * @return the orderBomSW
	 */
	@JsonProperty("ORD_BOM_SW")
	public String getOrderBomSW() {
		return orderBomSW;
	}
	/**
	 * @param orderBomSW the orderBomSW to set
	 */
	public void setOrderBomSW(String orderBomSW) {
		this.orderBomSW = orderBomSW;
	}
	/**
	 * @return the orderBomOut
	 */
	@JsonProperty("ORD_BOM_OUT")
	public String getOrderBomOut() {
		return orderBomOut;
	}
	/**
	 * @param orderBomOut the orderBomOut to set
	 */
	public void setOrderBomOut(String orderBomOut) {
		this.orderBomOut = orderBomOut;
	}
	/**
	 * @return the orderBomUsed
	 */
	@JsonProperty("ORD_BOM_USED_OUT")
	public String getOrderBomUsed() {
		return orderBomUsed;
	}
	/**
	 * @param orderBomUsed the orderBomUsed to set
	 */
	public void setOrderBomUsed(String orderBomUsed) {
		this.orderBomUsed = orderBomUsed;
	}
	/**
	 * @return the sopIndicator
	 */
	@JsonProperty("SOP_IND_OUT")
	public String getSopIndicator() {
		return sopIndicator;
	}
	/**
	 * @param sopIndicator the sopIndicator to set
	 */
	public void setSopIndicator(String sopIndicator) {
		this.sopIndicator = sopIndicator;
	}
	/**
	 * @return the bomAccountingDays
	 */
	@JsonProperty("BOM_ACCTG_DAY_OUT")
	public String getBomAccountingDays() {
		return bomAccountingDays;
	}
	/**
	 * @param bomAccountingDays the bomAccountingDays to set
	 */
	public void setBomAccountingDays(String bomAccountingDays) {
		this.bomAccountingDays = bomAccountingDays;
	}
	/**
	 * @return the sopAccountingDays
	 */
	@JsonProperty("SOP_ACCTG_DAY_OUT")
	public String getSopAccountingDays() {
		return sopAccountingDays;
	}
	/**
	 * @param sopAccountingDays the sopAccountingDays to set
	 */
	public void setSopAccountingDays(String sopAccountingDays) {
		this.sopAccountingDays = sopAccountingDays;
	}
	
    
    
}
