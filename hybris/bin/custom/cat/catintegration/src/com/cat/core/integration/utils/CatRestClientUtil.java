package com.cat.core.integration.utils;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import com.cat.core.integration.constants.CatintegrationConstants;
import com.cat.core.integration.edh.response.EDHError;
import com.cat.core.integration.exception.CatEDHException;
import com.cat.core.integration.exception.CatRecordNotFoundException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.hybris.platform.util.Config;
import de.hybris.platform.util.localization.Localization;


/**
 * This Cat RestClientUtil class
 * @author sjeedula
 *
 */
public class CatRestClientUtil {
	private static final Logger LOG = Logger.getLogger(CatRestClientUtil.class);
	 private CatRestClientUtil() {
	      //not called
	   }
	
	/**
	 * @param webserviceName : Response JSON String
	 * @param response : actual Response
	 */
	public static void logResponse(final Object response, final String webserviceName)
	{
		final ObjectMapper responseMapper = new ObjectMapper();
		String resjsonInString = null;
		try
		{
			resjsonInString = responseMapper.writeValueAsString(response);
			LOG.debug(webserviceName + " Response <--- JSON String --->  \n" + resjsonInString);
		}
		catch (final JsonProcessingException e)
		{
			LOG.error(e);
		}

	}
	/**
	 * @param response : actual JSON Response.
	 * @return : returns JSON String
	 */
	public static String getJSONResponse(final Object response)
	{
		final ObjectMapper responseMapper = new ObjectMapper();
		String resjsonInString = null;
		try
		{
			resjsonInString = responseMapper.writeValueAsString(response);
		}
		catch (final JsonProcessingException e)
		{
			LOG.error(e);
		}
		return resjsonInString;
	}
	/**
	 * @param request : Input request
	 * @param endpoint : EDH endpoint 
	 * @param webServiceName : EDH webservice Call
	 * @param productReference : Product reference field
	 */
	public static void logRequest(final Object request, final String endpoint, final String webServiceName, final String productReference)
	{
		final ObjectMapper requestMapper = new ObjectMapper();
		String reqjsonInString = null;
		try
		{
			reqjsonInString = requestMapper.writeValueAsString(request);
			LOG.debug(webServiceName + "  Request <--- JSON String ---> URL: " + endpoint + "  - productReference " + productReference + "\n"
					+ reqjsonInString);
		}
		catch (final JsonProcessingException e1)
		{
			LOG.error(e1);
		}
	}	
	
	/**
	 * @param marketingCompany : this to check the data
	 * @param firstFeatureSet : First feature set
	 * @param salesModel: product sales model
	 */
	public static void isValidData(String marketingCompany,String firstFeatureSet,String salesModel) {
		if (marketingCompany.isEmpty() || (firstFeatureSet.isEmpty() || salesModel.isEmpty())) {
			throw new IllegalArgumentException("Please Provide Missing Parameters.");
		}
	}
	
	/**
	 * @param marketingCompany input parameters
	 * @param firstFeatureSet input parameters
	 */
	public static void isValidData(String marketingCompany,String firstFeatureSet) {
		if (StringUtils.isBlank(marketingCompany) || StringUtils.isBlank(firstFeatureSet)) {
			throw new IllegalArgumentException("Please Provide Missing Parameters.");
		}
	}
	
	/**
	 * @param response
	 * @return generic response.
	 */
	public static <T> T getMockData(final Class<T> responseClass)
	{
		final String filePath = Config.getParameter("mock.data.path.folderpath") + responseClass.getSimpleName() + ".json";

		final ObjectMapper mapper = new ObjectMapper();
		T o = null;
		try
		{
			o = mapper.readValue(new File(filePath), responseClass);
		}
		catch (final IOException e)
		{
			LOG.error("Error parsing Mock Data for " + responseClass.getName(), e);
		}
		return o;
	}

	/**
	 * @param e actual exception
	 * @throws CatEDHException : generic CatException.
	 */
	public static EDHError processException(final Exception e) //NOSONAR
	{
		EDHError edhError = new EDHError(); 
		if (e.getCause() instanceof ConnectException || e.getCause() instanceof SocketTimeoutException)
		{
			LOG.log(Level.ERROR," ConnectException "+e.getMessage(), e);
			getErrorMSG(edhError);

		}
		else if (e instanceof HttpClientErrorException)
		{
			final HttpClientErrorException hce = (HttpClientErrorException) e;
			LOG.log(Level.ERROR, " HttpClientErrorException "+hce.getStatusCode()+" "+hce.getMessage(), hce);
			getErrorMSG(edhError);
		}
		else if (e instanceof HttpServerErrorException)
		{
			final HttpServerErrorException hse = (HttpServerErrorException) e;
			LOG.log(Level.ERROR, " HttpServerErrorException "+hse.getStatusCode()+" "+hse.getMessage(), hse);
			getErrorMSG(edhError);
		}
		else if (e instanceof UnknownHostException)
		{
			final UnknownHostException uhe = (UnknownHostException) e;
			LOG.log(Level.ERROR, " UnknownHostException  "+uhe.getMessage(), uhe);
			getErrorMSG(edhError);
		}
		else if (e instanceof CatRecordNotFoundException)
		{
			final CatRecordNotFoundException crne = (CatRecordNotFoundException) e;
			LOG.log(Level.ERROR, " CatRecordNotFoundException  "+crne.getMessage(), crne);
			edhError.setDescription(Localization.getLocalizedString(CatintegrationConstants.NO_RESULTS));
			edhError.setCode("600");
		}
		else if (e instanceof ResourceAccessException)
		{
			final ResourceAccessException rae = (ResourceAccessException) e;
			LOG.log(Level.ERROR, " ResourceAccessException  "+rae.getMessage(), rae);
			getErrorMSG(edhError);
		}
		else
		{
			LOG.log(Level.ERROR, " UNEXPECTED "+e.getMessage(), e);
			getErrorMSG(edhError);
		}
		return edhError;
		
	}
	private static void getErrorMSG(EDHError edhError) {
		edhError.setDescription(Localization.getLocalizedString(CatintegrationConstants.ERROR_MSG));
		edhError.setMailid(Localization.getLocalizedString(CatintegrationConstants.ERROR_MSG_MAILID));
	}
	public static <T> T getJSONObject(final Class<T> responseClass,String response)
	{

		final ObjectMapper mapper = new ObjectMapper();
		T o = null;
		try
		{
			o = mapper.readValue(response, responseClass);
		}
		catch (final IOException e)
		{
			LOG.error("Error parsing Mock Data for " + responseClass.getName(), e);
		}
		return o;
	}
	public static String getJSONFormattedString(Object responseEntity, String jsonResponse,String msg)
    {
          if(null!=responseEntity){
              CatRestClientUtil.logResponse(responseEntity, msg);
              jsonResponse = CatRestClientUtil.getJSONResponse(responseEntity);
          }
          return jsonResponse;
    }

}
