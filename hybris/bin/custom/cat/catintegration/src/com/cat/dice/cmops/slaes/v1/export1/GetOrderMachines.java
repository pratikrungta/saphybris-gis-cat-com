
package com.cat.dice.cmops.slaes.v1.export1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="orderMachinesRequest" type="{http://www.cat.com/service/cmops/OrderMachinesRequest/v1}OrderMachinesRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "orderMachinesRequest"
})
@XmlRootElement(name = "getOrderMachines")
public class GetOrderMachines {

    @XmlElement(required = true, nillable = true)
    protected OrderMachinesRequest orderMachinesRequest;

    /**
     * Gets the value of the orderMachinesRequest property.
     * 
     * @return
     *     possible object is
     *     {@link OrderMachinesRequest }
     *     
     */
    public OrderMachinesRequest getOrderMachinesRequest() {
        return orderMachinesRequest;
    }

    /**
     * Sets the value of the orderMachinesRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderMachinesRequest }
     *     
     */
    public void setOrderMachinesRequest(OrderMachinesRequest value) {
        this.orderMachinesRequest = value;
    }

}
