
package com.cat.dice.cmops.slaes.v1.export1;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "getOrderMachinesFault1_getOrderMachinesFault", targetNamespace =
        "http://www.cat.com/service/cmops/CMOPSOrderingService/v1")
public class GetOrderMachinesGetOrderMachinesFaultMsg
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private String faultInfo;//NOSONAR

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public GetOrderMachinesGetOrderMachinesFaultMsg(String message, String faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param cause
     * @param message
     */
    public GetOrderMachinesGetOrderMachinesFaultMsg(String message, String faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: java.lang.String
     */
    public String getFaultInfo() {
        return faultInfo;
    }

}
