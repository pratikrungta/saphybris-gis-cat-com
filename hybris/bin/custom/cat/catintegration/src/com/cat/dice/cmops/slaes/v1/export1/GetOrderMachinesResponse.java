
package com.cat.dice.cmops.slaes.v1.export1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="orderMachinesResponse" type="{http://www.cat.com/service/cmops/OrderMachinesResponse/v1}OrderMachinesResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "orderMachinesResponse"
})
@XmlRootElement(name = "getOrderMachinesResponse")
public class GetOrderMachinesResponse {

    @XmlElement(required = true, nillable = true)
    protected OrderMachinesResponse orderMachinesResponse;

    /**
     * Gets the value of the orderMachinesResponse property.
     * 
     * @return
     *     possible object is
     *     {@link OrderMachinesResponse }
     *     
     */
    public OrderMachinesResponse getOrderMachinesResponse() {
        return orderMachinesResponse;
    }

    /**
     * Sets the value of the orderMachinesResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderMachinesResponse }
     *     
     */
    public void setOrderMachinesResponse(OrderMachinesResponse value) {
        this.orderMachinesResponse = value;
    }

}
