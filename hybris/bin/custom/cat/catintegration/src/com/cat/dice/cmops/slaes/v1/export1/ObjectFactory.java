
package com.cat.dice.cmops.slaes.v1.export1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the export1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetOrderMachinesFault1GetOrderMachinesFault_QNAME = new QName("http://www.cat.com/service/cmops/CMOPSOrderingService/v1", "getOrderMachinesFault1_getOrderMachinesFault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: export1
     * 
     */
    public ObjectFactory() {//NOSONAR
    }

    /**
     * Create an instance of {@link GetOrderMachines }
     * 
     */
    public GetOrderMachines createGetOrderMachines() {
        return new GetOrderMachines();
    }

    /**
     * Create an instance of {@link OrderMachinesRequest }
     * 
     */
    public OrderMachinesRequest createOrderMachinesRequest() {
        return new OrderMachinesRequest();
    }

    /**
     * Create an instance of {@link GetOrderMachinesResponse }
     * 
     */
    public GetOrderMachinesResponse createGetOrderMachinesResponse() {
        return new GetOrderMachinesResponse();
    }

    /**
     * Create an instance of {@link OrderMachinesResponse }
     * 
     */
    public OrderMachinesResponse createOrderMachinesResponse() {
        return new OrderMachinesResponse();
    }

    /**
     * Create an instance of {@link OrderRecordsType }
     * 
     */
    public OrderRecordsType createOrderRecordsType() {
        return new OrderRecordsType();
    }

    /**
     * Create an instance of {@link OrderResultsType }
     * 
     */
    public OrderResultsType createOrderResultsType() {
        return new OrderResultsType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.cat.com/service/cmops/CMOPSOrderingService/v1", name = "getOrderMachinesFault1_getOrderMachinesFault")
    public JAXBElement<String> createGetOrderMachinesFault1GetOrderMachinesFault(String value) {
        return new JAXBElement<String>(_GetOrderMachinesFault1GetOrderMachinesFault_QNAME, String.class, null, value);
    }

}
