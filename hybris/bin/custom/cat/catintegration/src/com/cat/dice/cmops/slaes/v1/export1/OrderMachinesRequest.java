
package com.cat.dice.cmops.slaes.v1.export1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderMachinesRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderMachinesRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="orderRecords" type="{http://www.cat.com/service/cmops/orderRecordsType/v1}orderRecordsType" maxOccurs="399" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderMachinesRequest", namespace = "http://www.cat.com/service/cmops/OrderMachinesRequest/v1", propOrder = {
    "orderRecords"
})
public class OrderMachinesRequest {

    protected List<OrderRecordsType> orderRecords;

    /**
     * Gets the value of the orderRecords property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the orderRecords property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderRecords().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderRecordsType }
     * 
     * 
     */
    public List<OrderRecordsType> getOrderRecords() {
        if (orderRecords == null) {
            orderRecords = new ArrayList<OrderRecordsType>();
        }
        return this.orderRecords;
    }

}
