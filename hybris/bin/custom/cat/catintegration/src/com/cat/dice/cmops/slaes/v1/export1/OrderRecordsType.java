
package com.cat.dice.cmops.slaes.v1.export1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for orderRecordsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="orderRecordsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="orderRecord" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="80"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "orderRecordsType", namespace = "http://www.cat.com/service/cmops/orderRecordsType/v1", propOrder = {
    "orderRecord"
})
public class OrderRecordsType {

    protected String orderRecord;

    /**
     * Gets the value of the orderRecord property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderRecord() {
        return orderRecord;
    }

    /**
     * Sets the value of the orderRecord property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderRecord(String value) {
        this.orderRecord = value;
    }

}
