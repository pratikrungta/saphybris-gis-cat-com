
package com.cat.dice.cmops.slaes.v1.export1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for orderResultsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="orderResultsType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="orderMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "orderResultsType", namespace = "http://www.cat.com/service/cmops/orderResultsType/v1", propOrder = {
    "orderMessage"
})
public class OrderResultsType {

    protected String orderMessage;

    /**
     * Gets the value of the orderMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderMessage() {
        return orderMessage;
    }

    /**
     * Sets the value of the orderMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderMessage(String value) {
        this.orderMessage = value;
    }

}
