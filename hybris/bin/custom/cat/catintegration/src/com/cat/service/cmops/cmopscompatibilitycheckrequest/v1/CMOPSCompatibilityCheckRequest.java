
package com.cat.service.cmops.cmopscompatibilitycheckrequest.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.cat.service.cmops.specs.v1.Specs;


/**
 * <p>Java class for CMOPSCompatibilityCheckRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CMOPSCompatibilityCheckRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="messageType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="dealerCode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="dealerOrderNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="paymentTermsCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="manufacturingFacilityCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="compatibilityVerifyNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="7"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="orderProductType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="orderType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="dealerIdentificationNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="shipToDealerCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="orderPriorityType" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="priceSalesModel" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="specs" type="{http://www.cat.com/service/cmops/Specs/v1}Specs" maxOccurs="104"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CMOPSCompatibilityCheckRequest", propOrder = {
    "messageType",
    "dealerCode",
    "dealerOrderNumber",
    "paymentTermsCode",
    "manufacturingFacilityCode",
    "compatibilityVerifyNumber",
    "orderProductType",
    "orderType",
    "dealerIdentificationNumber",
    "shipToDealerCode",
    "orderPriorityType",
    "priceSalesModel",
    "specs"
})
public class CMOPSCompatibilityCheckRequest {

    @XmlElement(required = true)
    protected String messageType;
    @XmlElement(required = true)
    protected String dealerCode;
    protected String dealerOrderNumber;
    protected String paymentTermsCode;
    protected String manufacturingFacilityCode;
    protected String compatibilityVerifyNumber;
    protected String orderProductType;
    protected String orderType;
    protected String dealerIdentificationNumber;
    protected String shipToDealerCode;
    protected String orderPriorityType;
    protected String priceSalesModel;
    @XmlElement(required = true)
    protected List<Specs> specs;

    /**
     * Gets the value of the messageType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageType() {
        return messageType;
    }

    /**
     * Sets the value of the messageType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageType(String value) {
        this.messageType = value;
    }

    /**
     * Gets the value of the dealerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerCode() {
        return dealerCode;
    }

    /**
     * Sets the value of the dealerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerCode(String value) {
        this.dealerCode = value;
    }

    /**
     * Gets the value of the dealerOrderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerOrderNumber() {
        return dealerOrderNumber;
    }

    /**
     * Sets the value of the dealerOrderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerOrderNumber(String value) {
        this.dealerOrderNumber = value;
    }

    /**
     * Gets the value of the paymentTermsCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentTermsCode() {
        return paymentTermsCode;
    }

    /**
     * Sets the value of the paymentTermsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentTermsCode(String value) {
        this.paymentTermsCode = value;
    }

    /**
     * Gets the value of the manufacturingFacilityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManufacturingFacilityCode() {
        return manufacturingFacilityCode;
    }

    /**
     * Sets the value of the manufacturingFacilityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManufacturingFacilityCode(String value) {
        this.manufacturingFacilityCode = value;
    }

    /**
     * Gets the value of the compatibilityVerifyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompatibilityVerifyNumber() {
        return compatibilityVerifyNumber;
    }

    /**
     * Sets the value of the compatibilityVerifyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompatibilityVerifyNumber(String value) {
        this.compatibilityVerifyNumber = value;
    }

    /**
     * Gets the value of the orderProductType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderProductType() {
        return orderProductType;
    }

    /**
     * Sets the value of the orderProductType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderProductType(String value) {
        this.orderProductType = value;
    }

    /**
     * Gets the value of the orderType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * Sets the value of the orderType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderType(String value) {
        this.orderType = value;
    }

    /**
     * Gets the value of the dealerIdentificationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerIdentificationNumber() {
        return dealerIdentificationNumber;
    }

    /**
     * Sets the value of the dealerIdentificationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerIdentificationNumber(String value) {
        this.dealerIdentificationNumber = value;
    }

    /**
     * Gets the value of the shipToDealerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToDealerCode() {
        return shipToDealerCode;
    }

    /**
     * Sets the value of the shipToDealerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToDealerCode(String value) {
        this.shipToDealerCode = value;
    }

    /**
     * Gets the value of the orderPriorityType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderPriorityType() {
        return orderPriorityType;
    }

    /**
     * Sets the value of the orderPriorityType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderPriorityType(String value) {
        this.orderPriorityType = value;
    }

    /**
     * Gets the value of the priceSalesModel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriceSalesModel() {
        return priceSalesModel;
    }

    /**
     * Sets the value of the priceSalesModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriceSalesModel(String value) {
        this.priceSalesModel = value;
    }

    /**
     * Gets the value of the specs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpecs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Specs }
     * 
     * 
     */
    public List<Specs> getSpecs() {
        if (specs == null) {
            specs = new ArrayList<Specs>();
        }
        return this.specs;
    }
    
    public  void setSpecs(List<Specs> specs) {
        if (specs != null) {
            this.specs = specs;
        }
       
    }

}
