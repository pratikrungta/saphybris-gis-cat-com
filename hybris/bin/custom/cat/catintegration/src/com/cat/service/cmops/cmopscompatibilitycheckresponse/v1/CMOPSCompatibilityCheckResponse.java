
package com.cat.service.cmops.cmopscompatibilitycheckresponse.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.cat.service.cmops.specsresults.v1.SpecsResults;


/**
 * <p>Java class for CMOPSCompatibilityCheckResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CMOPSCompatibilityCheckResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="messageType">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="dealerCode">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="dealerOrderNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="paymentTermsCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="manufacturingFacilityCode" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="compatibilityVerifyNumber" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="7"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="compatibilityPassedOrFailedIndicator" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="paymentTermsPassedOrFailedIndicator" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="dateAndTimeStamp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="specsResults" type="{http://www.cat.com/service/cmops/SpecsResults/v1}SpecsResults" maxOccurs="428" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CMOPSCompatibilityCheckResponse", propOrder = {
    "messageType",
    "dealerCode",
    "dealerOrderNumber",
    "paymentTermsCode",
    "manufacturingFacilityCode",
    "compatibilityVerifyNumber",
    "compatibilityPassedOrFailedIndicator",
    "paymentTermsPassedOrFailedIndicator",
    "dateAndTimeStamp",
    "specsResults"
})
public class CMOPSCompatibilityCheckResponse {

    @XmlElement(required = true)
    protected String messageType;
    @XmlElement(required = true)
    protected String dealerCode;
    protected String dealerOrderNumber;
    protected String paymentTermsCode;
    protected String manufacturingFacilityCode;
    protected String compatibilityVerifyNumber;
    protected String compatibilityPassedOrFailedIndicator;
    protected String paymentTermsPassedOrFailedIndicator;
    protected String dateAndTimeStamp;
    protected List<SpecsResults> specsResults;

    /**
     * Gets the value of the messageType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageType() {
        return messageType;
    }

    /**
     * Sets the value of the messageType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageType(String value) {
        this.messageType = value;
    }

    /**
     * Gets the value of the dealerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerCode() {
        return dealerCode;
    }

    /**
     * Sets the value of the dealerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerCode(String value) {
        this.dealerCode = value;
    }

    /**
     * Gets the value of the dealerOrderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerOrderNumber() {
        return dealerOrderNumber;
    }

    /**
     * Sets the value of the dealerOrderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerOrderNumber(String value) {
        this.dealerOrderNumber = value;
    }

    /**
     * Gets the value of the paymentTermsCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentTermsCode() {
        return paymentTermsCode;
    }

    /**
     * Sets the value of the paymentTermsCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentTermsCode(String value) {
        this.paymentTermsCode = value;
    }

    /**
     * Gets the value of the manufacturingFacilityCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManufacturingFacilityCode() {
        return manufacturingFacilityCode;
    }

    /**
     * Sets the value of the manufacturingFacilityCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManufacturingFacilityCode(String value) {
        this.manufacturingFacilityCode = value;
    }

    /**
     * Gets the value of the compatibilityVerifyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompatibilityVerifyNumber() {
        return compatibilityVerifyNumber;
    }

    /**
     * Sets the value of the compatibilityVerifyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompatibilityVerifyNumber(String value) {
        this.compatibilityVerifyNumber = value;
    }

    /**
     * Gets the value of the compatibilityPassedOrFailedIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompatibilityPassedOrFailedIndicator() {
        return compatibilityPassedOrFailedIndicator;
    }

    /**
     * Sets the value of the compatibilityPassedOrFailedIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompatibilityPassedOrFailedIndicator(String value) {
        this.compatibilityPassedOrFailedIndicator = value;
    }

    /**
     * Gets the value of the paymentTermsPassedOrFailedIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentTermsPassedOrFailedIndicator() {
        return paymentTermsPassedOrFailedIndicator;
    }

    /**
     * Sets the value of the paymentTermsPassedOrFailedIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentTermsPassedOrFailedIndicator(String value) {
        this.paymentTermsPassedOrFailedIndicator = value;
    }

    /**
     * Gets the value of the dateAndTimeStamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateAndTimeStamp() {
        return dateAndTimeStamp;
    }

    /**
     * Sets the value of the dateAndTimeStamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateAndTimeStamp(String value) {
        this.dateAndTimeStamp = value;
    }

    /**
     * Gets the value of the specsResults property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specsResults property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpecsResults().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SpecsResults }
     * 
     * 
     */
    public List<SpecsResults> getSpecsResults() {
        if (specsResults == null) {
            specsResults = new ArrayList<SpecsResults>();
        }
        return this.specsResults;
    }

}
