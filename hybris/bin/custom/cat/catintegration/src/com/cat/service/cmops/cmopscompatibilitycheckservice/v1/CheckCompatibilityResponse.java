
package com.cat.service.cmops.cmopscompatibilitycheckservice.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.cat.service.cmops.cmopscompatibilitycheckresponse.v1.CMOPSCompatibilityCheckResponse;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CMOPSCompatibilityCheckResponse" type="{http://www.cat.com/service/cmops/CMOPSCompatibilityCheckResponse/v1}CMOPSCompatibilityCheckResponse"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cmopsCompatibilityCheckResponse"
})
@XmlRootElement(name = "checkCompatibilityResponse")
public class CheckCompatibilityResponse {

    @XmlElement(name = "CMOPSCompatibilityCheckResponse", required = true, nillable = true)
    protected CMOPSCompatibilityCheckResponse cmopsCompatibilityCheckResponse;

    /**
     * Gets the value of the cmopsCompatibilityCheckResponse property.
     * 
     * @return
     *     possible object is
     *     {@link CMOPSCompatibilityCheckResponse }
     *     
     */
    public CMOPSCompatibilityCheckResponse getCMOPSCompatibilityCheckResponse() {
        return cmopsCompatibilityCheckResponse;
    }

    /**
     * Sets the value of the cmopsCompatibilityCheckResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link CMOPSCompatibilityCheckResponse }
     *     
     */
    public void setCMOPSCompatibilityCheckResponse(CMOPSCompatibilityCheckResponse value) {
        this.cmopsCompatibilityCheckResponse = value;
    }

}
