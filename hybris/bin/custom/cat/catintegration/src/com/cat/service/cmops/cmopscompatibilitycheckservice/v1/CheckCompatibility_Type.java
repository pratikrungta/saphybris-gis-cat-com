
package com.cat.service.cmops.cmopscompatibilitycheckservice.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.cat.service.cmops.cmopscompatibilitycheckrequest.v1.CMOPSCompatibilityCheckRequest;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CMOPSCompatibilityCheckRequest" type="{http://www.cat.com/service/cmops/CMOPSCompatibilityCheckRequest/v1}CMOPSCompatibilityCheckRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cmopsCompatibilityCheckRequest"
})
@XmlRootElement(name = "checkCompatibility")
public class CheckCompatibility_Type {

    @XmlElement(name = "CMOPSCompatibilityCheckRequest", required = true, nillable = true)
    protected CMOPSCompatibilityCheckRequest cmopsCompatibilityCheckRequest;

    /**
     * Gets the value of the cmopsCompatibilityCheckRequest property.
     * 
     * @return
     *     possible object is
     *     {@link CMOPSCompatibilityCheckRequest }
     *     
     */
    public CMOPSCompatibilityCheckRequest getCMOPSCompatibilityCheckRequest() {
        return cmopsCompatibilityCheckRequest;
    }

    /**
     * Sets the value of the cmopsCompatibilityCheckRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link CMOPSCompatibilityCheckRequest }
     *     
     */
    public void setCMOPSCompatibilityCheckRequest(CMOPSCompatibilityCheckRequest value) {
        this.cmopsCompatibilityCheckRequest = value;
    }

}
