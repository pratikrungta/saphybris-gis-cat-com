package com.cat.core.integration.edh.service.impl;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.web.client.ResourceAccessException;

import com.cat.core.integration.exception.CatRecordNotFoundException;
import com.cat.core.integration.service.impl.CatEDHConfiguratorServiceImpl;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.servicelayer.ServicelayerBaseTest;

@IntegrationTest
public class CatEDHServiceImplTest extends ServicelayerBaseTest {

	CatEDHConfiguratorServiceImpl catEDHConfiguratorServiceImpl;
	private static final Logger LOG = Logger.getLogger(CatEDHServiceImplTest.class); 
	private String partyId ;
	private String configId ;
	private String getdecendents;
	
	@Before
	public void setUp() throws Exception
	{
		 partyId = "E140";
		 configId = "259-55de03f2ccd89795713ec4361fc49008_3888259";
		 getdecendents="false";
		catEDHConfiguratorServiceImpl = new CatEDHConfiguratorServiceImpl();
		
	}
	
	@Test
	public void testGetInventoryCountByPartyId() throws Exception{
		
			String inventoryCountByPartyId = catEDHConfiguratorServiceImpl.getInventoryCountByPartyId(partyId, configId, getdecendents);
			assertNotNull(inventoryCountByPartyId);
			assertEquals("[null]",inventoryCountByPartyId);
	}

	
	@Test
	public void testCatRecordNotFoundExceptionResponse() throws Exception{
		try {
			catEDHConfiguratorServiceImpl.getInventoryCountByPartyId(partyId, configId, getdecendents);
		} catch (CatRecordNotFoundException e) {
			assertEquals("No Results Found", e.getMessage());
		}  
	}
	
	
	
	
	
}
