/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.jalo;

import de.hybris.platform.europe1.constants.Europe1Tools;
import de.hybris.platform.europe1.jalo.PriceRow;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.C2LManager;
import de.hybris.platform.jalo.c2l.Currency;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.order.price.JaloPriceFactoryException;
import de.hybris.platform.jalo.order.price.PriceInformation;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.product.Unit;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.util.DateRange;
import de.hybris.platform.util.PriceValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;

import com.cat.bcp.core.jalo.ConfigVariantProduct;


/**
 * This is the extension manager of the Catpricefactory extension.
 */
public class CatpricefactoryManager extends GeneratedCatpricefactoryManager
{
	/**
	 * This method is used to fetch Price row.
	 *
	 * @param sessionContext
	 * @param product
	 * @param productGroup
	 * @param user
	 * @param userGroup
	 * @param qtd
	 * @param unit
	 * @param currency
	 * @param date
	 * @param net
	 * @param giveAwayMode
	 * @return PriceRow
	 */
	@Override
	public PriceRow matchPriceRowForPrice(final SessionContext ctx, final Product product, final EnumerationValue productGroup,
			final User user, final EnumerationValue userGroup, final long qtd, final Unit unit, final Currency currency,
			final Date date, final boolean net, final boolean giveAwayMode) throws JaloPriceFactoryException
	{

		verifyMandatoryAttributes(product, productGroup, user, userGroup, currency, date);

		//Get price rows from DB with product and customer as criteria
		final Collection<PriceRow> rows = queryPriceRows4Price(ctx, product, productGroup, user, userGroup);

		if (!(rows.isEmpty()))
		{
			final List ret = new ArrayList(rows);
			if (ret.size() > 1)
			{
				Collections.sort(ret, new PriceRowInfoComparator(currency));
			}

			//Filter rows for
			final List<PriceRow> list = filterPriceRows(filterPriceRows4Price(ret, qtd, unit, currency, date));
			if (CollectionUtils.isNotEmpty(list))
			{
				return list.get(0);
			}
		}

		return null;
	}

	/**
	 * This method is used to filter Price rows.
	 *
	 * @param Collection
	 *           of PriceRow
	 * @param qtd
	 * @param unit
	 * @param curr
	 * @param date
	 *
	 * @return List of PriceRow
	 */
	protected List<PriceRow> filterPriceRows4Price(final Collection<PriceRow> rows, final long qtd, final Unit unit,
			final Currency curr, final Date date)
	{
		if (rows.isEmpty())
		{
			return Collections.emptyList();

		}
		final Currency base = (curr.isBase().booleanValue()) ? null : C2LManager.getInstance().getBaseCurrency();
		final List ret = new ArrayList(rows);
		final long quantity = (qtd == 0L) ? 1L : qtd;
		for (final ListIterator it = ret.listIterator(); it.hasNext();)
		{
			final PriceRow priceRow = (PriceRow) it.next();

			if (quantity < priceRow.getMinqtdAsPrimitive())
			{
				it.remove();
			}

			final Currency currency = priceRow.getCurrency();
			if ((!(curr.equals(currency))) && ((base == null) || (!(base.equals(currency)))))
			{
				it.remove();
			}
			else
			{
				filterForUnitAndDate(unit, date, it, priceRow);
			}
		}
		return ret;
	}

	/**
	 * This method is used to filter price rows on the base of unit and date.
	 *
	 * @param date
	 * @param it
	 * @param priceRow
	 */
	private void filterForUnitAndDate(final Unit unit, final Date date, final ListIterator it, final PriceRow priceRow)
	{
		if (unit != null)
		{
			final Set convertible = unit.getConvertibleUnits();
			final Unit user = priceRow.getUnit();
			if ((!(unit.equals(user))) && (!(convertible.contains(user))))
			{
				it.remove();
			}
		}
		else
		{
			final DateRange dateRange = priceRow.getDateRange();
			if ((dateRange != null) && (!(dateRange.encloses(date))))
			{
				it.remove();
			}
		}
	}

	/**
	 * This method is used to get base Price for order entry on cart page.
	 *
	 * @param entry
	 * @return price value.
	 */
	@Override
	public PriceValue getBasePrice(final AbstractOrderEntry entry) throws JaloPriceFactoryException
	{
		final SessionContext ctx = getSession().getSessionContext();
		final AbstractOrder order = entry.getOrder(ctx);
		final Currency currency = order.getCurrency(ctx);
		final Product product = entry.getProduct();

		Product catProductForPrice = product;
		if (product != null && (product instanceof ConfigVariantProduct))
		{
			final ConfigVariantProduct configVariantProduct = (ConfigVariantProduct) product;
			final Product baseProduct = configVariantProduct.getBaseProduct();
			if (baseProduct != null)
			{
				catProductForPrice = baseProduct;
			}
		}
		final EnumerationValue productGroup = getPPG(ctx, catProductForPrice);
		final User user = order.getUser();
		final EnumerationValue userGroup = getUPG(ctx, user);
		final Unit unit = entry.getUnit(ctx);
		final long quantity = entry.getQuantity(ctx).longValue();
		final boolean net = order.isNet().booleanValue();
		final Date date = order.getDate(ctx);

		final boolean isADR = false;
		PriceRow row;
		verifyMandatoryAttributes(catProductForPrice, productGroup, user, userGroup, unit, currency, date);
		row = matchPriceRowForPrice(ctx, catProductForPrice, productGroup, user, userGroup, quantity, unit, currency, date, net,
				isADR);

		if (row != null)
		{
			final Currency rowCurr = row.getCurrency();
			double price;
			if (currency.equals(rowCurr))
			{
				price = row.getPriceAsPrimitive() / row.getUnitFactorAsPrimitive();
			}
			else
			{
				price = rowCurr.convert(currency, row.getPriceAsPrimitive() / row.getUnitFactorAsPrimitive());
			}

			final Unit priceUnit = row.getUnit();
			final Unit entryUnit = entry.getUnit();
			final double convertedPrice = priceUnit.convertExact(entryUnit, price);

			return new PriceValue(currency.getIsoCode(), convertedPrice, row.isNetAsPrimitive());

		}
		return new PriceValue(currency.getIsoCode(), 0.00, false);
	}

	/**
	 * This method is used to get price informations on PDP page.
	 *
	 * @param sessionContext
	 * @param product
	 * @param productGroup
	 * @param user
	 * @param userGroup
	 * @param curr
	 * @param date
	 * @param net
	 * @param taxValues
	 * @return price info.
	 */
	@Override
	protected List getPriceInformations(final SessionContext ctx, final Product product, final EnumerationValue productGroup,
			final User user, final EnumerationValue userGroup, final Currency curr, final boolean net, final Date date,
			final Collection taxValues) throws JaloPriceFactoryException
	{
		Product catProductForPrice = product;
		if (product != null && (product instanceof ConfigVariantProduct))
		{
			final ConfigVariantProduct configVariantProduct = (ConfigVariantProduct) product;
			final Product baseProduct = configVariantProduct.getBaseProduct();
			if (baseProduct != null)
			{
				catProductForPrice = baseProduct;
			}
		}
		final Collection<PriceRow> priceRows = filterPriceRows(
				matchPriceRowsForInfo(ctx, catProductForPrice, productGroup, user, userGroup, curr, date, net));
		final List priceInfos = new ArrayList(priceRows.size());

		Collection theTaxValues = taxValues;

		final List defaultPriceInfos = new ArrayList(priceRows.size());

		for (final PriceRow row : priceRows)
		{
			PriceInformation pInfo = Europe1Tools.createPriceInformation(row, curr);
			if (pInfo.getPriceValue().isNet() != net)
			{
				if (theTaxValues == null)
				{
					theTaxValues = Europe1Tools.getTaxValues(
							getTaxInformations(catProductForPrice, getPTG(ctx, catProductForPrice), user, getUTG(ctx, user), date));
				}

				pInfo = new PriceInformation(pInfo.getQualifiers(), pInfo.getPriceValue().getOtherPrice(theTaxValues));

			}
			priceInfos.add(pInfo);
		}

		if (CollectionUtils.isEmpty(priceInfos))
		{
			return defaultPriceInfos;

		}

		return priceInfos;
	}

	/**
	 * This method is used to fetch List of price rows for PDP page.
	 *
	 * @param sessionContext
	 * @param product
	 * @param productGroup
	 * @param user
	 * @param userGroup
	 * @param currency
	 * @param date
	 * @param net
	 * @return List of PriceRows.
	 */
	@Override
	public List<PriceRow> matchPriceRowsForInfo(final SessionContext ctx, final Product product,
			final EnumerationValue productGroup, final User user, final EnumerationValue userGroup, final Currency currency,
			final Date date, final boolean net) throws JaloPriceFactoryException
	{
		verifyMandatoryAttributes(product, productGroup, user, userGroup, currency, date);
		final Collection rows = queryPriceRows4Price(ctx, product, productGroup, user, userGroup);
		if (!(rows.isEmpty()))
		{
			final List ret = new ArrayList(rows);
			if (ret.size() > 1)
			{
				Collections.sort(ret, new PriceRowInfoComparator(currency));
			}
			return filterPriceRows4Info(ret, currency, date, null);

		}
		return Collections.emptyList();
	}

	protected class PriceRowInfoComparator implements Comparator<PriceRow>
	{
		private final Currency curr;
		private final boolean net;

		protected PriceRowInfoComparator(final Currency paramCurrency)
		{
			this.curr = paramCurrency;
			this.net = false;
		}

		@Override
		public int compare(final PriceRow row1, final PriceRow row2)
		{
			final long u1Match = row1.getUnit().getPK().getLongValue();
			final long u2Match = row2.getUnit().getPK().getLongValue();
			if (u1Match != u2Match)

			{
				return (u1Match < u2Match) ? -1 : 1;
			}

			final long min1 = row1.getMinqtdAsPrimitive();
			final long min2 = row2.getMinqtdAsPrimitive();
			if (min1 != min2)

			{
				return (min1 > min2) ? -1 : 1;
			}

			final int matchValue1 = row1.getMatchValueAsPrimitive();
			final int matchValue2 = row2.getMatchValueAsPrimitive();
			if (matchValue1 != matchValue2)

			{
				return matchValue2 - matchValue1;
			}

			final boolean c1Match = this.curr.equals(row1.getCurrency());
			final boolean c2Match = this.curr.equals(row2.getCurrency());
			if (c1Match != c2Match)

			{
				return c1Match ? -1 : 1;
			}

			final boolean n1Match = this.net == row1.isNetAsPrimitive();
			final boolean n2Match = this.net == row2.isNetAsPrimitive();
			if (n1Match != n2Match)

			{
				return n1Match ? -1 : 1;
			}

			final boolean row1Range = row1.getStartTime() != null;
			final boolean row2Range = row2.getStartTime() != null;

			if (row1Range != row2Range)

			{
				return row1Range ? -1 : 1;
			}
			return row1.getPK().compareTo(row2.getPK());
		}
	}

	/**
	 * This method is used to verify mandatory attributes.
	 *
	 * @param product
	 * @param productGroup
	 * @param user
	 * @param unit
	 * @param userGroup
	 * @param currency
	 * @param date
	 */
	private static void verifyMandatoryAttributes(final Product product, final EnumerationValue productGroup, final User user,
			final EnumerationValue userGroup, final Unit unit, final Currency currency, final Date date)
			throws JaloPriceFactoryException
	{
		verifyMandatoryAttributes(product, productGroup, user, userGroup, currency, date);

		if (unit == null)
		{
			throw new JaloPriceFactoryException("cannot match price without unit", 0);
		}
	}

	/**
	 * This method is used to verify mandatory attributes.
	 *
	 * @param product
	 * @param productGroup
	 * @param user
	 * @param userGroup
	 * @param currency
	 * @param date
	 */
	private static void verifyMandatoryAttributes(final Product product, final EnumerationValue productGroup, final User user,
			final EnumerationValue userGroup, final Currency currency, final Date date) throws JaloPriceFactoryException
	{
		if ((product == null) && (productGroup == null))
		{
			throw new JaloPriceFactoryException(
					"cannot match price info without product and product group - at least one must be present", 0);
		}
		if ((user == null) && (userGroup == null))
		{
			throw new JaloPriceFactoryException("cannot match price info without user and user group - at least one must be present",
					0);
		}
		if (currency == null)
		{
			throw new JaloPriceFactoryException("cannot match price info without currency", 0);
		}
		if (date == null)
		{
			throw new JaloPriceFactoryException("cannot match price info without date", 0);
		}

	}

}
