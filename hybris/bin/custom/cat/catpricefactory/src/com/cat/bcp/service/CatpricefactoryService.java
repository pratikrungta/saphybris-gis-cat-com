/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.service;

/**
 * PriceFactory Service layer.
 */
public interface CatpricefactoryService
{
	/**
	 * This method is used to get Hybris Logo URL
	 *
	 * @param logoCode
	 *           logoCode
	 * @return LOGO URL
	 */
	String getHybrisLogoUrl(String logoCode);

	/**
	 * This method is used to create logo
	 *
	 * @param logoCode
	 *           logoCode
	 */
	void createLogo(String logoCode);
}
