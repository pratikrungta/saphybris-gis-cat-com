/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.setup;

import static com.cat.bcp.constants.CatpricefactoryConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import com.cat.bcp.constants.CatpricefactoryConstants;
import com.cat.bcp.service.CatpricefactoryService;


@SystemSetup(extension = CatpricefactoryConstants.EXTENSIONNAME)
public class CatpricefactorySystemSetup
{
	private final CatpricefactoryService catpricefactoryService;

	public CatpricefactorySystemSetup(final CatpricefactoryService catpricefactoryService)
	{
		this.catpricefactoryService = catpricefactoryService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		catpricefactoryService.createLogo(PLATFORM_LOGO_CODE);
	}
}
