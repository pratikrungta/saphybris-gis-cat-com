/**
 *
 */
package com.cat.bcp.sso;

import de.hybris.platform.core.model.user.UserModel;


/**
 * @author rkolanupaka
 *
 */
public interface CatSSOUserService
{
    /**
     * The method is used to get the user information
     * @param id the user ID
     * @return the user model
     */
    UserModel getSSOUser(String id);
}
