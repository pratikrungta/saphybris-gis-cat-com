/**
 *
 */
package com.cat.bcp.sso.impl;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.internal.service.AbstractService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.cat.bcp.DefaultSSOService;
import com.cat.bcp.sso.CatSSOUserService;


/**
 * @author rkolanupaka
 *
 */
public class CatSSOUserServiceImpl extends AbstractService implements CatSSOUserService
{

	private static final Logger LOGGER = Logger.getLogger(DefaultSSOService.class);

	private ModelService modelService;

	private UserService userService;

	@Override
	public UserModel getSSOUser(final String id)
	{
		if (StringUtils.isEmpty(id))
		{
			throw new IllegalArgumentException("User info must not be empty");
		}
		final UserModel user = lookupExisting(id);
		if (user.isLoginDisabled())
		{
			LOGGER.error("Login is diabled. Prevent user for further navigation for user uid =" + user.getUid());
			return null;
		}
		return user;
	}

	/**
	 * Check if a user exists or not
	 *
	 * @param id
	 *           the user id to search for
	 * @return return user model in case the user is found or null if not found
	 */
	protected UserModel lookupExisting(final String id)
	{
		try
		{
			return userService.getUserForUID(id);
		}
		catch (final UnknownIdentifierException e)
		{
			LOGGER.error("Exception occured while fetching User for id=" + id, e);
			return null;
		}
	}



	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}



	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}



	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}



	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}

}
