/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp;

import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.user.UserManager;
import de.hybris.platform.persistence.security.EJBPasswordEncoderNotFoundException;
import de.hybris.platform.util.Config;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cat.bcp.constants.CatsamlssoConstants;
import com.cat.bcp.sso.CatSSOUserService;
import com.cat.bcp.utils.DefaultSAMLUtil;
import com.cat.bcp.utils.SAMLUtil;


/**
 *
 */
@Controller
@RequestMapping("/saml/**")
public class RedirectionController
{

	private final static Logger LOGGER = Logger.getLogger(RedirectionController.class);

	@Resource(name = "catSSOUserService")
	private CatSSOUserService catSSOUserService;

	private final SAMLUtil samlUil = new DefaultSAMLUtil();
	Logger LOG = Logger.getLogger(RedirectionController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String redirect(final HttpServletResponse response, final HttpServletRequest request)
	{
		LOG.info("redirect : entered");
		final SAMLCredential credential = (SAMLCredential) SecurityContextHolder.getContext().getAuthentication().getCredentials();

		
		final UserModel user;

		String referenceURL = StringUtils.substringAfter(request.getServletPath(), "/saml/");

		if (!StringUtils.isEmpty(request.getQueryString()))
		{
			referenceURL += request.getQueryString().isEmpty() ? "" : "?" + request.getQueryString();
		}
		String id = null;
		try
		{
			id = samlUil.getUserId(credential);
			user = catSSOUserService.getSSOUser(id);

			storeTokenFromSaml(response, user);

			//continue to the redirection and token will be generated only in case the user has valid group
			final String redirectURL = StringUtils.defaultIfEmpty(Config.getParameter(CatsamlssoConstants.SSO_REDIRECT_URL),
					CatsamlssoConstants.DEFAULT_REDIRECT_URL);
			LOG.info("redirect : Leaving with redirecting to : " + CatsamlssoConstants.REDIRECT_PREFIX + redirectURL + referenceURL);
			return CatsamlssoConstants.REDIRECT_PREFIX + redirectURL + referenceURL;

		}
		catch (final IllegalArgumentException e)
		{
			LOGGER.error("Exception occurred for id =" + id, e);
		}

		catch (final Exception ex)
		{
			LOGGER.error("Excepttion occurred", ex);
		}
		LOG.info("redirect : leaving with redirecting to errors");
		return "/error";
	}

	public void storeTokenFromSaml(final HttpServletResponse response, final UserModel user)
	{
		try
		{
			final String cookiePath = StringUtils.defaultIfEmpty(Config.getParameter(CatsamlssoConstants.SSO_COOKIE_PATH),
					CatsamlssoConstants.DEFAULT_COOKIE_PATH);

			final String cookieMaxAgeStr = StringUtils.defaultIfEmpty(Config.getParameter(CatsamlssoConstants.SSO_COOKIE_MAX_AGE),
					String.valueOf(CatsamlssoConstants.DEFAULT_COOKIE_MAX_AGE));

			int cookieMaxAge;

			if (!NumberUtils.isNumber(cookieMaxAgeStr))
			{
				cookieMaxAge = CatsamlssoConstants.DEFAULT_COOKIE_MAX_AGE;
			}
			else
			{
				cookieMaxAge = Integer.valueOf(cookieMaxAgeStr).intValue();
			}
			LOG.info("storeTokenFromSaml : Cookie name =" + Config.getParameter(CatsamlssoConstants.SSO_COOKIE_NAME)
					+ " :: Max age = " + cookieMaxAge);
			UserManager.getInstance().storeLoginTokenCookie(
					//
					StringUtils.defaultIfEmpty(Config.getParameter(CatsamlssoConstants.SSO_COOKIE_NAME),
							CatsamlssoConstants.SSO_DEFAULT_COOKIE_NAME), // cookie name
					user.getUid(), // user id
					"en", // language iso code
					null, // credentials to check later
					cookiePath, // cookie path
					StringUtils.defaultIfEmpty(Config.getParameter(CatsamlssoConstants.SSO_COOKIE_DOMAIN),
							CatsamlssoConstants.SSO_DEFAULT_COOKIE_DOMAIN), // cookie domain
					true, // secure cookie
					cookieMaxAge, // max age in seconds
					response);
		}
		catch (final EJBPasswordEncoderNotFoundException e)
		{
			throw new RuntimeException(e);
		}
	}
}
