/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.setup;

import de.hybris.platform.commerceservices.dataimport.impl.CoreDataImportService;
import de.hybris.platform.commerceservices.setup.AbstractSystemSetup;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetup.Process;
import de.hybris.platform.core.initialization.SystemSetup.Type;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.initialization.SystemSetupParameter;

import java.util.List;

import com.cat.bcp.constants.CatsampledataConstants;
import com.cat.bcp.service.CatsampledataService;


@SystemSetup(extension = CatsampledataConstants.EXTENSIONNAME)
public class CatsampledataSystemSetup extends AbstractSystemSetup
{

	private CoreDataImportService coreDataImportService;

	/**
	 * @return the coreDataImportService
	 */
	public CoreDataImportService getCoreDataImportService()
	{
		return coreDataImportService;
	}

	/**
	 * @param coreDataImportService
	 *           the coreDataImportService to set
	 */
	public void setCoreDataImportService(final CoreDataImportService coreDataImportService)
	{
		this.coreDataImportService = coreDataImportService;
	}

	private final CatsampledataService catsampledataService;

	public static final String CAT = "NA";

	public CatsampledataSystemSetup(final CatsampledataService catsampledataService)
	{
		this.catsampledataService = catsampledataService;
	}

	@SystemSetup(type = Type.PROJECT, process = Process.ALL)
	public void createProjectData(final SystemSetupContext context)
	{
		importSampleUserData(context, CAT);
		//importSampleProductData(context, CAT);
	}


	public void importSampleUserData(final SystemSetupContext context, final String catalogName)
	{
		logInfo(context, "Begin importing User & Warehouse data [" + CAT + "]");
		importImpexFile(context, "/catsampledata/import/" + CAT + "/Customer_Warehouse_Sampledata.impex", true);


	}

	public void importSampleProductData(final SystemSetupContext context, final String catalogName)
	{
		logInfo(context, "Begin importing Product & Category data [" + CAT + "]");
		importImpexFile(context, "/catsampledata/import/" + CAT + "ProductCatalog/products.impex", true);
		getCoreDataImportService().synchronizeProductCatalog(this, context, CAT, true);
	}

	@Override
	public List<SystemSetupParameter> getInitializationOptions()
	{
		// YTODO Auto-generated method stub
		return null;
	}
}
