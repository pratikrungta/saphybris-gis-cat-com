module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    watch: {
        less: {
            files: ['web/webroot/WEB-INF/_ui-src/shared/less/variableMapping.less','web/webroot/WEB-INF/_ui-src/shared/less/generatedVariables.less',
                    'web/webroot/WEB-INF/_ui-src/responsive/lib/ybase-*/less/*', 'web/webroot/WEB-INF/_ui-src/**/themes/**/less/*.less', 'web/webroot/WEB-INF/views/responsive/styles/*.less','web/webroot/WEB-INF/views/responsive/styles/**/*.less'],
            tasks: ['less'],
        },
        fonts: {
            files: ['web/webroot/WEB-INF/_ui-src/**/themes/**/fonts/*'],
            tasks: ['sync:syncfonts'],
        },
        ybasejs: {
            files: ['web/webroot/WEB-INF/_ui-src/responsive/lib/ybase-0.1.0/js/**/*.js'],
            tasks: ['sync:syncybase'],
        },
        jquery: {
            files: ['web/webroot/WEB-INF/_ui-src/responsive/lib/jquery*.js'],
            tasks: ['sync:syncjquery'],
        },
      importjs: {
        files: ['web/webroot/WEB-INF/views/responsive/scripts/**/*.js','web/webroot/WEB-INF/views/responsive/scripts/*.js'],
        tasks: ['import_js:files','uglify','import_js:catCoreMin'],
      }
    },
    less: {
        default: {
            files: [
                {
                    expand: true,
                    cwd: 'web/webroot/WEB-INF/_ui-src/',
                    src: '**/themes/**/less/style.less',
                    dest: 'web/webroot/_ui/',
                    ext: '.css',
                    rename:function(dest,src){
                       var nsrc = src.replace(new RegExp("/themes/(.*)/less"),"/theme-$1/css");
                       return dest+nsrc;
                    }
                }
            ]
        },
      cusom: {
        options: {
          outputStyle: 'compressed',
          sourceMap: false
        },
        files: {
          'web/webroot/_ui/responsive/cat/css/core.css': 'web/webroot/WEB-INF/views/responsive/styles/_styles.less'
        }
      },
    },
    cssmin: {
      options: {
        mergeIntoShorthands: false,
        roundingPrecision: -1
      },
      dist: {
        src: 'web/webroot/_ui/responsive/cat/css/core.css',
        dest: 'web/webroot/_ui/responsive/cat/css/core.min.css'
      }
    },

    uglify: {
        my_target: {
        files: {
          'web/webroot/WEB-INF/views/responsive/scripts/.temp/catScripts.min.js': ['web/webroot/WEB-INF/views/responsive/scripts/.temp/catScripts.js']
        }
      }
    },

    import_js: {
      files: {
        expand: true,
        cwd: 'web/webroot/WEB-INF/views/responsive/scripts/',
        src: ['catScripts.js'],
        dest: 'web/webroot/WEB-INF/views/responsive/scripts/.temp',
        ext: '.js'
      },
      catCoreMin:{
        expand: true,
        cwd: 'web/webroot/WEB-INF/views/responsive/scripts/',
        src: ['core.js'],
        dest: 'web/webroot/_ui/responsive/cat/js/',
        ext: '.min.js'
      }
    },

    
    sync : {
      syncfonts: {
        files: [{
                expand: true,
          cwd: 'web/webroot/WEB-INF/_ui-src/',
          src: '**/themes/**/fonts/*',
          dest: 'web/webroot/_ui/',
          rename:function(dest,src){
                  var nsrc = src.replace(new RegExp("/themes/(.*)"),"/theme-$1");
                  return dest+nsrc;
             }
        }]
      },
      syncybase: {
        files: [{
          cwd: 'web/webroot/WEB-INF/_ui-src/responsive/lib/ybase-0.1.0/js/',
          src: '**/*.js',
          dest: 'web/webroot/_ui/responsive/common/js',
        }]
      },
      syncjquery: {
        files: [{
          cwd: 'web/webroot/WEB-INF/_ui-src/responsive/lib',
          src: 'jquery*.js',
          dest: 'web/webroot/_ui/responsive/common/js',
        }]
      }
    }
    
});
 
  // Plugins
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-sync');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-import-js');

  // Default task(s).
  grunt.registerTask('default', ['less', 'cssmin','sync', 'import_js:files','uglify','import_js:catCoreMin']);

};
