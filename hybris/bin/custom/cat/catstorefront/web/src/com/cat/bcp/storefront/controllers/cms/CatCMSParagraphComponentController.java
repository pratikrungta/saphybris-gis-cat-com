/**
 *
 */
package com.cat.bcp.storefront.controllers.cms;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.session.SessionService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.CatCMSParagraphComponentModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.servicelayer.CatProductService;
import com.cat.bcp.storefront.controllers.ControllerConstants;


/**
 * Controller for CatCMSParagraphComponent is used to set the shipping factor for CUV products in CMSParagraphComponent.
 */
@Controller("CatCMSParagraphComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.CatCMSParagraphComponent)
public class CatCMSParagraphComponentController extends AbstractAcceleratorCMSComponentController<CatCMSParagraphComponentModel>
{
	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "catProductService")
	private CatProductService catProductService;

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final CatCMSParagraphComponentModel component)
	{
		final ProductModel product = sessionService.getAttribute("ProductModel");

		final String productType = catProductService.getProductType(product);

		if (CatCoreConstants.UTV.equals(productType))
		{
			final String shippingFactor = getShippingFactor(product);
			model.addAttribute("shippingFactor", shippingFactor);
		}
	}


	/**
	 * This method gets the shipping factor for CUV product.
	 *
	 * @param product
	 *           the product
	 * @return the shipping factor
	 */
	private String getShippingFactor(final ProductModel product)
	{
		if (product instanceof ConfigVariantProductModel)
		{
			final ConfigVariantProductModel configProduct = (ConfigVariantProductModel) product;
			final BaseVariantProductModel baseProduct = (BaseVariantProductModel) configProduct.getBaseProduct();

			return baseProduct.getShippingFactor();
		}
		return StringUtils.EMPTY;
	}

}


