/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.storefront.controllers.cms;

import de.hybris.platform.acceleratorcms.model.components.MiniCartComponentModel;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.product.data.ProductData;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.servicelayer.CatProductService;
import com.cat.bcp.facades.customer.CatCustomerFacade;
import com.cat.bcp.facades.order.CatCartFacade;
import com.cat.bcp.storefront.controllers.ControllerConstants;


/**
 * Controller for CMS MiniCartComponent
 */
@Controller("MiniCartComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.MiniCartComponent)
public class MiniCartComponentController extends AbstractAcceleratorCMSComponentController<MiniCartComponentModel>
{
	private static final Logger LOG = Logger.getLogger(MiniCartComponentController.class);

	private static final String QUANTITY = "quantity";
	private static final String PRODUCT_CODE = "productCode";
	private static final String UTV_REDIRECT = "utvRedirect";
	private static final String TRUCKLOAD_REDIRECT = "truckloadRedirect";

	public static final String TOTAL_PRICE = "totalPrice";
	public static final String TOTAL_ITEMS = "totalItems";
	public static final String TOTAL_DISPLAY = "totalDisplay";
	public static final String TOTAL_NO_DELIVERY = "totalNoDelivery";
	public static final String SUB_TOTAL = "subTotal";
	public static final String USER_TYPE = "userType";

	@Resource(name = "cartFacade")
	private CatCartFacade cartFacade;

	@Resource(name = "catCustomerFacade")
	private CatCustomerFacade catCustomerFacade;

	@Resource(name = "catProductService")
	private CatProductService catProductService;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final MiniCartComponentModel component)
	{
		final CartData cartData = cartFacade.getMiniCart();
		LOG.debug("In Minicart Component Controller");
		model.addAttribute(SUB_TOTAL, cartData.getSubTotal());
		if (cartData.getDeliveryCost() != null)
		{
			final PriceData withoutDelivery = cartData.getDeliveryCost();
			withoutDelivery.setValue(cartData.getTotalPrice().getValue().subtract(cartData.getDeliveryCost().getValue()));
			model.addAttribute(TOTAL_NO_DELIVERY, withoutDelivery);
		}
		else
		{
			model.addAttribute(TOTAL_NO_DELIVERY, cartData.getTotalPrice());
		}

		final String userType = catCustomerFacade.getUserType();
		model.addAttribute(USER_TYPE, userType);
		model.addAttribute(TOTAL_PRICE, cartData.getTotalPrice());
		model.addAttribute(TOTAL_DISPLAY, component.getTotalDisplay());
		model.addAttribute(TOTAL_ITEMS, cartData.getTotalUnitCount());
		if (CollectionUtils.isNotEmpty(cartData.getEntries()) && cartData.getEntries().get(0).getProduct() != null)
		{
			final ProductData product = cartData.getEntries().get(0).getProduct();
			populateReviewOrderRedirectFlag(cartData, product, model);
			model.addAttribute(PRODUCT_CODE, cartData.getEntries().get(0).getProduct().getCode());
			model.addAttribute(QUANTITY, cartData.getTotalUnitCount());
		}
	}

	/**
	 * This method populates the review order redirect flag for utv products. It will either redirect to review truckload
	 * page or utv review order page.
	 *
	 * @param cartData
	 *           the cart data
	 * @param product
	 *           the product
	 * @param model
	 *           the model
	 */
	private void populateReviewOrderRedirectFlag(final CartData cartData, final ProductData product, final Model model)
	{
		if (catProductService.isUtvProduct(product))
		{
			LOG.debug("UTV Products Exist adding redirect attribute for review order page ::: ");
			for (final OrderEntryData cartEntry : cartData.getEntries())
			{
				if (StringUtils.contains(cartEntry.getTruckloadId(), CatCoreConstants.MIX_AND_MATCH_TRUCK))
				{
					model.addAttribute(TRUCKLOAD_REDIRECT, Boolean.TRUE);
					break;
				}
				else
				{
					model.addAttribute(UTV_REDIRECT, Boolean.TRUE);
				}
			}
		}
		
	}
}
