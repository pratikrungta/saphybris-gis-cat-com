/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.storefront.controllers.cms;

import de.hybris.platform.acceleratorcms.enums.NavigationBarMenuLayout;
import de.hybris.platform.acceleratorcms.model.components.NavigationBarComponentModel;
import de.hybris.platform.catalog.CatalogService;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.facades.customer.CatCustomerFacade;
import com.cat.bcp.storefront.controllers.ControllerConstants;


/**
 *
 */
@Controller("NavigationBarComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.NavigationBarComponent)
public class NavigationBarComponentController extends AbstractAcceleratorCMSComponentController<NavigationBarComponentModel>
{
	public static final String CATEGORIES = "categories";

	public static final String USER_TYPE = "userType";

	@Resource(name = "categoryService")
	private CategoryService categoryService;

	@Resource(name = "catalogService")
	private CatalogService catalogService;

	@Resource(name = "catCategoryUrlConverter")
	private Converter<CategoryModel, CategoryData> catCategoryUrlConverter;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "catCustomerFacade")
	private CatCustomerFacade catCustomerFacade;

	@Override
	protected void fillModel(final HttpServletRequest request, final Model model, final NavigationBarComponentModel component)
	{
		final Map<String, Collection<CategoryData>> categoryMap = new HashMap<>();
		if (component.getDropDownLayout() != null)
		{
			model.addAttribute("dropDownLayout", component.getDropDownLayout().getCode().toLowerCase());
		}
		else if (component.getNavigationNode() != null && component.getNavigationNode().getChildren() != null
				&& !component.getNavigationNode().getChildren().isEmpty())
		{
			// Component has children but not drop down layout, default to auto
			model.addAttribute("dropDownLayout", NavigationBarMenuLayout.AUTO.getCode().toLowerCase());
		}
		final CategoryModel newCategory = categoryService.getCategoryForCode(StringUtils.lowerCase(CatCoreConstants.NEW));
		final CategoryModel usedCategory = categoryService.getCategoryForCode(StringUtils.lowerCase(CatCoreConstants.USED));
		categoryMap.put("New", getCategoryDataForProducts(newCategory));
		categoryMap.put("Used", getCategoryDataForProducts(usedCategory));
		model.addAttribute(CatCoreConstants.ATTACHMENTS_LABEL, CatCoreConstants.ACCESSORY_PAGE);
		model.addAttribute(CATEGORIES, categoryMap);
		final String userType = catCustomerFacade.getUserType();
		model.addAttribute(USER_TYPE, userType);
	}

	/**
	 * Method to get sub categories for a root category
	 *
	 * @param category
	 *           the category model for which the sub categories needs to be retrieved
	 * @return Collection<CategoryData>
	 */
	public Collection<CategoryData> getCategoryDataForProducts(final CategoryModel category)
	{
		final Collection<CategoryModel> categoryModelCollection = category.getAllSubcategories();
		final List<CategoryModel> categoryList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(categoryModelCollection))
		{
			for (final CategoryModel categoryVal : categoryModelCollection)
			{
				populateFinalCategoryList(categoryList, categoryVal);
			}
		}
		return catCategoryUrlConverter.convertAll(categoryList);
	}


	/**
	 * Method to filter out categories based on user groups
	 *
	 * @param categoryList
	 *           category list to be populated
	 * @param categoryVal
	 *           the category model for which the user group needs to be compared.
	 */
	private void populateFinalCategoryList(final List<CategoryModel> categoryList, final CategoryModel categoryVal)
	{
		for (final PrincipalModel userGroup : categoryVal.getAllowedPrincipals())
		{
			if (userService.getAllUserGroupsForUser(userService.getCurrentUser()).contains(userGroup))
			{
				categoryList.add(categoryVal);
			}
		}
	}

}
