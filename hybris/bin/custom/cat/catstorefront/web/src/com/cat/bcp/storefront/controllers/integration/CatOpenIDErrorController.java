/**
 *
 */
package com.cat.bcp.storefront.controllers.integration;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cat.bcp.storefront.controllers.ControllerConstants;


/**
 * @author prrungta
 *
 */
@Controller
@RequestMapping("/openidError")
public class CatOpenIDErrorController extends AbstractPageController
{
	private static final String ERROR_CMS_PAGE = "notFound";

	@RequestMapping(method = RequestMethod.GET)
	public String redirect(final Model model, final HttpServletRequest request) throws CMSItemNotFoundException
	{

		storeCmsPageInModel(model, getContentPageForLabelOrId(ERROR_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ERROR_CMS_PAGE));
		return ControllerConstants.Views.Pages.Error.OpenidErrorPage;
	}



}
