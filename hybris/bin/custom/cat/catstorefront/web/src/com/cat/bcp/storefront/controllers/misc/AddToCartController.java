/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.storefront.controllers.misc;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorfacades.product.data.ProductWrapperData;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.AbstractController;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddToCartOrderForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddToEntryGroupForm;
import de.hybris.platform.commercefacades.order.converters.populator.GroupCartModificationListPopulator;
import de.hybris.platform.commercefacades.order.data.AddToCartParams;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.util.Config;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.facades.customer.CatCustomerFacade;
import com.cat.bcp.facades.order.CatCartFacade;
import com.cat.bcp.storefront.controllers.ControllerConstants;
import com.cat.bcp.storefront.forms.CatAddToCartForm;


/**
 * Controller for Add to Cart functionality which is not specific to a certain page.
 */
@Controller
public class AddToCartController extends AbstractController
{
	private static final String BASKET_ERROR_OCCURRED = "basket.error.occurred";
	private static final String QUANTITY_ATTR = "quantity";
	private static final String TYPE_MISMATCH_ERROR_CODE = "typeMismatch";
	private static final String ERROR_MSG_TYPE = "errorMsg";
	private static final String QUANTITY_INVALID_BINDING_MESSAGE_KEY = "basket.error.quantity.invalid.binding";
	private static final String SHOWN_PRODUCT_COUNT = "catstorefront.storefront.minicart.shownProductCount";
	private static final String SUCCESS = "success";

	private static final Logger LOG = Logger.getLogger(AddToCartController.class);

	@Resource(name = "cartFacade")
	private CatCartFacade cartFacade;

	@Resource(name = "productVariantFacade")
	private ProductFacade productFacade;

	@Resource(name = "catCustomerFacade")
	private CatCustomerFacade catCustomerFacade;

	@Resource(name = "groupCartModificationListPopulator")
	private GroupCartModificationListPopulator groupCartModificationListPopulator;

	private static final String CHECKOUT_REVIEW_PAGE = "/checkout/reviewUTV/";

	private static final String PRODUCT_CODE_PATH_VARIABLE_PATTERN = "{productCode:.*}";

	@RequestMapping(value = "/cart/add", method = RequestMethod.GET)
	public String addToCart(@RequestParam("productCodePost") final String code, @RequestParam("quantities") final int[] quantities,
			@RequestParam("shipToDates") final String[] shipToDates, final Model model) throws ParseException
	{
		final Map<String, Object> cartType = cartFacade.getCartType(code);
		model.addAttribute(CatCoreConstants.CART_TYPE, cartType.get(CatCoreConstants.CART_PRODUCT_TYPE));
		model.addAttribute(CatCoreConstants.IS_UTV_CART, cartType.get(CatCoreConstants.IS_UTV_CART));

		int totalQty = 0;
		//clear any items from cart
		cartFacade.clearCart();
		for (int i = 0; i < quantities.length; i++)
		{
			final String dt = shipToDates[i];
			final DateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.US);
			final Date date = formatter.parse(dt);

			int cartQty;
			if (quantities[i] < 1)
			{
				model.addAttribute(ERROR_MSG_TYPE, "basket.error.quantity.invalid");
				model.addAttribute(QUANTITY_ATTR, Long.valueOf(0L));
			}
			else
			{
				try
				{
					cartQty = quantities[i];
					totalQty = totalQty + cartQty;

					final String b2bUnit = catCustomerFacade.getCurrentCustomer().getUnit().getUid();
					final String uniqueTruckloadId = b2bUnit + System.currentTimeMillis();

					final CartModificationData cartModification = cartFacade.addToCart(code, cartQty, date, uniqueTruckloadId,
							StringUtils.EMPTY);
					model.addAttribute(QUANTITY_ATTR, Long.valueOf(cartModification.getQuantityAdded()));
					model.addAttribute("entry", cartModification.getEntry());
					model.addAttribute("cartCode", cartModification.getCartCode());
					model.addAttribute("isQuote", cartFacade.getSessionCart().getQuoteData() != null ? Boolean.TRUE : Boolean.FALSE);

					checkCartModificationQuantity(model, cartQty, cartModification);
				}
				catch (final CommerceCartModificationException ex)
				{
					logDebugException(ex);
					model.addAttribute(ERROR_MSG_TYPE, BASKET_ERROR_OCCURRED);
					model.addAttribute(QUANTITY_ATTR, Long.valueOf(0L));
				}
			}
		}
		model.addAttribute("product", productFacade.getProductForCodeAndOptions(code, Arrays.asList(ProductOption.BASIC)));

		return "redirect:" + CHECKOUT_REVIEW_PAGE + code + "?productCode=" + code + "&orderQuantity=" + totalQty;
	}

	/**
	 * @param model
	 * @param cartQty
	 * @param cartModification
	 */
	private void checkCartModificationQuantity(final Model model, final long cartQty, final CartModificationData cartModification)
	{
		if (cartModification.getQuantityAdded() == 0L)
		{
			model.addAttribute(ERROR_MSG_TYPE, "basket.information.quantity.noItemsAdded." + cartModification.getStatusCode());
		}
		else if (cartModification.getQuantityAdded() < cartQty)
		{
			model.addAttribute(ERROR_MSG_TYPE,
					"basket.information.quantity.reducedNumberOfItemsAdded." + cartModification.getStatusCode());
		}
	}

	protected String getViewWithBindingErrorMessages(final Model model, final BindingResult bindingErrors)
	{
		for (final ObjectError error : bindingErrors.getAllErrors())
		{
			if (isTypeMismatchError(error))
			{
				model.addAttribute(ERROR_MSG_TYPE, QUANTITY_INVALID_BINDING_MESSAGE_KEY);
			}
			else
			{
				model.addAttribute(ERROR_MSG_TYPE, error.getDefaultMessage());
			}
		}
		return ControllerConstants.Views.Fragments.Cart.AddToCartPopup;
	}

	protected boolean isTypeMismatchError(final ObjectError error)
	{
		return error.getCode().equals(TYPE_MISMATCH_ERROR_CODE);
	}

	@RequestMapping(value = "/cart/addGrid", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public final String addGridToCart(@RequestBody final AddToCartOrderForm form, final Model model)
	{
		final Set<String> multidErrorMsgs = new HashSet<>();
		final List<CartModificationData> modificationDataList = new ArrayList<>();

		for (final OrderEntryData cartEntry : form.getCartEntries())
		{
			if (!isValidProductEntry(cartEntry))
			{
				LOG.error("Error processing entry");
			}
			else if (!isValidQuantity(cartEntry))
			{
				multidErrorMsgs.add("basket.error.quantity.invalid");
			}
			else
			{
				final String errorMsg = addEntryToCart(modificationDataList, cartEntry, true);
				if (StringUtils.isNotEmpty(errorMsg))
				{
					multidErrorMsgs.add(errorMsg);
				}

			}
		}

		if (CollectionUtils.isNotEmpty(modificationDataList))
		{
			groupCartModificationListPopulator.populate(null, modificationDataList);

			model.addAttribute("modifications", modificationDataList);
		}

		if (CollectionUtils.isNotEmpty(multidErrorMsgs))
		{
			model.addAttribute("multidErrorMsgs", multidErrorMsgs);
		}

		model.addAttribute("numberShowing", Integer.valueOf(Config.getInt(SHOWN_PRODUCT_COUNT, 3)));


		return ControllerConstants.Views.Fragments.Cart.AddToCartPopup;
	}

	@RequestMapping(value = "/cart/addQuickOrder", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public final String addQuickOrderToCart(@RequestBody final AddToCartOrderForm form, final Model model)
	{
		final List<CartModificationData> modificationDataList = new ArrayList();
		final List<ProductWrapperData> productWrapperDataList = new ArrayList();
		final int maxQuickOrderEntries = Config.getInt("catstorefront.quick.order.rows.max", 25);
		final int sizeOfCartEntries = CollectionUtils.size(form.getCartEntries());
		form.getCartEntries().stream().limit(Math.min(sizeOfCartEntries, maxQuickOrderEntries)).forEach(cartEntry -> {
			String errorMsg = StringUtils.EMPTY;
			final String sku = !isValidProductEntry(cartEntry) ? StringUtils.EMPTY : cartEntry.getProduct().getCode();
			if (StringUtils.isEmpty(sku))
			{
				errorMsg = "text.quickOrder.product.code.invalid";
			}
			else if (!isValidQuantity(cartEntry))
			{
				errorMsg = "text.quickOrder.product.quantity.invalid";
			}
			else
			{
				errorMsg = addEntryToCart(modificationDataList, cartEntry, false);
			}

			if (StringUtils.isNotEmpty(errorMsg))
			{
				productWrapperDataList.add(createProductWrapperData(sku, errorMsg));
			}
		});

		if (CollectionUtils.isNotEmpty(productWrapperDataList))
		{
			model.addAttribute("quickOrderErrorData", productWrapperDataList);
			model.addAttribute("quickOrderErrorMsg", "basket.quick.order.error");
		}

		if (CollectionUtils.isNotEmpty(modificationDataList))
		{
			model.addAttribute("modifications", modificationDataList);
		}

		return ControllerConstants.Views.Fragments.Cart.AddToCartPopup;
	}

	@RequestMapping(value = "/entrygroups/cart/addToEntryGroup", method =
	{ RequestMethod.POST, RequestMethod.GET })
	public String addEntryGroupToCart(final Model model, @Valid final AddToEntryGroupForm form, final BindingResult bindingErrors)
	{
		if (bindingErrors.hasErrors())
		{
			return getViewWithBindingErrorMessages(model, bindingErrors);
		}
		final long quantity = 1L;
		try
		{
			final AddToCartParams addToCartParams = new AddToCartParams();
			addToCartParams.setEntryGroupNumbers(new HashSet(Collections.singletonList(form.getEntryGroupNumber())));
			addToCartParams.setProductCode(form.getProductCode());
			addToCartParams.setQuantity(quantity);
			addToCartParams.setStoreId(null);
			final CartModificationData cartModification = cartFacade.addToCart(addToCartParams);
			model.addAttribute(QUANTITY_ATTR, Long.valueOf(cartModification.getQuantityAdded()));
			model.addAttribute("entry", cartModification.getEntry());
			model.addAttribute("cartCode", cartModification.getCartCode());

			checkCartModificationQuantity(model, quantity, cartModification);
		}
		catch (final CommerceCartModificationException ex)
		{
			logDebugException(ex);
			model.addAttribute(ERROR_MSG_TYPE, BASKET_ERROR_OCCURRED);
			model.addAttribute(QUANTITY_ATTR, Long.valueOf(0L));
		}
		model.addAttribute("product",
				productFacade.getProductForCodeAndOptions(form.getProductCode(), Arrays.asList(ProductOption.BASIC)));

		return REDIRECT_PREFIX + "/cart";
	}

	/**
	 * Adds the new product to cart for SP users.
	 *
	 * @param code
	 *           the product code
	 * @param model
	 *           the model
	 * @param form
	 *           the add to cart form
	 * @return Redirects to cart page
	 */
	@ResponseBody
	@RequestMapping(value = "/cart/addNewProduct", method = RequestMethod.POST)
	public Map addNewProductToCart(@RequestBody final CatAddToCartForm form, final Model model, final HttpSession session)
	{
		final Map resultMap = new HashMap();

		final Map<String, Object> cartType = cartFacade.getCartType(form.getProductCodePost());
		model.addAttribute(CatCoreConstants.CART_TYPE, cartType.get(CatCoreConstants.CART_PRODUCT_TYPE));
		model.addAttribute(CatCoreConstants.IS_UTV_CART, cartType.get(CatCoreConstants.IS_UTV_CART));

		final boolean isIM = (null != session.getAttribute("isIM")) ? (boolean) session.getAttribute("isIM") : false;

		final boolean isUTVUser = (null != session.getAttribute("isUTVUser")) ? (boolean) session.getAttribute("isUTVUser") : false;

		if (!form.isConfigurable() && StringUtils.isNotEmpty(form.getLaneType()))
		{
			cartFacade.clearCart();
		}
		if (isIM || isUTVUser)
		{
			cartFacade.setCartLaneTypeOnCart(
					StringUtils.isNotEmpty(form.getLaneType()) ? form.getLaneType().toUpperCase() : form.getLaneType());
		}

		String redirectUrl = null;

		try
		{
			final long qty = form.getQty();
			LOG.info("inside add to cart method : qty:" + qty);

			if (!form.isConfigurable())
			{
				cartFacade.addToCart(form.getProductCodePost(), qty);
			}
			else
			{
				if (StringUtils.isEmpty(form.getLaneType()) && cartFacade.getSessionCart().getLaneType() != null)
				{
					form.setLaneType(cartFacade.getSessionCart().getLaneType().toString());
				}
				if (cartFacade.getSessionCart().getLaneType() != null
						&& !cartFacade.getSessionCart().getLaneType().toString().equalsIgnoreCase(form.getLaneType()))
				{
					cartFacade.clearCart();
				}
				cartFacade.addToCart(form.getProductCodePost(), qty, true, form.getLaneType().toUpperCase());
				cartFacade.updateCartForConfigurableProducts(form.getProductCodePost(), qty, null);
			}
		}
		catch (final CommerceCartModificationException ex)
		{
			logDebugException(ex);
			populateResultMap(resultMap, "/error", Boolean.FALSE);
			return resultMap;
		}
		catch (final Exception ex)
		{
			// To be handled through Error page controller
			LOG.info("Exception caused :-", ex);
			populateResultMap(resultMap, "/error", Boolean.FALSE);
			return resultMap;
		}

		if (isIM || isUTVUser)
		{
			redirectUrl = "/cart/reOrderCart";
			populateResultMap(resultMap, redirectUrl, Boolean.TRUE);
		}
		else
		{
			redirectUrl = "/cart/catCartPage?productCode=" + form.getProductCodePost();
			populateResultMap(resultMap, redirectUrl, Boolean.TRUE);
		}
		return resultMap;
	}

	/**
	 * @param resultMap
	 * @param redirectUrl
	 * @param success
	 */
	private void populateResultMap(final Map resultMap, final String redirectUrl, final Boolean success)
	{
		resultMap.put(SUCCESS, success);
		resultMap.put("redirectUrl", redirectUrl);
	}

	protected ProductWrapperData createProductWrapperData(final String sku, final String errorMsg)
	{
		final ProductWrapperData productWrapperData = new ProductWrapperData();
		final ProductData productData = new ProductData();
		productData.setCode(sku);
		productWrapperData.setProductData(productData);
		productWrapperData.setErrorMsg(errorMsg);
		return productWrapperData;
	}

	protected void logDebugException(final Exception ex)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug(ex);
		}
	}

	protected String addEntryToCart(final List<CartModificationData> modificationDataList, final OrderEntryData cartEntry,
			final boolean isReducedQtyError)
	{
		String errorMsg = StringUtils.EMPTY;
		try
		{
			final long qty = cartEntry.getQuantity().longValue();
			final CartModificationData cartModificationData = cartFacade.addToCart(cartEntry.getProduct().getCode(), qty);
			if (cartModificationData.getQuantityAdded() == 0L)
			{
				errorMsg = "basket.information.quantity.noItemsAdded." + cartModificationData.getStatusCode();
			}
			else if (cartModificationData.getQuantityAdded() < qty && isReducedQtyError)
			{
				errorMsg = "basket.information.quantity.reducedNumberOfItemsAdded." + cartModificationData.getStatusCode();
			}

			modificationDataList.add(cartModificationData);

		}
		catch (final CommerceCartModificationException ex)
		{
			errorMsg = BASKET_ERROR_OCCURRED;
			logDebugException(ex);
		}
		return errorMsg;
	}

	protected boolean isValidProductEntry(final OrderEntryData cartEntry)
	{
		return cartEntry.getProduct() != null && StringUtils.isNotBlank(cartEntry.getProduct().getCode());
	}

	protected boolean isValidQuantity(final OrderEntryData cartEntry)
	{
		return cartEntry.getQuantity() != null && cartEntry.getQuantity().longValue() >= 1L;
	}

	@ResponseBody
	@SuppressWarnings("boxing")
	@RequestMapping(value = "/cart/addReorderProduct/" + PRODUCT_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public Map<String, Object> reviewOrder(@PathVariable("productCode") final String productCode,
			@RequestParam("reorderProductQuantity") final int reorderProductQuantity, final HttpSession session,
			@RequestParam(value = "laneType", required = false) final String laneType)
	{
		validateParameterNotNull(productCode, "ProductCode Parameter must not be null");
		validateParameterNotNull(reorderProductQuantity, "Reorder ProductQuantity Parameter id must not be null");
		final Map<String, Object> cartType = cartFacade.getCartType(productCode);


		final Map<String, Object> resultMap = new HashMap();
		resultMap.put(CatCoreConstants.CART_TYPE, cartType.get(CatCoreConstants.CART_PRODUCT_TYPE));
		resultMap.put(CatCoreConstants.IS_UTV_CART, cartType.get(CatCoreConstants.IS_UTV_CART));
		try
		{
			//isIM check is for both IM and UTV users.
			if (StringUtils.isNotEmpty(laneType))
			{
				cartFacade.clearCart();
			}
			final boolean isIM = (null != session.getAttribute("isIM")) ? (boolean) session.getAttribute("isIM") : false;
			if (isIM)
			{
				cartFacade.setCartLaneTypeOnCart(StringUtils.isNotEmpty(laneType) ? laneType.toUpperCase() : laneType);
				LOG.info("Inside Reorder Add to Cart method : reorderProductQuantity: " + reorderProductQuantity);
				cartFacade.addToCart(productCode, reorderProductQuantity);
			}
		}
		catch (final CommerceCartModificationException ex)
		{
			logDebugException(ex);
			resultMap.put(SUCCESS, false);
		}
		catch (final Exception ex)
		{
			// To be handled through Error page controller
			LOG.info("Exception caused :-", ex);
			resultMap.put(SUCCESS, false);
		}

		resultMap.put(SUCCESS, true);
		resultMap.put(CatCoreConstants.CART_LANE_TYPE,
				null != cartFacade.getSessionCart() ? cartFacade.getSessionCart().getLaneType() : StringUtils.EMPTY);
		return resultMap;
	}
}