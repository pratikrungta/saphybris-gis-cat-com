/**
 *
 */
package com.cat.bcp.storefront.controllers.misc;



import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.core.GenericSearchConstants.LOG;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;



/**
 * @author ridjain
 *
 *         This controller will be called in case of any exception and based on errorCode it will redirect to
 *         ErrorPageController to fetch the errorPage.
 *
 */
@ControllerAdvice
public class CatExceptionController extends AbstractPageController
{
	protected static final Logger LOG = Logger.getLogger(CatExceptionController.class);

	private static final String REDIRECT_URL = REDIRECT_PREFIX + "/error";

	/**
	 * Intercepts Exception and redirects to error method to show Error Page.
	 *
	 * @param exception
	 * @param request
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	@ExceptionHandler(Exception.class)
	public String handleAllException(final Exception exception, final HttpServletRequest request) throws CMSItemNotFoundException
	{
		LOG.error(exception.getMessage(), exception);
		request.setAttribute("message", exception.getMessage());
		return REDIRECT_URL + "?errorCode=400"; // Redirect to Cat Error Page with errorCode.

	}

}




