
package com.cat.bcp.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.util.localization.Localization;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.exception.CatEDHRecordNotFoundException;
import com.cat.bcp.core.exception.CatException;
import com.cat.bcp.facades.integration.CatIntegrationFacade;
import com.cat.facades.edh.CatEDHInventoryDetailsData;




/**
 * Controller for EDH Implementation
 *
 * @author ravjonnalagadda
 */
@Controller
@RequestMapping(value = "/edh")
public class CatEDHPageController extends AbstractPageController
{



	private static final Logger LOG = Logger.getLogger(CatEDHPageController.class);

	@Resource(name = "catIntegrationFacade")
	CatIntegrationFacade catIntegrationFacade;

	/**
	 *
	 * This method will return EDH Inventory Details
	 *
	 * @param inventoryDetailsData
	 *           : InventoryDetails Data has DealerCode and Config product.
	 * @return: this method will return inventory details in the form of JSON object.
	 *
	 */
	@RequestMapping(value = "/details", method = RequestMethod.POST)
	@ResponseBody
	public String getInventoryDetails(@RequestBody final CatEDHInventoryDetailsData inventoryDetailsData)
	{
		String response;
		try
		{
			response = catIntegrationFacade.getInventoryDetails(inventoryDetailsData);
			LOG.debug("Base model details:" + inventoryDetailsData.getBaseSalesModelList());
		}
		catch (final CatException e)
		{
			response = processException(e);

		}
		return response;

	}

	/**
	 *
	 * @param partyId
	 *           : dealerId or PDC
	 * @param configId
	 *           : Config variant product
	 * @param getDescendantNodes
	 *           : boolean for children dealer address.
	 *
	 * @return: will return EDH inventory details by PartId.
	 *
	 */
	@RequestMapping(value = "/inventorycount", method = RequestMethod.GET)
	@ResponseBody
	public String getInventoryCountByPartyId(@RequestParam(value = "partyId") final String partyId,
			@RequestParam(value = "configId") final String configId,
			@RequestParam(value = "getDescendantNodes") final Optional<String> getDescendantNodes)
	{
		LOG.debug("::::in CatEDHPageController and Processing getInventoryCountByPartyId");
		String response;
		try
		{
			if (getDescendantNodes.isPresent())
			{
				response = catIntegrationFacade.getInventoryCountByPartyId(partyId, configId,
						StringUtils.isBlank(getDescendantNodes.get()) ? "false" : getDescendantNodes.get());
			}
			else
			{
				response = catIntegrationFacade.getInventoryCountByPartyId(partyId, configId, null);
			}

		}
		catch (final CatException e)
		{
			response = processException(e);

		}
		LOG.debug("::::in CatEDHPageController and Processed getInventoryCountByPartyId");
		return response;
	}


	/**
	 * @param e
	 *           : generic cat exception
	 * @return : exception message
	 */
	private String processException(final CatException e)
	{
		final Map<String, String> map = new HashMap<>();
		if (e instanceof CatEDHRecordNotFoundException)
		{
			LOG.error(e.getMessage());
			map.put("errmsg", Localization.getLocalizedString(CatCoreConstants.CAT_EDH_NORESULTS_MSG));
		}
		else
		{
			LOG.error(e.getMessage());
			map.put("errmsg", Localization.getLocalizedString(CatCoreConstants.CAT_EDH_UNEXPECTED_MSG));
		}
		return map.toString();
	}
}
