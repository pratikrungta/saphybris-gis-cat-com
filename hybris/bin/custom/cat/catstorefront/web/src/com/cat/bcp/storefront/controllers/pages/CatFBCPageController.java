
package com.cat.bcp.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.ProductFacade;

import java.util.Optional;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cat.bcp.facades.integration.CatFBCFacade;
import com.cat.bcp.facades.order.CatCartFacade;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * Controller for FBC StoreFront Implementation
 *
 * @author ravjonnalagadda
 */
@Controller
@RequestMapping(value = "/fbc")
public class CatFBCPageController extends AbstractPageController
{
	private static final String CAT_FBC_CMS_PAGE = "catFBCCMSPage";
	private static final Logger LOG = Logger.getLogger(CatFBCPageController.class);

	@Resource(name = "catFBCFacade")
	CatFBCFacade catFBCFacade;

	@Resource(name = "productVariantFacade")
	private ProductFacade productFacade;

	@Resource(name = "catCartFacade")
	CatCartFacade catCartFacade;

	/**
	 * @param model
	 * @return model
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getFbcData(final Model model, @RequestParam(value = "Lane") final Optional<String> laneCode)
			throws CMSItemNotFoundException
	{

		if (laneCode.isPresent() && catCartFacade.getSessionCart().getLaneType() != null
				&& !laneCode.get().equalsIgnoreCase(catCartFacade.getSessionCart().getLaneType().toString()))
		{
			catCartFacade.clearCart();
		}
		LOG.debug("::::in CatFBCPageController and Processing getFbcData");
		final ContentPageModel catFbcCMSPage = getContentPageForLabelOrId(CAT_FBC_CMS_PAGE);
		storeCmsPageInModel(model, catFbcCMSPage);
		setUpMetaDataForContentPage(model, catFbcCMSPage);
		model.addAttribute("page", "CAT FBC Page");
		LOG.debug("::::in CatFBCPageController and Processed getFbcData");
		return getViewForPage(model);
	}

	public static String getJSONResponse(final Object response)
	{
		final ObjectMapper responseMapper = new ObjectMapper();
		String resjsonInString = null;
		try
		{
			resjsonInString = responseMapper.writeValueAsString(response);
		}
		catch (final JsonProcessingException e)
		{
			LOG.error(e);
		}
		return resjsonInString;
	}


	/**
	 * @param dca
	 * @param laneCode
	 * @return : Initiate FBC request and creates FBC session.
	 * @throws Exception
	 */
	@RequestMapping(value = "/initiateFBCRequest", method = RequestMethod.GET)
	@ResponseBody
	public String initiateFBCAPIRequest(@RequestParam(value = "dca") final String dca,
			@RequestParam(value = "laneCode") final Optional<String> laneCode) throws Exception
	{
		String response = null;
		if (laneCode.isPresent())
		{
			response = catFBCFacade.initiateFBCAPIRequest(dca,
					StringUtils.isBlank(laneCode.get()) ? StringUtils.EMPTY : laneCode.get());
		}
		return response;
	}

	/**
	 * @param featureCode
	 * @param productCode
	 * @return : Returns selections for the featurecode
	 */
	@RequestMapping(value = "/selectAPIResponse", method = RequestMethod.GET)
	@ResponseBody
	public String getFBCSelectAPIResponse(@RequestParam(value = "featureCode") final String featureCode,
			@RequestParam(value = "productCode") final String productCode)
	{
		final String configId = "";
		return catFBCFacade.getFBCSelectAPIResponse(configId, featureCode, productCode);
	}

}
