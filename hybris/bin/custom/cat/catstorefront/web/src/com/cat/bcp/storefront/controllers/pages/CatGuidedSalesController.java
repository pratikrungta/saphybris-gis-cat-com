package com.cat.bcp.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.product.data.ProductData;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cat.bcp.facades.search.DealerSearchFacade;


/**
 * Controller for Guided Sales Implementation
 *
 * @author Deloitte Digital
 */
@Controller
@RequestMapping(value = "/guidedsales")
public class CatGuidedSalesController extends SearchPageController
{
	private static final Logger LOG = Logger.getLogger(CatGuidedSalesController.class);

	private static final String GUIDED_SALES_CMS_PAGE = "catGuidedSales";

	@Resource(name = "dealerSearchFacade")
	private DealerSearchFacade dealerSearchFacade;

	/**
	 * This method is used to return Guided Sales page.
	 *
	 * @param model
	 * @return JSP Page
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/page", method = RequestMethod.GET)
	public String guidedSales(final Model model) throws CMSItemNotFoundException
	{
		LOG.debug("****************Inside guidedSales method*****************");
		storeCmsPageInModel(model, getContentPageForLabelOrId(GUIDED_SALES_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(GUIDED_SALES_CMS_PAGE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}


	@RequestMapping(method = RequestMethod.GET, value = "/getSalesModelList")
	@ResponseBody
	public List<ProductData> getSalesModelsList(
			@RequestParam(value = "categoryNameList", defaultValue = "") final String categoryNameList)
	{
		final List<ProductData> finalProductDataList = new ArrayList<>();
		final String[] categoryNameArray = categoryNameList.split(",");
		for (final String categoryName : categoryNameArray)
		{
			final CategoryModel category = dealerSearchFacade.getCategoryByName(categoryName.trim());
			if (category != null)
			{
				finalProductDataList.addAll(dealerSearchFacade.getSalesModels(category.getCode(), false));
			}
		}
		return finalProductDataList;
	}

}
