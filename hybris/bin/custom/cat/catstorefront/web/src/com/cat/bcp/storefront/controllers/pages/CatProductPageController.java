/**
 *
 */
package com.cat.bcp.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cmsfacades.data.MediaData;
import de.hybris.platform.commercefacades.user.data.CustomerData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.facade.product.CatProductFacade;
import com.cat.bcp.facades.order.CatCartFacade;
import com.cat.facades.product.CatAccessoryData;


/**
 * @author sagdhingra
 *
 */
@Controller
@RequestMapping(value = "/accessories")
public class CatProductPageController extends ProductPageController
{

	private static final String ACCESSORY_CMS_PAGE = "catAccessoriesListPage";

	@Resource(name = "catProductFacade")
	private CatProductFacade catProductFacade;

	@Resource(name = "cartFacade")
	private CatCartFacade cartFacade;


	/**
	 *
	 * This method will return accessory data for utility vehicles
	 *
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String showAccessoryPage(final Model model) throws CMSItemNotFoundException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(ACCESSORY_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ACCESSORY_CMS_PAGE));
		final Breadcrumb breadcrumb1 = new Breadcrumb(CatCoreConstants.LINK_URL, CatCoreConstants.ACCESSORY_PAGE,
				CatCoreConstants.LAST_LINK_CLASS);
		final CustomerData customer = getCustomerFacade().getCurrentCustomer();
		model.addAttribute("userEmailId", customer.getEmail());
		final List<Breadcrumb> breadcrumbs = new ArrayList<>();
		breadcrumbs.add(breadcrumb1);
		final Map<String, Object> cartType = cartFacade.getCartType("");
		model.addAttribute(CatCoreConstants.CART_TYPE, cartType.get(CatCoreConstants.CART_PRODUCT_TYPE));
		model.addAttribute(CatCoreConstants.IS_UTV_CART, cartType.get(CatCoreConstants.IS_UTV_CART));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	/**
	 *
	 * This method will return accessory data for utility vehicles
	 *
	 */
	@RequestMapping(value = "/getAccessories", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, List<CatAccessoryData>> getAccessoryData() throws CMSItemNotFoundException
	{
		return catProductFacade.getAccessoryData();
	}


	/**
	 *
	 * This method will return accessory data for utility vehicles
	 *
	 */
	@RequestMapping(value = "/getCUVModels", method = RequestMethod.GET)
	@ResponseBody
	public List<String> getCUVModels() throws CMSItemNotFoundException
	{
		return catProductFacade.getCUVModels();
	}

	/**
	 *
	 * This method will return Coaching Tools data for Product.
	 *
	 */
	@RequestMapping(value = "/getCoachingToolsData", method = RequestMethod.GET)
	@ResponseBody
	public List<MediaData> getCoachingToolsData(@RequestParam(value = "productCode", required = true) final String productCode)
	{
		return catProductFacade.getCoachingToolsData(productCode);
	}
}
