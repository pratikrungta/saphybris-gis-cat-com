/**
 *
 */
package com.cat.bcp.storefront.controllers.pages;


import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.model.AlertProductModel;
import com.cat.bcp.core.model.RestockAlertModel;
import com.cat.bcp.core.servicelayer.impl.CatOrderWindowCalculationUtility;
import com.cat.bcp.facades.customer.CatCustomerFacade;
import com.cat.bcp.facades.order.CatCartFacade;
import com.cat.bcp.facades.order.CatReorderFacade;
import com.cat.bcp.facades.search.DealerSearchFacade;


/**
 * @author sagdhingra
 *
 */
@Controller
@RequestMapping("/reorder")
public class CatReorderController extends SearchPageController
{


	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(CatReorderController.class);


	@Resource(name = "productConverter")
	private Converter<ProductModel, ProductData> productConverter;

	@Resource(name = "catCustomerFacade")
	private CatCustomerFacade catCustomerFacade;

	@Resource(name = "dealerSearchFacade")
	private DealerSearchFacade dealerSearchFacade;

	@Resource(name = "catReorderFacade")
	private CatReorderFacade catReorderFacade;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "cartFacade")
	private CatCartFacade cartFacade;

	@Resource(name = "catOrderWindowCalculationUtility")
	private CatOrderWindowCalculationUtility catOrderWindowCalculationUtility;

	private static final String LINK_URL = "#";
	private static final String LAST_LINK_CLASS = "active";
	private static final String REORDER_PRODUCTS = "Reorder Products";
	private static final String MANAGE_ORDERS_PAGE = "manageOrders";

	@Override
	@RequestMapping(value = "/viewManageOrders", method = RequestMethod.GET)
	public String getManageOrdersPage(final Model model) throws CMSItemNotFoundException
	{
		final List<Breadcrumb> breadcrumbs = new ArrayList<>();

		final Breadcrumb breadcrumb1 = new Breadcrumb(LINK_URL, REORDER_PRODUCTS, LAST_LINK_CLASS);
		breadcrumbs.add(breadcrumb1);
		final B2BCustomerModel customerModel = catCustomerFacade.getCurrentDealer();
		final String userType = catCustomerFacade.getUserType();
		if ((CatCoreConstants.UTV.equals(userType) || CatCoreConstants.IM.equals(userType))
				&& CollectionUtils.isEmpty(getReorderProducts(model, "", "").getResults()))
		{
			return REDIRECT_TO_HOMEPAGE;
		}
		model.addAttribute("lowStockCategories", catReorderFacade.getLowStockProductsCategories(customerModel.getDefaultB2BUnit()));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);
		final Map<String, Object> cartType = cartFacade.getCartType("");
		model.addAttribute(CatCoreConstants.CART_TYPE, cartType.get(CatCoreConstants.CART_PRODUCT_TYPE));
		model.addAttribute(CatCoreConstants.IS_UTV_CART, cartType.get(CatCoreConstants.IS_UTV_CART));
		storeCmsPageInModel(model, getContentPageForLabelOrId(MANAGE_ORDERS_PAGE));
		return getViewForPage(model);
	}


	@Override
	@ResponseBody
	@RequestMapping(value = "/getReorderProducts", method = RequestMethod.GET)
	public SearchResultsData<ProductData> getReorderProducts(final Model model,
			@RequestParam(value = "salesModelId", defaultValue = "") final String salesModelId,
			@RequestParam(value = "category", defaultValue = "") final String category) throws CMSItemNotFoundException
	{
		final B2BCustomerModel customerModel = catCustomerFacade.getCurrentDealer();
		List<RestockAlertModel> alertEntries;

		alertEntries = catReorderFacade.getLowStockProducts(customerModel.getDefaultB2BUnit(), category, salesModelId);
		final int numOfProdsPerPage = Integer.parseInt(Config.getParameter("cat.products.per.page"));
		final SearchResultsData<ProductData> searchResultsData = new SearchResultsData<>();

		if (CollectionUtils.isNotEmpty(alertEntries))
		{
			final List<ProductData> productList = catReorderFacade.getReorderProducts(alertEntries,
					customerModel.getDefaultB2BUnit());
			final PaginationData pagination = new PaginationData();
			pagination.setTotalNumberOfResults(alertEntries.size());
			pagination.setPageSize(numOfProdsPerPage);
			searchResultsData.setResults(productList);
			if (null != cartFacade.getSessionCart())
			{
				searchResultsData.setCartLaneType(
						null != cartFacade.getSessionCart().getLaneType() ? cartFacade.getSessionCart().getLaneType().getCode()
								: StringUtils.EMPTY);
			}

			searchResultsData.setPagination(pagination);
		}

		return searchResultsData;

	}


	@Override
	@ResponseBody
	@RequestMapping(value = "/getReorderConfigProducts", method = RequestMethod.GET)
	public SearchResultsData<ProductData> getReorderConfigProducts(final Model model,
			@RequestParam(value = "salesModelCode") final String salesModelCode) throws CMSItemNotFoundException
	{

		final B2BCustomerModel customerModel = catCustomerFacade.getCurrentDealer();
		final String alertId = salesModelCode + "_" + customerModel.getDefaultB2BUnit().getUid();

		final List<AlertProductModel> alertEntries = catReorderFacade.getAssociatedConfigProducts(alertId);

		final SearchResultsData<ProductData> searchResultsData = new SearchResultsData<>();
		final List<ProductData> productList = new ArrayList();
		for (final AlertProductModel restockEntry : alertEntries)
		{
			final ProductData product = productConverter.convert(restockEntry.getProduct());
			productList.add(product);

		}

		final PaginationData pagination = new PaginationData();
		pagination.setTotalNumberOfResults(alertEntries.size());

		searchResultsData.setResults(productList);
		searchResultsData.setPagination(pagination);

		return searchResultsData;

	}

	@Override
	@RequestMapping(method = RequestMethod.GET, value = "/selectLowStockCategory")
	@ResponseBody
	public List<ProductData> getLowStockSalesModels(@RequestParam(value = "categoryCode", defaultValue = "") final String category)
	{
		List<ProductData> productList = null;
		try
		{
			final B2BCustomerModel customerModel = catCustomerFacade.getCurrentDealer();
			List<RestockAlertModel> alertEntries;
			alertEntries = catReorderFacade.getLowStockProducts(customerModel.getDefaultB2BUnit(), category, "");
			productList = new ArrayList();
			for (final RestockAlertModel restockAlertModel : alertEntries)
			{
				final ProductData product = productConverter.convert(restockAlertModel.getProduct());
				productList.add(product);
			}
		}
		catch (final Exception e)
		{
			LOG.error("Error while loading low stock sales Model" + e.getMessage(), e);
		}

		return productList;
	}


	@RequestMapping(method = RequestMethod.GET, value = "/orderingWindowForLanes")
	@ResponseBody
	public Map<String, Object> getOrderingWindowMessage()
	{
		return catOrderWindowCalculationUtility.getOrderingWindowMessage();
	}
}
