/**
 *
 */
package com.cat.bcp.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.util.Config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.exception.CatSpeccheckException;
import com.cat.bcp.facades.search.DealerSearchFacade;
import com.cat.bcp.facades.speccheck.CatSpeccheckFacade;


/**
 * @author asomjal : Controller is used for Speccheck integration.
 */
@Controller
@RequestMapping("/speccheck")
public class CatSpecCheckProxyController extends AbstractSearchPageController
{
	private static final Logger LOG = Logger.getLogger(CatSpecCheckProxyController.class);

	public static final String CATEGORIES = "categories";

	@Resource(name = "categoryService")
	private CategoryService categoryService;

	@Resource(name = "catSpeccheckFacade")
	private CatSpeccheckFacade catSpeccheckFacade;

	@Resource(name = "dealerSearchFacade")
	private DealerSearchFacade dealerSearchFacade;

	@Resource(name = "productService")
	private ProductService productService;

	/**
	 * This method is used to return Speccheck PLP page.
	 *
	 * @param model
	 * @param categoryName
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/viewSpeccheckPLP", method = RequestMethod.GET)
	public String viewSpeccheck(final Model model,
			@RequestParam(value = "categoryName", required = false) final String categoryName) throws CMSItemNotFoundException
	{
		LOG.debug("****************inside viewSpeccheckPLP method*****************");
		model.addAttribute(CMS_PAGE_TITLE, CatCoreConstants.SPECCHECK_CATERPILLAR);
		model.addAttribute(CMS_PAGE_MODEL, getContentPageForLabelOrId(CatCoreConstants.SPECCHECK_PLPPAGE));
		model.addAttribute(CatCoreConstants.SPECCHEKCOMPARE_MEDIA_PREFIX_KEY,
				Config.getParameter(CatCoreConstants.SPECCHEKCOMPARE_MEDIA_PREFIX_VALUE));
		final ContentPageModel registerOptionPage = getContentPageForLabelOrId(CatCoreConstants.SPECCHECK_PLPPAGE);
		model.addAttribute(CatCoreConstants.CATEGORY_NAME_VAL, categoryName);
		final List<Breadcrumb> breadcrumbs = new ArrayList<>();
		final Breadcrumb breadcrumb = new Breadcrumb(CatCoreConstants.LINK_URL, CatCoreConstants.SPECCHECK_PRODUCTLIST,
				CatCoreConstants.LAST_LINK_CLASS);
		breadcrumbs.add(breadcrumb);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);
		return getViewForPage(registerOptionPage);
	}

	/**
	 * This method is used to return Speccheck compare page.
	 *
	 * @param model
	 * @param salesModel
	 * @return
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/viewSpeccheckCompare", method = RequestMethod.GET)
	public String viewSpeccheckCompare(final Model model,
			@RequestParam(value = "salesModel", required = false) final String salesModel,
			@RequestParam(value = "categoryName", required = false, defaultValue = "COMPACT TRACK LOADERS") final String categoryName)
			throws CMSItemNotFoundException
	{
		LOG.debug("****************inside viewSpeccheckCompare method*****************");
		model.addAttribute(CMS_PAGE_TITLE, CatCoreConstants.SPECCHECK_CATERPILLAR);
		model.addAttribute(CMS_PAGE_MODEL, getContentPageForLabelOrId(CatCoreConstants.SPECCHECK_COMPPAGE));
		model.addAttribute(CatCoreConstants.SPECCHEKCOMPARE_MEDIA_PREFIX_KEY,
				Config.getParameter(CatCoreConstants.SPECCHEKCOMPARE_MEDIA_PREFIX_VALUE));
		final ProductData productData = catSpeccheckFacade.getProductData(salesModel);
		model.addAttribute(CatCoreConstants.SPECCHEKCOMPARE_SALESMODEL, productData.getCode());
		model.addAttribute(CatCoreConstants.SPECCHEKCOMPARE_IMAGEURL, catSpeccheckFacade.getImageURL(productData));
		model.addAttribute(CatCoreConstants.SPECCHEKCOMPARE_PRODUCTDATA, productData);
		model.addAttribute(CatCoreConstants.SPECCHECK_MACHINEIDONLOAD, productData.getMachineId());
		final ContentPageModel registerOptionPage = getContentPageForLabelOrId(CatCoreConstants.SPECCHECK_COMPPAGE);
		final List<Breadcrumb> breadcrumbs = new ArrayList<>();
		final Breadcrumb breadcrumb = new Breadcrumb(CatCoreConstants.SPECCHECK_BREADCRUMBFORPLPURI + categoryName,
				CatCoreConstants.SPECCHECK_PRODUCTLIST, CatCoreConstants.MIDDLE_LINK_CLASS);
		final Breadcrumb breadcrumb1 = new Breadcrumb(CatCoreConstants.LINK_URL, CatCoreConstants.SPECCHECK_COMPAREMODELS,
				CatCoreConstants.LAST_LINK_CLASS);
		breadcrumbs.add(breadcrumb);
		breadcrumbs.add(breadcrumb1);
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);

		return getViewForPage(registerOptionPage);
	}

	@RequestMapping(value = "/getAllCategoriesData", method = RequestMethod.GET)
	@ResponseBody
	public Collection<CategoryData> getAllCategoriesData()
	{
		final CategoryModel category = categoryService.getCategoryForCode(CatCoreConstants.SPECCHECK_EQUIPMENT);
		return catSpeccheckFacade.getCategoryData(category);
	}


	/**
	 * This method is used to populate Sales Models against selected category for comparison on PLP page.
	 *
	 * @param categoryCode
	 * @return List of Sales Models
	 */
	@RequestMapping(value = "/getCompareSalesModels", method = RequestMethod.GET)
	@ResponseBody
	public List<ProductData> getSalesModelForCompare(@RequestParam(value = "categoryCode") final String categoryCode)
	{
		final CategoryModel category = dealerSearchFacade.getCategoryByName(categoryCode);

		if (category != null)
		{
			return catSpeccheckFacade.getSalesModelForCompare(category);
		}
		return Collections.emptyList();
	}


	/**
	 * When user clicks on compare button on the comparison page this handler is used to return the comparison data from
	 * Speccheck Handler 6. Also we can use this to get results from speccheck Handler 2,5
	 *
	 * @param mids
	 * @param fid
	 * @param rid
	 * @param handler
	 * @param uom
	 *           (defaultvalue is 'imperial' i.e. '2' - unit of measurement)
	 * @return JSON response from speccheck
	 */
	@RequestMapping(value = "/callSpeccheckApi", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object callSpeccheckApi(@RequestParam(value = "mids", required = false) final String mids,
			@RequestParam(value = "fid", required = false) final String fid,
			@RequestParam(value = "rid", required = false) final String rid,
			@RequestParam(value = "handler", required = true) final String handler,
			@RequestParam(value = "uom", defaultValue = "2", required = false) final String uom) throws CatSpeccheckException
	{
		return catSpeccheckFacade.getSpeccheckResponse(mids, fid, rid, handler, uom);
	}

	/**
	 * While user comes to comparison page from PLP, This handler is used to populate competitors drop-downs against
	 * selected sales model.
	 *
	 * @param salesModel
	 * @return JSON response
	 */
	@RequestMapping(value = "/competitorProducts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object getCompetitorProducts(@RequestParam(value = "salesModel", required = true) final String salesModel)
			throws CatSpeccheckException
	{
		final ProductModel productModel = productService.getProductForCode(salesModel);
		final Collection<CategoryModel> categories = productModel.getSupercategories();
		for (final CategoryModel category : categories)
		{
			if ((CatCoreConstants.UTV).equalsIgnoreCase(category.getCode()))
			{
				return catSpeccheckFacade.getUTVProductsForCompare(category, salesModel);
			}
		}
		return catSpeccheckFacade.getCompetitorProducts(salesModel);
	}

	/**
	 * When user clicks on compare button on the comparison page this handler is used to return the comparison data from
	 * Hybris for UTV products.
	 *
	 * @param salesModels
	 * @param uom
	 *           (defaultvalue is 'imperial' i.e. '2' - unit of measurement)
	 * @return JSON response from Hybris
	 */
	@RequestMapping(value = "/productCompareUsingHybris", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Object productCompareUsingHybris(@RequestParam(value = "salesModels", required = true) final String salesModels,
			@RequestParam(value = "uom", defaultValue = "2", required = false) final String uom) throws CatSpeccheckException
	{
		return catSpeccheckFacade.getProductSpecs(salesModels, uom);
	}
}
