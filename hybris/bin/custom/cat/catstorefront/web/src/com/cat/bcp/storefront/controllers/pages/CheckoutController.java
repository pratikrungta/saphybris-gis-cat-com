/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.storefront.controllers.pages;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorfacades.flow.impl.SessionOverrideCheckoutFlowFacade;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ProductBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCheckoutController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.GuestRegisterForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.GuestRegisterValidator;
import de.hybris.platform.acceleratorstorefrontcommons.security.AutoLoginStrategy;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2bcommercefacades.company.B2BUnitFacade;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSSiteService;
import de.hybris.platform.commercefacades.coupon.data.CouponData;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.util.ResponsiveUtils;
import de.hybris.platform.order.CartService;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.enums.LaneTypeEnum;
import com.cat.bcp.core.servicelayer.CatProductService;
import com.cat.bcp.facade.product.CatStockFacade;
import com.cat.bcp.facades.address.CatReviewOrderData;
import com.cat.bcp.facades.customer.CatCustomerFacade;
import com.cat.bcp.facades.integration.CatSnopFacade;
import com.cat.bcp.facades.order.CatCartFacade;
import com.cat.bcp.facades.order.CatCheckoutFacade;
import com.cat.bcp.storefront.controllers.ControllerConstants;
import com.cat.bcp.storefront.forms.CatReviewOrderForm;
import com.cat.core.integration.snop.response.CatSnopResponseMapper;
import com.cat.facades.order.CATOrderReviewData;
import com.cat.facades.order.PurchaseOrderCheck;
import com.cat.facades.user.CatPlaceOrderForAccessoriesData;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * CheckoutController
 */
@Controller
@RequestMapping(value = "/checkout")
public class CheckoutController extends AbstractCheckoutController
{

	private static final Logger LOG = Logger.getLogger(CheckoutController.class);
	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
	 * the issue and future resolution.
	 */
	private static final String ORDER_CODE_PATH_VARIABLE_PATTERN = "{orderCode:.*}";
	private static final String PRODUCT_CODE_PATH_VARIABLE_PATTERN = "{productCode:.*}";
	private static final String CHECKOUT_ORDER_CONFIRMATION_CMS_PAGE_LABEL = "orderConfirmation";
	private static final String REVIEW_ORDER_CMS_PAGE_LABEL = "reviewOrder";
	private static final String CONTINUE_URL_KEY = "continueUrl";
	private static final String REVIEW_ORDER = "Review Order";
	private static final String REORDER_PRODUCTS = "Reorder Products";
	private static final String LINK_URL = "#";
	private static final String LAST_LINK_CLASS = "active";
	private static final String VIEW_MANAGEORDERS_LINK_URL = "/search/viewManageOrders";
	private static final String PLACE_ORDER_EXCEPTION_MESSAGE = "Exception occured in #placeReOrder() call:";
	private static final String PRODUCT_CODE_NOT_NULL = "ProductCode Parameter must not be null";
	private static final String REORDER_QTY_NOT_NULL = "ReOrder ProductQuantity Parameter id must not be null";
	public static final String SHIPPING_ADDRESS_LIST = "shippingAddressList";
	public static final String HIDDEN_SHIPPING_ADDRESS_LIST = "hiddenShippingAddressList";
	public static final String ALL_SHIPPING_ADDRESS_LIST = "allShippingAddressList";
	public static final String ENABLEADDCOUNTY = "enableAddCounty";

	@Resource(name = "productFacade")
	private ProductFacade productFacade;

	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;

	@Resource(name = "checkoutFacade")
	private CatCheckoutFacade checkoutFacade;

	@Resource(name = "guestRegisterValidator")
	private GuestRegisterValidator guestRegisterValidator;

	@Resource(name = "autoLoginStrategy")
	private AutoLoginStrategy autoLoginStrategy;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "cartFacade")
	private CatCartFacade cartFacade;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "b2bUnitFacade")
	private B2BUnitFacade b2bUnitFacade;

	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "catStockFacade")
	private CatStockFacade catStockFacade;

	@Resource(name = "catCustomerFacade")
	private CatCustomerFacade catCustomerFacade;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "catProductService")
	private CatProductService catProductService;

	@Resource(name = "productBreadcrumbBuilder")
	private ProductBreadcrumbBuilder productBreadcrumbBuilder;

	@Autowired
	private CatSnopFacade catSnopFacade;

	@Autowired
	private CMSSiteService cmsSiteService;

	@ExceptionHandler(ModelNotFoundException.class)
	public String handleModelNotFoundException(final ModelNotFoundException exception, final HttpServletRequest request)
	{
		request.setAttribute("message", exception.getMessage());
		return FORWARD_PREFIX + "/404";
	}

	@RequestMapping(method = RequestMethod.GET)
	public String checkout(final RedirectAttributes redirectModel)
	{
		if (getCheckoutFlowFacade().hasValidCart())
		{
			if (validateCart(redirectModel))
			{
				return REDIRECT_PREFIX + "/cart";
			}
			else
			{
				checkoutFacade.prepareCartForCheckout();
				return getCheckoutRedirectUrl();
			}
		}

		LOG.info("Missing, empty or unsupported cart");

		// No session cart or empty session cart. Bounce back to the cart page.
		return REDIRECT_PREFIX + "/cart";
	}

	@RequestMapping(value = "/orderConfirmation/" + ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	@RequireHardLogIn
	public String orderConfirmation(@PathVariable("orderCode") final String orderCode, final HttpServletRequest request,
			final Model model, final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		SessionOverrideCheckoutFlowFacade.resetSessionOverrides();
		return processOrderCode(orderCode, model, request, redirectModel);
	}


	@RequestMapping(value = "/orderConfirmation/" + ORDER_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.POST)
	public String orderConfirmation(final GuestRegisterForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		getGuestRegisterValidator().validate(form, bindingResult);
		return processRegisterGuestUserRequest(form, bindingResult, model, request, response, redirectModel);
	}

	protected String processRegisterGuestUserRequest(final GuestRegisterForm form, final BindingResult bindingResult,
			final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		if (bindingResult.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "form.global.error");
			return processOrderCode(form.getOrderCode(), model, request, redirectModel);
		}
		try
		{
			getCustomerFacade().changeGuestToCustomer(form.getPwd(), form.getOrderCode());
			getAutoLoginStrategy().login(getCustomerFacade().getCurrentCustomer().getUid(), form.getPwd(), request, response);
			getSessionService().removeAttribute(WebConstants.ANONYMOUS_CHECKOUT);
		}
		catch (final DuplicateUidException e)
		{
			// User already exists
			LOG.warn("guest registration failed: " + e);
			model.addAttribute(new GuestRegisterForm());
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"guest.checkout.existingaccount.register.error", new Object[]
					{ form.getUid() });
			return REDIRECT_PREFIX + request.getHeader("Referer");
		}

		return REDIRECT_PREFIX + "/";
	}

	protected String processOrderCode(final String orderCode, final Model model, final HttpServletRequest request,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		final OrderData orderDetails;

		try
		{
			orderDetails = orderFacade.getOrderDetailsForCode(orderCode);
		}
		catch (final UnknownIdentifierException e)
		{
			LOG.error("Attempted to load an order confirmation that does not exist or is not visible. Redirect to home page.", e);
			return REDIRECT_PREFIX + ROOT;
		}

		if (orderDetails.isGuestCustomer() && !StringUtils.substringBefore(orderDetails.getUser().getUid(), "|")
				.equals(getSessionService().getAttribute(WebConstants.ANONYMOUS_CHECKOUT_GUID)))
		{
			return getCheckoutRedirectUrl();
		}

		if (orderDetails.getEntries() != null && !orderDetails.getEntries().isEmpty())
		{
			for (final OrderEntryData entry : orderDetails.getEntries())
			{
				final String productCode = entry.getProduct().getCode();
				final ProductData product = productFacade.getProductForCodeAndOptions(productCode,
						Arrays.asList(ProductOption.BASIC, ProductOption.PRICE, ProductOption.CATEGORIES));
				entry.setProduct(product);
			}
		}

		model.addAttribute("orderCode", orderCode);
		model.addAttribute("orderData", orderDetails);
		model.addAttribute("allItems", orderDetails.getEntries());
		model.addAttribute("deliveryAddress", orderDetails.getDeliveryAddress());
		model.addAttribute("deliveryMode", orderDetails.getDeliveryMode());
		model.addAttribute("paymentInfo", orderDetails.getPaymentInfo());
		model.addAttribute("pageType", PageType.ORDERCONFIRMATION.name());

		final List<CouponData> giftCoupons = orderDetails.getAppliedOrderPromotions().stream()
				.filter(x -> CollectionUtils.isNotEmpty(x.getGiveAwayCouponCodes())).flatMap(p -> p.getGiveAwayCouponCodes().stream())
				.collect(Collectors.toList());
		model.addAttribute("giftCoupons", giftCoupons);

		processEmailAddress(model, orderDetails);

		final String continueUrl = (String) getSessionService().getAttribute(WebConstants.CONTINUE_URL);
		model.addAttribute(CONTINUE_URL_KEY, (continueUrl != null && !continueUrl.isEmpty()) ? continueUrl : ROOT);

		final AbstractPageModel cmsPage = getContentPageForLabelOrId(CHECKOUT_ORDER_CONFIRMATION_CMS_PAGE_LABEL);
		storeCmsPageInModel(model, cmsPage);
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CHECKOUT_ORDER_CONFIRMATION_CMS_PAGE_LABEL));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		if (ResponsiveUtils.isResponsive())
		{
			return getViewForPage(model);
		}

		return ControllerConstants.Views.Pages.Checkout.CheckoutConfirmationPage;
	}

	protected void processEmailAddress(final Model model, final OrderData orderDetails)
	{
		final String uid;

		if (orderDetails.isGuestCustomer() && !model.containsAttribute("guestRegisterForm"))
		{
			final GuestRegisterForm guestRegisterForm = new GuestRegisterForm();
			guestRegisterForm.setOrderCode(orderDetails.getGuid());
			uid = orderDetails.getPaymentInfo().getBillingAddress().getEmail();
			guestRegisterForm.setUid(uid);
			model.addAttribute(guestRegisterForm);
		}
		else
		{
			uid = orderDetails.getUser().getUid();
		}
		model.addAttribute("email", uid);
	}

	protected GuestRegisterValidator getGuestRegisterValidator()
	{
		return guestRegisterValidator;
	}

	protected AutoLoginStrategy getAutoLoginStrategy()
	{
		return autoLoginStrategy;
	}


	@SuppressWarnings("boxing")
	@RequestMapping(value = "/reviewUTV/" + PRODUCT_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String reviewOrderUTV(@RequestParam("productCode") final String productCode,
			@RequestParam("orderQuantity") final int orderQuantity, final Model model)
	{
		validateParameterNotNull(productCode, PRODUCT_CODE_NOT_NULL);
		validateParameterNotNull(orderQuantity, REORDER_QTY_NOT_NULL);
		ProductData productData = null;
		try
		{
			productData = productFacade.getProductForCodeAndOptions(productCode, Arrays.asList(ProductOption.CATEGORIES,
					ProductOption.VARIANT_MATRIX_BASE, ProductOption.VARIANT_MATRIX_URL, ProductOption.VARIANT_MATRIX_MEDIA));
			final boolean isUtv = catProductService.isUtvProduct(productData);
			model.addAttribute("isUtilityProduct", isUtv);
			final ProductData baseProductData = productFacade.getProductForCodeAndOptions(productData.getCode(), Arrays
					.asList(ProductOption.VARIANT_MATRIX_BASE, ProductOption.VARIANT_MATRIX_URL, ProductOption.VARIANT_MATRIX_MEDIA));
			model.addAttribute("shippingFactor", baseProductData.getShippingFactor());
			final CatReviewOrderData reviewOrderData = checkoutFacade.getReviewOrderData(productCode, null, orderQuantity);
			reviewOrderData.setCurrentStock(0);
			model.addAttribute("reviewOrderData", reviewOrderData);
			model.addAttribute("entryNumber", 0);
			storeCmsPageInModel(model, getContentPageForLabelOrId(REVIEW_ORDER_CMS_PAGE_LABEL));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REVIEW_ORDER_CMS_PAGE_LABEL));
		}
		catch (final CMSItemNotFoundException e)
		{
			LOG.warn("unable to load review re-order details page : " + e);
		}

		final List<Breadcrumb> breadcrumbs = new ArrayList<>();
		breadcrumbs.add(new Breadcrumb(productData.getUrl(), productData.getName(), CatCoreConstants.MIDDLE_LINK_CLASS));
		final Breadcrumb breadcrumb = new Breadcrumb(LINK_URL, REVIEW_ORDER, LAST_LINK_CLASS);
		breadcrumbs.add(breadcrumb);

		model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);
		return getViewForPage(model);
	}

	@SuppressWarnings("boxing")
	@RequestMapping(value = "/review/" + PRODUCT_CODE_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String reviewOrder(@PathVariable("productCode") final String productCode,
			@RequestParam("salesModelCode") final String salesModelCode,
			@RequestParam("reOrderProductQuantity") final int reOrderProductQuantity, final Model model, final HttpSession session)
	{
		validateParameterNotNull(productCode, PRODUCT_CODE_NOT_NULL);
		validateParameterNotNull(reOrderProductQuantity, REORDER_QTY_NOT_NULL);

		try
		{
			final boolean isIM = (null != session.getAttribute("isIM")) ? (boolean) session.getAttribute("isIM") : false;
			if (isIM)
			{
				checkoutFacade.addToCart(productCode, reOrderProductQuantity);
				final CatReviewOrderData reviewOrderData = checkoutFacade.getReviewOrderData(productCode, salesModelCode,
						reOrderProductQuantity);
				model.addAttribute("reviewOrderData", reviewOrderData);
				storeCmsPageInModel(model, getContentPageForLabelOrId(REVIEW_ORDER_CMS_PAGE_LABEL));
				setUpMetaDataForContentPage(model, getContentPageForLabelOrId(REVIEW_ORDER_CMS_PAGE_LABEL));
			}
		}
		catch (final CMSItemNotFoundException e)
		{
			LOG.warn("unable to load review re-order details page : " + e);
		}

		final List<Breadcrumb> breadcrumbs = new ArrayList<>();

		breadcrumbs.add(new Breadcrumb(VIEW_MANAGEORDERS_LINK_URL, REORDER_PRODUCTS, LAST_LINK_CLASS));
		breadcrumbs.add(new Breadcrumb(LINK_URL, REVIEW_ORDER, LAST_LINK_CLASS));

		model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);
		return getViewForPage(model);
	}

	@ResponseBody
	@RequestMapping(value = "/submitReOrder", method = RequestMethod.POST)
	public Map placeUTVOrder(final CatReviewOrderForm form, final BindingResult bindingResult, final Model model,
			final HttpServletRequest request, final HttpServletResponse response, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		convertPONumberToUppercase(form);
		final Map duplicatePOresultMap = getDuplicatePosMap(form.getPurchaseOrderList());
		if (!MapUtils.isEmpty(duplicatePOresultMap))
		{
			return duplicatePOresultMap;
		}
		final Map resultMap = new HashMap();
		final PurchaseOrderCheck purchaseOrderCheck = checkoutFacade.checkPOList(form.getPurchaseOrderList());
		if (purchaseOrderCheck.getPurchaseOrderPresent().booleanValue())
		{
			try
			{
				if (cartFacade.hasEntries())
				{
					final CATOrderReviewData catOrderReviewData = populateCATOrderReviewData(form);
					final OrderData orderData = checkoutFacade.placeCatOrder(catOrderReviewData);

					splitUTVOrder(form, orderData);
					writePurchaseOrderObject(purchaseOrderCheck, orderData, resultMap);
				}
			}
			catch (final Exception e)
			{
				LOG.error(PLACE_ORDER_EXCEPTION_MESSAGE + e.getMessage(), e);
			}
			return resultMap;
		}
		else
		{

			return getJsonofDuplicatePos(purchaseOrderCheck, resultMap);
		}
	}

	/**
	 * This method is used to convert Purchase orders to Upper Case
	 *
	 * @param form
	 *           CatReviewOrderForm
	 */
	private void convertPONumberToUppercase(final CatReviewOrderForm form)
	{
		final List<String> purchaseOrderList = form.getPurchaseOrderList();
		final List<String> purchaseOrderListUpperCase = purchaseOrderList.stream().map(String::toUpperCase)
				.collect(Collectors.toList());
		form.setPurchaseOrderList(purchaseOrderListUpperCase);
	}

	/**
	 * @param form
	 * @param orderData
	 */
	private void splitUTVOrder(final CatReviewOrderForm form, final OrderData orderData)
	{
		if (orderData != null)
		{
			if (form.getPurchaseOrderList().size() > 1)
			{
				checkoutFacade.splitOrderByPO(orderData.getCode(), form.getPurchaseOrderList());
				orderData.setSplitOrderCount(Integer.valueOf(form.getPurchaseOrderList().size()));
			}
			else
			{
				orderData.setSplitOrderCount(Integer.valueOf(1));
			}
		}
	}


	/**
	 * @param purchaseOrderCheck
	 * @param orderData
	 * @param resultMap
	 */
	private void writePurchaseOrderObject(final PurchaseOrderCheck purchaseOrderCheck, final OrderData orderData,
			final Map resultMap)
	{
		try
		{
			final ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Include.ALWAYS);

			final String purchaseOrderCheckObject = mapper.writer().writeValueAsString(purchaseOrderCheck);
			final String orderListObject = mapper.writer().writeValueAsString(orderData);

			resultMap.put("purchaseOrderCheck", purchaseOrderCheckObject);

			resultMap.put("orderDataList", orderListObject);
		}
		catch (final Exception ex)
		{
			LOG.error("Excpetion Occured in object mapping in writePurchaseOrderObject() method: ", ex);
		}
	}

	private CATOrderReviewData populateCATOrderReviewData(final CatReviewOrderForm form)
	{
		final CATOrderReviewData catOrderReviewData = new CATOrderReviewData();

		catOrderReviewData.setAcceptance(form.getAcceptance());
		if (StringUtils.isNotEmpty(form.getComments()))
		{
			catOrderReviewData.setComments(form.getComments());
		}
		if (form.getOrderedQty() != null)
		{
			catOrderReviewData.setOrderedQty(form.getOrderedQty());
		}
		if (CollectionUtils.isNotEmpty(form.getPurchaseOrderList()))
		{
			catOrderReviewData.setPurchaseOrder(form.getPurchaseOrderList());
		}
		if (StringUtils.isNotEmpty(form.getSalesModelCode()))
		{
			catOrderReviewData.setSalesModelCode(form.getSalesModelCode());
		}
		if (StringUtils.isNotEmpty(form.getSelectedShippingAddressPK()))
		{
			catOrderReviewData.setSelectedShippingAddressPK(form.getSelectedShippingAddressPK());
		}
		if (StringUtils.isNotEmpty(form.getConfigModelCode()))
		{
			catOrderReviewData.setVariantConfigProductCode(form.getConfigModelCode());
		}

		if (CollectionUtils.isNotEmpty(form.getShippingAddressList()))
		{
			catOrderReviewData.setShippingAddressList(form.getShippingAddressList());
		}

		return catOrderReviewData;
	}


	@ResponseBody
	@RequestMapping(value = "/getDealerShippingAddresses", method = RequestMethod.GET)
	public final Map getShippingAddresses(@RequestParam(value = "sortParameter", required = false) final String sortParameter,
			@RequestParam(value = "shipToPk", required = false) final String shipToAddressPk)
	{
		final Map resultMap = new HashMap();
		final B2BCustomerModel customerModel = catCustomerFacade.getCurrentDealer();

		if (customerModel.getDefaultB2BUnit() != null)
		{
			final List<AddressData> shippingAddressList = catCustomerFacade.getDealerShipTos(customerModel, sortParameter);

			if (CollectionUtils.isNotEmpty(shippingAddressList))
			{
				final List<AddressData> visibleAddressData = shippingAddressList.stream()
						.filter(shippingAddress -> shippingAddress.isVisibleInAddressBook()).collect(Collectors.toList());
				final List<AddressData> hiddenAddressData = shippingAddressList.stream()
						.filter(shippingAddress -> !shippingAddress.isVisibleInAddressBook()).collect(Collectors.toList());
				resultMap.put(SHIPPING_ADDRESS_LIST, visibleAddressData);
				resultMap.put(HIDDEN_SHIPPING_ADDRESS_LIST, hiddenAddressData);
				resultMap.put(ALL_SHIPPING_ADDRESS_LIST, shippingAddressList);
			}
			if (StringUtils.isNotBlank(shipToAddressPk))
			{
				resultMap.put(ENABLEADDCOUNTY, catCustomerFacade.checkIfUnlinkedCountiesPresent(shipToAddressPk));
			}
		}
		return resultMap;
	}



	/**
	 *
	 * @param purchaseOrderCheck
	 *           the purchaseOrderCheck object
	 * @param resultMap
	 *           the map containing the {@link PurchaseOrderCheck} object
	 * @return maop containing the {@link PurchaseOrderCheck} if there are duplicate Po's else returns null
	 */
	private Map getJsonofDuplicatePos(final PurchaseOrderCheck purchaseOrderCheck, final Map resultMap)
	{
		try
		{
			final ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Include.ALWAYS);

			final String purchaseOrderCheckObject = mapper.writer().writeValueAsString(purchaseOrderCheck);
			resultMap.put("purchaseOrderCheck", purchaseOrderCheckObject);

			return resultMap;
		}

		catch (final Exception ex)
		{
			LOG.error("Excpetion Occured in object mapping: ", ex);
		}
		return null;
	}


	private Map getDuplicatePosMap(final List<String> purchaseOrderList)
	{

		final Map map = new HashMap();
		final Set<String> duplicatePos = new HashSet();
		final Set tempSet = new HashSet();
		for (final String purchaseOrderNbr : purchaseOrderList)
		{
			if (!tempSet.add(purchaseOrderNbr))
			{
				duplicatePos.add(purchaseOrderNbr);
			}
		}

		if (CollectionUtils.isNotEmpty(duplicatePos))
		{
			final PurchaseOrderCheck purchaseOrderCheck = new PurchaseOrderCheck();
			purchaseOrderCheck.setPurchaseOrderPresent(Boolean.FALSE);
			purchaseOrderCheck.setListOfErrorPO(duplicatePos.stream().collect(Collectors.toList()));
			purchaseOrderCheck.setDuplicatePONumbers(Boolean.TRUE);
			return getJsonofDuplicatePos(purchaseOrderCheck, map);
		}
		return null;
	}

	@SuppressWarnings("boxing")
	@ResponseBody
	@RequestMapping(value = "/placeReOrder", method = RequestMethod.POST)
	public Map placeReOrder(@RequestParam final String selectedShippingAddressPK,
			@RequestParam(required = false) final String comments, final Model model, final HttpServletRequest request,
			final HttpServletResponse response)
	{
		validateParameterNotNull(selectedShippingAddressPK, "ReOrder selected ShippingAddress PK Parameter must not be null");
		final Map<String, java.util.Collection<String>> poMap = getSessionService().getAttribute(CatCoreConstants.REORDER_PO_LIST);
		final List<java.util.Collection<String>> poListCollection = poMap.values().stream().collect(Collectors.toList());
		final List<String> poNumbersList = new ArrayList<>();
		poListCollection.forEach(poSet -> poNumbersList.addAll(poSet));
		final Map duplicatePOresultMap = getDuplicatePosMap(poNumbersList);
		if (!MapUtils.isEmpty(duplicatePOresultMap))
		{
			return duplicatePOresultMap;
		}
		final Map resultMap = new HashMap();
		final PurchaseOrderCheck purchaseOrderCheck = checkoutFacade.checkPOList(poNumbersList);
		if (purchaseOrderCheck.getPurchaseOrderPresent().booleanValue() && cartFacade.hasEntries())
		{
			try
			{
				final OrderData orderData = checkoutFacade.placeCatReOrder(comments, poNumbersList, selectedShippingAddressPK);
				if (null != orderData && CollectionUtils.isNotEmpty(orderData.getEntries()))
				{
					final Map poListMap = new HashMap();
					orderData.getEntries().stream().collect(Collectors.groupingBy(OrderEntryData::getSalesModelId))
							.forEach((salesModel, orderEntries) -> poListMap.put(salesModel,
									orderEntries.stream().mapToLong(OrderEntryData::getQuantity).sum()));
					resultMap.put("resultPO", poListMap);
					splitOrderAndAddAdditionalInfo(orderData);
					writePurchaseOrderObject(purchaseOrderCheck, orderData, resultMap);
					getSessionService().removeAttribute(CatCoreConstants.REORDER_PO_LIST);
				}
			}

			catch (final Exception e)
			{
				LOG.error(PLACE_ORDER_EXCEPTION_MESSAGE + e.getMessage(), e);
			}
			return resultMap;
		}
		else
		{
			return getJsonofDuplicatePos(purchaseOrderCheck, resultMap);
		}
	}

	/**
	 * This method is used to split order and add additional Info
	 *
	 * @param orderData
	 *           orderData
	 */
	private void splitOrderAndAddAdditionalInfo(final OrderData orderData)
	{
		final List<OrderEntryData> orderEntryDataList = orderData.getEntries();
		if (orderEntryDataList.size() > 1 || (orderEntryDataList.size() == 1 && orderEntryDataList.get(0) != null
				&& orderEntryDataList.get(0).getQuantity().intValue() > 1))
		{
			int splitOrderCount = 0;
			for (final OrderEntryData orderEntryData : orderEntryDataList)
			{
				splitOrderCount = splitOrderCount + orderEntryData.getQuantity().intValue();
			}
			orderData.setSplitOrderCount(Integer.valueOf(splitOrderCount));
			checkoutFacade.splitOrderByPO(orderData.getCode(), null);
		}
		else if (orderEntryDataList.get(0) != null && orderEntryDataList.get(0).getProduct() != null)
		{
			checkoutFacade.sendCatOrderConfirmationEmail(orderData.getCode());
			checkoutFacade.createAdditionalInfo(orderData.getCode(), orderEntryDataList.get(0).getProduct().getCode());
			orderData.setSplitOrderCount(Integer.valueOf(1));
		}
	}


	/**
	 * This method clears the cart.
	 */
	@ResponseBody
	@RequestMapping(value = "/clearCart", method = RequestMethod.GET)
	public void clearCart()
	{
		cartFacade.clearCart();
	}

	/**
	 * This method is used to place Order For Accessories
	 *
	 */
	@SuppressWarnings("boxing")
	@ResponseBody
	@RequestMapping(value = "/placeOrderForAccessories", method = RequestMethod.POST)
	public OrderData placeOrderForAccessories(@RequestBody final CatPlaceOrderForAccessoriesData catPlaceOrderForAccessoriesData)
	{
		validateParameterNotNull(catPlaceOrderForAccessoriesData.getSelectedShippingAddressPK(),
				"ReOrder selected ShippingAddress PK Parameter must not be null");
		OrderData orderData = new OrderData();
		if (cartFacade.hasEntries())
		{
			try
			{
				orderData = checkoutFacade.placeCatOrderForAccessories(catPlaceOrderForAccessoriesData.getComments(),
						catPlaceOrderForAccessoriesData.getSelectedShippingAddressPK());
				if (null != orderData && CollectionUtils.isNotEmpty(orderData.getEntries()))
				{
					final int orderSplitCount = orderData.getEntries().stream()
							.collect(Collectors.groupingBy(OrderEntryData::getCompatibleModel)).keySet().size();
					orderData.setSplitOrderCount(Integer.valueOf(orderSplitCount));
					checkoutFacade.splitOrderForAccessories(orderData.getCode());
				}

			}
			catch (final Exception e)
			{
				LOG.error(PLACE_ORDER_EXCEPTION_MESSAGE + e.getMessage(), e);
			}
		}
		return orderData;
	}

	/**
	 * @return : Returns selections for the featurecode
	 */
	@RequestMapping(value = "/validateSnopRequest", method = RequestMethod.GET)
	@ResponseBody
	public List<Map<String, Object>> validateSnopRequest() throws Exception
	{
		List<Map<String, Object>> resultMapList = new ArrayList();
		try
		{
			final CartData cartData = cartFacade.getSessionCart();
			if (null != cartData.getLaneType() && !LaneTypeEnum.LANE1.equals(cartData.getLaneType())
					&& cmsSiteService.getCurrentSite().isSnopRequired())
			{
				final CatSnopResponseMapper catSnopResponseMapper = catSnopFacade.validateSnopRequest();
				resultMapList = checkoutFacade.populateResponse(catSnopResponseMapper, cartData.getLaneType().toString());
			}
			else
			{
				final Map<String, Object> resultMap = new HashMap();
				resultMap.put("SUCCESS", Boolean.TRUE);
				resultMapList.add(resultMap);
			}


		}
		catch (final Exception e)
		{
			LOG.error("SNOP :: ERROR : " + e.getMessage(), e);
		}
		return resultMapList;
	}
}