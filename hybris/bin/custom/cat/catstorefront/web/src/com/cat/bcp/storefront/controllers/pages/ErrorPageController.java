/**
 *
 */
package com.cat.bcp.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.cat.bcp.storefront.controllers.ControllerConstants;


/**
 * @author prrungta
 *
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/error")
public class ErrorPageController extends AbstractPageController
{

	private static final String ERROR_CMS_PAGE = "notFound";

	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	/**
	 * Redirect to this method when any exception/error occurred.
	 *
	 * @param model
	 * @param response
	 * @return Error Page
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String exceptionOccured(@RequestParam(value = "errorCode", required = false) final int errorCode, final Model model,
			final HttpServletResponse response) throws CMSItemNotFoundException
	{
		switch (errorCode)
		{
			case HttpServletResponse.SC_BAD_REQUEST:
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				model.addAttribute("errorMessage", "Exception Occured");
				break;
			case HttpServletResponse.SC_NOT_FOUND:
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
				model.addAttribute("errorMessage", "Product Not Found");
				break;
			case HttpServletResponse.SC_INTERNAL_SERVER_ERROR:
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				model.addAttribute("errorMessage", "Internal Server Error");
				break;
			case HttpServletResponse.SC_SERVICE_UNAVAILABLE:
				response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
				model.addAttribute("errorMessage", "Service Unavailable");
				break;
			case HttpServletResponse.SC_HTTP_VERSION_NOT_SUPPORTED:
				response.setStatus(HttpServletResponse.SC_HTTP_VERSION_NOT_SUPPORTED);
				model.addAttribute("errorMessage", "HTTP VERSION NOT SUPPORTED");
				break;
			default:
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				model.addAttribute("errorMessage", "Error Occured");
				break;
		}

		storeCmsPageInModel(model, getContentPageForLabelOrId(ERROR_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(ERROR_CMS_PAGE));
		model.addAttribute(WebConstants.MODEL_KEY_ADDITIONAL_BREADCRUMB,
				resourceBreadcrumbBuilder.getBreadcrumbs("breadcrumb.not.found"));
		return ControllerConstants.Views.Pages.Error.ErrorPage;
	}
}
