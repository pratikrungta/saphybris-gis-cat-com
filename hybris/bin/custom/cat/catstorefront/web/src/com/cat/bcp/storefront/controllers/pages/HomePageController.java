/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.catalog.constants.GeneratedCatalogConstants.Enumerations.ArticleApprovalStatus;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartStatusForCurrentOrderWindow;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.enums.LaneTypeEnum;
import com.cat.bcp.core.enums.ProductConditionEnum;
import com.cat.bcp.core.event.RecentViewedItemsCollection;
import com.cat.bcp.core.model.RestockAlertModel;
import com.cat.bcp.core.servicelayer.CatOrderLaneProductService;
import com.cat.bcp.core.servicelayer.impl.CatOrderWindowCalculationUtility;
import com.cat.bcp.core.sso.util.CatSSOUtil;
import com.cat.bcp.core.util.RecentViewedCircularQueue;
import com.cat.bcp.facades.customer.CatCustomerFacade;
import com.cat.bcp.facades.order.CatCartFacade;
import com.cat.bcp.facades.order.CatReorderFacade;
import com.cat.bcp.facades.utility.CatCartDataErrorMap;
import com.cat.facades.order.CatErrorData;



/**
 * Controller for home page
 */
@Controller
@RequestMapping("/")
public class HomePageController extends AbstractPageController
{
	private static final Logger LOG = Logger.getLogger(HomePageController.class);
	private static final String US = "US";
	private static final String CA = "CA";


	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "userService")
	private UserService userService;


	@Resource(name = "sessionService")
	SessionService sessionService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;


	@Resource(name = "catSSOUtil")
	private CatSSOUtil catSSOUtil;

	@Resource(name = "accProductFacade")
	private ProductFacade productFacade;


	@Resource(name = "customerFacade")
	private CustomerFacade customerFacade;

	@Resource(name = "catCustomerFacade")
	private CatCustomerFacade catCustomerFacade;

	@Resource(name = "productConverter")
	private Converter<ProductModel, ProductData> productConverter;

	@Resource(name = "catOrderWindowCalculationUtility")
	private CatOrderWindowCalculationUtility catOrderWindowCalculationUtility;

	@Resource(name = "catOrderLaneProductService")
	private CatOrderLaneProductService catOrderLaneProductService;

	@Resource(name = "cartFacade")
	private CatCartFacade cartFacade;

	@Resource(name = "catReorderFacade")
	private CatReorderFacade catReorderFacade;

	private static final String VISITED_PRODUCTS_SESSION_KEY = "RECENT_VIEWED_ITEMS";
	private static final String VISITED_USED_PRODUCTS_SESSION_KEY = "RECENT_VIEWED_USED_ITEMS";
	private static final String VISITED_PRODUCTS_MODEL_KEY = "productCollection";
	private static final String VISITED_USED_PRODUCTS_MODEL_KEY = "usedProductCollection";
	public static final String DEFAULT_SEARCH_PAGE = "defaultSearchPage";
	private static final String NEW_PRODUCT_PLP = "newProductPLP";
	private static final String CURRENT_DEALER = "currentDealer";
	private static final String IS_SP = "isSP";
	private static final String IS_UTV_USER = "isUTVUser";
	private static final String IS_IM = "isIM";
	private static final String ORDER_WINDOW = "orderWindowValidation";
	private static final String COMPATIBLE_CHECK = "compatibilityValidation";
	private static final String STOCK_CHECK = "stockValidation";
	private static final String UNAPPROVED_CHECK = "unapprovedValidation";
	private static final String REORDERABLE_CHECK = "reorderableValidation";
	private static final String RESTOCK_CHECK = "restockValidation";

	private final int maxRecentViewedItems = Config.getInt("cat.maxRecentItems", 6);

	@RequestMapping(method = RequestMethod.GET)
	public String home(@RequestParam(value = "logout", defaultValue = "false") final boolean logout, final Model model,
			final RedirectAttributes redirectModel, final HttpServletRequest request, final HttpServletResponse response,
			final HttpSession session) throws CMSItemNotFoundException, ParseException
	{

		final UserModel um = userService.getCurrentUser();
		if (logout)
		{
			LOG.info("Signout user with userid =" + um.getUid());

			//Remove Session Attributes
			sessionService.removeAttribute(CURRENT_DEALER);
			sessionService.removeAttribute(IS_IM);
			sessionService.removeAttribute(IS_UTV_USER);
			sessionService.removeAttribute(IS_SP);
			sessionService.removeAttribute(DEFAULT_SEARCH_PAGE);
			sessionService.removeAttribute(VISITED_PRODUCTS_SESSION_KEY);
			sessionService.removeAttribute(VISITED_USED_PRODUCTS_SESSION_KEY);

			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER, "account.confirmation.signout.title");
			return null;
		}
		resetFBCcartData();
		setRoleRelatedData(model, session);
		if (!CatCoreConstants.UTV.equals(catCustomerFacade.getUserType()))
		{
			catCustomerFacade.removeUnauthorizedFavoriteAndRecentProducts();
		}

		final List<ProductData> favProdList = catCustomerFacade.getCustomerFavoriteProducts();

		if (CollectionUtils.isNotEmpty(favProdList))
		{
			final List<ProductData> newProdList = new ArrayList();
			final List<ProductData> usedProdList = new ArrayList();
			for (final ProductData favProd : favProdList)
			{
				addFavoriteProducts(newProdList, usedProdList, favProd);
			}
			model.addAttribute("newFavoriteList", newProdList);
			model.addAttribute("usedFavoriteList", usedProdList);
		}

		for (final ProductConditionEnum productCondition : ProductConditionEnum.values())
		{

			storeRecentViewedProductsInModel(model, favProdList, productCondition);

		}

		storeCmsPageInModel(model, getContentPageForLabelOrId(null));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(null));
		updatePageTitle(model, getContentPageForLabelOrId(null));
		return getViewForPage(model);
	}

	/**
	 *
	 */
	private void resetFBCcartData()
	{
		if (cartFacade.hasSessionCart())
		{
			final CartData cart = cartFacade.getSessionCart();
			boolean isConfigurable = false;


			for (final OrderEntryData oed : cart.getEntries())
			{
				if (oed.getConfigurable())
				{
					isConfigurable = true;
					break;
				}
			}
			if (isConfigurable)
			{
				cartFacade.updateCartForConfigurableProducts(null, 0, null);
			}
		}
	}

	/**
	 * This method is used to clear cart Based on order window.
	 */
	@ResponseBody
	@RequestMapping(value = "/getCartStatusForCurrentOrderWindow", method = RequestMethod.GET)
	public CartStatusForCurrentOrderWindow getCartStatusForCurrentOrderWindow()
	{
		final CartStatusForCurrentOrderWindow cartStatusForCurrentOrderWindow = new CartStatusForCurrentOrderWindow();

		final Map<LaneTypeEnum, String> laneValidationMap = cartFacade.getLaneValidationMap();

		final CatCartDataErrorMap isCartCleardMap = cartFacade.getCartValidationMap(laneValidationMap);
		final Map<String, List<OrderEntryData>> errorDataMap = isCartCleardMap.getErrorEntryMap();

		if (MapUtils.isNotEmpty(errorDataMap))
		{
			cartFacade.clearAndRecalculateCart();
			final List<CatErrorData> catErrorDataList = new ArrayList();
			final CatErrorData catErrorData = new CatErrorData();
			if (errorDataMap.containsKey(ORDER_WINDOW))
			{
				catErrorData.setErrorMessage(getMessageSource().getMessage(
						"cat.homepage.cart.orderWindow.status." + isCartCleardMap.getLaneType().getCode().toLowerCase(), null,
						getI18nService().getCurrentLocale()));
			}
			else if (errorDataMap.containsKey(COMPATIBLE_CHECK) || errorDataMap.containsKey(UNAPPROVED_CHECK)
					|| errorDataMap.containsKey(REORDERABLE_CHECK) || errorDataMap.containsKey(RESTOCK_CHECK))
			{
				catErrorData.setErrorMessage(getMessageSource().getMessage(
						"cat.homepage.cart.compatible.status." + isCartCleardMap.getLaneType().getCode().toLowerCase(), null,
						getI18nService().getCurrentLocale()));
			}
			else if (errorDataMap.containsKey(STOCK_CHECK))
			{
				catErrorData.setErrorMessage(getMessageSource().getMessage(
						"cat.homepage.cart.stock.status." + isCartCleardMap.getLaneType().getCode().toLowerCase(), null,
						getI18nService().getCurrentLocale()));
			}
			catErrorDataList.add(catErrorData);

			cartStatusForCurrentOrderWindow.setErrorData(catErrorDataList);
			cartStatusForCurrentOrderWindow.setIsCartCleared(Boolean.TRUE);
		}
		else
		{
			cartStatusForCurrentOrderWindow.setIsCartCleared(Boolean.FALSE);
		}
		return cartStatusForCurrentOrderWindow;
	}

	/**
	 * Adds fav product to the list
	 *
	 * @param newProdList
	 *           - new prod list
	 * @param usedProdList
	 *           - used prod list
	 * @param favProd
	 *           - The favorite product
	 */
	private void addFavoriteProducts(final List<ProductData> newProdList, final List<ProductData> usedProdList,
			final ProductData favProd)
	{
		if (null != favProd.getProductCondition())
		{
			if (ProductConditionEnum.NEW.equals(favProd.getProductCondition()))
			{
				newProdList.add(favProd);
			}
			else if (ProductConditionEnum.USED.equals(favProd.getProductCondition()))
			{
				usedProdList.add(favProd);
			}
		}

	}

	/**
	 * Method used to set user type and low stock alert .
	 *
	 * @param model
	 *           the model
	 * @param session
	 *           the session
	 */
	private void setRoleRelatedData(final Model model, final HttpSession session)
	{
		final String userType = catCustomerFacade.getUserType();
		final CustomerData customerData = customerFacade.getCurrentCustomer();
		if (null != customerData && null != customerData.getUnit())
		{
			session.setAttribute(CURRENT_DEALER, customerData.getUnit().getName());
			model.addAttribute(CURRENT_DEALER, customerData.getUnit());
		}

		if (CatCoreConstants.IM.equalsIgnoreCase(userType) || CatCoreConstants.UTV.equalsIgnoreCase(userType))
		{
			final B2BCustomerModel customerModel = catCustomerFacade.getCurrentDealer();
			final List<RestockAlertModel> alertEntries = catReorderFacade.getLowStockProducts(customerModel.getDefaultB2BUnit(), "",
					"");
			model.addAttribute("lowStockCount", alertEntries.size());
			model.addAttribute("orderingWindowMessage", catOrderWindowCalculationUtility.getOrderingWindowMessage());
			session.setAttribute(IS_IM, true);
		}

		if (null != session.getAttribute(DEFAULT_SEARCH_PAGE))
		{
			model.addAttribute(DEFAULT_SEARCH_PAGE, session.getAttribute(DEFAULT_SEARCH_PAGE));
			session.setAttribute(DEFAULT_SEARCH_PAGE, NEW_PRODUCT_PLP);
		}
		else
		{
			model.addAttribute(DEFAULT_SEARCH_PAGE, NEW_PRODUCT_PLP);
		}
		if (CatCoreConstants.UTV.equalsIgnoreCase(userType))
		{
			session.setAttribute(IS_UTV_USER, Boolean.TRUE);
		}

		if (CatCoreConstants.SP.equalsIgnoreCase(userType))
		{
			session.setAttribute(IS_SP, true);
		}
	}

	/**
	 * Method to store recently viewed used products in model by either: 1) Retrieving latest recently viewed new and
	 * used product details from session or 2) Retrieving recently viewed new and used product details from customer
	 * details.
	 *
	 * @param model
	 * @param favProdList
	 * @param productCondition
	 */
	private void storeRecentViewedProductsInModel(final Model model, final List<ProductData> favProdList,
			final ProductConditionEnum productCondition)
	{

		LOG.info("Start::Load recently viewed New and Used products On Home Page");

		String visitedProductSessionKey = null;
		String productCollectionModelKey = null;

		RecentViewedItemsCollection recentlyViewedItems = null;

		final UserModel userModel = userService.getCurrentUser();

		if (null != productCondition)
		{
			if (ProductConditionEnum.NEW.equals(productCondition))
			{
				visitedProductSessionKey = VISITED_PRODUCTS_SESSION_KEY;
				productCollectionModelKey = VISITED_PRODUCTS_MODEL_KEY;
				recentlyViewedItems = sessionService.getOrLoadAttribute(visitedProductSessionKey,
						() -> new RecentViewedItemsCollection(maxRecentViewedItems));
				loadRecentViewedData(recentlyViewedItems, userModel, visitedProductSessionKey, productCondition);
			}
			else if (ProductConditionEnum.USED.equals(productCondition))
			{
				visitedProductSessionKey = VISITED_USED_PRODUCTS_SESSION_KEY;
				productCollectionModelKey = VISITED_USED_PRODUCTS_MODEL_KEY;
				recentlyViewedItems = sessionService.getOrLoadAttribute(visitedProductSessionKey,
						() -> new RecentViewedItemsCollection(maxRecentViewedItems));
				loadRecentViewedData(recentlyViewedItems, userModel, visitedProductSessionKey, productCondition);
			}
		}
		sessionService.setAttribute(visitedProductSessionKey, recentlyViewedItems);
		model.addAttribute(productCollectionModelKey, sessionService.getAttribute(visitedProductSessionKey));
		if (null != recentlyViewedItems)
		{

			removeUnAvailableItems(recentlyViewedItems, model, visitedProductSessionKey, productCollectionModelKey);
			setProductAsFavourite(recentlyViewedItems, favProdList);
		}
		LOG.info("End::Load recently viewed New and Used products On Home Page");
	}

	/**
	 * @param recentlyViewedItems
	 * @param favProdList
	 */
	private void setProductAsFavourite(final RecentViewedItemsCollection recentlyViewedItems, final List<ProductData> favProdList)
	{
		for (int i = 0; i < favProdList.size(); i++)
		{
			for (final ProductData productData : recentlyViewedItems.getProductCodes())
			{
				if (productData.getCode().equalsIgnoreCase(favProdList.get(i).getCode()))
				{
					productData.setFavorite(Boolean.TRUE);
				}
			}
		}
	}

	/**
	 * @param recentlyViewedItems
	 * @param model
	 * @param visitedProductSessionKey
	 * @param productCollectionModelKey
	 */
	private void removeUnAvailableItems(final RecentViewedItemsCollection recentlyViewedItems, final Model model,
			final String visitedProductSessionKey, final String productCollectionModelKey)
	{
		final RecentViewedItemsCollection changedRecentlyViewedItems = new RecentViewedItemsCollection(maxRecentViewedItems);
		changedRecentlyViewedItems.setProductCondition(recentlyViewedItems.getProductCondition());
		changedRecentlyViewedItems.setCustomer(recentlyViewedItems.getCustomer());
		for (final ProductData productData : recentlyViewedItems.getProductCodes())
		{
			try
			{
				final ProductData reorderableProduct = productFacade.getProductForCodeAndOptions(productData.getCode(), null);
				if (BooleanUtils.isFalse(reorderableProduct.getIsReorderable()))
				{
					productData.setIsReorderable(Boolean.FALSE);
				}
				else
				{
					productData.setIsReorderable(Boolean.TRUE);
				}
				//only add approved products to recently viewed items
				changedRecentlyViewedItems.addProductCode(productData);
			}
			catch (final UnknownIdentifierException exception)
			{
				LOG.error(String.format("Product with code '%s' is not found! or not unique!", productData.getCode()), exception);
				continue;
			}
		}
		//set this to session and model attributes
		sessionService.setAttribute(visitedProductSessionKey, changedRecentlyViewedItems);
		model.addAttribute(visitedProductSessionKey, sessionService.getAttribute(productCollectionModelKey));
	}

	/**
	 * @param recentlyViewedItems
	 * @param userModel
	 * @param visitedProductSessionKey
	 * @param productCondition
	 */
	private void loadRecentViewedData(final RecentViewedItemsCollection recentlyViewedItems, final UserModel userModel,
			final String visitedProductSessionKey, final ProductConditionEnum productCondition)
	{
		if (userModel instanceof B2BCustomerModel && isCollectionEmpty(recentlyViewedItems))
		{

			final Collection<ProductModel> productSet = productCondition.equals(ProductConditionEnum.NEW)
					? getApprovedProducts(((B2BCustomerModel) userModel).getRecentlyViewedProduct())
					: getApprovedProducts(((B2BCustomerModel) userModel).getRecentlyViewedUsedProduct());
			loadRecentlyViewedItemDetails(recentlyViewedItems, productSet, visitedProductSessionKey, productCondition, userModel);
		}
	}

	/**
	 * This method is to return only approved products from recently viewed items.
	 *
	 * @param favoriteProducts
	 * @return Collection<ProductModel>
	 */
	private Collection<ProductModel> getApprovedProducts(final Collection<ProductModel> favoriteProducts)
	{
		Collection<ProductModel> products = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(favoriteProducts))
		{
			products = favoriteProducts.stream().filter(productModel -> ArticleApprovalStatus.APPROVED.toString()
					.equalsIgnoreCase(productModel.getApprovalStatus().getCode())).collect(Collectors.toList());
		}
		return products;
	}

	private boolean isCollectionEmpty(final RecentViewedItemsCollection collection)
	{
		return collection.getProductCodes().isEmpty() ? true : false;
	}

	@ResponseBody
	@RequestMapping(value = "/getFavorite", method = RequestMethod.GET)
	public SearchPageData getFavorites(final Model model)
	{
		final SearchPageData searchPageData = new SearchPageData();

		final B2BCustomerModel customerModel = catCustomerFacade.getCurrentDealer();
		final List<ProductData> favProdList = productConverter.convertAll(customerModel.getFavoriteProducts());
		searchPageData.setResults(favProdList);
		return searchPageData;
	}


	protected void updatePageTitle(final Model model, final AbstractPageModel cmsPage)
	{
		storeContentPageTitleInModel(model, getPageTitleResolver().resolveHomePageTitle(cmsPage.getTitle()));
	}

	/**
	 * Method to load recently viewed products for a customer on home page.
	 *
	 * @param recentlyViewedItems
	 * @param productSet
	 * @param visitedProductSessionKey
	 * @param productCondition
	 * @param userModel
	 */
	private void loadRecentlyViewedItemDetails(final RecentViewedItemsCollection recentlyViewedItems,
			final Collection<ProductModel> productSet, final String visitedProductSessionKey,
			final ProductConditionEnum productCondition, final UserModel userModel)
	{
		if (CollectionUtils.isNotEmpty(productSet))
		{
			final RecentViewedCircularQueue<ProductModel> recentProductCollection = new RecentViewedCircularQueue<>(
					productSet.size());
			recentProductCollection.addAll(productSet);
			final List<ProductOption> options = new ArrayList<>(Arrays.asList(ProductOption.BASIC, ProductOption.URL,
					ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION, ProductOption.GALLERY,
					ProductOption.CATEGORIES, ProductOption.REVIEW, ProductOption.PROMOTIONS, ProductOption.CLASSIFICATION,
					ProductOption.VARIANT_FULL, ProductOption.STOCK, ProductOption.VOLUME_PRICES, ProductOption.PRICE_RANGE,
					ProductOption.DELIVERY_MODE_AVAILABILITY, ProductOption.REFERENCES));
			recentProductCollection.forEach(product -> {
				final ProductData productData = productFacade.getProductForCodeAndOptions(product.getCode(), options);
				recentlyViewedItems.setCustomer((B2BCustomerModel) userModel);
				recentlyViewedItems.addProductCode(productData);
			});
		}

		recentlyViewedItems.setProductCondition(productCondition);
		sessionService.setAttribute(visitedProductSessionKey, recentlyViewedItems);
	}


	@RequestMapping(value = "/countries", method = RequestMethod.GET)
	@ResponseBody
	public List<CountryData> getCountries(final Model model, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		final List<CountryData> countries = new ArrayList<>();
		countries.add(i18NFacade.getCountryForIsocode(US));
		countries.add(i18NFacade.getCountryForIsocode(CA));
		return countries;
	}

	@RequestMapping(value = "/{countryIso}/regions", method = RequestMethod.GET)
	@ResponseBody
	public List<RegionData> getRegionsForCountry(@PathVariable("countryIso") final String countryIso, final Model model,
			final RedirectAttributes redirectModel) throws CMSItemNotFoundException
	{
		return i18NFacade.getRegionsForCountryIso(countryIso);
	}

}