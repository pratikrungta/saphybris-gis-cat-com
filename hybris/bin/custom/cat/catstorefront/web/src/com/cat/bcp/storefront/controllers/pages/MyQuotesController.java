/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.storefront.controllers.pages;

import de.hybris.platform.acceleratorfacades.ordergridform.OrderGridFormFacade;
import de.hybris.platform.acceleratorfacades.product.data.ReadOnlyOrderGridData;
import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.catalog.enums.ArticleApprovalStatus;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.comment.data.CommentData;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.quote.data.QuoteData;
import de.hybris.platform.commercefacades.voucher.VoucherFacade;
import de.hybris.platform.commerceservices.enums.QuoteAction;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.core.enums.QuoteState;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.util.Config;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cat.bcp.facades.customer.CatCustomerFacade;
import com.cat.bcp.facades.order.CatQuoteFacade;
import com.cat.bcp.storefront.controllers.ControllerConstants;


@Controller
@RequestMapping(value = "/my-account/my-quotes")
public class MyQuotesController extends AbstractSearchPageController
{
	private static final Logger LOG = Logger.getLogger(MyQuotesController.class);

	private static final String MY_QUOTES_CMS_PAGE = "my-quotes";
	private static final String QUOTE_DETAILS_CMS_PAGE = "quote-detail";
	private static final String REDIRECT_QUOTE_LIST_URL = REDIRECT_PREFIX + "/my-account/my-quotes/";
	private static final String PAGINATION_NUMBER_OF_COMMENTS = "quote.pagination.numberofcomments";
	private static final String ALLOWED_ACTIONS = "allowedActions";
	private static final String SYSTEM_ERROR_PAGE_NOT_FOUND = "system.error.page.not.found";
	private static final String QUOTE_CART_INSUFFICIENT_ACCESS_RIGHTS = "quote.cart.insufficient.access.rights.error";

	private static final String REPLICATED_QUOTE = "replicatedQuote";



	@Resource(name = "accountBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;

	@Resource(name = "quoteFacade")
	private CatQuoteFacade quoteFacade;

	@Resource(name = "voucherFacade")
	private VoucherFacade voucherFacade;

	@Resource(name = "orderGridFormFacade")
	private OrderGridFormFacade orderGridFormFacade;

	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	@Resource(name = "messageSource")
	private MessageSource messageSource;

	@Resource(name = "defaultCatCustomerFacade")
	private CatCustomerFacade defaultCatCustomerFacade;

	@Resource(name = "sessionService")
	SessionService sessionService;

	@ResponseBody
	@RequestMapping(method = RequestMethod.GET)
	public Map<String, Object> quotes(@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "search", required = false) final String searchCode,
			@RequestParam(value = "sort", defaultValue = "byDate") final String sortCode, final Model model)
			throws CMSItemNotFoundException
	{
		if (null != sessionService.getAttribute(REPLICATED_QUOTE))
		{
			sessionService.removeAttribute(REPLICATED_QUOTE);
		}
		final Map<String, Object> resultMap = new HashMap();
		final int numOfQuotesPerPage = Integer.parseInt(Config.getParameter("cat.quotes.per.page"));
		final PageableData pageableData = createPageableData(page, numOfQuotesPerPage, sortCode, showMode);
		pageableData.setSearch(searchCode);
		final Map<String, String> quoteStatusMap = new HashMap();
		resultMap.put("quoteList", getQuoteFacade().getPagedQuotes(pageableData));
		quoteStatusMap.put("BUYER_OFFER",
				messageSource.getMessage("cat.quote.status.code.BUYER_OFFER", null, getI18nService().getCurrentLocale()));
		quoteStatusMap.put("SELLERAPPROVER_PENDING",
				messageSource.getMessage("cat.quote.status.code.SELLERAPPROVER_PENDING", null, getI18nService().getCurrentLocale()));
		quoteStatusMap.put("SELLERAPPROVER_APPROVED",
				messageSource.getMessage("cat.quote.status.code.SELLERAPPROVER_APPROVED", null, getI18nService().getCurrentLocale()));
		quoteStatusMap.put("SELLERAPPROVER_REJECTED",
				messageSource.getMessage("cat.quote.status.code.SELLERAPPROVER_REJECTED", null, getI18nService().getCurrentLocale()));
		quoteStatusMap.put("BUYER_ACCEPTED",
				messageSource.getMessage("cat.quote.status.code.BUYER_ACCEPTED", null, getI18nService().getCurrentLocale()));
		quoteStatusMap.put("BUYER_REJECTED",
				messageSource.getMessage("cat.quote.status.code.BUYER_REJECTED", null, getI18nService().getCurrentLocale()));
		resultMap.put("quoteStatusMetaData", quoteStatusMap);
		return resultMap;
	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public String quoteHistoryView(final Model model) throws CMSItemNotFoundException
	{
		final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
		breadcrumbs.add(new Breadcrumb("/my-account/my-quotes",
				getMessageSource().getMessage("text.account.manageQuotes.breadcrumb", null, getI18nService().getCurrentLocale()),
				null));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);
		storeCmsPageInModel(model, getContentPageForLabelOrId(MY_QUOTES_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MY_QUOTES_CMS_PAGE));
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
		return getViewForPage(model);
	}

	@RequestMapping(value = "/{quoteCode}", method = RequestMethod.GET)
	public String quote(final Model model, final RedirectAttributes redirectModel,
			@PathVariable("quoteCode") final String quoteCode) throws CMSItemNotFoundException
	{
		final String replicatedQuoteCode = sessionService.getAttribute(REPLICATED_QUOTE);
		final String parentQuote = replicatedQuoteCode != null ? replicatedQuoteCode : quoteCode;
		sessionService.removeAttribute(REPLICATED_QUOTE);
		try
		{
			final QuoteData quoteData = getQuoteFacade().getQuoteForCode(parentQuote);


			model.addAttribute("quoteData", quoteData);
			final BigDecimal totalListPrice = quoteData.getTotalPrice().getValue().add(quoteData.getTotalDiscounts().getValue());
			model.addAttribute("totalListPrice", getFormattedPriceValue(totalListPrice));
			enableEditButton(quoteData, model);

			loadCommentsShown(model);
			sortComments(quoteData);

			storeCmsPageInModel(model, getContentPageForLabelOrId(QUOTE_DETAILS_CMS_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(QUOTE_DETAILS_CMS_PAGE));

			setAllowedActions(model, parentQuote);

			final List<Breadcrumb> breadcrumbs = accountBreadcrumbBuilder.getBreadcrumbs(null);
			breadcrumbs.add(new Breadcrumb("/my-account/my-quotes/view",
					getMessageSource().getMessage("text.account.manageQuotes.breadcrumb", null, getI18nService().getCurrentLocale()),
					null));
			breadcrumbs.add(new Breadcrumb("/" + urlEncode(parentQuote) + "/",
					getMessageSource().getMessage("breadcrumb.quote.view", new Object[]
					{ parentQuote }, "Quote {0}", getI18nService().getCurrentLocale()), null));
			model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);
			model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);
			model.addAttribute("warehouseName", defaultCatCustomerFacade.getDealerWarehouseName());
			return getViewForPage(model);
		}
		catch (final UnknownIdentifierException e)
		{
			LOG.warn("Unable to load cart for quote " + parentQuote, e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, SYSTEM_ERROR_PAGE_NOT_FOUND, null);
			return REDIRECT_QUOTE_LIST_URL;
		}
		catch (final ModelNotFoundException e)
		{
			LOG.warn("Attempted to load a quote that does not exist or is not accessible by current user:" + parentQuote, e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					QUOTE_CART_INSUFFICIENT_ACCESS_RIGHTS, new Object[]
					{ getCartFacade().getSessionCart().getCode() });
			return REDIRECT_PREFIX + ROOT;
		}

	}

	/**
	 * Get formatted monetary value with currency symbol
	 *
	 * @param value
	 *           the value to be formatted
	 *
	 * @return formatted threshold string
	 */
	protected String getFormattedPriceValue(final BigDecimal value)
	{
		return priceDataFactory.create(PriceDataType.BUY, value, getCurrentCurrency().getIsocode()).getFormattedValue();
	}

	/**
	 * Determines if Edit Quote Button should be enabled or disabled.
	 *
	 * @param quoteData
	 * @return true or false
	 */
	private void enableEditButton(final QuoteData quoteData, final Model model)
	{
		final List<OrderEntryData> configVariantList = quoteData.getEntries().stream()
				.filter(entry -> !entry.getMandatoryProduct().booleanValue() && StringUtils.isBlank(entry.getConfigVariantId()))
				.collect(Collectors.toList());
		final Optional<OrderEntryData> unAppovedConfigVariant = configVariantList.stream()
				.filter(orderEntry -> orderEntry.getProduct().getApprovalStatus().equals(ArticleApprovalStatus.UNAPPROVED)).findAny();
		if (QuoteState.BUYER_ACCEPTED.equals(quoteData.getState()) || unAppovedConfigVariant.isPresent()
				|| QuoteState.BUYER_REJECTED.equals(quoteData.getState()))
		{
			model.addAttribute("enableEditButton", Boolean.FALSE);
		}
		else
		{
			model.addAttribute("enableEditButton", Boolean.TRUE);
		}
		if (unAppovedConfigVariant.isPresent())
		{
			model.addAttribute("isQuoteHasUnapprovedProducts", Boolean.TRUE);
		}
		else
		{
			model.addAttribute("isQuoteHasUnapprovedProducts", Boolean.FALSE);
		}
		final Optional<OrderEntryData> appovedConfigVariant = configVariantList.stream()
				.filter(orderEntry -> orderEntry.getProduct().getApprovalStatus().equals(ArticleApprovalStatus.APPROVED)).findAny();
		if (appovedConfigVariant.isPresent())
		{
			model.addAttribute("enableReplicateButton", Boolean.TRUE);
		}
		else if (!appovedConfigVariant.isPresent() && unAppovedConfigVariant.isPresent())
		{
			model.addAttribute("enableReplicateButton", Boolean.FALSE);
		}
		if (unAppovedConfigVariant.isPresent() && appovedConfigVariant.isPresent())
		{
			model.addAttribute("isQuoteContainsUnpprovedProducts", Boolean.TRUE);
		}
		else
		{
			model.addAttribute("isQuoteContainsUnpprovedProducts", Boolean.FALSE);

		}

	}

	@RequestMapping(value = "/{quoteCode}/getReadOnlyProductVariantMatrix", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getProductVariantMatrixForResponsive(@PathVariable("quoteCode") final String quoteCode,
			@RequestParam("productCode") final String productCode, final Model model)
	{
		final QuoteData quoteData = getQuoteFacade().getQuoteForCode(quoteCode);
		final Map<String, ReadOnlyOrderGridData> readOnlyMultiDMap = orderGridFormFacade.getReadOnlyOrderGridForProductInOrder(
				productCode, Arrays.asList(ProductOption.BASIC, ProductOption.CATEGORIES), quoteData);
		model.addAttribute("readOnlyMultiDMap", readOnlyMultiDMap);

		return ControllerConstants.Views.Fragments.Checkout.ReadOnlyExpandedOrderForm;
	}

	protected void sortComments(final AbstractOrderData orderData)
	{
		if (orderData != null)
		{
			if (CollectionUtils.isNotEmpty(orderData.getComments()))
			{
				final List<CommentData> sortedComments = orderData.getComments().stream()
						.sorted((comment1, comment2) -> comment2.getCreationDate().compareTo(comment1.getCreationDate()))
						.collect(Collectors.toList());
				orderData.setComments(sortedComments);
			}

			if (CollectionUtils.isNotEmpty(orderData.getEntries()))
			{
				setSortedComments(orderData);
			}
		}
	}

	/**
	 * @param orderData
	 */
	private void setSortedComments(final AbstractOrderData orderData)
	{
		for (final OrderEntryData orderEntry : orderData.getEntries())
		{
			if (CollectionUtils.isNotEmpty(orderEntry.getComments()))
			{
				final List<CommentData> sortedEntryComments = orderEntry.getComments().stream()
						.sorted((comment1, comment2) -> comment2.getCreationDate().compareTo(comment1.getCreationDate()))
						.collect(Collectors.toList());

				orderEntry.setComments(sortedEntryComments);
			}
			else if (orderEntry.getProduct() != null && orderEntry.getProduct().getMultidimensional() != null
					&& orderEntry.getProduct().getMultidimensional())
			{
				setMultiDOrderComments(orderEntry);

			}
		}

	}

	/**
	 * @param orderEntry
	 */
	private void setMultiDOrderComments(final OrderEntryData orderEntry)
	{
		if (CollectionUtils.isNotEmpty(orderEntry.getEntries()))
		{
			for (final OrderEntryData multiDOrderEntry : orderEntry.getEntries())
			{
				if (CollectionUtils.isNotEmpty(multiDOrderEntry.getComments()))
				{
					final List<CommentData> sortedMultiDOrderEntryComments = multiDOrderEntry.getComments().stream()
							.sorted((comment1, comment2) -> comment2.getCreationDate().compareTo(comment1.getCreationDate()))
							.collect(Collectors.toList());

					multiDOrderEntry.setComments(sortedMultiDOrderEntryComments);
				}
			}
		}

	}

	protected void loadCommentsShown(final Model model)
	{
		final int commentsShown = getSiteConfigService().getInt(PAGINATION_NUMBER_OF_COMMENTS, 5);
		model.addAttribute("commentsShown", Integer.valueOf(commentsShown));
	}

	/**
	 * Set allowed actions for a given quote on model.
	 *
	 * @param model
	 *           the MVC model
	 * @param quoteCode
	 *           the quote to be checked.
	 */
	protected void setAllowedActions(final Model model, final String quoteCode)
	{
		final Set<QuoteAction> actionSet = getQuoteFacade().getAllowedActions(quoteCode);

		if (actionSet != null)
		{
			final Map<String, Boolean> actionsMap = actionSet.stream()
					.collect(Collectors.toMap((v) -> v.getCode(), (v) -> Boolean.TRUE));
			model.addAttribute(ALLOWED_ACTIONS, actionsMap);
		}
	}

	protected CatQuoteFacade getQuoteFacade()
	{
		return quoteFacade;
	}

	public VoucherFacade getVoucherFacade()
	{
		return voucherFacade;
	}

	protected CartFacade getCartFacade()
	{
		return cartFacade;
	}
}
