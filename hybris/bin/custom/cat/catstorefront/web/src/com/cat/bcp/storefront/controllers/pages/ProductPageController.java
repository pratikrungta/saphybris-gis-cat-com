/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.storefront.controllers.pages;

import de.hybris.platform.acceleratorfacades.futurestock.FutureStockFacade;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ProductBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.FutureStockForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.ReviewForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.ReviewValidator;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.acceleratorstorefrontcommons.util.XSSFilterUtil;
import de.hybris.platform.acceleratorstorefrontcommons.variants.VariantSortStrategy;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.catalog.enums.ProductReferenceTypeEnum;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.AbstractPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.BaseOptionData;
import de.hybris.platform.commercefacades.product.data.CategoryData;
import de.hybris.platform.commercefacades.product.data.ClassificationData;
import de.hybris.platform.commercefacades.product.data.FeatureData;
import de.hybris.platform.commercefacades.product.data.FeatureValueData;
import de.hybris.platform.commercefacades.product.data.FutureStockData;
import de.hybris.platform.commercefacades.product.data.ImageData;
import de.hybris.platform.commercefacades.product.data.ImageDataType;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.product.data.ProductReferenceData;
import de.hybris.platform.commercefacades.product.data.ReviewData;
import de.hybris.platform.commercefacades.product.data.StockData;
import de.hybris.platform.commerceservices.url.UrlResolver;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.enums.ProductConditionEnum;
import com.cat.bcp.core.event.RecentViewedItemsCollection;
import com.cat.bcp.core.event.RecentViewedProductEvent;
import com.cat.bcp.core.model.AlertProductModel;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.RestockAlertModel;
import com.cat.bcp.core.model.ShipToAfterDateModel;
import com.cat.bcp.core.servicelayer.CatProductService;
import com.cat.bcp.facade.product.CatProductFacade;
import com.cat.bcp.facade.product.CatStockFacade;
import com.cat.bcp.facades.comparator.CatShipToAfterDateComparator;
import com.cat.bcp.facades.customer.CatCustomerFacade;
import com.cat.bcp.facades.order.CatCartFacade;
import com.cat.bcp.facades.search.DealerSearchFacade;
import com.cat.bcp.storefront.controllers.ControllerConstants;
import com.cat.facades.order.CatLaneSpecificDataForPDP;
import com.cat.facades.product.InventoryAgeData;
import com.cat.facades.product.filter.PLPFilterData;
import com.google.common.collect.Maps;


/**
 * Controller for product details page
 */
@Controller
@RequestMapping(value = "/**/p")
public class ProductPageController extends AbstractPageController
{

	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains one or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on the
	 * issue and future resolution.
	 */
	private static final Logger LOG = Logger.getLogger(ProductPageController.class);
	private static final String PRODUCT_CODE_PATH_VARIABLE_PATTERN = "/{productCode:.*}";
	private static final String REVIEWS_PATH_VARIABLE_PATTERN = "{numberOfReviews:.*}";

	private static final String FUTURE_STOCK_ENABLED = "storefront.products.futurestock.enabled";
	private static final String STOCK_SERVICE_UNAVAILABLE = "basket.page.viewFuture.unavailable";
	private static final String NOT_MULTISKU_ITEM_ERROR = "basket.page.viewFuture.not.multisku";
	private static final String VISITED_PRODUCTS_SESSION_KEY = "RECENT_VIEWED_ITEMS";
	private static final String VISITED_USED_PRODUCTS_SESSION_KEY = "RECENT_VIEWED_USED_ITEMS";
	private static final String PRIMARY_OFFERING_ID = "primaryOfferingId";
	private static final String SALES_OFFERING_ID = "salesModelId";
	private static final String PDC_FLAG = "pdcFlag";
	private static final String MFU_FLAG = "mfuFlag";
	private static final String WAREHOUSE_FLAG = "wsFlag";
	private static final String PAGE = "page";
	private static final String BRANCHLOCATION = "branchLocation";
	private static final String MANUFACTURINGYEAR_MIN = "manufacturingYearMin";
	private static final String MANUFACTURINGYEAR_MAX = "manufacturingYearMax";
	private static final String HOUSEUSED_MIN = "hoursUsedMin";
	private static final String HOUSEUSED_MAX = "hoursUsedMax";
	private static final String PRICE_MIN = "priceMin";
	private static final String PRICE_MAX = "priceMax";
	private static final String DEALERCERTIFIED = "dealerCertified";
	private static final String ALLFILTERPARAMETER = "allFilterParameters";
	private static final String CATEGORY = "category";
	private static final String DEFAULTFACETVALUE = "&defaultFacetValue=false";
	private static final String BACKWARDNAVIGATIONFLAG = "&backwardNavigationFlag=true";
	private static final String NULL = "null";
	private static final String DEFAULT_SEARCH_PAGE = "defaultSearchPage";
	public static final String PART = "PART";
	public static final String ATTACHMENT = "ATTACHMENT";
	private static final String SHIP_TO_AFTER_DATE = "shipToAfterDates";
	private static final String REDIRECT_URL = REDIRECT_PREFIX + "/error";
	private static final String LANETYPE = "laneType";

	@Resource(name = "productDataUrlResolver")
	private UrlResolver<ProductData> productDataUrlResolver;

	@Resource(name = "productVariantFacade")
	private ProductFacade productFacade;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "productBreadcrumbBuilder")
	private ProductBreadcrumbBuilder productBreadcrumbBuilder;

	@Resource(name = "cmsPageService")
	private CMSPageService cmsPageService;

	@Resource(name = "variantSortStrategy")
	private VariantSortStrategy variantSortStrategy;

	@Resource(name = "reviewValidator")
	private ReviewValidator reviewValidator;

	@Resource(name = "futureStockFacade")
	private FutureStockFacade futureStockFacade;

	@Resource(name = "catStockFacade")
	private CatStockFacade catStockFacade;

	@Resource(name = "eventService")
	private EventService eventService;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	@Resource(name = "customerFacade")
	private CustomerFacade customerFacade;

	@Resource(name = "catProductService")
	private CatProductService catProductService;

	@Resource(name = "catCustomerFacade")
	private CatCustomerFacade catCustomerFacade;

	@Resource(name = "cartFacade")
	private CatCartFacade cartFacade;

	@Resource(name = "dealerSearchFacade")
	private DealerSearchFacade dealerSearchFacade;

	@Resource(name = "catProductFacade")
	private CatProductFacade catProductFacade;

	private final int maxRecentViewedItems = Config.getInt("cat.maxRecentItems", 6);

	/**
	 * This method returns view for PDP page.
	 *
	 * @param productCode
	 *           product Code
	 * @param model
	 *           Model Object
	 * @param plpFilterData
	 *           PLPFilterData Object
	 * @param request
	 *           HttpServletRequest Object
	 * @param response
	 *           HttpServletResponse Object
	 * @param searchKeywordCaptured
	 *           searchKeywordCaptured
	 * @return View For Product Details Page
	 * @throws CMSItemNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN, method =
	{ RequestMethod.GET, RequestMethod.POST })
	public String productDetail(@PathVariable("productCode") final String productCode, final Model model,
			final PLPFilterData plpFilterData, final HttpServletRequest request, final HttpServletResponse response,
			@RequestParam(value = "searchKeywordCaptured", required = false) final String searchKeywordCaptured)
			throws CMSItemNotFoundException, UnsupportedEncodingException
	{
		final List<ProductOption> extraOptions = Arrays.asList(ProductOption.VARIANT_MATRIX_BASE, ProductOption.VARIANT_MATRIX_URL,
				ProductOption.VARIANT_MATRIX_MEDIA);
		final ProductModel productModel = productService.getProductForCode(productCode);

		final boolean isUtv = catProductService.isUtvProduct(productModel);
		final ProductData productData = productFacade.getProductForCodeAndOptions(productCode, extraOptions);

		model.addAttribute("utilityProducts", isUtv);
		final Map<String, Object> cartType = cartFacade.getCartType("");
		model.addAttribute(CatCoreConstants.CART_TYPE, cartType.get(CatCoreConstants.CART_PRODUCT_TYPE));
		model.addAttribute(CatCoreConstants.IS_UTV_CART, cartType.get(CatCoreConstants.IS_UTV_CART));

		final String redirection = checkRequestUrl(request, response, productDataUrlResolver.resolve(productData));
		if (StringUtils.isNotEmpty(redirection))
		{
			return redirection;
		}

		final String userType = catCustomerFacade.getUserType();
		model.addAttribute("userType", userType);
		final B2BCustomerModel user = (B2BCustomerModel) userService.getCurrentUser();
		try
		{
			if (userType.equalsIgnoreCase(CatCoreConstants.IM) || userType.equalsIgnoreCase(CatCoreConstants.UTV))
			{
				model.addAttribute("isInventoryManager", Boolean.TRUE);
				final ProductData baseProductData = productFacade.getProductForCodeAndOptions(productData.getBaseProduct(),
						extraOptions);

				final String alertId = baseProductData.getBaseProduct() + "_" + user.getDefaultB2BUnit().getUid();
				if (!isUtv)
				{
					getRestockAlertForProduct(model, alertId, productModel.getCode());
				}
				if (userType.equalsIgnoreCase(CatCoreConstants.UTV))
				{
					populateOrderWindows(model, productModel);
				}
				model.addAttribute("stock", new RestockAlertModel());

			}
			updatePageTitle(model);
			final Map<String, Object> map = new HashMap();
			map.put(PRIMARY_OFFERING_ID, plpFilterData.getPrimaryOfferingId());
			map.put(SALES_OFFERING_ID, plpFilterData.getSalesModelId());
			map.put(PDC_FLAG, plpFilterData.getPdcFlag());
			map.put(MFU_FLAG, plpFilterData.getMfuFlag());
			map.put(WAREHOUSE_FLAG, plpFilterData.getWsFlag());
			map.put(PAGE, plpFilterData.getPage());
			map.put(BRANCHLOCATION, plpFilterData.getBranchLocation());
			map.put(MANUFACTURINGYEAR_MIN, plpFilterData.getManufacturingYearMin());
			map.put(MANUFACTURINGYEAR_MAX, plpFilterData.getManufacturingYearMax());
			map.put(HOUSEUSED_MIN, plpFilterData.getHoursUsedMin());
			map.put(HOUSEUSED_MAX, plpFilterData.getHoursUsedMax());
			map.put(PRICE_MIN, plpFilterData.getPriceMin());
			map.put(PRICE_MAX, plpFilterData.getPriceMax());
			map.put(DEALERCERTIFIED, plpFilterData.getDealerCertified());
			map.put(CATEGORY, plpFilterData.getCategory());
			map.put(LANETYPE, StringUtils.isNotEmpty(plpFilterData.getLaneType()) ? plpFilterData.getLaneType() : "all");

			model.addAttribute(ALLFILTERPARAMETER, map);
			model.addAttribute(LANETYPE, StringUtils.isNotEmpty(plpFilterData.getLaneType()) ? plpFilterData.getLaneType() : "all");
			populateProductDetailForDisplay(productCode, model, request, extraOptions);
		}
		catch (final UnknownIdentifierException e)
		{
			LOG.error("::::::::::Unapproved Product::::::::", e);
			return REDIRECT_URL + "?errorCode=404";
		}



		model.addAttribute(new ReviewForm());
		model.addAttribute("pageType", PageType.PRODUCT.name());
		model.addAttribute("dealerCode", user.getDefaultB2BUnit().getUid());
		model.addAttribute("futureStockEnabled", Boolean.valueOf(Config.getBoolean(FUTURE_STOCK_ENABLED, false)));

		final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(productData.getKeywords());
		final String metaDescription = MetaSanitizerUtil.sanitizeDescription(productData.getDescription());
		setUpMetaData(model, metaKeywords, metaDescription);
		return getViewForPage(model);
	}

	/**
	 * This method is used to populate Order windows.
	 *
	 * @param model
	 *           model
	 * @param productModel
	 *           productModel
	 */
	private void populateOrderWindows(final Model model, final ProductModel productModel)
	{
		LOG.debug("ProductPageCotroller -> productDetail():Start Fetching Order Window");
		final Map orderWindows = catStockFacade.getOrderWindowAndShipAfterDates(Arrays.asList(productModel));
		final List<ShipToAfterDateModel> shipToAfterList = (List<ShipToAfterDateModel>) orderWindows.get(SHIP_TO_AFTER_DATE);
		if (CollectionUtils.isNotEmpty(shipToAfterList))
		{
			Collections.sort(shipToAfterList, new CatShipToAfterDateComparator());
			orderWindows.remove(SHIP_TO_AFTER_DATE);
			orderWindows.put(SHIP_TO_AFTER_DATE, shipToAfterList);
		}
		model.addAttribute("orderWindows", orderWindows);
		LOG.debug("ProductPageCotroller -> productDetail():Done Fetching Order Window");
	}

	/**
	 * This method is used to get Lane Specific Data for PDP
	 *
	 * @param productCode
	 *           configuration productCode
	 * @param salesModel
	 *           sales Model code
	 * @return CatLaneSpecificData Object
	 */
	@ResponseBody
	@RequestMapping(value = "/getLaneSpecificData", method = RequestMethod.GET)
	public CatLaneSpecificDataForPDP getLaneSpecificData(@RequestParam("productCode") final String productCode,
			@RequestParam("salesModel") final String salesModel)
	{
		final String userType = catCustomerFacade.getUserType();
		if (userType.equalsIgnoreCase(CatCoreConstants.IM) || userType.equalsIgnoreCase(CatCoreConstants.UTV))
		{
			return catProductFacade.getLaneSpecificData(productCode, salesModel);
		}
		return new CatLaneSpecificDataForPDP();
	}

	/**
	 * @param model
	 * @param alertId
	 * @param productCode
	 *
	 */
	private void getRestockAlertForProduct(final Model model, final String alertId, final String productCode)
	{
		try
		{
			final RestockAlertModel restockAlert = catStockFacade.getStockForSalesModel(alertId);
			if (null != restockAlert && StringUtils.isNotBlank(productCode))
			{
				final List<AlertProductModel> alertProductsList = dealerSearchFacade
						.getAssociatedConfigProducts(restockAlert.getAlertId());
				final Optional<AlertProductModel> restockAlertProduct = alertProductsList.stream()
						.filter(alertProduct -> alertProduct.getProduct().getCode().equalsIgnoreCase(productCode)).findAny();
				if (restockAlertProduct.isPresent())
				{
					model.addAttribute("currentStockLevel", restockAlert.getCurrentStockLevel());
					model.addAttribute("minStockLevel", restockAlert.getMinimumStockLevel());
					model.addAttribute("maxStockLevel", restockAlert.getMaximumStockLevel());
					model.addAttribute("recommendedStockLevel", restockAlert.getRecommendedStockLevel());
				}
			}
		}
		catch (final Exception e)
		{
			LOG.error("No Alert found for this product" + e.getMessage(), e);
		}

	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/orderForm", method = RequestMethod.GET)
	public String productOrderForm(@PathVariable("productCode") final String productCode, final Model model,
			final HttpServletRequest request) throws CMSItemNotFoundException
	{
		final List<ProductOption> extraOptions = Arrays.asList(ProductOption.VARIANT_MATRIX_BASE,
				ProductOption.VARIANT_MATRIX_PRICE, ProductOption.VARIANT_MATRIX_MEDIA, ProductOption.VARIANT_MATRIX_STOCK,
				ProductOption.URL);

		final ProductData productData = productFacade.getProductForCodeAndOptions(productCode, extraOptions);
		updatePageTitle(productCode, model);

		populateProductDetailForDisplay(productCode, model, request, extraOptions);

		if (!model.containsAttribute(WebConstants.MULTI_DIMENSIONAL_PRODUCT))
		{
			return REDIRECT_PREFIX + productDataUrlResolver.resolve(productData);
		}

		return ControllerConstants.Views.Pages.Product.OrderForm;
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/zoomImages", method = RequestMethod.GET)
	public String showZoomImages(@PathVariable("productCode") final String productCode,
			@RequestParam(value = "galleryPosition", required = false) final String galleryPosition, final Model model)
	{
		final ProductData productData = productFacade.getProductForCodeAndOptions(productCode,
				Collections.singleton(ProductOption.GALLERY));
		final List<Map<String, ImageData>> images = getGalleryImages(productData);
		populateProductData(productData, model);
		if (galleryPosition != null)
		{
			try
			{
				model.addAttribute("zoomImageUrl", images.get(Integer.parseInt(galleryPosition)).get("zoom").getUrl());
			}
			catch (final IndexOutOfBoundsException | NumberFormatException ex)
			{
				if (LOG.isDebugEnabled())
				{
					LOG.debug(ex);
				}
				model.addAttribute("zoomImageUrl", "");
			}
		}
		return ControllerConstants.Views.Fragments.Product.ZoomImagesPopup;
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/quickView", method = RequestMethod.GET)
	public String showQuickView(@PathVariable("productCode") final String productCode, final Model model,
			final HttpServletRequest request)
	{
		final ProductModel productModel = productService.getProductForCode(productCode);
		final ProductData productData = productFacade.getProductForCodeAndOptions(productCode,
				Arrays.asList(ProductOption.BASIC, ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION,
						ProductOption.CATEGORIES, ProductOption.PROMOTIONS, ProductOption.STOCK, ProductOption.REVIEW,
						ProductOption.VARIANT_FULL, ProductOption.DELIVERY_MODE_AVAILABILITY));

		sortVariantOptionData(productData);
		populateProductData(productData, model);
		getRequestContextData(request).setProduct(productModel);

		return ControllerConstants.Views.Fragments.Product.QuickViewPopup;
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/review", method =
	{ RequestMethod.GET, RequestMethod.POST })
	public String postReview(@PathVariable final String productCode, final ReviewForm form, final BindingResult result,
			final Model model, final HttpServletRequest request, final RedirectAttributes redirectAttrs)
			throws CMSItemNotFoundException
	{
		getReviewValidator().validate(form, result);

		final ProductData productData = productFacade.getProductForCodeAndOptions(productCode, null);
		if (result.hasErrors())
		{
			updatePageTitle(productCode, model);
			GlobalMessages.addErrorMessage(model, "review.general.error");
			model.addAttribute("showReviewForm", Boolean.TRUE);
			populateProductDetailForDisplay(productCode, model, request, Collections.emptyList());
			storeCmsPageInModel(model, getPageForProduct(productCode));
			return getViewForPage(model);
		}

		final ReviewData review = new ReviewData();
		review.setHeadline(XSSFilterUtil.filter(form.getHeadline()));
		review.setComment(XSSFilterUtil.filter(form.getComment()));
		review.setRating(form.getRating());
		review.setAlias(XSSFilterUtil.filter(form.getAlias()));
		productFacade.postReview(productCode, review);
		GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.CONF_MESSAGES_HOLDER, "review.confirmation.thank.you.title");

		return REDIRECT_PREFIX + productDataUrlResolver.resolve(productData);
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/reviewhtml/"
			+ REVIEWS_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public String reviewHtml(@PathVariable("productCode") final String productCode,
			@PathVariable("numberOfReviews") final String numberOfReviews, final Model model, final HttpServletRequest request)
	{
		final ProductModel productModel = productService.getProductForCode(productCode);
		final List<ReviewData> reviews;
		final ProductData productData = productFacade.getProductForCodeAndOptions(productCode,
				Arrays.asList(ProductOption.BASIC, ProductOption.REVIEW));

		if ("all".equals(numberOfReviews))
		{
			reviews = productFacade.getReviews(productCode);
		}
		else
		{
			final int reviewCount = Math.min(Integer.parseInt(numberOfReviews),
					productData.getNumberOfReviews() == null ? 0 : productData.getNumberOfReviews().intValue());
			reviews = productFacade.getReviews(productCode, Integer.valueOf(reviewCount));
		}

		getRequestContextData(request).setProduct(productModel);
		model.addAttribute("reviews", reviews);
		model.addAttribute("reviewsTotal", productData.getNumberOfReviews());
		model.addAttribute(new ReviewForm());

		return ControllerConstants.Views.Fragments.Product.ReviewsTab;
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/writeReview", method = RequestMethod.GET)
	public String writeReview(@PathVariable final String productCode, final Model model) throws CMSItemNotFoundException
	{
		model.addAttribute(new ReviewForm());
		setUpReviewPage(model, productCode);
		return ControllerConstants.Views.Pages.Product.WriteReview;
	}

	protected void setUpReviewPage(final Model model, final String productCode) throws CMSItemNotFoundException
	{
		final ProductData productData = productFacade.getProductForCodeAndOptions(productCode, null);
		final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(productData.getKeywords());
		final String metaDescription = MetaSanitizerUtil.sanitizeDescription(productData.getDescription());
		setUpMetaData(model, metaKeywords, metaDescription);
		storeCmsPageInModel(model, getPageForProduct(productCode));
		model.addAttribute("product", productFacade.getProductForCodeAndOptions(productCode, Arrays.asList(ProductOption.BASIC)));
		updatePageTitle(productCode, model);
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/writeReview", method = RequestMethod.POST)
	public String writeReview(@PathVariable final String productCode, final ReviewForm form, final BindingResult result,
			final Model model, final HttpServletRequest request, final RedirectAttributes redirectAttrs)
			throws CMSItemNotFoundException
	{
		getReviewValidator().validate(form, result);

		final ProductData productData = productFacade.getProductForCodeAndOptions(productCode, null);

		if (result.hasErrors())
		{
			GlobalMessages.addErrorMessage(model, "review.general.error");
			populateProductDetailForDisplay(productCode, model, request, Collections.emptyList());
			setUpReviewPage(model, productCode);
			return ControllerConstants.Views.Pages.Product.WriteReview;
		}

		final ReviewData review = new ReviewData();
		review.setHeadline(XSSFilterUtil.filter(form.getHeadline()));
		review.setComment(XSSFilterUtil.filter(form.getComment()));
		review.setRating(form.getRating());
		review.setAlias(XSSFilterUtil.filter(form.getAlias()));
		productFacade.postReview(productCode, review);
		GlobalMessages.addFlashMessage(redirectAttrs, GlobalMessages.CONF_MESSAGES_HOLDER, "review.confirmation.thank.you.title");

		return REDIRECT_PREFIX + productDataUrlResolver.resolve(productData);
	}

	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/futureStock", method = RequestMethod.GET)
	public String productFutureStock(@PathVariable("productCode") final String productCode, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) throws CMSItemNotFoundException
	{
		final boolean futureStockEnabled = Config.getBoolean(FUTURE_STOCK_ENABLED, false);
		if (futureStockEnabled)
		{
			final List<FutureStockData> futureStockList = futureStockFacade.getFutureAvailability(productCode);
			if (futureStockList == null)
			{
				GlobalMessages.addErrorMessage(model, STOCK_SERVICE_UNAVAILABLE);
			}
			else if (futureStockList.isEmpty())
			{
				GlobalMessages.addInfoMessage(model, "product.product.details.future.nostock");
			}

			populateProductDetailForDisplay(productCode, model, request, Collections.emptyList());
			model.addAttribute("futureStocks", futureStockList);

			return ControllerConstants.Views.Fragments.Product.FutureStockPopup;
		}
		else
		{
			return ControllerConstants.Views.Pages.Error.ErrorNotFoundPage;
		}
	}

	@ResponseBody
	@RequestMapping(value = PRODUCT_CODE_PATH_VARIABLE_PATTERN + "/grid/skusFutureStock", method =
	{ RequestMethod.POST }, produces = MediaType.APPLICATION_JSON_VALUE)
	public final Map<String, Object> productSkusFutureStock(final FutureStockForm form, final Model model)
	{
		final String productCode = form.getProductCode();
		final List<String> skus = form.getSkus();
		final boolean futureStockEnabled = Config.getBoolean(FUTURE_STOCK_ENABLED, false);

		Map<String, Object> result = new HashMap<>();
		if (futureStockEnabled && CollectionUtils.isNotEmpty(skus) && StringUtils.isNotBlank(productCode))
		{
			final Map<String, List<FutureStockData>> futureStockData = futureStockFacade
					.getFutureAvailabilityForSelectedVariants(productCode, skus);

			if (futureStockData == null)
			{
				// future availability service is down, we show this to the user
				result = Maps.newHashMap();
				final String errorMessage = getMessageSource().getMessage(NOT_MULTISKU_ITEM_ERROR, null,
						getI18nService().getCurrentLocale());
				result.put(NOT_MULTISKU_ITEM_ERROR, errorMessage);
			}
			else
			{
				for (final Map.Entry<String, List<FutureStockData>> entry : futureStockData.entrySet())
				{
					result.put(entry.getKey(), entry.getValue());
				}
			}
		}
		return result;
	}

	@ExceptionHandler(UnknownIdentifierException.class)
	public String handleUnknownIdentifierException(final UnknownIdentifierException exception, final HttpServletRequest request)
	{
		request.setAttribute("message", exception.getMessage());
		return FORWARD_PREFIX + "/404";
	}

	protected void updatePageTitle(final String productCode, final Model model)
	{
		storeContentPageTitleInModel(model, getPageTitleResolver().resolveProductPageTitle(productCode));
	}

	protected void updatePageTitle(final Model model)
	{
		storeContentPageTitleInModel(model, getPageTitleResolver().resolveContentPageTitle(null));
	}

	protected void populateProductDetailForDisplay(final String productCode, final Model model, final HttpServletRequest request,
			final List<ProductOption> extraOptions) throws CMSItemNotFoundException
	{
		Boolean isProductIMAddToCart = null;
		Boolean isCurrentStockMin = null;

		final B2BCustomerModel customerModel = catCustomerFacade.getCurrentDealer();

		final ProductModel productModel = productService.getProductForCode(productCode);
		final List<ProductOption> options = new ArrayList<>(Arrays.asList(ProductOption.BASIC, ProductOption.URL,
				ProductOption.PRICE, ProductOption.SUMMARY, ProductOption.DESCRIPTION, ProductOption.GALLERY,
				ProductOption.CATEGORIES, ProductOption.REVIEW, ProductOption.PROMOTIONS, ProductOption.CLASSIFICATION,
				ProductOption.VARIANT_FULL, ProductOption.STOCK, ProductOption.VOLUME_PRICES, ProductOption.PRICE_RANGE,
				ProductOption.DELIVERY_MODE_AVAILABILITY, ProductOption.REFERENCES));

		final String userType = catCustomerFacade.getUserType();

		options.addAll(extraOptions);

		final ProductData productData = productFacade.getProductForCodeAndOptions(productCode, options);


		final List<ProductReferenceData> productReferenceDataList = productData.getProductReferences();
		final List<String> productReferenceAttachment = new ArrayList<>();
		final List<String> productReferenceParts = new ArrayList<>();
		populateProductReferences(productReferenceDataList, productReferenceAttachment, productReferenceParts);

		model.addAttribute("attachmentReferenceData", productReferenceAttachment);

		model.addAttribute("partReferenceData", productReferenceParts);



		final ProductConditionEnum productCondition = getProductType(productModel, model);

		setDefaultSearchPage(request, productCondition);

		getRequestContextData(request).setProduct(productModel);

		populateFavouriteProducts(productData, productModel, productCondition);

		populateProductClassificationData(productData, productCondition, model);

		populateProductAccessories(productData, model);

		final ProductData baseProductData = productFacade.getProductForCodeAndOptions(productData.getBaseProduct(), extraOptions);

		if (null != productData.getBaseProduct())
		{
			productData.setBaseProduct(productData.getBaseProduct().replaceFirst("(.{3})(?!$)", "$1-"));
		}

		if (productModel instanceof ConfigVariantProductModel)
		{
			model.addAttribute("salesModel", baseProductData.getBaseProduct());
			model.addAttribute("primaryOffering", baseProductData.getName());
		}
		else
		{
			model.addAttribute("salesModel", baseProductData.getCode());
		}

		model.addAttribute("isPdpPage", Boolean.TRUE);

		final List<StockData> stockData = catStockFacade.populateStockForProduct(productModel);

		model.addAttribute("stockData", stockData);

		model.addAttribute("productCondition", productCondition);

		sortVariantOptionData(productData);
		storeCmsPageInModel(model, getPageForProduct(productCode));
		populateProductData(productData, model);

		populateBreadCrumbs(productData, model, productCondition);

		if (CollectionUtils.isNotEmpty(productData.getVariantMatrix()))
		{
			model.addAttribute(WebConstants.MULTI_DIMENSIONAL_PRODUCT,
					Boolean.valueOf(CollectionUtils.isNotEmpty(productData.getVariantMatrix())));
		}

		if (userType.equalsIgnoreCase(CatCoreConstants.IM) || userType.equalsIgnoreCase(CatCoreConstants.UTV))
		{
			isProductIMAddToCart = catStockFacade.isIMAddToCartConfigProduct(productData.getSalesModelId(), productModel,
					customerModel);

			final String imAlertId = baseProductData.getBaseProduct() + "_" + customerModel.getDefaultB2BUnit().getUid();

			final RestockAlertModel restockAlert = catStockFacade.getStockForSalesModel(imAlertId);

			if (null != restockAlert)
			{
				if (restockAlert.getCurrentStockLevel().compareTo(restockAlert.getMinimumStockLevel()) < 0)
				{
					isCurrentStockMin = Boolean.TRUE;
				}
				else
				{
					isCurrentStockMin = Boolean.FALSE;
				}
			}

		}
		model.addAttribute("isProductIMAddToCart", isProductIMAddToCart);
		model.addAttribute("isCurrentStockMin", isCurrentStockMin);

		final List<InventoryAgeData> inventoryAgeDatas = catStockFacade.getInventoryAgeForProduct(productModel);
		model.addAttribute("inventoryAge", inventoryAgeDatas);
	}

	/**
	 * @param productReferenceDataList
	 *           - reference data list
	 * @param productReferenceAttachment
	 *           - ref attachment list
	 * @param productReferenceParts
	 *           - ref parts list
	 */
	private void populateProductReferences(final List<ProductReferenceData> productReferenceDataList,
			final List<String> productReferenceAttachment, final List<String> productReferenceParts)
	{
		if (CollectionUtils.isNotEmpty(productReferenceDataList))
		{
			for (final ProductReferenceData productReferenceData : productReferenceDataList)
			{
				if (ATTACHMENT.equals(productReferenceData.getReferenceType().name()))
				{
					productReferenceAttachment.add(productReferenceData.getTarget().getName());

				}
				else if (PART.equals(productReferenceData.getReferenceType().name()))
				{
					productReferenceParts.add(productReferenceData.getTarget().getName());
				}
			}

		}

	}

	/**
	 * Method used to set default search page
	 *
	 * @param request
	 * @param productCondition
	 */
	private void setDefaultSearchPage(final HttpServletRequest request, final ProductConditionEnum productCondition)
	{
		if (null != productCondition && productCondition.equals(ProductConditionEnum.USED))
		{
			request.getSession().setAttribute(DEFAULT_SEARCH_PAGE, "usedProductPLP");
		}
		else if (null != productCondition && productCondition.equals(ProductConditionEnum.NEW))
		{
			request.getSession().setAttribute(DEFAULT_SEARCH_PAGE, "newProductPLP");
		}
	}

	/**
	 * @param productData
	 * @param model
	 */
	private void populateProductAccessories(final ProductData productData, final Model model)
	{
		final List<ProductReferenceData> references = productData.getProductReferences();

		final Map<String, List<ProductReferenceData>> accessories = references.stream()
				.filter(reference -> reference.getReferenceType().equals(ProductReferenceTypeEnum.ACCESSORIES))
				.collect(Collectors.toList()).stream().filter(refs -> refs.getDescription() != null)
				.collect(Collectors.groupingBy(ProductReferenceData::getDescription));

		model.addAttribute("accessories", accessories);
	}

	/**
	 * This method is used to get Product Type.
	 *
	 * @param productModel
	 * @param model
	 * @return Product Condition.
	 */
	private ProductConditionEnum getProductType(final ProductModel productModel, final Model model)
	{
		final boolean isUtv = catProductService.isUtvProduct(productModel);
		ProductConditionEnum productCondition = null;
		if (productModel instanceof ConfigVariantProductModel)
		{
			productCondition = ProductConditionEnum.NEW;
		}
		else if (productModel instanceof BaseVariantProductModel)
		{
			if (isUtv)
			{
				productCondition = ProductConditionEnum.NEW;
			}
			else
			{
				productCondition = ProductConditionEnum.USED;
			}
		}

		model.addAttribute("productCondition", productCondition);
		return productCondition;
	}


	/**
	 * This method is used to populate favourite products.
	 *
	 * @param productModel
	 */
	private void populateFavouriteProducts(final ProductData productData, final ProductModel productModel,
			final ProductConditionEnum productCondition)
	{
		final UserModel user = userService.getCurrentUser();
		if (user instanceof B2BCustomerModel)
		{
			final RecentViewedItemsCollection collection = saveProductCodeInSession(productData, productCondition);
			//added null check for this collection
			if (null != collection)
			{
				collection.setCustomer((B2BCustomerModel) user);
				bindProductToRecentlyViewedItemsCollection(collection);
			}
			final Set<ProductModel> favProdList = ((B2BCustomerModel) user).getFavoriteProducts();
			if (favProdList.contains(productModel))
			{
				productData.setFavorite(true);
			}
			else
			{
				productData.setFavorite(false);
			}
		}

	}

	/**
	 * @param productData
	 * @param productCondition
	 * @param model
	 */
	private void populateProductClassificationData(final ProductData productData, final ProductConditionEnum productCondition,
			final Model model)
	{
		if (null != productCondition)
		{
			LOG.info("Product type visited is " + productCondition.getCode());
			if (ProductConditionEnum.USED.equals(productCondition))
			{
				populateClassificationDataInModel(model, productData);
			}
		}
	}

	/**
	 * @param model
	 * @param productData
	 */
	private void populateClassificationDataInModel(final Model model, final ProductData productData)
	{
		if (CollectionUtils.isNotEmpty(productData.getClassifications()))
		{
			for (final ClassificationData classification : productData.getClassifications())
			{
				populateFeatureDataInModel(model, classification);
			}
		}

	}

	/**
	 * @param model
	 * @param classification
	 */
	private void populateFeatureDataInModel(final Model model, final ClassificationData classification)
	{
		if (CollectionUtils.isNotEmpty(classification.getFeatures()))
		{
			for (final FeatureData feature : classification.getFeatures())
			{

				if (CollectionUtils.isNotEmpty(feature.getFeatureValues()))
				{

					populateFeatureValues(model, feature);
				}
			}
		}
	}

	/**
	 * @param model
	 * @param feature
	 */
	private void populateFeatureValues(final Model model, final FeatureData feature)
	{
		for (final FeatureValueData featureValueData : feature.getFeatureValues())
		{
			if (feature.getName() != null && feature.getName().equals(CatCoreConstants.BRANCH_LOCATION))
			{
				String branchLoc = featureValueData.getValue();
				if (featureValueData.getValue().contains("/"))
				{
					branchLoc = branchLoc.replaceAll("/", " / ");
				}
				model.addAttribute(feature.getName(), branchLoc);
			}
			else if (feature.getName() != null && !feature.getName().equals(CatCoreConstants.BRANCH_LOCATION))
			{
				model.addAttribute(feature.getName(), featureValueData.getValue());
			}
		}
	}

	/**
	 * This method is used to populate BreadCrumbs.
	 *
	 * @param productData
	 * @param model
	 * @param request
	 * @param productCondition
	 */
	private void populateBreadCrumbs(final ProductData productData, final Model model, final ProductConditionEnum productCondition)
	{
		final List<Breadcrumb> breadcrumbList = productBreadcrumbBuilder.getBreadcrumbs(productData.getCode());
		final List<Breadcrumb> breadcrumbs = new ArrayList<>();
		CategoryData categoryData = new CategoryData();
		if (productData.getCategories() != null && !productData.getCategories().isEmpty())
		{
			categoryData = productData.getCategories().iterator().next();
		}
		else
		{
			categoryData.setCode("#");
		}
		final Map modelMap = model.asMap();
		final Map<String, Object> allFilterParametersMap = (Map<String, Object>) modelMap.get(ALLFILTERPARAMETER);
		final String categoryCode = String.valueOf(allFilterParametersMap.get(CATEGORY));
		if (StringUtils.isNotBlank(categoryCode) && !categoryCode.equalsIgnoreCase(NULL))
		{
			String url = "";
			if (ProductConditionEnum.NEW.equals(productCondition))
			{
				url = getRefinedURLForNewProduct(CatCoreConstants.CONFIG_LINK_URL_NEW, model);
			}
			else if (ProductConditionEnum.USED.equals(productCondition))
			{
				url = getRefinedURLForUsedProduct(CatCoreConstants.CONFIG_LINK_URL_USED, model);
			}
			if (StringUtils.isNotBlank(url))
			{
				final Breadcrumb breadcrumb = new Breadcrumb(url + DEFAULTFACETVALUE + BACKWARDNAVIGATIONFLAG,
						CatCoreConstants.CONFIGURATIONS, CatCoreConstants.MIDDLE_LINK_CLASS);
				breadcrumb.setCategoryCode(categoryData.getName());
				breadcrumbs.add(breadcrumb);
			}
		}
		for (int i = 0; i < breadcrumbList.size(); i++)
		{

			if (i > 2)
			{
				breadcrumbs.add(breadcrumbList.get(i));
			}

		}
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);

	}

	/**
	 * This method is used to get refined BreadCrumb URL for New Products.
	 *
	 * @param configLinkUrl
	 * @param model
	 * @return BreadCrumb URL String
	 */
	private String getRefinedURLForNewProduct(final String configLinkUrl, final Model model)
	{
		final Map modelMap = model.asMap();
		final Map<String, Object> allFilterParametersMap = (Map<String, Object>) modelMap.get(ALLFILTERPARAMETER);
		return configLinkUrl.replaceFirst("p1", String.valueOf(allFilterParametersMap.get(CATEGORY)))
				.replaceFirst("p2", String.valueOf(allFilterParametersMap.get(SALES_OFFERING_ID)))
				.replaceFirst("p3", String.valueOf(allFilterParametersMap.get(PRIMARY_OFFERING_ID)))
				.replaceFirst("p4", String.valueOf(allFilterParametersMap.get(PDC_FLAG)))
				.replaceFirst("p5", String.valueOf(allFilterParametersMap.get(MFU_FLAG)))
				.replaceFirst("p6", String.valueOf(allFilterParametersMap.get(WAREHOUSE_FLAG)))
				.replaceFirst("p7", String.valueOf(allFilterParametersMap.get(PAGE)))
				.replaceFirst("p8", String.valueOf(allFilterParametersMap.get(LANETYPE)));

	}

	/**
	 * This method is used to get refined BreadCrumb URL for Used Products. If the user browses from the Landing page to PDP
	 * of used product(i.e. search used product with serial number) then the breadCrumb shall be home-->Respective PDP. So
	 * 'url' will be blank String and 'Configuration' breadCrumb is not calculated.
	 *
	 * @param configLinkUrl
	 * @param model
	 * @return BreadCrumb URL String
	 */
	private String getRefinedURLForUsedProduct(final String configLinkUrl, final Model model)
	{
		final Map modelMap = model.asMap();
		final Map<String, Object> allFilterParametersMap = (Map<String, Object>) modelMap.get(ALLFILTERPARAMETER);
		String url = "";
		if (allFilterParametersMap.get(PAGE) != null)
		{
			url = configLinkUrl.replaceFirst("p1", String.valueOf(allFilterParametersMap.get(CATEGORY)))
					.replaceFirst("p2", String.valueOf(allFilterParametersMap.get(SALES_OFFERING_ID)))
					.replaceFirst("p3", String.valueOf(allFilterParametersMap.get(PRIMARY_OFFERING_ID)))
					.replaceFirst("p7", String.valueOf(allFilterParametersMap.get(PAGE)))
					.replaceFirst("p8", String.valueOf(allFilterParametersMap.get(BRANCHLOCATION)))
					.replaceFirst("p9", String.valueOf(allFilterParametersMap.get(MANUFACTURINGYEAR_MIN)))
					.replaceFirst("p10", String.valueOf(allFilterParametersMap.get(MANUFACTURINGYEAR_MAX)))
					.replaceFirst("p11", String.valueOf(allFilterParametersMap.get(HOUSEUSED_MIN)))
					.replaceFirst("p12", String.valueOf(allFilterParametersMap.get(HOUSEUSED_MAX)))
					.replaceFirst("p13", String.valueOf(allFilterParametersMap.get(PRICE_MIN)))
					.replaceFirst("p14", String.valueOf(allFilterParametersMap.get(PRICE_MAX)))
					.replaceFirst("p15", String.valueOf(allFilterParametersMap.get(DEALERCERTIFIED)));
		}
		return url;
	}

	protected void populateProductData(final ProductData productData, final Model model)
	{
		model.addAttribute("galleryImages", getGalleryImages(productData));
		model.addAttribute("product", productData);
		if (productData.getConfigurable().booleanValue())
		{
			final List<ConfigurationInfoData> configurations = productFacade.getConfiguratorSettingsForCode(productData.getCode());
			if (CollectionUtils.isNotEmpty(configurations))
			{
				model.addAttribute("configuratorType", configurations.get(0).getConfiguratorType());
			}
		}
	}

	protected void sortVariantOptionData(final ProductData productData)
	{
		if (CollectionUtils.isNotEmpty(productData.getBaseOptions()))
		{
			for (final BaseOptionData baseOptionData : productData.getBaseOptions())
			{
				if (CollectionUtils.isNotEmpty(baseOptionData.getOptions()))
				{
					Collections.sort(baseOptionData.getOptions(), variantSortStrategy);
				}
			}
		}

		if (CollectionUtils.isNotEmpty(productData.getVariantOptions()))
		{
			Collections.sort(productData.getVariantOptions(), variantSortStrategy);
		}
	}

	protected List<Map<String, ImageData>> getGalleryImages(final ProductData productData)
	{
		final List<Map<String, ImageData>> galleryImages = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(productData.getImages()))
		{
			final List<ImageData> images = new ArrayList<>();
			for (final ImageData image : productData.getImages())
			{
				if (ImageDataType.GALLERY.equals(image.getImageType()))
				{
					images.add(image);
				}
			}
			if (CollectionUtils.isNotEmpty(images))
			{
				images.sort((final ImageData image1, final ImageData image2) -> image1.getGalleryIndex()
						.compareTo(image2.getGalleryIndex()));
				addFormatsToGalleryImages(galleryImages, images);
			}
		}
		return galleryImages;
	}

	protected void addFormatsToGalleryImages(final List<Map<String, ImageData>> galleryImages, final List<ImageData> images)
	{
		int currentIndex = images.get(0).getGalleryIndex().intValue();
		Map<String, ImageData> formats = new HashMap<>();
		for (final ImageData image : images)
		{

			if (currentIndex != image.getGalleryIndex().intValue())
			{
				galleryImages.add(formats);
				formats = new HashMap<>();
				currentIndex = image.getGalleryIndex().intValue();
			}
			formats.put(image.getFormat(), image);
		}
		if (!formats.isEmpty())
		{
			galleryImages.add(formats);
		}
	}

	protected ReviewValidator getReviewValidator()
	{
		return reviewValidator;
	}

	protected AbstractPageModel getPageForProduct(final String productCode) throws CMSItemNotFoundException
	{
		final ProductModel productModel = productService.getProductForCode(productCode);
		sessionService.setAttribute("ProductModel", productModel);
		return cmsPageService.getPageForProduct(productModel);
	}

	private void bindProductToRecentlyViewedItemsCollection(final RecentViewedItemsCollection collection)
	{
		final RecentViewedProductEvent event = new RecentViewedProductEvent(collection);
		eventService.publishEvent(event);
	}

	/**
	 * Method to save recently viewed products by customer in session based on Product condition type.
	 *
	 * @param productData
	 * @param productCondition
	 * @return RecentViewedItemsCollection
	 */
	private RecentViewedItemsCollection saveProductCodeInSession(final ProductData productData,
			final ProductConditionEnum productCondition)
	{
		RecentViewedItemsCollection recentViewedItemsCollection = null;
		String productTypeVistedKey = "";

		if (null != productCondition)
		{
			if (ProductConditionEnum.NEW.equals(productCondition))
			{
				recentViewedItemsCollection = sessionService.getOrLoadAttribute(VISITED_PRODUCTS_SESSION_KEY,
						() -> new RecentViewedItemsCollection(maxRecentViewedItems));

				productTypeVistedKey = VISITED_PRODUCTS_SESSION_KEY;
			}
			else if (ProductConditionEnum.USED.equals(productCondition))
			{
				recentViewedItemsCollection = sessionService.getOrLoadAttribute(VISITED_USED_PRODUCTS_SESSION_KEY,
						() -> new RecentViewedItemsCollection(maxRecentViewedItems));

				productTypeVistedKey = VISITED_USED_PRODUCTS_SESSION_KEY;
			}
		}
		//Resolved SONAR issue
		if (null != recentViewedItemsCollection)

		{
			recentViewedItemsCollection.setProductCondition(productCondition);
			recentViewedItemsCollection.addProductCode(productData);
		}
		sessionService.setAttribute(productTypeVistedKey, recentViewedItemsCollection);
		return recentViewedItemsCollection;

	}


}