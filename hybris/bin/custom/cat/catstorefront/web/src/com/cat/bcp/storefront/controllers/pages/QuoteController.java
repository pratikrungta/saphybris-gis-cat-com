/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.storefront.controllers.pages;

import de.hybris.bootstrap.xml.InvalidFormatException;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.ResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.ThirdPartyConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCartPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.QuoteDiscountForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.QuoteForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.VoucherForm;
import de.hybris.platform.acceleratorstorefrontcommons.tags.Functions;
import de.hybris.platform.acceleratorstorefrontcommons.util.AddressDataUtil;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.misc.UrlUtils;
import de.hybris.platform.commercefacades.comment.data.CommentData;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.QuoteFacade;
import de.hybris.platform.commercefacades.order.SaveCartFacade;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CatQuoteSerialNumbersData;
import de.hybris.platform.commercefacades.order.data.CatReserveQuoteData;
import de.hybris.platform.commercefacades.order.data.CommerceCartMetadata;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.PriceDataType;
import de.hybris.platform.commercefacades.quote.data.QuoteData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commercefacades.util.CommerceCartMetadataUtils;
import de.hybris.platform.commercefacades.voucher.VoucherFacade;
import de.hybris.platform.commercefacades.voucher.data.VoucherData;
import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.enums.DiscountType;
import de.hybris.platform.commerceservices.enums.QuoteAction;
import de.hybris.platform.commerceservices.order.CommerceQuoteAssignmentException;
import de.hybris.platform.commerceservices.order.CommerceQuoteExpirationTimeException;
import de.hybris.platform.commerceservices.order.exceptions.IllegalQuoteStateException;
import de.hybris.platform.commerceservices.order.exceptions.IllegalQuoteSubmitException;
import de.hybris.platform.commerceservices.order.exceptions.QuoteUnderThresholdException;
import de.hybris.platform.commerceservices.util.QuoteExpirationTimeUtils;
import de.hybris.platform.core.enums.QuoteState;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.exceptions.ModelSavingException;
import de.hybris.platform.servicelayer.exceptions.SystemException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.internal.model.impl.ItemModelCloneCreator.CannotCloneException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.tx.Transaction;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.enums.CatQuoteLostReasonEnum;
import com.cat.bcp.core.exception.CatException;
import com.cat.bcp.core.integration.jobs.service.CatIntegrationService;
import com.cat.bcp.facade.export.CatQuoteExportFacade;
import com.cat.bcp.facades.customer.CatCustomerFacade;
import com.cat.bcp.facades.order.CatQuoteFacade;
import com.cat.bcp.facades.user.CatUserFacade;
import com.cat.bcp.storefront.dto.CatQuoteResponseDTO;
import com.cat.bcp.storefront.forms.CatQuoteDiscountForm;
import com.cat.bcp.storefront.forms.CatQuoteForm;
import com.cat.bcp.storefront.forms.CatQuoteResponseData;
import com.cat.bcp.storefront.forms.CatQuoteVETForm;
import com.cat.bcp.storefront.util.QuoteExpirationTimeConverter;


/**
 * Controller for Quotes
 */

@Controller
@RequestMapping(value = "/quote")
public class QuoteController extends AbstractCartPageController
{
	private static final String CALCULATE_CART_ERROR_MESSAGE = "Failed to calculate session cart when applying the discount for quote %s";
	private static final String QUOTE_FAILURE_REASON = "Attempt to submit a quote that is not allowed.";
	private static final String REDIRECT_CART_URL = REDIRECT_PREFIX + "/cart/catCartPage";
	private static final String REDIRECT_QUOTE_LIST_URL = REDIRECT_PREFIX + "/my-account/my-quotes/";
	private static final String REDIRECT_QUOTE_EDIT_URL = REDIRECT_PREFIX + "/quote/%s/edit/";
	private static final String REDIRECT_QUOTE_DETAILS_URL = REDIRECT_PREFIX + "/my-account/my-quotes/%s/";
	private static final String QUOTE_EDIT_CMS_PAGE = "catQuotePage";
	private static final String VOUCHER_FORM = "voucherForm";
	private static final String ALLOWED_ACTIONS = "allowedActions";
	private static final String DATE_FORMAT_KEY = "text.quote.dateformat";
	// localization properties
	private static final String PAGINATION_NUMBER_OF_COMMENTS = "quote.pagination.numberofcomments";
	private static final String QUOTE_EMPTY_CART_ERROR = "quote.cart.empty.error";
	private static final String QUOTE_CREATE_ERROR = "quote.create.error";
	private static final String QUOTE_REQUOTE_ERROR = "quote.requote.error";
	private static final String QUOTE_NOT_EDITABLE_ERROR = "quote.not.editable";
	private static final String QUOTE_EDIT_LOCKED_ERROR = "quote.edit.locked";
	private static final String QUOTE_TEXT_CANCEL_SUCCESS = "text.quote.cancel.success";
	private static final String QUOTE_TEXT_NOT_CANCELABLE = "text.quote.not.cancelable";
	private static final String QUOTE_NOT_SUBMITABLE_ERROR = "text.quote.not.submitable";
	private static final String QUOTE_NEWCART_ERROR = "quote.newcart.error";
	private static final String QUOTE_NEWCART_SUCCESS = "quote.newcart.success";
	private static final String QUOTE_SAVE_CART_ERROR = "quote.save.cart.error";
	private static final String QUOTE_SUBMIT_ERROR = "quote.submit.error";
	private static final String QUOTE_SUBMIT_SUCCESS = "quote.submit.success";
	private static final String QUOTE_EXPIRED = "quote.state.expired";
	private static final String QUOTE_REJECT_INITIATION_REQUEST = "quote.reject.initiate.request";
	private static final String QUOTE_CART_INSUFFICIENT_ACCESS_RIGHTS = "quote.cart.insufficient.access.rights.error";
	private static final String US = "US";
	private static final String CN = "CA";
	private static final String ACCEPTED_STATE = "BUYER_ACCEPTED";

	private static final Logger LOG = Logger.getLogger(QuoteController.class);

	@Resource(name = "simpleBreadcrumbBuilder")
	private ResourceBreadcrumbBuilder resourceBreadcrumbBuilder;

	@Resource(name = "quoteFacade")
	private CatQuoteFacade quoteFacade;

	@Resource(name = "voucherFacade")
	private VoucherFacade voucherFacade;

	@Resource(name = "saveCartFacade")
	private SaveCartFacade saveCartFacade;

	@Autowired
	private SmartValidator smartValidator;

	@Resource(name = "priceDataFactory")
	private PriceDataFactory priceDataFactory;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource
	private UserService userService;

	@Resource
	private CommonI18NService commonI18NService;

	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "defaultCatCustomerFacade")
	private CatCustomerFacade defaultCatCustomerFacade;

	@Resource(name = "userFacade")
	private CatUserFacade userFacade;

	@Resource(name = "addressDataUtil")
	private AddressDataUtil addressDataUtil;

	@Resource(name = "catQuoteExportFacade")
	private CatQuoteExportFacade catQuoteExportFacade;

	@Resource(name = "sessionService")
	SessionService sessionService;

	@Resource(name = "catIntegrationService")
	CatIntegrationService catIntegrationService;



	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * Creates a new quote based on session cart.
	 *
	 * @param redirectModel
	 *           the redirect model
	 * @return Mapping to quote page.
	 */
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public String createQuote(final RedirectAttributes redirectModel)
	{
		try
		{
			if (!getCartFacade().hasEntries())
			{
				// No session cart or empty session cart. Bounce back to the cart page.
				LOG.debug("Missing or empty cart");
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_EMPTY_CART_ERROR, null);
				return REDIRECT_CART_URL;
			}
			removeCoupons(redirectModel);

			final QuoteData quoteData = getQuoteFacade().initiateQuote();

			return String.format(REDIRECT_QUOTE_EDIT_URL, urlEncode(quoteData.getCode()));
		}
		catch (final IllegalArgumentException | CannotCloneException | ModelSavingException e)
		{
			LOG.error("Unable to create quote", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_CREATE_ERROR, null);
			return REDIRECT_CART_URL;
		}
	}

	/**
	 * Removes all coupons from the client cart. To be updated in a future release.
	 *
	 * @param redirectModel
	 */
	protected void removeCoupons(final RedirectAttributes redirectModel)
	{
		final List<VoucherData> vouchers = voucherFacade.getVouchersForCart();

		for (final VoucherData voucher : vouchers)
		{
			try
			{
				voucherFacade.releaseVoucher(voucher.getCode());
			}
			catch (final VoucherOperationException e)
			{
				GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.voucher.release.error",
						new Object[]
						{ voucher.getCode() });
				if (LOG.isDebugEnabled())
				{
					LOG.debug(e.getMessage(), e);
				}
			}
		}
	}

	/**
	 * Adds discount to an existing quote.
	 *
	 * @param quoteCode
	 *           Quote to have discounts applied.
	 * @param form
	 *           Discount info.
	 * @param redirectModel
	 * @return Mapping redirect to quote page.
	 */
	@RequestMapping(value = "{quoteCode}/discount/apply", method = RequestMethod.POST)
	public String applyDiscountAction(@PathVariable("quoteCode") final String quoteCode, @Valid final QuoteDiscountForm form,
			final RedirectAttributes redirectModel)
	{
		try
		{
			getQuoteFacade().applyQuoteDiscount(form.getDiscountRate(), form.getDiscountType());
		}
		catch (final IllegalArgumentException e)
		{
			LOG.error(String.format("Error applying discount for quote %s", quoteCode), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"text.quote.discount.apply.argument.invalid.error", null);
		}
		catch (final SystemException e)
		{
			LOG.error(String.format(CALCULATE_CART_ERROR_MESSAGE, quoteCode), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"text.quote.discount.apply.calculation.error", null);
			return REDIRECT_CART_URL;
		}

		return String.format(REDIRECT_QUOTE_EDIT_URL, urlEncode(quoteCode));
	}

	/**
	 * Loads quote edit page.
	 *
	 * @param model
	 * @param redirectModel
	 * @param quoteCode
	 * @return Mapping to edit page.
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/{quoteCode}/edit", method = RequestMethod.GET)
	public String showQuoteEdit(final Model model, final RedirectAttributes redirectModel,
			@PathVariable("quoteCode") final String quoteCode,
			@RequestParam(value = "firstTimeEdit", defaultValue = "true") final boolean firstTimeEdit,
			@RequestParam(value = "removeSessionCart", defaultValue = "true") final boolean removeSessionCart,
			@RequestParam(value = "replicateQuote", defaultValue = "false") final boolean replicateQuote)
			throws CMSItemNotFoundException
	{
		try
		{
			clearCart(removeSessionCart);
			final QuoteData quoteData = getQuoteFacade().getQuoteForCode(quoteCode);

			if (firstTimeEdit)
			{
				quoteData.setQuoteCreatedTime(new Date());
			}
			if (!firstTimeEdit || replicateQuote)
			{
				model.addAttribute("firstTimeEdit", Boolean.valueOf(firstTimeEdit));
			}
			model.addAttribute("catQuoteData", quoteData);
			model.addAttribute("isReplicateQuote", Boolean.valueOf(replicateQuote));

			if (LOG.isDebugEnabled())
			{
				LOG.debug("Quote State is ::::::\t" + quoteData.getState().getCode());
			}

			if (quoteData.getState().getCode().equals(ACCEPTED_STATE))
			{
				return String.format(REDIRECT_QUOTE_DETAILS_URL, urlEncode(quoteCode));
			}

			setQuoteFacade(quoteCode, replicateQuote);

			setCountryAndRegionData(model);

			model.addAttribute("warehouseName", defaultCatCustomerFacade.getDealerWarehouseName());

		}
		catch (final UnknownIdentifierException e)
		{
			LOG.warn("Attempted to load a quote that does not exist or is not editable", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_NOT_EDITABLE_ERROR,
					new Object[]
					{ quoteCode });
			return String.format(REDIRECT_QUOTE_DETAILS_URL, urlEncode(quoteCode));
		}
		catch (final CommerceQuoteAssignmentException e)
		{
			LOG.warn("Attempted to edit a quote that is assigned to another user: " + quoteCode, e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER, QUOTE_EDIT_LOCKED_ERROR, new Object[]
			{ quoteCode, e.getAssignedUser() });
			return String.format(REDIRECT_QUOTE_DETAILS_URL, urlEncode(quoteCode));
		}
		catch (final ModelNotFoundException e)
		{
			LOG.warn("Attempted to load a quote that does not exist or is not accessible by current user:" + quoteCode, e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					QUOTE_CART_INSUFFICIENT_ACCESS_RIGHTS, new Object[]
					{ getCartFacade().getSessionCart().getCode() });
			return REDIRECT_PREFIX + ROOT;
		}
		catch (final SystemException e)
		{
			LOG.warn("There was error saving the session cart", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_SAVE_CART_ERROR, null);
			return String.format(REDIRECT_QUOTE_DETAILS_URL, urlEncode(quoteCode));
		}

		final CartData cartData = getCartFacade().getSessionCartWithEntryOrdering(false);
		prepareQuotePageElements(model, cartData, true);
		if (firstTimeEdit)
		{
			setExpirationDateForQuote(cartData.getQuoteData(), cartData.getQuoteData().getCreationTime());
		}
		if (replicateQuote)
		{
			setExpirationDateForQuote(cartData.getQuoteData(), cartData.getQuoteData().getQuoteCreatedTime());
		}

		createProductEntryList(model, cartData);
		storeCmsPageInModel(model, getContentPageForLabelOrId(QUOTE_EDIT_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(QUOTE_EDIT_CMS_PAGE));

		sortComments(cartData);
		continueUrl(model);
		setAllowedActions(model, quoteCode);

		model.addAttribute("exportCsvEnabled", Boolean.TRUE);
		populateBreadCrumbsForQuoteEdit(model, quoteCode, firstTimeEdit, replicateQuote, cartData);
		model.addAttribute(ThirdPartyConstants.SeoRobots.META_ROBOTS, ThirdPartyConstants.SeoRobots.NOINDEX_NOFOLLOW);

		final double quoteThreshold = getQuoteFacade().getQuoteRequestThreshold(quoteCode);
		if (quoteThreshold > 0
				&& !(GlobalMessages.containsMessage(model, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_REJECT_INITIATION_REQUEST)))
		{
			// Display quote request minimum threshold only if it's set and quote version is equal to 1
			GlobalMessages.addMessage(model, GlobalMessages.INFO_MESSAGES_HOLDER, QUOTE_REJECT_INITIATION_REQUEST, new Object[]
			{ getFormattedPriceValue(quoteThreshold) });
		}

		return getViewForPage(model);
	}



	/**
	 * This method sets quote facade parameters
	 *
	 * @param quoteCode
	 *           - the quote code
	 * @param replicateQuote
	 *           - replicate Quote flag
	 */
	private void setQuoteFacade(final String quoteCode, final boolean replicateQuote)
	{
		if (!replicateQuote)
		{
			quoteFacade.enableQuoteEdit(quoteCode);
		}
		else
		{
			quoteFacade.enableQuoteReplicate(quoteCode);
		}

	}

	/**
	 * @param removeSessionCart
	 *
	 */
	private void clearCart(final boolean removeSessionCart)
	{
		if (removeSessionCart)
		{
			getCartFacade().removeSessionCart();
		}

	}

	/**
	 * Method for populating breadcrumbs for edit quote page
	 *
	 * @param model
	 * @param quoteCode
	 * @param firstTimeEdit
	 * @param replicateQuote
	 * @param cartData
	 */
	private void populateBreadCrumbsForQuoteEdit(final Model model, final String quoteCode, final boolean firstTimeEdit,
			final boolean replicateQuote, final CartData cartData)
	{
		final List<Breadcrumb> breadcrumbs = new ArrayList<>();


		if (firstTimeEdit && !replicateQuote)
		{
			final String productCode = getLastEntryFromCart(cartData);
			final Breadcrumb breadcrumb = new Breadcrumb(CatCoreConstants.CAT_PDP_URI_PREFIX + productCode,
					CatCoreConstants.CAT_PRODUCT_DETAILS_PAGE, CatCoreConstants.MIDDLE_LINK_CLASS);
			breadcrumbs.add(breadcrumb);
			final Breadcrumb breadcrumb1 = new Breadcrumb(CatCoreConstants.CART_PAGE_URI, CatCoreConstants.CAT_CART,
					CatCoreConstants.MIDDLE_LINK_CLASS);
			breadcrumbs.add(breadcrumb1);
			final Breadcrumb breadcrumb2 = new Breadcrumb(CatCoreConstants.LINK_URL, CatCoreConstants.CAT_QUOTE,
					CatCoreConstants.LAST_LINK_CLASS);
			breadcrumbs.add(breadcrumb2);
		}
		else if (replicateQuote)
		{
			final Breadcrumb breadcrumb = new Breadcrumb(CatCoreConstants.CAT_QUOTE_HISTORY_URI, CatCoreConstants.CAT_QUOTE_HISTORY,
					CatCoreConstants.MIDDLE_LINK_CLASS);
			breadcrumbs.add(breadcrumb);
			final Breadcrumb breadcrumb1 = new Breadcrumb(
					CatCoreConstants.CAT_QUOTE_DETAILS_URI + sessionService.getAttribute("replicatedQuote"),
					CatCoreConstants.CAT_QUOTE_DETAILS, CatCoreConstants.MIDDLE_LINK_CLASS);
			breadcrumbs.add(breadcrumb1);
			final Breadcrumb breadcrumb2 = new Breadcrumb(CatCoreConstants.LINK_URL, CatCoreConstants.CAT_REPLICATE_QUOTE,
					CatCoreConstants.LAST_LINK_CLASS);
			breadcrumbs.add(breadcrumb2);
		}
		else
		{
			final Breadcrumb breadcrumb = new Breadcrumb(CatCoreConstants.CAT_QUOTE_HISTORY_URI, CatCoreConstants.CAT_QUOTE_HISTORY,
					CatCoreConstants.MIDDLE_LINK_CLASS);
			breadcrumbs.add(breadcrumb);
			final Breadcrumb breadcrumb1 = new Breadcrumb(CatCoreConstants.CAT_QUOTE_DETAILS_URI + quoteCode,
					CatCoreConstants.CAT_QUOTE_DETAILS, CatCoreConstants.MIDDLE_LINK_CLASS);
			breadcrumbs.add(breadcrumb1);
			final Breadcrumb breadcrumb2 = new Breadcrumb(CatCoreConstants.LINK_URL, CatCoreConstants.CAT_EDIT_QUOTE,
					CatCoreConstants.LAST_LINK_CLASS);
			breadcrumbs.add(breadcrumb2);
		}
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);
	}

	/**
	 * @param cartData
	 */
	private String getLastEntryFromCart(final CartData cartData)
	{
		final long count = cartData.getEntries().stream().filter(entry -> BooleanUtils.isFalse(entry.getMandatoryProduct()))
				.count();
		final OrderEntryData orderEntryData = cartData.getEntries().stream()
				.filter(entry -> BooleanUtils.isFalse(entry.getMandatoryProduct())).skip(count - 1).findFirst().get();
		return orderEntryData.getProduct().getCode();
	}



	/**
	 * @param model
	 */
	private void setCountryAndRegionData(final Model model)
	{
		final List<CountryData> countries = new ArrayList<>();
		countries.add(i18NFacade.getCountryForIsocode(US));
		countries.add(i18NFacade.getCountryForIsocode(CN));
		model.addAttribute("countries", countries);
		model.addAttribute("usStates", i18NFacade.getRegionsForCountryIso(US));
		model.addAttribute("canadaStates", i18NFacade.getRegionsForCountryIso(CN));
	}

	protected void fillQuoteForm(final Model model, final AbstractOrderData data)
	{
		if (!model.containsAttribute("catQuoteForm"))
		{
			final Locale currentLocale = getI18nService().getCurrentLocale();
			final String expirationTimePattern = getMessageSource().getMessage(DATE_FORMAT_KEY, null, currentLocale);

			final CatQuoteForm quoteForm = new CatQuoteForm();
			quoteForm.setName(data.getName());
			quoteForm.setDescription(data.getDescription());
			quoteForm.setExpirationTime(
					QuoteExpirationTimeConverter.convertDateToString(data.getExpirationTime(), expirationTimePattern, currentLocale));
			model.addAttribute("catQuoteForm", quoteForm);
		}
		model.addAttribute("catQuoteDiscountForm", new CatQuoteDiscountForm());
	}


	protected void fillVouchers(final Model model)
	{
		model.addAttribute("appliedVouchers", getVoucherFacade().getVouchersForCart());
		if (!model.containsAttribute(VOUCHER_FORM))
		{
			model.addAttribute(VOUCHER_FORM, new VoucherForm());
		}
	}

	protected void setUpdatable(final Model model, final CartData cartData, final boolean updatable)
	{
		for (final OrderEntryData entry : cartData.getEntries())
		{
			entry.setUpdateable(updatable);
		}

		model.addAttribute("disableUpdate", Boolean.valueOf(!updatable));
	}

	protected void setExpirationTimeAttributes(final Model model)
	{
		model.addAttribute("defaultOfferValidityPeriodDays",
				Integer.valueOf(QuoteExpirationTimeUtils.getDefaultOfferValidityPeriodDays()));
		model.addAttribute("minOfferValidityPeriodDays",
				Integer.valueOf(QuoteExpirationTimeUtils.getMinOfferValidityPeriodInDays()));
	}

	protected void prepareQuotePageElements(final Model model, final CartData cartData, final boolean updatable)
	{
		fillQuoteForm(model, cartData);
		fillVouchers(model);
		setUpdatable(model, cartData, updatable);
		loadCommentsShown(model);

		model.addAttribute("savedCartCount", saveCartFacade.getSavedCartsCountForCurrentUser());
		model.addAttribute("quoteCount", quoteFacade.getQuotesCountForCurrentUser());

		setExpirationTimeAttributes(model);
	}

	@RequestMapping(value = "/{quoteCode}/cancel", method = RequestMethod.POST)
	public String cancelQuote(@PathVariable("quoteCode") final String quoteCode, final RedirectAttributes redirectModel)
	{
		try
		{
			quoteFacade.cancelQuote(quoteCode);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, QUOTE_TEXT_CANCEL_SUCCESS,
					new Object[]
					{ quoteCode });

		}
		catch (final UnknownIdentifierException uie)
		{
			LOG.warn("Attempted to cancel a quote that does not exist or is not visible: " + quoteCode, uie);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_TEXT_NOT_CANCELABLE,
					new Object[]
					{ quoteCode });
		}
		catch (final CommerceQuoteAssignmentException e)
		{
			LOG.warn("Attempted to cancel a quote that is assigned to another user: " + quoteCode, e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER, QUOTE_EDIT_LOCKED_ERROR, new Object[]
			{ quoteCode, e.getAssignedUser() });
			return String.format(REDIRECT_QUOTE_DETAILS_URL, urlEncode(quoteCode));
		}

		return REDIRECT_QUOTE_LIST_URL;
	}

	@RequestMapping(value = "/removeSessionCart", method = RequestMethod.DELETE)
	public void removeSessionCartAndRestorePreviousCart()
	{
		getCartFacade().removeSessionCart();
	}

	/**
	 * Submit quote to next responsible in the workflow (e.g. from buyer to seller, from sales representative to sales
	 * approver).
	 *
	 * @param catQuoteForm
	 *           the cat quote form
	 * @param bindingResult
	 *           the binding result
	 * @param model
	 * @param request
	 * @param response
	 * @param redirectModel
	 *           the redirect model
	 * @return CatQuoteResponseDTO
	 */
	@RequestMapping(value = "/submit", method = RequestMethod.POST)
	@ResponseBody
	public CatQuoteResponseDTO submitQuote(@RequestBody final CatQuoteForm catQuoteForm, final BindingResult bindingResult,
			final Model model, final HttpServletRequest request, final HttpServletResponse response,
			final RedirectAttributes redirectModel)
	{
		if (null != sessionService.getAttribute("replicatedQuote"))
		{
			sessionService.removeAttribute("replicatedQuote");
		}
		final Transaction tx = Transaction.current();
		boolean isTransSuccess = false;
		try
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Inside quote submit");
			}
			final QuoteData quoteData = populateQuoteData(catQuoteForm);
			//Begin Quote submit After the Transaction Begins.

			tx.begin();
			quoteFacade.submitQuote(catQuoteForm.getQuoteCode());

			if (null != catQuoteForm.getCatQuoteSerialNumbersData())
			{
				quoteFacade.saveReservationQuoteData(catQuoteForm.getQuoteCode(),
						catQuoteForm.getCatQuoteSerialNumbersData().getCatReserveQuoteData());
			}

			addCustomerInfo(catQuoteForm);

			final QuoteData returnedQuoteData = quoteFacade.saveQuoteDetails(quoteData, catQuoteForm.getQuoteCode());

			isTransSuccess = true;
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, QUOTE_SUBMIT_SUCCESS, null);
			return prepareResponseObject(returnedQuoteData, true, null, false);
		}
		catch (final CommerceQuoteAssignmentException cqae)
		{
			LOG.warn("Attempted to submit a quote that is assigned to another user: " + catQuoteForm.getQuoteCode(), cqae);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.INFO_MESSAGES_HOLDER, QUOTE_EDIT_LOCKED_ERROR, new Object[]
			{ catQuoteForm.getQuoteCode(), cqae.getAssignedUser() });
			return prepareResponseObject(null, false, "Attempted to submit a quote that is assigned to another user", false);
		}
		catch (final IllegalQuoteSubmitException e)
		{
			LOG.warn(QUOTE_FAILURE_REASON, e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_NOT_SUBMITABLE_ERROR);
			return prepareResponseObject(null, false, QUOTE_FAILURE_REASON, false);
		}
		catch (final QuoteUnderThresholdException e)
		{
			final double quoteThreshold = getQuoteFacade().getQuoteRequestThreshold(catQuoteForm.getQuoteCode());
			LOG.warn(String.format("Quote %s under threshold", catQuoteForm.getQuoteCode()), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_REJECT_INITIATION_REQUEST,
					new Object[]
					{ getFormattedPriceValue(quoteThreshold) });
			return prepareResponseObject(null, false, "Quote %s under threshold", false);
		}
		catch (final IllegalStateException | UnknownIdentifierException | ModelSavingException | IllegalArgumentException e)
		{
			LOG.error(String.format("Unable to submit quote %s", catQuoteForm.getQuoteCode()), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_SUBMIT_ERROR, null);
			return prepareResponseObject(null, false, QUOTE_FAILURE_REASON, false);
		}
		catch (final ParseException parseException)
		{
			LOG.error(String.format("Parse exception %s", catQuoteForm.getQuoteCode()), parseException);
			return prepareResponseObject(null, false, "Technical Error while saving Quote", false);
		}
		catch (final DuplicateUidException e)
		{
			LOG.warn("User with the " + catQuoteForm.getQuoteCode(), e);
			return prepareResponseObject(null, false, "Failed to save Customer info", true);
		}
		finally
		{
			if (isTransSuccess)
			{
				tx.commit();
			}
			else
			{
				if (tx.isRunning())
				{
					tx.rollback();
				}
			}
		}
	}

	/**
	 * @param quoteData
	 * @param success
	 * @param errorMessage
	 * @return CatQuoteResponseDTO
	 */
	private CatQuoteResponseDTO prepareResponseObject(final QuoteData quoteData, final boolean success, final String errorMessage,
			final boolean userExists)
	{
		final CatQuoteResponseDTO catQuoteResponseDTO = new CatQuoteResponseDTO();
		catQuoteResponseDTO.setStatus(Boolean.valueOf(success));
		catQuoteResponseDTO.setUserExists(Boolean.valueOf(userExists));
		if (quoteData != null)
		{
			catQuoteResponseDTO.setQuoteCustomer(quoteData.getCustomerName());
			catQuoteResponseDTO.setQuoteCode(quoteData.getCode());
			catQuoteResponseDTO.setQuoteName(quoteData.getName());
		}
		catQuoteResponseDTO.setErrorMessage(errorMessage);
		return catQuoteResponseDTO;

	}

	/**
	 * Populate quote data.
	 *
	 * @param catQuoteForm
	 *           the cat quote form
	 * @return the quote data
	 * @throws ParseException
	 *            the parse exception
	 */
	private QuoteData populateQuoteData(final CatQuoteForm catQuoteForm) throws ParseException
	{

		final QuoteData quoteData = new QuoteData();
		quoteData.setName(catQuoteForm.getName());
		quoteData.setDescription(catQuoteForm.getDescription());
		final SimpleDateFormat sdf = new SimpleDateFormat(CatCoreConstants.DATE_FORMAT);
		if (StringUtils.isNotEmpty(catQuoteForm.getExpirationTime()))
		{
			quoteData.setExpirationTime(sdf.parse(catQuoteForm.getExpirationTime()));
		}
		quoteData.setCustomerName(catQuoteForm.getCustomerName());
		quoteData.setCustomerEmail(catQuoteForm.getCustomerEmail());
		if (StringUtils.isNotEmpty(catQuoteForm.getEppAdditionalServices()))
		{
			quoteData.setEppAdditionalServices(catQuoteForm.getEppAdditionalServices());
		}
		if (StringUtils.isNotEmpty(catQuoteForm.getCsaAdditionalServices()))
		{
			quoteData.setCsaAdditionalServices(catQuoteForm.getCsaAdditionalServices());
		}
		if (StringUtils.isNotEmpty(catQuoteForm.getQuoteComments()))
		{
			quoteData.setQuoteComments(catQuoteForm.getQuoteComments());
		}
		quoteData.setSendToDSM(catQuoteForm.getSendToDSM());
		if (StringUtils.isNotEmpty(catQuoteForm.getCustomerResponseDate()))
		{
			quoteData.setAcceptedByCustomerDate(sdf.parse(catQuoteForm.getCustomerResponseDate()));
		}
		if (StringUtils.isNotEmpty(catQuoteForm.getSubmittedToCustomerDate()))
		{
			quoteData.setSubmittedToCustomerDate(sdf.parse(catQuoteForm.getSubmittedToCustomerDate()));
		}
		if (StringUtils.isNotEmpty(catQuoteForm.getQuoteLostReason()))
		{
			quoteData.setQuoteLostReason(CatQuoteLostReasonEnum.valueOf(catQuoteForm.getQuoteLostReason()));
		}
		if (StringUtils.isNotEmpty(catQuoteForm.getQuoteState()))
		{
			quoteData.setState(QuoteState.valueOf(catQuoteForm.getQuoteState()));
		}
		return quoteData;
	}


	/**
	 * Exports the quote document in word format
	 *
	 * @param quoteCode
	 *           to generate export for customer quotations.
	 * @param redirectModel
	 * @return Mapping redirect to quote page.
	 */

	@SuppressWarnings(
	{ "resource", "boxing" })
	@RequestMapping(value = "/{quoteCode}/export", method = RequestMethod.GET)
	public void exportQuote(@PathVariable("quoteCode") final String quoteCode, final Model model,
			final RedirectAttributes redirectModel, final HttpServletRequest request, final HttpServletResponse response)
			throws InvalidFormatException, IOException
	{

		final ClassLoader classLoader = getClass().getClassLoader();


		final FileInputStream fileStream = catQuoteExportFacade.getExportFileStream(CatCoreConstants.EXPORT_QUOTE_FILE);

		final QuoteData quoteData = getQuoteFacade().getQuoteForCode(quoteCode);

		final String hostURL = UrlUtils.extractHostInformationFromRequest(request);
		catQuoteExportFacade.prepareExportDocument(fileStream, quoteData, getUser(), getLocalesExportMap(), hostURL);

		final File updatedExportDoc = new File(classLoader.getResource(CatCoreConstants.UPDATED_EXPORT_QUOTE_FILE).getFile());

		final File filePDF = new File(updatedExportDoc.getAbsolutePath());

		catQuoteExportFacade.printExportWord(filePDF, response, quoteData);
	}


	public Map<String, String> getLocalesExportMap()
	{
		final Map<String, String> localeMap = new HashMap<String, String>();

		localeMap.put("cat.quote.export.product.details",
				getMessageSource().getMessage("cat.quote.export.product.details", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.machine.config",
				getMessageSource().getMessage("cat.quote.export.machine.config", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.machine.attachments",
				getMessageSource().getMessage("cat.quote.export.machine.attachments", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.quote.details",
				getMessageSource().getMessage("cat.quote.export.quote.details", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.quote.terms",
				getMessageSource().getMessage("cat.quote.export.quote.terms", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.quote.price",
				getMessageSource().getMessage("cat.quote.export.quote.price", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.quote.discount",
				getMessageSource().getMessage("cat.quote.export.quote.discount", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.quote.transaction",
				getMessageSource().getMessage("cat.quote.export.quote.transaction", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.quote.description",
				getMessageSource().getMessage("cat.quote.export.quote.description", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.quote.country",
				getMessageSource().getMessage("cat.quote.export.quote.country", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.last.date",
				getMessageSource().getMessage("cat.quote.export.last.date", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.csa.details",
				getMessageSource().getMessage("cat.quote.export.csa.details", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.epp.details",
				getMessageSource().getMessage("cat.quote.export.epp.details", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.customer.details",
				getMessageSource().getMessage("cat.quote.export.customer.details", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.cart.details",
				getMessageSource().getMessage("cat.quote.export.cart.details", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.cart.product.family",
				getMessageSource().getMessage("cat.quote.export.cart.product.family", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.cart.config.id",
				getMessageSource().getMessage("cat.quote.export.cart.config.id", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.cart.sales.model",
				getMessageSource().getMessage("cat.quote.export.cart.sales.model", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.cart.list.price",
				getMessageSource().getMessage("cat.quote.export.cart.list.price", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.cart.qty",
				getMessageSource().getMessage("cat.quote.export.cart.qty", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.cart.discount",
				getMessageSource().getMessage("cat.quote.export.cart.discount", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.cart.transaction",
				getMessageSource().getMessage("cat.quote.export.cart.transaction", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.customer.name",
				getMessageSource().getMessage("cat.quote.export.customer.name", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.customer.email",
				getMessageSource().getMessage("cat.quote.export.customer.email", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.customer.shipping.address", getMessageSource()
				.getMessage("cat.quote.export.customer.shipping.address", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.customer.billing.address", getMessageSource()
				.getMessage("cat.quote.export.customer.billing.address", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.company.name",
				getMessageSource().getMessage("cat.quote.export.company.name", null, getI18nService().getCurrentLocale()));
		localeMap.put("cat.quote.export.description",
				getMessageSource().getMessage("cat.quote.export.description", null, getI18nService().getCurrentLocale()));

		return localeMap;

	}

	/**
	 * Approve a quote from the sales representative
	 *
	 * @param quoteCode
	 * @param redirectModel
	 * @return Mapping of redirect to the quote details page.
	 */
	@RequestMapping(value = "/{quoteCode}/approve", method = RequestMethod.POST)
	public String approveQuote(@PathVariable("quoteCode") final String quoteCode, final RedirectAttributes redirectModel)
	{
		try
		{
			getQuoteFacade().approveQuote(quoteCode);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "quote.approve.success", null);
		}
		catch (final IllegalStateException | UnknownIdentifierException | ModelSavingException | IllegalArgumentException e)
		{
			LOG.error(String.format("Unable to approve quote %s", quoteCode), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "quote.approve.error", null);
			return REDIRECT_PREFIX + ROOT;
		}
		return String.format(REDIRECT_QUOTE_DETAILS_URL, urlEncode(quoteCode));
	}

	/**
	 * Reject a quote from the sales representative
	 *
	 * @param quoteCode
	 * @param redirectModel
	 * @return Mapping of redirect to the quote details page.
	 */
	@RequestMapping(value = "/{quoteCode}/reject", method = RequestMethod.POST)
	public String rejectQuote(@PathVariable("quoteCode") final String quoteCode, final RedirectAttributes redirectModel)
	{
		try
		{
			getQuoteFacade().rejectQuote(quoteCode);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, "quote.reject.success", null);
		}
		catch (final IllegalStateException | UnknownIdentifierException | ModelSavingException | IllegalArgumentException e)
		{
			LOG.error(String.format("Unable to reject quote %s", quoteCode), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "quote.reject.error", null);
			return REDIRECT_PREFIX + ROOT;
		}

		return String.format(REDIRECT_QUOTE_DETAILS_URL, urlEncode(quoteCode));
	}

	@RequestMapping(value = "/{quoteCode}/requote", method = RequestMethod.POST)
	public String requote(@PathVariable("quoteCode") final String quoteCode, final RedirectAttributes redirectModel)
	{

		try
		{
			removeCoupons(redirectModel);
			final QuoteData quoteData = getQuoteFacade().requote(quoteCode);

			return String.format(REDIRECT_QUOTE_EDIT_URL, urlEncode(quoteData.getCode()));
		}
		catch (final IllegalQuoteStateException | CannotCloneException | IllegalArgumentException | ModelSavingException e)
		{
			LOG.error("Unable to requote", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_REQUOTE_ERROR, null);
			return String.format(REDIRECT_QUOTE_DETAILS_URL, urlEncode(quoteCode));
		}
	}

	protected Optional<String> handleEditModeSubmitQuote(final String quoteCode, final QuoteForm quoteForm,
			final BindingResult bindingResult, final RedirectAttributes redirectModel)
	{
		final boolean isSeller = Functions.isQuoteUserSalesRep();
		final Object validationGroup = isSeller ? QuoteForm.Seller.class : QuoteForm.Buyer.class;

		smartValidator.validate(quoteForm, bindingResult, validationGroup);

		if (bindingResult.hasErrors())
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					isSeller ? "text.quote.expiration.time.invalid" : "text.quote.name.description.invalid", null);
			return Optional.of(String.format(REDIRECT_QUOTE_EDIT_URL, urlEncode(quoteCode)));
		}

		try
		{
			CommerceCartMetadata cartMetadata;
			if (isSeller)
			{
				final Optional<Date> expirationTime = Optional.ofNullable(getExpirationDateFromString(quoteForm.getExpirationTime()));
				cartMetadata = CommerceCartMetadataUtils.metadataBuilder().expirationTime(expirationTime)
						.removeExpirationTime(!expirationTime.isPresent()).build();
			}
			else
			{
				cartMetadata = CommerceCartMetadataUtils.metadataBuilder().name(Optional.ofNullable(quoteForm.getName()))
						.description(Optional.ofNullable(quoteForm.getDescription())).build();
			}

			getCartFacade().updateCartMetadata(cartMetadata);
		}
		catch (final IllegalArgumentException e)
		{
			LOG.warn(String.format("Invalid metadata input field(s) for quote %s", quoteCode), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					isSeller ? "text.quote.expiration.time.invalid" : "text.quote.name.description.invalid", null);
			return Optional.of(String.format(REDIRECT_QUOTE_EDIT_URL, urlEncode(quoteCode)));
		}

		return Optional.empty();
	}

	@RequestMapping(value = "/{quoteCode}/newcart", method = RequestMethod.GET)
	public String newCart(@PathVariable("quoteCode") final String quoteCode, final RedirectAttributes redirectModel)
			throws CMSItemNotFoundException
	{
		try
		{
			removeCoupons(redirectModel);
			final QuoteData quoteData = quoteFacade.newCart();
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER, QUOTE_NEWCART_SUCCESS, new Object[]
			{ quoteData.getCode() });
			return REDIRECT_CART_URL;
		}
		catch (final IllegalArgumentException e)
		{
			LOG.error("Unable to sync cart into quote. Illegal argument used to invoke a method", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_NEWCART_ERROR, null);
			return String.format(REDIRECT_QUOTE_EDIT_URL, urlEncode(quoteCode));
		}
		catch (final SystemException e)
		{
			LOG.error("Unable to save quote while trying to close quote edit mode", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_NEWCART_ERROR, null);
			return String.format(REDIRECT_QUOTE_EDIT_URL, urlEncode(quoteCode));
		}
	}

	/**
	 * Place an order for the given quote.
	 *
	 * @param quoteCode
	 * @param redirectModel
	 * @return Mapping of redirect to the checkout page.
	 */
	@RequestMapping(value = "/{quoteCode}/checkout", method = RequestMethod.POST)
	public String placeOrder(@PathVariable("quoteCode") final String quoteCode, final RedirectAttributes redirectModel)
	{
		try
		{
			getQuoteFacade().acceptAndPrepareCheckout(quoteCode);
		}
		catch (final CommerceQuoteExpirationTimeException e)
		{
			LOG.warn(String.format("Quote has Expired. Quote Code : [%s]", quoteCode), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_EXPIRED, null);
			return String.format(REDIRECT_QUOTE_DETAILS_URL, urlEncode(quoteCode));
		}
		catch (final UnknownIdentifierException e)
		{
			LOG.warn(String.format("Attempted to place order with a quote that does not exist or is not visible: %s", quoteCode), e);
			return REDIRECT_QUOTE_LIST_URL;
		}
		catch (final SystemException e)
		{
			LOG.warn("There was error saving the session cart", e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, QUOTE_SAVE_CART_ERROR, null);
			return String.format(REDIRECT_QUOTE_DETAILS_URL, urlEncode(quoteCode));
		}
		return getCheckoutRedirectUrl();
	}

	@ResponseBody
	@RequestMapping(value = "/{quoteCode}/expiration", method = RequestMethod.POST)
	public ResponseEntity<String> setQuoteExpiration(@PathVariable("quoteCode") final String quoteCode, final QuoteForm quoteForm,
			final BindingResult bindingResult)
	{
		smartValidator.validate(quoteForm, bindingResult, QuoteForm.Seller.class);

		if (bindingResult.hasErrors())
		{
			final String errorMessage = getMessageSource().getMessage(bindingResult.getAllErrors().get(0).getCode(), null,
					getI18nService().getCurrentLocale());
			return new ResponseEntity<String>(errorMessage, HttpStatus.BAD_REQUEST);
		}

		try
		{
			final Optional<Date> expirationTime = Optional.ofNullable(getExpirationDateFromString(quoteForm.getExpirationTime()));
			final CommerceCartMetadata cartMetadata = CommerceCartMetadataUtils.metadataBuilder().expirationTime(expirationTime)
					.removeExpirationTime(!expirationTime.isPresent()).build();

			getCartFacade().updateCartMetadata(cartMetadata);
		}
		catch (final IllegalArgumentException e)
		{
			LOG.warn(String.format("Invalid expiration time input for quote %s", quoteCode), e);
			return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
		}
		catch (final IllegalStateException | IllegalQuoteStateException | UnknownIdentifierException | ModelSavingException e)
		{
			LOG.error(String.format("Failed to update expiration time for quote %s", quoteCode), e);
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<String>(HttpStatus.OK);
	}

	/**
	 * Update quote name and description
	 *
	 * @param quoteCode
	 * @param quoteForm
	 * @param bindingResult
	 * @return response entity
	 */
	@ResponseBody
	@RequestMapping(value = "/{quoteCode}/metadata", method = RequestMethod.POST)
	public ResponseEntity<String> setQuoteMetadata(@PathVariable("quoteCode") final String quoteCode, final QuoteForm quoteForm,
			final BindingResult bindingResult)
	{
		smartValidator.validate(quoteForm, bindingResult, QuoteForm.Buyer.class);

		if (bindingResult.hasErrors())
		{
			final String errorMessage = getMessageSource().getMessage(bindingResult.getAllErrors().get(0).getCode(), null,
					getI18nService().getCurrentLocale());
			return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
		}

		try
		{
			final Optional<String> quoteName = Optional.ofNullable(quoteForm.getName());
			final Optional<String> quoteDescription = Optional.ofNullable(quoteForm.getDescription());

			final CommerceCartMetadata cartMetadata = CommerceCartMetadataUtils.metadataBuilder().name(quoteName)
					.description(quoteDescription).build();

			getCartFacade().updateCartMetadata(cartMetadata);
		}
		catch (final IllegalArgumentException e)
		{
			LOG.warn(String.format("Invalid metadata input for quote %s", quoteCode), e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		catch (final IllegalStateException | UnknownIdentifierException | ModelSavingException e)
		{
			LOG.error(String.format("Failed to update metadata for quote %s", quoteCode), e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	protected Date getExpirationDateFromString(final String expirationTime)
	{
		final Locale currentLocale = getI18nService().getCurrentLocale();
		final String expirationTimePattern = getMessageSource().getMessage(DATE_FORMAT_KEY, null, currentLocale);

		return QuoteExpirationTimeConverter.convertStringToDate(expirationTime, expirationTimePattern, currentLocale);
	}

	/**
	 * Add a quote comment to a given quote.
	 *
	 * @param comment
	 * @param redirectModel
	 * @return Mapping of redirect to the quote details page.
	 */
	@RequestMapping(value = "/comment", method = RequestMethod.POST)
	public ResponseEntity<String> addQuoteComment(@RequestParam("comment") final String comment,
			final RedirectAttributes redirectModel)
	{
		try
		{
			getQuoteFacade().addComment(comment);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
					"text.confirmation.quote.comment.added");
		}
		catch (final IllegalArgumentException e)
		{
			LOG.warn("Attempted to add a comment that is invalid", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/entry/comment", method = RequestMethod.POST)
	public ResponseEntity<String> addQuoteEntryComment(@RequestParam("entryNumber") final long entryNumber,
			@RequestParam("comment") final String comment, final RedirectAttributes redirectModel)
	{
		try
		{
			getQuoteFacade().addEntryComment(entryNumber, comment);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
					"text.confirmation.quote.comment.added");
		}
		catch (final IllegalArgumentException e)
		{
			LOG.warn("Attempted to add an entry comment that is invalid", e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}

	protected void sortComments(final CartData cartData)
	{
		if (cartData != null)
		{
			if (CollectionUtils.isNotEmpty(cartData.getComments()))
			{
				final List<CommentData> sortedComments = cartData.getComments().stream()
						.sorted((comment1, comment2) -> comment2.getCreationDate().compareTo(comment1.getCreationDate()))
						.collect(Collectors.toList());
				cartData.setComments(sortedComments);
			}

			if (CollectionUtils.isNotEmpty(cartData.getEntries()))
			{
				setComments(cartData);
			}
		}
	}

	public void setComments(final CartData cartData)
	{
		for (final OrderEntryData orderEntry : cartData.getEntries())
		{
			if (CollectionUtils.isNotEmpty(orderEntry.getComments()))
			{
				final List<CommentData> sortedEntryComments = orderEntry.getComments().stream()
						.sorted((comment1, comment2) -> comment2.getCreationDate().compareTo(comment1.getCreationDate()))
						.collect(Collectors.toList());

				orderEntry.setComments(sortedEntryComments);
			}
			else if (orderEntry.getProduct() != null && orderEntry.getProduct().getMultidimensional() != null
					&& Boolean.TRUE.equals(orderEntry.getProduct().getMultidimensional())
					&& CollectionUtils.isNotEmpty(orderEntry.getEntries()))
			{

				setMultipleOrderEntries(orderEntry);
			}
		}

	}

	/**
	 * @param orderEntry
	 *           - the order entry
	 */
	public void setMultipleOrderEntries(final OrderEntryData orderEntry)
	{
		for (final OrderEntryData multiDOrderEntry : orderEntry.getEntries())
		{
			if (CollectionUtils.isNotEmpty(multiDOrderEntry.getComments()))
			{
				final List<CommentData> sortedMultiDOrderEntryComments = multiDOrderEntry.getComments().stream()
						.sorted((comment1, comment2) -> comment2.getCreationDate().compareTo(comment1.getCreationDate()))
						.collect(Collectors.toList());

				multiDOrderEntry.setComments(sortedMultiDOrderEntryComments);
			}
		}

	}

	protected void loadCommentsShown(final Model model)
	{
		final int commentsShown = getSiteConfigService().getInt(PAGINATION_NUMBER_OF_COMMENTS, 5);
		model.addAttribute("commentsShown", Integer.valueOf(commentsShown));
	}

	/**
	 * Set allowed actions for a given quote on model.
	 *
	 * @param model
	 *           the MVC model
	 * @param quoteCode
	 *           the quote to be checked.
	 */
	protected void setAllowedActions(final Model model, final String quoteCode)
	{
		final Set<QuoteAction> actionSet = getQuoteFacade().getAllowedActions(quoteCode);

		if (actionSet != null)
		{
			final Map<String, Boolean> actionsMap = actionSet.stream()
					.collect(Collectors.toMap(v -> v.getCode(), v -> Boolean.TRUE));
			model.addAttribute(ALLOWED_ACTIONS, actionsMap);
		}
	}

	@ExceptionHandler(IllegalQuoteStateException.class)
	public String handleIllegalQuoteStateException(final IllegalQuoteStateException exception, final HttpServletRequest request)
	{
		final Map<String, Object> currentFlashScope = RequestContextUtils.getOutputFlashMap(request);

		LOG.warn("Invalid quote state for performed action.", exception);

		final String statusMessageKey = String.format("text.account.quote.status.display.%s", exception.getQuoteState());
		final String actionMessageKey = String.format("text.account.quote.action.display.%s", exception.getQuoteAction());

		GlobalMessages.addFlashMessage(currentFlashScope, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.quote.illegal.state.error",
				new Object[]
				{ getMessageSource().getMessage(actionMessageKey, null, getI18nService().getCurrentLocale()), exception.getQuoteCode(), getMessageSource().getMessage(statusMessageKey, null, getI18nService().getCurrentLocale()) });


		return REDIRECT_QUOTE_LIST_URL;
	}

	/**
	 * Get formatted monetary value with currency symbol
	 *
	 * @param value
	 *           the value to be formatted
	 *
	 * @return formatted threshold string
	 */
	protected String getFormattedPriceValue(final double value)
	{
		return priceDataFactory.create(PriceDataType.BUY, BigDecimal.valueOf(value), getCurrentCurrency().getIsocode())
				.getFormattedValue();
	}

	protected ResourceBreadcrumbBuilder getResourceBreadcrumbBuilder()
	{
		return resourceBreadcrumbBuilder;
	}

	protected QuoteFacade getQuoteFacade()
	{
		return quoteFacade;
	}

	protected VoucherFacade getVoucherFacade()
	{
		return voucherFacade;
	}

	/**
	 * Adds the customer info.
	 *
	 * @param catQuoteForm
	 *           the cat quote form
	 * @throws DuplicateUidException
	 *            the duplicate uid exception
	 */
	private void addCustomerInfo(final CatQuoteForm catQuoteForm) throws DuplicateUidException
	{
		try
		{
			createOrUpdateCustomer(catQuoteForm);
			final AddressForm shippingAddress = catQuoteForm.getDefaultShippingAddress();
			shippingAddress.setShippingAddress(Boolean.TRUE);
			final AddressData shippingAddressData = addressDataUtil.convertToVisibleAddressData(shippingAddress);
			shippingAddressData.setEmail(catQuoteForm.getCustomerEmail());
			QuoteData quoteData;

			if (catQuoteForm.getIsShippingSameAsBilling().booleanValue())
			{
				shippingAddressData.setDefaultAddress(true);
				shippingAddressData.setBillingAddress(true);
				quoteFacade.updateQuoteAddress(catQuoteForm.getQuoteCode(), shippingAddressData);
				quoteData = quoteFacade.getQuoteForCode(catQuoteForm.getQuoteCode());
				if (quoteData.getDeliveryAddress() != null)
				{
					shippingAddressData.setId(quoteData.getDeliveryAddress().getId());
				}
				shippingAddressData.setEmail(catQuoteForm.getCustomerEmail());
				shippingAddressData.setBillingAddress(false);
				addOrEditAddress(catQuoteForm, shippingAddressData);

			}
			else
			{
				quoteFacade.updateQuoteAddress(catQuoteForm.getQuoteCode(), shippingAddressData);
				quoteData = quoteFacade.getQuoteForCode(catQuoteForm.getQuoteCode());
				if (quoteData.getDeliveryAddress() != null)
				{
					shippingAddressData.setId(quoteData.getDeliveryAddress().getId());
				}
				addOrEditAddress(catQuoteForm, shippingAddressData);
				shippingAddressData.setEmail(catQuoteForm.getCustomerEmail());
				final AddressForm billingAddress = catQuoteForm.getDefaultBillingAddress();
				billingAddress.setBillingAddress(Boolean.TRUE);
				final AddressData billingAddressData = addressDataUtil.convertToVisibleAddressData(billingAddress);
				billingAddressData.setBillingAddress(true);
				billingAddressData.setShippingAddress(false);
				billingAddressData.setEmail(catQuoteForm.getCustomerEmail());
				quoteFacade.updateQuoteAddress(catQuoteForm.getQuoteCode(), billingAddressData);
				quoteData = quoteFacade.getQuoteForCode(catQuoteForm.getQuoteCode());
				if (quoteData.getPaymentAddress() != null)
				{
					billingAddressData.setId(quoteData.getPaymentAddress().getId());
				}
				addOrEditAddress(catQuoteForm, billingAddressData);
			}
		}
		catch (final Exception e)
		{
			throw new DuplicateUidException("Fault! An account with that email address already exists", e);
		}
	}


	/**
	 * @param catQuoteForm
	 */
	private void createOrUpdateCustomer(final CatQuoteForm catQuoteForm)
	{
		if (!catQuoteForm.getExistingCustomer().booleanValue())
		{
			userFacade.createCustomer(catQuoteForm.getCustomerName(), catQuoteForm.getCustomerEmail());
		}
		else
		{
			userFacade.updateCustomer(catQuoteForm.getCustomerName(), catQuoteForm.getCustomerEmail(),
					catQuoteForm.getCustomerUid());
		}
	}

	/**
	 * Adding or editing the address based on existing customer flag
	 *
	 * @param catQuoteForm
	 * @param shippingAddressData
	 */
	private void addOrEditAddress(final CatQuoteForm catQuoteForm, final AddressData shippingAddressData)
	{
		if (!catQuoteForm.getExistingCustomer().booleanValue())
		{
			userFacade.addAddress(shippingAddressData, catQuoteForm.getCustomerEmail());
		}
		else
		{
			userFacade.editAddress(shippingAddressData, catQuoteForm.getCustomerEmail());
		}
	}

	/**
	 * Fetching suggestions based on name input.
	 *
	 * @param name
	 *           fetch based on this input.
	 *
	 * @return List<String> suggestions object.
	 */
	@ResponseBody
	@RequestMapping(value = "/nameSuggestions/{name}", method = RequestMethod.GET)
	public List<String> getCustomerNameSuggestions(@PathVariable("name") final String name)
	{
		return defaultCatCustomerFacade.getCustomerNameSuggestions(urlEncode(name));
	}

	/**
	 * Fetching customer data with name.
	 *
	 * @param name
	 *           fetch based on this input.
	 *
	 * @return CustomerData customer data json object.
	 */
	@ResponseBody
	@RequestMapping(value = "/customerNameDetails/{name}", method = RequestMethod.GET)
	public CustomerData fetchExistingCustomerDetailsByName(@PathVariable("name") final String name)
	{
		return defaultCatCustomerFacade.fetchExistingCustomerDetailsByName(urlEncode(name));
	}

	/**
	 * Fetching suggestions based on email input.
	 *
	 * @param email
	 *           fetch based on this input.
	 *
	 * @return List<String> suggestions object.
	 */
	@ResponseBody
	@RequestMapping(value = "/emailSuggestions/{email}", method = RequestMethod.GET)
	public List<String> getCustomerEmailSuggestions(@PathVariable("email") final String email)
	{
		return defaultCatCustomerFacade.getCustomerEmailSuggestions(urlEncode(email));
	}

	/**
	 * Fetching customer data with email.
	 *
	 * @param email
	 *           fetch based on this input.
	 *
	 * @return CustomerData customer data json object.
	 */
	@ResponseBody
	@RequestMapping(value = "/customerEmailDetails/{email}", method = RequestMethod.GET)
	public CustomerData fetchExistingCustomerDetailsByEmail(@PathVariable("email") final String email)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("email Value" + email);
		}
		return defaultCatCustomerFacade.fetchExistingCustomerDetailsByEmail(urlEncode(email));
	}


	/**
	 * Adds discount to an existing quote.
	 *
	 * @param quoteCode
	 *           Quote to have discounts applied.
	 * @param form
	 *           Discount info.
	 * @param redirectModel
	 * @return Mapping redirect to quote page.
	 */
	@ResponseBody
	@RequestMapping(value = "{quoteCode}/discountEntryLevel/apply", method = RequestMethod.POST)
	public CartData applyDiscountEntryLevel(@PathVariable("quoteCode") final String quoteCode, final CatQuoteDiscountForm form,
			final RedirectAttributes redirectModel, final HttpServletResponse response)
	{
		try
		{
			quoteFacade.applyQuoteDiscountEntryLevel(form.getEntryNumber(), form.getDiscountRate().doubleValue(),
					DiscountType.valueOf(form.getDiscountType()));
		}
		catch (final IllegalArgumentException e)
		{
			LOG.error(String.format("Error applying discount for quote %s", quoteCode), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"text.quote.discount.apply.argument.invalid.error", null);
			final CartData cartData = getCartFacade().getSessionCartWithEntryOrdering(false);
			quoteFacade.setErrorDataForCart(cartData, form.getEntryNumber(), e.getMessage());
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return cartData;

		}
		catch (final SystemException e)
		{
			LOG.error(String.format(CALCULATE_CART_ERROR_MESSAGE, quoteCode), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER,
					"text.quote.discount.apply.calculation.error", null);
			final CartData cartData = getCartFacade().getSessionCartWithEntryOrdering(false);
			quoteFacade.setErrorDataForCart(cartData, form.getEntryNumber(), e.getMessage());
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return cartData;
		}
		final CartData cartData = getCartFacade().getSessionCartWithEntryOrdering(false);
		quoteFacade.setCatQuoteErrorData(cartData);
		return cartData;
	}



	/**
	 * Method to refresh the cart data removing discount values
	 *
	 * @param quoteCode
	 *           quote code for which values have to be removed
	 *
	 * @return CartData cart data object
	 */
	@ResponseBody
	@RequestMapping(value = "{quoteCode}/refreshCartData", method = RequestMethod.GET)
	public CartData refreshCartData(@PathVariable("quoteCode") final String quoteCode)
	{
		try
		{
			quoteFacade.refreshCartData();
		}
		catch (final IllegalArgumentException e)
		{
			LOG.error(String.format("Error removing discount for quote %s", quoteCode), e);
		}
		catch (final SystemException e)
		{
			LOG.error(String.format(CALCULATE_CART_ERROR_MESSAGE, quoteCode), e);
		}
		final CartData cartData = getCartFacade().getSessionCartWithEntryOrdering(false);
		quoteFacade.setCatQuoteErrorData(cartData);
		return cartData;
	}

	/**
	 * Method to set the expiration date in quote data
	 *
	 * @param data
	 *           data to set the expiration date
	 *
	 * @param creationDate
	 *           expiration date will be some value ahead of this date
	 */
	private void setExpirationDateForQuote(final AbstractOrderData data, final Date creationDate)
	{
		final Calendar c = Calendar.getInstance();
		c.setTime(creationDate);
		c.add(Calendar.DATE, QuoteExpirationTimeUtils.getDefaultOfferValidityPeriodDays()); // number of days to add
		data.setExpirationTime(c.getTime());
	}


	/**
	 * Method to get the reason list why the quote was lost
	 *
	 *
	 */
	@RequestMapping(value = "/quoteLostReasons", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, CatQuoteLostReasonEnum> getQuotationLostReasons()
	{
		return quoteFacade.getQuotationLostReasons();
	}

	/**
	 * Method to get the quote states which a sales person can see
	 *
	 *
	 */
	@RequestMapping(value = "/quoteStatuses", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, QuoteState> getQuoteStatusesForSalesPerson()
	{
		return quoteFacade.getQuoteStatusesForSalesPerson();
	}


	/**
	 * Method to delete the quote if a user navigates away from the quote page
	 *
	 * @param quoteCode
	 *           quote which is to be deleted
	 */
	@ResponseBody
	@RequestMapping(value = "{quoteCode}/deleteQuotes", method = RequestMethod.DELETE)
	public void deleteQuotes(@PathVariable("quoteCode") final String quoteCode)
	{
		try
		{
			quoteFacade.deleteQuote(quoteCode);
		}
		catch (final Exception e)
		{
			LOG.error(String.format("Error while removing quote %s", quoteCode), e);
		}
	}

	@RequestMapping(value = "/{quoteCode}/updateQuotes", method = RequestMethod.GET)
	public void updateQuotes(@PathVariable("quoteCode") final String quoteCode,
			@RequestParam(value = "submittedToDSU", defaultValue = "false") final boolean submittedToDSU)
	{
		if (StringUtils.isNotEmpty(quoteCode))
		{

			try
			{
				quoteFacade.updateQuoteStatus(quoteCode, submittedToDSU);
			}
			catch (final Exception e)
			{
				LOG.error("Error in updating Update Quote Statuses", e);
			}
		}
	}

	/**
	 * Method to fetch the quote details on quote Edit Page
	 *
	 * @param quoteCode
	 *           quote which is to be fetched
	 */
	@ResponseBody
	@RequestMapping(value = "{quoteCode}/quoteDetails", method = RequestMethod.GET)

	public CatQuoteResponseData getQuoteDetails(@PathVariable("quoteCode") final String quoteCode)
	{
		final CatQuoteResponseData catQuoteResponseData = new CatQuoteResponseData();
		try
		{
			final QuoteData quoteData = getQuoteFacade().getQuoteForCode(quoteCode);
			BeanUtils.copyProperties(quoteData, catQuoteResponseData);
		}
		catch (final Exception e)
		{
			LOG.error(String.format("Error while fetching quote details %s", quoteCode), e);
		}
		return catQuoteResponseData;
	}

	/**
	 * This method is used to get List of Serial Numbers that can be reserved for given Config Variant Product.
	 *
	 * @param productCode
	 * @return CatQuoteReserveForm
	 * @throws CatException
	 */
	@ResponseBody
	@RequestMapping(value = "/getReservationQuoteData", method = RequestMethod.GET)
	public CatQuoteSerialNumbersData getReservationQuoteData(@RequestParam(value = "productCode") final String productCode)
	{
		final CatQuoteSerialNumbersData catQuoteSerialNumbersData = new CatQuoteSerialNumbersData();
		final B2BCustomerModel b2bCustomerModel = (B2BCustomerModel) userService.getCurrentUser();
		final String dealerCode = b2bCustomerModel.getDefaultB2BUnit().getUid();
		final List<CatReserveQuoteData> catReserveQuoteDataList = new ArrayList<>();
		try
		{

			final CatReserveQuoteData catReserveQuoteData = catIntegrationService.getSerialNumbersData(productCode, dealerCode);
			if (null != catReserveQuoteData)
			{
				catReserveQuoteDataList.add(catReserveQuoteData);
				catQuoteSerialNumbersData.setCatReserveQuoteData(catReserveQuoteDataList);
			}
		}
		catch (final CatException e)
		{
			LOG.error("Error while fetching Serial Numbers data ", e);
		}
		return catQuoteSerialNumbersData;
	}

	/**
	 * Method to replicate Quote from an existing user quote.
	 *
	 * @param quoteCode
	 *           quote which needs to be verified for replicate functionality
	 *
	 * @param removeSessionCart
	 *           parameter to identify if we need to remove cart from session or not
	 * @return CatQuoteResponseDTO
	 */
	@ResponseBody
	@RequestMapping(value = "/{quoteCode}/replicate", method = RequestMethod.GET)
	public CatQuoteResponseDTO verifyReplicateQuote(@PathVariable("quoteCode") final String quoteCode,
			@RequestParam(value = "removeSessionCart", defaultValue = "false") final boolean removeSessionCart,
			@RequestParam(value = "allowReplicate", defaultValue = "false") final boolean allowReplicate)
	{
		final CatQuoteResponseDTO responseDto = new CatQuoteResponseDTO();

		//verify if there is a cart in user session
		if (!removeSessionCart && getCartFacade().getSessionCart() != null
				&& CollectionUtils.isNotEmpty(getCartFacade().getSessionCart().getEntries()))
		{
			//session cart exists. Send status as false to notify customer that there is a session cart
			responseDto.setStatus(Boolean.FALSE);
			responseDto.setCartExists(Boolean.TRUE);
			responseDto.setErrorMessage("Your products in the cart would be lost. Do you want to proceed ? ");
			return responseDto;
		}
		//if removeSessionCart is true, remove session cart.
		clearCart(removeSessionCart);

		//replicate quote
		final QuoteData replicatedQuoteData = quoteFacade.requote(quoteCode);
		responseDto.setQuoteCode(replicatedQuoteData.getCode());
		responseDto.setQuoteName(replicatedQuoteData.getName());
		responseDto.setQuoteCustomer(replicatedQuoteData.getCustomerName());
		responseDto.setStatus(Boolean.TRUE);
		sessionService.setAttribute("replicatedQuote", quoteCode);
		return responseDto;
	}

	/**
	 * Method to validate cart before edit quote
	 *
	 * @param quoteCode
	 *           the quote code
	 *
	 * @param removeSessionCart
	 *           parameter to identify if we need to remove cart from session or not
	 * @return CatQuoteResponseDTO
	 */
	@ResponseBody
	@RequestMapping(value = "/{quoteCode}/editCheck", method = RequestMethod.GET)
	public CatQuoteResponseDTO verifyEditQuote(@PathVariable("quoteCode") final String quoteCode,
			@RequestParam(value = "removeSessionCart", defaultValue = "false") final boolean removeSessionCart,
			@RequestParam(value = "allowReplicate", defaultValue = "false") final boolean allowReplicate)
	{
		final CatQuoteResponseDTO responseDto = new CatQuoteResponseDTO();

		//verify if there is a cart in user session
		if (!removeSessionCart && getCartFacade().getSessionCart() != null
				&& CollectionUtils.isNotEmpty(getCartFacade().getSessionCart().getEntries()))
		{
			//session cart exists. Send status as false to notify customer that there is a session cart
			responseDto.setStatus(Boolean.FALSE);
			responseDto.setCartExists(Boolean.TRUE);
			responseDto.setErrorMessage("Your products in the cart would be lost. Do you want to proceed ? ");
			return responseDto;
		}
		//if removeSessionCart is true, remove session cart.
		clearCart(removeSessionCart);

		//replicate quote
		responseDto.setQuoteCode(quoteCode);
		responseDto.setStatus(Boolean.TRUE);
		return responseDto;
	}

	/**
	 * Adds the value estimation entry level.
	 *
	 * @param quoteCode
	 *           the quote code
	 * @param form
	 *           the form
	 * @param response
	 *           the response
	 * @return the cart data
	 */
	@ResponseBody
	@RequestMapping(value = "/{quoteCode}/valueEstimation/add", method = RequestMethod.POST)
	public CartData addValueEstimationEntryLevel(@PathVariable("quoteCode") final String quoteCode,
			@RequestBody final CatQuoteVETForm form, final RedirectAttributes redirectModel, final HttpServletResponse response)
	{
		try
		{
			quoteFacade.addValueEstimationForQuoteEntry(form.getVetLink(), form.getVetProduct(), form.getEntryNumber());
		}
		catch (final IllegalArgumentException | SystemException e)
		{
			LOG.error(String.format("Error adding value estimation %s", quoteCode), e);
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.quote.vet.add.error", null);
			final CartData cartData = getCartFacade().getSessionCartWithEntryOrdering(false);
			quoteFacade.setErrorDataForCart(cartData, form.getEntryNumber(), e.getMessage());
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return cartData;

		}

		final CartData cartData = getCartFacade().getSessionCartWithEntryOrdering(false);
		quoteFacade.setCatQuoteErrorData(cartData);
		return cartData;
	}

	/**
	 * Removes the value estimation entry level.
	 *
	 * @param quoteCode
	 *           the quote code
	 * @param form
	 *           the form
	 * @param response
	 *           the response
	 * @return the cart data
	 */
	@ResponseBody
	@RequestMapping(value = "/{quoteCode}/valueEstimation/remove", method = RequestMethod.POST)
	public CartData removeValueEstimationEntryLevel(@PathVariable("quoteCode") final String quoteCode,
			@RequestBody final CatQuoteVETForm form, final RedirectAttributes redirectModel, final HttpServletResponse response)
	{
		try
		{
			quoteFacade.removeValueEstimationForQuoteEntry(form.getVetLink(), form.getVetProduct(), form.getEntryNumber());
		}
		catch (final IllegalArgumentException | SystemException e)
		{
			LOG.error(String.format("Error removing value estimation %s", quoteCode), e);

			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.quote.vet.remove.error", null);

			final CartData cartData = getCartFacade().getSessionCartWithEntryOrdering(false);
			quoteFacade.setErrorDataForCart(cartData, form.getEntryNumber(), e.getMessage());
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return cartData;

		}

		final CartData cartData = getCartFacade().getSessionCartWithEntryOrdering(false);
		quoteFacade.setCatQuoteErrorData(cartData);
		return cartData;
	}

	/**
	 * Edits the value estimation entry level.
	 *
	 * @param quoteCode
	 *           the quote code
	 * @param form
	 *           the form
	 * @param redirectModel
	 *           the redirect model
	 * @param response
	 *           the response
	 * @return the cart data
	 */
	@ResponseBody
	@RequestMapping(value = "/{quoteCode}/valueEstimation/editSave", method = RequestMethod.POST)
	public CartData editValueEstimationEntryLevel(@PathVariable("quoteCode") final String quoteCode,
			@RequestBody final CatQuoteVETForm form, final RedirectAttributes redirectModel, final HttpServletResponse response)
	{
		try
		{
			quoteFacade.editValueEstimationForQuoteEntry(form.getCatQuoteVETDatas(), form.getEntryNumber());
		}
		catch (final IllegalArgumentException | SystemException e)
		{
			LOG.error(String.format("Error editing value estimation %s", quoteCode), e);

			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, "text.quote.vet.edit.error", null);

			final CartData cartData = getCartFacade().getSessionCartWithEntryOrdering(false);
			quoteFacade.setErrorDataForCart(cartData, form.getEntryNumber(), e.getMessage());
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return cartData;

		}

		final CartData cartData = getCartFacade().getSessionCartWithEntryOrdering(false);
		quoteFacade.setCatQuoteErrorData(cartData);
		return cartData;
	}
}
