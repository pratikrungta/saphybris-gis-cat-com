/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.storefront.controllers.pages;

import de.hybris.platform.acceleratorcms.model.components.SearchBoxComponentModel;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.acceleratorservices.customer.CustomerLocationService;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.SearchBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController;
import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.category.CategoryService;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.servicelayer.services.CMSComponentService;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.AutocompleteResultData;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.enums.SearchQueryContext;
import de.hybris.platform.commerceservices.search.facetdata.FacetData;
import de.hybris.platform.commerceservices.search.facetdata.FacetRefinement;
import de.hybris.platform.commerceservices.search.facetdata.FacetValueData;
import de.hybris.platform.commerceservices.search.facetdata.ProductSearchPageData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.PaginationData;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.security.PrincipalModel;
import de.hybris.platform.core.model.user.UserGroupModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.util.Config;
import de.hybris.platform.variants.model.VariantProductModel;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.enums.CatIndexedTypeEnum;
import com.cat.bcp.core.enums.ProductConditionEnum;
import com.cat.bcp.core.model.AlertProductModel;
import com.cat.bcp.core.model.BaseVariantProductModel;
import com.cat.bcp.core.model.ConfigVariantProductModel;
import com.cat.bcp.core.model.RestockAlertModel;
import com.cat.bcp.core.servicelayer.CatProductService;
import com.cat.bcp.facades.comparator.CatProductSeatCapacityComparator;
import com.cat.bcp.facades.customer.CatCustomerFacade;
import com.cat.bcp.facades.order.CatCartFacade;
import com.cat.bcp.facades.product.data.AutoSuggestResultsData;
import com.cat.bcp.facades.search.DealerSearchFacade;
import com.cat.bcp.facades.speccheck.CatSpeccheckFacade;
import com.cat.bcp.storefront.forms.CatSearchRequestForm;
import com.sap.security.core.server.csi.XSSEncoder;



@Controller
@RequestMapping("/search")
public class SearchPageController extends AbstractSearchPageController
{
	private static final String SEARCH_META_DESCRIPTION_ON = "search.meta.description.on";
	private static final String SEARCH_META_DESCRIPTION_RESULTS = "search.meta.description.results";

	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(SearchPageController.class);

	private static final String COMPONENT_UID_PATH_VARIABLE_PATTERN = "{componentUid:.*}";
	private static final String FACET_SEPARATOR = ":";

	private static final String SEARCH_CMS_PAGE_ID = "search";
	private static final String NO_RESULTS_CMS_PAGE_ID = "searchEmpty";
	public static final String RELEVANCE_SORT = ":name-asc";

	public static final String PRIMARY_OFFERING_ID = "primaryOfferingId";
	public static final String SALES_MODEL_ID = "salesModelId";
	public static final String CATEGORY_CODE = "categoryCode";

	public static final String PRODUCTFAMILYNAMESUGGESTER = "productFamilyNameSuggester";


	@Resource(name = "productSearchFacade")
	private ProductSearchFacade<ProductData> productSearchFacade;

	@Resource(name = "searchBreadcrumbBuilder")
	private SearchBreadcrumbBuilder searchBreadcrumbBuilder;

	@Resource(name = "customerLocationService")
	private CustomerLocationService customerLocationService;

	@Resource(name = "cmsComponentService")
	private CMSComponentService cmsComponentService;

	@Resource(name = "categoryService")
	private CategoryService categoryService;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "productConverter")
	private Converter<ProductModel, ProductData> productConverter;

	@Resource(name = "catCustomerFacade")
	private CatCustomerFacade catCustomerFacade;

	@Resource(name = "dealerSearchFacade")
	private DealerSearchFacade dealerSearchFacade;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "sessionService")
	SessionService sessionService;

	@Resource(name = "configurationService")
	private ConfigurationService configurationService;

	@Resource(name = "customerFacade")
	private CustomerFacade customerFacade;

	@Resource(name = "catProductService")
	private CatProductService catProductService;


	@Resource(name = "catSpeccheckFacade")
	private CatSpeccheckFacade catSpeccheckFacade;

	@Resource(name = "userService")
	private UserService userService;

	@Resource(name = "cartFacade")
	private CatCartFacade cartFacade;

	private static final String LAST_LINK_CLASS = "active";
	private static final String CONFIGURATIONS = "Configurations";
	private static final String REORDER_PRODUCTS = "Reorder Products";
	private static final String LINK_URL = "#";
	private static final String CAT_IM_HOMEPAGE = "imhomepage";
	private static final String MANAGE_ORDERS_PAGE = "manageOrders";
	public static final String DEFAULT_SEARCH_PAGE = "defaultSearchPage";
	public static final String REDIRECT_TO_HOMEPAGE = REDIRECT_PREFIX + "/";
	private static final String REDIRECT_URL = REDIRECT_PREFIX + "/error";



	private static final String[] USED_FACET_ARR =
	{ "HOURS_USED", "MANUFACTURING_YEAR", "PRICE" };

	private static final String[] REMAINING_USED_FACET_ARR =
	{ "BRANCH_LOCATION" };
	private static final String CATEGORIES = "categories";

	private static final String[] SUGGEST_USED =
	{ "salesModelNameSuggester", "productFamilyNameSuggester", "serialNumberSuggester" };
	private static final String[] SUGGEST_NEW =
	{ "salesModelNameSuggester", "baseModelNameSuggester", "productFamilyNameSuggester" };





	@RequestMapping(value = "/viewManageOrders", method = RequestMethod.GET)
	public String getManageOrdersPage(final Model model) throws CMSItemNotFoundException
	{

		final List<Breadcrumb> breadcrumbs = new ArrayList<>();

		final Breadcrumb breadcrumb1 = new Breadcrumb(LINK_URL, REORDER_PRODUCTS, LAST_LINK_CLASS);
		breadcrumbs.add(breadcrumb1);
		final B2BCustomerModel customerModel = catCustomerFacade.getCurrentDealer();
		final String userType = catCustomerFacade.getUserType();
		if ((CatCoreConstants.UTV.equals(userType) || CatCoreConstants.IM.equals(userType))
				&& CollectionUtils.isEmpty(getReorderProducts(model, "", "").getResults()))
		{
			return REDIRECT_TO_HOMEPAGE;
		}
		model.addAttribute("lowStockCategories",
				dealerSearchFacade.getLowStockProductsCategories(customerModel.getDefaultB2BUnit()));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);
		final Map<String, Object> cartType = cartFacade.getCartType("");
		model.addAttribute(CatCoreConstants.CART_TYPE, cartType.get(CatCoreConstants.CART_PRODUCT_TYPE));
		model.addAttribute(CatCoreConstants.IS_UTV_CART, cartType.get(CatCoreConstants.IS_UTV_CART));
		storeCmsPageInModel(model, getContentPageForLabelOrId(MANAGE_ORDERS_PAGE));
		return getViewForPage(model);
	}

	@ResponseBody
	@RequestMapping(value = "/getReorderProducts", method = RequestMethod.GET)
	public SearchResultsData<ProductData> getReorderProducts(final Model model,
			@RequestParam(value = "salesModelId", defaultValue = "") final String salesModelId,
			@RequestParam(value = "category", defaultValue = "") final String category) throws CMSItemNotFoundException
	{
		final B2BCustomerModel customerModel = catCustomerFacade.getCurrentDealer();
		List<RestockAlertModel> alertEntries;

		alertEntries = dealerSearchFacade.getLowStockProducts(customerModel.getDefaultB2BUnit(), category, salesModelId);
		final int numOfProdsPerPage = Integer.parseInt(Config.getParameter("cat.products.per.page"));

		final List<ProductData> productList = dealerSearchFacade.getReorderProducts(alertEntries,
				customerModel.getDefaultB2BUnit());
		final SearchResultsData<ProductData> searchResultsData = new SearchResultsData<>();
		final PaginationData pagination = new PaginationData();
		pagination.setTotalNumberOfResults(alertEntries.size());
		pagination.setPageSize(numOfProdsPerPage);
		searchResultsData.setResults(productList);
		searchResultsData.setPagination(pagination);

		return searchResultsData;

	}


	@ResponseBody
	@RequestMapping(value = "/getReorderConfigProducts", method = RequestMethod.GET)
	public SearchResultsData<ProductData> getReorderConfigProducts(final Model model,
			@RequestParam(value = "salesModelCode") final String salesModelCode) throws CMSItemNotFoundException
	{

		final B2BCustomerModel customerModel = catCustomerFacade.getCurrentDealer();
		final String alertId = salesModelCode + "_" + customerModel.getDefaultB2BUnit().getUid();

		final List<AlertProductModel> alertEntries = dealerSearchFacade.getAssociatedConfigProducts(alertId);

		final SearchResultsData<ProductData> searchResultsData = new SearchResultsData<>();
		final List<ProductData> productList = new ArrayList();
		for (final AlertProductModel restockEntry : alertEntries)
		{
			final ProductData product = productConverter.convert(restockEntry.getProduct());
			productList.add(product);

		}

		final PaginationData pagination = new PaginationData();
		pagination.setTotalNumberOfResults(alertEntries.size());

		searchResultsData.setResults(productList);
		searchResultsData.setPagination(pagination);

		return searchResultsData;

	}

	@RequestMapping(value = "/lowInventory", method = RequestMethod.GET)
	public String getLowInventoryCount(@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode, final Model model) throws CMSItemNotFoundException
	{
		try
		{
			final Map<String, String> usedFacetMap = new HashMap<>();
			final String finalSearchQuery = dealerSearchFacade.buildSearchQuery("", "", "", "", false, usedFacetMap, null, false,
					"all");

			final ProductSearchPageData<SearchStateData, ProductData> searchPageData = performSearch(finalSearchQuery, page,
					showMode, sortCode, getSearchPageSize());

			model.addAttribute("lowInventoryCount", searchPageData.getPagination().getTotalNumberOfResults());
			model.addAttribute("isIM", true);
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage(), e);

		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(CAT_IM_HOMEPAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(CAT_IM_HOMEPAGE));
		return getViewForPage(model);
	}

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public String getViewPage(final Model model, final HttpServletRequest request, final HttpSession session,
			@RequestParam(value = "categoryCode", defaultValue = "") final String categoryCode,
			@RequestParam(value = "primaryOfferingId", defaultValue = "") final String primaryOfferingId,
			@RequestParam(value = "salesModelId", defaultValue = "") final String salesModelId,
			@RequestParam(value = "pdcFlag", defaultValue = "true") final String pdcFlag,
			@RequestParam(value = "mfuFlag", defaultValue = "true") final String mfuFlag,
			@RequestParam(value = "wsFlag", defaultValue = "true") final String wsFlag,
			@RequestParam(value = "isUsed", defaultValue = "false") final String isUsed,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "inTransitFlag", defaultValue = "false") final boolean inTransitFlag)
			throws CMSItemNotFoundException
	{

		final List<Breadcrumb> breadcrumbs = new ArrayList<>();

		final Breadcrumb breadcrumb1 = new Breadcrumb(LINK_URL, CONFIGURATIONS, LAST_LINK_CLASS);
		breadcrumbs.add(breadcrumb1);
		final String userType = catCustomerFacade.getUserType();
		boolean isGatewayProduct = false;
		CategoryModel categoryModel = null;
		model.addAttribute("userType", userType);


		Collection<CategoryModel> categories;
		if (StringUtils.isNotEmpty(primaryOfferingId))
		{
			final ProductModel productModel = dealerSearchFacade.getProductByName(primaryOfferingId);
			if (StringUtils.isNotEmpty(productModel.getCode()))
			{
				isGatewayProduct = dealerSearchFacade.isGatewayProduct(productModel);
				categories = populateModelIfPrimaryOfferingNotEmpty(model, isGatewayProduct, productModel);
				categoryModel = setCategoryCodeInModel(model, categoryModel, categories);
			}
			else
			{
				return REDIRECT_URL + "?errorCode=404";
			}
		}
		else if (StringUtils.isNotEmpty(salesModelId))
		{
			ProductModel productModel;
			try
			{
				productModel = productService.getProductForCode(salesModelId.toUpperCase());
			}
			catch (final Exception e)
			{
				LOG.error("Could not find product with code " + salesModelId + " Exception ", e);
				productModel = dealerSearchFacade.getProductByName(salesModelId.toUpperCase());
			}
			if (StringUtils.isNotEmpty(productModel.getCode()))
			{
				isGatewayProduct = dealerSearchFacade.isGatewayProduct(productModel);
				model.addAttribute(SALES_MODEL_ID, salesModelId.toUpperCase());
				categories = fetchCategoryFromFirstVariant(isUsed, productModel);
				categoryModel = setCategoryCodeInModel(model, categoryModel, categories);
			}
			else
			{
				return REDIRECT_URL + "?errorCode=404";
			}
		}
		else
		{
			model.addAttribute(SALES_MODEL_ID, salesModelId.toUpperCase());
			final CategoryModel category = dealerSearchFacade.getCategoryByName(categoryCode);
			if (null != category.getPk())
			{
				if (BooleanUtils.isTrue(category.getGatewayCategory()))
				{
					isGatewayProduct = true;
				}
				model.addAttribute(CATEGORY_CODE, categoryCode);
				categoryModel = category;
			}
		}
		model.addAttribute(PRIMARY_OFFERING_ID, primaryOfferingId.toUpperCase());
		model.addAttribute("utilityProducts", isGatewayProduct);

		final CategoryModel category = BooleanUtils.isFalse(Boolean.valueOf(isUsed))
				? categoryService.getCategoryForCode(StringUtils.lowerCase(CatCoreConstants.NEW))
				: categoryService.getCategoryForCode(StringUtils.lowerCase(CatCoreConstants.USED));
		model.addAttribute(CATEGORIES, catSpeccheckFacade.getCategoryData(category));

		model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);

		model.addAttribute("pdcFlag", pdcFlag);
		model.addAttribute("mfuFlag", mfuFlag);
		model.addAttribute("wsFlag", wsFlag);
		model.addAttribute("isUsed", isUsed);
		model.addAttribute("page", page);
		model.addAttribute("inTransitFlag", inTransitFlag);

		if (!isCategoryAccessibleToUser(categoryModel))
		{
			model.addAttribute("isAuthorized", false);
		}
		if (Boolean.valueOf(isUsed))
		{
			session.setAttribute(DEFAULT_SEARCH_PAGE, "usedProductPLP");
		}

		storeContinueUrl(request);
		updatePageTitle("", model);
		if (catCustomerFacade.isUserIM(catCustomerFacade.getCurrentDealer().getGroups()))
		{
			model.addAttribute("isIM", true);
		}
		storeCmsPageInModel(model, getContentPageForLabelOrId(SEARCH_CMS_PAGE_ID));
		return getViewForPage(model);
	}

	/**
	 * Method to fetch the category from the first used base variant or new
	 *
	 * @param isUsed
	 *           isUsed flag to fetch the values from used or new base variant products
	 * @param productModel
	 *           the sales model for which base variant is to be fetched
	 * @return Collection<CategoryModel>
	 */
	private Collection<CategoryModel> fetchCategoryFromFirstVariant(final String isUsed, final ProductModel productModel)
	{
		for (final VariantProductModel variant : productModel.getVariants())
		{
			if (variant instanceof BaseVariantProductModel && "true".equalsIgnoreCase(isUsed))
			{
				if (ProductConditionEnum.USED.equals(((BaseVariantProductModel) variant).getProductCondition()))
				{
					return variant.getSupercategories();
				}
			}
			else if (variant instanceof BaseVariantProductModel && "false".equalsIgnoreCase(isUsed)
					&& !ProductConditionEnum.USED.equals(((BaseVariantProductModel) variant).getProductCondition()))
			{

				return variant.getSupercategories();

			}
		}
		return Collections.emptyList();
	}


	/**
	 * Method to restrict the category to gateway or non gateway categories.
	 *
	 * @param model
	 *           model object in which the values have to be set
	 * @param categoryModel
	 *           category model which is passed
	 * @param categories
	 *           fetch gateway or non gateway categories out of the categories
	 * @return CategoryModel
	 */
	private CategoryModel setCategoryCodeInModel(final Model model, final CategoryModel categoryModel,
			final Collection<CategoryModel> categories)
	{
		CategoryModel categoryVal = categoryModel;
		for (final CategoryModel category : categories)
		{
			if (null != category.getGatewayCategory())
			{
				categoryVal = category;
				model.addAttribute(CATEGORY_CODE, category.getName());
				return categoryVal;
			}
		}
		return categoryVal;
	}


	/**
	 * Method to populate model in case primary offering suggestion is selected.
	 *
	 * @param model
	 *           model object in which the values have to be set
	 * @param isGatewayProduct
	 *           flag to determine categories for the product
	 * @param productModel
	 *           type caste to base variant product model
	 * @return CategoryModel
	 */
	private Collection<CategoryModel> populateModelIfPrimaryOfferingNotEmpty(final Model model, final boolean isGatewayProduct,
			final ProductModel productModel)
	{
		Collection<CategoryModel> categories;
		if (!isGatewayProduct)
		{
			final BaseVariantProductModel product = (BaseVariantProductModel) productModel;
			categories = product.getSupercategories();
			model.addAttribute(SALES_MODEL_ID, product.getBaseProduct().getName());
		}
		else
		{
			//setting sales model id and the categories
			categories = setCategories(productModel, model);
		}
		return categories;
	}



	/**
	 * Method to check if category can be accessed by a user
	 *
	 * @param categoryModel
	 *           category which is to be checked
	 * @return boolean
	 */
	private boolean isCategoryAccessibleToUser(final CategoryModel categoryModel)
	{
		if (null != categoryModel)
		{
			final List<PrincipalModel> principalUserGroups = categoryModel.getAllowedPrincipals();
			for (final UserGroupModel userGroup : userService.getAllUserGroupsForUser(userService.getCurrentUser()))
			{
				if (principalUserGroups.contains(userGroup))
				{
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @param productModel
	 * @return
	 */
	private Collection<CategoryModel> setCategories(final ProductModel productModel, final Model model)
	{
		final Collection<CategoryModel> categories = productModel.getSupercategories();

		if (productModel instanceof ConfigVariantProductModel)
		{
			final ConfigVariantProductModel configProd = (ConfigVariantProductModel) productModel;
			final BaseVariantProductModel baseProduct = (BaseVariantProductModel) configProd.getBaseProduct();
			model.addAttribute(SALES_MODEL_ID, baseProduct.getBaseProduct().getName());
		}

		else if (productModel instanceof BaseVariantProductModel)
		{
			final BaseVariantProductModel baseProduct = (BaseVariantProductModel) productModel;
			model.addAttribute(SALES_MODEL_ID, baseProduct.getBaseProduct().getName());
		}
		else
		{
			model.addAttribute(SALES_MODEL_ID, productModel.getName());
		}
		return categories;
	}

	@ResponseBody
	@RequestMapping(method = RequestMethod.GET, params = "!q")
	public SearchResultsData<ProductData> textSearch(@RequestParam(value = "text", defaultValue = "") final String searchText,
			final HttpServletRequest request, final Model model) throws CMSItemNotFoundException
	{
		final SearchResultsData<ProductData> searchResultsData = new SearchResultsData<>();
		if (StringUtils.isNotBlank(searchText))
		{
			final PageableData pageableData = createPageableData(0, getSearchPageSize(), null, ShowMode.Page);

			final SearchStateData searchState = new SearchStateData();
			final SearchQueryData searchQueryData = new SearchQueryData();
			final String finalSearchText = searchText + RELEVANCE_SORT;
			searchQueryData.setValue(finalSearchText);
			searchState.setQuery(searchQueryData);

			ProductSearchPageData<SearchStateData, ProductData> searchPageData = null;
			try
			{
				searchPageData = encodeSearchPageData(productSearchFacade.textSearch(searchState, pageableData));
			}
			catch (final ConversionException e) // NOSONAR
			{
				// nothing to do - the exception is logged in SearchSolrQueryPopulator
			}
			searchResultsData.setResults(null != searchPageData ? searchPageData.getResults() : new ArrayList<ProductData>());

		}

		return searchResultsData;
	}

	@RequestMapping(method = RequestMethod.GET, params = "q")
	public String refineSearch(@RequestParam("q") final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode,
			@RequestParam(value = "text", required = false) final String searchText, final HttpServletRequest request,
			final Model model) throws CMSItemNotFoundException
	{
		final ProductSearchPageData<SearchStateData, ProductData> searchPageData = performSearch(searchQuery, page, showMode,
				sortCode, getSearchPageSize());

		populateModel(model, searchPageData, showMode);
		model.addAttribute("userLocation", customerLocationService.getUserLocation());

		if (searchPageData.getPagination().getTotalNumberOfResults() == 0)
		{
			updatePageTitle(searchPageData.getFreeTextSearch(), model);
			storeCmsPageInModel(model, getContentPageForLabelOrId(NO_RESULTS_CMS_PAGE_ID));
		}
		else
		{
			storeContinueUrl(request);
			updatePageTitle(searchPageData.getFreeTextSearch(), model);
			storeCmsPageInModel(model, getContentPageForLabelOrId(SEARCH_CMS_PAGE_ID));
		}
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, searchBreadcrumbBuilder.getBreadcrumbs(null, searchPageData));
		model.addAttribute("pageType", PageType.PRODUCTSEARCH.name());

		final String metaDescription = MetaSanitizerUtil
				.sanitizeDescription(getMessageSource().getMessage(SEARCH_META_DESCRIPTION_RESULTS, null,
						SEARCH_META_DESCRIPTION_RESULTS, getI18nService().getCurrentLocale()) + " " + searchText + " "
						+ getMessageSource().getMessage(SEARCH_META_DESCRIPTION_ON, null, SEARCH_META_DESCRIPTION_ON,
								getI18nService().getCurrentLocale())
						+ " " + getSiteName());

		final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(searchText);
		setUpMetaData(model, metaKeywords, metaDescription);

		return getViewForPage(model);
	}

	protected ProductSearchPageData<SearchStateData, ProductData> performSearch(final String searchQuery, final int page,
			final ShowMode showMode, final String sortCode, final int pageSize)
	{
		final PageableData pageableData = createPageableData(page, pageSize, sortCode, showMode);

		final SearchStateData searchState = new SearchStateData();
		final SearchQueryData searchQueryData = new SearchQueryData();
		searchQueryData.setValue(searchQuery);
		searchState.setQuery(searchQueryData);

		return encodeSearchPageData(productSearchFacade.textSearch(searchState, pageableData));
	}

	protected ProductSearchPageData<SearchStateData, ProductData> performProductSearch(final String searchQuery, final int page,
			final ShowMode showMode, final String sortCode, final int pageSize, final CatIndexedTypeEnum indexType)
	{
		final PageableData pageableData = createPageableData(page, pageSize, sortCode, showMode);

		final SearchStateData searchState = new SearchStateData();
		final SearchQueryData searchQueryData = new SearchQueryData();
		searchQueryData.setValue(searchQuery);
		searchState.setQuery(searchQueryData);
		return encodeSearchPageData(dealerSearchFacade.textSearch(searchState, pageableData, indexType));
	}

	@RequestMapping(value = "/serialNumberSearch", method = RequestMethod.GET)
	public String serialNumberSearch(@RequestParam("serialNumber") final String serialNumber, final HttpServletResponse response,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode,
			@RequestParam(value = "searchKeywordCaptured", required = false) final String searchKeywordCaptured)
	{
		final String finalSearchQuery = CatCoreConstants.SERIAL_NUMBER + serialNumber;
		final ProductSearchPageData<SearchStateData, ProductData> searchPageData = performProductSearch(finalSearchQuery, 0,
				showMode, sortCode, getSearchPageSize(), CatIndexedTypeEnum.BASEVARIANTPRODUCT);
		final List<ProductData> productList = searchPageData.getResults();
		String productCode = null;
		if (CollectionUtils.isNotEmpty(productList) && productList.size() == 1)
		{
			productCode = productList.iterator().next().getCode();
			return "redirect:/p/" + productCode + "?searchKeywordCaptured=" + searchKeywordCaptured;
		}
		return productCode;
	}

	@ResponseBody
	@RequestMapping(value = "/results", method = RequestMethod.POST)
	public SearchResultsData<ProductData> jsonSearchResults(@RequestBody final CatSearchRequestForm catSearchRequestForm)
			throws CMSItemNotFoundException
	{
		final SearchResultsData<ProductData> searchResultsData = new SearchResultsData<>();
		try
		{
			final CategoryModel categoryModel = dealerSearchFacade.getCategoryByName(catSearchRequestForm.getCategory());
			//checking category and if user has access to the category.
			if (!isCategoryAccessibleToUser(categoryModel))
			{
				return new SearchResultsData<>();
			}

			Map<String, String> usedFacetsMap = new HashMap<>();
			if (catSearchRequestForm.isUsed() && !catSearchRequestForm.isDefaultFacetValue())
			{
				usedFacetsMap = populateUsedFacetsMap(catSearchRequestForm.getHoursUsedMin(), catSearchRequestForm.getHoursUsedMax(),
						catSearchRequestForm.getPriceMin(), catSearchRequestForm.getPriceMax(),
						catSearchRequestForm.getManufacturingYearMin(), catSearchRequestForm.getManufacturingYearMax(),
						catSearchRequestForm.getDealerCertified());
			}
			final String finalSearchQuery = dealerSearchFacade.buildSearchQuery(catSearchRequestForm.getCategory(),
					catSearchRequestForm.getSalesModelId(), catSearchRequestForm.getPrimaryOfferingId(), catSearchRequestForm.getQ(),
					catSearchRequestForm.isUsed(), usedFacetsMap, catSearchRequestForm.getSelectedBranchLocation(),
					catSearchRequestForm.isInTransitFlag(), catSearchRequestForm.getLaneType());

			CatIndexedTypeEnum indexType = CatIndexedTypeEnum.CONFIGVARIANTPRODUCT;
			if (catSearchRequestForm.isUsed())
			{
				indexType = CatIndexedTypeEnum.BASEVARIANTPRODUCT;
			}
			final ProductSearchPageData<SearchStateData, ProductData> searchPageData = performProductSearch(finalSearchQuery,
					catSearchRequestForm.getPage(), catSearchRequestForm.getShow(), RELEVANCE_SORT, getSearchPageSize(), indexType);

			populateSearchResultsData(searchResultsData, searchPageData, catSearchRequestForm.isUsed(),
					catSearchRequestForm.isDefaultFacetValue(), catSearchRequestForm.isBackwardNavigationFlag());

		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage(), e);
		}

		if (catSearchRequestForm.getCategory().contains(CatCoreConstants.UTV_TEXT))
		{
			Collections.reverse(searchResultsData.getResults());
			Collections.sort(searchResultsData.getResults(), new CatProductSeatCapacityComparator());
		}

		return searchResultsData;
	}


	/**
	 * @param searchResultsData
	 * @param searchPageData
	 * @param backwardNavigationFlag
	 * @param defaultFacetValue
	 * @param isUsed
	 */
	private void populateSearchResultsData(final SearchResultsData<ProductData> searchResultsData,
			final ProductSearchPageData<SearchStateData, ProductData> searchPageData, final boolean isUsed,
			final boolean defaultFacetValue, final boolean backwardNavigationFlag)
	{
		searchResultsData.setResults(searchPageData.getResults());
		searchResultsData.setPagination(searchPageData.getPagination());

		searchResultsData.setResults(catCustomerFacade.updateFavoriteProductsInResult(searchPageData.getResults()));

		if ((isUsed && CollectionUtils.isNotEmpty(searchPageData.getFacets()) && defaultFacetValue) || backwardNavigationFlag)
		{
			populateRemainingUsedFacetsMap(searchPageData.getFacets(), searchResultsData);
		}

	}

	/**
	 * Populating the remaining filters which will be displayed after searching for a used product
	 *
	 * @param facets
	 * @param searchResultsData
	 */
	private void populateRemainingUsedFacetsMap(final List<FacetData<SearchStateData>> facets,
			final SearchResultsData<ProductData> searchResultsData)
	{
		final List<String> facetList = Arrays.stream(USED_FACET_ARR).collect(Collectors.toList());
		final Map<String, Double> usedFacetMap = prepareUsedFacetMapFromIndexedData(facets, facetList);
		searchResultsData.setUsedFacetMap(usedFacetMap);
		final List<String> remainingFacetsList = Arrays.stream(REMAINING_USED_FACET_ARR).collect(Collectors.toList());
		final Map<String, List<String>> remainingUsedFacetMap = prepareRemainingUsedFacetFromIndexedData(facets,
				remainingFacetsList);
		LOG.debug("facetListMap" + remainingUsedFacetMap);
		searchResultsData.setRemainingUsedFacetMap(remainingUsedFacetMap);

	}

	/**
	 * Populate map with the range values which will further be converted into the SOLR query
	 *
	 * @param hoursUsedMin
	 * @param hoursUsedMax
	 * @param priceMin
	 * @param priceMax
	 * @param manufacturingYearMin
	 * @param manufacturingYearMax
	 * @param dealerCertified
	 * @return Map<String, String>
	 */
	private Map<String, String> populateUsedFacetsMap(final Integer hoursUsedMin, final Integer hoursUsedMax,
			final Integer priceMin, final Integer priceMax, final Integer manufacturingYearMin, final Integer manufacturingYearMax,
			final String dealerCertified)
	{
		final Map<String, String> usedFacetsMap = new HashMap<>();
		usedFacetsMap.put(CatCoreConstants.SOLR_HOURS_USED, CatCoreConstants.OPEN_BRACKET + hoursUsedMin + CatCoreConstants.SOLR_TO
				+ hoursUsedMax + CatCoreConstants.CLOSE_BRACKET);
		usedFacetsMap.put(CatCoreConstants.SOLR_PRICE,
				CatCoreConstants.OPEN_BRACKET + priceMin + CatCoreConstants.SOLR_TO + priceMax + CatCoreConstants.CLOSE_BRACKET);
		usedFacetsMap.put(CatCoreConstants.SOLR_MANUFACTURING_YEAR, CatCoreConstants.OPEN_BRACKET + manufacturingYearMin
				+ CatCoreConstants.SOLR_TO + manufacturingYearMax + CatCoreConstants.CLOSE_BRACKET);
		if (StringUtils.isNotEmpty(dealerCertified) && StringUtils.equalsIgnoreCase(dealerCertified, CatCoreConstants.TRUE_FLAG))
		{
			usedFacetsMap.put(CatCoreConstants.SOLR_DEALER_CERTIFIED, dealerCertified);
		}
		return usedFacetsMap;
	}

	/**
	 * Setting the minimum and maximum values for the range filter values to be displayed
	 *
	 * @param facets
	 * @param facetValueList
	 * @return Map<String, Double>
	 */
	public Map<String, Double> prepareUsedFacetMapFromIndexedData(final List<FacetData<SearchStateData>> facets,
			final List<String> facetValueList)
	{
		final Map<String, Double> facetMap = new HashMap<>();
		for (final String facetValue : facetValueList)
		{
			final Optional<FacetData<SearchStateData>> facetOpt = facets.stream()
					.filter(facet -> facetValue.equalsIgnoreCase(facet.getCode())).findFirst();
			if (facetOpt.isPresent())
			{
				final List<Double> facetList = facetOpt.get().getValues().stream().map(facetVal -> Double.valueOf(facetVal.getCode()))
						.collect(Collectors.toList());
				LOG.debug("facetlist" + facetList);
				final double min = facetList.stream().min(Comparator.comparing(Double::valueOf)).get().doubleValue();
				final double max = facetList.stream().max(Comparator.comparing(Double::valueOf)).get().doubleValue();
				facetMap.put(facetValue + "_MIN", min);
				facetMap.put(facetValue + "_MAX", max);
			}
		}
		return facetMap;
	}

	/**
	 * Preparing the list of branch location filter values to be displayed
	 *
	 * @param facets
	 * @param facetValueList
	 * @return Map<String, List<String>>
	 */
	public Map<String, List<String>> prepareRemainingUsedFacetFromIndexedData(final List<FacetData<SearchStateData>> facets,
			final List<String> facetValueList)
	{
		final Map<String, List<String>> facetMap = new HashMap<>();
		for (final String facetValue : facetValueList)
		{
			final Optional<FacetData<SearchStateData>> facetOpt = facets.stream()
					.filter(facet -> facetValue.equalsIgnoreCase(facet.getCode())).findFirst();
			if (facetOpt.isPresent())
			{
				final List<String> facetList = facetOpt.get().getValues().stream().map(facetVal -> facetVal.getCode())
						.collect(Collectors.toList());
				LOG.debug("facetlist" + facetList);
				facetMap.put(facetValue, facetList);
			}
		}
		return facetMap;
	}



	@ResponseBody
	@RequestMapping(value = "/facets", method = RequestMethod.GET)
	public FacetRefinement<SearchStateData> getFacets(@RequestParam("q") final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode) throws CMSItemNotFoundException
	{
		final SearchStateData searchState = new SearchStateData();
		final SearchQueryData searchQueryData = new SearchQueryData();
		searchQueryData.setValue(searchQuery);
		searchState.setQuery(searchQueryData);

		final ProductSearchPageData<SearchStateData, ProductData> searchPageData = productSearchFacade.textSearch(searchState,
				createPageableData(page, getSearchPageSize(), sortCode, showMode));
		final List<FacetData<SearchStateData>> facets = refineFacets(searchPageData.getFacets(),
				convertBreadcrumbsToFacets(searchPageData.getBreadcrumbs()));
		final FacetRefinement<SearchStateData> refinement = new FacetRefinement<>();
		refinement.setFacets(facets);
		refinement.setCount(searchPageData.getPagination().getTotalNumberOfResults());
		refinement.setBreadcrumbs(searchPageData.getBreadcrumbs());
		return refinement;
	}

	@ResponseBody
	@RequestMapping(value = "/autocomplete/" + COMPONENT_UID_PATH_VARIABLE_PATTERN, method = RequestMethod.GET)
	public AutocompleteResultData getAutocompleteSuggestions(@PathVariable final String componentUid,
			@RequestParam("term") final String term) throws CMSItemNotFoundException
	{
		final AutocompleteResultData resultData = new AutocompleteResultData();

		final SearchBoxComponentModel component = (SearchBoxComponentModel) cmsComponentService.getSimpleCMSComponent(componentUid);

		if (component.isDisplaySuggestions())
		{
			resultData.setSuggestions(subList(productSearchFacade.getAutocompleteSuggestions(term), component.getMaxSuggestions()));
		}


		if (component.isDisplayProducts())
		{
			resultData.setProducts(subList(productSearchFacade.textSearch(term, SearchQueryContext.SUGGESTIONS).getResults(),
					component.getMaxProducts()));
		}

		return resultData;
	}

	@ResponseBody
	@RequestMapping(value = "/favorite", method = RequestMethod.GET)
	public Map addToFavorites(@RequestParam("productCodePost") final String code, final Model model)
	{
		final ProductModel product = productService.getProductForCode(code);

		final B2BCustomerModel customerModel = catCustomerFacade.getCurrentDealer();
		final Set<ProductModel> favProdList = customerModel.getFavoriteProducts();
		final Set favProductSet = new HashSet();
		return catCustomerFacade.toggleFavorite(favProdList, favProductSet, product);
	}


	protected <E> List<E> subList(final List<E> list, final int maxElements)
	{
		if (CollectionUtils.isEmpty(list))
		{
			return Collections.emptyList();
		}

		if (list.size() > maxElements)
		{
			return list.subList(0, maxElements);
		}

		return list;
	}

	protected void updatePageTitle(final String searchText, final Model model)
	{
		storeContentPageTitleInModel(model, getPageTitleResolver().resolveContentPageTitle(
				getMessageSource().getMessage("search.meta.title", null, "search.meta.title", getI18nService().getCurrentLocale())
						+ " " + searchText));
	}


	protected ProductSearchPageData<SearchStateData, ProductData> encodeSearchPageData(
			final ProductSearchPageData<SearchStateData, ProductData> searchPageData)
	{
		final SearchStateData currentQuery = searchPageData.getCurrentQuery();

		if (currentQuery != null)
		{
			try
			{
				final SearchQueryData query = currentQuery.getQuery();
				final String encodedQueryValue = XSSEncoder.encodeHTML(query.getValue());
				query.setValue(encodedQueryValue);
				currentQuery.setQuery(query);
				searchPageData.setCurrentQuery(currentQuery);
				searchPageData.setFreeTextSearch(XSSEncoder.encodeHTML(searchPageData.getFreeTextSearch()));

				final List<FacetData<SearchStateData>> facets = searchPageData.getFacets();
				if (CollectionUtils.isNotEmpty(facets))
				{
					processFacetData(facets);
				}
			}
			catch (final UnsupportedEncodingException e)
			{
				if (LOG.isDebugEnabled())
				{
					LOG.debug("Error occured during Encoding the Search Page data values", e);
				}
			}
		}
		return searchPageData;
	}

	protected void processFacetData(final List<FacetData<SearchStateData>> facets) throws UnsupportedEncodingException
	{
		for (final FacetData<SearchStateData> facetData : facets)
		{
			final List<FacetValueData<SearchStateData>> topFacetValueDatas = facetData.getTopValues();
			if (CollectionUtils.isNotEmpty(topFacetValueDatas))
			{
				processFacetDatas(topFacetValueDatas);
			}
			final List<FacetValueData<SearchStateData>> facetValueDatas = facetData.getValues();
			if (CollectionUtils.isNotEmpty(facetValueDatas))
			{
				processFacetDatas(facetValueDatas);
			}
		}
	}

	protected void processFacetDatas(final List<FacetValueData<SearchStateData>> facetValueDatas)
			throws UnsupportedEncodingException
	{
		for (final FacetValueData<SearchStateData> facetValueData : facetValueDatas)
		{
			final SearchStateData facetQuery = facetValueData.getQuery();
			final SearchQueryData queryData = facetQuery.getQuery();
			final String queryValue = queryData.getValue();
			if (StringUtils.isNotBlank(queryValue))
			{
				final String[] queryValues = queryValue.split(FACET_SEPARATOR);
				final StringBuilder queryValueBuilder = new StringBuilder();
				queryValueBuilder.append(XSSEncoder.encodeHTML(queryValues[0]));
				for (int i = 1; i < queryValues.length; i++)
				{
					queryValueBuilder.append(FACET_SEPARATOR).append(queryValues[i]);
				}
				queryData.setValue(queryValueBuilder.toString());
			}
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/selectCategory")
	@ResponseBody
	public List<ProductData> getSalesModelsForCategory(
			@RequestParam(value = "categoryCode", defaultValue = "") final String categoryCode,
			@RequestParam(value = "isUsed", defaultValue = "false") final boolean isUsedFlag)
	{
		final CategoryModel category = dealerSearchFacade.getCategoryByName(categoryCode);
		if (category != null)
		{
			return dealerSearchFacade.getSalesModels(category.getCode(), isUsedFlag);
		}
		return Collections.emptyList();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/selectLowStockCategory")
	@ResponseBody
	public List<ProductData> getLowStockSalesModels(@RequestParam(value = "categoryCode", defaultValue = "") final String category)
	{
		List<ProductData> productList = null;
		try
		{
			final B2BCustomerModel customerModel = catCustomerFacade.getCurrentDealer();
			List<RestockAlertModel> alertEntries;
			alertEntries = dealerSearchFacade.getLowStockProducts(customerModel.getDefaultB2BUnit(), category, "");
			productList = new ArrayList();
			for (final RestockAlertModel restockAlertModel : alertEntries)
			{
				final ProductData product = productConverter.convert(restockAlertModel.getProduct());
				productList.add(product);
			}
		}
		catch (final Exception e)
		{
			LOG.error("Error while loading low stock sales Model" + e.getMessage(), e);
		}

		return productList;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/selectSalesModel")
	@ResponseBody
	public List<ProductData> getBaseModelsForSalesModel(
			@RequestParam(value = "productCode", defaultValue = "") final String productCode)
	{
		try
		{
			return dealerSearchFacade.getBaseModels(productCode.toUpperCase());
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage(), e);
			final ProductModel product = dealerSearchFacade.getProductByName(productCode.toUpperCase());
			return dealerSearchFacade.getBaseModels(product.getCode());
		}
	}


	@ResponseBody
	@RequestMapping(value = "/autosuggestOld", method = RequestMethod.GET)
	public AutoSuggestResultsData getAutosuggestResults(@RequestParam(value = "text", defaultValue = "") final String searchQuery,
			@RequestParam(value = "page", defaultValue = "0") final int page,
			@RequestParam(value = "show", defaultValue = "Page") final ShowMode showMode,
			@RequestParam(value = "sort", required = false) final String sortCode) throws CMSItemNotFoundException
	{
		final AutoSuggestResultsData autoSuggestResultData = new AutoSuggestResultsData();
		try
		{
			final CatIndexedTypeEnum indexType = CatIndexedTypeEnum.CONFIGVARIANTPRODUCT;
			final String categorySearchQuery = dealerSearchFacade.buildCategorySearchQuery(searchQuery);

			final ProductSearchPageData<SearchStateData, ProductData> categorySearchPageData = performProductSearch(
					categorySearchQuery, page, showMode, sortCode, MAX_PAGE_LIMIT, indexType);

			final List<ProductData> categorySearchResults = categorySearchPageData.getResults();

			if (!categorySearchResults.isEmpty())
			{
				autoSuggestResultData.setProductFamily(dealerSearchFacade.setProductFamily(categorySearchResults));
			}
			else
			{
				autoSuggestResultData.setProductFamily(Collections.emptyList());

				final String salesModelSearchQuery = dealerSearchFacade.buildSalesModelSearchQuery(searchQuery);

				final ProductSearchPageData<SearchStateData, ProductData> salesModelSearchPageData = performProductSearch(
						salesModelSearchQuery, page, showMode, sortCode, MAX_PAGE_LIMIT, indexType);

				autoSuggestResultData.setSalesModels(dealerSearchFacade.setSalesModels(salesModelSearchPageData.getResults()));

				final String baseModelSearchQuery = dealerSearchFacade.buildBaseModelSearchQuery(searchQuery);

				final ProductSearchPageData<SearchStateData, ProductData> baseModelSearchPageData = performProductSearch(
						baseModelSearchQuery, page, showMode, sortCode, MAX_PAGE_LIMIT, indexType);

				autoSuggestResultData.setBaseModels(dealerSearchFacade.setBaseModels(baseModelSearchPageData.getResults()));
			}
		}
		catch (final Exception e)
		{
			LOG.error(e.getMessage(), e);
		}
		return autoSuggestResultData;
	}


	@ResponseBody
	@RequestMapping(value = "/autosuggest", method = RequestMethod.GET)
	public Map<String, Collection<String>> getAutosuggestResultsForUsedProduct(@RequestParam("text") final String text,
			@RequestParam(value = "isUsed", defaultValue = "false") final boolean isUsed, final Model model)
	{
		final Pattern pattern = Pattern.compile("^[A-Za-z0-9].*$");
		final Matcher matcher = pattern.matcher(text);
		CatIndexedTypeEnum indexType = CatIndexedTypeEnum.CONFIGVARIANTPRODUCT;
		if (isUsed)
		{
			indexType = CatIndexedTypeEnum.BASEVARIANTPRODUCT;
		}
		Map<String, Collection<String>> autoSuggestionMap = new HashMap<>();
		if (StringUtils.isNotEmpty(text.trim()) && matcher.matches())
		{
			autoSuggestionMap = dealerSearchFacade.getAutocompleteSuggestions(text, indexType);
			if (LOG.isDebugEnabled())
			{
				LOG.debug("autoSuggestionMap" + autoSuggestionMap);
			}
			final Collection<String> autoSuggestCollection = autoSuggestionMap.get(PRODUCTFAMILYNAMESUGGESTER);
			final Collection<String> productFamilyCollection = new ArrayList<>();
			if (CollectionUtils.isNotEmpty(autoSuggestCollection))
			{
				populateProductFamilyCollection(autoSuggestCollection, productFamilyCollection);
				autoSuggestionMap.put(PRODUCTFAMILYNAMESUGGESTER, productFamilyCollection);
			}
		}
		else
		{
			if (isUsed)
			{
				createAnEmptyMap(autoSuggestionMap, SUGGEST_USED);
			}
			else
			{
				createAnEmptyMap(autoSuggestionMap, SUGGEST_NEW);
			}
		}
		model.addAttribute("autoSuggestionMap", autoSuggestionMap);
		return autoSuggestionMap;
	}

	/**
	 * Method to populate product family collection for new products
	 *
	 * @param autoSuggestCollection
	 *           auto suggest collection from solr
	 * @param productFamilyCollection
	 *           the collection to be populated
	 */
	private static void populateProductFamilyCollection(final Collection<String> autoSuggestCollection,
			final Collection<String> productFamilyCollection)
	{
		for (final String autoSuggest : autoSuggestCollection)
		{
			if (!"Used".equalsIgnoreCase(autoSuggest))
			{
				productFamilyCollection.add(autoSuggest);
			}
		}
	}

	/**
	 * Method to create an empty map in case text is blank
	 *
	 * @param autoSuggestionMap
	 *           Auto suggestion map to be populated
	 * @param suggesterArr
	 *           suggester list which needs to be attached to the key
	 */
	private static void createAnEmptyMap(final Map<String, Collection<String>> autoSuggestionMap, final String[] suggesterArr)
	{
		for (final String suggester : suggesterArr)
		{
			autoSuggestionMap.put(suggester, new ArrayList<String>());
		}
	}

	public static class SearchResultsData<RESULT>
	{
		private List<RESULT> results;
		private PaginationData pagination;
		private Map<String, Object> warehouseMap;
		private Map<String, Double> usedFacetMap;
		private Map<String, List<String>> remainingUsedFacetMap;
		private String cartLaneType;



		/**
		 * @return the remainingUsedFacetMap
		 */
		public Map<String, List<String>> getRemainingUsedFacetMap()
		{
			return remainingUsedFacetMap;
		}

		/**
		 * @param remainingUsedFacetMap
		 *           the remainingUsedFacetMap to set
		 */
		public void setRemainingUsedFacetMap(final Map<String, List<String>> remainingUsedFacetMap)
		{
			this.remainingUsedFacetMap = remainingUsedFacetMap;
		}


		/**
		 * @return the warehouseMap
		 */
		public Map<String, Object> getWarehouseMap()
		{
			return warehouseMap;
		}

		public List<RESULT> getResults()
		{
			return results;
		}

		/**
		 * @param wsMap
		 */
		public void setWarehouseMap(final Map<String, Object> wsMap)
		{
			this.warehouseMap = wsMap;

		}

		public void setResults(final List<RESULT> results)
		{
			this.results = results;
		}

		/**
		 * @return the usedFacetMap
		 */
		public Map<String, Double> getUsedFacetMap()
		{
			return usedFacetMap;
		}

		/**
		 * @param usedFacetMap
		 *           the usedFacetMap to sets
		 */
		public void setUsedFacetMap(final Map<String, Double> usedFacetMap)
		{
			this.usedFacetMap = usedFacetMap;
		}

		public PaginationData getPagination()
		{
			return pagination;
		}

		public void setPagination(final PaginationData pagination)
		{
			this.pagination = pagination;
		}

		/**
		 * @return the cartLaneType
		 */
		public String getCartLaneType()
		{
			return cartLaneType;
		}

		/**
		 * @param cartLaneType
		 *           the cartLaneType to set
		 */
		public void setCartLaneType(final String cartLaneType)
		{
			this.cartLaneType = cartLaneType;
		}
	}
}


