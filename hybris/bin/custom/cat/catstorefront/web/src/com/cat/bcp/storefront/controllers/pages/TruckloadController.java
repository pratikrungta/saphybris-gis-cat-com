package com.cat.bcp.storefront.controllers.pages;


import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.Breadcrumb;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.ProductBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractCheckoutController;
import de.hybris.platform.acceleratorstorefrontcommons.forms.validation.GuestRegisterValidator;
import de.hybris.platform.acceleratorstorefrontcommons.security.AutoLoginStrategy;
import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.b2bcommercefacades.company.B2BUnitFacade;
import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.OrderFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.commerceservices.order.CommerceCartModificationException;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.exceptions.CalculationException;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.core.model.ShipToAfterDateModel;
import com.cat.bcp.core.servicelayer.CatProductService;
import com.cat.bcp.facade.product.CatStockFacade;
import com.cat.bcp.facades.comparator.CatProductNameComparator;
import com.cat.bcp.facades.comparator.CatProductSeatCapacityComparator;
import com.cat.bcp.facades.comparator.CatShipToAfterDateComparator;
import com.cat.bcp.facades.customer.CatCustomerFacade;
import com.cat.bcp.facades.order.CatCartFacade;
import com.cat.bcp.facades.order.CatCheckoutFacade;
import com.cat.bcp.storefront.forms.CatTruckloadForm;


/**
 * CheckoutController
 */
@Controller
@RequestMapping(value = "/truckload")
public class TruckloadController extends AbstractCheckoutController
{

	public static final String CONCATENATED_SHIPPING_ADDRESS_LIST = "concatenatedShippingAddressList";
	public static final String DEALER_NAME = "dealerName";
	public static final String TRUCKLOAD_ENTRIES = "truckloadEntries";
	public static final String SHIP_TO_AFTER_DATES = "shipToAfterDates";
	public static final String CUV_PRODUCTS_LIST = "cuvProductsList";
	public static final String POSSIBLE_TRUCKLOAD_COMBINATION_LIST = "possibleTruckloadCombinationList";
	public static final String SHIPPING_ADDRESS_LIST = "shippingAddressList";
	private static final Logger LOG = Logger.getLogger(TruckloadController.class);
	/**
	 * We use this suffix pattern because of an issue with Spring 3.1 where a Uri value is incorrectly extracted if it
	 * contains on or more '.' characters. Please see https://jira.springsource.org/browse/SPR-6164 for a discussion on
	 * the issue and future resolution.
	 */

	private static final String TRUCKLOAD_CONFIGURATOR_CMS_PAGE_LABEL = "truckloadConfigurator";
	private static final String TRUCKLOAD_REVIEW_ORDER_CMS_PAGE_LABEL = "truckloadReviewOrder";
	private static final String TRUCKLOAD = "Truckload";
	private static final String REVIEW_ORDER = "Review Order";
	private static final String LINK_URL = "#";
	private static final String LAST_LINK_CLASS = "active";
	private static final String PRODUCT_CODE_NOT_NULL = "ProductCode Parameter must not be null";
	private static final String TRUCKLOAD_ID_NOT_NULL = "Truckload id Parameter must not be null";
	private static final String TOTAL_CART_QTY = "totalCartQuantity";
	private static final String TOTAL_CART_PRICE = "totalCartPrice";
	private static final String PO_MAP = "poMap";
	private static final String TRUCKLOAD_PO_MAP = "truckloadPOMap";
	private static final String SUCCESS = "success";
	private static final String TRUCKLOAD_IMG_URL = "truckloadImageUrl";
	private static final String TRUCKLOAD_CONFIGURATOR_LINK = "/truckload/configureTruckload/?productCode=";
	@Resource(name = "productFacade")
	private ProductFacade productFacade;

	@Resource(name = "orderFacade")
	private OrderFacade orderFacade;

	@Resource(name = "checkoutFacade")
	private CatCheckoutFacade checkoutFacade;

	@Resource(name = "guestRegisterValidator")
	private GuestRegisterValidator guestRegisterValidator;

	@Resource(name = "autoLoginStrategy")
	private AutoLoginStrategy autoLoginStrategy;

	@Resource(name = "commonI18NService")
	private CommonI18NService commonI18NService;

	@Resource(name = "cartFacade")
	private CatCartFacade cartFacade;

	@Resource(name = "modelService")
	private ModelService modelService;

	@Resource(name = "cartService")
	private CartService cartService;

	@Resource(name = "b2bUnitFacade")
	private B2BUnitFacade b2bUnitFacade;

	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "catStockFacade")
	private CatStockFacade catStockFacade;

	@Resource(name = "catCustomerFacade")
	private CatCustomerFacade catCustomerFacade;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource(name = "catProductService")
	private CatProductService catProductService;

	@Resource(name = "productBreadcrumbBuilder")
	private ProductBreadcrumbBuilder productBreadcrumbBuilder;

	@Resource(name = "catProductReverseConverter")
	private Converter<ProductData, ProductModel> catProductReverseConverter;

	@ExceptionHandler(ModelNotFoundException.class)
	public String handleModelNotFoundException(final ModelNotFoundException exception, final HttpServletRequest request)
	{
		request.setAttribute("message", exception.getMessage());
		return FORWARD_PREFIX + "/404";
	}

	@RequestMapping(method = RequestMethod.GET)
	public String checkout(final RedirectAttributes redirectModel)
	{
		if (getCheckoutFlowFacade().hasValidCart())
		{
			if (validateCart(redirectModel))
			{
				return REDIRECT_PREFIX + "/cart";
			}
			else
			{
				checkoutFacade.prepareCartForCheckout();
				return getCheckoutRedirectUrl();
			}
		}

		LOG.info("Missing, empty or unsupported cart");

		// No session cart or empty session cart. Bounce back to the cart page.
		return REDIRECT_PREFIX + "/cart";
	}



	/**
	 * This method gets the truckload configurator page data.
	 *
	 * @return the truckload configurator page data
	 */
	@ResponseBody
	@RequestMapping(value = "/getTruckloadData", method = RequestMethod.GET)
	public Map<String, Object> getTruckloadConfiguratorPageData()
	{
		final Map orderWindowMap = catStockFacade.getOrderWindowAndShipAfterDates(null);
		final Map<String, Object> resultMap = new HashMap();
		final List<ShipToAfterDateModel> shipToDates = (List<ShipToAfterDateModel>) orderWindowMap.get(SHIP_TO_AFTER_DATES);
		Collections.sort(shipToDates, new CatShipToAfterDateComparator());
		final B2BCustomerModel customerModel = catCustomerFacade.getCurrentDealer();

		if (customerModel.getDefaultB2BUnit() != null)
		{
			final List<AddressData> shippingAddressList = catCustomerFacade.getDealerShipTos(customerModel, null);

			if (CollectionUtils.isNotEmpty(shippingAddressList))
			{
				resultMap.put(CONCATENATED_SHIPPING_ADDRESS_LIST,
						catCustomerFacade.getConcatenatedShippingAddressList(shippingAddressList));
				resultMap.put(SHIPPING_ADDRESS_LIST, shippingAddressList);
			}
			resultMap.put(DEALER_NAME, customerModel.getDefaultB2BUnit().getName());
		}
		if (CollectionUtils.isNotEmpty(shipToDates))
		{
			resultMap.put(SHIP_TO_AFTER_DATES, checkoutFacade.getListOfFormattedShipToDates(shipToDates));
			final List<ProductData> prodDataList = checkoutFacade.getConfigProductsFromCategory(CatCoreConstants.UTV.toLowerCase());
			if (CollectionUtils.isNotEmpty(prodDataList))
			{
				Collections.reverse(prodDataList);
				Collections.sort(prodDataList, new CatProductSeatCapacityComparator());
			}
			resultMap.put(CUV_PRODUCTS_LIST, prodDataList);
			resultMap.put(POSSIBLE_TRUCKLOAD_COMBINATION_LIST, checkoutFacade.getTruckloadCombinationList());
			final CartData cartData = cartFacade.getSessionCart();
			if (CollectionUtils.isNotEmpty(cartData.getEntries()))
			{
				resultMap.put(TRUCKLOAD_ENTRIES, checkoutFacade.retainTruckloads(cartData));
				resultMap.put(TOTAL_CART_QTY, cartData.getTotalUnitCount());
				resultMap.put(TOTAL_CART_PRICE, cartData.getTotalPrice().getValue());
			}
		}
		if (cartService.hasSessionCart() && CollectionUtils.isNotEmpty(cartService.getSessionCart().getEntries()))
		{
			resultMap.put(PO_MAP, cartService.getSessionCart().getPoNumberMap());
		}
		return resultMap;

	}


	/**
	 * This method gets the Products as per the ship after date.
	 *
	 * @return the truckload configurator page data
	 */
	@ResponseBody
	@RequestMapping(value = "/getProductsForDate", method = RequestMethod.POST)
	public Map<String, Object> getProductsForDate(@RequestParam final String shipAfterDate) throws ParseException
	{
		final DateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.US);
		final Date date = formatter.parse(shipAfterDate);
		final List<ProductData> productDataList = catStockFacade.getProductsForShipAfterDate(date);
		final Map<String, Object> resultMap = new HashMap();

		final List<ProductData> allProdDataList = checkoutFacade.getConfigProductsFromCategory(CatCoreConstants.UTV.toLowerCase());

		List<ProductData> unavailableProdDataList = populateUnavailableProdDataList(allProdDataList, productDataList);

		if (CollectionUtils.isNotEmpty(unavailableProdDataList))
		{
			final Map orderWindowMap = catStockFacade
					.getOrderWindowAndShipAfterDates(catProductReverseConverter.convertAll(unavailableProdDataList));

			final List<ShipToAfterDateModel> shipAfterSet = (List<ShipToAfterDateModel>) orderWindowMap.get("shipToAfterDates");
			Collections.sort(unavailableProdDataList, new CatProductNameComparator());
			Collections.reverse(unavailableProdDataList);
			resultMap.put("unavailableProductsList", unavailableProdDataList);

			final StringBuilder prdMsg = new StringBuilder();
			populateInfoMsg(prdMsg, shipAfterSet, unavailableProdDataList, resultMap);
			resultMap.put("productInformationMsg", prdMsg.toString());
		}
		else
		{
			resultMap.put("productInformationMsg", "");
			unavailableProdDataList = new ArrayList();
			resultMap.put("unavailableProductsList", unavailableProdDataList);
		}
		return resultMap;

	}




	/**
	 * Populate unavailable products data list.
	 *
	 * @param allProdDataList
	 *           all products data list
	 * @param productDataList
	 *           product data list
	 * @return the list of unavailable products
	 */
	private List<ProductData> populateUnavailableProdDataList(final List<ProductData> allProdDataList,
			final List<ProductData> productDataList)
	{
		final List<ProductData> unavailableProdDataList = new ArrayList();

		final Map<String, ProductData> allproductsMap = new HashMap();
		final List<String> productCode = new ArrayList();
		for (final ProductData allProductData : allProdDataList)
		{
			productCode.add(allProductData.getCode());
			allproductsMap.put(allProductData.getCode(), allProductData);
		}

		final Map<String, ProductData> productMap = new HashMap();
		for (final ProductData productData : productDataList)
		{
			productMap.put(productData.getCode(), productData);
		}

		for (final String code : productCode)
		{
			if (!productMap.containsKey(code))
			{
				unavailableProdDataList.add(allproductsMap.get(code));
			}
		}
		return unavailableProdDataList;
	}

	/**
	 * @param prdMsg
	 *           - the message
	 * @param shipAfterSet
	 *           - ship after months
	 * @param resultMap
	 *           - the result map
	 * @param unavailableProdDataList
	 *           - unavailable product list
	 */
	private void populateInfoMsg(final StringBuilder prdMsg, final List<ShipToAfterDateModel> shipAfterSet,
			final List<ProductData> unavailableProdDataList, final Map<String, Object> resultMap)
	{
		final Iterator<ProductData> prdIterator = unavailableProdDataList.iterator();

		final StringBuilder arg1 = new StringBuilder();
		arg1.append(prdIterator.next().getName());
		while (prdIterator.hasNext())
		{
			arg1.append(getMessageSource().getMessage("truckload.product.info.comma", new Object[] {},
					getI18nService().getCurrentLocale()));
			arg1.append(prdIterator.next().getName());
		}

		final Set<String> shipAfterNameSet = new HashSet();
		for (final ShipToAfterDateModel shipToAfterModel : shipAfterSet)
		{
			shipAfterNameSet.add(shipToAfterModel.getShipAfterName());
		}
		final List<String> monthList = new ArrayList(shipAfterNameSet);
		Collections.reverse(monthList);
		resultMap.put("unavailableProducts_ShipAfterMonths", monthList);

		final Iterator<String> monthIterator = monthList.iterator();
		final StringBuilder arg2 = new StringBuilder();
		arg2.append(monthIterator.next());
		while (monthIterator.hasNext())
		{
			arg2.append(getMessageSource().getMessage("truckload.product.info.comma", new Object[] {},
					getI18nService().getCurrentLocale()));
			arg2.append(monthIterator.next());
		}

		prdMsg.append(getMessageSource().getMessage("truckload.product.ship.from.factory", new Object[]
		{ arg1.toString(), arg2.toString() }, getI18nService().getCurrentLocale()));

	}

	/**
	 * This method clears the cart.
	 */
	@ResponseBody
	@RequestMapping(value = "/clearCart", method = RequestMethod.GET)
	public void clearCart()
	{
		cartFacade.clearCart();
	}

	/**
	 * This method returns the CMS page for truckload configuration.
	 *
	 * @param productCode
	 *           the product code
	 * @param model
	 *           the model
	 * @return the CMS page for truckload configurator
	 */
	@RequestMapping(value = "/configureTruckload/", method = RequestMethod.GET)
	public String configureTruckload(@RequestParam("productCode") final String productCode, final Model model)
	{
		final ProductModel productModel = productService.getProductForCode(productCode);
		final CustomerData customer = getCustomerFacade().getCurrentCustomer();
		model.addAttribute("userEmailId", customer.getEmail());
		try
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(TRUCKLOAD_CONFIGURATOR_CMS_PAGE_LABEL));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(TRUCKLOAD_CONFIGURATOR_CMS_PAGE_LABEL));
		}
		catch (final CMSItemNotFoundException e)
		{
			LOG.warn("Unable to load truckload configurator page : " + e);
		}

		final List<Breadcrumb> breadcrumbs = populateBreadCrumbs(productModel);
		breadcrumbs.add(new Breadcrumb(LINK_URL, TRUCKLOAD, LAST_LINK_CLASS));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);
		return getViewForPage(model);
	}

	/**
	 * This method is used to populate bread crumbs for truckload configurator page.
	 *
	 * @param product
	 *           the product
	 *
	 */
	private List<Breadcrumb> populateBreadCrumbs(final ProductModel product)
	{
		final List<Breadcrumb> breadcrumbList = productBreadcrumbBuilder.getBreadcrumbs(product.getCode());

		CategoryModel category = null;
		final List<Breadcrumb> breadcrumbs = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(product.getSupercategories()))
		{
			category = product.getSupercategories().iterator().next();
		}

		if (category != null)
		{
			final String url = getUrlForCategory(CatCoreConstants.CONFIG_LINK_URL_NEW, category.getName());
			if (StringUtils.isNotBlank(url))
			{
				final Breadcrumb configurationsBreadcrumb = new Breadcrumb(url, CatCoreConstants.CONFIGURATIONS,
						CatCoreConstants.MIDDLE_LINK_CLASS);
				configurationsBreadcrumb.setCategoryCode(category.getCode());
				breadcrumbs.add(configurationsBreadcrumb);
			}
		}
		for (int i = 0; i < breadcrumbList.size(); i++)
		{
			if (i > 2)
			{
				final String url = breadcrumbList.get(i).getUrl();
				final String pdpUrl = url.substring(0, url.lastIndexOf('/') + 1).concat(product.getCode());
				final Breadcrumb pdpBreadcrumb = new Breadcrumb(pdpUrl, product.getName(), CatCoreConstants.MIDDLE_LINK_CLASS);
				breadcrumbs.add(pdpBreadcrumb);
			}
		}

		return breadcrumbs;

	}

	/**
	 * This method is used to get configurations url.
	 *
	 * @param configLinkUrl
	 *           the config link url
	 * @param categoryName
	 *           the category name
	 * @return BreadCrumb URL String
	 */
	private String getUrlForCategory(final String configLinkUrl, final String categoryName)
	{
		final boolean flag = false;
		return configLinkUrl.replaceFirst("p1", categoryName).replaceFirst("p2", StringUtils.EMPTY)
				.replaceFirst("p3", StringUtils.EMPTY).replaceFirst("p4", String.valueOf(flag))
				.replaceFirst("p5", String.valueOf(flag)).replaceFirst("p6", String.valueOf(flag))
				.replaceFirst("p7", StringUtils.EMPTY);
	}


	/**
	 * This method returns the truckload review order page.
	 *
	 * @param productCode
	 *           the product code
	 * @param model
	 *           the model
	 * @return CMS page for truckload review order page
	 */
	@RequestMapping(value = "/reviewTruckload/", method = RequestMethod.GET)
	public String truckloadReviewOrder(@RequestParam("productCode") final String productCode, final Model model)
	{

		validateParameterNotNull(productCode, PRODUCT_CODE_NOT_NULL);
		final ProductData productData = productFacade.getProductForCodeAndOptions(productCode,
				Arrays.asList(ProductOption.CATEGORIES, ProductOption.VARIANT_MATRIX_BASE, ProductOption.VARIANT_MATRIX_URL,
						ProductOption.VARIANT_MATRIX_MEDIA));
		final boolean isUtv = catProductService.isUtvProduct(productData);
		model.addAttribute("isUtilityProduct", isUtv);
		final ProductModel product = productService.getProductForCode(productCode);
		try
		{
			storeCmsPageInModel(model, getContentPageForLabelOrId(TRUCKLOAD_REVIEW_ORDER_CMS_PAGE_LABEL));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(TRUCKLOAD_REVIEW_ORDER_CMS_PAGE_LABEL));
		}
		catch (final CMSItemNotFoundException e)
		{
			LOG.warn("Unable to load truckload review order page : " + e);
		}

		final List<Breadcrumb> breadcrumbs = populateBreadCrumbs(product);
		breadcrumbs.add(new Breadcrumb(TRUCKLOAD_CONFIGURATOR_LINK.concat(productCode), TRUCKLOAD, LAST_LINK_CLASS));
		breadcrumbs.add(new Breadcrumb(LINK_URL, REVIEW_ORDER, LAST_LINK_CLASS));
		model.addAttribute(WebConstants.BREADCRUMBS_KEY, breadcrumbs);
		return getViewForPage(model);
	}

	/**
	 * This method validates the truckload and adds to cart if success.
	 *
	 * @param form
	 *           the Truckload form
	 *
	 * @return Map - success and failure with relevant data
	 * @throws ParseException,
	 *            CommerceCartModificationException
	 */

	@ResponseBody
	@RequestMapping(value = "/validateTruckload", method = RequestMethod.POST)
	public Map<String, Object> validateTruckload(final CatTruckloadForm form)
			throws CommerceCartModificationException, ParseException
	{
		Map<String, Object> resultMap = new HashMap();

		final boolean isValid = checkoutFacade.validateTruckload(form.getTwoSeaterQty(), form.getFiveSeaterQty());

		if (!isValid) //Not a valid truckload
		{
			resultMap = checkoutFacade.setSuggestionAndMessage(resultMap, form.getTwoSeaterQty(), form.getFiveSeaterQty());
		}
		else // Valid truckload combination, add truck to cart
		{
			final String imageUrl = checkoutFacade.getTruckloadImage(form.getTwoSeaterQty(), form.getFiveSeaterQty());
			resultMap.put(TRUCKLOAD_IMG_URL, imageUrl);

			resultMap = checkoutFacade.doAddToCart(form.getQuantities(), form.getProductCodes(), form.getProductNames(),
					form.getShipAfterDate(), resultMap, imageUrl);
			resultMap.put(SUCCESS, Boolean.TRUE);
		}

		return resultMap;
	}

	/**
	 * This method removes the cart entry based on truckload id.
	 *
	 * @param truckloadId
	 *           the truckload id
	 * @return the map - success/failure
	 */
	@ResponseBody
	@RequestMapping(value = "/removeTruckload/", method = RequestMethod.GET)
	public Map<String, Object> removeTruckload(@RequestParam("truckloadId") final String truckloadId)
	{
		validateParameterNotNull(truckloadId, TRUCKLOAD_ID_NOT_NULL);
		final Map<String, Object> resultMap = new HashMap();
		try
		{
			checkoutFacade.removeTruckload(truckloadId);
			resultMap.put(SUCCESS, Boolean.TRUE);
			resultMap.put(TOTAL_CART_QTY, cartFacade.getSessionCart().getTotalUnitCount());
		}
		catch (final CalculationException e)
		{
			LOG.warn("Unable to update cart : " + e);
			resultMap.put(SUCCESS, Boolean.FALSE);
		}
		return resultMap;
	}

	/**
	 * This method update the cart entry based on truckload id.
	 *
	 * @param truckloadId
	 *           the truckload id
	 * @param form
	 *           the Cat truckload form
	 * @return the result map
	 * @throws CalculationException
	 *            the calculation exception
	 * @throws CommerceCartModificationException
	 *            the commerce cart modification exception
	 * @throws ParseException
	 *            the parse exception
	 */
	@ResponseBody
	@RequestMapping(value = "/updateTruckload/", method = RequestMethod.POST)
	public Map<String, Object> updateTruckload(@RequestParam("truckloadId") final String truckloadId, final CatTruckloadForm form)
			throws CalculationException, CommerceCartModificationException, ParseException
	{
		Map<String, Object> resultMap = new HashMap();

		final boolean isValid = checkoutFacade.validateTruckload(form.getTwoSeaterQty(), form.getFiveSeaterQty());

		if (!isValid) //Not a valid truckload
		{
			resultMap = checkoutFacade.setSuggestionAndMessage(resultMap, form.getTwoSeaterQty(), form.getFiveSeaterQty());
		}
		else // Valid truckload combination, remove the previous cart entries for given truckloadId and add new cart entries as per updated quantities
		{
			checkoutFacade.removeTruckload(truckloadId);
			final String imageUrl = checkoutFacade.getTruckloadImage(form.getTwoSeaterQty(), form.getFiveSeaterQty());
			resultMap.put(TRUCKLOAD_IMG_URL, imageUrl);

			resultMap = checkoutFacade.doAddToCart(form.getQuantities(), form.getProductCodes(), form.getProductNames(),
					form.getShipAfterDate(), resultMap, imageUrl);
			resultMap.put(SUCCESS, Boolean.TRUE);
		}

		return resultMap;
	}


}