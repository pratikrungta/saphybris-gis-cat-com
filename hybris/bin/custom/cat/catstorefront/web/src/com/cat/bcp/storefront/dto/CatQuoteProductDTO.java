/**
 *
 */
package com.cat.bcp.storefront.dto;

/**
 * This is the response for quote products with no stock
 *
 * @author ridjain
 *
 */
public class CatQuoteProductDTO
{
	private String code;
	private String name;
	private String baseProduct;
	private String salesModel;
	private String imageUrl;
	private String productFamily;
	private String primaryOfferingName;

	/**
	 * @return the code
	 */
	public String getCode()
	{
		return code;
	}

	/**
	 * @param code
	 *           the code to set
	 */
	public void setCode(final String code)
	{
		this.code = code;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *           the name to set
	 */
	public void setName(final String name)
	{
		this.name = name;
	}

	/**
	 * @return the imageUrl
	 */
	public String getImageUrl()
	{
		return imageUrl;
	}

	/**
	 * @param imageUrl
	 *           the imageUrl to set
	 */
	public void setImageUrl(final String imageUrl)
	{
		this.imageUrl = imageUrl;
	}

	/**
	 * @return the productFamily
	 */
	public String getProductFamily()
	{
		return productFamily;
	}

	/**
	 * @param productFamily
	 *           the productFamily to set
	 */
	public void setProductFamily(final String productFamily)
	{
		this.productFamily = productFamily;
	}

	/**
	 * @return the baseProduct
	 */
	public String getBaseProduct()
	{
		return baseProduct;
	}

	/**
	 * @param baseProduct
	 *           the baseProduct to set
	 */
	public void setBaseProduct(final String baseProduct)
	{
		this.baseProduct = baseProduct;
	}

	/**
	 * @return the salesModel
	 */
	public String getSalesModel()
	{
		return salesModel;
	}

	/**
	 * @param salesModel
	 *           the salesModel to set
	 */
	public void setSalesModel(final String salesModel)
	{
		this.salesModel = salesModel;
	}

	/**
	 * @return the primaryOfferingName
	 */
	public String getPrimaryOfferingName()
	{
		return primaryOfferingName;
	}

	/**
	 * @param primaryOfferingName
	 *           the primaryOfferingName to set
	 */
	public void setPrimaryOfferingName(final String primaryOfferingName)
	{
		this.primaryOfferingName = primaryOfferingName;
	}



}
