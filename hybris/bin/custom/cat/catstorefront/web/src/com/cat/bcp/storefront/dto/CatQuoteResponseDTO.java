/**
 *
 */
package com.cat.bcp.storefront.dto;

import java.util.List;


/**
 * This is the response object when we save/edit quote
 *
 * @author vjagannadharaotel
 *
 */
public class CatQuoteResponseDTO
{
	private String quoteCode;
	private String quoteName;
	private String quoteCustomer;
	private Boolean status;
	private String errorMessage;
	private Boolean userExists;
	private Boolean cartExists;
	private Boolean stockNotExists;
	private List<CatQuoteProductDTO> productsWithNoStock;

	/**
	 * @return the productsWithNoStock
	 */
	public List<CatQuoteProductDTO> getProductsWithNoStock()
	{
		return productsWithNoStock;
	}

	/**
	 * @param productsWithNoStock
	 *           the productsWithNoStock to set
	 */
	public void setProductsWithNoStock(final List<CatQuoteProductDTO> productsWithNoStock)
	{
		this.productsWithNoStock = productsWithNoStock;
	}

	/**
	 * @return the userExists
	 */
	public Boolean getUserExists()
	{
		return userExists;
	}

	/**
	 * @param userExists
	 *           the userExists to set
	 */
	public void setUserExists(final Boolean userExists)
	{
		this.userExists = userExists;
	}

	/**
	 * @return the quoteCode
	 */
	public String getQuoteCode()
	{
		return quoteCode;
	}

	/**
	 * @param quoteCode
	 *           the quoteCode to set
	 */
	public void setQuoteCode(final String quoteCode)
	{
		this.quoteCode = quoteCode;
	}

	/**
	 * @return the quoteName
	 */
	public String getQuoteName()
	{
		return quoteName;
	}

	/**
	 * @param quoteName
	 *           the quoteName to set
	 */
	public void setQuoteName(final String quoteName)
	{
		this.quoteName = quoteName;
	}

	/**
	 * @return the status
	 */
	public Boolean getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *           the status to set
	 */
	public void setStatus(final Boolean status)
	{
		this.status = status;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage()
	{
		return errorMessage;
	}

	/**
	 * @param errorMessage
	 *           the errorMessage to set
	 */
	public void setErrorMessage(final String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	/**
	 * @return the quoteCustomer
	 */
	public String getQuoteCustomer()
	{
		return quoteCustomer;
	}

	/**
	 * @param quoteCustomer
	 *           the quoteCustomer to set
	 */
	public void setQuoteCustomer(final String quoteCustomer)
	{
		this.quoteCustomer = quoteCustomer;
	}

	/**
	 * @return the cartExists
	 */
	public Boolean getCartExists()
	{
		return cartExists;
	}

	/**
	 * @param cartExists
	 *           the cartExists to set
	 */
	public void setCartExists(final Boolean cartExists)
	{
		this.cartExists = cartExists;
	}

	/**
	 * @return the stockNotExists
	 */
	public Boolean getStockNotExists()
	{
		return stockNotExists;
	}

	/**
	 * @param stockNotExists
	 *           the stockNotExists to set
	 */
	public void setStockNotExists(final Boolean stockNotExists)
	{
		this.stockNotExists = stockNotExists;
	}
}
