/**
 *
 */
package com.cat.bcp.storefront.filters;

import de.hybris.platform.util.Config;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.storefront.util.CatOpenIdConnectUserDetails;
import com.cat.bcp.storefront.util.CatOpenIdSSOUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * @author prrungta
 *
 */
public class CatOpenIdConnectFilter extends AbstractAuthenticationProcessingFilter
{


	private static final Logger LOGGER = Logger.getLogger(CatOpenIdConnectFilter.class);

	private String clientId;
	private String issuer;

	private OAuth2RestOperations oauth2RestTemplate;
	private CatOpenIdSSOUtil catOpenIdSSOUtil;

	/**
	 * Constructor.
	 *
	 * @param defaultFilterProcessesUrl
	 *           String default filter URL.
	 */
	public CatOpenIdConnectFilter(final String defaultFilterProcessesUrl)
	{
		super(defaultFilterProcessesUrl);
	}


	/**
	 * {@inheritDoc}
	 */

	@Override
	public Authentication attemptAuthentication(final HttpServletRequest request, final HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		try
		{
			final String code = request.getParameter("code");
			final String state = request.getParameter("state");

			auth = this.getAuthenticationManager().authenticate(handleAuthentication(code, state));

		}

		catch (final BadCredentialsException | DisabledException | OAuth2Exception e)
		{
			LOGGER.error("Credentials Excpetion occured : Could not obtain user details from token : ", e);
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			response.sendRedirect("/openidError");
		}
		return auth;
	}

	/**
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	private Authentication handleAuthentication(final String code, final String state)
			throws OAuth2Exception, JsonParseException, JsonMappingException, IOException
	{
		OAuth2AccessToken accessToken;

		final OAuth2ClientContext auth2ClientContext = getOauth2RestTemplate().getOAuth2ClientContext();
		accessToken = getCatOpenIdSSOUtil().getAccessToken();
		final AccessTokenRequest accessTokenRequest = auth2ClientContext.getAccessTokenRequest();

		if (accessToken == null)
		{
			// if user removes the session.
			accessTokenRequest.setAuthorizationCode(code);
			accessTokenRequest.setStateKey(state);

			auth2ClientContext.setAccessToken(null);// NO SONAR
		}

		auth2ClientContext.setPreservedState(state, Config.getParameter("oauth2.cat.redirectUri"));
		accessToken = getOauth2RestTemplate().getAccessToken();

		final String accessTokenValue = accessToken.getValue();
		final Jwt tokenDecoded = JwtHelper.decode(accessTokenValue);
		final Map<String, String> authInfo = new ObjectMapper().readValue(tokenDecoded.getClaims(), Map.class);
		verifyClaims(authInfo);
		final CatOpenIdConnectUserDetails user = new CatOpenIdConnectUserDetails(authInfo, accessToken);
		return new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());

	}

	/**
	 * method to verify the token claims.
	 *
	 * @param claims
	 *           Map
	 */
	public void verifyClaims(final Map claims)
	{
		final int exp = (int) claims.get(CatCoreConstants.OIDC_EXPIRATION);
		final Date expireDate = new Date(exp * 1000L);
		final Date now = new Date();
		if (expireDate.before(now) || !claims.get(CatCoreConstants.OIDC_ISSUER).equals(issuer)
				|| !claims.get(CatCoreConstants.OIDC_CLIENT_ID).equals(clientId))
		{
			LOGGER.warn("Exception in verifying claims : Invalid Claims : ");
			throw new BadCredentialsException("Invalid Claims,User is not verified "); //NOSONAR
		}
	}


	/**
	 * @return the clientId
	 */
	public String getClientId()
	{
		return clientId;
	}

	/**
	 * @param clientId
	 *           the clientId to set
	 */
	public void setClientId(final String clientId)
	{
		this.clientId = clientId;
	}

	/**
	 * @return the issuer
	 */
	public String getIssuer()
	{
		return issuer;
	}

	/**
	 * @param issuer
	 *           the issuer to set
	 */
	public void setIssuer(final String issuer)
	{
		this.issuer = issuer;
	}

	/**
	 * @return the oauth2RestTemplate
	 */
	public OAuth2RestOperations getOauth2RestTemplate()
	{
		return oauth2RestTemplate;
	}

	/**
	 * @param oauth2RestTemplate
	 *           the oauth2RestTemplate to set
	 */
	public void setOauth2RestTemplate(final OAuth2RestOperations oauth2RestTemplate)
	{
		this.oauth2RestTemplate = oauth2RestTemplate;
	}

	@Override
	protected boolean requiresAuthentication(final HttpServletRequest request, final HttpServletResponse response)
	{
		return getCatOpenIdSSOUtil().requiresAuthentication();
	}


	/**
	 * @return the catOpenIdSSOUtil
	 */
	public CatOpenIdSSOUtil getCatOpenIdSSOUtil()
	{
		return catOpenIdSSOUtil;
	}


	/**
	 * @param catOpenIdSSOUtil
	 *           the catOpenIdSSOUtil to set
	 */
	public void setCatOpenIdSSOUtil(final CatOpenIdSSOUtil catOpenIdSSOUtil)
	{
		this.catOpenIdSSOUtil = catOpenIdSSOUtil;
	}



}
