/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.storefront.forms;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


/**
 * @author vjagannadharaotel
 *
 */
public class CatAddToCartForm
{
	@NotNull(message = "{basket.error.quantity.notNull}")
	@Min(value = 0, message = "{basket.error.quantity.invalid}")
	@Digits(fraction = 0, integer = 10, message = "{basket.error.quantity.invalid}")
	private long qty = 1L;

	private String productCodePost;

	private boolean configurable;


	/**
	 * @return the configurable
	 */
	public boolean isConfigurable()
	{
		return configurable;
	}

	/**
	 * @param configurable
	 *           the configurable to set
	 */
	public void setConfigurable(final boolean configurable)
	{
		this.configurable = configurable;
	}

	/**
	 * @return the laneType
	 */
	public String getLaneType()
	{
		return laneType;
	}

	/**
	 * @param laneType
	 *           the laneType to set
	 */
	public void setLaneType(final String laneType)
	{
		this.laneType = laneType;
	}

	private String laneType;

	/**
	 * @param quantity
	 */
	public void setQty(final long quantity)
	{
		this.qty = quantity;
	}

	/**
	 * @return
	 */
	public long getQty()
	{
		return qty;
	}

	/**
	 * @return the productCodePost
	 */
	public String getProductCodePost()
	{
		return productCodePost;
	}

	/**
	 * @param productCodePost
	 *           the productCodePost to set
	 */
	public void setProductCodePost(final String productCodePost)
	{
		this.productCodePost = productCodePost;
	}
}
