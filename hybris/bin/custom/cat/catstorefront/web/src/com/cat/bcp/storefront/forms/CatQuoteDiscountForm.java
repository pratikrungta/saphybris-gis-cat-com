/**
 *
 */
package com.cat.bcp.storefront.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.QuoteDiscountForm;


/**
 * @author sagdhingra
 *
 */
public class CatQuoteDiscountForm extends QuoteDiscountForm
{

	long entryNumber;

	/**
	 * @return the entryNumber
	 */
	public long getEntryNumber()
	{
		return entryNumber;
	}

	/**
	 * @param entryNumber
	 *           the entryNumber to set
	 */
	public void setEntryNumber(final long entryNumber)
	{
		this.entryNumber = entryNumber;
	}

}
