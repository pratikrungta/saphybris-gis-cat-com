/**
 *
 */
package com.cat.bcp.storefront.forms;

import de.hybris.platform.acceleratorstorefrontcommons.forms.AddressForm;
import de.hybris.platform.acceleratorstorefrontcommons.forms.QuoteForm;
import de.hybris.platform.commercefacades.order.data.CatQuoteSerialNumbersData;


/**
 * @author Deloitte
 *
 */
public class CatQuoteForm extends QuoteForm
{
	private String customerName;
	private String customerEmail;
	private String customerUid;
	private AddressForm defaultShippingAddress;
	private AddressForm defaultBillingAddress;
	private Boolean isShippingSameAsBilling;
	private String eppAdditionalServices;
	private String csaAdditionalServices;
	private Boolean existingCustomer;
	private String quoteComments;
	private Boolean sendToDSM;
	private String quoteStatus;
	private Boolean editMode;
	private String quoteCode;
	private String quoteLostReason;
	private String submittedToCustomerDate;
	private String quoteState;
	private String customerResponseDate;
	private CatQuoteSerialNumbersData catQuoteSerialNumbersData;

	/**
	 * @return the catQuoteSerialNumbersData
	 */
	public CatQuoteSerialNumbersData getCatQuoteSerialNumbersData()
	{
		return catQuoteSerialNumbersData;
	}

	/**
	 * @param catQuoteSerialNumbersData
	 *           the catQuoteSerialNumbersData to set
	 */
	public void setCatQuoteSerialNumbersData(final CatQuoteSerialNumbersData catQuoteSerialNumbersData)
	{
		this.catQuoteSerialNumbersData = catQuoteSerialNumbersData;
	}

	/**
	 * @return the quoteLostReason
	 */
	public String getQuoteLostReason()
	{
		return quoteLostReason;
	}

	/**
	 * @param quoteLostReason
	 *           the quoteLostReason to set
	 */
	public void setQuoteLostReason(final String quoteLostReason)
	{
		this.quoteLostReason = quoteLostReason;
	}

	/**
	 * @return the submittedToCustomerDate
	 */
	public String getSubmittedToCustomerDate()
	{
		return submittedToCustomerDate;
	}

	/**
	 * @param submittedToCustomerDate
	 *           the submittedToCustomerDate to set
	 */
	public void setSubmittedToCustomerDate(final String submittedToCustomerDate)
	{
		this.submittedToCustomerDate = submittedToCustomerDate;
	}

	/**
	 * @return the quoteState
	 */
	public String getQuoteState()
	{
		return quoteState;
	}

	/**
	 * @param quoteState
	 *           the quoteState to set
	 */
	public void setQuoteState(final String quoteState)
	{
		this.quoteState = quoteState;
	}

	/**
	 * @return the customerResponseDate
	 */
	public String getCustomerResponseDate()
	{
		return customerResponseDate;
	}

	/**
	 * @param customerResponseDate
	 *           the customerResponseDate to set
	 */
	public void setCustomerResponseDate(final String customerResponseDate)
	{
		this.customerResponseDate = customerResponseDate;
	}

	/**
	 * @param quoteComments
	 *           the quoteComments to set
	 */
	public void setQuoteComments(final String quoteComments)
	{
		this.quoteComments = quoteComments;
	}

	/**
	 * @return the sendToDSM
	 */
	public Boolean getSendToDSM()
	{
		return sendToDSM;
	}

	/**
	 * @param sendToDSM
	 *           the sendToDSM to set
	 */
	public void setSendToDSM(final Boolean sendToDSM)
	{
		this.sendToDSM = sendToDSM;
	}

	/**
	 * @return the quoteStatus
	 */
	public String getQuoteStatus()
	{
		return quoteStatus;
	}

	/**
	 * @param quoteStatus
	 *           the quoteStatus to set
	 */
	public void setQuoteStatus(final String quoteStatus)
	{
		this.quoteStatus = quoteStatus;
	}

	/**
	 * @return the quoteComments
	 */
	public String getQuoteComments()
	{
		return quoteComments;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName()
	{
		return customerName;
	}

	/**
	 * @param customerName
	 *           the customerName to set
	 */
	public void setCustomerName(final String customerName)
	{
		this.customerName = customerName;
	}


	/**
	 * @return the customerEmail
	 */
	public String getCustomerEmail()
	{
		return customerEmail;
	}

	/**
	 * @param customerEmail
	 *           the customerEmail to set
	 */
	public void setCustomerEmail(final String customerEmail)
	{
		this.customerEmail = customerEmail;
	}

	/**
	 * @return the isShippingSameAsBilling
	 */
	public Boolean getIsShippingSameAsBilling()
	{
		return isShippingSameAsBilling;
	}

	/**
	 * @param isShippingSameAsBilling
	 *           the isShippingSameAsBilling to set
	 */
	public void setIsShippingSameAsBilling(final Boolean isShippingSameAsBilling)
	{
		this.isShippingSameAsBilling = isShippingSameAsBilling;
	}

	/**
	 * @return the eppAdditionalServices
	 */
	public String getEppAdditionalServices()
	{
		return eppAdditionalServices;
	}

	/**
	 * @param eppAdditionalServices
	 *           the eppAdditionalServices to set
	 */
	public void setEppAdditionalServices(final String eppAdditionalServices)
	{
		this.eppAdditionalServices = eppAdditionalServices;
	}

	/**
	 * @return the csaAdditionalServices
	 */
	public String getCsaAdditionalServices()
	{
		return csaAdditionalServices;
	}

	/**
	 * @param csaAdditionalServices
	 *           the csaAdditionalServices to set
	 */
	public void setCsaAdditionalServices(final String csaAdditionalServices)
	{
		this.csaAdditionalServices = csaAdditionalServices;
	}

	/**
	 * @return the existingCustomer
	 */
	public Boolean getExistingCustomer()
	{
		return existingCustomer;
	}

	/**
	 * @param existingCustomer
	 *           the existingCustomer to set
	 */
	public void setExistingCustomer(final Boolean existingCustomer)
	{
		this.existingCustomer = existingCustomer;
	}

	/**
	 * @return the defaultShippingAddress
	 */
	public AddressForm getDefaultShippingAddress()
	{
		return defaultShippingAddress;
	}

	/**
	 * @param defaultShippingAddress
	 *           the defaultShippingAddress to set
	 */
	public void setDefaultShippingAddress(final AddressForm defaultShippingAddress)
	{
		this.defaultShippingAddress = defaultShippingAddress;
	}

	/**
	 * @return the defaultBillingAddress
	 */
	public AddressForm getDefaultBillingAddress()
	{
		return defaultBillingAddress;
	}

	/**
	 * @param defaultBillingAddress
	 *           the defaultBillingAddress to set
	 */
	public void setDefaultBillingAddress(final AddressForm defaultBillingAddress)
	{
		this.defaultBillingAddress = defaultBillingAddress;
	}

	/**
	 * @return the editMode
	 */
	public Boolean getEditMode()
	{
		return editMode;
	}

	/**
	 * @param editMode
	 *           the editMode to set
	 */
	public void setEditMode(final Boolean editMode)
	{
		this.editMode = editMode;
	}

	/**
	 * @return the quoteCode
	 */
	public String getQuoteCode()
	{
		return quoteCode;
	}

	/**
	 * @param quoteCode
	 *           the quoteCode to set
	 */
	public void setQuoteCode(final String quoteCode)
	{
		this.quoteCode = quoteCode;
	}

	/**
	 * @return the customerUid
	 */
	public String getCustomerUid()
	{
		return customerUid;
	}

	/**
	 * @param customerUid
	 *           the customerUid to set
	 */
	public void setCustomerUid(final String customerUid)
	{
		this.customerUid = customerUid;
	}


}
