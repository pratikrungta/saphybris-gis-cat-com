/**
 *
 */
package com.cat.bcp.storefront.forms;

import de.hybris.platform.commercefacades.product.data.PriceData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.enums.QuoteState;

import java.util.Date;

/**
 * @author sagdhingra
 *
 */

import com.cat.bcp.core.enums.CatQuoteLostReasonEnum;


public class CatQuoteResponseData
{


	private Integer version;


	private QuoteState state;


	private Date creationTime;


	private Date updatedTime;


	private Boolean hasCart;


	private String orderCode;


	private PriceData previousEstimatedTotal;


	private CustomerData quoteCustomer;


	private String customerName;


	private String customerEmail;


	private String eppAdditionalServices;


	private String csaAdditionalServices;


	private String quoteComments;


	private Boolean sendToDSM;


	private Date submittedToCustomerDate;

	private Date expirationTime;

	private Date acceptedByCustomerDate;


	private CatQuoteLostReasonEnum quoteLostReason;

	private String code;


	private String name;


	private String description;

	private AddressData deliveryAddress;

	private AddressData paymentAddress;

	private Boolean isShippingSameAsBilling;


	public CatQuoteResponseData()
	{
		// default constructor
	}

	/**
	 * @return the isShippingSameAsBilling
	 */
	public Boolean getIsShippingSameAsBilling()
	{
		return isShippingSameAsBilling;
	}



	/**
	 * @param isShippingSameAsBilling
	 *           the isShippingSameAsBilling to set
	 */
	public void setIsShippingSameAsBilling(final Boolean isShippingSameAsBilling)
	{
		this.isShippingSameAsBilling = isShippingSameAsBilling;
	}



	/**
	 * @return the deliveryAddress
	 */
	public AddressData getDeliveryAddress()
	{
		return deliveryAddress;
	}



	/**
	 * @param deliveryAddress
	 *           the deliveryAddress to set
	 */
	public void setDeliveryAddress(final AddressData deliveryAddress)
	{
		this.deliveryAddress = deliveryAddress;
	}



	/**
	 * @return the paymentAddress
	 */
	public AddressData getPaymentAddress()
	{
		return paymentAddress;
	}



	/**
	 * @param paymentAddress
	 *           the paymentAddress to set
	 */
	public void setPaymentAddress(final AddressData paymentAddress)
	{
		this.paymentAddress = paymentAddress;
	}


	public void setVersion(final Integer version)
	{
		this.version = version;
	}



	public Integer getVersion()
	{
		return version;
	}



	public void setState(final QuoteState state)
	{
		this.state = state;
	}



	public QuoteState getState()
	{
		return state;
	}



	public void setCreationTime(final Date creationTime)
	{
		this.creationTime = creationTime != null ? (Date) creationTime.clone() : null;
	}



	public Date getCreationTime()
	{
		return creationTime != null ? (Date) creationTime.clone() : null;
	}



	public void setUpdatedTime(final Date updatedTime)
	{
		this.updatedTime = updatedTime != null ? (Date) updatedTime.clone() : null;
	}



	public Date getUpdatedTime()
	{
		return updatedTime != null ? (Date) updatedTime.clone() : null;
	}



	public void setHasCart(final Boolean hasCart)
	{
		this.hasCart = hasCart;
	}



	public Boolean getHasCart()
	{
		return hasCart;
	}



	public void setOrderCode(final String orderCode)
	{
		this.orderCode = orderCode;
	}



	public String getOrderCode()
	{
		return orderCode;
	}



	public void setPreviousEstimatedTotal(final PriceData previousEstimatedTotal)
	{
		this.previousEstimatedTotal = previousEstimatedTotal;
	}



	public PriceData getPreviousEstimatedTotal()
	{
		return previousEstimatedTotal;
	}



	public void setQuoteCustomer(final CustomerData quoteCustomer)
	{
		this.quoteCustomer = quoteCustomer;
	}



	public CustomerData getQuoteCustomer()
	{
		return quoteCustomer;
	}



	public void setCustomerName(final String customerName)
	{
		this.customerName = customerName;
	}



	public String getCustomerName()
	{
		return customerName;
	}



	public void setCustomerEmail(final String customerEmail)
	{
		this.customerEmail = customerEmail;
	}



	public String getCustomerEmail()
	{
		return customerEmail;
	}



	public void setEppAdditionalServices(final String eppAdditionalServices)
	{
		this.eppAdditionalServices = eppAdditionalServices;
	}



	public String getEppAdditionalServices()
	{
		return eppAdditionalServices;
	}



	public void setCsaAdditionalServices(final String csaAdditionalServices)
	{
		this.csaAdditionalServices = csaAdditionalServices;
	}



	public String getCsaAdditionalServices()
	{
		return csaAdditionalServices;
	}



	public void setQuoteComments(final String quoteComments)
	{
		this.quoteComments = quoteComments;
	}



	public String getQuoteComments()
	{
		return quoteComments;
	}



	public void setSendToDSM(final Boolean sendToDSM)
	{
		this.sendToDSM = sendToDSM;
	}



	public Boolean getSendToDSM()
	{
		return sendToDSM;
	}



	public void setSubmittedToCustomerDate(final Date submittedToCustomerDate)
	{
		this.submittedToCustomerDate = submittedToCustomerDate != null ? (Date) submittedToCustomerDate.clone() : null;
	}



	public Date getSubmittedToCustomerDate()
	{
		return submittedToCustomerDate != null ? (Date) submittedToCustomerDate.clone() : null;
	}



	public void setAcceptedByCustomerDate(final Date acceptedByCustomerDate)
	{
		this.acceptedByCustomerDate = acceptedByCustomerDate != null ? (Date) acceptedByCustomerDate.clone() : null;
	}



	public Date getAcceptedByCustomerDate()
	{
		return acceptedByCustomerDate != null ? (Date) acceptedByCustomerDate.clone() : null;
	}



	public void setQuoteLostReason(final CatQuoteLostReasonEnum quoteLostReason)
	{
		this.quoteLostReason = quoteLostReason;
	}



	public CatQuoteLostReasonEnum getQuoteLostReason()
	{
		return quoteLostReason;
	}



	/**
	 * @return the code
	 */
	public String getCode()
	{
		return code;
	}



	/**
	 * @param code
	 *           the code to set
	 */
	public void setCode(final String code)
	{
		this.code = code;
	}



	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}



	/**
	 * @param name
	 *           the name to set
	 */
	public void setName(final String name)
	{
		this.name = name;
	}



	/**
	 * @return the description
	 */
	public String getDescription()
	{
		return description;
	}



	/**
	 * @param description
	 *           the description to set
	 */
	public void setDescription(final String description)
	{
		this.description = description;
	}



	/**
	 * @return the expirationTime
	 */
	public Date getExpirationTime()
	{
		return expirationTime != null ? (Date) expirationTime.clone() : null;
	}



	/**
	 * @param expirationTime
	 *           the expirationTime to set
	 */
	public void setExpirationTime(final Date expirationTime)
	{
		this.expirationTime = expirationTime != null ? (Date) expirationTime.clone() : null;
	}



}
