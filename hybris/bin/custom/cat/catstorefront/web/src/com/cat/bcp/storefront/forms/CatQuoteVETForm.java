/**
 *
 */
package com.cat.bcp.storefront.forms;

import java.util.List;

import com.cat.facades.quote.CatQuoteVETData;


/**
 * @author Deloitte
 *
 */
public class CatQuoteVETForm
{

	long entryNumber;
	private String vetLink;
	private String vetProduct;
	private List<CatQuoteVETData> catQuoteVETDatas;

	/**
	 * @return the entryNumber
	 */
	public long getEntryNumber()
	{
		return entryNumber;
	}

	/**
	 * @param entryNumber
	 *           the entryNumber to set
	 */
	public void setEntryNumber(final long entryNumber)
	{
		this.entryNumber = entryNumber;
	}

	/**
	 * @return the vetLink
	 */
	public String getVetLink()
	{
		return vetLink;
	}

	/**
	 * @param vetLink
	 *           the vetLink to set
	 */
	public void setVetLink(final String vetLink)
	{
		this.vetLink = vetLink;
	}

	/**
	 * @return the vetProduct
	 */
	public String getVetProduct()
	{
		return vetProduct;
	}

	/**
	 * @param vetProduct
	 *           the vetProduct to set
	 */
	public void setVetProduct(final String vetProduct)
	{
		this.vetProduct = vetProduct;
	}

	/**
	 * @return the catQuoteVETDatas
	 */
	public List<CatQuoteVETData> getCatQuoteVETDatas()
	{
		return catQuoteVETDatas;
	}

	/**
	 * @param catQuoteVETDatas
	 *           the catQuoteVETDatas to set
	 */
	public void setCatQuoteVETDatas(final List<CatQuoteVETData> catQuoteVETDatas)
	{
		this.catQuoteVETDatas = catQuoteVETDatas;
	}


}
