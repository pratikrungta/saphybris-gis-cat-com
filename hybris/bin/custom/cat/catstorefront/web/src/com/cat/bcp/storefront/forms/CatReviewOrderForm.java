/**
 *
 */
package com.cat.bcp.storefront.forms;

import java.util.List;



/**
 * @author manjam
 * 
 * 
 */
public class CatReviewOrderForm
{

	private Boolean identical;
	private Boolean acceptance;
	private String salesModelCode;
	private String configModelCode;



	private Integer orderedQty;
	private List<String> purchaseOrderList;
	private String comments;
	private String selectedShippingAddressPK;
	private List<String> shippingAddressList;

	/**
	 * @return the purchaseOrderList
	 */
	public List<String> getPurchaseOrderList()
	{
		return purchaseOrderList;
	}

	/**
	 * @param purchaseOrderList
	 *           the purchaseOrderList to set
	 */
	public void setPurchaseOrderList(final List<String> purchaseOrderList)
	{
		this.purchaseOrderList = purchaseOrderList;
	}

	private String shippingAddress;

	/**
	 * @return the shippingAddress
	 */
	public String getShippingAddress()
	{
		return shippingAddress;
	}

	/**
	 * @param shippingAddress
	 *           the shippingAddress to set
	 */
	public void setShippingAddress(final String shippingAddress)
	{
		this.shippingAddress = shippingAddress;
	}



	/**
	 * @return the configModelCode
	 */
	public String getConfigModelCode()
	{
		return configModelCode;
	}

	/**
	 * @param configModelCode
	 *           the configModelCode to set
	 */
	public void setConfigModelCode(final String configModelCode)
	{
		this.configModelCode = configModelCode;
	}

	/**
	 * @return the comments
	 */
	public String getComments()
	{
		return comments;
	}

	/**
	 * @param comments
	 *           the comments to set
	 */
	public void setComments(final String comments)
	{
		this.comments = comments;
	}

	/**
	 * @return the identical
	 */
	public Boolean getIdentical()
	{
		return identical;
	}

	/**
	 * @param identical
	 *           the identical to set
	 */
	public void setIdentical(final Boolean identical)
	{
		this.identical = identical;
	}

	/**
	 * @return the acceptance
	 */
	public Boolean getAcceptance()
	{
		return acceptance;
	}

	/**
	 * @param acceptance
	 *           the acceptance to set
	 */
	public void setAcceptance(final Boolean acceptance)
	{
		this.acceptance = acceptance;
	}

	/**
	 * @return the salesModelCode
	 */
	public String getSalesModelCode()
	{
		return salesModelCode;
	}

	/**
	 * @param salesModelCode
	 *           the salesModelCode to set
	 */
	public void setSalesModelCode(final String salesModelCode)
	{
		this.salesModelCode = salesModelCode;
	}

	/**
	 * @return the orderedQty
	 */
	public Integer getOrderedQty()
	{
		return orderedQty;
	}

	/**
	 * @param orderedQty
	 *           the orderedQty to set
	 */
	public void setOrderedQty(final Integer orderedQty)
	{
		this.orderedQty = orderedQty;
	}


	/**
	 * @return the selectedShippingAddressPK
	 */
	public String getSelectedShippingAddressPK()
	{
		return selectedShippingAddressPK;
	}

	/**
	 * @param selectedShippingAddressPK the selectedShippingAddressPK to set
	 */
	public void setSelectedShippingAddressPK(final String selectedShippingAddressPK)
	{
		this.selectedShippingAddressPK = selectedShippingAddressPK;
	}


	public List<String> getShippingAddressList() {
		return shippingAddressList;
	}

	public void setShippingAddressList(List<String> shippingAddressList) {
		this.shippingAddressList = shippingAddressList;
	}
}