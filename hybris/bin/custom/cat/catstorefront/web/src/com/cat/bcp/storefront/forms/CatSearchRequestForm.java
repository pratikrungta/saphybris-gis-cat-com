/**
 *
 */
package com.cat.bcp.storefront.forms;

import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractSearchPageController.ShowMode;


/**
 * @author asomjal
 *
 */
public class CatSearchRequestForm
{
	private String q;
	private String category = "";
	private String primaryOfferingId = "";
	private String salesModelId = "";
	private String dealerCertified;
	private Integer hoursUsedMin;
	private Integer hoursUsedMax;
	private Integer priceMin;
	private Integer priceMax;
	private Integer manufacturingYearMin;
	private Integer manufacturingYearMax;
	private String laneType = "all";
	private String[] selectedBranchLocation;
	private boolean defaultFacetValue = true;
	private boolean isUsed = false;
	private int page = 0;
	private ShowMode show = ShowMode.Page;
	private String sort;
	private boolean inTransitFlag = false;
	private boolean backwardNavigationFlag = false;

	/**
	 * @return the q
	 */
	public String getQ()
	{
		return q;
	}

	/**
	 * @param q
	 *           the q to set
	 */
	public void setQ(final String q)
	{
		this.q = q;
	}

	/**
	 * @return the isUsed
	 */
	public boolean isUsed()
	{
		return isUsed;
	}

	/**
	 * @param isUsed
	 *           the isUsed to set
	 */
	public void setIsUsed(final boolean isUsed)
	{
		this.isUsed = isUsed;
	}

	/**
	 * @return the category
	 */
	public String getCategory()
	{
		return category;
	}

	/**
	 * @param category
	 *           the category to set
	 */
	public void setCategory(final String category)
	{
		this.category = category;
	}

	/**
	 * @return the primaryOfferingId
	 */
	public String getPrimaryOfferingId()
	{
		return primaryOfferingId;
	}

	/**
	 * @param primaryOfferingId
	 *           the primaryOfferingId to set
	 */
	public void setPrimaryOfferingId(final String primaryOfferingId)
	{
		this.primaryOfferingId = primaryOfferingId;
	}

	/**
	 * @return the salesModelId
	 */
	public String getSalesModelId()
	{
		return salesModelId;
	}

	/**
	 * @param salesModelId
	 *           the salesModelId to set
	 */
	public void setSalesModelId(final String salesModelId)
	{
		this.salesModelId = salesModelId;
	}

	/**
	 * @return the dealerCertified
	 */
	public String getDealerCertified()
	{
		return dealerCertified;
	}

	/**
	 * @param dealerCertified
	 *           the dealerCertified to set
	 */
	public void setDealerCertified(final String dealerCertified)
	{
		this.dealerCertified = dealerCertified;
	}

	/**
	 * @return the hoursUsedMin
	 */
	public Integer getHoursUsedMin()
	{
		return hoursUsedMin;
	}

	/**
	 * @param hoursUsedMin
	 *           the hoursUsedMin to set
	 */
	public void setHoursUsedMin(final Integer hoursUsedMin)
	{
		this.hoursUsedMin = hoursUsedMin;
	}

	/**
	 * @return the hoursUsedMax
	 */
	public Integer getHoursUsedMax()
	{
		return hoursUsedMax;
	}

	/**
	 * @param hoursUsedMax
	 *           the hoursUsedMax to set
	 */
	public void setHoursUsedMax(final Integer hoursUsedMax)
	{
		this.hoursUsedMax = hoursUsedMax;
	}

	/**
	 * @return the priceMin
	 */
	public Integer getPriceMin()
	{
		return priceMin;
	}

	/**
	 * @param priceMin
	 *           the priceMin to set
	 */
	public void setPriceMin(final Integer priceMin)
	{
		this.priceMin = priceMin;
	}

	/**
	 * @return the priceMax
	 */
	public Integer getPriceMax()
	{
		return priceMax;
	}

	/**
	 * @param priceMax
	 *           the priceMax to set
	 */
	public void setPriceMax(final Integer priceMax)
	{
		this.priceMax = priceMax;
	}

	/**
	 * @return the manufacturingYearMin
	 */
	public Integer getManufacturingYearMin()
	{
		return manufacturingYearMin;
	}

	/**
	 * @param manufacturingYearMin
	 *           the manufacturingYearMin to set
	 */
	public void setManufacturingYearMin(final Integer manufacturingYearMin)
	{
		this.manufacturingYearMin = manufacturingYearMin;
	}

	/**
	 * @return the manufacturingYearMax
	 */
	public Integer getManufacturingYearMax()
	{
		return manufacturingYearMax;
	}

	/**
	 * @param manufacturingYearMax
	 *           the manufacturingYearMax to set
	 */
	public void setManufacturingYearMax(final Integer manufacturingYearMax)
	{
		this.manufacturingYearMax = manufacturingYearMax;
	}

	/**
	 * @return the selectedBranchLocation
	 */
	public String[] getSelectedBranchLocation()
	{
		return selectedBranchLocation;
	}

	/**
	 * @param selectedBranchLocation
	 *           the selectedBranchLocation to set
	 */
	public void setSelectedBranchLocation(final String[] selectedBranchLocation)
	{
		this.selectedBranchLocation = selectedBranchLocation;
	}

	/**
	 * @return the defaultFacetValue
	 */
	public boolean isDefaultFacetValue()
	{
		return defaultFacetValue;
	}

	/**
	 * @param defaultFacetValue
	 *           the defaultFacetValue to set
	 */
	public void setDefaultFacetValue(final boolean defaultFacetValue)
	{
		this.defaultFacetValue = defaultFacetValue;
	}

	/**
	 * @return the page
	 */
	public int getPage()
	{
		return page;
	}

	/**
	 * @param page
	 *           the page to set
	 */
	public void setPage(final int page)
	{
		this.page = page;
	}

	/**
	 * @return the show
	 */
	public ShowMode getShow()
	{
		return show;
	}

	/**
	 * @param show
	 *           the show to set
	 */
	public void setShow(final ShowMode show)
	{
		this.show = show;
	}

	/**
	 * @return the sort
	 */
	public String getSort()
	{
		return sort;
	}

	/**
	 * @param sort
	 *           the sort to set
	 */
	public void setSort(final String sort)
	{
		this.sort = sort;
	}

	/**
	 * @return the inTransitFlag
	 */
	public boolean isInTransitFlag()
	{
		return inTransitFlag;
	}

	/**
	 * @param inTransitFlag
	 *           the inTransitFlag to set
	 */
	public void setInTransitFlag(final boolean inTransitFlag)
	{
		this.inTransitFlag = inTransitFlag;
	}

	/**
	 * @return the backwardNavigationFlag
	 */
	public boolean isBackwardNavigationFlag()
	{
		return backwardNavigationFlag;
	}

	/**
	 * @param backwardNavigationFlag
	 *           the backwardNavigationFlag to set
	 */
	public void setBackwardNavigationFlag(final boolean backwardNavigationFlag)
	{
		this.backwardNavigationFlag = backwardNavigationFlag;
	}

	/**
	 * @return the laneType
	 */
	public String getLaneType()
	{
		return laneType;
	}

	/**
	 * @param laneType
	 *           the laneType to set
	 */
	public void setLaneType(final String laneType)
	{
		this.laneType = laneType;
	}

}
