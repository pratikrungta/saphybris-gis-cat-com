/**
 *
 */
package com.cat.bcp.storefront.forms;

/**
 * @author avaijapurkar
 *
 */
public class CatTruckloadForm
{
	private String code;
	private int[] twoSeaterQty;
	private int[] fiveSeaterQty;
	private String[] productCodes;
	private String[] productNames;
	private String shipAfterDate;
	private int[] quantities;

	/**
	 * @return the shipAfterDate
	 */
	public String getShipAfterDate()
	{
		return shipAfterDate;
	}

	/**
	 * @param shipAfterDate
	 *           the shipAfterDate to set
	 */
	public void setShipAfterDate(final String shipAfterDate)
	{
		this.shipAfterDate = shipAfterDate;
	}

	/**
	 * @return the productCodes
	 */
	public String[] getProductCodes()
	{
		return productCodes;
	}

	/**
	 * @param productCodes
	 *           the productCodes to set
	 */
	public void setProductCodes(final String[] productCodes)
	{
		this.productCodes = productCodes;
	}

	/**
	 * @return the quantities
	 */
	public int[] getQuantities()
	{
		return quantities;
	}

	/**
	 * @param quantities
	 *           the quantities to set
	 */
	public void setQuantities(final int[] quantities)
	{
		this.quantities = quantities;
	}

	/**
	 * @return the code
	 */
	public String getCode()
	{
		return code;
	}

	/**
	 * @param code
	 *           the code to set
	 */
	public void setCode(final String code)
	{
		this.code = code;
	}

	/**
	 * @return the twoSeaterQty
	 */
	public int[] getTwoSeaterQty()
	{
		return twoSeaterQty;
	}

	/**
	 * @param twoSeaterQty
	 *           the twoSeaterQty to set
	 */
	public void setTwoSeaterQty(final int[] twoSeaterQty)
	{
		this.twoSeaterQty = twoSeaterQty;
	}

	/**
	 * @return the fiveSeaterQty
	 */
	public int[] getFiveSeaterQty()
	{
		return fiveSeaterQty;
	}

	/**
	 * @param fiveSeaterQty
	 *           the fiveSeaterQty to set
	 */
	public void setFiveSeaterQty(final int[] fiveSeaterQty)
	{
		this.fiveSeaterQty = fiveSeaterQty;
	}

	/**
	 * @return the productNames
	 */
	public String[] getProductNames()
	{
		return productNames;
	}

	/**
	 * @param productNames
	 *           the productNames to set
	 */
	public void setProductNames(final String[] productNames)
	{
		this.productNames = productNames;
	}


}
