/**
 *
 */
package com.cat.bcp.storefront.forms.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.cat.bcp.storefront.forms.CatQuoteForm;


/**
 * Method for validating the quote form sent by front end
 *
 * @author sagdhingra
 *
 */
@Component("quoteValidator")
public class CatQuoteValidator implements Validator
{
	/**
	 * Method to identify the class which is to be validated customer
	 *
	 * @param aClass
	 *           class which is to be compared
	 */
	@Override
	public boolean supports(final Class<?> aClass)
	{
		return CatQuoteForm.class.equals(aClass);
	}

	/**
	 * Method to validate the quote form
	 *
	 * @param arg0
	 *           object which is to be validated
	 * @param arg1
	 *           The errors object which is to be populated
	 */
	@Override
	public void validate(final Object arg0, final Errors arg1)
	{
		final CatQuoteForm catQuoteForm = (CatQuoteForm) arg0;
		validateCustomerResponseDate(catQuoteForm, arg1);
	}

	/**
	 * Method to validate if customer response date is not null when status is approved by customer
	 *
	 * @param catQuoteForm
	 *           CatQuote Form which is to be validated
	 * @param errors
	 *           The errors object which is to be populated
	 */
	private void validateCustomerResponseDate(final CatQuoteForm catQuoteForm, final Errors errors)
	{
		if (("BUYER_ACCEPTED").equalsIgnoreCase(catQuoteForm.getQuoteState())
				&& StringUtils.isEmpty(catQuoteForm.getCustomerResponseDate()))
		{
			errors.rejectValue("customerResponseDate", "The customer response date cannot be blank");
		}
	}

}
