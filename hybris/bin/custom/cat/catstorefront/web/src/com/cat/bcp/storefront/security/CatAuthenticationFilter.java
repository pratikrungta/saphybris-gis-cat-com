/**
 *
 */
package com.cat.bcp.storefront.security;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.servicelayer.model.ModelService;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedCredentialsNotFoundException;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;


/**
 * @author rkolanupaka
 *
 */
public class CatAuthenticationFilter extends RequestHeaderAuthenticationFilter
{

	private final AuthenticationManager authenticationManager;



	public CatAuthenticationFilter(final ModelService modelService, final AuthenticationManager authenticationManager)
	{
		this.authenticationManager = authenticationManager;
	}


	@Override
	protected Object getPreAuthenticatedPrincipal(final HttpServletRequest request)
	{
		final String principal = JaloSession.getCurrentSession().getUser().getUid();

		if (principal == null)
		{
			throw new PreAuthenticatedCredentialsNotFoundException("Principle not found in JaloSession");
		}

		return principal;
	}

	/**
	 * @return the authenticationManager
	 */
	public AuthenticationManager getAuthenticationManager()
	{
		return authenticationManager;
	}

	/**
	 * @param authenticationManager
	 *           the authenticationManager to set
	 */
	@Override
	@Autowired
	public void setAuthenticationManager(final AuthenticationManager authenticationManager)
	{
		super.setAuthenticationManager(authenticationManager);
	}


}
