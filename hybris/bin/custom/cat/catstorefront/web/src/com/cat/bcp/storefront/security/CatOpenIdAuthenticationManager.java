/**
 *
 */
package com.cat.bcp.storefront.security;

import de.hybris.platform.b2b.model.B2BCustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.jalo.JaloItemNotFoundException;
import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.jalo.user.UserManager;
import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import com.cat.bcp.core.constants.CatCoreConstants;
import com.cat.bcp.storefront.util.CatOpenIdConnectUserDetails;


/**
 * The Class CatOpenIdAuthenticationManager which authenticates the user.
 *
 * @author prrungta
 */
public class CatOpenIdAuthenticationManager implements AuthenticationManager
{
	private static final Logger LOG = Logger.getLogger(CatOpenIdAuthenticationManager.class);

	@Resource(name = "userService")
	private UserService userService;

	@Override
	public Authentication authenticate(final Authentication authentication) throws AuthenticationException
	{

		final CatOpenIdConnectUserDetails userDetails = (CatOpenIdConnectUserDetails) authentication.getPrincipal();
		final String userId = userDetails.getUserId().toString().toLowerCase();

		try
		{
			final User user = UserManager.getInstance().getUserByLogin(userId);

			final UserModel userModel = userService.getUserForUID(user.getUid());
			if (!(userModel instanceof B2BCustomerModel))
			{
				throw new BadCredentialsException("User " + userModel.getUid() + " is not B2BCustomer");
			}

			final B2BCustomerModel b2BCustomerModel = (B2BCustomerModel) userModel;

			if (b2BCustomerModel.isLoginDisabled() || !b2BCustomerModel.getActive())
			{
				throw new DisabledException("User account " + userModel.getUid() + " is disabled/locked");
			}

			JaloSession.getCurrentSession().setUser(user);
			JaloSession.getCurrentSession().setAttribute(CatCoreConstants.OPENID_ACCESS_TOKEN, userDetails.getToken());
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Token Expiry Time is ::::\t" + userDetails.getToken().getExpiration());
			}
			return authentication;

		}
		catch (final JaloItemNotFoundException e)
		{
			throw new BadCredentialsException("User or password incorrect, user: " + userId, e);
		}
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}
}
