/**
 *
 */
package com.cat.bcp.storefront.security;

import de.hybris.platform.servicelayer.user.UserService;

import javax.annotation.Resource;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;


/**
 * @author rkolanupaka
 *
 */
public class CatPreAuthenticatedAuthenticationProvider extends PreAuthenticatedAuthenticationProvider
{

	@Resource(name = "userService")
	private UserService userService;

	@Override
	public Authentication authenticate(final Authentication authentication) throws AuthenticationException
	{
		return super.authenticate(authentication);
	}

	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}

	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}
}
