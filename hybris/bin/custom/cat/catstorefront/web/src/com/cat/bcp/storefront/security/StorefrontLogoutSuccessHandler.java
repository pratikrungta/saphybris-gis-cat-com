/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.cat.bcp.storefront.security;

import de.hybris.platform.acceleratorstorefrontcommons.security.GUIDCookieStrategy;
import de.hybris.platform.jalo.JaloSession;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;


public class StorefrontLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler
{
	Logger LOG = Logger.getLogger(StorefrontLogoutSuccessHandler.class);
	private GUIDCookieStrategy guidCookieStrategy;
	private List<String> restrictedPages;
	private String defaultTargetUrl;

	protected GUIDCookieStrategy getGuidCookieStrategy()
	{
		return guidCookieStrategy;
	}

	@Required
	public void setGuidCookieStrategy(final GUIDCookieStrategy guidCookieStrategy)
	{
		this.guidCookieStrategy = guidCookieStrategy;
	}

	protected List<String> getRestrictedPages()
	{
		return restrictedPages;
	}

	public void setRestrictedPages(final List<String> restrictedPages)
	{
		this.restrictedPages = restrictedPages;
	}

	/**
	 * @param defaultTargetUrl
	 *           the defaultTargetUrl to set
	 */

	@Override
	public void setDefaultTargetUrl(final String defaultTargetUrl)
	{
		this.defaultTargetUrl = defaultTargetUrl;
	}

	@Override
	public void onLogoutSuccess(final HttpServletRequest request, final HttpServletResponse response,
			final Authentication authentication) throws IOException, ServletException
	{
		LOG.info("onLogoutSuccess : entered");
		LOG.info("onLogoutSuccess : defaultTargetUrl = " + this.defaultTargetUrl);
		LOG.info("onLogoutSuccess : parent defaultTargetUrl = " + super.getDefaultTargetUrl());
		//check the jalo session and close the session. This is for explicit closing of hybris session
		if (JaloSession.getCurrentSession() != null)
		{
			JaloSession.getCurrentSession().close();
		}

		getGuidCookieStrategy().deleteCookie(request, response);

		//invalidate session
		if (request.getSession() != null)
		{
			request.getSession().invalidate();
		}

		// Delegate to default redirect behavior
		super.onLogoutSuccess(request, response, authentication);

		LOG.info("onLogoutSuccess : completed");

	}

	@Override
	protected String determineTargetUrl(final HttpServletRequest request, final HttpServletResponse response)
	{
		LOG.info("determineTargetUrl : entered");
		String targetUrl = this.defaultTargetUrl;

		for (final String restrictedPage : getRestrictedPages())
		{
			// When logging out from a restricted page, return user to homepage.
			if (targetUrl.contains(restrictedPage))
			{
				targetUrl = super.getDefaultTargetUrl();
				LOG.info("determineTargetUrl : targetUrl is part of restricted pages");
			}
		}
		LOG.info("determineTargetUrl : leaving with targetUrl = " + targetUrl);
		return targetUrl;
	}
}
