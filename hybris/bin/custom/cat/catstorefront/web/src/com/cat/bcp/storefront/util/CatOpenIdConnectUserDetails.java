/**
 *
 */
package com.cat.bcp.storefront.util;

import de.hybris.platform.util.Config;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

import com.cat.bcp.core.constants.CatCoreConstants;


/**
 * @author prrungta
 *
 */
public class CatOpenIdConnectUserDetails implements UserDetails
{

	private static final long serialVersionUID = 1L;

	private String userId;
	private String username;
	private OAuth2AccessToken token;

	/**
	 * Constructor.
	 *
	 * @param userInfo
	 *           Map<String, String>
	 * @param token
	 *           OAuth2AccessToken
	 */
	public CatOpenIdConnectUserDetails(final Map<String, String> userInfo, final OAuth2AccessToken token)
	{
		this.userId = userInfo.get(Config.getParameter(CatCoreConstants.OIDC_CUPID));
		this.username = userInfo.get(Config.getParameter(CatCoreConstants.OIDC_LOGIN_ID));
		this.token = token;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getUsername()
	{
		return username;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities()
	{
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));
	}

	/**
	 * getUserId()
	 */
	public String getUserId()
	{
		return userId;
	}

	/**
	 * @param userId
	 *           User ID string
	 */
	public void setUserId(final String userId)
	{
		this.userId = userId;
	}

	/**
	 * getToken()
	 */
	public OAuth2AccessToken getToken()
	{
		return token;
	}

	/**
	 * @param token
	 *           Oauth Token
	 */
	public void setToken(final OAuth2AccessToken token)
	{
		this.token = token;
	}

	/**
	 * @param username
	 *           User Name
	 */
	public void setUsername(final String username)
	{
		this.username = username;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getPassword()
	{
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isAccountNonExpired()
	{
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isAccountNonLocked()
	{
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isCredentialsNonExpired()
	{
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEnabled()
	{
		return true;
	}



}
