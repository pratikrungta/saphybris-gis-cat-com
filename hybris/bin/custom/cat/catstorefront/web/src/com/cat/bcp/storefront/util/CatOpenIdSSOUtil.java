/**
 *
 */
package com.cat.bcp.storefront.util;

import de.hybris.platform.jalo.JaloSession;

import org.springframework.security.oauth2.common.OAuth2AccessToken;

import com.cat.bcp.core.constants.CatCoreConstants;


/**
 * @author prrungta
 *
 */
public class CatOpenIdSSOUtil
{

	/**
	 * Requires authentication.
	 *
	 * @return true, if authentication is required.
	 */
	public boolean requiresAuthentication()
	{
		final String uid = JaloSession.getCurrentSession().getUser().getUid();
		final OAuth2AccessToken accessToken = getAccessToken();

		if ((accessToken != null && !accessToken.isExpired()) && (org.apache.commons.lang.StringUtils.isNotEmpty(uid)
				&& !org.apache.commons.lang.StringUtils.equalsIgnoreCase(uid, CatCoreConstants.ANONYMOUS_USER)))
		{
			return false;
		}


		return true;
	}

	/**
	 * Gets the access token.
	 *
	 * @return the access token
	 */
	public OAuth2AccessToken getAccessToken()
	{
		return (OAuth2AccessToken) JaloSession.getCurrentSession().getAttribute(CatCoreConstants.OPENID_ACCESS_TOKEN);
	}

}
