<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav"%>


<cms:pageSlot position="TopHeaderSlot" var="component" element="div" class="container">
	<cms:component component="${component}" />
</cms:pageSlot>



<header class="js-mainHeader">
	<nav class="navigation navigation--top hidden-xs">
		<div class="row header-row cat-container">
			<div class="col-xs-7 col-md-5 no-pad-left">
				<div class="visible-xs">
					<button class="btn js-toggle-sm-navigation" type="button">
							<span class="glyphicon glyphicon-align-justify"></span>
					</button>
				</div>
				<div class="nav__left js-site-logo">
					<cms:pageSlot position="SiteLogo" var="logo" limit="1">
						<cms:component component="${logo}" element="div" class="yComponentWrapper"/>
					</cms:pageSlot>
				</div>
				<!-- <div id="top-analytics" class="analytics">
					<cms:pageSlot position="AnalyticsLink" var="link" limit="1">
							<cms:component component="${link}" element="a" class="yComponentWrapper"/>
					</cms:pageSlot>
					 <a href=""> Analytics</a>	
				</div>
				<div id="top-gns-link" class="gns-link">
					<cms:pageSlot position="PortalLink" var="link" limit="1">
						<cms:component component="${link}" element="a"
							class="yComponentWrapper" />
					</cms:pageSlot>
					<a href=""> Analytics</a>
				</div> -->
				<div id="top-ci-retail" class="ci-retail-label">
					<spring:theme code="header.ci.retail.cockpit"/>
				</div>
				
			</div>
			<div class="col-xs-5 col-md-7 no-pad-right">
				<div class="nav__right">
					<!-- <div class="gns-link"> -->
					<%-- <cms:pageSlot position="MiniCart" var="cart" element="div" class="miniCartSlot componentContainer mobile__nav__row--table hidden-sm hidden-md hidden-lg">
								<cms:component component="${cart}" element="div" class="mobile__nav__row--table-cell" />
							</cms:pageSlot> --%>
							
					<!-- </div> -->
					
					
					<ul class="nav__links pull-right">
						<c:if test="${empty hideHeaderLinks}">
							<c:if test="${uiExperienceOverride}">
								<li class="backToMobileLink"><c:url
										value="/_s/ui-experience?level=" var="backToMobileStoreUrl" />
									<a href="${backToMobileStoreUrl}"> <spring:theme
											code="text.backToMobileStore" />
									</a>
								</li>
							</c:if>

							<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
								<c:set var="maxNumberChars" value="25" />
								<c:if test="${fn:length(user.firstName) gt maxNumberChars}">
									<c:set target="${user}" property="firstName"
										value="${fn:substring(user.firstName, 0, maxNumberChars)}..." />
								</c:if>
								<c:if test="${isUTVUser eq true || isIM  eq true}">
									<li class="order-history">
										<a href="/my-account/cat/orders" class="order-history"><spring:theme code="header.orderhistory.text"/></a>
									</li>									
								</c:if>
								<li class="logged_in js-logged_in" id="profile_drop_down_icon">
									<ycommerce:testId code="header_LoggedUser">
								<c:set var ="username" value="${user.firstName}'s" /> 				
								<c:set var="userN" value="${fn:toLowerCase(username)}" />	
								<div class="mobile-ci-label visible-xs"><spring:theme code="header.ci.retail.cockpit"/></div>
								<div class="userAccountName hide_overflow" title="${userN}">${userN}</div>
										 
									<div class="NamenIcon">
									<img class="userProfile-icon" src="/_ui/responsive/cat/images/fill-5@3x.png"><spring:theme code="header.account.text"/>		
										
										 
										 <i id="profile_drop_down_icon" class="fa fa-angle-down"></i>
   								 <span id="myDropdown" class="dropdown-content">
       								 <a href="/my-account/profile"><spring:theme code="header.myprofile.text"/></a>
    								  <c:if test="${isUTVUser eq true || isIM  eq true}">
										<a href="/my-account/getCountyMapping" class="county-mapping order-history"><spring:theme code="cat.county.mapping.heading.text"/></a>
    								 </c:if>
    								 <c:if test="${isSP eq true}">
    								    <a href="/my-account/my-quotes/view" class="quote-history"><spring:theme code="header.quotehistory.text"/></a>
    								 </c:if>
    								  <a href="/logout" class="desktop-logout"><spring:theme code="header.signout.text"/></a>
    							</span></div>
					<div class="edit-profile-mobile hide-important"> 
						<div class="user-contact">${fn:escapeXml(user.userRole)}</div>
						<a href="/my-account/profile"><spring:theme code="profile.edit"/> <i class="fa fa-pencil" aria-hidden="true"></i></a>
					</div>
 
										 
									</ycommerce:testId>
								</li>
								<span style="display:none" id="GADealerName"><%=session.getAttribute("currentDealer")%></span>
								
							</sec:authorize>

							 <cms:pageSlot position="HeaderLinks" var="link">
								 <cms:component component="${link}" element="li" />
							 </cms:pageSlot>

							<%-- <sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')" >
								<li class="liOffcanvas">
									<ycommerce:testId code="header_Login_link">
										<a href="<c:url value='/login'/>">
											<spring:theme code="header.link.login" />
										</a>
									</ycommerce:testId>
								</li>
							</sec:authorize> 

							<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')" >
								<li class="liOffcanvas">
									<ycommerce:testId code="header_signOut">
									<img class="lock-icon" src="/_ui/responsive/cat/images/lock-icon@3x.png">
										<a href="/logout">
											<spring:theme code="header.link.logout" />
										</a>
									</ycommerce:testId>
								</li>
							</sec:authorize> --%>

						</c:if>
					</ul>
					<cms:pageSlot position="MiniCart" var="cart" limit="1">
							<cms:component component="${cart}" element="a"
								class="yComponentWrapper" />
						</cms:pageSlot>
						
				</div>
			</div>
		</div>
	</nav>
	<%-- a hook for the my account links in desktop/wide desktop--%>
	<div class="hidden-xs js-secondaryNavAccount collapse" id="accNavComponentDesktopOne">
		<ul class="nav__links">

		</ul>
	</div>
	<div class="hidden-xs js-secondaryNavCompany collapse" id="accNavComponentDesktopTwo">
		<ul class="nav__links js-nav__links">

		</ul>
	</div>
<!-- 	<nav class="navigation navigation--middle js-navigation--middle">
		<div class="container-fluid">
			<div class="row">
				<div class="mobile__nav__row mobile__nav__row--table">
					<div class="mobile__nav__row--table-group">
						<div class="mobile__nav__row--table-row">
							<div class="mobile__nav__row--table-cell visible-xs hidden-sm">
								<button class="mobile__nav__row--btn btn mobile__nav__row--btn-menu js-toggle-sm-navigation"
										type="button">
									<span class="glyphicon glyphicon-align-justify"></span>
								</button>
							</div>

							<div class="mobile__nav__row--table-cell visible-xs mobile__nav__row--seperator">
								<ycommerce:testId code="header_search_activation_button">
									<button	class="mobile__nav__row--btn btn mobile__nav__row--btn-search js-toggle-xs-search hidden-sm hidden-md hidden-lg" type="button">
										<span class="glyphicon glyphicon-search"></span>
									</button>
								</ycommerce:testId>
							</div>

							<c:if test="${empty hideHeaderLinks}">
								<ycommerce:testId code="header_StoreFinder_link">
									<div class="mobile__nav__row--table-cell hidden-sm hidden-md hidden-lg mobile__nav__row--seperator">
										<a href="<c:url value="/store-finder"/>" class="mobile__nav__row--btn mobile__nav__row--btn-location btn">
											<span class="glyphicon glyphicon-map-marker"></span>
										</a>
									</div>
								</ycommerce:testId>
							</c:if>

							<cms:pageSlot position="MiniCart" var="cart" element="div" class="miniCartSlot componentContainer mobile__nav__row--table hidden-sm hidden-md hidden-lg">
								<cms:component component="${cart}" element="div" class="mobile__nav__row--table-cell" />
							</cms:pageSlot>

						</div>
					</div>
				</div>
			</div>
			
		</div>
	</nav> -->
	<a id="skiptonavigation"></a>
<!-- 	<sec:authorize access="hasAnyRole('ROLE_ANONYMOUS')">
		<div class="yellow-strip"></div>
	</sec:authorize> -->
	<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
	<nav:topNavigation />
	</sec:authorize>
	<sec:authorize access="!hasAnyRole('ROLE_ANONYMOUS')">
	<!-- <center><h3><spring:theme code="cat.search.portal.text"/></h3></center> -->
			<div class="row desktop__nav search-center">
				<div class="nav__left col-xs-12 col-sm-6">
					<div class="row">
						<div class="col-sm-12">
							<div class="site-search">
								<cms:pageSlot position="SearchBox" var="component">
									<cms:component component="${component}" element="div"/>
								</cms:pageSlot>
							</div>
						</div>
					</div>
				</div>
				<%-- <div class="nav__right col-xs-6 col-xs-6 hidden-xs">
					<ul class="nav__links nav__links--shop_info">
						<%-- <li>
							<c:if test="${empty hideHeaderLinks}">
								<ycommerce:testId code="header_StoreFinder_link">
									<div class="nav-location hidden-xs">
										<a href="<c:url value="/store-finder"/>" class="btn">
											<span class="glyphicon glyphicon-map-marker"></span>
										</a>
									</div>
								</ycommerce:testId>
							</c:if>

						</li>
 
						<li>
						
							<%-- <cms:pageSlot position="MiniCart" var="cart" element="div" class="componentContainer">

								<cms:component component="${cart}" element="div"/>

							</cms:pageSlot> 
						</li>
					</ul>
				</div> --%>
			</div>
			</sec:authorize>
</header>


<cms:pageSlot position="BottomHeaderSlot" var="component" element="div"	class="container-fluid">
	<cms:component component="${component}" />
</cms:pageSlot>
