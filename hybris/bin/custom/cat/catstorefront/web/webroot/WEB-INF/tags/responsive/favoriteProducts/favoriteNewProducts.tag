<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


	<div class="products" ng-show="fpc.view.hasFavoriteNewProducts" ng-init="fpc.hasNewFavoriteProducts('${fn:length(newFavoriteList) gt 0 }')" ng-cloak>
		<div class="row cat-row">
			<div class="col-xs-6 col-sm-4 card-wrap" ng-repeat="onFly in fpc.view.favoriteNewProductsAddedOnFly track by $index">
				<div class="single-card col-sm-3 col-xs-6">
					<cat-card image="{{onFly.image}}"
								primary-header="{{onFly.primaryHeader}}"
								pdp-link="{{onFly.pdpLink}}"
								book-marked="{{onFly.bookMarked}}"
								product-id-for-communication="{{onFly.productIdForCommunication}}"
								relation="slave"
								dis-continued="{{onFly.disContinued}}"
								>
					</cat-card>
				</div>
			</div>
			<div class="fav-invisible">
				<c:forEach items="${newFavoriteList}" var="product">
					<div class="col-xs-6 col-sm-4">
						<c:set value="${product.images[0]}" var="imageUrl"/>
						<c:if test="${not product.isReorderable}">
							<div class="single-card" ng-init="fpc.addNewFavorite(
								{
									primaryHeader:'${product.name}',
									pdpLink:'${product.url}',
									productIdForCommunication:'${product.code}',
									image:'${imageUrl.url}',
									disContinued: 'true'
								}
							)">
								<cat-card image="${imageUrl.url}"
									primary-header="${product.name}" 
									pdp-link="${product.url}"
									book-marked="true"
									product-id-for-communication="${product.code}"
									dis-continued="true"
									>
								</cat-card>
							</div>
						</c:if>
								
						<c:if test="${product.isReorderable}">
							<div class="single-card" ng-init="fpc.addNewFavorite(
								{
									primaryHeader:'${product.name}',
									pdpLink:'${product.url}',
									productIdForCommunication:'${product.code}',
									image:'${imageUrl.url}',
									disContinued: 'false'
								}
							)">
								<cat-card 
									image="${imageUrl.url}"
									primary-header="${product.name}" 
									pdp-link="${product.url}"
									book-marked="true"
									product-id-for-communication="${product.code}"
									dis-continued="false"
									relation="slave"
								>
								</cat-card>
							</div>
						</c:if>
				
					</div>
				</c:forEach>
			</div>
				<!-- end of loop -->
		</div>
	</div>
	<div class="empty" ng-show="!fpc.view.hasFavoriteNewProducts"><spring:theme code='cat.no.new.favorite.product.msg' /></div>

