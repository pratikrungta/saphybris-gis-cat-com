<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="favoriteProductsComponents" tagdir="/WEB-INF/tags/responsive/favoriteProducts" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


        <div class="favorite-products cat-container row" ng-controller="favoriteProductsController as fpc">
            <div class="header upperCase col-md-12">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="text favorite-product-label">
                            <spring:theme code="cat.homepage.favorite.text"/>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div class="toggle-btn-group">
                            <div class="btn-group">
                                <label class="btn btn-primary" ng-model="fpc.view.showFavoriteProductsOf" uib-btn-radio="'new'" uib-uncheckable="uncheckable"> <spring:theme code="cat.new"/></label>
                                <label class="btn btn-primary" ng-model="fpc.view.showFavoriteProductsOf" uib-btn-radio="'used'" uib-uncheckable="uncheckable"> <spring:theme code="cat.used"/></label>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <div class="used-products col-md-12" ng-show="fpc.view.usedProducts">
            <favoriteProductsComponents:favoriteUsedProducts />
        </div>

        <div class="new-product col-md-12" ng-show="fpc.view.newProducts">
            <favoriteProductsComponents:favoriteNewProducts />
        </div>

        </div>