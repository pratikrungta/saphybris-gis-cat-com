<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


	<div class="products" ng-show="fpc.view.hasFavoriteUsedProducts" ng-init="fpc.hasUsedFavoriteProducts('${fn:length(usedFavoriteList) gt 0 }')" ng-cloak>
		<div class="row cat-row">
			<div class="col-xs-6 col-sm-4 card-wrap" ng-repeat="onFly in fpc.view.favoriteUsedProductsAddedOnFly track by $index">
				<div class="single-card col-sm-3 col-xs-6">
					<cat-card image="{{onFly.image}}"
								primary-header="{{onFly.primaryHeader}}"
                pdp-link="{{onFly.pdpLink}}"
								book-marked="{{onFly.bookMarked}}" 
								product-type="used"
								product-id-for-communication="{{onFly.productIdForCommunication}}"
								relation="slave">
          </cat-card>
				</div>
			</div>

		<div class="fav-invisible">
			<c:forEach items="${usedFavoriteList}" var="product">
					<div class="col-xs-6 col-sm-4">
					<c:set value="${product.images[0]}" var="imageUrl"/>
						<div class="single-card" ng-init="fpc.addUsedFavorite(
							{primaryHeader:'${product.serialNumber}',
							pdpLink:'${product.url}',
							productIdForCommunication:'${product.code}',
							image:'${imageUrl.url}'		
						}
						)">
							<!-- <cat-card image="${imageUrl.url}"
								primary-header="${product.serialNumber}"
                pdp-link="${product.url}"
								book-marked="true" 
								product-type="used"
								product-id-for-communication="${product.code}">
                </cat-card> -->
						</div>
					</div>
			</c:forEach>
			<!-- end of loop -->
		</div>	
		</div>
	</div>
	<div class="empty" ng-show="!fpc.view.hasFavoriteUsedProducts" ng-cloak><spring:theme code='cat.no.used.favorite.product.msg' /></div>
