<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="col-xs-12 col-md-12 main-container no-padding" ng-controller="fbcMustSelectController as fmc" ng-init="fmc.populateFBCData()" ng-cloak>
<div class="col-xs-12 col-md-12 breadcrumb-section no-padding">
<ol class="breadcrumb">
    <li>Home</li>
    <li>{{fmc.fromPage}}</li>
    <li ng-class="{'active':!fmc.showReviewParts}">Add Parts</li>
    <li class="active" ng-if="fmc.showReviewParts">Review Added Items</li>
</ol>
</div>
<div ng-show="!fmc.isDataLoaded && !fmc.fbcError" class="cat-loading-spinner loadingSymbol"></div>
<div class="col-xs-12 col-md-12 fbc-error-container no-padding" ng-if="fmc.fbcError">
    <div class="col-xs-12 col-md-12 error-msg-container">
        <label class="oops-msg"><spring:theme code="cat.error.page.msg" /></label>
        <div class="cat-logo"></div>
        <label class="try-again-msg"><spring:theme code="cat.error.page.pageNotFound" /><a ng-click="fmc.navigateToHome();"><spring:theme code="cat.error.page.homePage" /></a>{{fmc.fbcErrorText}}</label>
    </div>
</div>
<div class="col-xs-12 col-md-12 products-container" ng-if="fmc.isDataLoaded && !fmc.showReviewParts && !fmc.fbcError">
    <div class="col-xs-12 col-md-12 header-row clearfix">
       <div class="product-image pull-left">
         <img ng-src="{{fmc.imageURL || fmc.defaultDownloadImage}}"/>
       </div>
       <div class="product-details pull-left">
            <div class="product-header">
             {{fmc.mustSelectHeading}} 
             <span class="product-model">{{fmc.salesModel}} {{fmc.productFamily}}</span>
            </div>
       </div>
    </div>
    <div class="col-xs-12 col-md-12 product-type-row clearfix">
        <div class="pull-left product-type-name">
           <label>{{fmc.fbcFeaturesList.selectionCriteria}} - {{fmc.fbcFeaturesList.description}}</label>
        </div>
        <div class="pull-right next-btn-container hidden-xs">
             <button class="btn update-product-list next-btn pull-right" ng-class="{'disabled' : fmc.userSelection.disableNextBtn}" ng-disabled="fmc.userSelection.disableNextBtn" ng-click="fmc.loadFreshFBCProducts();">Next</button> 
        </div>
    </div>
    <div class="col-xs-12 col-md-12 product-card-section">
        <div class="row product-card-row" ng-repeat="productRow in fmc.chunkedProductCardsList">
            <div class="col-xs-12 col-md-3 product-card-col" ng-repeat="cardData in productRow">
                <fbc-product-card data="{{cardData}}"></fbc-product-card>
            </div>
        </div>
    </div>
</div>
<div class="col-xs-12 col-md-12 next-button-bottom-row clearfix" ng-if="fmc.isDataLoaded && !fmc.showReviewParts && !fmc.fbcError">
    <button class="btn update-product-list next-btn pull-right" ng-class="{'disabled' : fmc.userSelection.disableNextBtn}" ng-disabled="fmc.userSelection.disableNextBtn" ng-click="fmc.loadFreshFBCProducts();">Next</button> 
</div>
<div class="col-xs-12 col-md-12 review-parts-container no-padding" ng-if="fmc.showReviewParts && !fmc.fbcError">
        <div class="col-xs-12 col-md-12 review-parts-header">
        <label>{{fmc.reviewPartsHeading}}</label>
        </div>
        <div class="col-xs-12 col-md-12 review-parts-table-container no-padding hidden-xs">
            <table class="col-xs-12 col-md-12 review-parts-table">
                <thead>
                    <tr>
                        <th>{{fmc.reviewPartsFeatureCode}}</th>
                        <th>{{fmc.reviewPartsReferenceNo}}</th>
                        <th class="text-right quantity">{{fmc.reviewPartsQuantity}}</th>
                        <th>{{fmc.reviewPartsPartDesc}}</th>
                        <th class="fixed-width-col text-center">{{fmc.reviewPartsAI}}</th>
                        <th class="fixed-width-col text-center">{{fmc.reviewPartsLN}}</th>
                        <th class="text-right price-each">{{fmc.reviewPartsPriceEach}}</th>
                        <th class="text-right total-price">{{fmc.reviewPartsTotalPrice}}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="row in fmc.selectionsList">
                        <td>{{row.featureCode}}</td>
                        <td>{{row.referenceNumber}}</td>
                        <td class="text-right quantity">{{fmc.selectedQty}}</td>
                        <td>{{row.description}}</td>
                        <td class="fixed-width-col text-center">{{row.assemblyIndicator}}</td>
                        <td class="fixed-width-col text-center">L</td>
                        <td class="text-right price-each">{{row.listPrice  | currency}}</td>
                        <td class="text-right total-price">{{(row.listPrice * fmc.selectedQty)  | currency}}</td> 
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-xs-12 col-md-12 review-parts-table-container visible-xs">
            <div class="col-xs-12 col-md-12 review-parts-mobile-card" ng-repeat="row in fmc.selectionsList">
                <div class="col-xs-12 col-md-12 row-1">
                    <div class="col-xs-12 mobile-card-row no-padding">
                        <label class="value part-desc">{{row.description}}</label>
                    </div>
                    <div class="col-xs-6 mobile-card-row no-padding">
                        <label class="header">{{fmc.reviewPartsFeatureCode}}</label>
                        <label class="value">{{row.featureCode}}</label>
                    </div>
                    <div class="col-xs-6 mobile-card-row no-padding">
                        <label class="header">{{fmc.reviewPartsReferenceNo}}</label>
                        <label class="value">{{row.referenceNumber}}</label>
                    </div>
                </div>
                <div class="col-xs-12 col-md-12 row-2">
                    <div class="col-xs-6 mobile-card-row no-padding">
                        <label class="header">{{fmc.reviewPartsAI}}</label>
                        <label class="value">{{row.assemblyIndicator}}</label>
                    </div>
                    <div class="col-xs-6 mobile-card-row no-padding">
                        <label class="header">{{fmc.reviewPartsLN}}</label>
                        <label class="value">L</label>
                    </div>
                    <div class="col-xs-6 mobile-card-row no-padding">
                        <label class="header">{{fmc.reviewPartsQuantity}}</label>
                        <label class="value">{{fmc.selectedQty}}</label>
                    </div>
                    <div class="col-xs-6 mobile-card-row no-padding">
                        <label class="header">{{fmc.reviewPartsPriceEach}}</label>
                        <label class="value">{{row.listPrice  | currency}}</label>
                    </div>
                </div>
                <div class="col-xs-12 col-md-12 row-3">
                    <div class="col-xs-6 list-price-header no-padding">
                        <label>{{fmc.reviewPartsTotalPrice}}</label>
                    </div>
                    <div class="col-xs-6 list-price-value no-padding">
                        <label>{{(row.listPrice * fmc.selectedQty)  | currency}}</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-12 total-parts-row hidden-xs">
            <div class="col-xs-12 col-md-10 header-col no-padding clearfix">
                <label class="pull-right header">{{fmc.reviewPartsTotalParts}}</label>
            </div>
            <div class="col-xs-12 col-md-2 value-col no-padding clearfix">
                <label class="pull-right value">{{fmc.totalParts * fmc.selectedQty}} Units</label>
            </div>
        </div>
        <div class="col-xs-12 col-md-12 total-price-row hidden-xs">
            <div class="col-xs-12 col-md-10 header-col no-padding clearfix">
                <label class="pull-right header">{{fmc.reviewPartsTotalListPrice}}</label>
            </div>
            <div class="col-xs-12 col-md-2 value-col no-padding clearfix">
                <label class="pull-right value">{{(fmc.totalListPrice * fmc.selectedQty) | currency}}</label>
            </div>
        </div>
        <div class="col-xs-12 col-md-12 total-row visible-xs">
        <div class="col-xs-12 col-md-12 parts-row clearfix">
            <label class="pull-left header">{{fmc.reviewPartsTotalParts}}</label>
            <label class="pull-right value">{{fmc.totalParts}}</label>
        </div>
        <div class="col-xs-12 col-md-12 price-row clearfix">
            <label class="pull-left header">{{fmc.reviewPartsTotalListPrice}}</label>
            <label class="pull-right value">{{(fmc.totalListPrice * fmc.selectedQty) | currency}}</label>
        </div>
        </div>
        <div class="col-xs-12 col-md-12 add-to-cart-btn-row">
            <button class="btn update-product-list add-to-cart-btn pull-right" ng-disabled="fmc.addToCartDisabled" ng-click="fmc.redirectToCartPage();">{{fmc.reviewPartsAddToCart}}</button>
        </div>
    </div>
</div>
