<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="row cat-container cat-typeahead">
	<cat-search plp-link="${request.contextPath}/search/view/" id="landingPageAutoComplete" placeholder-new-products="<spring:theme code='cat.search.new.watermark.text'/>" placeholder-used-products="<spring:theme code='cat.search.used.watermark.text'/>" placeholder-for-mobile="Enter Product Family" search-for="newProducts" default-value="${defaultSearchPage}"></cat-search>
</div>
<!-- type a head dropdown template  required for FE team-->

<script type="text/ng-template" id="typeahead-item.html">
	<div class="typeahead-group-header" ng-if="match.model.firstInGroup" ng-class="(!!match.model.noResultsFound)?'no-results-found':''">{{match.model.group}}</div>
	<a class="result" ng-if="!match.model.noResultsFound">
	<span ng-bind-html="match.label | uibTypeaheadHighlight:query"></span>
	</a>
	<div class="typeahead-group-footer" ng-if="match.model.lastObject"></div>
</script>


