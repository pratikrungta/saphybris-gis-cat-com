<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
        <div class="favorite-products cat-container">
            <div class="header upperCase">
                <dic class="row">
                    <div class="col-xs-6">
                        <div>
                            <spring:theme code="cat.homepage.favorite.text"/>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="pull-right toggle-btn-group">
                            <div class="btn-group">
                                <label class="btn btn-primary" ng-model="checkModel.left" uib-btn-checkbox><spring:theme code="cat.new"/></label>
                                <label class="btn btn-primary" ng-model="checkModel.middle" uib-btn-checkbox><spring:theme code="cat.used"/></label>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <c:if test="${fn:length(favoriteList) gt 0  }">
            <div class="products cat-container">
                <div class="row">
                    <c:forEach items="${favoriteList}" var="product">
                        <div class="col-xs-6 col-sm-4">
                            <div class="single-card">
                                <cat-card image="${product.primaryImageUrl}" primary-header=${product.name} pdp-link="${product.url}" book-marked="true" product-id-for-communication="${product.code}"></cat-card>
                            </div>
                        </div>
                    </c:forEach>
                    <!-- end of loop -->
                </div>
            </div>
        </c:if>
        <c:if test="${fn:length(favoriteList) eq 0  }">
            <div class="empty">
            	<spring:theme code="product.favorite.icon"/>
            </div>
        </c:if>
        </div>
