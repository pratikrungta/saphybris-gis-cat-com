<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="row low-stock no-margin">
    <div class="low-stock-content cat-container">
        <div class="reorder-text-section">
            <div>
                <spring:theme code="cat.inventory.alert.text" arguments="${user.firstName}"/>
            </div>
            <div>
                <spring:theme code="cat.inventory.alert.message" arguments="${lowStockCount}"/>
            </div>
        </div>
        
        
        <div class="reorder-days-message">
      
            <c:choose>
                        <c:when test="${orderingWindowMessage.lane2.orderWindowStatus eq 'open' and orderingWindowMessage.lane2.leftDays ge 1}">
                        <spring:theme code="cat.orderingWindowMessage.lane2.closes.in" arguments="${orderingWindowMessage.lane2.leftDays}"/>
						</c:when>                        
                        
                        <c:when test="${orderingWindowMessage.lane2.orderWindowStatus eq 'open' and orderingWindowMessage.lane2.leftDays eq 0}">
						<spring:theme code="cat.orderingWindowMessage.lane2.closes.today"/>
                        </c:when>

                        <c:when test="${orderingWindowMessage.lane3.orderWindowStatus eq 'open' and orderingWindowMessage.lane3.leftDays ge 1}">
                        <spring:theme code="cat.orderingWindowMessage.lane3.closes.in" arguments="${orderingWindowMessage.lane3.leftDays}"/>
                        </c:when>
                        
                        <c:when test="${orderingWindowMessage.lane3.orderWindowStatus eq 'open' and orderingWindowMessage.lane3.leftDays eq 0}">
                        <spring:theme code="cat.orderingWindowMessage.lane3.closes.today"/>
                        </c:when>
                        
                        <c:when test="${orderingWindowMessage.lane2.orderWindowStatus eq 'closed' and orderingWindowMessage.lane3.orderWindowStatus eq 'closed' and orderingWindowMessage.lane3.leftDays lt orderingWindowMessage.lane2.leftDays}">
						<spring:theme code="cat.orderingWindowMessage.lane3.opens.in" arguments="${orderingWindowMessage.lane3.leftDays}"/>
                        </c:when>
                        
                        <c:when test="${orderingWindowMessage.lane2.orderWindowStatus eq 'closed' and orderingWindowMessage.lane3.orderWindowStatus eq 'closed' and orderingWindowMessage.lane3.leftDays gt orderingWindowMessage.lane2.leftDays}">
                        <spring:theme code="cat.orderingWindowMessage.lane2.opens.in" arguments="${orderingWindowMessage.lane2.leftDays}"/>
                        </c:when>
                        
                        </c:choose>
        </div>
        <div class="reorder-condition-message">
            <spring:theme code="cat.orderingWindowMessage.refresh"/>
        </div>
        <div>
        	<a href="/reorder/viewManageOrders" target="_self"><button class="btn cat-buttons primary-btn manage-order"><spring:theme code="cat.inventory.alert.manage.reorder.products"/></button></a>
        </div>
    </div>
</div>