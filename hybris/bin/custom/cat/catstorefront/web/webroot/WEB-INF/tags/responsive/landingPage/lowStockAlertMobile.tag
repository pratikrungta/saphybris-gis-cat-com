<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


    <section class="low-stock-order-container fade-move-up" onclick="event.stopPropagation()">
        <div class="arrow"></div>
        <h4 class="text-left">
                <img class="warning" src="/_ui/responsive/cat/images/warning_dark_red.svg" /> 
                <span class="alert-text"><spring:theme code="cat.inventory.alert.text"/></span>         
        </h4>
        <p class="warning-message text-left">
                <spring:theme code="cat.inventory.alert.message" arguments="${lowStockCount}"/>
        </p>
        
                <a href="/reorder/viewManageOrders" target="_self" 
                class="manage-m-order text-center">
                <spring:theme code="cat.inventory.alert.manage.orders"/></a>
            
    </section>
