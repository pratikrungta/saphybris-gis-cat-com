<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="row">
	<div class="col-xs-12">
	<div class="row">
		<div class="col-xs-6">
			<div class="header upperCase">
				
				<span><spring:theme code="cat.used.pdp.title.text" /></span>

				<c:if test="${not empty DEALER_CERTIFIED and DEALER_CERTIFIED eq true }">
				<span class="usedPDPImage"></span>
				</c:if>
				
			</div>

		</div>
		<div class="col-xs-6">
			<div class="price">
				<c:if test="${not empty PRICE and PRICE ne '0'}">
					<span class="pull-right">&#36;${PRICE}</span>
				</c:if>
			</div>
		</div>
	</div>
	<div class="row dealer-section">
		<div class="col-xs-6">
			<div class="dealer-name">
				<spring:theme code="cat.used.pdp.dealer.text" />
			</div>
		</div>
		<div class="col-xs-6">
			<div class="price-info">
				<c:if test="${not empty PRICE and PRICE ne '0'}">
					<div class="pull-right"><spring:theme code="cat.used.pdp.sellingprice" /></div>
					<div class="pull-right"><spring:theme code="cat.used.pdp.additionalCharges" /></div>
				</c:if>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 machine-details-text upperCase">
			<spring:theme code="cat.used.pdp.machine.details.text" />
		</div>
	</div>
	<div class="row visible-xs">
		<div class="col-xs-6">
			<div class="product-into-text upperCase"><spring:theme code="cat.used.pdp.branch.location.text" /></div>
		</div>
		<div class="col-xs-6">
			<c:choose>
				<c:when test="${not empty BRANCH_LOCATION}">
					<div class="product-into-value">${BRANCH_LOCATION}</div>
				</c:when>
				<c:otherwise>
					<div class="product-into-value"><spring:theme code="cat.used.pdp.hyphen" /></div>
				</c:otherwise>
			</c:choose>
		</div>
		<div class="col-xs-6">
			<div class="product-into-text upperCase"><spring:theme code="cat.used.pdp.hours.used.text" /></div>
		</div>
		<div class="col-xs-6">
			<c:choose>
				<c:when test="${not empty HOURS_USED}">
					<div class="product-into-value pad-left-10">${HOURS_USED}</div>
				</c:when>
				<c:otherwise>
					<div class="product-into-value pad-left-10"><spring:theme code="cat.used.pdp.hyphen" /></div>
				</c:otherwise>
			</c:choose>
		</div>
		<div class="col-xs-6">
			<div class="product-into-text upperCase"><spring:theme code="cat.used.pdp.manufacturing.year.text" /></div>
		</div>
		<div class="col-xs-6">
			<c:choose>
				<c:when test="${not empty MANUFACTURING_YEAR}">
					<div class="product-into-value pdtValuePDP">${MANUFACTURING_YEAR}</div>
				</c:when>
				<c:otherwise>
					<div class="product-into-value pdtValuePDP"><spring:theme code="cat.used.pdp.hyphen" /></div>
				</c:otherwise>
			</c:choose>
		</div>
		
	</div>
	<div class="row pad-left-10 hidden-xs">
		<div class="col-xs-4 no-padding">
			<div class="product-into-text upperCase"><spring:theme code="cat.used.pdp.branch.location.text" /></div>
		</div>
		<div class="col-xs-3">
			<div class="product-into-text upperCase"><spring:theme code="cat.used.pdp.hours.used.text" /></div>
		</div>
		<div class="col-xs-5 manufacturingYearPDPTxt">
			<div class="product-into-text upperCase"><spring:theme code="cat.used.pdp.manufacturing.year.text" /></div>
		</div>
	</div>

	<div class="row pad-left-10 hidden-xs">
		<div class="col-xs-4">
			<c:choose>
				<c:when test="${not empty BRANCH_LOCATION}">
					<div class="product-into-value">${BRANCH_LOCATION}</div>
				</c:when>
				<c:otherwise>
					<div class="product-into-value"><spring:theme code="cat.used.pdp.hyphen" /></div>
				</c:otherwise>
			</c:choose>
		</div>
		<div class="col-xs-3">
			<c:choose>
				<c:when test="${not empty HOURS_USED}">
					<div class="product-into-value pad-left-10">${HOURS_USED}</div>
				</c:when>
				<c:otherwise>
					<div class="product-into-value pad-left-10"><spring:theme code="cat.used.pdp.hyphen" /></div>
				</c:otherwise>
			</c:choose>
		</div>
		<div class="col-xs-5">
			<c:choose>
				<c:when test="${not empty MANUFACTURING_YEAR}">
					<div class="product-into-value pdtValuePDP">${MANUFACTURING_YEAR}</div>
				</c:when>
				<c:otherwise>
					<div class="product-into-value pdtValuePDP"><spring:theme code="cat.used.pdp.hyphen" /></div>
				</c:otherwise>
			</c:choose>
		</div>
	</div>

	</div>
</div>
