<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%> 

<div class="utv-order-container" ng-controller="utvPDPPlaceOrderController as upo" ng-cloak>

    <script type="text/ng-template" id="utvInformationMessage.html">
	   <cms:component uid="orderWindowInfoParagraphComponent"/>
    </script>

	<script type="text/ng-template" id="myModalContent.html">
    <div class="utv-more-information modal-body" align-vertical-center-modal-dialog="full-screen-xs">
        <div class="close-icon">
            <img src="/_ui/responsive/cat/images/close-icon.png" class=" pull-right" aria-hidden="true" ng-click="upo.cancel()">
        </div>
    <cms:component uid="orderQuantityInfoParagraphComponent"/>
    </div>
    </script>

    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="header upperCase">
                <spring:theme code="utv.order.placeAnOrder"/> <img class="info-icon" src="/_ui/responsive/cat/images/info-icon.svg" uib-popover-template="upo.view.informatonMesage" popover-placement="bottom-center" popover-trigger="'mouseenter'">
            </div>
        </div>
    </div>

	  
    <c:if test="${fn:length(orderWindows) gt 0}">

        <div class="row">
            <div class="col-md-12 col-xs-12">
                    <div class="message"><cms:component uid="shipAfterDatesParagraphComponent"/></div>
            </div>
        </div>

    <div class="row place-order">
        <div class="col-md-12 col-xs-12">
            <div class="row place-order-header">
                <div class="col-md-4 col-xs-4 month">
                    <spring:theme code="utv.order.month"/>
                </div>
                <div class="col-md-4 col-xs-4 units">
                    <spring:theme code="utv.order.units"/>
                </div>
                <div class="col-md-4 col-xs-4 date">
                    <spring:theme code="utv.order.shipAfterDate"/>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-xs-12">
        <c:set var="monthName" value=""/>
            <c:forEach var="shipAfterDate" items="${orderWindows.shipToAfterDates}" varStatus="counter">
            <div class="row reorder-months" ng-class="{'order-line':${monthName ne shipAfterDate.shipAfterName }}">
                <div class="col-md-4 col-xs-4">
                    <div>
                    <c:if test="${monthName ne shipAfterDate.shipAfterName }">
                    <b class="months">${fn:toUpperCase(shipAfterDate.shipAfterName)}</b>
                    </c:if>
                    <c:set var="monthName" value="${shipAfterDate.shipAfterName }"/>
                    </div>

                    <c:set var="day" value="${shipAfterDate.shipAfterDate.date}"/>
                    <c:if test="${shipAfterDate.shipAfterDate.date < 10}">
                   		<c:set var="day" value="0${shipAfterDate.shipAfterDate.date}"/>
                   	</c:if> 
                  
                    <c:if test="${shipAfterDate.shipAfterDate.month lt 10}">
                    <c:set var="month" value="0${shipAfterDate.shipAfterDate.month+1}"/>
                    </c:if>
                    <c:if test="${shipAfterDate.shipAfterDate.month ge 10}">
                    <c:set var="month" value="${shipAfterDate.shipAfterDate.month+1}"/>
                    </c:if>
 		            <c:set var="year" value="${1900+shipAfterDate.shipAfterDate.year}"/>
 		           <input type="text" name="shipToDates" style="display:none" value="${shipAfterDate.shipAfterDate}"/>
                </div>
               <div class="col-md-4 col-xs-4 units">
                    <div class="input-params">
                        <img class="decrement" src="/_ui/responsive/cat/images/minus-icon.png" ng-click="upo.decrement(${counter.index})" ng-disabled="upo.view.reorderMonths[${counter.index}].cannotDecrement" ng-class="(upo.view.reorderMonths[${counter.index}].cannotDecrement)?'disabled':''">
                        <input type="text" size=3 id="qty${counter.index}" name="qty" restrict-to="[0-9]" ng-change="upo.validateQuantity($parent.$index, $index)" ng-init="upo.updateMonths(${counter.index},0,0,0,${product.shippingFactor})" ng-model="upo.view.reorderMonths[${counter.index}].value" readonly="readonly" />
                        <img class="increment" src="/_ui/responsive/cat/images/plus-icon.png" ng-click="upo.increment(${counter.index})" ng-disabled="upo.view.reorderMonths[${counter.index}].cannnotIncrement" ng-class="(upo.view.reorderMonths[${counter.index}].cannnotIncrement)?'disabled':''">
                    </div>
                </div>
                 <div class="col-md-4 col-xs-4 date">
                    <c:if test="${shipAfterDate.shipFromDate.date mod 10 eq 1}">${shipAfterDate.shipFromDate.date}st</c:if>
                    <c:if test="${shipAfterDate.shipFromDate.date mod 10 eq 2}">${shipAfterDate.shipFromDate.date}nd</c:if>
                    <c:if test="${shipAfterDate.shipFromDate.date mod 10 eq 3}">${shipAfterDate.shipFromDate.date}rd</c:if>
                    <c:if test="${shipAfterDate.shipFromDate.date mod 10 gt 3 or shipAfterDate.shipFromDate.date mod 10 eq 0 or (shipAfterDate.shipFromDate.date gt 10 and shipAfterDate.shipFromDate.date lt 20)}">${shipAfterDate.shipFromDate.date}th</c:if>
                    - 
                    
                    <c:if test="${shipAfterDate.shipToDate.date mod 10 eq 1}">${shipAfterDate.shipToDate.date}st</c:if>
                    <c:if test="${shipAfterDate.shipToDate.date mod 10 eq 2}">${shipAfterDate.shipToDate.date}nd</c:if>
                    <c:if test="${shipAfterDate.shipToDate.date mod 10 eq 3}">${shipAfterDate.shipToDate.date}rd</c:if>
                    <c:if test="${shipAfterDate.shipToDate.date mod 10 gt 3 or shipAfterDate.shipToDate.date mod 10 eq 0 or (shipAfterDate.shipToDate.date gt 10 and shipAfterDate.shipToDate.date lt 20)}">${shipAfterDate.shipToDate.date}th</c:if>
                </div>
            </div>
         </c:forEach> 
        </div>
        <div class="col-md-12 hidden-xs">
                <div class="row order-button">
                     <div class="col-md-4">
                        <spring:theme code="utv.order.total"/>
                    </div>
                    <div class="col-md-4" style="text-align: center;" ng-bind="upo.view.unitTotalValue">
                    </div>
                    <div class="col-md-4">
                        <button class="cat-buttons primary-btn" ng-click="navigateToUTVRevieworder('${product.code}','0',${utilityProducts},'${cartType}')" ng-disabled="upo.view.unitTotalValue<1" ng-class="{'disabled':(upo.view.unitTotalValue<1)}">Order</button>
                    </div>
                </div>
        </div>
    </div>
    <div class="reorder-bottom-mobile row visible-xs">
        <div class="col-xs-6 quantity-units">
            <p class="quantity"><spring:theme code="utv.order.quantity"></spring:theme></p>
            <p class="units"><span ng-bind="upo.view.unitTotalValue">
            </span><span> <spring:theme code="utv.order.units.small"></spring:theme></span><p>
        </div> 
        <div class="col-xs-6 order-button">
            <button class="reorder-button upperCase" ng-class="(!upo.view.canreorder)?'disabled':''" ng-click="navigateToUTVRevieworder('${product.code}','0',${utilityProducts},'${cartType}')" ng-disabled="upo.view.unitTotalValue<1">Order</button>
        </div>
    </div>
     <div class="config-truckLoad row">
        <div class="col-xs-12 info"><spring:theme code="configure.truckload.text"/></div>
        <div class="col-xs-12 text-right"><button class="cat-buttons secondary-btn config-button" ng-click="navigateToTruckLoad('${product.code}','0',${utilityProducts},'${cartType}')"><spring:theme code="cat.configure.truckload"></spring:theme></button></div>
    </div>
    </c:if>
    <c:if test="${fn:length(orderWindows) eq 0}">
	      <div class="message"><cms:component uid="nextOrderWindowInfoParagraphComponent"/></div>
	</c:if>      

   
</div>