<div ng-controller="newProductsPDPMobileController as npmc" class="pdp-new-product-mobile-view visible-xs" ng-cloak>
<div class="cat-container row">
    <div class="col-xs-12 product-row">
        <div class="col-xs-12 carousel-header">
            <span class="section-header">259 AB1 5674</span>
        </div>
        <slick scroll-y="scrollY" arrows="false" class="slider col-xs-12 no-padding" infinite="false" speed=300  ng-if="dataLoaded" init-onload="true" slides-to-show="1" slides-to-scroll="1" dots="true">
        <div ng-repeat="item in items">
            <span class="clearfix">
               <img ng-if="bookMarked" class="book-marked favorite-icon" src="/_ui/responsive/cat/images/fill-265.svg" ng-click="toggleBookMark()" stop-event alt="Book Mark">
			   <img ng-if="!bookMarked" class="not-book-marked favorite-icon" src="/_ui/responsive/cat/images/fill-264.svg" ng-click="toggleBookMark()" stop-event alt="Book Mark">
               <img ng-src="{{npmc.defaultDownloadImage}}" />
            </span>
        </div>
        </slick>
    </div>
    <div class="col-xs-12 product-description-container product-row">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. </p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet.</p>
    </div>
    <div class="col-xs-12 no-padding product-row">
        <hr>
    </div>
    <div class="col-xs-12 product-row">
        <span class="section-header">259D STOCK LEVELS</span>
        <div class="col-xs-12 no-padding stock-level-row">
            <div class="col-xs-6">MINIMUM</div>
            <div class="col-xs-6 text-right">5 units</div>
        </div>
        <div class="col-xs-12 no-padding stock-level-row">
            <div class="col-xs-6">MAXIMUM</div>
            <div class="col-xs-6 text-right">15 units</div>
        </div>
        <div class="col-xs-12 no-padding stock-level-row">
            <div class="col-xs-6">RECOMMENDED</div>
            <div class="col-xs-6 text-right">9 units</div>
        </div>
        <div class="col-xs-12 no-padding stock-level-row">
            <div class="col-xs-6">CURRENT</div>
            <div class="col-xs-6 below-level text-right">4 units</div>
        </div>
    </div>
    <div class="col-xs-12 no-padding product-row">
        <hr>
    </div>
    <div class="col-xs-12 product-row">
        <span class="section-header">259-80987 REORDER QUANTITY</span>
        <div class="col-xs-6 section-header stock-level-row no-padding">CURRENT</div>
        <div class="col-xs-6 section-header text-right stock-level-row">
            <span class="decrement"></span>
            <input class="quantity-box" restrict-input-quote type="text" value="0"/>
            <span class="increment"></span>
        </div>
    </div>
</div>
<div class="col-xs-12 no-padding product-compare-accordion">
    <uib-accordion>
        <div uib-accordion-group class="panel-default" is-open="true">
            <uib-accordion-heading>
                <a data-toggle="collapse" ng-class="{'expanded':status.open,'collapsed':!status.open}">
                    SEARCH PARAMETERS
                </a>
            </uib-accordion-heading>
                <div class="accordion-body">
                    <div class="col-xs-12 horizontal-padding">
                        <div class="col-xs-6 header no-padding">PRODUCT FAMILY</div>
                        <div class="col-xs-6 value text-right no-padding">Compact Track Loader</div>
                    </div>
                    <div class="col-xs-12 horizontal-padding">
                        <div class="col-xs-6 header no-padding">SALES MODEL</div>
                        <div class="col-xs-6 value text-right no-padding">259D</div>
                    </div>
                    <div class="col-xs-12 horizontal-padding">
                        <div class="col-xs-6 header no-padding no-margin-bottom">RETAIL CONFIGURATION</div>
                        <div class="col-xs-6 value text-right no-padding no-margin-bottom">259 AB1 5674</div>
                    </div>
                </div>
        </div>
        <div uib-accordion-group class="panel-default" is-open="true">
            <uib-accordion-heading>
                <a data-toggle="collapse" ng-class="{'expanded':status.open,'collapsed':!status.open}">
                    INVENTRY COUNT
                </a>
            </uib-accordion-heading>
                <div class="accordion-body">
                    <div class="col-xs-12 horizontal-padding">
                        <div class="col-xs-6 value no-padding">ADDRESS</div>
                        <div class="col-xs-6 value no-padding text-right">QUANTITY</div>
                    </div> 
                    <div class="col-xs-12 horizontal-padding">
                        <div class="col-xs-6 header no-padding">
                            Warehouse X121 S State Street,Chicago - 360001
                        </div>
                        <div class="col-xs-6 value no-padding text-right">27</div>
                    </div>
                    <div class="col-xs-12 no-padding"><hr/></div>
                    <div class="col-xs-12 horizontal-padding">
                        <div class="col-xs-6 header no-padding">
                            Warehouse X121 S State Street,Chicago - 360001
                        </div>
                        <div class="col-xs-6 value no-padding text-right below-level">Not in stock</div>
                    </div> 
                </div>
        </div>
        <div uib-accordion-group class="panel-default" is-open="true">
            <uib-accordion-heading>
                <a data-toggle="collapse" ng-class="{'expanded':status.open,'collapsed':!status.open}">
                    MACHINE CONFIGURATIONS
                </a>
            </uib-accordion-heading>
                <div class="accordion-body">
                    <div class="col-xs-12 horizontal-padding">
                        <ul class="header">
                            <li>-12 volt Electrical System</li>
                            <li>-80 ampere Alternator</li>
                            <li>-Ignition Key Start/Stop Switch</li>
                            <li>-Lights: Gauge Backlighting, Two Rear Tail Lights, Two Rear Halogen Working Lights, Two Adjustable Front Halogen Lights, Dome Light</li>
                            <li>-Backup Alarm</li>
                            <li>-Heavy Duty Battery, 880 CCA</li>
                        </ul>
                    </div>
                </div>
        </div>
        <div uib-accordion-group class="panel-default" is-open="true">
            <uib-accordion-heading>
                <a data-toggle="collapse" ng-class="{'expanded':status.open,'collapsed':!status.open}">
                    ATTACHMENTS
                </a>
            </uib-accordion-heading>
                <div class="accordion-body">
                    <div class="col-xs-12 horizontal-padding">
                        <ul class="header">
                            <li>-Attachment 1</li>
                            <li>-Attachment 2</li>
                            <li>-Attachment 3</li>
                        </ul>
                    </div>
                </div>
        </div>
    </uib-accordion>
</div>
<div class="col-xs-12 cart-create-quote-section">
    <div class="no-padding quote-price-mobile-view">
        <label class="total-price-heading">QUANTITY</label><br/>
        <label class="total-price-value">3 units</label>
    </div>
    <div class="no-padding quote-btn">
        <button class="btn update-product-list create-quote-btn pull-right">REORDER</button>
    </div>
</div>
</div>