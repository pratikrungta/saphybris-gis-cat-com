<div class="cat-container row pdp-used-product-mobile-view visible-xs" ng-controller="usedProductsPDPMobileController as upmc">
    <div class="col-xs-12 no-padding">
        <div class="col-xs-7 no-padding">
            <div class="col-xs-12 used-equipment-heading">USED EQUIPMENTS</div>
            <div class="col-xs-12 results-heading">Showing 20 of 50 results</div>
        </div>
        <div class="col-xs-5">
            <button class="btn pull-right filters-button" ng-click="upmc.toggleSidebar($event);">Filters</button>
        </div>
    </div>
    <pdp-used-product-card ng-repeat="product in upmc.productListToDisplay" header-code="MAC00259"
     sales-model="259D"
     image="{{upmc.defaultDownloadImage}}"
     price="3456"
     dealer-name="Carolina CAT"
     branch-location="State College"
     hours-used="500"
     manufacturing-year="2016"
     book-marked="false"
    >
    </pdp-used-product-card>
    <div class="show-more-container cat-container row" ng-if="upmc.noOfPagesInMobileView < upmc.items.length">
        <span class="show-more" ng-click="upmc.loadMoreProducts();">Show More</span>
    </div>
    <div id="sidebar-wrapper" class="">
                <div class="col-xs-12 pdp-sidebar-padding">
                    <div class="pull-left">
                        <span class="filters-heading">FILTERS</span>
                        <span class="filter-divider"></span>
                    </div>
                    <div class="pull-right">
                        <span class="close-modal" ng-click="upmc.toggleSidebar($event);"></span>
                    </div>
                </div>
                <div class="col-xs-12 inventory-heading-row pdp-sidebar-padding">
                    <span class="inventory-heading">INVENTORY TYPE</span>
                </div>
                <div class="col-xs-12 product-dropdown-row pdp-sidebar-padding">
                        <div class="col-xs-12 product-family-dropdown-header no-padding competitor-dropdown-header">PRODUCT FAMILY</div>
                        <span uib-dropdown class="product-family-btn col-xs-12">
                            <a class="col-xs-12 product-family-dropdown clearfix" href uib-dropdown-toggle>
                                <label class="product-family-dropdown-label">{{upmc.productCompareDropDownLabel.name | initcaps}}</label> 
                                <img class="pull-right product-family-dropdown-arrow" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"/>
                            </a>
                            <ul class="col-xs-12 dropdown-menu product-family-dropdown-menu" uib-dropdown-menu>
                                <li role="menuitem" class="product-family-dropdown-item" ng-repeat="category in upmc.productCategories" ng-click="upmc.updateDropDownValue(category);">{{category.name | initcaps}}</li>
                            </ul>
                        </span>   
                </div>
                <div class="col-xs-12 product-dropdown-row pdp-sidebar-padding">
                        <div class="col-xs-12 product-family-dropdown-header no-padding competitor-dropdown-header">SALES MODEL</div>
                        <span uib-dropdown class="product-family-btn col-xs-12">
                            <a class="col-xs-12 product-family-dropdown clearfix" href uib-dropdown-toggle>
                                <label class="product-family-dropdown-label">259D</label> 
                                <img class="pull-right product-family-dropdown-arrow" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"/>
                            </a>
                            <ul class="col-xs-12 dropdown-menu product-family-dropdown-menu" uib-dropdown-menu>
                                <li role="menuitem" class="product-family-dropdown-item">259D</li>
                            </ul>
                        </span>   
                </div>
                <div class="col-xs-12 no-padding">
                    <hr/>
                </div>
                <div class="col-xs-12 no-padding product-compare-accordion">
                    <uib-accordion close-others="false">
                        <div uib-accordion-group class="panel-default" ng-repeat="item in upmc.accordionData" is-open="item.open">
                            <uib-accordion-heading ng-click="item.open = !item.open">
                                <a data-toggle="collapse" ng-class="{'expanded':item.open,'collapsed':!item.open}">
                                    {{item.label}}
                                </a>
                            </uib-accordion-heading>
                            <div class="accordion-body" ng-if="item.label === 'BRANCH LOCATION'">
                                 <div class="col-xs-12">
                                    <input type="text" placeholder="Enter Location" class="branch-location-search-box"/>
                                 </div>   
                                 <ul class="col-xs-12 branch-list">
                                    <li>
                                        <label class="additional-service-checkbox-container">Chicago
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="additional-service-checkbox-container">New York
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="additional-service-checkbox-container">Raleigh
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="additional-service-checkbox-container">Seatle
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="additional-service-checkbox-container">Charlotte
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                    </li>
                                 </ul>
                            </div>
                            <div class="accordion-body" ng-if="item.label === 'MANUFACTURING YEAR'">
                               <div class="col-xs-12 clearfix">
                                    <input type="text" class="pull-left text-box" placeholder="From"/>
                                    <span class="pull-left divider">-</span>
                                    <input type="text" class="pull-left text-box" placeholder="To"/>
                               </div>
                               <div class="col-xs-12">
                                    <rzslider rz-slider-model="slider.minValue"
                                    rz-slider-high="slider.maxValue"
                                    rz-slider-options="slider.options"></rzslider>
                               </div>  
                            </div>
                            <div class="accordion-body" ng-if="item.label === 'HOURS USED'">
                               <div class="col-xs-12 clearfix">
                                    <input type="text" class="pull-left text-box" placeholder="min"/>
                                    <span class="pull-left divider">-</span>
                                    <input type="text" class="pull-left text-box" placeholder="max"/>
                               </div>
                               <div class="col-xs-12">
                                    <rzslider rz-slider-model="slider.minValue"
                                    rz-slider-high="slider.maxValue"
                                    rz-slider-options="slider.options"></rzslider>
                               </div>  
                            </div>
                            <div class="accordion-body" ng-if="item.label === 'PRICE'">
                               <div class="col-xs-12 clearfix">
                                    <input type="text" class="pull-left text-box" placeholder="min"/>
                                    <span class="pull-left divider">-</span>
                                    <input type="text" class="pull-left text-box" placeholder="max"/>
                               </div>
                               <div class="col-xs-12">
                                    <rzslider rz-slider-model="slider.minValue"
                                    rz-slider-high="slider.maxValue"
                                    rz-slider-options="slider.options"></rzslider>
                               </div>  
                            </div>
                            <div class="accordion-body" ng-if="item.label === 'CERTIFICATION'">
                               <ul class="col-xs-12 branch-list">
                                    <li>
                                        <label class="additional-service-checkbox-container">Dealer Certified
                                            <input type="checkbox">
                                            <span class="checkmark"></span>
                                        </label>
                                    </li>
                                </ul> 
                            </div>
                        </div>
                    </uib-accordion>
                </div> 
                <div class="col-xs-12">
                    <button class="btn update-product-list pdp-used-product-update pull-right">UPDATE</button>
                </div>     
</div>
</div>