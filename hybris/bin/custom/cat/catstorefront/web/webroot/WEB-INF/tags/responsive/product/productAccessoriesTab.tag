<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true"
	type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<div class="tab-details">
	<ycommerce:testId code="productDetails_content_label">
	<div class="accessories-sub-headding">
	<spring:theme code="product.tabs.overview" />
	</div>
		<p><a class="accessories-overview-link"><spring:theme code="product.tabs.accessories.click" /></a><spring:theme code="product.tabs.accessories.order" /></p>
		<div class="tab-data">
			<c:set var="flag" value="false" />

			<c:forEach items="${accessories}" var="accessory">
				<table class="table">
					<tbody>
						<tr class="table-header">
							<th class="padleft40"><spring:theme code="product.tabs.partDescription" /></th>
							<th class="width-30"><spring:theme code="product.tabs.partNumber" /></th>
							<th class="width-30"><spring:theme code="product.tabs.netPrice" /></th>
						</tr>
						<c:set var="flag" value="true" />
						<div class="accessories-sub-headding"> ${accessory.key}</div>
						<c:forEach items="${accessory.value}" var="productReferenceData">
							<tr>
								<td class="attrib padleft40">${productReferenceData.target.name}</td>
								<td class="attrib">${productReferenceData.target.code}</td>
								<td class="attrib">${productReferenceData.target.price.formattedValue}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</c:forEach>
			<c:if test="${flag eq false}">
				<spring:theme code="product.tabs.noAccessories" />
			</c:if>
		</div>
	</ycommerce:testId>
</div>