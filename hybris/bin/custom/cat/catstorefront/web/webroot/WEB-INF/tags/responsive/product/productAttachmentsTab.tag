<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<div class="tab-details">
	<ycommerce:testId code="productDetails_content_label">
		<p>
			<div class="tab-data">
			<c:set var="flag" value="false"/>
			  <ul>
				<c:forEach items="${product.productReferences}" var="reference">
					<c:if test="${(reference.referenceType) eq 'ATTACHMENT'}">
						<c:set var="flag" value="true"/>
						<li><i class="fa fa-circle bullet" aria-hidden="true"></i>${fn:toLowerCase(reference.target.name)}</li> 
					</c:if>
				</c:forEach>
				</ul>
				<c:if test="${flag eq false}">
					<spring:theme code="product.tabs.noAttachments" />
				</c:if>
			</div>
		<p>
	</ycommerce:testId>
</div>