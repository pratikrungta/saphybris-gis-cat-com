<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="product-classifications">
		<div class="main-heading">
			<spring:theme code="product.tabs.overview" />
		</div>
		<div class="message">
			${product.specificationOverview}
		</div>
		
		<div class="toggle-buttons">
			<div class="btn-group pull-right">
			   <label class="btn btn-primary text hidden-xs"><spring:theme code="cat.speccheck.compare.units" /> </label>
        <label class="btn btn-primary radio-button active units-us"><spring:theme code="cat.speccheck.us" /></label>
				<label class="btn btn-primary radio-button units-metrics"><spring:theme code="cat.speccheck.compare.metric" /></label>
			</div>
		</div>
		
		<c:if test="${not empty product.classifications}">
			<c:forEach items="${product.classifications}" var="classification">
			<c:if test="${classification.code ne 'UTV_PROD'}">
				<div class="headline pull-left">${fn:escapeXml(classification.name)}</div>
					<table class="table">
						<tbody>
							<tr class="header-row">
								<td> <spring:theme code="cat.description.label" /> </td>
								<td> <spring:theme code="cat.value.label" /> </td>
							</tr>
							<c:forEach items="${classification.features}" var="feature">
								<c:if test = "${fn:contains(feature.code, '_us')}">
								<tr class="units_metrics_us">
	         						<td class="attrib" >${fn:escapeXml(feature.name)}</td>
									<td>
										<c:forEach items="${feature.featureValues}" var="value" varStatus="status">
											${fn:escapeXml(value.value)}
											<c:choose>
												<c:when test="${feature.range}">
													${not status.last ? '-' : fn:escapeXml(feature.featureUnit.symbol)}
												</c:when>
												<c:otherwise>
													${fn:escapeXml(feature.featureUnit.symbol)}
													${not status.last ? '<br/>' : ''}
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</td>
								</tr>
	      						</c:if>
	      						<c:if test = "${fn:contains(feature.code, '_metric')}">
								<tr class="units_metrics_metric">
	         						<td class="attrib" >${fn:escapeXml(feature.name)}</td>
									<td>
										<c:forEach items="${feature.featureValues}" var="value" varStatus="status">
											${fn:escapeXml(value.value)}
											<c:choose>
												<c:when test="${feature.range}">
													${not status.last ? '-' : fn:escapeXml(feature.featureUnit.symbol)}
												</c:when>
												<c:otherwise>
													${fn:escapeXml(feature.featureUnit.symbol)}
													${not status.last ? '<br/>' : ''}
												</c:otherwise>
											</c:choose>
										</c:forEach>
									</td>
								</tr>
	      						</c:if>
							</c:forEach>
						</tbody>
					</table>
					</c:if>
			</c:forEach>
		</c:if>
</div>
