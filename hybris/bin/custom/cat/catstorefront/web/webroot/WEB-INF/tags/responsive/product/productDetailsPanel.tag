<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="pdpComponents" tagdir="/WEB-INF/tags/responsive/pdp"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>


<div class="row cat-container" id="pdp-container" ng-app="cat-app"
	ng-controller="pdpController" ng-init="roleFeatureInfoProductDetails('${isUTVUser}','${isIM}','${isSP}');setter('${userType}',${utilityProducts}, '${product.code}', '${salesModel}', '${productCondition.code}','${laneType}')">
	<input type="hidden" name="productCodePost" id="productCodePostId" value="${fn:escapeXml(product.code)}"/>
	
	<pop-up id="favoriteLimitReached"
		alert-header="You have reached the maximum limit of marking a product favorite. Please unmark some favorite products to continue"
		ok="DONE">
	</pop-up>
	<div class="row page-headding clearfix">
		<div class="headding upperCase">
			<spring:theme code="cat.pdp.headding.text" />
		</div>
				<a  ng-click ="backToReorderInitiated()" ng-if="isFromReorderPLP()"
				class="back-to-reorder pull-right" ng-cloak>
						<spring:theme code="reorder.back"/>
		</a>
		
	</div>
	<div class="row">
		<div class="col-sm-3 hidden-xs left-content">

			<cms:pageSlot position="ProductLeftRefinements" var="feature"
				element="div" class="product-list-left-refinements-slot">
				<cms:component component="${feature}" element="div"
					class="yComponentWrapper product-list-left-refinements-component" />
			</cms:pageSlot>

			<c:if test="${not isInventoryManager and not utilityProducts and productCondition.code == 'New'}">
				<%-- 			<cms:pageSlot position="PDPVirtualShowRoomAccess_Section" var="comp" --%>
				<%--  				element="div" class="product-list-left-refinements-slot">  --%>
				<%--  				<cms:component component="${comp}" element="div"  --%>
				<%--  					class="yComponentWrapper product-list-left-refinements-component" />  --%>
				<%--  			</cms:pageSlot>  --%>
				<c:choose>
					<c:when test="${(product.supportMaterialLink==null)}">
						<div class="pdp-tools">
							<div class="single-tool">
								<div>
									<spring:theme code="cat.pdp.support.material" />
								</div>
								<div class="nav-link">
									<a href="http://www.cat.com/en_US.html" target="_blank"><spring:theme code="cat.pdp.view" /></a>
								</div>
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<div class="pdp-tools">
							<div class="single-tool">
								<div>
									<spring:theme code="cat.pdp.support.material" />
								</div>
								<div class="nav-link">
									<a href="${product.supportMaterialLink}" target="_blank"><spring:theme
											code="cat.pdp.view" /></a>
								</div>
							</div>
						</div>
					</c:otherwise>
				</c:choose>
				
			</c:if>

			<c:if test="${not isInventoryManager and not utilityProducts and (productCondition.code == 'New' or productCondition.code == 'Used')}"> 
				<c:if test="${not empty product.coachingToolLinks}">
					<div class="pdp-tools">
						<div class="col-xs-12 single-tool">
							<div>
								<spring:theme code="cat.pdp.coaching.tools" />
							</div>
							<div class="nav-link nav-multibuttons" ng-click="viewCoachingTools()">
								<a href="javascript:void(0)">View List</a>
							</div>
						</div>
					</div>
				</c:if>
			</c:if>
		</div>

		<div class="col-xs-12 col-sm-9 right-content">
			<div class="row">
			  	<div class="col-xs-12 col-sm-6 mobile-no-padding">
				 	<div class="borderTitle">
						<div class="product-details shoppingCartPageTitle page-title">
							<ycommerce:testId
								code="productDetails_productNamePrice_label_${product.code}">
								<c:if test="${utilityProducts}">
									<div class="name">${fn:escapeXml(salesModel)}</div>
								</c:if>
								<c:if test="${not utilityProducts}">
									<c:if test="${productCondition ne null and productCondition eq 'USED'}">
									<div class="name">
										<c:if test="${not empty SERIAL_NUMBER}">
										${fn:escapeXml(SERIAL_NUMBER)}
										</c:if>
									</div>
								</c:if>
								<c:if test="${productCondition ne null and productCondition eq 'NEW'}">
									<div class="name">${fn:escapeXml(product.name)}</div>
								</c:if>
								</c:if>
							</ycommerce:testId>
						</div>
					</div>
				</div>
		 	</div>
			<div class="row product-img-desc">
				<div class="col-xs-12 col-sm-6 image-container">
					<product:productImagePanel galleryImages="${galleryImages}" />
					<c:if test="${not empty product.categories}">
						<c:if test="${fn:toLowerCase(product.categories[0].name) == 'telehandlers'}">
							<div class="row tele-handler-message visible-xs">
								! Actual configuration differs from image presented. Please verify the configuration details below.
							</div>
						</c:if>
					</c:if>
				</div>
				<div class="col-xs-12 col-sm-6 mobile-no-padding">
					<c:if test="${productCondition ne null and productCondition eq 'USED'}">
						<div class="prodcut-metaData usedPDP">
							<pdpComponents:usedProductMetaData />
						</div>
					</c:if>

					<div class="row product-details pdtDetailsContainerBox">
						<c:if test="${productCondition ne null and productCondition eq 'NEW'}">
							<div class="col-xs-12">
								<div class="row utv-headding-row">
									<div class="col-xs-8 utv-headding">
											<span class="name"><spring:theme code="cat.pdp.caterpillar"/></span>
									</div>
									<div class="col-xs-4 utv-price">
										<div class="pull-right">
											<div class="formated-price">
												<format:price priceData="${product.price}" />
											</div>
											<c:if test="${utilityProducts ne null and utilityProducts eq 'true'}">
												<div class="pull-right price-desc">
													<spring:theme code="cat.pdp.msrp.text" />
												</div>
											</c:if>
										</div>
									</div>
								</div>
							</div>
						</c:if>
						<div class="col-xs-12">
							<div class="description readMoreClass" id="pdp-description">${ycommerce:sanitizeHTML(product.description)} </div> <div class="col-xs-12 remove-maring-right"> 
<span class="pull-right" id="description-read-more">View More</span> </div>
							<c:if test="${productCondition ne null and productCondition eq 'NEW'}">
									<c:if test="${userType ne null and userType eq 'SP'}">
										<cms:pageSlot position="AddToCart" var="component" element="div"
											class="page-details-variants-select">
											<cms:component component="${component}" element="div"
												class="yComponentWrapper page-details-add-to-cart-component" />
										</cms:pageSlot>
										<c:if test="${not product.isReorderable}">
											<div class="discontinuedStatus">
												<div class="col-xs-1"><img class="warn-iconPDP" src="/_ui/responsive/cat/images/warning.png"></div>
												<div class="col-xs-11 discontinuedStatusText"><spring:theme code="cat.pdp.discontinued.message" /></div>
											</div>
										</c:if>

									</c:if>
								</c:if>
						</div>
					</div>

					<c:if test="${isInventoryManager and utilityProducts }">
							<div class="utvPlaceOrder">
								<pdpComponents:utvPlaceAnOrder />
							</div>
					</c:if>
				
						<div ng-if="${productCondition.code == 'New'} && ${isInventoryManager and not utilityProducts}" class="col-xs-12" id="im-reorder-container">
							<div class="row">
								<div ng-if="laneWiseData.stockingRecommendation !== null" class="col-xs-12 secondary-header text-uppercase no-padding">${salesModel}
									Stock Levels
								</div>
							</div>
							<div class="graph-container">
								<inventory-bar-graph ng-if="pageLoadFlag && laneWiseData.stockingRecommendation !== null" graph="laneWiseData.stockingRecommendation" ></inventory-bar-graph>
							</div>
							<div ng-if="!laneWiseData.hideAddToCart">
								<c:if test="${product.isReorderable}"><%-- <c:if test="${isInventoryManager  and not product.isReorderable and (productCondition ne null and productCondition eq 'NEW')}"> --%>
									<div class="ng-cloak lane-btn-container">
										<p class="select-lane"><spring:theme code="cat.lane.orderingWindowMessage.selectLane" /> :</p>
										<div class="lane-btns" ng-repeat="btn in view.laneButtons track by $index" ng-if="btn.actualLaneObj">
											<button ng-click="laneBtnClick(btn)" class="cat-buttons secondary-btn" ng-class="{'selectedBtn' : view.laneBtnSelectedData.btnKey === btn.isSelectedKey }">{{btn.buttonKey}}
												<span class="dot" ng-class="{'{{btn.isSelectedKey.toUpperCase()}}-dot' : btn.isSelectedKey}"></span>
											</button>
										</div>
										<div ng-show="view.laneBtnSelectedData.btnKey !== 'lane1' && view.laneBtnSelectedData.laneBtnObj != null " class="configuration-box">
											<div ng-if="view.laneBtnSelectedData.btnKey == 'lane2'" class="config-no" ng-bind="view.laneBtnSelectedData.laneBtnObj.committedCount"></div>
											<div ng-if="view.laneBtnSelectedData.btnKey == 'lane3'" class="config-no" ng-bind="view.laneBtnSelectedData.laneBtnObj.allocationCount"></div>
											<div ng-if="view.laneBtnSelectedData.btnKey == 'lane2'" class="allocation-date"><spring:theme code="cat.lane.orderingWindowMessage.committedFor" arguments="{{view.laneBtnSelectedData.laneBtnObj.month_Year}}" /></div>
											<div ng-if="view.laneBtnSelectedData.btnKey == 'lane3'" class="allocation-date"><spring:theme code="cat.lane.orderingWindowMessage.allocatedFor" arguments="{{view.laneBtnSelectedData.laneBtnObj.month_Year}}" /></div>
											<div class="order-window-msg">
												<span ng-if="view.laneBtnSelectedData.btnKey == 'lane2' && view.laneBtnSelectedData.laneBtnObj.orderWindowStatus === 'open'">
														<spring:theme code="cat.lane.orderingWindowMessage.closes.lane2" /> {{view.laneBtnSelectedData.laneBtnObj.leftDays}} <spring:theme code="cat.lane.orderingWindowMessage.closes.lane.days" />
												</span>
												<span ng-if="view.laneBtnSelectedData.btnKey == 'lane2' && view.laneBtnSelectedData.laneBtnObj.orderWindowStatus !== 'open'">
														<spring:theme code="cat.lane.orderingWindowMessage.open.lane2" /> {{view.laneBtnSelectedData.laneBtnObj.leftDays}} <spring:theme code="cat.lane.orderingWindowMessage.closes.lane.days" />
												</span>
												<span ng-if="view.laneBtnSelectedData.btnKey == 'lane3' && view.laneBtnSelectedData.laneBtnObj.orderWindowStatus === 'open'">
														<spring:theme code="cat.lane.orderingWindowMessage.closes.lane3" /> {{view.laneBtnSelectedData.laneBtnObj.leftDays}} <spring:theme code="cat.lane.orderingWindowMessage.closes.lane.days" />
												</span>
												<span ng-if="view.laneBtnSelectedData.btnKey == 'lane3' && view.laneBtnSelectedData.laneBtnObj.orderWindowStatus !== 'open'">
														<spring:theme code="cat.lane.orderingWindowMessage.open.lane3" /> {{view.laneBtnSelectedData.laneBtnObj.leftDays}} <spring:theme code="cat.lane.orderingWindowMessage.closes.lane.days" />
												</span>
											</div>
										</div>
									</div>
								</c:if>
								<div ng-if="view.laneBtnSelectedData.laneBtnObj.displayAddToCartButton">
									<c:if test="${isInventoryManager and product.isReorderable}">
										<cms:pageSlot position="AddToCart" var="component" element="div"
										class="page-details-variants-select">
										<cms:component component="${component}" element="div"
											class="yComponentWrapper page-details-add-to-cart-component" />
									</cms:pageSlot>	
									</c:if>
								</div>
							</div>
						</div>
					
					<c:if test="${isInventoryManager  and not product.isReorderable and (productCondition ne null and productCondition eq 'NEW')}">
						<div class="discontinuedStatusIM">
							<div class="col-xs-1"><img class="warn-iconPDP" src="/_ui/responsive/cat/images/warning.png"></div>
							<div class="col-xs-11 discontinuedStatusText"><spring:theme code="cat.pdp.discontinued.message" /></div>
						</div>
					</c:if>
				</div>
				<c:if test="${not empty product.categories}">
					<c:if test="${fn:toLowerCase(product.categories[0].name) == 'telehandlers'}">
						<div class="row tele-handler-message hidden-xs">
							! Actual configuration differs from image presented. Please verify the configuration details below.
						</div>
					</c:if>
				</c:if>
				<c:if test="${userType ne null and userType eq 'SP'}">
					<div class="row parts-attachments">
						<product:productPageTabs />
					</div>
				</c:if>
				<c:if test="${isInventoryManager and productCondition eq 'USED'}">
					<div class="row parts-attachments">
						<product:productPageTabs />
					</div>
				</c:if>
				<c:if test="${isInventoryManager and productCondition ne 'USED'}">
					<div class="row parts-attachments">
						<product:productPageTabs />
					</div>
				</c:if>
			</div>
		</div>
			<div class="support-mob-tools col-xs-12 visible-xs">
					<c:if test="${not isInventoryManager and not utilityProducts and productCondition.code == 'New'}">
			<%-- 			<cms:pageSlot position="PDPVirtualShowRoomAccess_Section" var="comp" --%>
			<%--  				element="div" class="product-list-left-refinements-slot">  --%>
			<%--  				<cms:component component="${comp}" element="div"  --%>
			<%--  					class="yComponentWrapper product-list-left-refinements-component" />  --%>
			<%--  			</cms:pageSlot>  --%>
			<c:choose>
				<c:when test="${(product.supportMaterialLink==null)}">
					<div class="pdp-tools">
						<div class="single-tool">
							<div class="nav-link col-xs-12">
								<a href="http://www.cat.com/en_US.html" target="_blank"><spring:theme code="cat.pdp.support.material" /></a>
							</div>
						</div>
					</div>
				</c:when>
				<c:otherwise>
					<div class="pdp-tools">
						<div class="single-tool">
							<div class="nav-link col-xs-12">
								<a href="${product.supportMaterialLink}" target="_blank"><spring:theme code="cat.pdp.support.material" /></a>
							</div>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
			
		</c:if>
		<c:if test="${not isInventoryManager and not utilityProducts and (productCondition.code == 'New' or productCondition.code == 'Used')}"> 
			<c:if test="${not empty product.coachingToolLinks}">
				<div class="pdp-tools">
					<div class="single-tool">
						<div class="nav-link col-xs-12" ng-click="viewCoachingTools()">
							<spring:theme code="cat.pdp.coaching.tools" />
						</div>
					</div>
				</div>
			</c:if>
		</c:if>
		</div>
	</div>
		

	<%-- <c:if test="${productCondition.code == 'New'}">
		<product:pdpNewProductMobileView/>
	</c:if>
	<c:if test="${productCondition.code == 'Used'}">
		<product:pdpUsedProductMobileView/>
	</c:if>		 --%>
</div>
