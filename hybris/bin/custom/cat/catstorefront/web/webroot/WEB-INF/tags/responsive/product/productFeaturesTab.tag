<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true"
	type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="tab-details">
	<ycommerce:testId code="productDetails_content_label">
		<div class="tab-data  feature-tab-details">
			<ul class="row stock-details">
				<c:forEach items="${product.catProductFeatures}"
					var="catProductFeature" varStatus="status">
					<li><i class="fa fa-circle bullet" aria-hidden="true"></i>${catProductFeature.productFeature}</li>
				</c:forEach>
			</ul>
		</div>
	</ycommerce:testId>
</div>

