<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true"
	type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<div class="tab-details">
	<ycommerce:testId code="productDetails_content_label">
		<div product-inventory-count-directive  
		data-tab-name="${fn:escapeXml(title)}" 
		data-dealer-code="${dealerCode}" 
		data-product-code="${product.code}"></div>
	</ycommerce:testId>
</div>

