<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<div class="tab-details">
	<ycommerce:testId code="productDetails_content_label">
		<p>
			<div class="tab-data" ng-init='setConfigLengths("${fn:length(partReferenceData)}", "${fn:length(attachmentReferenceData)}")'>
				<div class="tab-data-section configuration">
					<h5 class="section-title"><spring:theme code="product.tabs.configurationdetails" /></h5>
					<c:set var="flag" value="false"/>
					<ul class="row configuration-list lists" ng-class='{ "readMore": partCustom }' id="configuration-details">
						<c:forEach items="${partReferenceData}" var="reference">
							<c:set var="flag" value="true"/>
							<li class="col-md-4"><i class="bullet-dash" aria-hidden="true"></i>${reference}</li> 
						</c:forEach>
					</ul>
					<div ng-if="partLength > maxRowLimit">
						<button class="configuration-link view-more" ng-click="partReadMore()" ng-show="partCustom">MORE</button>
						<button class="configuration-link view-less" ng-click="partReadMore()" ng-hide="partCustom">LESS</button>
					</div>
					<c:if test="${flag eq false}">
						<spring:theme code="product.tabs.noParts" />
					</c:if>
				</div>
				<div class="tab-data-section features">
					<h5 class="section-title"><spring:theme code="product.tabs.features" /></h5> 
					<c:set var="flag" value="false"/>
					<ul class="row features-list lists" ng-class='{ "readMore": attachmentCustom }' id="features-details">
						<c:forEach items="${attachmentReferenceData}" var="reference">
							<c:set var="flag" value="true"/>
							<li class="col-md-4"><i class="bullet-dash" aria-hidden="true"></i>${reference}</li>
						</c:forEach>
					</ul>
					<div ng-if="attachmentLength > maxRowLimit">
						<button class="features-link view-more" ng-click="attachmentReadMore()" ng-show="attachmentCustom">MORE</button>
						<button class="features-link view-less" ng-click="attachmentReadMore()" ng-hide="attachmentCustom">LESS</button>					 
					</div>
					<c:if test="${flag eq false}">
						<spring:theme code="product.tabs.noAttachments" />
					</c:if>
				</div>
			</div>
		<p>
	</ycommerce:testId>
</div>