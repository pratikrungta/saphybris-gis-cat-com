<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="product-compare-dropdown-container row cat-container" ng-controller="productDropDownController as pdc" ng-init="pdc.fetchProductCategories();" ng-cloak>
    <div class="col-md-12">
        <div class="header"><spring:theme code="cat.homepage.productcompare.text"/></div> 
        <div class="sub-header"><spring:theme code="cat.homepage.select.product.compare"/></div>
    </div>
    <div class="col-md-12">
            <div class="col-md-12 no-padding" uib-dropdown>
                    <button type="button" id="product-compare-dropdown" class="btn col-xs-12 drop-down-button" uib-dropdown-toggle>
                    <div class="text" ng-cloak>{{pdc.productCompareDropDownLabel.name | initcaps}}</div>
                    <div class="arrow pull-right"><img class="drop-down" src="/_ui/responsive/cat/images/product-compare-arrow.png" /> </div>
                    </button>
                    <ul uib-dropdown-menu class="col-md-12" role="menu" aria-labelledby="product-compare-dropdown">
                            <li ng-repeat = "category in pdc.productCategories" data-code = "{{category.code}}" ng-click="pdc.updateDropDownValue(category);">
                                <a href="#">
                                    <span>
                                        {{category.name | initcaps}}
                                    </span>
                                </a>
                            </li>
                    </ul>
            </div>
    </div>
    <div class="col-md-12">
        <button class="btn pull-right product-compare-next" ng-disabled = "pdc.productCompareDropDownLabel.name === pdc.dropdownLabel" ng-click="pdc.navigateToConfigureScreen();"><spring:theme code="cat.next"/></button>
    </div>   
</div>
