<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="quotation-cart-container" ng-controller="quotationCartController as qcc" ng-init="qcc.loadCartProducts('${media_prefix_domain}');qcc.roleFeatureInfoQoute();" ng-cloak>
    <div ng-if="qcc.isEmptyCart != undefined  && qcc.isEmptyCart == 0">
        <div class="row no-gutters">
            <div class="row cat-container page-loader" ng-show="!qcc.ajaxSuccess">
            <div class="cat-loading-spinner quotation-cart-loading-spinner"></div>
        </div>
        <div class="product-list-header quotation-cart-page-header cat-container row">
            <span><spring:theme code="cat.quote.cart.section.text" /></span>
        </div>
        <div class="speckcheck-product-list cat-container row">
            <div class="col-md-12 cart-header-section">
                <span><spring:theme code="cat.quote.cart.details.text" /></span>
            </div>
            <div class="col-md-12 cart-product-details-table-section no-padding">
                <table class="col-md-12 cart-product-details-table">
                    <thead>
                        <tr>
                            <th class="col-md-3"><spring:theme code="cat.quote.cart.products.text" /></th>
                            <th class="list-price-header col-md-2"><spring:theme code="cat.quote.cart.list.price.text" /></th>
                            <th class="quantityTabHeader col-md-2"><spring:theme code="cat.quote.cart.quantity.text" /><spring:theme code="cat.quote.cart.mandatory.symbol" /></th>
                            <th class="headerCart col-md-2"><span><spring:theme code="cat.quote.cart.transaction.text" /></span> <span><spring:theme code="cat.quote.cart.price.text" /></span></th>
                            <th class="remove-btn-col col-md-3"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="cart-product-row" ng-repeat="cartProduct in qcc.cartProducts.entries">
                            <td data-title="{{cartProduct.product.categories[0].name}}" class="product-name col-md-3">
                                <div class="product-container">
                                    <div class="row">
                                        <div class="serial-number hidden-xs">
                                            <span>{{$index+1}}.</span>
                                        </div>
                                        <div class="product-name hidden-xs">
                                            <label class="product-family-heading">{{cartProduct.product.categories[0].name}}</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="pull-left product-image-container">
                                            <img ng-src="{{cartProduct.product.images[0].url  || qcc.defaultDownloadImage }}">
                                        </div>
                                        <div class="pull-left visible-xs product-details-container">
                                            <label class="product-family-value">{{cartProduct.product.primaryOfferingName}} ({{cartProduct.product.baseProduct}})</label>
                                            <label class="configuration-id-heading"><spring:theme code="cat.quote.cart.configid.text" /></label>
                                            <label class="configuration-id-value">{{cartProduct.product.name}}</label>
                                        </div>
                                        <div class="pull-right hidden-xs product-details-container">
                                            <label class="product-family-value">{{cartProduct.product.primaryOfferingName}} ({{cartProduct.product.baseProduct}})</label>
                                            <label class="configuration-id-heading"><spring:theme code="cat.quote.cart.configid.text" /></label>
                                            <label class="configuration-id-value">{{cartProduct.product.name}}</label>
                                        </div>
                                        <span class="remove" ng-click="qcc.showRemoveItemPopup($index);"></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </td>
                            <td data-title="LIST PRICE/PRODUCT" class="list-price-per-product col-md-2">
                                <label>{{cartProduct.basePrice.formattedValue}}</label>
                            </td>
                            <td data-title="QUANTITY" class="quantity col-md-2">
                                <div class="quantityContent pull-right">
                                    <span class="decrement" ng-click="qcc.updateCartProducts($index,'decrement');"></span>
                                    <input class="quantity-box" restrict-to="[0-9]" ng-keyup = "qcc.Prevent($event)" type="text" ng-blur="qcc.updateCartProducts($index,'blur');" value="{{qcc.getFormattedText(cartProduct.quantity)}}"/>
                                    <span class="increment" ng-click="qcc.updateCartProducts($index,'increment');"></span>
                                </div>
                                <div class="clearfix"></div>
                            </td>
                            <td data-title="TRANSACTION PRICE" class="list-price col-md-2">
                                <div>
                                    <label>{{cartProduct.totalPrice.formattedValue}}</label>
                                </div>
                                <div class="clearfix"></div>
                            </td>
                            <td class="remove-btn-col hidden-xs col-md-3">
                                <span class="remove-text" ng-click="qcc.showRemoveItemPopup($index);"><spring:theme code="cat.quote.cart.remove.text" /></span><span class="remove" ng-click="qcc.showRemoveItemPopup($index);"></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-12 cart-create-quote-section no-padding">
                <div class="col-md-12 quote-price-desktop-view total-qty-section">
                    <div class="col-md-10 no-padding">
                        <span class="total-price-heading pull-right"><spring:theme code="cat.quote.cart.total.quantity.text" /></span>
                    </div>
                    <div class="col-md-2 no-padding">
                        <span class="total-price-value pull-right">
                        {{qcc.cartProducts.totalUnitCount}} <spring:theme code="cat.quote.cart.units.text" /></span>
                    </div>
                </div>
                <div class="col-md-12 quote-price-desktop-view total-list-price-section">
                    <div class="col-md-10 no-padding">
                        <span class="total-price-heading pull-right"><spring:theme code="cat.quote.cart.total.list.price.text" /></span>
                    </div>
                    <div class="col-md-2 no-padding">
                        <span class="total-price-value pull-right">
                        {{qcc.cartProducts.totalPrice.formattedValue}}</span>
                    </div>
                </div>
                <div class="col-md-12 quote-price-desktop-view cart-disclaimer-section">
                    <div class="col-md-12 no-padding">
                        <span class="disclaimer-text pull-right"><spring:theme code="cat.quote.cart.price.additional.charges.text" /></span>
                    </div>
                </div>
                <div class="no-padding quote-price-mobile-view">
                    <label class="total-price-heading"><spring:theme code="cat.quote.cart.total.text" /></label><br/>
                    <label class="total-price-value">{{qcc.cartProducts.totalPrice.formattedValue}}</label>
                </div>
                <div class="col-md-12 quote-btn">
                    <c:if test="${userType ne null and userType eq 'SP'}">
                        <button class="btn update-product-list create-quote-btn  pull-right" ng-click="qcc.navigateToQuote();"><spring:theme code="cat.quote.cart.proceed.quote.text" /></button>
                    </c:if>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
     </div>
    </div>
    <div class="row cat-container empty-cart" ng-if="qcc.isEmptyCart != undefined && qcc.isEmptyCart != 0">
        <div class="col-md-12 empty-cart-text">
            <spring:theme code="cat.cart.empty.message"/>
        </div>
        <div class="col-md-12">
            <button class="btn update-product-list add-products-cart-btn" ng-click="qcc.navigateToHome();"><spring:theme code="cat.quote.cart.add.products.text" /></button>
        </div>
    </div>
</div>

