<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="quotation-cart-container cat-container row">
    <div class="col-md-12 cart-header-section">
        <span><spring:theme code="cat.quote.details.cart.details" /></span>
    </div>
    <div class="col-md-12 cart-product-details-table-section no-padding">
        <table class="col-md-12 cart-product-details-table">
            <thead>
                <tr>
                    <th><spring:theme code="cat.quote.details.cart.products" /></th>
                    <th class="force-text-right"><spring:theme code="cat.quote.details.cart.list.price" /></th>
                    <th class="force-text-right"><spring:theme code="cat.quote.details.cart.qty" /></th>
                    <th class="text-center"><spring:theme code="cat.quote.details.cart.discount" /></th>
                    <th><spring:theme code="cat.quote.details.cart.transaction.price" /></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="quoteEntry" items="${quoteData.entries}">
                    <tr class="cart-product-row"> 
                        <td data-title="${quoteEntry.product.categories[0].name}"
                            class="product-name">
                            <div class="product-container">
                                <div class="product-family-heading">${quoteEntry.product.categories[0].name}</div>
                                <div class="pull-left product-image-container">
                                    <c:choose>
                                        <c:when test="${quoteEntry.product.images[0].url ne null}">
                                            <img src="${quoteEntry.product.images[0].url}">
                                        </c:when>
                                        <c:otherwise>
                                            <img src='/_ui/responsive/cat/images/download_bkp.png'>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                                <div class="pull-left product-details-container">
                                    <div class="hidden-xs"><label class="product-family-value">${quoteEntry.product.primaryOfferingName}<br/>(${quoteEntry.product.baseProduct})</label></div>
                                    <div class="visible-xs"><label class="product-family-value">${quoteEntry.product.primaryOfferingName} (${quoteEntry.product.baseProduct})</label></div>
                                    <label class="configuration-id-heading"><spring:theme code="cat.quote.details.cart.configuration.id" /></label> <label class="configuration-id-value">${quoteEntry.product.name}</label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </td>
                        <td data-title="LIST PRICE/PRODUCT"
                            class="list-price-per-product"><label>${quoteEntry.basePrice.formattedValue}</label>
                        </td>
                        <td data-title="QUANTITY" class="quantity">
                        <label>${quoteEntry.quantity}</label>
                        </td>
                        <td data-title="DISCOUNT" class="discount">
                        <c:if test="${empty quoteData.quoteDiscountsType}">
                        <label><spring:theme code="cat.quote.details.cart.default.dollar" /></label>
                        </c:if>
                        <c:if
                                test="${quoteData.quoteDiscountsType == 'ABSOLUTE'}">
                                <c:if test="${empty quoteEntry.discount.value}">
                                    <label><spring:theme code="cat.quote.details.cart.default.dollar" /></label>
                                </c:if>
                                <c:if test="${not empty quoteEntry.discount.value}">
                                    <label><spring:theme code="cat.quote.details.cart.dollar" /> ${quoteEntry.discount.value}</label>
                                </c:if>
                            </c:if> <c:if test="${quoteData.quoteDiscountsType == 'PERCENT'}">
                                <c:if test="${empty quoteEntry.discount.value}">
                                    <label><spring:theme code="cat.quote.details.cart.default.percent" /></label>
                                </c:if>
                                <c:if test="${not empty quoteEntry.discount.value}">
                                    <label>${quoteEntry.discount.value} <spring:theme code="cat.quote.details.cart.percent" /></label>
                                </c:if>
                            </c:if></td>
                        <td data-title="TRANSACTION PRICE" class="list-price">
                            <div>

                                <label>${quoteEntry.totalPrice.formattedValue}</label>
                            </div>
                            <div class="clearfix"></div>
                        </td>
                    </tr>
                    <c:if test="${quoteEntry.serialNumbers ne null}">
                        <tr class="reserve-data-row epp-csa-row">
                            <td colspan="6">
                                <div class="col-md-12 epp-csa-col">
                                    <div class="reserve-machines-heading  col-md-12">
                                       <spring:theme code="cat.quote.details.cart.reservation.msg" arguments="${warehouseName},${quoteEntry.endDate}"/>
                                    </div>
                                    <div class="reserve-machines-col col-md-6">
                                        <div class="serial-numbers-column  col-md-12">
                                            <span class="serial-number"><spring:theme code="cat.quote.details.cart.serial.numbers" /></span>
                                            <c:forEach var="serialNumber" items="${quoteEntry.serialNumbers}">
                                                <div class="pills">
                                                    <span class="badge col-md-3">
                                                        ${serialNumber}
                                                    </span>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </c:if>
                        <c:if test="${quoteEntry.eppEntries ne null || quoteEntry.csaEntries ne null}">
                        <tr class="epp-csa-row">
                            <td class="epp-csa-table-col" colspan = "5">
                                <div class="col-md-12 no-padding">
                                    <div class="col-md-10 col-lg-11 epp-csa-col offset-md-2 offset-lg-1 pull-right no-padding">
                                        <table class="col-md-12 epp-csa-table">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th class="text-right"><spring:theme code="cat.quote.details.cart.price" /></th>
                                                    <th class="text-right"><spring:theme code="cat.quote.details.cart.qty" /></th>
                                                    <th class="text-right"><spring:theme code="cat.quote.details.cart.total" /></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                        <c:if test="${quoteEntry.eppEntries ne null}">
                                                <c:forEach var="eppAdditonalInfoEntry" items="${quoteEntry.eppEntries}" varStatus="eppProducts">
                                                
                                                    <tr>
                                                        <td class="col-md-6">
                                                            <div class="col-md-12 no-padding">
                                                                    <div class="col-md-3 no-padding"><span class="config-label">
                                                                    <spring:theme code="cat.quote.details.cart.EPP" arguments="${eppProducts.count}"/></span></div>
                                                                    <div class="col-md-9">
                                                                        <c:forEach var="configInfo" items="${eppAdditonalInfoEntry.configurationInfos}">
                                                                            <!-- <c:if test="${configInfo.configurationLabel eq 'EPPOption'}"> -->
                                                                                <span class="config-value">${configInfo.configurationValue}</span>
                                                                            <!-- </c:if> -->
                                                                        </c:forEach>
                                                                    </div>
                                                            </div>
                                                        </td>
                                                        <td class="text-right">
                                                            <c:if test="${quoteEntry.eppEntries ne null and quoteEntry.eppEntries[0] ne null}">
                                                                <span>${eppAdditonalInfoEntry.basePrice.formattedValue}</span>
                                                            </c:if>
                                                        </td>
                                                        <td class="text-right">
                                                            <c:if test="${quoteEntry.eppEntries ne null and eppAdditonalInfoEntry ne null}">
                                                                <span>${eppAdditonalInfoEntry.quantity}</span>
                                                            </c:if>
                                                        </td>
                                                        <td class="text-right">
                                                            <c:if test="${quoteEntry.eppEntries ne null and eppAdditonalInfoEntry ne null}">
                                                                <span>${eppAdditonalInfoEntry.totalPrice.formattedValue}</span>
                                                            </c:if>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                        </c:if>
                                         <c:if test="${quoteEntry.csaEntries ne null}">
                                                <c:forEach var="csaAdditonalInfoEntry" items="${quoteEntry.csaEntries}" varStatus="csaProducts">
                                                        <tr>
                                                            <td class="col-md-6">
                                                                <div class="col-md-12 no-padding">
                                                                        <div class="col-md-3 no-padding"><span class="config-label"><spring:theme code="cat.quote.details.cart.CSA" arguments="${csaProducts.count}"/></span></div>
                                                                        <div class="col-md-9">
                                                                        <span class="config-value">${csaAdditonalInfoEntry.configurationInfos[1].configurationValue}</span> &nbsp;
                                                                        <span class="config-value">${csaAdditonalInfoEntry.configurationInfos[0].configurationValue} <spring:theme code="cat.quote.details.cart.CSA.Hrs" /></span>&nbsp;
                                                                        <span class="config-value">${csaAdditonalInfoEntry.configurationInfos[2].configurationValue} <spring:theme code="cat.quote.details.cart.CSA.years" /></span>
                                                                        </div>
                                                                </div>
                                                            </td>
                                                            <td class="text-right">
                                                                <c:if test="${quoteEntry.csaEntries ne null and quoteEntry.csaEntries[0] ne null}">
                                                                    <span>${csaAdditonalInfoEntry.basePrice.formattedValue}</span>
                                                                </c:if>
                                                            </td>
                                                            <td class="text-right">
                                                                <c:if test="${quoteEntry.csaEntries ne null and csaAdditonalInfoEntry ne null}">
                                                                    <span>${csaAdditonalInfoEntry.quantity}</span>
                                                                </c:if>
                                                            </td>
                                                            <td class="text-right">
                                                                <c:if test="${quoteEntry.csaEntries ne null and csaAdditonalInfoEntry ne null}">
                                                                    <span>${csaAdditonalInfoEntry.totalPrice.formattedValue}</span>
                                                                </c:if>
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                </c:if> 
                                                <!-- <tr>
                                                    <td class="text-left">
                                                        <div>
                                                            <c:if test="${quoteEntry.entries ne null and quoteEntry.entries[0] ne null}">
                                                                <span>${quoteEntry.entries[0].basePrice.formattedValue}</span>
                                                            </c:if>
                                                        </div>
                                                    </td>
                                                    <td class="text-center">
                                                        <c:if test="${quoteEntry.entries ne null and quoteEntry.entries[0] ne null}">
                                                            <span>${quoteEntry.entries[0].quantity}</span>
                                                        </c:if>
                                                    </td>
                                                    <td class="text-right">
                                                        <c:if test="${quoteEntry.entries ne null and quoteEntry.entries[0] ne null}">
                                                            <span>${quoteEntry.entries[0].totalPrice.formattedValue}</span>
                                                        </c:if>
                                                    </td>
                                                </tr> -->
                                            </tbody>
                                         </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </c:if>
                </c:forEach>
            </tbody>
        </table>
    </div>
    <div class="col-md-12 col-xs-12 total-price-section">
        <div class="col-md-12 col-xs-12 total-list-price no-padding">
             <div class="col-md-10 col-xs-6 no-padding">
                <label class="price-header"><spring:theme code="cat.quote.details.cart.total.list.price" /></label>
            </div>
            <div class="col-md-2 col-xs-6 no-padding">
                <label class="price-value">${totalListPrice}</label>
            </div>
        </div>
        <div class="col-md-12 col-xs-12 total-discount no-padding">
            <div class="col-md-10 col-xs-6 no-padding">
                <label class="price-header"><spring:theme code="cat.quote.details.cart.total.discount" /></label>
            </div>
            <div class="col-md-2 col-xs-6 no-padding">
                <label class="price-value">${quoteData.totalDiscounts.formattedValue}</label>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-xs-12 transaction-price-section">
        <div class="col-md-10 col-xs-6 no-padding">
            <label class="price-header"><spring:theme code="cat.quote.details.cart.total.transaction.price" /></label>
        </div>
        <div class="col-md-2 col-xs-6 total-transction-price no-padding">
            <div>
                <label class="price-value">${quoteData.totalPrice.formattedValue}</label>
            </div>
        </div>
    </div>
</div>
