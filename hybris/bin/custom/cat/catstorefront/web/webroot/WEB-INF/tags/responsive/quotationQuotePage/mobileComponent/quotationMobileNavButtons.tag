<div class="row visible-xs cat-container quote-mobile-navigation" ng-controller="quotationMobileNavController as qmc">
    <div class="col-xs-6 no-padding">
        <div ng-show="qmc.buttonsMetaInfo.showPrevious">
            <img src="/_ui/responsive/cat/images/left-arrow01.png" width="40" height="40" ng-click="qmc.previousPage()"><span class="text text-left" ng-bind="qmc.buttonsMetaInfo.previousButtonText"></span>
        </div>
    </div>
    <div class="col-xs-6 no-padding">
        <div class="pull-right"  ng-show="qmc.buttonsMetaInfo.showNext">
            <span class="text text-right" ng-bind="qmc.buttonsMetaInfo.nextButtonText"></span><img  src="/_ui/responsive/cat/images/right-arrow01.png" width="40" height="40" ng-click="qmc.nextPage()">
        </div>
    </div>
</div>
