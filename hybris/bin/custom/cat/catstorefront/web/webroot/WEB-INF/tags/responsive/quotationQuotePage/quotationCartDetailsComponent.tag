<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<div class="quotation-cart-container cat-container row" ng-controller="quoteCartDetailsController as qcdc" ng-init="qcdc.loadCartProducts('${media_prefix_domain}','${cartData.quoteData.code}','${firstTimeEdit}', '${isReplicateQuote}');" ng-cloak ng-hide="qcdc.view.quoteMetaData.mobileMetaData.isMobileScreen && qcdc.view.quoteMetaData.mobileNav.currentPageNum!=2">
    <div class="quote-content-container">
        <div class="row no-gutters cart-header-section">
            <span><spring:theme code="cat.quote.cart.details.heading.text" /></span>
        </div>
        <article class="quote-cart-template-container">
            <div class="row no-gutters quote-cart-header hidden-xs">
                <div class="col-md-4">
                    <spring:theme code="cat.quote.cart.products.heading.text" />
                </div>
                <div class="col-md-8">
                    <div class="col-md-2"><spring:theme code="cat.quote.cart.item.listPrice" /></div>
                    <div class="col-md-3"><spring:theme code="cat.quote.cart.item.quantity" /></div>
                    <div class="col-md-2 discount-header"><spring:theme code="cat.quote.cart.item.discount" />
                        <div class="discount-container">    
                            <label>
                                <input type="radio" name="discount-type" value="ABSOLUTE" 
                                ng-model="qcdc.discountRadio"
                                ng-change="qcdc.toggleDiscountCalc(qcdc.discountRadio);"/>
                                <spring:theme code="cat.quote.cart.item.amount.discount" />
                                <span class="checkmark"></span>
                            </label>
                            <label>
                                <input type="radio" name="discount-type" value="PERCENT"
                                ng-model="qcdc.discountRadio"
                                ng-change="qcdc.toggleDiscountCalc(qcdc.discountRadio);"/>
                                <spring:theme code="cat.quote.cart.item.percentage.discount" />
                                <span class="checkmark"></span>
                            </label>
                        </div>   
                    </div>
                    <div class="col-md-2 text-right"><spring:theme code="cat.quote.cart.item.transactionPrice" /></div>
                </div>
            </div>
            <article class="quote-cart-template" ng-repeat="(productIndex, cartProduct) in qcdc.cartProducts.entries" 
            ng-init="eppHelper=qcdc.cartProductInitialDataSetUp(cartProduct).eppHelper;
                    eppItems=eppHelper.eppItems;
                    csaHelper=qcdc.cartProductInitialDataSetUp(cartProduct).csaHelper;
                    csaItems=csaHelper.csaItems">
                <section class="row no-gutters config-container">
                    <div class="col-md-4 col-xs-12 prd-img-container">
                        <div class="col-md-12">
                            <h6 class="cart-hddng text-uppercase">{{cartProduct.product.categories[0].name}}</h6>
                            <div class="col-md-6 col-xs-4">
                                <img ng-src="{{cartProduct.product.images[0].url || '/_ui/responsive/cat/images/download_bkp.png'}}">
                            </div>
                            <div class="col-md-6 col-xs-8">
                                <div class="col-xs-12 font-type-2 primary-offering">
                                    <label>{{cartProduct.product.primaryOfferingName}}</label>
                                    <label>({{cartProduct.product.baseProduct}})</label>
                                </div>
                                <div class="col-xs-12">
                                    <label class="config-id"><spring:theme code="cat.quote.cart.item.configurationID" /></label>
                                    <label class="font-type-2 config-id-value">{{cartProduct.product.name}}</label>
                                </div>
                            </div>
                        </div>
                        <button class="visible-xs close-btn"></button>
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="row no-gutters">    
                            <div class="col-md-2 col-xs-12">
                                <div class="visible-xs prd-price-header col-xs-6">
                                    <spring:theme code="cat.quote.cart.item.listPricePerProduct" />
                                </div>
                                <div class="col-xs-6 col-md-12 no-padding font-type-1">{{cartProduct.basePrice.formattedValue}}</div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <div class="row no-gutters">
                                    <div class="col-xs-6 qty-header visible-xs">
                                        <spring:theme code="cat.quote.cart.item.quantity.camelCase" />
                                    </div>
                                    <div class="col-xs-6 col-md-12">
                                        <span class="decrement" <c:if test="${firstTimeEdit eq true or empty firstTimeEdit}"> 
                                            ng-click="qcdc.cartDecrement(cartProduct);qcdc.updateCartProducts({eppObj:eppHelper,csaObj:csaHelper,cartProd:cartProduct})" </c:if>> </span>
                                        <input class="qty text-center" ng-disabled="${not firstTimeEdit and not empty firstTimeEdit}" 
                                        restrict-model-input="^[0-9]+(\.[0-9]{0,2})?$" type="text" 
                                        ng-model="cartProduct.quantity" 
                                        ng-blur="qcdc.updateCartProducts({eppObj:eppHelper,csaObj:csaHelper,cartProd:cartProduct})"/>
                                        <span class="increment" <c:if test="${firstTimeEdit eq true or empty firstTimeEdit}"> 
                                            ng-click="qcdc.cartIncrement(cartProduct);qcdc.updateCartProducts({eppObj:eppHelper,csaObj:csaHelper,cartProd:cartProduct})" </c:if>></span>
                                        <div class="col-md-12 hidden-xs font-type-1 has-error" ng-if="cartProduct.errorData.qtyError" ng-bind="cartProduct.errorData.qtyErrorMsg"></div>
                                    </div>
                                </div> 
                                <div class="row no-gutters visible-xs has-error text-error" ng-if="cartProduct.errorData.qtyError" ng-bind="cartProduct.errorData.qtyErrorMsg">
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-12">
                                <div class="col-xs-6 discount-header visible-xs">
                                    <spring:theme code="cat.quote.cart.item.discount.camelCase" />
                                </div>
                                <div class="col-xs-6 no-padding col-md-12 font-type-10 hidden-xs">
                                    <label>
                                        <span class="discount-dollar" ng-class="{'disable':qcdc.discountRadio !== 'ABSOLUTE'}">
                                            <spring:theme code="cat.quote.cart.item.amount.symbol"/></span>
                                        <input type="text" class="discount-price prd-discount-price text-right"
                                        ng-model="cartProduct.discount.value" quant="{{cartProduct.quantity}}"
                                        max="{{qcdc.discountRadio === 'ABSOLUTE' ? (cartProduct.basePrice.formattedValue):'100%'}}" 
                                        restrict-model-input="^[0-9]+(\.[0-9]{0,2})?$"
                                        ng-blur="qcdc.calcDiscount(productIndex, qcdc.discountRadio, cartProduct.discount.value||0)"/>
                                        <span class="inactive" ng-class="{'disable':qcdc.discountRadio === 'ABSOLUTE'}">
                                            <spring:theme code="cat.quote.cart.item.percentage.symbol"/></span>
                                    </label>
                                </div>
                                <div class="col-xs-6 no-padding col-md-12 font-type-10 visible-xs mobile-discount-box text-right">
                                <label class="discount-radio-container">
                                    <span class="price-discount" ng-class='{"active-discount" : qcdc.discountRadio == "ABSOLUTE"}'>$</span>
                                    <input type="radio" value="ABSOLUTE" ng-model="qcdc.discountRadio" 
                                    ng-change="qcdc.toggleDiscountCalc(qcdc.discountRadio);" 
                                    name="discount-radio">
                                    <span class="checkmark"></span>
                                </label>
                                <input class="discount-box" quant="{{cartProduct.quantity}}" 
                                    max="{{qcdc.discountRadio === 'ABSOLUTE'?(cartProduct.basePrice.formattedValue):'100%'}}"
                                    restrict-model-input="^[0-9]+(\.[0-9]{0,2})?$"  type="text" 
                                    ng-value="{{cartProduct.discount===null?0:cartProduct.discount.value}}"
                                    ng-model = "cartProduct.discount.value"
                                    ng-blur="qcdc.calcDiscount($index,qcdc.discountRadio,cartProduct.discount.value)"/>
                                <label class="discount-radio-container">
                                    <span ng-class='{"active-discount" : qcdc.discountRadio != "ABSOLUTE"}'>%</span>
                                    <input type="radio" value="PERCENT" ng-model="qcdc.discountRadio" 
                                    name="discount-radio" ng-change="qcdc.toggleDiscountCalc(qcdc.discountRadio);">
                                    <span class="checkmark"></span>
                                </label>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-12 transaction-price-container">
                                <div class="visible-xs col-xs-6 trans-header"><spring:theme code="cat.quote.cart.item.transactionPrice.camelCase" /></div>
                                <div class="col-xs-6 col-md-12 font-type-1 trans-value">{{cartProduct.totalPrice.formattedValue}}</div>
                            </div>
                            <div class="col-md-3 hidden-xs">
                                <div>
                                    <button class="edit-reserve-btn text-right" ng-if="cartProduct.reservedFlag"
                                    ng-click="qcdc.showReservePopup(productIndex);">
                                        <span><spring:theme code="cat.quote.cart.item.edit" /></span>
                                        <span><spring:theme code="cat.quote.cart.item.reserve.text" /></span>
                                    </button>
                                    <button class="hidden-xs reserve-btn" ng-class="{'disable': cartProduct.errorData.disableReserveBtn}" ng-if="!cartProduct.reservedFlag"
                                    ng-click="qcdc.showReservePopup(productIndex);" ng-disabled="cartProduct.errorData.disableReserveBtn">
                                        <span><spring:theme code="cat.quote.cart.item.reserve.text" /></span>
                                    </button>
                                    <div ng-if="cartProduct.errorData.disableReserveBtn && !cartProduct.reservedFlag" ng-bind="cartProduct.errorData.reserveBtnErrorMsg" class="cat-mesagesError"></div>
                                    <button class="hidden-xs remove-btn" ng-hide="qcdc.isEditQuote" ng-click="qcdc.showRemovePopup($index);"><span><spring:theme code="cat.quote.cart.item.remove.reservation" /></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="row no-gutters serial-number-container" ng-if="cartProduct.reservedFlag">
                    <div class="col-md-8 col-md-push-2">
                        <h6 class="serial-number-header"><spring:theme code="cat.quote.cart.item.reserve.message" /><b>${warehouseName}</b>
                            <spring:theme code="cat.quote.cart.item.reserve.restMessage" /><b>{{qcdc.getDateFormat(cartProduct.endDate)}}</b>
                        </h6>
                        <label class="font-type-7 text-capitalize"><spring:theme code="cat.quote.cart.item.serialNumbers" /></label>
                        <div class="serial-number-blck">
                            <div class="serial-numbers" ng-repeat="serialNumber in cartProduct.reservedSerialNos">
                                {{serialNumber}}
                            </div>
                        </div>
                    </div>
                </section>
               
                <section class="row no-gutters epp-csa-container">
                        <div class="col-md-10 col-md-push-2">
                            <hr>
                            <a href="#"
                            ng-class="{'plus-icon':!cartProduct.isEppCollapsed,'minus-icon':cartProduct.isEppCollapsed}" 
                            class="epp-csa-optn" ng-init="cartProduct.isEppCollapsed=!!eppItems.length"
                            ng-click="cartProduct.isEppCollapsed = !cartProduct.isEppCollapsed;
                                        !eppItems.length?qcdc.addNewEppOption(eppItems,eppHelper):''">
                            <spring:theme code="cat.quote.cart.item.add.eppOption" /></a>
                            <div uib-collapse="!cartProduct.isEppCollapsed">
                                <section class="row no-gutters epp-container" ng-repeat="eppItem in eppItems track by $index">
                                        <div class="col-md-2 col-xs-12  epp-optn-hddng"><spring:theme code="cat.quote.cart.item.eppOption"/> {{$index+1}}</div>
                                        <div class="col-md-10 col-xs-12">
                                            <div class="btn-group cat-buttons drop-down-button dropdown open" uib-dropdown>
                                                <a href="" class="btn col-xs-12 drop-down-button dropdown-toggle" uib-dropdown-toggle>
                                                    <div class="text">{{eppItem.eppOption || qcdc.selectLabel}}</div>
                                                    <div class="arrow pull-right">
                                                        <img class="drop-down" alt="drop-down-caret" src="/_ui/responsive/cat/images/arrow-down.png">
                                                    </div>
                                                </a>
                                                <ul class="dropdown-menu col-xs-12" uib-dropdown-menu role="menu" aria-labelledby="sales-model">
                                                    <li role="menuitem" ng-repeat="(eppIndex, options) in cartProduct.eppOptions" 
                                                        ng-click="eppItem.setAttributeWithValue({isEppDisabled:!!0,eppOption:options})">
                                                        <a>{{options}}</a>
                                                    </li>
                                                    <li role="menuitem" ng-click="qcdc.showEppCsaPopup('EPP', productIndex,eppItem);">
                                                        <a class="dropdown-value"><spring:theme code="cat.quote.cart.item.add.new.eppOption" /></a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="row no-gutters epp-headers hidden-xs">
                                                <label class="col-md-2"><spring:theme code="cat.quote.cart.item.epp.price" /></label>
                                                <label class="col-md-2 text-center"><spring:theme code="cat.quote.cart.item.epp.quantity" /></label>
                                                <label class="col-md-3 text-center"><spring:theme code="cat.quote.cart.item.epp.total" /></label>
                                            </div>
                                            <div class="row no-gutters visible-xs">
                                                <label class="col-xs-5 epp-list-price"><spring:theme code="cat.quote.cart.item.epp.listPrice" /></label>
                                                <label class="col-xs-6 epp-qty"><spring:theme code="cat.quote.cart.item.epp.qty" /></label>
                                            </div>
                                            <div class="row no-gutters">
                                                <label class="col-md-2 col-xs-5 epp-list-price-label font-type-10">
                                                    $<input type="text" class="discount-price" name="EppPrice" 
                                                    ng-model="eppItem.basePrice" restrict-model-input="^[0-9]+(\.[0-9]{0,2})?$" 
                                                    ng-change="eppItem.setAttributeWithValue({isEppDisabled:!!0})"/>
                                                </label>
                                                <label class="col-md-2 col-xs-6 epp-qty-label text-center">
                                                    <span class="decrement" ng-click="eppItem.decrement(eppHelper);"></span>
                                                    <input class="qty text-center" restrict-to="[0-9]" type="text" ng-model="eppItem.qty"
                                                           name="EppQty" ng-change="eppItem.changeInEppQty(eppHelper);"/>
                                                    <span class="increment" ng-click="eppItem.increment(eppHelper);"></span>
                                                </label>
                                                <label class="col-md-3 col-xs-12 epp-total-label font-type-1 text-center">
                                                    <label class="visible-xs col-xs-2 no-padding"><spring:theme code="cat.quote.cart.item.epp.total" /></label>
                                                    {{eppItem.totalPrice}}
                                                </label>
                                                <div class="col-md-5 col-xs-12 clearfix">
                                                    <button class="btns-type-1 pull-left" 
                                                    ng-class="{'disable': eppItem.isApplyDisabled()}" 
                                                    ng-disabled="eppItem.isApplyDisabled()" 
                                                    ng-click="eppItem.isEppDisabled=true;qcdc.applyEppCsaFilters(eppItem.getPostJson(), productIndex);">
                                                    <spring:theme code="cat.quote.cart.item.button.apply" /></button>
                                                    <button class="btns-type-1 clear-btn"
                                                    ng-click="qcdc.clearEppCsaFilters(eppItem,eppHelper,cartProduct)">
                                                    <spring:theme code="general.delete.button"/></button>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                <button class="epp-csa-add-new-btn btn-active" ng-disabled="eppHelper.isNewBtnDisable" 
                                ng-class="{'disable':eppHelper.isNewBtnDisable}" ng-click="qcdc.addNewEppOption(eppItems,eppHelper)"><spring:theme code="cat.quote.cart.item.add.new.eppOption" /></button>
                                <hr>
                            </div>
                            <a href="#"
                            ng-class="{'plus-icon':!cartProduct.isCsaCollapsed,'minus-icon':cartProduct.isCsaCollapsed}" 
                            class="epp-csa-optn" ng-init="cartProduct.isCsaCollapsed=!!csaItems.length"
                            ng-click="cartProduct.isCsaCollapsed = !cartProduct.isCsaCollapsed;
                            !csaItems.length?qcdc.addNewCsaOption(csaItems,csaHelper):''">
                            <spring:theme code="cat.quote.cart.item.add.csaOption" /></a>
                            <div uib-collapse="!cartProduct.isCsaCollapsed">
                                <section class="row no-gutters epp-csa-container" ng-repeat="csaItem in csaItems track by $index">   
                                    <div class="col-md-2 font-type-8 epp-optn-hddng"><spring:theme code="cat.quote.cart.item.csaOption" /> {{$index+1}}</div>
                                    <div class="col-md-10">
                                        <div class="btn-group cat-buttons drop-down-button dropdown open" uib-dropdown="">
                                            <a href="" class="btn col-xs-12 drop-down-button dropdown-toggle" uib-dropdown-toggle>
                                                <div class="text">{{csaItem.csaOption || qcdc.selectLabel}}</div>
                                                <div class="arrow pull-right">
                                                    <img class="drop-down" alt="drop-down-caret" src="/_ui/responsive/cat/images/arrow-down.png">
                                                </div>
                                            </a>
                                            <ul class="dropdown-menu col-xs-12" uib-dropdown-menu role="menu" aria-labelledby="sales-model">
                                                <li role="menuitem" ng-repeat="(csaIndex, options) in cartProduct.csaOptions" 
                                                ng-click="csaItem.setAttributeWithValue({csaOption:options,isCsaDisabled:!!0})">
                                                    <a>{{options}}</a>
                                                </li>
                                                <li role="menuitem" ng-click="qcdc.showEppCsaPopup('CSA', productIndex,csaItem);">
                                                    <a class="dropdown-value"><spring:theme code="cat.quote.cart.item.add.new.csaOption" /></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="row no-gutters epp-headers hidden-xs">
                                            <label class="col-md-2"><spring:theme code="cat.quote.cart.item.csa.hours" /></label>
                                            <label class="col-md-2 text-center"><spring:theme code="cat.quote.cart.item.csa.years" /></label>
                                        </div>
                                        <div class="row no-gutters">
                                            <label class="col-md-2 col-xs-5 csa-hrs-label">
                                                <span class="col-xs-12 visible-xs no-padding"><spring:theme code="cat.quote.cart.item.csa.hours" /></span>
                                                <input type="text" class="csa-hrs" name="CsaHrs" restrict-model-input="^[0-9]+(\.[0-9]{0,2})?$" 
                                                ng-model="csaItem.hours" 
                                                ng-change="csaItem.setAttributeWithValue({isCsaDisabled:!!0})"/>
                                            </label>
                                            <label class="col-md-2 col-xs-6 csa-yrs-label text-center">
                                                <span class="col-xs-12 visible-xs no-padding"><spring:theme code="cat.quote.cart.item.csa.years" /></span>
                                                <input type="text" class="csa-yrs" restrict-to="[0-9]" name="CsaYrs" 
                                                ng-model="csaItem.years" 
                                                ng-change="csaItem.setAttributeWithValue({isCsaDisabled:!!0})"/>
                                            </label>
                                        </div>
                                        <div class="row no-gutters epp-headers hidden-xs">
                                            <label class="col-md-2"><spring:theme code="cat.quote.cart.item.epp.price" /></label>
                                            <label class="col-md-2 text-center"><spring:theme code="cat.quote.cart.item.epp.quantity" /></label>
                                            <label class="col-md-3 text-center"><spring:theme code="cat.quote.cart.item.epp.total" /></label>
                                        </div>
                                        <div class="row no-gutters visible-xs">
                                            <label class="col-xs-5 epp-list-price"><spring:theme code="cat.quote.cart.item.epp.listPrice" /></label>
                                            <label class="col-xs-6 epp-qty"><spring:theme code="cat.quote.cart.item.epp.qty" /></label>
                                        </div>
                                        <div class="row no-gutters">
                                            <label class="col-md-2 col-xs-5 epp-list-price-label font-type-10">
                                                $<input type="text" class="discount-price" 
                                                name="CsaPrice" restrict-model-input="^[0-9]+(\.[0-9]{0,2})?$" 
                                                ng-model="csaItem.basePrice" 
                                                ng-change="csaItem.setAttributeWithValue({isCsaDisabled:!!0})"/>
                                            </label>
                                            <label class="col-md-2 col-xs-6 epp-qty-label text-center">
                                                <span class="decrement" ng-click="csaItem.decrement(csaHelper);"></span>
                                                <input class="qty text-center" type="text" restrict-to="[0-9]" 
                                                       name="CsaQty" ng-model="csaItem.qty" ng-change="csaItem.changeInCsaQty(csaHelper);"/>
                                                <span class="increment" ng-click="csaItem.increment(csaHelper);"></span>
                                            </label>
                                            <label class="col-md-3 col-xs-12 epp-total-label font-type-1 text-center">
                                                <label class="visible-xs col-xs-2 no-padding"><spring:theme code="cat.quote.cart.item.epp.total" /></label>
                                                {{csaItem.totalPrice}}
                                            </label>
                                            <div class="col-md-5 col-xs-12 clearfix">
                                                    <button class="btns-type-1 pull-left" 
                                                    ng-class="{'disable': csaItem.isApplyDisabled()}" 
                                                    ng-disabled="csaItem.isApplyDisabled()" 
                                                    ng-click="csaItem.isCsaDisabled=true;qcdc.applyEppCsaFilters(csaItem.getPostJson(),productIndex);">
                                                    <spring:theme code="cat.quote.cart.item.button.apply" /></button>
                                                    <button class="btns-type-1 clear-btn" 
                                                     ng-click="qcdc.clearEppCsaFilters(csaItem,csaHelper,cartProduct)">
                                                    <spring:theme code="general.delete.button"/></button>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <button class="epp-csa-add-new-btn btn-active" 
                                ng-click="qcdc.addNewCsaOption(csaItems,csaHelper)" ng-disabled="csaHelper.isNewBtnDisable" ng-class="{'disable':csaHelper.isNewBtnDisable}">
                                        <spring:theme code="cat.quote.cart.item.add.new.csaOption"></spring:theme>
                                    </button>
                                <hr>
                            </div>
                            <a href="#"
                            ng-class="{'plus-icon':!cartProduct.isVetCollapsed,'minus-icon':cartProduct.isVetCollapsed}" 
                            class="epp-csa-optn add-value-section"
                            ng-if="cartProduct.baseVETLink"
                            ng-click="cartProduct.isVetCollapsed = !cartProduct.isVetCollapsed">
                            Add Value Estimation</a>
                            <div uib-collapse="!cartProduct.isVetCollapsed" class="vet-section">
                                <div class="value-analysis-section">
                                    <div class="row va-item" ng-repeat="(vetIndex, vetData) in cartProduct.valueAnalysis"> 
                                        <div class="col-xs-12 col-md-11">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="va-title">Value Analysis {{vetIndex + 1}}</div>
                                                    <div ng-if="vetData.isEditing">
                                                        <input type="text" placeholder="Enter competitor's name and sales model" ng-model="vetData.vetProduct">
                                                        <input type="text" placeholder="Paste the 'saved URL' from VET Lite" ng-model="vetData.vetLink" ng-class="{'error' : vetData.hasError}">
                                                    </div>
                                                    <div ng-if="!vetData.isEditing">
                                                        <div class="vet-model-name">{{vetData.vetProduct}}</div>
                                                        <div class="vet-url">{{vetData.vetLink}}</div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="row va-item-border">
                                                        <div class="col-md-8 va-left">
                                                            <div class="error-msg" ng-if="vetData.hasError">
                                                                <span>This Competitve study URL has already been used.</span>
                                                                <span>Please enter a new URL to proceed</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 va-right">
                                                            <div class="buttons-section">
                                                                <button class="btns-type-1" ng-if="!vetData.isEditing" ng-class="{'disable' : cartProduct.isVetEditing}" ng-disabled="cartProduct.isVetEditing" ng-click="qcdc.editValueAnalysis(productIndex, vetIndex);">edit</button>
                                                                <button class="btns-type-1" ng-if="vetData.isEditing" ng-class="{'disable' : (!vetData.vetProduct || !vetData.vetLink)}" ng-disabled="(!vetData.vetProduct || !vetData.vetLink)" ng-click="qcdc.updateValueAnalysis(productIndex, vetIndex);">save</button>
                                                                <button class="btns-type-1 remove" ng-if="!vetData.isEditing" ng-click="qcdc.removeValueAnalysis(productIndex, vetIndex)">remove</button>
                                                                <button class="btns-type-1 remove" ng-if="vetData.isEditing" ng-click="qcdc.cancelValueAnalysis(productIndex, vetIndex)">cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="row" ng-if="cartProduct.valueAnalysis.length && !cartProduct.showAddVET">
                                        <div class="col-xs-12">
                                            <div class="add-va-item-link">
                                                <div class="upperCase">Want to conduct multiple analysis?</div>
                                                <div class="item-link" ng-click="qcdc.addMoreValueAnalysis(productIndex);">+ Add another link to the quote</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" ng-if="cartProduct.showAddVET || !cartProduct.valueAnalysis.length">
                                        <div class="col-md-8 vet-lite-description">Compare the <span>Caterpillar {{cartProduct.product.salesModelName}}</span> Sales Model with a competitor's product on <span>VET Lite</span></div>
                                        <div class="col-md-3"><button class="vet-lite-button upperCase" ng-class="{'disable' : cartProduct.isVetEditing}" ng-disabled="cartProduct.isVetEditing" ng-click="qcdc.showVetLitePopUp(productIndex);">Add Value Analysis</button></div>
                                    </div>
                                    <div class="row va-item" ng-if="cartProduct.vetData"> 
                                        <div class="col-xs-12 col-md-11">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div class="va-title">Value Analysis {{cartProduct.valueAnalysis.length + 1}}</div>
                                                    <input type="text" placeholder="Enter competitor's name and sales model" ng-model="cartProduct.vetData.vetProduct">
                                                    <input type="text" placeholder="Paste the 'saved URL' from VET Lite" ng-model="cartProduct.vetData.vetLink" ng-class="{'error' : cartProduct.vetData.hasError}">
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="row va-item-border">
                                                        <div class="col-md-8 va-left">
                                                            <div class="error-msg" ng-if="cartProduct.vetData.hasError">
                                                                <span>This Competitve study URL has already been used.</span>
                                                                <span>Please enter a new URL to proceed</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 va-right">
                                                            <div class="buttons-section">
                                                                <button class="btns-type-1" ng-class="{'disable' : (!cartProduct.vetData.vetProduct || !cartProduct.vetData.vetLink)}" ng-disabled="(!cartProduct.vetData.vetProduct || !cartProduct.vetData.vetLink)" ng-click="qcdc.addValueAnalysis(productIndex);">add</button>
                                                                <button class="btns-type-1 remove" ng-click="qcdc.cancelValueAnalysis(productIndex);">remove</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div> 
                </section>
                <div class="row no-gutters prdct-mobile-btns-container">
                    <button class="visible-xs prdct-mobile-btns" ng-if="cartProduct.reservedFlag"
                                    ng-click="qcdc.showReservePopup(productIndex);"
                    ><spring:theme code="cat.quote.cart.item.editReservation.text" /></button>
                </div>
                <div class="row no-gutters prdct-mobile-btns-container">
                    <button class="visible-xs prdct-mobile-btns" ng-class="{'disable': cartProduct.errorData.disableReserveBtn}" ng-if="!cartProduct.reservedFlag"
                    ng-click="qcdc.showReservePopup(productIndex);" ng-disabled="cartProduct.errorData.disableReserveBtn"><spring:theme code="cat.quote.cart.item.reserve.text" /></button>
                </div>
                <div ng-if="cartProduct.errorData.disableReserveBtn && !cartProduct.reservedFlag" ng-bind="cartProduct.errorData.reserveBtnErrorMsg" class="text-center cat-mesagesError mb-msg-err visible-xs"></div>
            </article>
            <div class="row hidden-xs text-right text-uppercase no-gutters total-price">
                <div class="col-md-8 price-header no-padding"><spring:theme code="cat.quote.cart.totalListPrice" /></div>
                <div class="col-md-2 price-value">{{ qcdc.totalListPrice | currency }}</div>
                <div class="col-md-8 price-header"><spring:theme code="cat.quote.cart.totalDiscount" /></div>
                <div class="col-md-2 price-value">{{qcdc.cartProducts.totalDiscounts.formattedValue}}</div>
            </div>

            <div class="row hidden-xs no-gutters text-right text-uppercase transaction-price">
                <div class="col-md-8 price-header no-padding"><spring:theme code="cat.quote.cart.totalTransactionPrice" /></div>
                <div class="col-md-2 price-value">{{qcdc.cartProducts.totalPrice.formattedValue}}</div>
            </div>
        </article>
    </div> 
    <div class="row visible-xs text-uppercase no-gutters total-price">
        <div class="row no-gutters">
            <div class="col-xs-6 price-header text-left no-padding"><spring:theme code="cat.quote.cart.totalListPrice" /></div>
            <div class="col-xs-6 text-right price-value">{{ qcdc.totalListPrice | currency }}</div>
        </div>
        <div class="row no-gutters">
            <div class="col-xs-6 text-left price-header"><spring:theme code="cat.quote.cart.totalDiscount" /></div>
            <div class="col-xs-6 text-right price-value">{{qcdc.cartProducts.totalDiscounts.formattedValue}}</div>
        </div>
    </div>

    <div class="row visible-xs no-gutters  text-uppercase transaction-price">
        <div class="col-xs-6 price-header text-left no-padding"><spring:theme code="cat.quote.cart.totalTransactionPrice" /></div>
        <div class="col-xs-6 text-right price-value">{{qcdc.cartProducts.totalPrice.formattedValue}}</div>
    </div>     
</div>