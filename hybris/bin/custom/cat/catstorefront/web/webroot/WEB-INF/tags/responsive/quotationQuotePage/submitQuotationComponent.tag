<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


 <div class="quotation-submit-container cat-container row" ng-controller="quotationSubmitController as qsc" ng-cloak ng-hide="qsc.view.quoteMetaData.mobileMetaData.isMobileScreen && qsc.view.quoteMetaData.mobileNav.currentPageNum!=3">
    <div class="col-xs-12 col-md-12 no-padding indicates-required">
        <span class="asterik pull-left"><spring:theme code="cat.quote.asterisk.symbol"/></span><label class="pull-left"><spring:theme code="cat.quote.mandatory.fields" /></label>
    </div>
    <div class="col-xs-12 col-md-12 no-padding" ng-if="!qsc.view.quoteMetaData.editQuote">
        <button class="btn cat-buttons primary-btn submit-quote-btn pull-right activate-mobile-buttons" ng-click="qsc.submitQuote();"><spring:theme code="cat.quote.button.submit" /></button>
    </div>


     <div class="col-xs-12 col-md-12 no-padding" ng-if="qsc.view.quoteMetaData.editQuote">
     		<div class="pull-right">
	     	<button class="btn cat-buttons secondary-btn download-quote-btn activate-mobile-buttons" ng-disabled="true">	<spring:theme code="cat.quote.button.exportDocument" /></button>
	        <button class="btn cat-buttons primary-btn submit-quote-btn activate-mobile-buttons" ng-click="qsc.submitQuote();">
	        <c:choose>
	        	<c:when test="${firstTimeEdit}">
	        		<spring:theme code="cat.quote.button.submit" />
	        	</c:when>
	        	<c:otherwise>
	        		<spring:theme code="cat.quote.button.save" />
	        	</c:otherwise>
	        </c:choose>
	        </button>
        </div>
    </div>

 </div>