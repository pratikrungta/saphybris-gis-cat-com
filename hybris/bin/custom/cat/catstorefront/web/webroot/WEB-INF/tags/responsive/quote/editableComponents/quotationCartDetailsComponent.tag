<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<div class="quotation-cart-container cat-container row" ng-controller="quoteCartDetailsController as qcdc" ng-init="qcdc.loadCartProducts('${media_prefix_domain}','${cartData.quoteData.code}','${firstTimeEdit}', '${isReplicateQuote}');" ng-cloak ng-hide="qcdc.view.quoteMetaData.mobileMetaData.isMobileScreen && qcdc.view.quoteMetaData.mobileNav.currentPageNum!=2">
    <div class="quote-content-container">
        <div class="row no-gutters cart-header-section">
            <span><spring:theme code="cat.quote.cart.details.heading.text" /></span>
        </div>
        <article class="quote-cart-template-container">
            <div class="row no-gutters quote-cart-header hidden-xs">
                <div class="col-md-4">
                    <spring:theme code="cat.quote.cart.products.heading.text" />
                </div>
                <div class="col-md-8">
                    <div class="col-md-2"><spring:theme code="cat.quote.cart.item.listPrice" /></div>
                    <div class="col-md-3"><spring:theme code="cat.quote.cart.item.quantity" /><spring:theme code="cat.quote.asterisk.symbol"/></div>
                    <div class="col-md-2 discount-header"><spring:theme code="cat.quote.cart.item.discount" />
                        <div class="discount-container">    
                            <label>
                                <input type="radio" name="discount-type" value="ABSOLUTE" 
                                ng-model="qcdc.discountRadio"
                                ng-change="qcdc.toggleDiscountCalc(qcdc.discountRadio);"/>
                                <spring:theme code="cat.quote.cart.item.amount.discount" />
                                <span class="checkmark"></span>
                            </label>
                            <label>
                                <input type="radio" name="discount-type" value="PERCENT"
                                ng-model="qcdc.discountRadio"
                                ng-change="qcdc.toggleDiscountCalc(qcdc.discountRadio);"/>
                                <spring:theme code="cat.quote.cart.item.percentage.discount" />
                                <span class="checkmark"></span>
                            </label>
                        </div>   
                    </div>
                    <div class="col-md-2 text-right"><spring:theme code="cat.quote.cart.item.transactionPrice" /></div>
                </div>
            </div>
            <article class="quote-cart-template" ng-repeat="(productIndex, cartProduct) in qcdc.cartProducts.entries">
                <section class="row no-gutters config-container">
                    <div class="col-md-4 col-xs-12 prd-img-container">
                        <div class="col-md-12">
                            <h6 class="cart-hddng text-uppercase">{{cartProduct.product.categories[0].name}}</h6>
                            <div class="col-md-6 col-xs-4">
                                <img ng-src="{{cartProduct.product.images[0].url || '/_ui/responsive/cat/images/download_bkp.png'}}">
                            </div>
                            <div class="col-md-6 col-xs-8">
                                <div class="col-xs-12 font-type-2 primary-offering">
                                    <label>{{cartProduct.product.primaryOfferingName}}</label>
                                    <label>({{cartProduct.product.baseProduct}})</label>
                                </div>
                                <div class="col-xs-12">
                                    <label class="config-id"><spring:theme code="cat.quote.cart.item.configurationID" /></label>
                                    <label class="font-type-2 config-id-value">{{cartProduct.product.name}}</label>
                                </div>
                            </div>
                        </div>
                        <button class="visible-xs close-btn"></button>
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="row no-gutters">    
                            <div class="col-md-2 col-xs-12">
                                <div class="visible-xs prd-price-header col-xs-6">
                                    <spring:theme code="cat.quote.cart.item.listPricePerProduct" />
                                </div>
                                <div class="col-xs-6 col-md-12 no-padding font-type-1">{{cartProduct.basePrice.formattedValue}}</div>
                            </div>
                            <div class="col-md-3 col-xs-12">
                                <div class="row no-gutters">
                                    <div class="col-xs-6 qty-header visible-xs">
                                        <spring:theme code="cat.quote.cart.item.quantity.camelCase" /><spring:theme code="cat.quote.asterisk.symbol"/>
                                    </div>
                                    <div class="col-xs-6 col-md-12">
                                        <span class="decrement" <c:if test="${firstTimeEdit eq true or empty firstTimeEdit}"> ng-click="qcdc.updateCartProducts(productIndex,'decrement')" </c:if>> </span>
                                        <input class="qty text-center" ng-disabled="${not firstTimeEdit and not empty firstTimeEdit}" 
                                        restrict-model-input="^[0-9]+(\.[0-9]{0,2})?$" type="text" 
                                        ng-model="cartProduct.quantity" 
                                        ng-blur="qcdc.updateCartProducts(productIndex,'blur');"/>
                                        <span class="increment" <c:if test="${firstTimeEdit eq true or empty firstTimeEdit}"> ng-click="qcdc.updateCartProducts(productIndex,'increment')" </c:if>></span>
                                        <div class="col-md-12 hidden-xs font-type-1 has-error" ng-if="cartProduct.errorData.qtyError" ng-bind="cartProduct.errorData.qtyErrorMsg"></div>
                                    </div>
                                </div> 
                                <div class="row no-gutters visible-xs has-error text-error" ng-if="cartProduct.errorData.qtyError" ng-bind="cartProduct.errorData.qtyErrorMsg">
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-12">
                                <div class="col-xs-6 discount-header visible-xs">
                                    <spring:theme code="cat.quote.cart.item.discount.camelCase" />
                                </div>
                                <div class="col-xs-6 no-padding col-md-12 font-type-10 hidden-xs">
                                    <label>
                                        <span class="discount-dollar" ng-if="qcdc.discountRadio === 'ABSOLUTE'"><spring:theme code="cat.quote.cart.item.amount.symbol"/></span>
                                        <input type="text" class="discount-price prd-discount-price text-right"
                                        ng-model="cartProduct.discount.value" quant="{{cartProduct.quantity}}"
                                        max="{{qcdc.discountRadio === 'ABSOLUTE' ? (cartProduct.basePrice.formattedValue):'100%'}}" 
                                        restrict-model-input="^[0-9]+(\.[0-9]{0,2})?$"
                                        ng-blur="qcdc.calcDiscount(productIndex, qcdc.discountRadio, cartProduct.discount.value||0)"/>
                                        <span class="inactive" ng-if="qcdc.discountRadio !== 'ABSOLUTE'"><spring:theme code="cat.quote.cart.item.percentage.symbol"/></span>
                                    </label>
                                </div>
                                <div class="col-xs-6 no-padding col-md-12 font-type-10 visible-xs mobile-discount-box text-right">
                                <label class="discount-radio-container">
                                    <span class="price-discount" ng-class='{"active-discount" : qcdc.discountRadio == "ABSOLUTE"}'>$</span>
                                    <input type="radio" value="ABSOLUTE" ng-model="qcdc.discountRadio" 
                                    
                                    ng-change="qcdc.toggleDiscountCalc(qcdc.discountRadio);" 
                                    name="discount-radio">
                                    <span class="checkmark"></span>
                                </label>
                                <input class="discount-box" quant="{{cartProduct.quantity}}" 
                                    max="{{qcdc.discountRadio === 'ABSOLUTE'?(cartProduct.basePrice.formattedValue):'100%'}}"
                                    restrict-model-input="^[0-9]+(\.[0-9]{0,2})?$"  type="text" 
                                    ng-value="{{cartProduct.discount===null?0:cartProduct.discount.value}}"
                                    ng-model = "cartProduct.discount.value"
                                    ng-blur="qcdc.calcDiscount($index,qcdc.discountRadio,cartProduct.discount.value)"/>
                                <label class="discount-radio-container">
                                    <span ng-class='{"active-discount" : qcdc.discountRadio != "ABSOLUTE"}'>%</span>
                                    <input type="radio" value="PERCENT" ng-model="qcdc.discountRadio" 
                                    name="discount-radio" ng-change="qcdc.toggleDiscountCalc(qcdc.discountRadio);">
                                    <span class="checkmark"></span>
                                </label>
                                </div>
                            </div>
                            <div class="col-md-2 col-xs-12 transaction-price-container">
                                <div class="visible-xs col-xs-6 trans-header"><spring:theme code="cat.quote.cart.item.transactionPrice.camelCase" /></div>
                                <div class="col-xs-6 col-md-12 font-type-1 trans-value">{{cartProduct.totalPrice.formattedValue}}</div>
                            </div>
                            <div class="col-md-3 hidden-xs">
                                <div>
                                    <button class="edit-reserve-btn text-right" ng-class="{'disable': cartProduct.errorData.disableReserveBtn}" ng-if="cartProduct.reservedFlag"
                                    ng-click="qcdc.showReservePopup(productIndex);" ng-disabled="cartProduct.errorData.disableReserveBtn">
                                        <span><spring:theme code="cat.quote.cart.item.edit" /></span>
                                        <span><spring:theme code="cat.quote.cart.item.reservation" /></span>
                                    </button>
                                    <button class="hidden-xs reserve-btn" ng-class="{'disable': cartProduct.errorData.disableReserveBtn}" ng-if="!cartProduct.reservedFlag"
                                    ng-click="qcdc.showReservePopup(productIndex);" ng-disabled="cartProduct.errorData.disableReserveBtn">
                                        <span><spring:theme code="cat.quote.cart.item.reserve.text" /></span>
                                    </button>
                                    <div ng-if="cartProduct.errorData.disableReserveBtn" ng-bind="cartProduct.errorData.reserveBtnErrorMsg" class="cat-mesagesError"></div>
                                    <button class="hidden-xs remove-btn" ng-hide="qcdc.isEditQuote" ng-click="qcdc.showRemovePopup($index);"><span><spring:theme code="cat.quote.cart.item.remove.reservation" /></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="row no-gutters serial-number-container" ng-if="cartProduct.reservedFlag">
                    <div class="col-md-8 col-md-push-2">
                        <h6 class="serial-number-header"><spring:theme code="cat.quote.cart.item.reserve.message" /><b>${warehouseName}</b>
                            <spring:theme code="cat.quote.cart.item.reserve.restMessage" /><b>{{qcdc.getDateFormat(cartProduct.endDate)}}</b>
                        </h6>
                        <label class="font-type-7 text-capitalize"><spring:theme code="cat.quote.cart.item.serialNumbers" /></label>
                        <div class="serial-number-blck">
                            <div class="serial-numbers" ng-repeat="serialNumber in cartProduct.reservedSerialNos">
                                {{serialNumber}}
                            </div>
                        </div>
                    </div>
                </section>
                <section class="row no-gutters epp-csa-container">
                    <div class="col-md-10 col-md-push-2">
                        <hr>
                        <a href=""
                        ng-class="{'plus-icon':!cartProduct.isCollapsed,'minus-icon':cartProduct.isCollapsed}" 
                        class="epp-csa-optn"
                        ng-click="cartProduct.isCollapsed = !cartProduct.isCollapsed"><spring:theme code="cat.quote.cart.item.add.eppCSAOption" /></a>
                        <div uib-collapse="!cartProduct.isCollapsed" class="">
                            <section class="row no-gutters epp-container">
                                <div class="col-md-2 col-xs-12  epp-optn-hddng"><spring:theme code="cat.quote.cart.item.eppOption" /><spring:theme code="cat.quote.asterisk.symbol"/></div>
                                <div class="col-md-10 col-xs-12">
                                    <div class="btn-group cat-buttons drop-down-button dropdown open" uib-dropdown>
                                        <a href="" class="btn col-xs-12 drop-down-button dropdown-toggle" uib-dropdown-toggle>
                                            <div class="text">{{cartProduct.eppLabel || qcdc.selectLabel}}</div>
                                            <div class="arrow pull-right">
                                                <img class="drop-down" alt="drop-down-caret" src="/_ui/responsive/cat/images/arrow-down.png">
                                            </div>
                                        </a>
                                        <ul class="dropdown-menu col-xs-12" uib-dropdown-menu role="menu" aria-labelledby="sales-model">
                                            <li role="menuitem" ng-repeat="(eppIndex, options) in cartProduct.eppOptions" ng-click="qcdc.updateEppDropdown(productIndex, options);">
                                                <a>{{options}}</a>
                                            </li>
                                            <li role="menuitem" ng-click="qcdc.showEppCsaPopup('EPP', productIndex);">
                                                <a class="dropdown-value"><spring:theme code="cat.quote.cart.item.add.new.eppOption" /></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="row no-gutters epp-headers hidden-xs">
                                        <label class="col-md-2"><spring:theme code="cat.quote.cart.item.epp.price" /><spring:theme code="cat.quote.asterisk.symbol"/></label>
                                        <label class="col-md-2 text-center"><spring:theme code="cat.quote.cart.item.epp.quantity" /><spring:theme code="cat.quote.asterisk.symbol"/></label>
                                        <label class="col-md-3 text-center"><spring:theme code="cat.quote.cart.item.epp.total" /></label>
                                    </div>
                                    <div class="row no-gutters visible-xs">
                                        <label class="col-xs-5 epp-list-price"><spring:theme code="cat.quote.cart.item.epp.listPrice" /><spring:theme code="cat.quote.asterisk.symbol"/></label>
                                        <label class="col-xs-6 epp-qty"><spring:theme code="cat.quote.cart.item.epp.qty" /><spring:theme code="cat.quote.asterisk.symbol"/></label>
                                    </div>
                                    <div class="row no-gutters">
                                        <label class="col-md-2 col-xs-5 epp-list-price-label font-type-10">
                                            $<input type="text" class="discount-price" restrict-model-input="^[0-9]+(\.[0-9]{0,2})?$" name="EppPrice" ng-model="cartProduct.entries[0].basePrice.value" ng-change="qcdc.checkEppCsaChange(false,productIndex)" ng-blur="qcdc.reinitializeEPPCSA($event,productIndex)"/>
                                        </label>
                                        <label class="col-md-2 col-xs-6 epp-qty-label text-center">
                                            <span class="decrement" ng-click="qcdc.updateEppCsaCounter(productIndex, 'decrement');"></span>
                                            <input class="qty text-center" restrict-to="[0-9]" type="text" data-index="{{productIndex}}" name="EppQty" ng-keyup="qcdc.Prevent($event,productIndex)" ng-model="cartProduct.entries[0].quantity" ng-change="qcdc.checkEppCsaChange(false,productIndex)" ng-blur="qcdc.reinitializeEPPCSA($event,productIndex); qcdc.updateEppCsaCounter(productIndex, 'blur')"/>
                                            <span class="increment" ng-click="qcdc.updateEppCsaCounter(productIndex, 'increment');"></span>
                                        </label>
                                        <label class="col-md-3 col-xs-12 epp-total-label font-type-1 text-center">
                                            <label class="visible-xs col-xs-2 no-padding"><spring:theme code="cat.quote.cart.item.epp.total" /></label>
                                            {{cartProduct.entries[0].totalPrice.formattedValue}}
                                        </label>
                                        <div class="col-md-5 col-xs-12 clearfix">
                                            <button class="btns-type-1 clear-btn pull-left" ng-class="{'btns-disable': !qcdc.isApplyEnable(productIndex)}" ng-disabled="!qcdc.isApplyEnable(productIndex)" ng-click="qcdc.clearEppCsaFilters(cartProduct.entries[0], productIndex);"><spring:theme code="cat.quote.cart.item.button.clear" /></button>
                                            <button class="btns-type-1" ng-class="{'btns-disable': !qcdc.isApplyEnable(productIndex, true)}" ng-disabled="!qcdc.isApplyEnable(productIndex, true)" ng-click="qcdc.applyEppCsaFilters(cartProduct.entries[0], productIndex);"><spring:theme code="cat.quote.cart.item.button.apply" /></button>
                                            <div>{{vm.cartProducts.entries[productIndex].entries[0].eppApplyEnable}}</div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <hr>
                            <section class="row no-gutters epp-csa-container">   
                                <div class="col-md-2 font-type-8 epp-optn-hddng"><spring:theme code="cat.quote.cart.item.csaOption" /><spring:theme code="cat.quote.asterisk.symbol"/></div>
                                <div class="col-md-10">
                                    <div class="btn-group cat-buttons drop-down-button dropdown open" uib-dropdown="">
                                        <a href="" class="btn col-xs-12 drop-down-button dropdown-toggle" uib-dropdown-toggle>
                                            <div class="text">{{cartProduct.csaLabel || qcdc.selectLabel}}</div>
                                            <div class="arrow pull-right">
                                                <img class="drop-down" alt="drop-down-caret" src="/_ui/responsive/cat/images/arrow-down.png">
                                            </div>
                                        </a>
                                        <ul class="dropdown-menu col-xs-12" uib-dropdown-menu role="menu" aria-labelledby="sales-model">
                                            <li role="menuitem" ng-repeat="(csaIndex, options) in cartProduct.csaOptions" ng-click="qcdc.updateCsaDropdown(productIndex, options);">
                                                <a>{{options}}</a>
                                            </li>
                                            <li role="menuitem" ng-click="qcdc.showEppCsaPopup('CSA', productIndex);">
                                                <a class="dropdown-value"><spring:theme code="cat.quote.cart.item.add.new.csaOption" /></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="row no-gutters epp-headers hidden-xs">
                                        <label class="col-md-2"><spring:theme code="cat.quote.cart.item.csa.hours" /><spring:theme code="cat.quote.asterisk.symbol"/></label>
                                        <label class="col-md-2 text-center"><spring:theme code="cat.quote.cart.item.csa.years" /><spring:theme code="cat.quote.asterisk.symbol"/></label>
                                    </div>
                                    <div class="row no-gutters">
                                        <label class="col-md-2 col-xs-5 csa-hrs-label">
                                            <span class="col-xs-12 visible-xs no-padding"><spring:theme code="cat.quote.cart.item.csa.hours" /><spring:theme code="cat.quote.asterisk.symbol"/></span>
                                            <input type="text" class="csa-hrs" name="CsaHrs" restrict-model-input="^[0-9]+(\.[0-9]{0,2})?$" ng-model="cartProduct.entries[1].configurationInfos[0].configurationValue" ng-change="qcdc.checkEppCsaChange(true,productIndex)" ng-blur="qcdc.reinitializeEPPCSA($event,productIndex)"/>
                                        </label>
                                        <label class="col-md-2 col-xs-6 csa-yrs-label text-center">
                                            <span class="col-xs-12 visible-xs no-padding"><spring:theme code="cat.quote.cart.item.csa.years" /><spring:theme code="cat.quote.asterisk.symbol"/></span>
                                            <input type="text" class="csa-yrs" restrict-to="[0-9]" name="CsaYrs" ng-model="cartProduct.entries[1].configurationInfos[2].configurationValue" ng-change="qcdc.checkEppCsaChange(true,productIndex)" ng-blur="qcdc.reinitializeEPPCSA($event,productIndex)"/>
                                        </label>
                                    </div>
                                    <div class="row no-gutters epp-headers hidden-xs">
                                        <label class="col-md-2"><spring:theme code="cat.quote.cart.item.epp.price" /><spring:theme code="cat.quote.asterisk.symbol"/></label>
                                        <label class="col-md-2 text-center"><spring:theme code="cat.quote.cart.item.epp.quantity" /><spring:theme code="cat.quote.asterisk.symbol"/></label>
                                        <label class="col-md-3 text-center"><spring:theme code="cat.quote.cart.item.epp.total" /></label>
                                    </div>
                                    <div class="row no-gutters visible-xs">
                                        <label class="col-xs-5 epp-list-price"><spring:theme code="cat.quote.cart.item.epp.listPrice" /><spring:theme code="cat.quote.asterisk.symbol"/></label>
                                        <label class="col-xs-6 epp-qty"><spring:theme code="cat.quote.cart.item.epp.qty" /><spring:theme code="cat.quote.asterisk.symbol"/></label>
                                    </div>
                                    <div class="row no-gutters">
                                        <label class="col-md-2 col-xs-5 epp-list-price-label font-type-10">
                                            $<input type="text" class="discount-price" name="CsaPrice" restrict-model-input="^[0-9]+(\.[0-9]{0,2})?$" ng-model="cartProduct.entries[1].basePrice.value" ng-change="qcdc.checkEppCsaChange(true,productIndex)" ng-blur="qcdc.reinitializeEPPCSA($event,productIndex)"/>
                                        </label>
                                        <label class="col-md-2 col-xs-6 epp-qty-label text-center">
                                            <span class="decrement" ng-click="qcdc.updateEppCsaCounter(productIndex, 'decrement', true);"></span>
                                            <input class="qty text-center" type="text" restrict-to="[0-9]" data-index="{{productIndex}}" name="CsaQty" ng-keyup="qcdc.Prevent($event,productIndex)" ng-model="cartProduct.entries[1].quantity" ng-change="qcdc.checkEppCsaChange(true,productIndex)" ng-blur="qcdc.reinitializeEPPCSA($event,productIndex); qcdc.updateEppCsaCounter(productIndex, 'blur', true)"/>
                                            <span class="increment" ng-click="qcdc.updateEppCsaCounter(productIndex, 'increment', true);"></span>
                                        </label>
                                        <label class="col-md-3 col-xs-12 epp-total-label font-type-1 text-center">
                                            <label class="visible-xs col-xs-2 no-padding"><spring:theme code="cat.quote.cart.item.epp.total" /></label>
                                            {{cartProduct.entries[1].totalPrice.formattedValue}}
                                        </label>
                                        <div class="col-md-5 col-xs-12 clearfix">
                                            <button class="btns-type-1 clear-btn pull-left" ng-class="{'btns-disable': !qcdc.isCSAApplyEnable(productIndex)}" ng-disabled="!qcdc.isCSAApplyEnable(productIndex)" ng-click="qcdc.clearEppCsaFilters(cartProduct.entries[1], productIndex, true);"><spring:theme code="cat.quote.cart.item.button.clear" /></button>
                                            <button class="btns-type-1" ng-class="{'btns-disable': !qcdc.isCSAApplyEnable(productIndex, true)}" ng-disabled="!qcdc.isCSAApplyEnable(productIndex, true)" ng-click="qcdc.applyEppCsaFilters(cartProduct.entries[1], productIndex, true);"><spring:theme code="cat.quote.cart.item.button.apply" /></button>
                                            <div>{{vm.cartProducts.entries[productIndex].entries[1].csaApplyEnable}}</div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div> 
                </section>
                <div class="row no-gutters prdct-mobile-btns-container">
                    <button class="visible-xs prdct-mobile-btns"
                    ng-class="{'disable': cartProduct.errorData.disableReserveBtn}" ng-if="cartProduct.reservedFlag"
                                    ng-click="qcdc.showReservePopup(productIndex);" ng-disabled="cartProduct.errorData.disableReserveBtn"
                    ><spring:theme code="cat.quote.cart.item.editReservation.text" /></button>
                </div>
                <div class="row no-gutters prdct-mobile-btns-container">
                    <button class="visible-xs prdct-mobile-btns" ng-class="{'disable': cartProduct.errorData.disableReserveBtn}" ng-if="!cartProduct.reservedFlag"
                    ng-click="qcdc.showReservePopup(productIndex);" ng-disabled="cartProduct.errorData.disableReserveBtn"><spring:theme code="cat.quote.cart.item.reserve.text" /></button>
                </div>
                <div ng-if="cartProduct.errorData.disableReserveBtn" ng-bind="cartProduct.errorData.reserveBtnErrorMsg" class="text-center cat-mesagesError mb-msg-err visible-xs"></div>
            </article>
            <!-- <quote-cart-component ng-repeat="(productIndex, cartProduct) in qcdc.cartProducts.entries"
            cart-product={{cartProduct}}
            epp-options={{eppcsaOptionsArray}}
            csa-options=[]
            ></quote-cart-component> -->
            <!-- <quote-cart-component></quote-cart-component>
            <quote-cart-component></quote-cart-component> -->
            <div class="row hidden-xs text-right text-uppercase no-gutters total-price">
                <div class="col-md-8 price-header no-padding"><spring:theme code="cat.quote.cart.totalListPrice" /></div>
                <div class="col-md-2 price-value">{{ qcdc.totalListPrice | currency }}</div>
                <div class="col-md-8 price-header"><spring:theme code="cat.quote.cart.totalDiscount" /></div>
                <div class="col-md-2 price-value">{{qcdc.cartProducts.totalDiscounts.formattedValue}}</div>
            </div>

            <div class="row hidden-xs no-gutters text-right text-uppercase transaction-price">
                <div class="col-md-8 price-header no-padding"><spring:theme code="cat.quote.cart.totalTransactionPrice" /></div>
                <div class="col-md-2 price-value">{{qcdc.cartProducts.totalPrice.formattedValue}}</div>
            </div>
        </article>
    </div> 
    <div class="row visible-xs text-uppercase no-gutters total-price">
        <div class="row no-gutters">
            <div class="col-xs-6 price-header text-left no-padding"><spring:theme code="cat.quote.cart.totalListPrice" /></div>
            <div class="col-xs-6 text-right price-value">{{ qcdc.totalListPrice | currency }}</div>
        </div>
        <div class="row no-gutters">
            <div class="col-xs-6 text-left price-header"><spring:theme code="cat.quote.cart.totalDiscount" /></div>
            <div class="col-xs-6 text-right price-value">{{qcdc.cartProducts.totalDiscounts.formattedValue}}</div>
        </div>
    </div>

    <div class="row visible-xs no-gutters  text-uppercase transaction-price">
        <div class="col-xs-6 price-header text-left no-padding"><spring:theme code="cat.quote.cart.totalTransactionPrice" /></div>
        <div class="col-xs-6 text-right price-value">{{qcdc.cartProducts.totalPrice.formattedValue}}</div>
    </div>     
</div>
