 <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="building"><spring:theme code="cat.quote.customer.details.shippingAddress.building"/></c:set>
<c:set var="streetName"><spring:theme code="cat.quote.customer.details.shippingAddress.streetName"/></c:set>
<c:set var="country"><spring:theme code="cat.quote.customer.details.shippingAddress.country"/></c:set>
<c:set var="city"><spring:theme code="cat.quote.customer.details.shippingAddress.city"/></c:set>
<c:set var="state"><spring:theme code="cat.quote.customer.details.shippingAddress.state"/></c:set>
<c:set var="zipCode"><spring:theme code="cat.quote.customer.details.shippingAddress.zipcode"/></c:set>
<c:set var="selectOption"><spring:theme code="cat.quote.dropdown.select"/></c:set>

 
 <div class="quotation-customer-container editable-quote cat-container row" ng-controller="quotationCustomerDetailsController as qcd" ng-init="qcd.updateReplicateBool('${isReplicateQuote}');" ng-cloak ng-hide="qcd.view.quoteMetaData.mobileMetaData.isMobileScreen && qcd.view.quoteMetaData.mobileNav.currentPageNum!=3">
    <div class="col-md-12 cart-header-section">
        <span><spring:theme code="cat.quote.customer.details.heading.text" /></span>
    </div>
    <div class="col-md-12 customer-details-section">
       <div class="col-md-12 select-customer-radio-section no-padding" ng-if="!qcd.view.quoteMetaData.editQuote">
            <label class="customer-radio-container pull-left margin-left-25 hidden-xs">
		<span class="radio-text"><spring:theme code="cat.quote.customer.details.existingCustomer" /></span>
                <input type="radio"  ng-model="qcd.view.customerData.customerType" value="old" ng-change="qcd.resetCustomerDetails()">
                <span class="checkmark new-customer"></span>
            </label>
	    <label class="customer-radio-container pull-left margin-left-30 visible-xs">
		<span class="radio-text"><spring:theme code="cat.quote.customer.details.existingCustomer" /></span>
                <input type="radio"  ng-model="qcd.view.customerData.customerType" value="old" ng-change="qcd.resetCustomerDetails()">
                <span class="checkmark new-customer"></span>
            </label>
            <label class="customer-radio-container pull-left margin-left-30"><span class="radio-text"><spring:theme code="cat.quote.customer.details.newCustomer" /></span>
                <input type="radio"  ng-model="qcd.view.customerData.customerType"  value="new"  ng-change="qcd.resetCustomerDetails()">
                <span class="checkmark existing-Customer"></span>
            </label>
            <div class="clearfix"></div>
       </div>
       <div class="col-xs-12 col-md-12 customer-data quote-edit">
            <div class="row customer-name no-padding customer-data-row">
                <div class="col-xs-12 col-md-2">
                    <label class="left-heading"><spring:theme code="cat.quote.customer.details.name" /></label><span class="asterik" ng-if="!qcd.view.quoteMetaData.editQuote"><spring:theme code="cat.quote.asterisk.symbol"/></span>
                </div>
                <div class="col-xs-12 col-md-3" ng-if="!qcd.view.quoteMetaData.editQuote">
                    <input type="text" id="catCustomerName" ng-if="qcd.view.customerData.customerType == 'old'" 
                    ng-model="qcd.view.customerData.customerName.value" placeholder="<spring:theme code="cat.quote.customer.name.placeHolder"/>" 
                    ng-disabled="qcd.isExistingCustomerDisabled" ng-class="{disable:qcd.isExistingCustomerDisabled}"
                    uib-typeahead="customer for customer in qcd.populateCustomerNameBasedOnSuggestion($viewValue)" 
                    typeahead-on-select="qcd.onCustomerNameSelect($item, $model, $label)" class="cat-inputs type1" 
                    typeahead-show-hint="true" typeahead-min-length="2">
                    <a class="clear-form" ng-if="qcd.isExistingCustomerDisabled" ng-click="qcd.clearExistingCustomerForm()"></a>
                    <img class="typeahead-arrow" ng-if="qcd.view.customerData.customerType == 'old'" 
                    ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"/>
                    <input type="text" class="cat-inputs type1" id="catCustomerName" ng-if="qcd.view.customerData.customerType == 'new'" 
                    placeholder="<spring:theme code="cat.quote.type.text"/>" ng-model="qcd.view.customerData.customerName.value">
                
                    <div class="cat-error type1" ng-if="!qcd.view.customerData.customerName.isValid"> <spring:theme code="cat.quote.details.quote.mandatory.message" /> </div>
                 </div>
                 <div class="col-xs-12 col-md-3" ng-if="qcd.view.quoteMetaData.editQuote">
                     <span ng-bind="qcd.view.customerData.customerName.value"></span>
                 </div>
             </div>
             <div class="row customer-email no-padding customer-data-row">
                 <div class="col-xs-12 col-md-2">
                     <label class="left-heading"><spring:theme code="cat.quote.customer.details.email" /></label><span class="asterik" ng-if="!qcd.view.quoteMetaData.editQuote"><spring:theme code="cat.quote.asterisk.symbol"/></span>
                 </div>
                 <div class="col-xs-12 col-md-3">
                     <input type="text" id="catCustomerEmail" ng-if="qcd.view.customerData.customerType == 'old'" ng-model="qcd.view.customerData.customerEmail.value" placeholder="<spring:theme code="cat.quote.customer.email.placeHolder"/>" uib-typeahead="customer for customer in qcd.populateCustomerEmailBasedOnSuggestion($viewValue)" typeahead-on-select="qcd.onCustomerEmailSelect($item, $model, $label)" class="cat-inputs type1" typeahead-show-hint="true" typeahead-min-length="2">
                     <img class="typeahead-arrow" ng-if="qcd.existingCustomer" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"/>
                    <input type="text" class="cat-inputs type1" id="catCustomerEmail" ng-if="qcd.view.customerData.customerType == 'new'" placeholder="<spring:theme code="cat.quote.type.text"/>" ng-model="qcd.view.customerData.customerEmail.value">
                    <div class="cat-error type1" ng-if="!qcd.view.customerData.customerEmail.isValid"> <spring:theme code="cat.quote.customer.email.invalid"/> </div>
                </div>
            </div>
            <div class="row shipping-address no-padding customer-data-row">
                <div class="col-xs-12 col-md-2">
                    <label class="left-heading"><spring:theme code="cat.quote.customer.details.shippingAddress" /></label><span class="asterik"><spring:theme code="cat.quote.asterisk.symbol"/></span>
                </div>
                <div class="col-xs-12 col-md-6 no-padding">
                    <div class="col-md-6 col-xs-12">
                        <div class="col-xs-12 col-md-12 hidden-xs product-family-dropdown-header no-padding competitor-dropdown-header"><spring:theme code="cat.quote.customer.details.shippingAddress.building" /></div>
                        <div class="col-xs-12 col-md-12 no-padding text-box">
                            <input type="text" ng-placeholder="qcd.view.quoteMetaData.mobileMetaData.isMobileScreen?'APT, SUITE, BUILDING':'Type'" class="value cat-inputs type1" ng-model="qcd.view.customerData.shippingAddress.line1.value" ng-class="{'has-error':!qcd.view.customerData.shippingAddress.line1.isValid}"/>
                            <div class="cat-error type1" ng-if="!qcd.view.customerData.shippingAddress.line1.isValid"><spring:theme code="cat.quote.details.quote.mandatory.message"/> </div>
                        </div>
                        <div class="col-xs-12 col-md-12 hidden-xs product-family-dropdown-header no-padding competitor-dropdown-header"><spring:theme code="cat.quote.customer.details.shippingAddress.streetName" /></div>
                        <div class="col-xs-12 col-md-12 no-padding text-box">
                            <input type="text" ng-placeholder="qcd.view.quoteMetaData.mobileMetaData.isMobileScreen?'STREET NAME':'Type'" class="value cat-inputs type1" ng-model="qcd.view.customerData.shippingAddress.line2.value" ng-class="{'has-error':!qcd.view.customerData.shippingAddress.line2.isValid}"/>
                            <div class="cat-error type1" ng-if="!qcd.view.customerData.shippingAddress.line2.isValid"><spring:theme code="cat.quote.details.quote.mandatory.message"/></div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xs-12 no-padding">
                        <div class = "col-md-6">
                            <div class="col-xs-12 col-md-12 hidden-xs product-family-dropdown-header no-padding competitor-dropdown-header"><spring:theme code="cat.quote.customer.details.shippingAddress.country" /></div>
                            <div class="col-xs-12 col-md-12 no-padding text-box">
                                <div class="btn-group col-xs-12 cat-buttons drop-down-button" uib-dropdown keyboard-nav=true>
                                    <button id="sales-model" type="button" class="btn col-xs-12 drop-down-button" uib-dropdown-toggle ng-class="{'not-selected':!qcd.view.customerData.shippingAddress.countryList.selectedCountry.name}">
                                    <div class="text">{{(qcd.view.customerData.shippingAddress.countryList.selectedCountry.name) || (qcd.view.quoteMetaData.mobileMetaData.isMobileScreen?'COUNTRY':'- Select -')}}</div>
                                    <div class="arrow pull-right">
                                        <i class="fa fa-angle-down hidden-xs" aria-hidden="true"></i>
                                        <img class="drop-down visible-xs" src="/_ui/responsive/cat/images/dropdown.png">
                                    </div>
                                    </button>
                                    <ul class="dropdown-menu col-xs-12" uib-dropdown-menu role="menu" aria-labelledby="sales-model">
                                        <li role="menuitem" ng-repeat="countryObj in qcd.view.customerData.shippingAddress.countryList.dataList" ng-click="qcd.shippingCountryChanged(countryObj)"><a href="#">{{countryObj.name}}</a></li>
                                    </ul>
                                </div>
                                <div class="cat-error type1" ng-if="!qcd.view.customerData.shippingAddress.countryList.isValid"><spring:theme code="cat.quote.details.quote.mandatory.message"/></div>
                            </div>
                            <div class="col-xs-12 col-md-12 hidden-xs product-family-dropdown-header no-padding competitor-dropdown-header"><spring:theme code="cat.quote.customer.details.shippingAddress.city" /></div>
                            <div class="col-xs-12 col-md-12 no-padding text-box">
                                <input type="text" ng-placeholder="qcd.view.quoteMetaData.mobileMetaData.isMobileScreen?'CITY':'Type'" restrict-to="[0-9a-zA-Z ]" class="value cat-inputs type1" ng-model="qcd.view.customerData.shippingAddress.town.value" ng-class="{'has-error':!qcd.view.customerData.shippingAddress.town.isValid}"/>
                                <div class="cat-error type1" ng-if="!qcd.view.customerData.shippingAddress.town.isValid"><spring:theme code="cat.quote.details.quote.mandatory.message"/></div>
                            </div>
                        </div>
                        <div class = "col-md-6">
                            <div class="col-xs-12 col-md-12 hidden-xs product-family-dropdown-header no-padding competitor-dropdown-header"><spring:theme code="cat.quote.customer.details.shippingAddress.state"/></div>
                            <div class="col-xs-12 col-md-12 no-padding text-box">
                                <div class="btn-group col-xs-12 cat-buttons drop-down-button" uib-dropdown keyboard-nav=true>
                                    <button id="sales-model" type="button" class="btn col-xs-12 drop-down-button" uib-dropdown-toggle ng-class="{'not-selected':!qcd.view.customerData.shippingAddress.regionList.selectedRegion.name}">
                                    <div class="text">{{(qcd.view.customerData.shippingAddress.regionList.selectedRegion.name) || (qcd.view.quoteMetaData.mobileMetaData.isMobileScreen?'STATE':'- Select -')}}</div>
                                    <div class="arrow pull-right">
                                        <i class="fa fa-angle-down hidden-xs" aria-hidden="true"></i>
                                        <img class="drop-down visible-xs" src="/_ui/responsive/cat/images/dropdown.png">
                                    </div>
                                    </button>
                                    <ul class="dropdown-menu col-xs-12" uib-dropdown-menu role="menu" aria-labelledby="sales-model">
                                        <li role="menuitem" ng-repeat="regionObj in qcd.view.customerData.shippingAddress.regionList.dataList" ng-click="qcd.shippingStateChanged(regionObj)"><a href="#">{{regionObj.name}}</a></li>
                                    </ul>
                                </div>
                                <div class="cat-error type1" ng-if="!qcd.view.customerData.shippingAddress.regionList.isValid"><spring:theme code="cat.quote.details.quote.mandatory.message"/></div>
                            </div>  
                            <div class="col-xs-12 col-md-12 hidden-xs product-family-dropdown-header no-padding competitor-dropdown-header"><spring:theme code="cat.quote.customer.details.shippingAddress.zipcode" /></div>
                            <div class="col-xs-12 col-md-12 no-padding text-box">
                                <input type="text" ng-placeholder="qcd.view.quoteMetaData.mobileMetaData.isMobileScreen?'ZIPCODE':'Type'" class="value cat-inputs type1" ng-model="qcd.view.customerData.shippingAddress.postalCode.value" ng-class="{'has-error':!qcd.view.customerData.shippingAddress.postalCode.isValid}"/>  
                                <div class="cat-error type1" ng-if="!qcd.view.customerData.shippingAddress.postalCode.isValid"><spring:theme code="cat.quote.details.quote.mandatory.message"/></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row col-md-12 billing-address no-padding customer-data-row">
                <div class="col-xs-12 col-md-2">
                    <label class="left-heading"><spring:theme code="cat.quote.customer.details.billingAddress" /></label><span class="asterik"><spring:theme code="cat.quote.asterisk.symbol"/></span>
                </div> 
                <div class="col-xs-12 col-md-6 no-padding">
                    <div class="col-md-6">
                        <div class="col-xs-12 col-md-12 billing-address-checkbox-section no-padding">
                            <label class="billing-address-checkbox"><spring:theme code="cat.quote.customer.details.sameAddress" />
                            <input type="checkbox" ng-model="qcd.view.customerData.isAddressSame" ng-change="qcd.isAddressSameChanged()">
                            <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="col-xs-12 col-md-12 hidden-xs product-family-dropdown-header no-padding competitor-dropdown-header"><spring:theme code="cat.quote.customer.details.billingAddress.building" /></div>
                        <div class="col-xs-12 col-md-12 no-padding text-box">
                            <input type="text" id="billAddressLine1" ng-model="qcd.view.customerData.billingAddress.line1.value"  ng-placeholder="qcd.view.quoteMetaData.mobileMetaData.isMobileScreen?'APT, SUITE, BUILDING':'Type'" class="value cat-inputs type1" ng-disabled="!!qcd.view.customerData.isAddressSame" ng-class="{'disabled': (!!qcd.view.customerData.isAddressSame), 'has-error':!qcd.view.customerData.billingAddress.line1.isValid}"/>
                            <div class="cat-error type1" ng-if="!qcd.view.customerData.billingAddress.line1.isValid"><spring:theme code="cat.quote.details.quote.mandatory.message" /> </div>
                        </div>
                        <div class="col-xs-12 col-md-12 hidden-xs product-family-dropdown-header no-padding competitor-dropdown-header"><spring:theme code="cat.quote.customer.details.billingAddress.streetName" /></div>
                        <div class="col-xs-12 col-md-12 no-padding text-box">
                            <input type="text" id="billAddressLine2" ng-model="qcd.view.customerData.billingAddress.line2.value"  ng-placeholder="qcd.view.quoteMetaData.mobileMetaData.isMobileScreen?'STREET NAME':'Type'" class="value cat-inputs type1" ng-disabled="!!qcd.view.customerData.isAddressSame" ng-class="{'disabled': (!!qcd.view.customerData.isAddressSame), 'has-error':!qcd.view.customerData.billingAddress.line2.isValid}"/>
                            <div class="cat-error type1" ng-if="!qcd.view.customerData.billingAddress.line2.isValid"><spring:theme code="cat.quote.details.quote.mandatory.message" /></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-12 no-padding">
                        <div class="col-md-6">
                            <div class="col-xs-12 col-md-12 hidden-xs product-family-dropdown-header no-padding competitor-dropdown-header"><spring:theme code="cat.quote.customer.details.billingAddress.country" /></div>
                            <div class="col-xs-12 col-md-12 no-padding text-box">
                                <div class="btn-group col-xs-12 cat-buttons drop-down-button" uib-dropdown keyboard-nav=true>
                                    <button id="sales-model" type="button" class="btn col-xs-12 drop-down-button" uib-dropdown-toggle ng-class="{'not-selected':!qcd.view.customerData.billingAddress.countryList.selectedCountry.name}">
                                    <div class="text">{{(qcd.view.customerData.billingAddress.countryList.selectedCountry.name) || (qcd.view.quoteMetaData.mobileMetaData.isMobileScreen?'COUNTRY':'- Select -')}}</div>
                                    <div class="arrow pull-right">
                                        <i class="fa fa-angle-down hidden-xs" aria-hidden="true"></i>
                                        <img class="drop-down visible-xs" src="/_ui/responsive/cat/images/dropdown.png">
                                    </div>
                                    </button>
                                    <ul class="dropdown-menu col-xs-12" uib-dropdown-menu role="menu" aria-labelledby="sales-model">
                                        <li role="menuitem" ng-repeat="countryObj in qcd.view.customerData.billingAddress.countryList.dataList" ng-click="qcd.billingCountryChanged(countryObj)"><a href="#">{{countryObj.name}}</a></li>
                                    </ul>
                                </div>
                                <div class="cat-error type1" ng-if="!qcd.view.customerData.billingAddress.countryList.isValid"><spring:theme code="cat.quote.details.quote.mandatory.message" /> </div>
                            </div>
                            <div class="col-xs-12 col-md-12 hidden-xs product-family-dropdown-header no-padding competitor-dropdown-header"><spring:theme code="cat.quote.customer.details.billingAddress.city" /></div>
                            <div class="col-xs-12 col-md-12 no-padding text-box">
                                <input type="text" ng-placeholder="qcd.view.quoteMetaData.mobileMetaData.isMobileScreen?'CITY':'Type'" id="billAddressTown" restrict-to="[0-9a-zA-Z ]" class="value cat-inputs type1" ng-model="qcd.view.customerData.billingAddress.town.value" ng-disabled="!!qcd.view.customerData.isAddressSame" ng-class="{'disabled': (!!qcd.view.customerData.isAddressSame), 'has-error':!qcd.view.customerData.billingAddress.line2.isValid}"/>
                                <div class="cat-error type1" ng-if="!qcd.view.customerData.billingAddress.town.isValid"><spring:theme code="cat.quote.details.quote.mandatory.message" /> </div>
                            </div>
                        </div>  
                        <div class="col-md-6">
                            <div class="col-xs-12 col-md-12 hidden-xs product-family-dropdown-header no-padding competitor-dropdown-header"><spring:theme code="cat.quote.customer.details.billingAddress.state" /></div>
                            <div class="col-xs-12 col-md-12 no-padding text-box">
                                <div class="btn-group col-xs-12 cat-buttons drop-down-button" uib-dropdown keyboard-nav=true>
                                    <button id="sales-model" type="button" class="btn col-xs-12 drop-down-button" uib-dropdown-toggle ng-class="{'not-selected':!!qcd.view.customerData.billingAddress.regionList.selectedRegion.name}">
                                    <div class="text">{{(qcd.view.customerData.billingAddress.regionList.selectedRegion.name) || (qcd.view.quoteMetaData.mobileMetaData.isMobileScreen?'STATE':'- Select -')}}</div>
                                    <div class="arrow pull-right">
                                        <i class="fa fa-angle-down hidden-xs" aria-hidden="true"></i>
                                        <img class="drop-down visible-xs" src="/_ui/responsive/cat/images/dropdown.png">
                                    </div>
                                    </button>
                                    <ul class="dropdown-menu col-xs-12" uib-dropdown-menu role="menu" aria-labelledby="sales-model">
                                        <li role="menuitem" ng-repeat="regionObj in qcd.view.customerData.billingAddress.regionList.dataList" ng-click="qcd.billingStateChanged(regionObj)"><a href="#">{{regionObj.name}}</a></li>
                                    </ul>
                                </div>
                                <div class="cat-error type1" ng-if="!qcd.view.customerData.billingAddress.regionList.isValid"><spring:theme code="cat.quote.details.quote.mandatory.message" /> </div>
                            </div>
                            <div class="col-xs-12 col-md-12 hidden-xs product-family-dropdown-header no-padding competitor-dropdown-header"><spring:theme code="cat.quote.customer.details.billingAddress.zipcode" /></div>
                            <div class="col-xs-12 col-md-12 no-padding text-box">
                                <input type="text" id="billAddressZipCode" ng-model="qcd.view.customerData.billingAddress.postalCode.value"  ng-placeholder="qcd.view.quoteMetaData.mobileMetaData.isMobileScreen?'ZIPCODE':'Type'" class="value cat-inputs type1" ng-disabled="!!qcd.view.customerData.isAddressSame" ng-class="{'disabled': (!!qcd.view.customerData.isAddressSame), 'has-error':!qcd.view.customerData.billingAddress.postalCode.isValid}"/>
                                <div class="cat-error type1" ng-if="!qcd.view.customerData.billingAddress.postalCode.isValid"><spring:theme code="cat.quote.details.quote.mandatory.message" /> </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
            <div class="row col-md-12 comments-section no-padding customer-data-row">
                <div class="col-xs-12 col-md-2">
                    <label class="left-heading"><spring:theme code="cat.quote.comments" /></label>
                </div>
                <div class="col-xs-12 col-md-8 comments-box">
                    <div class="col-xs-12 no-padding">
                    <input type="text" ng-model="qcd.view.customerData.customerComments.value" 
                    placeholder="Enter comments here" class="value col-md-12 cat-inputs type2" 
                    ng-class="{'has-error':qcd.view.customerData.customerComments.value.length > 255||!qcd.view.customerData.customerComments.isValid}" cat-max-length="255"/>
                    <span class="text-counter" ng-class="{'has-error':qcd.view.customerData.customerComments.value.length > 255}">
                        <span ng-if="qcd.view.customerData.customerComments.value == null">0</span> 
                        <span ng-if="qcd.view.customerData.customerComments.value != null" ng-bind-html="qcd.view.customerData.customerComments.value.length"></span> <spring:theme code="cat.quote.details.quote.limit.textArea" /></span>
                    </div>
                    <div class="col-xs-12 no-padding">
                     <div class="cat-error type1 col-xs-12 col-md-12" ng-if="!qcd.view.customerData.customerComments.isValid"> <spring:theme code="cat.quote.details.quote.mandatory.message" /> </div>
                     <div class="cat-error type1 col-xs-12 col-md-12" ng-if="qcd.view.customerData.customerComments.value.length > 255"> <spring:theme code="cat.quote.details.quote.character.limit.exceeded" /> </div>
                    </div>
                </div>
            </div>    
       </div>
    </div>
 </div>