<div class="page-header product-compare-header">QUOTE</div>
<div class="row cat-container quote-mobile-component" ng-controller="QuotationMobileController as qmc" ng-init="qmc.init('${media_prefix_domain}');" ng-cloak>
    <div class="col-md-6 col-md-offset-3">
        <form id="msform">
            <ul id="progressbar">
                <li class="active">QUOTE DETAILS</li>
                <li>CART DETAILS</li>
                <li>CUSTOMER DETAILS</li>
            </ul>
            <fieldset ng-controller="quotationQuoteDetailsController as qqdc" data-section="QUOTE DETAILS">
                <div class="col-xs-12 quote-details-heading">
                    <h2 class="fs-title">QUOTE DETAILS</h2>
                </div>
                <div class="col-xs-12 quote-details-content">
                    <div class="col-xs-6 no-padding quote-details-row">
                        <div class="col-md-12 text-left no-padding"><label class="col-header">QUOTE NUMBER</label></div>
                        <div class="col-md-12 text-left no-padding"><label class="col-value">86391</label></div>
                    </div>
                    <div class="col-xs-6 no-padding quote-details-row">
                        <div class="col-md-12 text-left no-padding"><label class="col-header">DATE CREATED</label></div>
                        <div class="col-md-12 text-left no-padding"><label class="col-value">01-01-2018</label></div>
                    </div>
                    <div class="col-xs-12 no-padding quote-details-row">
                        <div class="col-md-12 text-left no-padding"><label class="col-header">QUOTE NAME</label><span class="asterik">*</span></div>
                        <div class="col-md-12 text-left no-padding"><input type="text" class="text-box"/></div>
                    </div>
                    <div class="col-xs-12 no-padding quote-details-row">
                        <div class="col-md-12 text-left no-padding"><label class="col-header">VALID UNTIL</label></div>
                        <div class="col-md-12 text-left no-padding"><input mobiscroll-date="qmc.datePickerSettings" class="text-box"/></div>
                    </div>
                    <div class="col-xs-12 no-padding quote-details-row">
                        <div class="col-md-12 text-left no-padding"><label class="col-header">DESCRIPTION</label></div>
                        <div class="col-md-12 text-left no-padding"><input type="text" placeholder="Enter Description" class="text-box description-box"/></div>
                    </div>
                </div>
            </fieldset>
            <fieldset data-section="CART DETAILS" ng-controller="quoteCartDetailsController as qcdc" ng-init="qcdc.loadCartProducts('${media_prefix_domain}');">
                <div class="col-xs-12 quote-details-heading">
                    <h2 class="fs-title">CART DETAILS</h2>
                </div>
                 <div class="col-xs-12 cart-details-content" ng-repeat="cartProduct in qcdc.cartProducts.entries">
                    <div class="col-xs-12 product-details-section product-row">
                        <label class="pull-left product-name-heading">{{cartProduct.product.categories[0].name | initcaps}}</label>
                        <div class="clearfix"></div>
                        <div class="pull-left product-image-container">
                            <img ng-src="{{qcdc.mediaPrefix}}{{cartProduct.product.images[0].url}}"/>
                        </div>
                        <div class="pull-left product-description-container">
                           <label class="product-base-name">{{cartProduct.product.primaryOfferingName}} ({{cartProduct.product.baseProduct}})</label>
                           <label class="configuration-id-header">CONFIGURATION ID</label>
                           <label class="configuration-id-value">{{cartProduct.product.name}}</label>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-xs-12 list-price-per-product product-row">
                        <div class="pull-left heading">LIST PRICE/PRODUCT</div>
                        <div class="pull-right value">{{cartProduct.basePrice.formattedValue}}</div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-xs-12 quantity product-row">
                        <div class="pull-left heading">QUANTITY</div>
                         <div class="pull-right value">
                            <span class="decrement" ng-click="qcdc.updateCartProducts($index,'decrement');"></span>
                            <input class="quantity-box" type="text" ng-blur="qcdc.updateCartProducts($index,'blur');" value="{{cartProduct.quantity}}"/>
                            <span class="increment" ng-click="qcdc.updateCartProducts($index,'increment');"></span>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-xs-12 discount product-row">
                        <div class="pull-left heading">DISCOUNT</div>
                        <div class="pull-right value">
                            <span class="dollar-off pull-left" ng-click="qcdc.toggleDiscountCalc('$');">$</span>
                            <input class="discount-box pull-left" type="text" ng-blur="qcdc.calcDiscount($index,qcdc.discountRadio,$event)" value="0"/>
                            <span class="percent-off pull-left" ng-click="qcdc.toggleDiscountCalc('%');">%</span>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-xs-12 transaction product-row">
                        <div class="pull-left heading">TRANSACTION</div>
                        <div class="pull-right value">{{cartProduct.totalPrice.formattedValue}}</div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-xs-12 padding-top-15"></div>
                 </div>
                 <div class="col-xs-12 list-price-discount-section">
                    <div class="col-xs-12 price-row">
                       <div class="pull-left heading">TOTAL LIST PRICE</div>
                       <div class="pull-right value">{{qcdc.cartProducts.totalPrice.formattedValue}}</div>
                       <div class="clearfix"></div>
                    </div>
                    <div class="col-xs-12 price-row">
                       <div class="pull-left heading">TOTAL DISCOUNT</div>
                       <div class="pull-right value">{{qcdc.cartProducts.totalDiscounts.formattedValue}}</div>
                       <div class="clearfix"></div>
                    </div>
                 </div>
                 <div class="col-xs-12 transaction-price-section">
                    <div class="col-xs-12">
                       <div class="pull-left heading">TOTAL TRANSACTION PRICE</div>
                       <div class="pull-right value">{{qcdc.totalTransactionPrice}}</div>
                       <div class="clearfix"></div>
                    </div>
                 </div>
                 <div class="col-xs-12 additional-services-section">
                    <div class="col-xs-12 no-padding additional-services-header">
                        <label>ADDITIONAL SERVICES</label>
                    </div>
                    <div class="col-xs-12 no-padding additional-services-checkbox-section CSA">
                        <label class="additional-service-checkbox-container">Add CSA Option
                        <input type="checkbox">
                        <span class="checkmark"></span>
                        </label>
                        <input type="text" placeholder="Enter CSA Details" class="value col-md-8 description-box"/>
                    </div>
                    <div class="col-xs-12 no-padding additional-services-checkbox-section EPP">
                        <label class="additional-service-checkbox-container">Add EPP Option
                        <input type="checkbox">
                        <span class="checkmark"></span>
                        </label>
                        <input type="text" placeholder="Enter EPP Details" class="value col-md-8 description-box"/>
                    </div>
                </div>
            </fieldset>
            <fieldset data-section="CUSTOMER DETAILS" ng-controller="quotationCustomerDetailsController as qcdtc">
                <div class="col-xs-12 quote-details-heading">
                    <h2 class="fs-title">CUSTOMER DETAILS</h2>
                </div>
                <div class="col-xs-12 customer-details-section">
                    <div class="col-md-12 select-customer-radio-section no-padding">
                        <label class="customer-radio-container margin-left-25"><span class="radio-text">Existing Customer</span>
                            <input type="radio" checked="checked" name="radio" value="existing-customer">
                            <span class="checkmark new-customer"></span>
                        </label>
                        <label class="customer-radio-container margin-left-30"><span class="radio-text">New Customer</span>
                            <input type="radio" name="radio" value="new-customer">
                            <span class="checkmark existing-Customer"></span>
                        </label>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-xs-12 customer-data">
            <div class="col-xs-12 customer-name no-padding customer-data-row">
                <div class="col-xs-12 no-padding">
                    <label class="left-heading">CUSTOMER NAME</label><span class="asterik">*</span>
                </div>
                <div class="col-xs-12 no-padding">
                 <input type="text" ng-model="qcdtc.customerName" placeholder="Enter or select name" uib-typeahead="customer as customer.name for customer in qcdtc.nameArray | filter:{name:$viewValue}" class="col-md-12  text-box customer-name-typeahead" typeahead-show-hint="true" typeahead-min-length="2">
                 <img class="typeahead-arrow" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"/>
                    <!--<span uib-dropdown class="product-family-btn col-xs-12">
							<a class="col-xs-12 product-family-dropdown" href uib-dropdown-toggle>
								<label class="product-family-dropdown-label">Enter or Select name</label> 
								<img class="pull-right product-family-dropdown-arrow" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"/>
                                <span class="clearfix"></span>
							</a>
							<ul class="col-xs-12 dropdown-menu product-family-dropdown-menu" uib-dropdown-menu>
								<li class="product-family-dropdown-item dropdown1">
                                    <a class="dropdown-value" href="#">Value</a>
                                </li>
							</ul>
						</span> -->
                </div>
            </div>
            <div class="col-xs-12 customer-email no-padding customer-data-row">
                <div class="col-xs-12 no-padding">
                    <label class="left-heading">CUSTOMER EMAIL</label><span class="asterik">*</span>
                </div>
                <div class="col-xs-12 no-padding">
                <input type="text" ng-model="qcdtc.customerEmail" placeholder="Enter or select email" uib-typeahead="customer as customer.name for customer in qcdtc.nameArray | filter:{name:$viewValue}" class="col-md-10 customer-email-typeahead text-box" typeahead-show-hint="true" typeahead-min-length="2">
                <img class="typeahead-arrow" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"/>
                    <!--<span uib-dropdown class="product-family-btn col-xs-12">
							<a class="col-xs-12 product-family-dropdown" href uib-dropdown-toggle>
								<label class="product-family-dropdown-label">Enter or Select name</label> 
								<img class="pull-right product-family-dropdown-arrow" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"/>
                                <span class="clearfix"></span>
							</a>
							<ul class="col-xs-12 dropdown-menu product-family-dropdown-menu" uib-dropdown-menu>
								<li class="product-family-dropdown-item dropdown1">
                                    <a class="dropdown-value" href="#">Value</a>
                                </li>
							</ul>
						</span>  -->
                </div>
            </div>
            <div class="col-xs-12 shipping-address no-padding customer-data-row">
                <div class="col-xs-12 no-padding">
                    <label class="left-heading">SHIPPING ADDRESS</label><span class="asterik">*</span>
                </div>
                <div class="col-xs-12 no-padding">
                    <input type="text" placeholder="APT,SUITE,BUILDING" class="value col-xs-12 text-box"/>
                    <input type="text" placeholder="STREET NAME" class="value col-xs-12 text-box"/>
                    <div class="col-xs-12 product-family-dropdown-header no-padding competitor-dropdown-header">CITY</div>
                    <span uib-dropdown class="product-family-btn col-xs-12">
							<a class="col-xs-12 product-family-dropdown" href uib-dropdown-toggle>
								<label class="product-family-dropdown-label">Select</label> 
								<img class="pull-right product-family-dropdown-arrow" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"/>
                                <span class="clearfix"></span>
							</a>
							<ul class="col-xs-12 dropdown-menu product-family-dropdown-menu" uib-dropdown-menu>
								<li class="product-family-dropdown-item dropdown1">
                                    <a class="dropdown-value" href="#">Value</a>
                                </li>
							</ul>
					</span>
                    <div class="col-xs-12 product-family-dropdown-header no-padding competitor-dropdown-header">COUNTRY</div>
                    <span uib-dropdown class="product-family-btn col-xs-12">
							<a class="col-xs-12 product-family-dropdown" href uib-dropdown-toggle>
								<label class="product-family-dropdown-label">Select</label> 
								<img class="pull-right product-family-dropdown-arrow" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"/>
                                <span class="clearfix"></span>
							</a>
							<ul class="col-xs-12 dropdown-menu product-family-dropdown-menu" uib-dropdown-menu>
								<li class="product-family-dropdown-item dropdown1">
                                    <a class="dropdown-value" href="#">Value</a>
                                </li>
							</ul>
					</span>    
                </div>
                <div class="col-xs-12 no-padding">
                    <div class="col-xs-12 product-family-dropdown-header no-padding competitor-dropdown-header">STATE</div>
                    <span uib-dropdown class="product-family-btn col-xs-12">
							<a class="col-xs-12 product-family-dropdown" href uib-dropdown-toggle>
								<label class="product-family-dropdown-label">Select</label> 
								<img class="pull-right product-family-dropdown-arrow" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"/>
                                <span class="clearfix"></span>
							</a>
							<ul class="col-xs-12 dropdown-menu product-family-dropdown-menu" uib-dropdown-menu>
								<li class="product-family-dropdown-item dropdown1">
                                    <a class="dropdown-value" href="#">Value</a>
                                </li>
							</ul>
					</span>
                     <input type="text" placeholder="ZICPODE" class="value col-xs-12 text-box"/>
                </div>
            </div>
            <div class="col-xs-12 billing-address no-padding customer-data-row">
                <div class="col-xs-12 no-padding">
                    <label class="left-heading">BILLING ADDRESS</label><span class="asterik">*</span>
                </div>
                <div class="col-xs-12 billing-address-checkbox-section no-padding">
                        <label class="billing-address-checkbox">Same as shipping address
                        <input type="checkbox">
                        <span class="checkmark"></span>
                        </label>
                    </div>
                <div class="col-xs-12 no-padding">
                    <input type="text" placeholder="APT,SUITE,BUILDING" class="value col-xs-12 text-box"/>
                    <input type="text" placeholder="STREET NAME" class="value col-xs-12 text-box"/>
                    <div class="col-xs-12 product-family-dropdown-header no-padding competitor-dropdown-header">CITY</div>
                    <span uib-dropdown class="product-family-btn col-xs-12">
							<a class="col-xs-12 product-family-dropdown" href uib-dropdown-toggle>
								<label class="product-family-dropdown-label">Select</label> 
								<img class="pull-right product-family-dropdown-arrow" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"/>
                                <span class="clearfix"></span>
							</a>
							<ul class="col-xs-12 dropdown-menu product-family-dropdown-menu" uib-dropdown-menu>
								<li class="product-family-dropdown-item dropdown1">
                                    <a class="dropdown-value" href="#">Value</a>
                                </li>
							</ul>
					</span>
                    <div class="col-xs-12 product-family-dropdown-header no-padding competitor-dropdown-header">COUNTRY</div>
                    <span uib-dropdown class="product-family-btn col-xs-12">
							<a class="col-xs-12 product-family-dropdown" href uib-dropdown-toggle>
								<label class="product-family-dropdown-label">Select</label> 
								<img class="pull-right product-family-dropdown-arrow" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"/>
                                <span class="clearfix"></span>
							</a>
							<ul class="col-xs-12 dropdown-menu product-family-dropdown-menu" uib-dropdown-menu>
								<li class="product-family-dropdown-item dropdown1">
                                    <a class="dropdown-value" href="#">Value</a>
                                </li>
							</ul>
					</span>    
                </div>
                <div class="col-xs-12 no-padding  ">
                    <div class="col-xs-12 product-family-dropdown-header no-padding competitor-dropdown-header">STATE</div>
                    <span uib-dropdown class="product-family-btn col-xs-12">
							<a class="col-xs-12 product-family-dropdown" href uib-dropdown-toggle>
								<label class="product-family-dropdown-label">Select</label> 
								<img class="pull-right product-family-dropdown-arrow" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"/>
                                <span class="clearfix"></span>
							</a>
							<ul class="col-xs-12 dropdown-menu product-family-dropdown-menu" uib-dropdown-menu>
								<li class="product-family-dropdown-item dropdown1">
                                    <a class="dropdown-value" href="#">Value</a>
                                </li>
							</ul>
					</span>
                     <input type="text" placeholder="ZICPODE" class="value col-xs-12 text-box"/>
                </div>
            </div>
            <div class="col-xs-12 comments-section no-padding">
                <div class="col-xs-12 no-padding">
                    <label class="left-heading">COMMENTS</label><span class="asterik">*</span>
                </div>
                <div class="col-xs-12 no-padding">
                    <input type="text" placeholder="Enter comments" class="value col-xs-12 text-box comments-box"/>
                </div>
            </div> 
            </fieldset>
            <div class="col-md-12 no-padding indicates-required" ng-if="qmc.stepName === 'SUBMIT QUOTE'">
                <span class="asterik pull-left">*</span><label class="pull-left">Indicates required fields</label>
            </div>
            <div class="col-xs-12 no-padding">
                <button class="btn update-product-list mobile-quote-btn" ng-click="qmc.swipeView($event);">{{qmc.stepName}}</button>
            </div>
        </form>
    </div>
</div>