<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="selectOption"><spring:theme code="cat.quote.dropdown.select"/></c:set>


<div class="row cat-container quote-details-section-desktop  editable-quote" ng-controller="quotationQuoteDetailsController as qqdc" ng-init="qqdc.init('${cartData.quoteData.expirationTime}','${cartData.quoteData.code}','${cartData.quoteData.name}','${firstTimeEdit}','${isReplicateQuote}');" ng-cloak ng-hide="qqdc.view.quoteMetaData.mobileMetaData.isMobileScreen && qqdc.view.quoteMetaData.mobileNav.currentPageNum!=1">
    <div class="row">

        <div class="col-xs-12 col-md-12 quote-details-heading-section">
		    <span><spring:theme code="cat.quote.details.heading.text" /></span>
        </div>
    </div>
    <div class="row">
    <div class="col-xs-6 col-md-4 quote-number quote-details-col no-border border-right">
        <label class="sub-header"><spring:theme code="cat.quote.details.quote.number" /></label>
        <label class="value">${cartData.quoteData.code}</label>
    </div>
    <div class="col-xs-6 col-md-8 date-created quote-details-col no-border">
        <label class="sub-header"><spring:theme code="cat.quote.details.quote.createdDate" /></label>
        <label class="value">
	        <fmt:formatDate value="${catQuoteData.quoteCreatedTime}" dateStyle="medium" pattern="MM-dd-yyyy" type="date"/>
        </label>
    </div>
    </div>

    <div class="row row-top-border">
        <div class="col-xs-12 col-md-4 quote-name quote-details-col no-border border-right">
            <label class="sub-header pull-left"><spring:theme code="cat.quote.details.quote.name" /></label><span class="pull-left asterik"><spring:theme code="cat.quote.asterisk.symbol"/></span>
            <div class="clearfix"></div>
            <input type="text" ng-change="qqdc.checkQuotesFunc(qqdc.view.quote.name.value)" ng-model="qqdc.view.quote.name.value" class="cat-inputs type1 edit-quote-name" ng-class="{'has-error':!qqdc.view.quote.name.isValid}"/>
            <div class="cat-error type1" ng-if="!qqdc.view.quote.name.isValid"><spring:theme code="cat.quote.details.quote.mandatory.message" /></div>
        </div>
        <div class="col-xs-12 col-md-8 valid-until quote-details-col no-border" ng-if="!qqdc.view.quoteMetaData.editQuote">
            <label class="sub-header pull-left"><spring:theme code="cat.quote.details.quote.validDate" /></label><span class="pull-left asterik"><spring:theme code="cat.quote.asterisk.symbol"/></span>
             <div class="clearfix"></div>
             <div class="input-container">
                <input type="text" uib-datepicker-popup="{{qqdc.view.quote.metaData.dateFormat}}" ng-model="qqdc.view.quote.validTill.value" name="validTill" popup-placement="top-left" datepicker-popup-template-url="/_ui/responsive/cat/templates/customDatepickerTemplate.html" on-open-focus="false" is-open="qqdc.view.quote.validTill.popupOpend" datepicker-options="qqdc.view.quote.metaData.validTillDateOptions" ng-required="true" close-text="Close" ng-click="qqdc.view.quote.validTill.popupOpend = !qqdc.view.quote.validTill.popupOpend"  class="cat-inputs type1 valid-until-datepicker"  ng-class="{'has-error':!qqdc.view.quote.validTill.isValid}" ng-readonly="true"/>
                <span class="calendar-icon" ng-click="qqdc.view.quote.validTill.popupOpend = !qqdc.view.quote.validTill.popupOpend"></span>
             </div>    
             <div class="cat-error type1" ng-if="!qqdc.view.quote.validTill.isValid"> <spring:theme code="cat.quote.details.quote.mandatory.message" /> </div>
        </div>

        <div class="col-xs-12 col-md-8 valid-until quote-details-col no-border" ng-if="qqdc.view.quoteMetaData.editQuote">
            <label class="sub-header"><spring:theme code="cat.quote.details.quote.modifiedDate" /></label>
            <label class="value"><fmt:formatDate value="${cartData.quoteData.updatedTime}" pattern="MM-dd-yyyy" /></label>
        </div>
    </div>

    <div class="row row-top-border white-background">
        <div class="col-xs-12 col-md-9 quote-details-col description-section no-border">
            <label class="sub-header"><spring:theme code="cat.quote.details.quote.description" /></label>
            <input type="text" ng-model="qqdc.view.quote.description.value" 
            placeholder="<spring:theme code="cat.quote.detail.description.placeHolder"/>" class="value col-md-12 cat-inputs description-input type2" cat-max-length="255"/>
            <span class="text-counter text-right" class='has-error'>
                    {{qqdc.view.quote.description.value.length||0}} <spring:theme code="cat.quote.details.quote.limit.textArea" /></span>
            <p class="cat-error type1 col-xs-12 col-md-12" 
            ng-if="qqdc.view.quote.description.value.length > 255"> 
                <spring:theme code="cat.quote.details.quote.character.limit.exceeded" /></p>
        </div>
    </div>

    <div class="row row-top-border" ng-if="qqdc.view.quoteMetaData.editQuote">
        <div class="col-xs-6 col-md-4 quote-number quote-details-col no-border border-right">
            <label class="sub-header"><spring:theme code="cat.quote.details.quote.validDate" /></label>
            <div class="input-container">
                <input type="text" uib-datepicker-popup="{{qqdc.view.quote.metaData.dateFormat}}" ng-model="qqdc.view.quote.validTillQuoteEdit.value" name="validTillQuoteEdit" popup-placement="top-left" datepicker-popup-template-url="/_ui/responsive/cat/templates/customDatepickerTemplate.html" on-open-focus="false" is-open="qqdc.view.quote.validTillQuoteEdit.popupOpend" datepicker-options="qqdc.view.quote.metaData.validTillQuoteEditDateOptions" ng-required="true" close-text="Close" ng-click="qqdc.view.quote.validTillQuoteEdit.popupOpend = !qqdc.view.quote.validTillQuoteEdit.popupOpend"  class="cat-inputs type1 valid-until-datepicker" ng-readonly="true"/>
                <span class="calendar-icon" ng-click="qqdc.view.quote.validTillQuoteEdit.popupOpend = !qqdc.view.quote.validTillQuoteEdit.popupOpend"></span>
            </div>    
        </div>
        <div class="col-xs-6 col-md-8 date-created quote-details-col">
            <label class="sub-header" style="display:block;"><spring:theme code="cat.quote.details.quote.status" /><span class="asterik"><spring:theme code="cat.quote.asterisk.symbol"/></span></label>
            <span class="clearfix"></span>

                <div class="btn-group col-xs-8 cat-buttons drop-down-button" uib-dropdown>
                    <button id="sales-model" type="button" class="btn col-xs-12 drop-down-button" uib-dropdown-toggle ng-disabled="disabled" ng-class=" plp.view.salesModel.isDisabled?'disabled':''">
                    <div class="text">{{(qqdc.view.quote.status.selected.value) || '${selectOption}'}}</div>
                    <div class="arrow pull-right">
                        <i class="fa fa-angle-down hidden-xs" aria-hidden="true"></i>
                        <img class="drop-down visible-xs" src="/_ui/responsive/cat/images/drop-down.png">
                    </div>
                    </button>
                    <ul class="dropdown-menu col-xs-12" uib-dropdown-menu role="menu" aria-labelledby="sales-model">
                        <li role="menuitem" ng-repeat="status in qqdc.view.quote.status.list" ng-click="qqdc.setQuoteStatus(status)"><a href="#">{{status.value}}</a></li>
                    </ul>
                </div>
        </div>
    </div>

    <div class="row row-top-border" ng-if="qqdc.view.quoteMetaData.editQuote && qqdc.view.quote.status.selected.type == 'SUBMITED'">
        <div class="col-xs-12 col-md-12 quote-name quote-details-col  description-section no-border">
            <label class="sub-header"><spring:theme code="cat.quote.details.quote.status.submittedToCustomer" /></label>
            <div class="input-container">
                <input type="text" uib-datepicker-popup="{{qqdc.view.quote.metaData.dateFormat}}" ng-model="qqdc.view.quote.submitedToCustomer.value" name="submittedToCustomer" popup-placement="top-left" datepicker-popup-template-url="/_ui/responsive/cat/templates/customDatepickerTemplate.html" on-open-focus="false" is-open="qqdc.view.quote.submitedToCustomer.popupOpend" datepicker-options="qqdc.view.quote.metaData.submittedToCustomerDateOptions" ng-required="true" close-text="Close" ng-click="qqdc.view.quote.submitedToCustomer.popupOpend = !qqdc.view.quote.submitedToCustomer.popupOpend"  class="cat-inputs type1 valid-until-datepicker" ng-readonly="true"/>
                <span class="calendar-icon" ng-click="qqdc.view.quote.submitedToCustomer.popupOpend = !qqdc.view.quote.submitedToCustomer.popupOpend"></span>
            </div>
        </div>
    </div>

    <div class="row row-top-border" ng-if="qqdc.view.quoteMetaData.editQuote && qqdc.view.quote.status.selected.type == 'REJECTED' && qqdc.view.quote.status.selected.value !== 'Rejected by DSM'">
        <div class="col-xs-12 col-md-12 valid-until quote-details-col description-section no-border">
            <label class="sub-header"><spring:theme code="cat.quote.details.quote.reasonQuoteLost" /></label>
                <div class="btn-group col-xs-4 cat-buttons drop-down-button" uib-dropdown keyboard-nav>
                    <button id="sales-model" type="button" class="btn col-xs-12 drop-down-button" uib-dropdown-toggle ng-disabled="disabled" ng-class=" plp.view.salesModel.isDisabled?'disabled':''">
                    <div class="text">{{(qqdc.view.quote.reasonForQuoteLost.selected.value) || '${selectOption}'}}</div>
                    <div class="arrow pull-right">
                        <i class="fa fa-angle-down hidden-xs" aria-hidden="true"></i>
                        <img class="drop-down visible-xs" src="/_ui/responsive/cat/images/drop-down.png">
                    </div>
                    </button>
                    <ul class="dropdown-menu col-xs-12" uib-dropdown-menu role="menu" aria-labelledby="sales-model">
                        <li role="menuitem" ng-repeat="status in qqdc.view.quote.reasonForQuoteLost.list" ng-click="qqdc.setQuoteLostStatus(status)"><a href="#">{{status.value}}</a></li>
                    </ul>
                </div>
        </div>
    </div>

    <div class="row row-top-border" ng-if="(qqdc.view.quoteMetaData.editQuote) && ((qqdc.view.quote.status.selected.type == 'APPROVED' && qqdc.view.quote.status.selected.value !== 'Submitted to DSM') || qqdc.view.quote.status.selected.code == 'BUYER_REJECTED')">
        <div class="col-xs-12 col-md-12 quote-details-col description-section no-border">
            <label class="sub-header"><spring:theme code="cat.quote.details.quote.customerResponseDate" /></label>
            <div class="input-container">
                <input type="text" uib-datepicker-popup="{{qqdc.view.quote.metaData.dateFormat}}" ng-model="qqdc.view.quote.customerResponseData.value" name="customerResponseDate" popup-placement="top-left" datepicker-popup-template-url="/_ui/responsive/cat/templates/customDatepickerTemplate.html" on-open-focus="false" is-open="qqdc.view.quote.customerResponseData.popupOpend" datepicker-options="qqdc.view.quote.metaData.customerResponseDateOptions" ng-required="true" close-text="Close" ng-click="qqdc.view.quote.customerResponseData.popupOpend = !qqdc.view.quote.customerResponseData.popupOpend"  class="cat-inputs type1 valid-until-datepicker" ng-readonly="true" />
                <span class="calendar-icon" ng-click="qqdc.view.quote.customerResponseData.popupOpend = !qqdc.view.quote.customerResponseData.popupOpend"></span>
            </div>
        </div>
    </div>
</div>
