 <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="row visible-xs cat-container quote-mobile-navigation" ng-controller="quotationMobileProgressBar as qmb">
    <div class="col-xs-12 visible-xs no-padding" id="msform">
        <ul id="progressbar">
            <li ng-class="{'active':(qmb.progressBarData.currentPageNum >0)}"><spring:theme code="cat.quote.details.heading.text" /></li>
            <li ng-class="{'active':(qmb.progressBarData.currentPageNum >1)}"><spring:theme code="cat.quote.cart.details.heading.text" /></li>
            <li ng-class="{'active':(qmb.progressBarData.currentPageNum >2)}"><spring:theme code="cat.quote.customer.details.heading.text" /></li>
        </ul>
    </div> 
</div>
