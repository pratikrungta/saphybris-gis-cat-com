<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:url value="/quote/${quoteData.code}/export" var="exportQuoteURL" />
<c:url value="/quote/${quoteData.code}/edit/?firstTimeEdit=false" var="editQuoteURL" />
<div class="quotation-customer-container cat-container row" ng-controller="quotationCustomerDetailsController as qcd">
   <div class="col-xs-12 col-md-12 cart-header-section">
       <span><spring:theme code="cat.quote.details.customer.details" /></span>
   </div>
   <div class="col-xs-12 col-md-12  customer-details-section">
      <div class="col-xs-12 col-md-12 customer-data">
           <div class="row customer-name no-padding customer-data-row">
               <div class="col-xs-12 col-md-2">
                   <label class="left-heading"><spring:theme code="cat.quote.details.customer.name" /></label>
               </div>
               <div class="col-xs-12 col-md-3">
               <div class="value">${quoteData.quoteCustomer.name}</div>
               </div>
           </div>
           <div class="row customer-email no-padding customer-data-row">
               <div class="col-xs-12 col-md-2">
                   <label class="left-heading"><spring:theme code="cat.quote.details.customer.email" /></label>
               </div>
               <div class="col-xs-12 col-md-3">
                   <div class="value">${quoteData.customerEmail}</div>
               </div>
           </div>
           <div class="row shipping-address no-padding customer-data-row">
               <div class="col-xs-12 col-md-2">
                   <label class="left-heading"><spring:theme code="cat.quote.details.customer.shipping.address" /></label>
               </div>
               <div class="col-xs-12 col-md-3 addressValue">
               <c:if test="${quoteData.deliveryAddress ne null }">
               <div class="value">${quoteData.deliveryAddress.line1} ${quoteData.deliveryAddress.line2}</div>
               <div class="value">${quoteData.deliveryAddress.town}</div>
               <div class="value">${quoteData.deliveryAddress.region.name}, ${quoteData.deliveryAddress.postalCode}</div>
               </c:if>
               <c:if test="${quoteData.deliveryAddress eq null }">
               <div class="value">${quoteData.quoteCustomer.defaultShippingAddress.line1} ${quoteData.quoteCustomer.defaultShippingAddress.line2}</div>
               <div class="value">${quoteData.quoteCustomer.defaultShippingAddress.town}</div>
               <div class="value">${quoteData.quoteCustomer.defaultShippingAddress.region.name}, ${quoteData.quoteCustomer.defaultShippingAddress.postalCode}</div>
               </c:if>
               </div>
           </div>
           <div class="row shipping-address no-padding customer-data-row">
               <div class="col-xs-12 col-md-2">
                   <label class="left-heading"><spring:theme code="cat.quote.details.customer.billing.address" /></label>
               </div>
               <div class="col-xs-12 col-md-3 addressValue">
               <c:if test="${quoteData.paymentAddress ne null }">
               <div class="value">${quoteData.paymentAddress.line1} ${quoteData.paymentAddress.line2}</div>
               <div class="value">${quoteData.paymentAddress.town}</div>
               <div class="value">${quoteData.paymentAddress.region.name}, ${quoteData.paymentAddress.postalCode}</div>
               </c:if>
               <c:if test="${quoteData.paymentAddress eq null }">
               <div class="value">${quoteData.quoteCustomer.defaultBillingAddress.line1} ${quoteData.quoteCustomer.defaultBillingAddress.line2}</div>
               <div class="value">${quoteData.quoteCustomer.defaultBillingAddress.town}</div>
               <div class="value">${quoteData.quoteCustomer.defaultBillingAddress.region.name}, ${quoteData.quoteCustomer.defaultBillingAddress.postalCode}</div>
               </c:if>
               </div>
           </div>
           
           <div class="row comments-section no-padding customer-data-row">
               <div class="col-xs-12 col-md-2">
                   <label class="left-heading"><spring:theme code="cat.quote.details.comments" /></label>
                </div>
                <div class="col-xs-12 col-md-8">
                    <c:if test="${not empty quoteData.quoteComments}">
                        ${quoteData.quoteComments}
                    </c:if>
                    <c:if test="${empty quoteData.quoteComments}">
                        -
                    </c:if>
                </div>
           </div>    
      </div>
   </div>
</div>
<div ng-controller="quotationCustomerDetailsController as qcd">
<div class="hidden-xs">
<div class="quotation-submit-container cat-container row cleaefix top-40" ng-init="qcd.initButtonState('${enableEditButton}')">
   <div class="col-md-12 no-padding" clearfix style"">
        
       <c:choose>
           <c:when test="${enableEditButton}">
               <span class="btn cat-buttons primary-btn submit-quote-btn activate-mobile-buttons pull-right" ng-click="qcd.showLostProductPopup('${quoteData.code}');"><spring:theme code="cat.quote.details.editQuote" /></span>
               <a href="${exportQuoteURL}" target="_blank" class="btn cat-buttons secondary-btn download-quote-btn activate-mobile-buttons pull-right" ><span class="exportWordDoccumenttext"><spring:theme code="cat.quote.details.exportDocument" /></span></a>
           </c:when>
           <c:otherwise>
               <button class="btn cat-buttons primary-btn submit-quote-btn activate-mobile-buttons pull-right" ng-disabled="true"><spring:theme code="cat.quote.details.editQuote" /></button>
               <c:choose>
      	     	<c:when test="${isQuoteHasUnapprovedProducts}">
          	     	<a class="btn cat-buttons secondary-btn download-quote-btn activate-mobile-buttons pull-right" ng-disabled="true"><span class="exportWordDoccumenttext"><spring:theme code="cat.quote.details.exportDocument" /></span></a>
           	     	<div class="error-message"><spring:theme code="cat.quote.details.editAndExport.unapprovedProduct.errorMsg" /></div>
           	    </c:when>
           	  	<c:otherwise>
           	  		<a class="btn cat-buttons secondary-btn download-quote-btn activate-mobile-buttons pull-right" href="${exportQuoteURL}" target="_blank"><span class="exportWordDoccumenttext"><spring:theme code="cat.quote.details.exportDocument" /></span></a>
           	  	</c:otherwise>
           	  </c:choose>
           </c:otherwise>
       </c:choose>
   </div>
</div>
</div>
</div>
