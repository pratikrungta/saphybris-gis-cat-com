<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:url value="/" var="homePageUrl" />
<div class="page-header product-compare-header cat-container bottom-40"><spring:theme code="cat.quote.details.quote.details" />
    <div class="pull-right">
        <span class= "homeIcon"><a href="${homePageUrl}" target="_self"><img src="/_ui/responsive/cat/images/fill-300.png"></a></span>
        <span class="backToHomebtn"><a href="${homePageUrl}" target="_self"><spring:theme code="back.to.home" /></a></span>   
    </div>
</div>
<div class="row cat-container quote-details-section-desktop" ng-controller="replicateQuoteDetailsController as rqdc" ng-init="rqdc.setReplicateParam('${enableReplicateButton}','${isQuoteContainsUnpprovedProducts}')">
    <div class="col-xs-12 col-md-12 quote-details-heading-section">
        <div class="heading-text col-md-2">
            <spring:theme code="cat.quote.details.quote.details" />
        </div>
        <div class=" col-md-10 text-align-right hidden-xs">
            <div class="row">
                <div class="error-message" ng-if="!rqdc.view.enableReplicateButton"><spring:theme code="cat.quote.details.replicate.unapprovedProduct.errorMsg1" /></div>
                <div class="error-message" ng-if="!!rqdc.view.enableReplicateButton && rqdc.view.hasSomeUnapprovedProducts"><spring:theme code="cat.quote.details.replicate.unapprovedProduct.errorMsg2" /></div>
                <button class="cat-buttons replicate-quote-btn" 
                        ng-click="rqdc.showLostProductPopup('${quoteData.code}');"
                        ng-disabled="!rqdc.view.enableReplicateButton"
                        ng-class="{'primary-btn':(rqdc.view.enableReplicateButton) ,'secondary-btn':(!rqdc.view.enableReplicateButton),'disabled': (!rqdc.view.enableReplicateButton)}">
                    <spring:theme code="cat.quote.details.quote.replicate" />
                </button>
            </div>
        </div>

    </div>
    <div class="col-xs-6 col-md-4 border-right quote-details-col qouteNumber">
        <label class="sub-header"><spring:theme code="cat.quote.details.quote.number" /></label>
        <label class="value">${quoteData.code}</label>
    </div>
    <div class="col-xs-6 col-md-8 quote-details-col no-border-right">
        <label class="sub-header"><spring:theme code="cat.quote.details.quote.date.created" /></label>
        <label class="value">
	        <fmt:formatDate value="${quoteData.quoteCreatedTime}" dateStyle="medium" pattern="MM-dd-yyyy" type="date"/>
        </label>
    </div>
    <div class="col-xs-12 col-md-4 border-right row-top-border quote-details-col">
        <label class="sub-header pull-left"><spring:theme code="cat.quote.details.quote.name" /></label>
        <div class="clearfix"></div>
        <label class="value">${quoteData.name}</label>
    </div>
    <div class="col-xs-12 col-md-8 row-top-border quote-details-col no-border-right">
        <label class="sub-header pull-left"><spring:theme code="cat.quote.details.quote.modified.date" /></label>
         <div class="clearfix"></div>
         <label class="value"><fmt:formatDate value="${quoteData.updatedTime}" dateStyle="medium" pattern="MM-dd-yyyy" type="date"/></label>  
    </div>
    <div class="col-xs-12 col-md-12 row-top-border quote-details-col description-section no-border-right ">
        <label class="sub-header"><spring:theme code="cat.quote.details.quote.desc" /></label>
        <p class="value hidden-xs">${quoteData.description}</p>
        <label class="value visible-xs">${quoteData.description}</label>
    </div>
    <div class="col-xs-12 col-md-4 border-right row-top-border quote-details-col">
        <label class="sub-header pull-left"><spring:theme code="cat.quote.details.quote.valid.until" /></label>
        <div class="clearfix"></div>
        <label class="value"><fmt:formatDate value="${quoteData.expirationTime}" dateStyle="medium" pattern="MM-dd-yyyy" type="date"/></label>
    </div>
    <div class="col-xs-12 col-md-8 quote-details-col row-top-border no-border-right">
        <label class="sub-header pull-left"><spring:theme code="cat.quote.details.quote.status" /></label>
         <div class="clearfix"></div>
         <label class="value status-label"><spring:theme code="cat.quote.status.code.${quoteData.state.code}"></spring:theme></label>  
    </div>
    <div class="col-xs-12 col-md-4 border-right row-top-border quote-name quote-details-col">
        <label class="sub-header pull-left"><spring:theme code="cat.quote.details.quote.submitted" /></label>
        <div class="clearfix"></div>
        <label class="value">
        <c:if test="${empty quoteData.submittedToCustomerDate}">
        	-
        </c:if>
        <fmt:formatDate value="${quoteData.submittedToCustomerDate}" dateStyle="medium" pattern="MM-dd-yyyy" type="date"/></label>
    </div>
	<c:if test="${quoteData.state.code eq 'BUYER_REJECTED' }">
		<div
			class="col-xs-12 col-md-8 valid-until row-top-border quote-details-col no-border-right">
			<label class="sub-header pull-left"><spring:theme
					code="cat.quote.details.quote.reason.lost" /></label>
			<div class="clearfix"></div>
			<label class="value"> <c:if
					test="${empty quoteData.quoteLostReason}">
        	-
         </c:if> <c:if test="${not empty quoteData.quoteLostReason}">
					<spring:theme
						code="cat.quote.reason.lost.${quoteData.quoteLostReason.code}"></spring:theme>
				</c:if></label>
		</div>
	</c:if>
	<div class="col-xs-12 col-md-12 row-top-border quote-details-col no-border-right">
        <label class="sub-header"><spring:theme code="cat.quote.details.quote.response.date" /></label>
        <label class="value">
        <c:if test="${empty quoteData.acceptedByCustomerDate}">
        	-
        </c:if>
        <fmt:formatDate value="${quoteData.acceptedByCustomerDate}" dateStyle="medium" pattern="MM-dd-yyyy" type="date"/></label>
    </div>
</div>
