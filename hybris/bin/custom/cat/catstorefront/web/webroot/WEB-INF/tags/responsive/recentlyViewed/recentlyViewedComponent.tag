<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="recentlyViewedComponents" tagdir="/WEB-INF/tags/responsive/recentlyViewed" %>


<div class="" ng-controller="recentlyViwedController as rvc" ng-cloak>

    <div class="used-products" ng-show="rvc.catService.recentlyviwed.usedProducts">
        <recentlyViewedComponents:recentlyViewedUsedProducts /> 
    </div>

    <div class="new-products" ng-show="rvc.catService.recentlyviwed.newProducts">
        <recentlyViewedComponents:recentlyViewedNewProducts />
    </div>

</div>