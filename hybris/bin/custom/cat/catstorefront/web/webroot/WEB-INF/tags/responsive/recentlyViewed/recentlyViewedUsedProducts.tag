<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="recently-viewed cat-container row">
    <div class="header col-md-12">
        <spring:theme code="cat.homepage.recentlyviewed.text"/>
    </div>
<c:choose>
<c:when test="${not empty usedProductCollection and not empty usedProductCollection.productCodes}">
<c:set var="fieldLength" value="${usedProductCollection.productCodes.size()}"/>
    <div class="products col-md-12" >
        <div class="row">
            <slider id="sliderForUsedProducts" cards-visible="3" circular class="cat-owl">
                <c:forEach items="${usedProductCollection.productCodes}" varStatus="status">
                <c:set var="product" value="${usedProductCollection.productCodes[fieldLength - status.count]}"/>
                 <c:set value="${product.images[0]}" var="imageUrl"/>
                    <div class="single-card col-xs-6 col-sm-3 item">
                        <cat-card 
                            image="${imageUrl.url}" 
                            primary-header="${product.serialNumber}" 
                            pdp-link="${product.url}" 
                            book-marked="${product.favorite}" 
                            product-type="used"
                            product-id-for-communication="${product.code}"
                            relation="master">
                            </cat-card>
                    </div>
                </c:forEach>
            </slider>
        </div>
    </div>
    </c:when>
    <c:otherwise>
    <div class="empty col-md-12">
        <spring:theme code="cat.homepage.norecentlyviewed.used.text"/>
    </div>
</c:otherwise>
</c:choose>
</div>