<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<div class="row cat-container reorder-cart-page" ng-controller="reorderCartController as rcc" ng-init="rcc.loadReorderCart(); rcc.roleFeatureInfoBCP('${isUTVUser}');">
    <div id="pageLoader" class="page-loader">
            <span class="page-loader-status">Updating...</span>
        </div>
    <div ng-if="!rcc.view.isEmptyCart">
        <div class="col-md-12 page-header-section no-padding">
            <span><spring:theme code="im.reorder.cart.page.cart"/></span>
        </div>
        <div class="col-md-12 back-to-reorder no-padding clearfix hidden-xs" ng-if="rcc.backToReorder()">
            <span class="pull-right">
                <span class="back-to-reorder-arrow"></span>
            <a ng-click="rcc.backToReorderInitiated()" target="_self"
				class="back-to-reorder pull-right" ng-cloak>
						<spring:theme code="reorder.back"/></a></span>
        </div>
        <div class="col-md-12 cart-header-section no-padding">
            <span><spring:theme code="im.reorder.cart.page.cart.details"/></span>
        </div>
        <div class="col-md-12 cart-product-details-table-section no-padding">
                <reorder-cart-table-directive
                        reorder-cart-data = "rcc.view.reorderCartData"
                        po-number-values-array = "rcc.view.poNumberValuesArray"
                        po-number-count-array = "rcc.view.poNumberCountArray"
                        lane-type="rcc.view.cartDataFromResponse.laneTypeData"
                        lane-errors="rcc.laneErrors"
                    >
                </reorder-cart-table-directive>
        </div>
        <div class="col-md-12 total-list-price-section hidden-xs">
            <div class="col-md-10 no-padding">
                <span class="total-list-price-label pull-right"><spring:theme code="im.reorder.cart.page.total.list.price"/></span>
            </div>
            <div class="col-md-2 no-padding">
                <span class="total-list-price-value pull-right">
                {{rcc.view.cartDataFromResponse.totalPrice.formattedValue}}</span>
            </div>
        </div>
        <div class="col-md-12 total-qty-section hidden-xs">
            <div class="col-md-10 no-padding">
                <span class="total-qty-label pull-right"><spring:theme code="im.reorder.cart.page.total.quantity"/></span>
            </div>
            <div class="col-md-2 no-padding">
                <span class="total-qty-value pull-right">
                {{rcc.view.cartDataFromResponse.totalUnitCount}} <spring:theme code="im.reorder.cart.page.units"/></span>
            </div>
        </div>
        <div class="col-xs-12 total-quantity-section visible-xs">
            <div class="col-xs-6 total-quantity-label">
                TOTAL QUANTITY
            </div>
            <div class="col-xs-6 total-quantity-value">
                {{rcc.view.cartDataFromResponse.totalUnitCount}}
            </div>
            <div class="col-xs-6 total-list-price-label">
                TOTAL LIST PRICE
            </div>
            <div class="col-xs-6 total-list-price-value">
                {{rcc.view.cartDataFromResponse.totalPrice.formattedValue}}
            </div>
        </div>
        <div class="col-md-12 reorder-btn-section">
            <div class="col-md-2 col-xs-12 col-md-offset-10">
                <button class="btn cat-buttons primary-btn reorder-cart-button pull-right" ng-click="rcc.validateLaneToReviewOrder();"><spring:theme code="im.reorder.cart.page.reorder"/></button>
            </div>
            <%-- <div class="col-md-2 col-xs-12 reorder-button-container no-padding">
                <button class="btn cat-buttons primary-btn reorder-cart-button hidden-xs" ng-click="rcc.submitToReviewOrder();"><spring:theme code="im.reorder.cart.page.reorder"/></button>
                <div class="col-xs-12 floating-container clearfix visible-xs">
                    <div class="pull-left">
                        <label class="total-heading"><spring:theme code="cat.cart.total.message"/></label>
                        <label class="total-cost">{{rcc.view.cartDataFromResponse.totalPrice.formattedValue}}</label>
                        <label class="total-units">{{rcc.view.cartDataFromResponse.totalUnitCount}} <spring:theme code="im.reorder.cart.page.units"/></label>
                    </div>
                    <div class="pull-right">
                        <button class="btn cat-buttons primary-btn reorder-cart-button pull-right" ng-click="rcc.submitToReviewOrder();"><spring:theme code="im.reorder.cart.page.reorder"/></button>
                    </div>
                </div>
            </div> --%>
        </div>
    </div>
    <div ng-if="rcc.view.isEmptyCart" class="empty-cart">
        <div class="col-md-12 empty-cart-text">
            <spring:theme code="cat.cart.empty.message"/>
            
        </div>
        <div class="col-md-12">
            <button class="btn update-product-list add-products-cart-btn" ng-click="rcc.backToReorderPage();"><spring:theme code="truckload.review.add.produts"/></button>
        </div>
    </div>
</div>