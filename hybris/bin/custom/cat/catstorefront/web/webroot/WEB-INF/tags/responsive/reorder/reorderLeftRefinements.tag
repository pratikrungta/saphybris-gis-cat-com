<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 

<div id="rpc-facet-container">
		
	<div class="cat-search-parameters">
			<div class="params">
			<div class="param-head">
				<span class="upperCase"><spring:theme code="product.refinment.component.filters" /></span>
				<img class="pull-right visible-xs mobile-close-button" src="/_ui/responsive/cat/images/cross.png" 
				ng-click="rpc.closeMoblieFilters()">
			</div>
			<div class="params-sec">
				<div class="params-sec-heading"><spring:theme code="product.refinment.component.product.family" /></div>
					<div class="row params-sec-row">
						<div class="btn-group col-xs-12 params-sec-dropdown" uib-dropdown keyboard-nav="true">
							<button type="button" class="btn col-xs-12 drop-down-button" uib-dropdown-toggle ng-disabled="disabled">
							 <div class="text m-text">{{(rpc.view.productFamily.selectedProductName | titleCase) || '-- Select --'}}</div> 
							<div class="arrow pull-right">
							<i class="fa fa-angle-down" aria-hidden="true"></i>
							</div>
							</button>
							<ul class="dropdown-menu col-xs-12" uib-dropdown-menu role="menu">
								<c:forEach var="category" items="${lowStockCategories}">
								<li role="menuitem" data-productFamily-code="${category.code}" data-productFamily-name="${category.name}" ng-click="rpc.productFamilySelected('${category.code}','${category.name}')"><a href="#">${fn:toLowerCase(category.name)}</a></li>
									<%-- <c:if test="${category.subCategories ne null }">
										<c:forEach var="subCat" items="${category.subCategories}">
											<li role="menuitem" data-productFamily-code="${subCat.code}" data-productFamily-name="${subCat.name}" ng-click="rpc.productFamilySelected('${subCat.code}','${subCat.name}')">${fn:toLowerCase(subCat.name)}</li>
										</c:forEach>
									</c:if> --%>
								</c:forEach> 
							</ul>
						</div>
				  </div>
			</div>
			<div class="params-sec">
				<div class="params-sec-heading"> <spring:theme code="product.refinment.component.sales.model" /></div>
				<div class="row params-sec-row">
					<div class="btn-group col-xs-12 params-sec-dropdown" uib-dropdown keyboard-nav="true">
						<button id="sales-model" type="button" class="btn col-xs-12 drop-down-button" uib-dropdown-toggle ng-disabled="disabled" ng-class=" rpc.view.salesModel.isDisabled?'disabled':''">
						<div class="text m-text">{{(rpc.view.salesModel.selectedModelName) || '- Select -'}}</div>
						<div class="arrow pull-right">
							<i class="fa fa-angle-down" aria-hidden="true"></i>
						</div>
						</button>
						<ul class="dropdown-menu col-xs-12" uib-dropdown-menu role="menu" aria-labelledby="sales-model">
							<li role="menuitem" ng-repeat="salesModel in rpc.view.salesModel.listOfSalesModel track by $index" ng-click="rpc.salesModelSelected(salesModel.code,salesModel.name)"><a href="#">{{salesModel.name}}</a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="update-btn-container">
				<button class="update-btn btn" ng-disabled="!rpc.view.hasValidSearchParams?'disabled':''" ng-click="rpc.updateSearchData()"> <spring:theme code="product.refinment.component.update" /> </button>
			</div>

			</div>
	</div>
</div>