<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="row no-gutters reorder-card-container" ng-init="maintainIsUTVCart(${isUTVCart})">
    <div id="pageLoader" class="page-loader">
        <span class="page-loader-status">Updating...</span>
    </div>
    <div id="plpProductId{{resultIdx}}" class="reorder-card col-xs-12" ng-repeat="(resultIdx, searchObj) in rpc.view.searchResults">
        <!-- <div class="row">
            <div class="col-xs-12 main-header">
                {{searchObj.name}}
            </div>
        </div> -->
        <div class="row card-upper-section">
            <div class="col-sm-12 col-xs-12 img-section">
                <div class="col-xs-12 main-header visible-xs">{{searchObj.name}}</div>
                <div class="col-sm-3 col-xs-12 image">
                    <img ng-src="{{searchObj.images[0].url || rpc.defaultDownloadImage}}">
                </div>
                <div class="col-sm-9 graph-container-plp">
                    <div class="col-sm-4 clearfix">
                        <div class="main-header hidden-xs">{{searchObj.categories[0].name}}</div>
                        <div class="hidden-xs sub-header">{{searchObj.name}}</div>
                    </div>
                    <div class="col-sm-8 graph-align">
                        <inventory-bar-graph ng-if="searchObj.stockingRecommendation !== null" graph="searchObj.stockingRecommendation" ></inventory-bar-graph>
                    </div>
                </div>
            </div>    
            <div class="col-sm-12 col-xs-12 content">
                <!-- <div class="row">
                    <div class="col-xs-12 secondary-header">
                        <spring:theme code="im.reorder.search.results.projected.stock.level" />
                    </div>
                </div> -->
                <!-- <div class="row product-attribs">
                    <div class="col-sm-6 col-xs-4 single-attribs">
                        <div class="text"><spring:theme code="im.reorder.search.results.minimum.inventory" /></div>
                        <div class="value">{{searchObj.minimumStockLevel}} <spring:theme code="im.reorder.search.results.units" /></div>
                    </div>
                    <div class="col-xs-4 visible-xs"></div>
                    <div class="col-sm-6 col-xs-4 single-attribs">
                        <div class="text"><spring:theme code="im.reorder.search.results.maximum.inventory" /></div>
                        <div class="value">{{searchObj.maximumStockLevel}} <spring:theme code="im.reorder.search.results.units" /></div>
                    </div>
                </div>
                
                    <div class="row product-attribsBottom">
                        <div class="col-xs-8 col-sm-6 single-attribs">
                            <div class="text"><span uib-popover-html="'Recommended inventory that should be on hand in 3 months time based on future expected sales.'" popover-placement="top-left" popover-trigger="'mouseenter'" ><spring:theme code="im.reorder.search.results.recommended.inventory" /></span></div>
                            <div class="value">{{searchObj.recommendedStockLevel}} <spring:theme code="im.reorder.search.results.units" /></div>
                        </div>
                    <div class="col-xs-4 col-sm-6 single-attribs">
                        <div class="text"><span  uib-popover-html="'Projected total inventory on-hand in 3 months based on projected sales and orders already placed but not delivered.'" popover-placement="top-left" popover-trigger="'mouseenter'" ><spring:theme code="im.reorder.search.results.projected.inventory" /></span></div>
                        <div class="value">{{searchObj.currentStockLevel}} <spring:theme code="im.reorder.search.results.units" /></div>
                    </div>
                </div> -->
                <div class="hidden-xs row show-config" ng-click="rpc.retriveConfigValues(resultIdx)">
                    <div class="col-xs-12 show-config-cont upperCase">
                        <div class="show-configurations" ng-hide="searchObj.showConfiguration">
                            <div class="d-inline-block">
                                <span><spring:theme code="im.reorder.search.results.show.configuration" /></span>
                                <span class="visible-xs">({{rpc.getFormattedText(searchObj.total)}} <spring:theme code="im.reorder.search.results.units.added" />)</span>
                            </div>
                            <span class="pull-right">
                                <i class="fa fa-2x fa-angle-down" aria-hidden="true"></i>
                            </span>
                        </div>
                        <div class="hide-configurations" ng-hide="!searchObj.showConfiguration">
                            <div class="d-inline-block">
                                <span><spring:theme code="im.reorder.search.results.hide.configuration" /></span>
                                <span class="visible-xs">({{rpc.getFormattedText(searchObj.total)}} <spring:theme code="im.reorder.search.results.units.added" />)</span>
                            </div>
                            <span class="pull-right">
                                <i class="fa fa-2x fa-angle-up" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="cat-show-hide show-config-pannel" ng-show="searchObj.showConfiguration || rpc.mobileMeta.isMobile">
                    <div class="hidden-xs lane-btn-container lane-btn-plp clearfix">
                        <div class="lane-btns col-xs-4" ng-repeat="btn in rpc.view.laneButtons track by $index">
                            <button ng-click="rpc.laneBtnClick(btn, searchObj)" class="cat-buttons secondary-btn" ng-if="searchObj.configProductsList[btn.isSelectedKey].configVariantList && searchObj.configProductsList[btn.isSelectedKey].configVariantList.length" ng-class="{'selectedBtn' : searchObj.laneBtnSelected === btn.isSelectedKey }">{{btn.buttonKey}}<span class="dot" ng-class="{'{{btn.isSelectedKey.toUpperCase()}}-dot' : btn.isSelectedKey}"></span></button>
                        </div>
                    </div>
                    <div class="config-vaient">
                        <div class="row config-row" ng-repeat="(configIdKey, configVariant) in searchObj.configProductsList" ng-if="configIdKey == searchObj.laneBtnSelected || rpc.mobileMeta.isMobile">
                            <div class="visible-xs lane-btn-container lane-btn-plp clearfix">
                                <div ng-if="btn.isSelectedKey === configIdKey && configVariant.configVariantList && configVariant.configVariantList.length" class="lane-btns col-xs-12" ng-repeat="btn in rpc.view.laneButtons track by $index">
                                    <button ng-click="rpc.laneBtnClick(btn, searchObj, 'mobile')" class="cat-buttons secondary-btn">{{btn.buttonKey}}
                                        <span class="dot" ng-class="{'{{btn.isSelectedKey.toUpperCase()}}-dot' : btn.isSelectedKey}"></span>
                                        <span class="pull-right">
                                            <i class="fa" ng-class="configIdKey == searchObj.laneBtnSelected ? 'fa-angle-up' : 'fa-angle-down'" aria-hidden="true"></i>
                                        </span>
                                    </button>
                                </div>
                            </div>
                            <div ng-if="configIdKey !== 'lane1' && configIdKey == searchObj.laneBtnSelected" class="laneBase-info clearfix">
                                <div class="col-xs-10 col-sm-6 no-padding-desktop">
                                    <div class="lane-count" ng-if="configVariant.committedCount >= 0">{{configVariant.committedCount}}</div>
                                    <div class="lane-count" ng-if="configVariant.allocationCount >= 0">{{configVariant.allocationCount}}</div>
                                    <div class="lane-count-msg">
                                        <p class="msg-1"><span ng-if="configIdKey === 'lane2'"><spring:theme code="im.reorder.search.results.lane2.text" /></span><span ng-if="configIdKey === 'lane3'"><spring:theme code="im.reorder.search.results.lane3.text" /></span> 
                                            <span ng-if="configIdKey === 'lane2' && configVariant.orderWindowStatus === 'open'">
                                                <img class="info-icon" src="/_ui/responsive/cat/images/info-icon.svg" uib-popover="<spring:theme code="cat.lane.orderingWindowMessage.closes.lane2" /> {{configVariant.leftDays}} <spring:theme code="cat.lane.orderingWindowMessage.closes.lane.days" />" popover-placement="bottom-center" popover-class="bar-graph-info" popover-trigger="'mouseenter'">
                                            </span>
                                            <span ng-if="configIdKey === 'lane2' && configVariant.orderWindowStatus !== 'open'">
                                                <img class="info-icon" src="/_ui/responsive/cat/images/info-icon.svg" uib-popover="<spring:theme code="cat.lane.orderingWindowMessage.open.lane2" /> {{configVariant.leftDays}} <spring:theme code="cat.lane.orderingWindowMessage.closes.lane.days" />" popover-placement="bottom-center" popover-class="bar-graph-info" popover-trigger="'mouseenter'">
                                            </span>
                                            <span ng-if="configIdKey === 'lane3' && configVariant.orderWindowStatus === 'open'">
                                                <img class="info-icon" src="/_ui/responsive/cat/images/info-icon.svg" uib-popover="<spring:theme code="cat.lane.orderingWindowMessage.closes.lane3" /> {{configVariant.leftDays}} <spring:theme code="cat.lane.orderingWindowMessage.closes.lane.days" />" popover-placement="bottom-center" popover-class="bar-graph-info" popover-trigger="'mouseenter'">
                                            </span>
                                            <span ng-if="configIdKey === 'lane3' && configVariant.orderWindowStatus !== 'open'">
                                                <img class="info-icon" src="/_ui/responsive/cat/images/info-icon.svg" uib-popover="<spring:theme code="cat.lane.orderingWindowMessage.open.lane3" /> {{configVariant.leftDays}} <spring:theme code="cat.lane.orderingWindowMessage.closes.lane.days" />" popover-placement="bottom-center" popover-class="bar-graph-info" popover-trigger="'mouseenter'">
                                            </span>            
                                        </p>
                                        <p ng-if="configIdKey === 'lane2'" class="msg-2"><spring:theme code="im.reorder.search.results.lane2.committed.text" />{{configVariant.month_Year}}</p>
                                        <p ng-if="configIdKey === 'lane3'" class="msg-2"><spring:theme code="im.reorder.search.results.lane3.allocated.text" />{{configVariant.month_Year}}</p>
                                        <p class="visible-xs section-2-msg-1"><spring:theme code="im.reorder.search.results.orderWindowText" />{{configVariant.orderWindowStatus}}</p>
                                        <p class="visible-xs section-2-msg-2" ng-if="configVariant.orderWindowStatus === 'open'"><spring:theme code="im.reorder.search.results.closes.text" />{{configVariant.leftDays}} <spring:theme code="im.reorder.search.results.days" /></p>
                                        <p class="visible-xs section-2-msg-2" ng-if="configVariant.orderWindowStatus === 'closed'"><spring:theme code="im.reorder.search.results.reopens.text" />{{configVariant.leftDays}} <spring:theme code="im.reorder.search.results.days" /></p>
                                    </div>
                                </div>
                                <div class="hidden-xs col-xs-10 col-sm-5 lane-info-section-2">
                                    <p class="section-2-msg-1"><spring:theme code="im.reorder.search.results.orderWindowText" />{{configVariant.orderWindowStatus}}</p>
                                    <p class="section-2-msg-2" ng-if="configVariant.orderWindowStatus === 'open'"><spring:theme code="im.reorder.search.results.closes.text" />{{configVariant.leftDays}} <spring:theme code="im.reorder.search.results.days" /></p>
                                    <p class="section-2-msg-2" ng-if="configVariant.orderWindowStatus === 'closed'"><spring:theme code="im.reorder.search.results.reopens.text" />{{configVariant.leftDays}} <spring:theme code="im.reorder.search.results.days" /></p>
                                </div>
                            </div>
                            <div class="col-xs-12" ng-if="(configIdKey == searchObj.laneBtnSelected || !rpc.mobileMeta.isMobile) && configVariant.configVariantList.length">
                                <div class="col-xs-6 col-sm-3 config-label-1"><spring:theme code="im.reorder.search.results.configurations.text" /></div>
                                <div class="col-xs-6 col-sm-4 config-label-2"><spring:theme code="im.reorder.search.results.qty.text" /></div>
                            </div>
                            <div ng-if="configIdKey == searchObj.laneBtnSelected || !rpc.mobileMeta.isMobile" class="clearfix configVariant-row" ng-repeat="(configIdx, configObj) in configVariant.configVariantList">
                                <div class="col-xs-12">
                                    <div class="row" ng-if="!configObj.isReorderable">
                                        <div class="hidden-xs col-md-1 reorderDiscontinueWarnImage"><img class="warn-iconReorder" src="/_ui/responsive/cat/images/warning.png"></div>
                                        <div class="col-xs-12 col-md-2 discontinuedReorder config-name" ng-click="rpc.navigateToPdp(configObj.url);">
                                            {{configObj.name}}
                                        </div>
                                        <div class="col-xs-1 col-md-1 reorderDiscontinueWarnImage visible-xs"><img class="warn-iconReorder" src="/_ui/responsive/cat/images/warning.png"></div>
                                        <div class="col-xs-11 col-md-9 label warningmsg">
                                        <spring:theme code="im.reorder.search.results.discontinued.products" />
                                        </div>
                                    </div>
                                    <div class="row" ng-if="configObj.isReorderable">
                                        <div class="col-xs-6 col-md-3 config-name" ng-click="rpc.navigateToPdp(configObj.url);">
                                            {{configObj.name}}
                                        </div>
                                        <div class="col-xs-6 col-md-4 opertions" ng-if="!configObj.fbcMustSelectFlag" ng-class="{disable:configObj.isAddedToCart}">
                                           
                                            <div class="input-params">
                                                <span ng-click="configVariant.addToCartDisabled || rpc.validateQuantity(resultIdx, configIdKey, configIdx, 'decrement')" ng-class="!!(configObj.customConfig<=0)?'disabled':''"><img class="decrement" ng-class="!!(configObj.customConfig<=0)?'disabled':''" src="/_ui/responsive/cat/images/minus-icon.png"></span>
                                                <input type="text" size=1 ng-model="configObj.customConfig" restrict-to="[0-9]" ng-change="rpc.validateQuantity(resultIdx, configIdKey, configIdx, 'change')" ng-readonly="configObj.isAddedToCart || configVariant.addToCartDisabled"/> 
                                                <img class="increment" src="/_ui/responsive/cat/images/plus-icon.png" ng-click="configVariant.addToCartDisabled || rpc.validateQuantity(resultIdx, configIdKey, configIdx, 'increment')" ng-class=" (configObj.customConfig !== '00' && !!(configObj.customConfig>=configObj.allowedReorderStockLevel)) || configVariant.addToCartDisabled ?'disabled':''">
                                            </div>
                                        </div>
                                        
                                        <div class="col-xs-6 col-md-4 opertions" ng-if="configObj.fbcMustSelectFlag">
                                            <div class="input-params">
                                                <span ng-click="rpc.validateQuantity(resultIdx, configIdKey, configIdx, 'decrement')" ng-class="!!(configObj.customConfig<=0)?'disabled':''"><img class="decrement" ng-class="!!(configObj.customConfig<=0)?'disabled':''" src="/_ui/responsive/cat/images/minus-icon.png"></span>
                                            	<input type="text" size=1 ng-readonly="configVariant.orderWindowStatus && configVariant.orderWindowStatus =='closed'" ng-model="configObj.customConfig" restrict-to="[0-9]" ng-change="rpc.validateQuantity(resultIdx, configIdKey, configIdx, 'change')"/> 
                                                <img class="increment" src="/_ui/responsive/cat/images/plus-icon.png" ng-click="rpc.validateQuantity(resultIdx, configIdKey, configIdx, 'increment')" ng-disabled="configVariant.orderWindowStatus && configVariant.orderWindowStatus =='closed'" ng-class="(configVariant.orderWindowStatus && configVariant.orderWindowStatus == 'closed')?'disabled':''">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-4">
                                            <button class="btn reorder-btn fixedWidthbtn pull-right" 
                                            ng-click="rpc.addToCart(resultIdx, configIdKey, configIdx,${isUTVCart},rpc.cartType)" 
                                            ng-mousedown="$event.preventDefault();"
                                            ng-class='!(configObj.canReorder)?"disabled":""' ng-if="!configObj.isAddedToCart&&!configObj.fbcMustSelectFlag" 
                                            ng-disabled="!configObj.canReorder"><spring:theme code="im.reorder.search.results.add.to.cart" /></button>
                                            <button class="btn reorder-btn pull-right" 
                                            ng-click="rpc.addToCart(resultIdx, configIdx,${isUTVCart},rpc.cartType)" 
                                            ng-mousedown="$event.preventDefault();"
                                            ng-class='(configObj.isAddedToCart)?"disabled":""' ng-if="configObj.isAddedToCart&&!configObj.fbcMustSelectFlag" ng-disabled="configObj.isAddedToCart"><spring:theme code="im.reorder.search.results.added.to.cart" /></button>
                                            <button class="btn reorder-btn fixedWidthbtn pull-right" 
                                            ng-click="rpc.showSelectOptionsPopup(configObj,searchObj)" 
                                            ng-disabled="configObj.customConfig == 0"
                                            ng-mousedown="$event.preventDefault();"
                                            ng-if="configObj.fbcMustSelectFlag">
                                            <spring:theme code="cat.pdp.fbc.select.options"/></button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-3">

                                        </div>
                                        <div class="col-xs-6 error-message" ng-show="(configObj.wholeNumberError || configObj.maximumQuantityError)">
                                            <div class="user-info">
                                                <div class="whole-number-error" ng-show="(configObj.wholeNumberError)">
                                                    <spring:theme code="im.reorder.invalid.inventory.limit"/>
                                                </div>
                                                <div class="limit-error" ng-show="(configObj.maximumQuantityError)">
                                                    <spring:theme code="im.reorder.max.inventory.limit"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                            <div ng-if="searchObj.laneBtnSelected === configIdKey && configVariant.configVariantList.length" class="total-added-unit visible-xs">
                                <div class="col-md-offset-7 col-md-4 total-unit-count">
                                    <span class=""><spring:theme code="im.reorder.plp.open.bracket"/>{{rpc.getFormattedText(configVariant.totalAddedItem)}} <spring:theme code="im.reorder.search.results.units.added" /><spring:theme code="im.reorder.plp.close.bracket"/></span>
                                </div>
                            </div>
                            <div class="total-added-unit hidden-xs" ng-if="configVariant.configVariantList.length">
                                <div class="col-md-offset-7 col-md-4 total-unit-count">
                                    <span class=""><spring:theme code="im.reorder.plp.open.bracket"/>{{rpc.getFormattedText(configVariant.totalAddedItem)}} <spring:theme code="im.reorder.search.results.units.added" /><spring:theme code="im.reorder.plp.close.bracket"/></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
