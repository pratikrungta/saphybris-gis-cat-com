<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="cat-compare-container hidden-xs product-compare-desktop-view" ng-controller="productCompareController" ng-init="setMetaData('${machineId}','${salesModel}','${imageURL}','${media_prefix_domain}');">
    <div class="row no-gutters compare-page-container" ng-cloak>
        <div class="row cat-container compare-page-loader" ng-show="!ajaxSuccess">
        <div class="cat-loading-spinner speckcheck-loading-compare-spinner"></div>
    </div>
    <div class="page-header product-compare-header rm-pddng-lft"><spring:theme code="cat.speccheck.compare.compareModels"/></div>
    <div class="row">
        <div class="col-md-12 rm-pddng-lft">
       	   <div id="sticky-anchor" class="col-md-12 padding-left-15"></div>
           <div id="sticky-header" class="col-md-12 padding-left-15">
               <div class="units-row col-md-12" ng-if="!scrollableView">
                   <span class="units-label"><spring:theme code="cat.speccheck.compare.units"/></span><span class="label label-default units-type" ng-class="{'us-disabled':(uom === 1)}" ng-click="toggleUnits($event);"><spring:theme code="cat.speccheck.us"/></span><span class="label label-default units-type no-margin-left" ng-class="{'metric-disabled':(uom === 2)}" ng-click="toggleUnits($event);"><spring:theme code="cat.speccheck.compare.metric"/></span>
               </div>
               <div class="units-row selected-products-row col-md-12" ng-class="{'scrollable-shadow':scrollableView}">
                <div class="col-md-3 text-center product-compare-container">
                    <div class="item-detail" ng-if="selectCompetitorView && !scrollableView">
                        <div class="item-image">
                            <img ng-src="{{imageURL || defaultDownloadImage }}" class="product-compare-image" cat-fallback-image>
                        </div>
                        <div class="item-desc">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="machine-manufacturer"><spring:theme code="cat.speccheck.compare.caterpillar"/></div>
                                </div>
                                <div class="col-md-12">
                                    <div class="engine-model-heading"><spring:theme code="cat.speccheck.compare.salesModel"/></div>
                                    <div class="engine-model-value">{{salesModel}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item-detail" ng-if="!selectCompetitorView && !scrollableView">
                        <div class="item-image">
                        	<img ng-if="isUTVProduct" ng-src="{{machineArray[0].Filename  || defaultDownloadImage}}" class="product-compare-image" cat-fallback-image>
                            <img ng-if="!isUTVProduct" ng-src="{{mediaPrefix}}{{machineArray[0].Filename  || defaultDownloadImage}}" class="product-compare-image" cat-fallback-image>
                        </div>
                        <div class="item-desc">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="machine-manufacturer"><spring:theme code="cat.speccheck.compare.caterpillar"/></div>
                                </div>
                                <div class="col-md-12">
                                    <div class="engine-model-heading"><spring:theme code="cat.speccheck.compare.salesModel"/></div>
                                    <div class="engine-model-value">{{salesModel}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
		    <div class="item-detail scrollable-view col-md-12" ng-if="scrollableView">
                        <div class="item-image pull-left">
                        	<img ng-if="isUTVProduct" ng-src="{{machineArray[0].Filename || defaultDownloadImage}}" class="product-compare-image" cat-fallback-image>
                            <img ng-if="!isUTVProduct" ng-src="{{mediaPrefix}}{{machineArray[0].Filename || defaultDownloadImage}}" class="product-compare-image" cat-fallback-image> 
                        </div>
                        <div class="item-desc pull-right">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="machine-manufacturer"><spring:theme code="cat.speccheck.compare.caterpillar"/></div>
                                </div>
                                <div class="col-md-12">
                                    <div class="engine-model-value">{{salesModel}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
                <div class="col-md-3 text-center product-compare-container">
                    <div class="item-detail" ng-if="selectCompetitorView && !scrollableView">
                        <div class="col-md-12 no-padding">
						<div class="col-md-12 product-family-dropdown-header no-padding competitor-dropdown-header"><spring:theme code="cat.speccheck.compare.selectModelToCompare"/></div>
                        <span uib-dropdown class="product-family-btn col-md-12"  on-toggle="dropdownToggled('first')">
							<a class="col-md-10 product-family-dropdown" href uib-dropdown-toggle>
								<label class="product-family-dropdown-label" ng-if="productCompareDropDownLabel1 == dropdownLabel">{{productCompareDropDownLabel1}}</label> 
                                <label class="product-family-dropdown-label" ng-if="productCompareDropDownLabel1 != dropdownLabel">{{productCompareDropDownLabel1 | uppercase}}</label> 
								<img class="pull-right product-family-dropdown-arrow" ng-if="!dropdown1Open" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"  />
								<img class="pull-right product-family-dropdown-arrow" ng-if="dropdown1Open" ng-src="/_ui/responsive/cat/images/dropdown-arrow-up.png" />
								<span class="clearfix"></span>
							</a>
							<ul class="col-md-10 dropdown-menu product-family-dropdown-menu" uib-dropdown-menu>
								<li class="product-family-dropdown-item dropdown1" ng-repeat="category in productCategories">
                                    <a class="dropdown-group-header" ng-if="$index === 0 || (productCategories[$index-1].Manufacturer !== productCategories[$index].Manufacturer)">{{category.Manufacturer | initcaps}}</a>
                                    <a class="dropdown-value" ng-class="{'disable-option' : (category.Machine_ID === machineId3 || category.Machine_ID === machineId2 || category.Machine_ID === machineId)}" href="#" ng-click="updateDropDownValue($event,category);" ng-disabled="category.Machine_ID === machineId2 || category.Machine_ID === machineId3 || category.Machine_ID === machineId">{{category.Model | uppercase}}</a>
                                </li>
							</ul>
						</span>   
					    </div>
                    </div>
                    <div class="item-detail" ng-if="!selectCompetitorView && productCompareDropDownLabel1 !== dropdownLabel && !scrollableView">
                        <div class="item-image">
                        	<img ng-if="isUTVProduct" ng-src="{{machineArray[1].Filename || defaultDownloadImage}}" class="product-compare-image" cat-fallback-image>
                            <img ng-if="!isUTVProduct" ng-src="{{mediaPrefix}}{{machineArray[1].Filename || defaultDownloadImage}}" class="product-compare-image" cat-fallback-image>
                        </div>
                        <div class="item-desc">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="machine-manufacturer">{{competitor1 | initcaps}}</div>
                                </div>
                                <div class="col-md-12">
                                    <div class="engine-model-heading"><spring:theme code="cat.speccheck.compare.salesModel"/></div>
                                    <div class="engine-model-value">{{productCompareDropDownLabel1 | uppercase}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
		    <div class="item-detail scrollable-view col-md-12" ng-if="scrollableView && productCompareDropDownLabel1 !== dropdownLabel">
                        <div class="item-image pull-left">
                        	<img ng-if="isUTVProduct" ng-src="{{machineArray[1].Filename || defaultDownloadImage}}" class="product-compare-image-small" cat-fallback-image>
                            <img ng-if="!isUTVProduct" ng-src="{{mediaPrefix}}{{machineArray[1].Filename || defaultDownloadImage}}" class="product-compare-image-small" cat-fallback-image>
                        </div>
                        <div class="item-desc pull-right">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="machine-manufacturer">{{competitor1 | initcaps}}</div>
                                </div>
                                <div class="col-md-12">
                                    <div class="engine-model-value">{{productCompareDropDownLabel1 | uppercase}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="item-detail scrollable-view col-md-12" ng-if="scrollableView && productCompareDropDownLabel1 === dropdownLabel">
                        <div class="pull-left">
                            <div class="no-product-text-scroll"><spring:theme code="cat.speccheck.compare.noProductSelected"/></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
		    <div class="item-detail" ng-if="!scrollableView && !selectCompetitorView && productCompareDropDownLabel1 === dropdownLabel">
                        <div class="no-product-text"><spring:theme code="cat.speccheck.compare.noProductSelected"/></div>
                    </div>
                </div>
                <div class="col-md-3 text-center product-compare-container">
                    <div class="item-detail" ng-if="selectCompetitorView && !scrollableView">
                        <div class="col-md-12 no-padding">
						<div class="col-md-12 product-family-dropdown-header no-padding competitor-dropdown-header"><spring:theme code="cat.speccheck.compare.selectModelToCompare"/></div>
                        <span uib-dropdown class="product-family-btn col-md-12"  on-toggle="dropdownToggled('second');">
							<a class="col-md-10 product-family-dropdown" ng-class="{'disabled-dropdown':(productCompareDropDownLabel1 === dropdownLabel)}" href uib-dropdown-toggle ng-disabled="productCompareDropDownLabel1 === dropdownLabel">
								<label class="product-family-dropdown-label" ng-class="{'disabled-dropdown':(productCompareDropDownLabel1 === dropdownLabel)}" ng-if="productCompareDropDownLabel2 == dropdownLabel"> {{productCompareDropDownLabel2}}</label> 
                                <label class="product-family-dropdown-label" ng-class="{'disabled-dropdown':(productCompareDropDownLabel1 === dropdownLabel)}" ng-if="productCompareDropDownLabel2 != dropdownLabel"> {{productCompareDropDownLabel2 | uppercase}}</label>
								<img class="pull-right product-family-dropdown-arrow" ng-if="!dropdown2Open" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"/>
								<img class="pull-right product-family-dropdown-arrow" ng-if="dropdown2Open" ng-src="/_ui/responsive/cat/images/dropdown-arrow-up.png"/>
								<span class="clearfix"></span>
							</a>
							<ul class="col-md-10 dropdown-menu product-family-dropdown-menu" uib-dropdown-menu>
								<li class="product-family-dropdown-item dropdown2" ng-repeat="category in productCategories">
                                    <a class="dropdown-group-header" ng-if="$index === 0 || (productCategories[$index-1].Manufacturer !== productCategories[$index].Manufacturer)">{{category.Manufacturer | initcaps}}</a>
                                    <a class="dropdown-value" ng-class="{'disable-option' : (category.Machine_ID === machineId1 || category.Machine_ID === machineId3 || category.Machine_ID === machineId)}" href="#" ng-click="updateDropDownValue($event,category);" ng-disabled="category.Machine_ID === machineId1 || category.Machine_ID === machineId3 || category.Machine_ID === machineId">{{category.Model | uppercase}}</a>
                                </li>
							</ul>
						</span>   
					    </div>
                    </div>
                    <div class="item-detail" ng-if="!selectCompetitorView && productCompareDropDownLabel2 !== dropdownLabel && !scrollableView">
                        <div class="item-image">
                        	<img ng-if="isUTVProduct" ng-src="{{machineArray[2].Filename || defaultDownloadImage}}" class="product-compare-image" cat-fallback-image>
                            <img ng-if="!isUTVProduct" ng-src="{{mediaPrefix}}{{machineArray[2].Filename || defaultDownloadImage}}" class="product-compare-image" cat-fallback-image>
                        </div>
                        <div class="item-desc">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="machine-manufacturer">{{competitor2 | initcaps}}</div>
                                </div>
                                <div class="col-md-12">
                                    <div class="engine-model-heading"><spring:theme code="cat.speccheck.compare.salesModel"/></div>
                                    <div class="engine-model-value">{{productCompareDropDownLabel2 | uppercase}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
		    <div class="item-detail scrollable-view col-md-12" ng-if="scrollableView && productCompareDropDownLabel2 !== dropdownLabel">
                        <div class="item-image pull-left">
                        	<img ng-if="isUTVProduct" ng-src="{{machineArray[2].Filename || defaultDownloadImage}}" class="product-compare-image" cat-fallback-image>
                            <img ng-if="!isUTVProduct" ng-src="{{mediaPrefix}}{{machineArray[2].Filename || defaultDownloadImage}}" class="product-compare-image" cat-fallback-image>
                        </div>
                        <div class="item-desc pull-right">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="machine-manufacturer">{{competitor2 | initcaps}}</div>
                                </div>
                                <div class="col-md-12">
                                    <div class="engine-model-value">{{productCompareDropDownLabel2 | uppercase}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="item-detail scrollable-view col-md-12" ng-if="scrollableView && productCompareDropDownLabel2 === dropdownLabel">
                        <div class="pull-left">
                            <div class="no-product-text-scroll"><spring:theme code="cat.speccheck.compare.noProductSelected"/></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
		    <div class="item-detail" ng-if="!scrollableView && !selectCompetitorView && productCompareDropDownLabel2 === dropdownLabel">
                        <div class="no-product-text"><spring:theme code="cat.speccheck.compare.noProductSelected"/></div>
                    </div>
                </div>
                <div class="col-md-3 text-center product-compare-container">
                    <div class="item-detail" ng-if="selectCompetitorView && !scrollableView">
                        <div class="col-md-12 no-padding">
						<div class="col-md-12 product-family-dropdown-header no-padding competitor-dropdown-header"><spring:theme code="cat.speccheck.compare.selectModelToCompare"/></div>
                        <span uib-dropdown class="product-family-btn col-md-12" on-toggle="dropdownToggled('third');">
							<a class="col-md-10 product-family-dropdown" ng-class="{'disabled-dropdown':(productCompareDropDownLabel2 === dropdownLabel)}" href uib-dropdown-toggle ng-disabled="productCompareDropDownLabel2 === dropdownLabel">
								<label class="product-family-dropdown-label" ng-class="{'disabled-dropdown':(productCompareDropDownLabel2 === dropdownLabel)}" ng-if="productCompareDropDownLabel3 == dropdownLabel">{{productCompareDropDownLabel3}} </label> 
                                <label class="product-family-dropdown-label" ng-class="{'disabled-dropdown':(productCompareDropDownLabel2 === dropdownLabel)}" ng-if="productCompareDropDownLabel3 != dropdownLabel">{{productCompareDropDownLabel3 | uppercase}} </label> 
								<img class="pull-right product-family-dropdown-arrow" ng-if="!dropdown3Open" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"/>
								<img class="pull-right product-family-dropdown-arrow" ng-if="dropdown3Open" ng-src="/_ui/responsive/cat/images/dropdown-arrow-up.png"/>
								<span class="clearfix"></span>
							</a>
							<ul class="col-md-10 dropdown-menu product-family-dropdown-menu" uib-dropdown-menu>
								<li class="product-family-dropdown-item dropdown3" ng-repeat="category in productCategories">
                                    <a class="dropdown-group-header" ng-if="$index === 0 || (productCategories[$index-1].Manufacturer !== productCategories[$index].Manufacturer)">{{category.Manufacturer | initcaps}}</a>
                                    <a class="dropdown-value" href="#" ng-class="{'disable-option' : (category.Machine_ID === machineId1 || category.Machine_ID === machineId2 || category.Machine_ID === machineId)}" ng-click="updateDropDownValue($event,category);" ng-disabled="category.Machine_ID === machineId1 || category.Machine_ID === machineId2 || category.Machine_ID === machineId">{{category.Model | uppercase}}</a>
                                </li>
							</ul>
						</span>   
					    </div>
                    </div>
                    <div class="item-detail" ng-if="!selectCompetitorView && productCompareDropDownLabel3 !== dropdownLabel && !scrollableView">
                        <div class="item-image">
                        	<img ng-if="isUTVProduct" ng-src="{{machineArray[3].Filename || defaultDownloadImage}}" cat-fallback-image class="product-compare-image">
                            <img ng-if="!isUTVProduct" ng-src="{{mediaPrefix}}{{machineArray[3].Filename || defaultDownloadImage}}" cat-fallback-image class="product-compare-image">
                        </div>
                        <div class="item-desc">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="machine-manufacturer">{{competitor3 | initcaps}}</div>
                                </div>
                                <div class="col-md-12">
                                    <div class="engine-model-heading"><spring:theme code="cat.speccheck.compare.salesModel"/></div>
                                    <div class="engine-model-value">{{productCompareDropDownLabel3 | uppercase}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
		    <div class="item-detail scrollable-view col-md-12" ng-if="scrollableView && productCompareDropDownLabel3 !== dropdownLabel">
                        <div class="item-image pull-left">
                        	<img ng-if="isUTVProduct" ng-src="{{machineArray[3].Filename || defaultDownloadImage}}" class="product-compare-image" cat-fallback-image>
                            <img ng-if="!isUTVProduct" ng-src="{{mediaPrefix}}{{machineArray[3].Filename || defaultDownloadImage}}" class="product-compare-image" cat-fallback-image>
                        </div>
                        <div class="item-desc pull-right">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="machine-manufacturer">{{competitor3 | initcaps}}</div>
                                </div>
                                <div class="col-md-12">
                                    <div class="engine-model-value">{{productCompareDropDownLabel3 | uppercase}}</div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="item-detail scrollable-view col-md-12" ng-if="scrollableView && productCompareDropDownLabel3 === dropdownLabel">
                        <div class="pull-left">
                            <div class="no-product-text-scroll"><spring:theme code="cat.speccheck.compare.noProductSelected"/></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="item-detail" ng-if="!scrollableView && !selectCompetitorView && productCompareDropDownLabel3 === dropdownLabel">
                        <div class="no-product-text"><spring:theme code="cat.speccheck.compare.noProductSelected"/></div>
                    </div>  
                </div>
                </div>
                <div class="update-button units-row col-md-12 compare-toggle-button" ng-if="!scrollableView">
                    <button class="btn update-product-list pull-right" ng-click="toggleCompareView($event);" ng-disabled="productCompareDropDownLabel1 === dropdownLabel">
                        <span ng-if="selectCompetitorView"><spring:theme code="cat.speccheck.compare"/></span>
                        <span ng-if="!selectCompetitorView"><spring:theme code="cat.speccheck.compare.changeSelection"/></span>
                    </button>
		            <button class="btn update-product-list reset-selection pull-right" ng-if="selectCompetitorView" ng-disabled="productCompareDropDownLabel1 === dropdownLabel" ng-click="resetView();">
                        <span><spring:theme code="cat.speccheck.compare.reset"/></span>
                    </button>
                    <div class="clearfix"></div>
                </div>
        </div>
        <div class="col-md-12 product-compare-accordion padding-left-15" ng-if="!selectCompetitorView && ajaxSuccess">
           <uib-accordion>
                <div uib-accordion-group class="panel-default" is-open="specs.AccordionOpen" ng-repeat="specs in specsAccordionData">
                    <uib-accordion-heading>
                      <a data-toggle="collapse" ng-class="{'expanded': (specs.AccordionOpen === true),'collapsed':(specs.AccordionOpen === false)}" ng-click="updateAccordionIndex($index);">
                        {{specs.AccordionHeader.Label}}
                      </a>
                    </uib-accordion-heading>
                    <div class="panel-body">
                       <div ng-repeat="object in specs.AccordionContent" class="col-md-12 accordion-row" ng-class="{'scrollable-shadow':scrollableView,'odd-row':$odd,'even-row':$even}">    
                        <div class="col-md-12 column-header">{{object.Label}}</div>  
                        <div ng-repeat="data in object.Data" class="col-md-3 accordion-col no-padding" ng-class="($last ? 'no-border' : '')">    
                            <div class="col-md-12 column-padding grey-border-right" ng-if="data['Column '+$index] | isNotArray">
                                <div class="col-md-12 column-value">{{data['Column '+$index] | noSpace}} <span ng-if="object.Unit && data['Column '+$index] !== ''">({{object.Unit}})</span></div>
                            </div>
                            <div class="col-md-12 column-padding grey-border-right" ng-if="data['Column '+$index] | isArray">
                                <div ng-repeat = "col in data['Column '+$index]" class="col-md-12 no-horizontal-padding media-cols">
                                    <div class="column-value">{{col.Filename}}</div>
                                </div>
                            </div>
                        </div>
                       </div>
                    </div>
                </div>
            </uib-accordion>
    </div>
</div>
</div>
</div>
</div>
