<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="cat-main-content">
<div class="visible-xs product-compare-mobile-view" ng-controller="productCompareController" ng-init="setMetaData('${machineId}','${salesModel}','${imageURL}','${media_prefix_domain}');" ng-cloak>
<div class="cat-container row">
   <div class="page-header product-compare-header"><spring:theme code="cat.speccheck.compare.compareModels"/></div>
   <div class="col-xs-12 carousel-container">
        <div class="units-row col-xs-12" ng-if="!scrollableViewMobile">
            <span class="label label-default units-type" ng-class="{'us-disabled':(uom === 1)}" ng-click="toggleUnits($event);"><spring:theme code="cat.speccheck.us"/></span><span class="label label-default units-type no-margin-left" ng-class="{'metric-disabled':(uom === 2)}" ng-click="toggleUnits($event);"><spring:theme code="cat.speccheck.compare.metric"/></span>
        </div>
        <div id="sticky-anchor-mobile" class="col-xs-12"></div>
        <div id="sticky-header-mobile" class="col-xs-12">
            <slick  scroll-y="scrollY" infinite="false" speed=300 slides-to-show=2 touch-move="false" slides-to-scroll=1 class="slider one-time col-xs-12 no-padding">
                <div class="col-xs-3 text-center product-compare-container">
                            <div class="item-detail" ng-if="selectCompetitorViewMobile && !scrollableViewMobile">
                                <div class="item-image">
                                    <img ng-src="{{imageURL || defaultDownloadImage}}" class="product-compare-image" cat-fallback-image>
                                </div>
                                <div class="item-desc">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="machine-manufacturer"><spring:theme code="cat.speccheck.compare.caterpillar"/></div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="engine-model-heading"><spring:theme code="cat.speccheck.compare.salesModel"/></div>
                                            <div class="engine-model-value">{{salesModel}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item-detail no-padding-top" ng-if="!selectCompetitorViewMobile && !scrollableViewMobile">
                                <div class="item-image">
                                	<img ng-if="isUTVProduct" ng-src="{{machineArray[0].Filename || defaultDownloadImage}}" class="product-compare-image" cat-fallback-image> 
									<img ng-if="!isUTVProduct" ng-src="{{mediaPrefix}}{{machineArray[0].Filename || defaultDownloadImage}}" class="product-compare-image" cat-fallback-image>
                                </div>
                                <div class="item-desc">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="machine-manufacturer"><spring:theme code="cat.speccheck.compare.caterpillar"/></div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="engine-model-heading"><spring:theme code="cat.speccheck.compare.salesModel"/></div>
                                            <div class="engine-model-value">{{salesModel}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item-detail scrollable-view col-xs-12" ng-if="scrollableViewMobile">
                                <div class="item-desc">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="machine-manufacturer"><spring:theme code="cat.speccheck.compare.caterpillar"/></div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="engine-model-value">{{salesModel}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-xs-3 text-center product-compare-container">
                            <div class="item-detail" ng-if="selectCompetitorViewMobile && !scrollableViewMobile">
                                <div class="col-xs-12 no-padding">
                                <div class="col-xs-7 product-family-dropdown-header no-padding competitor-dropdown-header"><spring:theme code="cat.speccheck.compare.selectModelToCompare"/></div>
                                <span uib-dropdown class="product-family-btn col-xs-12" on-toggle="dropdownToggled('first')">
                                    <a class="col-xs-11 product-family-dropdown" href uib-dropdown-toggle>
                                        <label class="product-family-dropdown-label" ng-if="productCompareDropDownLabel1 == 'Select'">{{productCompareDropDownLabel1}}</label>
                                        <label class="product-family-dropdown-label" ng-if="productCompareDropDownLabel1 != 'Select'">{{productCompareDropDownLabel1 | uppercase}}</label>  
                                        <img class="pull-right product-family-dropdown-arrow" ng-if="!dropdown1Open" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"/>
                                        <img class="pull-right product-family-dropdown-arrow" ng-if="dropdown1Open" ng-src="/_ui/responsive/cat/images/dropdown-arrow-up.png"/>
                                        <span class="clearfix"></span>
                                    </a>
                                    <ul class="col-xs-11 dropdown-menu product-family-dropdown-menu" uib-dropdown-menu>
                                        <li class="product-family-dropdown-item dropdown1" ng-repeat="category in productCategories">
                                            <a class="dropdown-group-header" ng-if="$index === 0 || (productCategories[$index-1].Manufacturer !== productCategories[$index].Manufacturer)">{{category.Manufacturer | initcaps}}</a>
                                            <a class="dropdown-value" href="#" ng-class="{'disable-option' : (category.Machine_ID === machineId3 || category.Machine_ID === machineId2 || category.Machine_ID === machineId)}" ng-click="updateDropDownValue($event,category);" ng-disabled="category.Machine_ID === machineId2 || category.Machine_ID === machineId3 || category.Machine_ID === machineId">{{category.Model | uppercase}}</a>
                                        </li>
                                    </ul>
                                </span>   
                                </div>
                            </div>
                            <div class="item-detail no-padding-top" ng-if="!selectCompetitorViewMobile && productCompareDropDownLabel1 !== 'Select' && !scrollableViewMobile" >
                                <div class="item-image">
                                	<img ng-if="isUTVProduct" ng-src="{{machineArray[1].Filename || defaultDownloadImage}}" class="product-compare-image" cat-fallback-image> 
									<img ng-if="!isUTVProduct" ng-src="{{mediaPrefix}}{{machineArray[1].Filename || defaultDownloadImage}}" class="product-compare-image" cat-fallback-image>
                                </div>
                                <div class="item-desc">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="machine-manufacturer">{{competitor1 | initcaps}}</div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="engine-model-heading"><spring:theme code="cat.speccheck.compare.salesModel"/></div>
                                            <div class="engine-model-value">{{productCompareDropDownLabel1 | uppercase}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item-detail no-padding clearfix" ng-if="!selectCompetitorViewMobile && productCompareDropDownLabel1 == 'Select' && !scrollableViewMobile" >
                                <div class="pull-left"><div class="no-product-text-scroll"><spring:theme code="cat.speccheck.compare.noProductSelected"/></div></div>
                            </div>
                            <div class="item-detail scrollable-view col-xs-12" ng-if="scrollableViewMobile && productCompareDropDownLabel1 !== 'Select'">
                                <div class="item-desc">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="machine-manufacturer">{{competitor1 | initcaps}}</div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="engine-model-value">{{productCompareDropDownLabel1 | uppercase}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item-detail scrollable-view col-xs-12" ng-if="scrollableViewMobile && productCompareDropDownLabel1 === 'Select'">
                                <div class="pull-left">
                                    <div class="no-product-text-scroll"><spring:theme code="cat.speccheck.compare.noProductSelected"/></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-xs-3 text-center product-compare-container">
                            <div class="item-detail" ng-if="selectCompetitorViewMobile && !scrollableViewMobile">
                                <div class="col-xs-12 no-padding">
                                <div class="col-xs-7 product-family-dropdown-header no-padding competitor-dropdown-header"><spring:theme code="cat.speccheck.compare.selectModelToCompare"/></div>
                                <span uib-dropdown class="product-family-btn col-xs-12" on-toggle="dropdownToggled('second');">
                                    <a class="col-xs-11 product-family-dropdown" ng-class="{'disabled-dropdown':(productCompareDropDownLabel1 === 'Select')}" href uib-dropdown-toggle ng-disabled="productCompareDropDownLabel1 === 'Select'">
                                        <label class="product-family-dropdown-label" ng-class="{'disabled-dropdown':(productCompareDropDownLabel1 === 'Select')}" ng-if="productCompareDropDownLabel2 == 'Select'"> {{productCompareDropDownLabel2}}</label> 
                                        <label class="product-family-dropdown-label" ng-class="{'disabled-dropdown':(productCompareDropDownLabel1 === 'Select')}" ng-if="productCompareDropDownLabel2 != 'Select'"> {{productCompareDropDownLabel2 | uppercase}}</label> 
                                        <img class="pull-right product-family-dropdown-arrow" ng-if="!dropdown2Open" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"/>
                                        <img class="pull-right product-family-dropdown-arrow" ng-if="dropdown2Open" ng-src="/_ui/responsive/cat/images/dropdown-arrow-up.png"/>
                                        <span class="clearfix"></span>
                                    </a>
                                    <ul class="col-xs-11 dropdown-menu product-family-dropdown-menu" uib-dropdown-menu>
                                        <li class="product-family-dropdown-item dropdown2" ng-repeat="category in productCategories">
                                            <a class="dropdown-group-header" ng-if="$index === 0 || (productCategories[$index-1].Manufacturer !== productCategories[$index].Manufacturer)">{{category.Manufacturer | initcaps}}</a>
                                            <a class="dropdown-value" href="#" ng-class="{'disable-option' : (category.Machine_ID === machineId1 || category.Machine_ID === machineId3 || category.Machine_ID === machineId)}" ng-click="updateDropDownValue($event,category);" ng-disabled="category.Machine_ID === machineId1 || category.Machine_ID === machineId3 || category.Machine_ID === machineId">{{category.Model | uppercase}}</a>
                                        </li>
                                    </ul>
                                </span>   
                                </div>
                            </div>
                            <div class="item-detail no-padding-top" ng-if="!selectCompetitorViewMobile && productCompareDropDownLabel2 !== 'Select' && !scrollableViewMobile">
                                <div class="item-image">
                                	<img ng-if="isUTVProduct" ng-src="{{machineArray[2].Filename || defaultDownloadImage}}" class="product-compare-image" cat-fallback-image> 
									<img ng-if="!isUTVProduct" ng-src="{{mediaPrefix}}{{machineArray[2].Filename || defaultDownloadImage}}" class="product-compare-image" cat-fallback-image>
                                </div>
                                <div class="item-desc">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="machine-manufacturer">{{competitor2 | initcaps}}</div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="engine-model-heading"><spring:theme code="cat.speccheck.compare.salesModel"/></div>
                                            <div class="engine-model-value">{{productCompareDropDownLabel2 | uppercase}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item-detail no-padding clearfix" ng-if="!selectCompetitorViewMobile && productCompareDropDownLabel2 == 'Select' && !scrollableViewMobile" >
                                <div class="pull-left"><div class="no-product-text-scroll"><spring:theme code="cat.speccheck.compare.noProductSelected"/></div></div>
                            </div>
                            <div class="item-detail scrollable-view col-xs-12" ng-if="scrollableViewMobile && productCompareDropDownLabel2 !== 'Select'">
                                <div class="item-desc">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="machine-manufacturer">{{competitor2 | initcaps}}</div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="engine-model-value">{{productCompareDropDownLabel2 | uppercase}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item-detail scrollable-view col-xs-12" ng-if="scrollableViewMobile && productCompareDropDownLabel2 === 'Select'">
                                <div class="pull-left">
                                    <div class="no-product-text-scroll"><spring:theme code="cat.speccheck.compare.noProductSelected"/></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-xs-3 text-center product-compare-container">
                            <div class="item-detail" ng-if="selectCompetitorViewMobile && !scrollableViewMobile">
                                <div class="col-xs-12 no-padding">
                                <div class="col-xs-7 product-family-dropdown-header no-padding competitor-dropdown-header"><spring:theme code="cat.speccheck.compare.selectModelToCompare"/></div>
                                <span uib-dropdown class="product-family-btn col-xs-12" on-toggle="dropdownToggled('third');">
                                    <a class="col-xs-11 product-family-dropdown" ng-class="{'disabled-dropdown':(productCompareDropDownLabel2 === 'Select')}" href uib-dropdown-toggle ng-disabled="productCompareDropDownLabel2 === 'Select'">
                                        <label class="product-family-dropdown-label" ng-class="{'disabled-dropdown':(productCompareDropDownLabel2 === 'Select')}" ng-if="productCompareDropDownLabel3 == 'Select'">{{productCompareDropDownLabel3}} </label> 
                                        <label class="product-family-dropdown-label" ng-class="{'disabled-dropdown':(productCompareDropDownLabel2 === 'Select')}" ng-if="productCompareDropDownLabel3 != 'Select'">{{productCompareDropDownLabel3 | uppercase}} </label>
                                        <img class="pull-right product-family-dropdown-arrow" ng-if="!dropdown3Open" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"/>
                                        <img class="pull-right product-family-dropdown-arrow" ng-if="dropdown3Open" ng-src="/_ui/responsive/cat/images/dropdown-arrow-up.png"/>
                                        <span class="clearfix"></span>
                                    </a>
                                    <ul class="col-xs-11 dropdown-menu product-family-dropdown-menu" uib-dropdown-menu>
                                        <li class="product-family-dropdown-item dropdown3" ng-repeat="category in productCategories">
                                            <a class="dropdown-group-header" ng-if="$index === 0 || (productCategories[$index-1].Manufacturer !== productCategories[$index].Manufacturer)">{{category.Manufacturer | initcaps}}</a>
                                            <a class="dropdown-value" href="#" ng-class="{'disable-option' : (category.Machine_ID === machineId1 || category.Machine_ID === machineId2 || category.Machine_ID === machineId)}" ng-click="updateDropDownValue($event,category);" ng-disabled="category.Machine_ID === machineId1 || category.Machine_ID === machineId2 || category.Machine_ID === machineId">{{category.Model | uppercase}}</a>
                                        </li>
                                    </ul>
                                </span>   
                                </div>
                            </div>
                            <div class="item-detail no-padding-top" ng-if="!selectCompetitorViewMobile && productCompareDropDownLabel3 !== 'Select' && !scrollableViewMobile">
                                <div class="item-image">
                                	<img ng-if="isUTVProduct" ng-src="{{machineArray[3].Filename || defaultDownloadImage}}" class="product-compare-image" cat-fallback-image> 
									<img ng-if="!isUTVProduct" ng-src="{{mediaPrefix}}{{machineArray[3].Filename || defaultDownloadImage}}" class="product-compare-image" cat-fallback-image>
                                </div>
                                <div class="item-desc">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="machine-manufacturer">{{competitor3 | initcaps}}</div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="engine-model-heading"><spring:theme code="cat.speccheck.compare.salesModel"/></div>
                                            <div class="engine-model-value">{{productCompareDropDownLabel3 | uppercase}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           <div class="item-detail no-padding clearfix" ng-if="!selectCompetitorViewMobile && productCompareDropDownLabel3 == 'Select' && !scrollableViewMobile" >
                                <div class="pull-left"><div class="no-product-text-scroll"><spring:theme code="cat.speccheck.compare.noProductSelected"/></div></div>
                            </div>
                            <div class="item-detail scrollable-view col-xs-12" ng-if="scrollableViewMobile && productCompareDropDownLabel3 !== 'Select'">
                                <div class="item-desc">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="machine-manufacturer">{{competitor3 | initcaps}}</div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="engine-model-value">{{productCompareDropDownLabel3 | uppercase}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item-detail scrollable-view col-xs-12" ng-if="scrollableViewMobile && productCompareDropDownLabel3 === 'Select'">
                                <div class="pull-left">
                                    <div class="no-product-text-scroll"><spring:theme code="cat.speccheck.compare.noProductSelected"/></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
            </slick>

            <button class="btn update-product-list reset-selection col-xs-12" ng-if="selectCompetitorViewMobile" ng-disabled="productCompareDropDownLabel1 === 'Select'" ng-click="resetView();">
                <span><spring:theme code="cat.speccheck.compare.reset"/></span>
            </button>
            <button class="btn update-product-list col-xs-12" ng-class="{'change-selection-btn-mobile':!selectCompetitorViewMobile}" ng-click="toggleCompareView($event);" ng-disabled="productCompareDropDownLabel1 === 'Select'" ng-if="!scrollableViewMobile">
                <span ng-if="selectCompetitorViewMobile"><spring:theme code="cat.speccheck.compare"/></span>
                <span ng-if="!selectCompetitorViewMobile"><spring:theme code="cat.speccheck.compare.changeSelection"/></span>
            </button>
        </div>
    </div>
</div>
<div class="col-xs-12 product-compare-accordion no-padding" ng-if="!selectCompetitorViewMobile">
           <uib-accordion>
                <div uib-accordion-group class="panel-default" is-open="$first" ng-repeat="specs in specsAccordionData">
                    <uib-accordion-heading>
                      <a data-toggle="collapse" ng-class="{'expanded': ($first || status.open),'collapsed':!status.open}">
                        {{specs.AccordionHeader.Label}}
                      </a>
                    </uib-accordion-heading>
                    <div class="panel-body">
                       <div ng-repeat="object in specs.AccordionContent" class="col-xs-12 accordion-row" ng-class="{'odd-row':$odd,'even-row':$even}">    
                        <div class="col-xs-12 column-header">{{object.Label}}</div>  
                        <div ng-repeat="data in object.Data" class="col-xs-6 accordion-col no-padding" ng-class="((visibleSlideArray.indexOf($index) === 1) ? 'no-border' : '')" ng-show="visibleSlideArray.indexOf($index) > -1">    
                            <div class="col-xs-12 column-padding grey-border-right" ng-if="data['Column '+$index] | isNotArray">
                                <div class="col-xs-11 column-value" ng-class="{'no-padding':$first}">{{data['Column '+$index] | noSpace}} <span ng-if="object.Unit && data['Column '+$index] !== ''">({{object.Unit}})</span></div>
                            </div>
                        </div>
                       </div>
                    </div>
                </div>
            </uib-accordion>
        </div>
        </div>
    </div>
