<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div ng-controller="speckcheckProductListController as plc" ng-init="!isMobile&&plc.roleFeatureInfoCompare('${isUTVUser}','${isIM}','${isSP}');" class="compare-plp-desktop-view hidden-xs">
	<div class="row no-gutters" ng-cloak>
		<div class="row cat-container page-loader" ng-show="!plc.ajaxSuccess">
			<div class="cat-loading-spinner speckcheck-loading-plp-spinner"></div>
		</div>
		<div class="product-list-header cat-container row">
			<span><spring:theme code="cat.speccheck.plp.productList"/></span>
		</div>
		<div class="productlist-pagination-container-top cat-container row">
			<div class=" pull-right">
				<ul uib-pagination boundary-links="true" total-items="plc.pagination.totalItems" ng-model="plc.pagination.currentPage" 
				items-per-page="plc.pagination.elementsForCurrentPage" ng-change="plc.pageChanged()" 
				class="pagination-sm pull-right" 
				previous-text="&laquo;" next-text="&raquo;" first-text="" last-text="" max-size="3" 
				ng-if="plc.pagination.totalItems > 9"></ul>
			</div>
		</div>
		<div class="speckcheck-product-list cat-container row">
			<div class="col-md-12 product-list-container no-padding">
				<div class="col-md-3 left-filter-section">
					<div class="col-md-12 filters-section no-padding">
						<div class="filter-header col-md-12 no-padding">
							<span class="col-md-12 no-padding"><spring:theme code="cat.speccheck.plp.filters"/></span>
							<span class="col-md-3 filter-divider"></span>
						</div>

						<div class="params-sec col-xs-12">
							<div class="params-sec-heading"><spring:theme code="cat.speccheck.plp.productFamily"/></div>
							<div class="row params-sec-row">
								<div class="btn-group col-xs-12 params-sec-dropdown" uib-dropdown>
									<button id="sales-model" type="button" class="btn col-xs-12 drop-down-button" uib-dropdown-toggle ng-disabled="disabled" ng-class=" plp.view.salesModel.isDisabled?'disabled':''">
										<div class="text">{{plc.productCompareDropDownLabel.name | capitalize}}</div>
										<div class="arrow pull-right">
											<i class="fa fa-angle-down hidden-xs" aria-hidden="true"></i>
											<img class="drop-down visible-xs" src="/_ui/responsive/cat/images/drop-down.png"/>
										</div>
									</button>
									<ul class="dropdown-menu col-xs-12" uib-dropdown-menu role="menu" aria-labelledby="sales-model">
										<li role="menuitem" ng-repeat="category in plc.productCategories" ng-click="plc.updateDropDownValue(category);">{{category.name | capitalize}}</li>
									</ul>
								</div>
							</div>
						</div>


						<div class="update-button col-md-12 no-padding">
							<button class="btn update-product-list" ng-click="plc.updateProductsList();"><spring:theme code="product.update"/></button>
						</div>
					</div>
				</div>
				<div class="col-md-9 right-products-section">
					<div class="col-md-12 product-cards-section" ng-if="plc.productList.length > 0">
						<div class="row">
							 <div class="col-md-4" ng-repeat="product in plc.productList|limitTo: plc.upperLimit: plc.lowerLimit">
								<product-compare-card
									header-code="{{product.code}}"
									product-name="{{plc.showProductName}}"
									image="{{product.images[0].url || plc.defaultDownloadImage}}"
									product-desc="{{product.description}}"
									engine-model="{{product.featureEngineModel}}"
									net-power="{{product.featureNetPower}}"
									operating-weight="{{product.featureOperatingWeight}}"
									price = "{{product.price.formattedValue}}"
									fuel-type = "{{product.fuelType}}"
									max-speed="{{product.maxSpeed}}"
									seating-capacity = "{{product.seatCapacity}}"
									book-marked="false">
								</product-compare-card> 
							</div> 
						</div>
					</div>
					<div class="col-md-12 message" ng-if="plc.productListToDisplay.length == 0"><spring:theme code="cat.speccheck.plp.noResults"/></div>
				</div>
			</div>
		</div>
		<div class="productlist-pagination-container-bottom cat-container row">
			<div class=" pull-right">
				<ul uib-pagination boundary-links="true" total-items="plc.pagination.totalItems" 
				ng-model="plc.pagination.currentPage" items-per-page="plc.pagination.elementsForCurrentPage"
				 ng-change="plc.pageChanged()" 
				class="pagination-sm pull-right" previous-text="&laquo;" next-text="&raquo;" 
				first-text="" last-text="" max-size="3" ng-if="plc.pagination.totalItems > 9"></ul>
			</div>
		</div>
	</div>
</div>
