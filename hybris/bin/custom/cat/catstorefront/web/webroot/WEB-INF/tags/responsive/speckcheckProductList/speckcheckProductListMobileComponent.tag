<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div ng-controller="speckcheckProductListController as plc" ng-init="isMobile&&plc.roleFeatureInfoCompare('${isUTVUser}','${isIM}','${isSP}');" class="compare-plp-mobile-view cat-container row visible-xs">
    <div class="product-list-header cat-container row clearfix">
		<span class="pull-left"><spring:theme code="cat.speccheck.plp.productList"/></span>
        <button class="btn pull-right filters-button" ng-click="plc.toggleSidebar($event);"><spring:theme code="cat.speccheck.plp.filters.small"/></button>
        <div id="sidebar-wrapper" ng-class="{'active':plc.showSideBar}">
            <div class="col-xs-12">
                <div class="pull-left">
                    <span class="filters-heading"><spring:theme code="cat.speccheck.plp.filters"/></span>
                    <span class="filter-divider"></span>
                </div>
                <div class="pull-right">
                    <span class="close-modal" ng-click="plc.toggleSidebar($event);"></span>
                </div>
            </div>
            <div class="col-xs-12 inventory-heading-row">
                <span class="inventory-heading"><spring:theme code="cat.speccheck.plp.inventoryType"/></span>
            </div>
            <div class="col-xs-12 product-dropdown-row">
                    <div class="col-xs-12 product-family-dropdown-header no-padding competitor-dropdown-header"><spring:theme code="cat.speccheck.plp.productFamily"/></div>
                    <span uib-dropdown class="product-family-btn col-xs-12">
						<a class="col-xs-12 product-family-dropdown clearfix" href uib-dropdown-toggle>
							<label class="product-family-dropdown-label">{{plc.productCompareDropDownLabel.name | initcaps}}</label> 
							<img class="pull-right product-family-dropdown-arrow" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"/>
						</a>
						<ul class="col-xs-12 dropdown-menu product-family-dropdown-menu" uib-dropdown-menu>
                            <li role="menuitem" class="product-family-dropdown-item" ng-repeat="category in plc.productCategories" ng-click="plc.updateDropDownValue(category);">{{category.name | initcaps}}</li>
						</ul>
					</span>   
			</div>
            <div class="update-button col-xs-12 no-padding">
				<button class="btn update-product-list" ng-click="plc.updateProductsList();"><spring:theme code="product.update"/></button>
			</div>      
        </div>
	</div>
    <div class="product-cards-container cat-container row">
        <div class="col-xs-12 product-cards-section">
            <div class="row" ng-repeat="product in plc.productListToDisplayMobile">
                <product-compare-card-mobile
                    header-code="{{product.code}}"
                    product-name="{{plc.showProductName}}"
                    image="{{product.images[0].url}}"
                    product-desc="{{product.description}}"
                    engine-model="{{product.featureEngineModel}}"
                    net-power="{{product.featureNetPower}}"
                    operating-weight="{{product.featureOperatingWeight}}"
                    price = "{{product.price.formattedValue}}"
					fuel-type = "{{product.fuelType}}"
					max-speed="{{product.maxSpeed}}"
					seating-capacity = "{{product.seatCapacity}}">
                </product-compare-card-mobile>
            </div>
        </div>
    </div>
    <div class="show-more-container cat-container row" ng-if="plc.noOfPagesInMobileView < plc.productList.length">
        <span class="show-more" ng-click="plc.loadMoreProducts();"><spring:theme code="cat.speccheck.plp.showMore"/></span>
    </div>
</div>
