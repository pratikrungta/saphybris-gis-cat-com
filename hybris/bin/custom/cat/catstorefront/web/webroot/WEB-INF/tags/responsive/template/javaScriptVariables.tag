<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>

<%-- JS configuration --%>
	<script type="text/javascript">
		/*<![CDATA[*/
		<%-- Define a javascript variable to hold the content path --%>
		var ACC = { config: {},localization:{}};
			ACC.config.contextPath = "${contextPath}";
			ACC.config.encodedContextPath = "${encodedContextPath}";
			ACC.config.commonResourcePath = "${commonResourcePath}";
			ACC.config.themeResourcePath = "${themeResourcePath}";
			ACC.config.siteResourcePath = "${siteResourcePath}";
			ACC.config.rootPath = "${siteRootUrl}";	
			ACC.config.CSRFToken = "${CSRFToken.token}";
			ACC.pwdStrengthVeryWeak = '<spring:theme code="password.strength.veryweak" />';
			ACC.pwdStrengthWeak = '<spring:theme code="password.strength.weak" />';
			ACC.pwdStrengthMedium = '<spring:theme code="password.strength.medium" />';
			ACC.pwdStrengthStrong = '<spring:theme code="password.strength.strong" />';
			ACC.pwdStrengthVeryStrong = '<spring:theme code="password.strength.verystrong" />';
			ACC.pwdStrengthUnsafePwd = '<spring:theme code="password.strength.unsafepwd" />';
			ACC.pwdStrengthTooShortPwd = '<spring:theme code="password.strength.tooshortpwd" />';
			ACC.pwdStrengthMinCharText = '<spring:theme code="password.strength.minchartext"/>';
			ACC.accessibilityLoading = '<spring:theme code="aria.pickupinstore.loading"/>';
			ACC.accessibilityStoresLoaded = '<spring:theme code="aria.pickupinstore.storesloaded"/>';
			ACC.config.googleApiKey="${googleApiKey}";
			ACC.config.googleApiVersion="${googleApiVersion}";
			ACC.localization.EPPCSAUnsaved='<spring:theme code="cat.quote.cart.item.EPPCSAUnsaved"/>'
			ACC.localization.EPPOverflow='<spring:theme code="cat.quote.cart.item.EPPOverflow"/>';
			ACC.localization.CSAOverflow='<spring:theme code="cat.quote.cart.item.CSAOverflow"/>';
			ACC.localization.EPPCSAOverflow='<spring:theme code="cat.quote.cart.item.EPPCSAOverflow"/>';
			ACC.localization.inventoryAgeError='<spring:theme code="pdp.inventoryAge.serialNumber.oos" />';
			ACC.localization.serialNumberLabel ='<spring:theme code="pdp.inTransitInfo.serialNumber.text"/>';
			ACC.localization.estimatedDeliveryDateLabel = '<spring:theme code="pdp.inTransitInfo.estimatedDeliveryDate.text" />'
			ACC.localization.inventoryAgeLabel = '<spring:theme code="pdp.inventoryAge.age" />';
			ACC.localization.pdpMessageOutOfStock = '<spring:theme code="pdp.message.outofstock"/>';
			ACC.localization.inventoryAddressText='<spring:theme code="pdp.inventory.address.text" />';
			ACC.localization.inventoryQuantityText='<spring:theme code="pdp.inventory.quantity.text" />';
			ACC.localization.shippingAddressErrMsg='<spring:theme code="cat.review.order.shippingAddress.errMsg" />';
			ACC.localization.noResultsFound='<spring:theme code="cat.review.order.noResults" />';
			ACC.localization.Lane1='<spring:theme code="cat.lane.orderingWindowMessage.lane1" />';
			ACC.localization.Lane2='<spring:theme code="cat.lane.orderingWindowMessage.lane2" />';
			ACC.localization.Lane3='<spring:theme code="cat.lane.orderingWindowMessage.lane3" />';
			ACC.localization.plp1='<spring:theme code="im.reorder.search.results.lane1.text" />';
			ACC.localization.plp2='<spring:theme code="im.reorder.search.results.lane2.text" />';
			ACC.localization.plp3='<spring:theme code="im.reorder.search.results.lane3.text" />';
			ACC.localization.LaneMsg='<spring:theme code="cat.lane.orderingWindowMessage.toolTipm1" />';
			ACC.localization.minimum='<spring:theme code="cat.lane.orderingWindowMessage.minimum" />';
			ACC.localization.recommended='<spring:theme code="cat.lane.orderingWindowMessage.recommended" />';
			ACC.localization.current='<spring:theme code="cat.lane.orderingWindowMessage.current" />';
			ACC.localization.fbcMustSelectHeading = 'Add MANDATORY parts/attachments to your';
			ACC.localization.fbcReviewPartsHeading = 'REVIEW ADDED ITEMS';
			ACC.localization.fbcReviewPartsFeatureCode = 'FEATURE CODE';
			ACC.localization.fbcReviewPartsReferenceNo = 'REFERENCE#';
			ACC.localization.fbcReviewPartsQuantity = 'QUANTITY';
			ACC.localization.fbcReviewPartsPartDesc = 'PART DESCRIPTION';
			ACC.localization.fbcReviewPartsAssemblyIndicator = 'A/I';
			ACC.localization.fbcReviewPartsLN = 'L/N';
			ACC.localization.fbcReviewPartsPriceEach = 'PRICE EACH';
			ACC.localization.fbcReviewPartsTotalPrice = 'TOTAL PRICE';
			ACC.localization.fbcReviewPartsTotalParts = 'TOTAL ITEMS';
			ACC.localization.fbcReviewPartsTotalListPrice = 'TOTAL LIST PRICE';
			ACC.localization.fbcReviewPartsAddToCart = 'ADD TO CART';
			ACC.localization.fbcErrorText = '.If this issue persists please contact our help desk. Please contact retail_digital_enablement@cat.com for additional details';
			ACC.localization.truckloadConfigOptionsHeader = 'Configuration Options';
			ACC.localization.truckloadConfigSelectionHeader = 'Please select any one of the 6 options';
			ACC.localization.truckloadConfigSetOneSeaterHeading = '2 Seat Units';
			ACC.localization.truckloadConfigSetOneSeatCount = '(8 per truck)';
			ACC.localization.truckloadConfigSetOneOptionsHeading = 'Options - Set 1';
			ACC.localization.truckloadConfigSetOneOption1 = '8 x CUV82';
			ACC.localization.truckloadConfigSetOneOption2 = '8 x CUV102D';
			ACC.localization.truckloadConfigSetOneOption3 = '4 x CUV82 and  4 x CUV102D';
			ACC.localization.truckloadConfigSetTwoSeaterHeading = '5 Seat Units';
			ACC.localization.truckloadConfigSetTwoSeatCount = '(6 per truck)';
			ACC.localization.truckloadConfigSetTwoOptionsHeading = 'Options - Set 2';
			ACC.localization.truckloadConfigSetTwoOption1 = '6 x CUV85';
			ACC.localization.truckloadConfigSetTwoOption2 = '6 x CUV105D';
			ACC.localization.truckloadConfigSetTwoOption3 = '3 x CUV85 and 3 x CUV105D';

			<c:if test="${request.secure}"><c:url value="/search/autocompleteSecure"  var="autocompleteUrl"/></c:if>
			<c:if test="${not request.secure}"><c:url value="/search/autocomplete"  var="autocompleteUrl"/></c:if>
			ACC.autocompleteUrl = '${autocompleteUrl}';

			<c:url value="/login" var="loginUrl"/>
			ACC.config.loginUrl = '${loginUrl}';

			<c:url value="/authentication/status" var="authenticationStatusUrl"/>
			ACC.config.authenticationStatusUrl = '${authenticationStatusUrl}';

			<c:forEach var="jsVar" items="${jsVariables}">
				<c:if test="${not empty jsVar.qualifier}" >
					ACC.${jsVar.qualifier} = '${jsVar.value}';
				</c:if>
			</c:forEach>
		/*]]>*/
	</script>
	<template:javaScriptAddOnsVariables/>
	
	<%-- generated variables from commonVariables.properties --%>
	<script type="text/javascript" src="${sharedResourcePath}/js/generatedVariables.js"></script>