<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div class="col-xs-12 col-md-12 truckload-container" ng-controller="truckloadConfiguratorController as tcc" ng-init="tcc.loadTruckLoadData('${userEmailId}');tcc.roleFeatureinfoTruckLoad();'${isUTVUser}'" ng-cloak>
    <div class="col-xs-12 col-md-12 page-header-section no-padding">
       <label><spring:theme code="cat.truckload.configurator" /></label>
    </div>
    <div class="col-xs-12 col-md-12 truckload-configurator-header-section hidden-xs">
       <label><spring:theme code="cat.configure.truckload" /></label>
    </div>
    <div class="col-xs-12 col-md-12 truckload-config-disclaimer">
        <spring:theme code="cat.truckload.config.matrix.info" />
    </div>
    <div class="col-xs-12 col-md-12 truckload-config-options-header">
       {{tcc.truckloadConfigOptionsHeader}}
    </div>
    <div class="col-xs-12 col-md-12 truckload-config-options-row">
       <div class="col-xs-12 col-md-12 no-padding config-selection-header">{{tcc.truckloadConfigSelectionHeader}}</div>
       <div class="col-xs-6 col-md-3 set-1-col no-padding">
            <p class="seater-heading">{{tcc.truckloadConfigSetOneSeaterHeading}} <span class="seat-count">{{tcc.truckloadConfigSetOneSeatCount}}</span></p>
            <p class="option-heading">{{tcc.truckloadConfigSetOneOptionsHeading}}</p>
            <ul>
                <li>{{tcc.truckloadConfigSetOneOption1}}</li>
                <li>{{tcc.truckloadConfigSetOneOption2}}</li>
                <li>{{tcc.truckloadConfigSetOneOption3}}</li>
            </ul>
       </div>
       <div class="col-xs-6 col-md-3 set-2-col no-padding">
            <p class="seater-heading">{{tcc.truckloadConfigSetTwoSeaterHeading}} <span class="seat-count">{{tcc.truckloadConfigSetTwoSeatCount}}</span></p>
            <p class="option-heading">{{tcc.truckloadConfigSetTwoOptionsHeading}}</p>
            <ul>
                <li>{{tcc.truckloadConfigSetTwoOption1}}</li>
                <li>{{tcc.truckloadConfigSetTwoOption2}}</li>
                <li>{{tcc.truckloadConfigSetTwoOption3}}</li>
            </ul>
       </div>
    </div>
	<div class="col-xs-12 col-md-12 truckload-config-accordion no-padding">
        <uib-accordion>
            <div uib-accordion-group class="panel-default" is-open="true">
                <uib-accordion-heading>
                  <a data-toggle="collapse" class="pull-left">
                    <spring:theme code="cat.truckload.config.matrix" />
                  </a>
                  <span class="accordion-arrow pull-right"></span>
                  <div class="clearfix"></div>
                </uib-accordion-heading>
                <div class="panel-body">
                   <div class="col-xs-12 col-md-12 truckload-config-matrix-accordion-body no-padding">
					
					<table class="col-xs-12 col-md-12 truckload-matrix-table">
                            <thead>
                                    <tr class="hidden-xs">
                                        <th><spring:theme code="cat.truckload.config.machineType" /></th>
										<th ng-repeat="rows in tcc.truckloadConfigMatrix"><spring:theme code="cat.truckload.config.truckLoad" /></th>
                                    </tr>
                            </thead>
                            <tbody>
                                    <tr>
                                        <td><span><spring:theme code="cat.truckload.config.two.seat" /></span></td>
										<td ng-repeat="rows in tcc.truckloadConfigMatrix"><span>{{rows.twoSeaterCapacity}}</span></td>
									</tr>
									<tr>
										<td><span><spring:theme code="cat.truckload.config.five.seat" /></span></td>
										<td ng-repeat="rows in tcc.truckloadConfigMatrix"><span>{{rows.fiveSeaterCapacity}}</span></td>
									</tr>
									<tr class="hidden-xs">
										<td></td>
										<td ng-repeat="rows in tcc.truckloadConfigMatrix"></td>
                                    </tr>
                            </tbody>
                       </table>
                   </div>
                </div>
            </div>
        </uib-accordion>
    </div>
    <div ng-repeat="(truckIndex,object) in tcc.lineItemArray track by $index" class="col-xs-12 col-md-12 truckload-config-lineitem no-padding">
       <div id="truckloadForm{{truckIndex}}">
        <div class="col-xs-12 col-md-12 truckload-config-data-holder">
            <div class="col-xs-12 col-md-12 truck-heading-section no-padding">
                <label class="truckload-index-label pull-left"><spring:theme code="cat.truckload.config.truck" /> {{truckIndex+1}}</label>
                <label class="truckload-remove-button pull-right"  ng-if="tcc.lineItemArray.length > 1" ng-click="tcc.removeTruckloadLineItem(truckIndex);"><span class="remove-text"><spring:theme code="cat.truckload.config.remove" /></span><span class="remove-icon"></span></label>
            </div>
            <div class="col-xs-12 col-md-12 truckload-shipping-date-section no-padding">
               <label class="ship-after-date-label"><spring:theme code="cat.truckload.config.shipAfterDate" /></label>
               <span uib-dropdown class="product-family-btn col-xs-12 col-md-12">
                    <a class="col-xs-12 col-md-4 product-family-dropdown" href uib-dropdown-toggle>
                        <label class="product-family-dropdown-label">{{object.shipAfterDateLabel}}</label> 
                        <img class="pull-right product-family-dropdown-arrow" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"/>
                        <span class="clearfix"></span>
                    </a>
                    <ul class="col-xs-12 col-md-4 product-family-dropdown-menu" uib-dropdown-menu>
                        <li class="product-family-dropdown-item" ng-repeat="(dateCode,dateObj) in tcc.shipAfterDatesArray" ng-click = "tcc.setShipAfterDate(dateCode,dateObj,truckIndex);">
                            <a class="dropdown-value" href="#">{{dateObj}}</a>
                        </li>
                    </ul>
                    <input type="text" name="shipAfterDate" class="hidden-input" ng-model="object.shipAfterDateCode"/>
                    <input type="text" name="truckloadId" class="hidden-input" ng-model="object.truckloadId"/>
                </span>
                <div class="product-not-available">
                    {{object.productNotAvailableMsg}}
                </div>
                <div class="col-xs-12 col-md-12 truckload-error no-padding" ng-if="object.shipAfterDateError">
                    <spring:theme code="configure.truckload.select.shipping.date.message"/> 
                </div>  
            </div>
            <div class="col-xs-12 col-md-12 no-padding truckload-products-row">
                    <div ng-repeat="(cuvIndex,cuvProduct) in tcc.cuvProductsList" class="col-xs-6 col-md-3 truckload-product-card" ng-class="{'is-available-blur' : !object.cuvProductsArray[cuvIndex].isAvailable, 'is-available-blur-IE' : (!object.cuvProductsArray[cuvIndex].isAvailable && tcc.isIE) }">
                        <label class="sales-model">{{cuvProduct.name}} <span class="seat-details">({{cuvProduct.seatCapacity}} <spring:theme code="cat.truckload.config.seat" />)</span></label>
                        <div class="truckload-product-image">
                            <img ng-src="{{cuvProduct.images[0].url || tcc.defaultDownloadImage}}" cat-fallback-image/>
                        </div>
                        <div class="truckload-quantity-section">
                            <label class="quantity-label"><spring:theme code="cat.truckload.config.quantity"/></label>
                            <span class="decrement" ng-class="(object.cuvProductsArray[cuvIndex].quantities == 0) || (object.cuvProductsArray[cuvIndex].isAvailable && object.cuvProductsArray[cuvIndex].quantities == 0)? 'disabled' : ''" ng-click="tcc.changeTruckLoadQty('-',truckIndex,cuvIndex);"></span>
                            <input ng-readonly="!object.cuvProductsArray[cuvIndex].isAvailable" type="text" name="cuvQty" class="qty-textbox text-center" ng-model="object.cuvProductsArray[cuvIndex].quantities" restrict-model-input="^[0-9]+$" ng-blur="tcc.enableDisableAddToTruckBtn(truckIndex,cuvIndex);"/>
                            <input type="text" name="quantities" class="hidden-input" ng-model="object.cuvProductsArray[cuvIndex].quantities"/>
                            <input type="text" name="productCodes" class="hidden-input" ng-model="object.cuvProductsArray[cuvIndex].productCodes"/>
                            <input type="text" name="productNames" class="hidden-input" ng-model="object.cuvProductsArray[cuvIndex].productNames"/>
                            <input type="text" ng-if="cuvProduct.seatCapacity == '2'" name="twoSeaterQty" class="hidden-input" ng-model="object.cuvProductsArray[cuvIndex].quantities" />
                            <input type="text" ng-if="cuvProduct.seatCapacity == '5'" name="fiveSeaterQty" class="hidden-input" ng-model="object.cuvProductsArray[cuvIndex].quantities"/>
                            <span class="increment" ng-class="(!object.cuvProductsArray[cuvIndex].isAvailable || object.cuvProductsArray[cuvIndex].quantities >= cuvProduct.shippingFactor || !object.cuvProductsArray[cuvIndex].correctCombination || object.maxCUVQtyReached) ? 'disabled' : ''" ng-click="object.cuvProductsArray[cuvIndex].isAvailable && tcc.changeTruckLoadQty('+',truckIndex,cuvIndex);"></span>

                        </div>
                    </div>
            </div> 
            <div class="col-xs-12 col-md-12 no-padding truckload-info-section">
                <div class="col-xs-12 col-md-3 add-to-truckload-box no-padding visible-xs">
                    <div class="truckload-button-wrapper">
                        <button type="button" ng-if="object.truckButtonType == 'ADD'" ng-disabled="object.addToTruckDisable" ng-class="(object.addToTruckDisable)?'disabled':''" class="btn update-product-list add-to-truckload-btn" ng-click="tcc.addProductsToTruck(truckIndex);"><spring:theme code="cat.truckload.config.addToTruck" /></button>
                        <button type="button" ng-if="object.truckButtonType == 'UPDATE'" ng-disabled="object.addToTruckDisable" ng-class="(object.addToTruckDisable)?'disabled':''" class="btn update-product-list add-to-truckload-btn" ng-click="tcc.updateTruckProducts(truckIndex);"><spring:theme code="configure.truckload.update.truck"/></button> 
                    </div>
                </div>
                <div class="col-xs-12 col-md-9 truckload-error less-error no-padding" ng-if="object.addToTruckResponse != undefined && object.addToTruckResponse.errorMessage == 'LESSER'">
                    <spring:theme code="configure.truckload.less.message"/>
                </div>
                <div class="col-xs-12 col-md-9 truckload-error more-error no-padding" ng-if="object.addToTruckResponse != undefined && object.addToTruckResponse.errorMessage == 'GREATER'">
                    <spring:theme code="configure.truckload.more.message"/>
                </div>
                <div class="col-xs-12 col-md-9 truckload-success no-padding" ng-if="object.addToTruckSuccessMsg">
                    <spring:theme code="configure.truckload.success.message"/>
                </div>
                <div class="col-xs-12 col-md-9 truckload-info-box">
                    <label class="truckload-info-label no-padding"><spring:theme code="cat.truckload.config.info.heading" /></label>
                    <div class="col-xs-12 col-md-4 truckload-qty-info no-padding hidden-xs" ng-if="object.totalUnits == 0">
                        <p> <spring:theme code="cat.truckload.config.info.message.emptyTruck" /></p>
                        <p> <spring:theme code="cat.truckload.config.info.message.selectQty" /></p>
                    </div>
                    <div class="col-xs-12 col-md-4 truckload-qty-info no-padding hidden-xs" ng-if="object.totalUnits > 0">
                        <ul>
                            <li ng-repeat="cuvProduct in object.cuvProductsArray track by $index" ng-if="cuvProduct.quantities > 0" class="clearfix truckload-info-list">
                                <span class="pull-left header">{{cuvProduct.productNames}}</span>
                                <span class="pull-right value">{{cuvProduct.quantities}} <spring:theme code="cat.truckload.config.units" /></span>
                            </li>
                            <li class="clearfix truckload-info-list">
                                <span class="pull-left value"><spring:theme code="configure.truckload.total"/></span>
                                <span class="pull-right value">{{object.totalUnits}} <spring:theme code="cat.truckload.config.units" /></span>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-md-4 truckload-qty-info no-padding visible-xs">
                        <p ng-if="object.totalUnits == 0"> <spring:theme code="cat.truckload.config.info.message" /></p>
                        <ul ng-if="object.totalUnits > 0">
                            <li ng-repeat="cuvProduct in object.cuvProductsArray track by $index" ng-if="cuvProduct.quantities > 0" class="clearfix truckload-info-list">
                                <span class="pull-left header">{{cuvProduct.productNames}}</span>
                                <span class="pull-right value">{{cuvProduct.quantities}} <spring:theme code="cat.truckload.config.units" /></span>
                            </li>
                            <li class="clearfix truckload-info-list">
                                <span class="pull-left value"><spring:theme code="configure.truckload.total"/></span>
                                <span class="pull-right value">{{object.totalUnits}} <spring:theme code="cat.truckload.config.units" /></span>
                            </li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-md-8 truckload-image">
                        <div class="col-xs-12 col-md-12 cuvImageHolder">
                            <div ng-repeat="cuvImages in object.cuvImagesArray track by $index" ng-class="cuvImages"></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-9 truckload-disclaimer visible-xs">
                        <spring:theme code="cat.truckload.config.disclaimer" />
                    </div>
                </div>
                <div class="col-xs-12 col-md-3 add-to-truckload-box no-padding hidden-xs">
                    <div class="truckload-button-wrapper">
                        <button type="button" ng-if="object.truckButtonType == 'ADD'" ng-disabled="object.addToTruckDisable" ng-class="(object.addToTruckDisable)?'disabled':''" class="btn update-product-list add-to-truckload-btn" ng-click="tcc.addProductsToTruck(truckIndex);"><spring:theme code="cat.truckload.config.addToTruck" /></button> 
                        <button type="button" ng-if="object.truckButtonType == 'UPDATE'" ng-disabled="object.addToTruckDisable" ng-class="(object.addToTruckDisable)?'disabled':''" class="btn update-product-list add-to-truckload-btn" ng-click="tcc.updateTruckProducts(truckIndex);">UPDATE TRUCK</button> 
                    </div>
                </div>
                <div class="col-xs-12 col-md-9 truckload-disclaimer hidden-xs">
                    <spring:theme code="cat.truckload.config.disclaimer" />
                </div>
            </div>
        </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-12 add-truckload-section">
        <div class="col-xs-12 col-md-12 add-truckload-wrapper no-padding" ng-if="!tcc.lineItemArray[tcc.lineItemArray.length - 1].addNewLineItem">
             <span class="increment disabled" uib-popover-html="'Please fill the current truck to add a new one.'" popover-placement="bottom-left" popover-trigger="'mouseenter'"></span>
             <span class="add-to-truck-label disabled" uib-popover-html="'Please fill the current truck to add a new one.'" popover-placement="bottom-left" popover-trigger="'mouseenter'"><spring:theme code="cat.truckload.config.addNewTruck" /></span>
        </div>
        <div class="col-xs-12 col-md-12 add-truckload-wrapper no-padding" ng-if="tcc.lineItemArray[tcc.lineItemArray.length - 1].addNewLineItem">
             <span class="increment" ng-click="tcc.addTruckLoadLineItem();"></span>
             <span class="add-to-truck-label" ng-click="tcc.addTruckLoadLineItem();"><spring:theme code="cat.truckload.config.addNewTruck" /></span>
        </div>
    </div>
    <div class="col-xs-12 col-md-12 truckload-total-qty-section">
        <div class="col-xs-12 col-md-12 truckload-qty-wrapper no-padding">
            <div class="col-xs-6 col-md-11 no-padding">
                <span class="total-qty-label pull-right hidden-xs"><spring:theme code="cat.truckload.config.totalQuantity" /></span>
                <span class="total-qty-label pull-left visible-xs"><spring:theme code="cat.truckload.config.totalQuantity" /></span>
            </div>
            <div class="col-xs-6 col-md-1 no-padding">
                <span class="total-qty-value pull-right">{{tcc.totalTruckloadUnits}} <spring:theme code="cat.truckload.config.units" /></span>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-12 next-button-section">
        <button ng-disabled="tcc.totalTruckloadUnits <= 0" class="btn update-product-list next-btn pull-right" ng-class="tcc.totalTruckloadUnits === 0 ? 'disabled':''" ng-click="tcc.truckloadNavigatetoReviewOrder()"><spring:theme code="cat.truckload.config.next" /></button> 
    </div>
</div>
