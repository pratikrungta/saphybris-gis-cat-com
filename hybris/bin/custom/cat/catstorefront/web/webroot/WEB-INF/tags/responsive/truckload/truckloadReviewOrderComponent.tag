<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div class="cat-row">
    <div class="cat-container order-review-page cat-main-content" id="CaterpillarPageWrap" ng-app="cat-app" ng-controller="reorderReviewController as rrc"
        ng-cloak ng-init="rrc.loadTruckloadReviewOrderData()">
	<div ng-if="!rrc.view.truckload.isEmptyPage">
        <review-order-pop-up id="orderSuccessMessage" alert-header="Your order has been submitted!" ok="DONE" product-info-text="Retail Configuration"
            product-info-value="{{rrc.view.response.configurationId}}" quantity-info-text="Quantity" quantity-units="{{rrc.view.response.reorderUnits}}"
            quantity="{{rrc.view.response.reorderQty}}" home-page-link={{rrc.view.response.homePageLink}} view-order-histroy-link="{{rrc.view.response.orderHistroyLink}}"
            type="success" product-type="truckload" result-po="rrc.view.resultPO" is-truckload-submit-success = "true" total-truckload="{{rrc.view.truckload.totalTruckloadCount}}" 
            total-truckload-units="{{rrc.view.truckload.totalCartQuantity}}" total-truckload-price="{{rrc.view.truckload.totalCartPrice}}" split-order-count="{{rrc.view.response.splitOrderCount}}"></review-order-pop-up>


        <review-order-pop-up id="orderErrorMessage" alert-header="VALIDAtiON ERRoR" order-number="{{rrc.view.response.orderNumber}}"
            error-info="This PO number already exists. Please enter a new one and then resubmit order." error-pos="rrc.view.errorPos"
            type="error" product-type="truckloadValidationError"></review-order-pop-up>

        <div class="row">
            <div class="col-xs-12 align-to-left">
                <div class="super-header upperCase">
                    <spring:theme code="cat.review.order.message" />
                </div>
            </div>
        </div>

        <form:form name="orderReviewForm" id="reviewOrderForm" action="/checkout/submitReOrder" onsubmit="return false;" commandName="ReviewOrderForm"
            method="POST">
            
            <div class="row sectionWrap delearsDetailsHold truckloadDealer">
                <div class="col-xs-12 headerHold">
                    <spring:theme code="im.reorder.dealer.details" />
                </div>
                <div class="col-xs-12 dealer-box">
                    <div class="row">
                        <div class="col-xs-12 col-md-2 no-padding">
                            <p class="lable">
                                <spring:theme code="im.reorder.dealer.name" />
                            </p>
                        </div>
                        <div class="col-xs-12 col-md-9 dealer-section no-padding">
                            <div class="clearfix">
                                <div class="col-md-4 no-padding">
                                    <input type="text" name="dealerName" class="form-control no-border" id="dealerName" readonly="readonly"
                                        value="{{rrc.view.dealerName}}" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row sectionWrap orderDetailsTab truckload-revieworder">
                <div class="row col-md-12">
                    <div class="col-md-12 orderDetails_new">
                        <div class="orderDetailsHeader_new no-border-bottom">
                            <div class="col-xs-6 headerHold clearfix">
				                <span class="pull-left order-details-header"><spring:theme code="im.reorder.details" /></span>
                            </div>
                            <div class="col-xs-6 headerHold clearfix">
                                    <span class="pull-right edit-truckload-container">
                                        <span class="edit-text hidden-xs" ng-click="rrc.editTruckload();">Edit</span>
                                        <span class="edit-icon" ng-click="rrc.editTruckload();"></span>
                                    </span>
                            </div>
                        </div>
                        <!-- Order header Tabs -->
                        <div class="orderDetailsHeaderTab_new" ng-repeat="(key,entry) in rrc.view.truckload.Entries track by $index">
                            <div class="row truckload-container">
                                <div class="col-lg-3 col-sm-3 col-xs-12 truck-data truck-box-1 no-padding">
                                    <div class="truck-no">
					                    <span><spring:theme code="truckload.truck.text" /> {{ $index + 1 }}</span>
                                        <span class="remove-icon pull-right visible-xs" ng-click="rrc.showRemoveTruckloadPopup(entry.truckloadId,key);"></span>
                                    </div>
                                    <div class="truck-price clearfix">
                                            <div class="col-xs-6 col-sm-12 no-padding heading"><spring:theme code="truckload.review.order.price.text" /></div>
                                            <div class="col-xs-6 col-sm-12 no-padding value"><span>$</span>{{entry.truckload_price | number}}</div>
                                        </div>
                                    <div class="ship-date row">
                                        <div class="col-xs-6 col-sm-12 no-padding heading"><spring:theme code="truckload.review.order.ship.after.date.text" /></div>
                                        <div class="col-xs-6 col-sm-12 no-padding value">{{entry.dateRange.dateLabel}}</div>
                                    </div>
                                    <div class="row entry-address visible-xs" ng-if="rrc.isMobile">
                                            <div class="col-xs-12 col-md-12 no-padding">
                                                <p class="lable">
                                                    <spring:theme code="im.reorder.shipping.address" />
                                                </p>
                                            </div>
                                            <div class="col-xs-12 col-md-12 dealer-section">
                                                <div class="form-group clearfix">
                                                    <div class="col-md-6 typeahead-template-wrapper">
                                                            <cat-address  ship-address-id="reviewShippingAddressId{{$index}}" update-selected-address="updateTruckloadDropDownValue(addrssObj,key)"></cat-address>
                                                    </div>
                    
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <div class="col-lg-2 col-sm-3 col-xs-12 truck-data truck-box-2 no-padding">
                                    <div class="truck-info"><spring:theme code="truckload.review.order.information" /></div>
                                    <div class="truckload-info-table">
                                        <div class="row hidden-xs">
                                            <div class="col-xs-6 col-sm-6 no-padding heading">
                                                <spring:theme code="truckload.model.text" />
                                            </div>
                                            <div class="col-xs-6 col-sm-6 no-padding heading quantity">
                                                <spring:theme code="truckload.quantity.text" />
                                            </div>
                                        </div>
                                        <div class="row" ng-repeat="(productName, productCount) in entry.productList">
                                            <div class="col-xs-6 col-sm-6 no-padding value">
                                                {{productName}}
                                            </div>
                                            <div class="col-xs-6 col-sm-6 no-padding value quantity">
                                                {{productCount}} <spring:theme code="truckload.review.units"/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6 no-padding value">
                                                <spring:theme code="truckload.total.text" />
                                            </div>
                                            <div class="col-xs-6 col-sm-6 no-padding value quantity">
                                                {{entry.totalCount}} <spring:theme code="truckload.review.units"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-7 col-sm-6 col-xs-12 truck-data no-padding">
                                        <div class="truckload-img" ng-style="{'background-image': 'url(' + entry.truckloadImageUrl + ')' }"></div>
					                    <span class="remove-icon hidden-xs" ng-click="rrc.showRemoveTruckloadPopup(entry.truckloadId,key);"></span>
                                </div>
                                
                            </div>
                            <div class="row entry-address hidden-xs" ng-if="!rrc.isMobile">
                                    <div class="col-xs-12 col-md-12 no-padding">
                                        <p class="lable">
                                            <spring:theme code="im.reorder.shipping.address" />
                                        </p>
                                    </div>
                                    <div class="col-xs-12 col-md-12 dealer-section">
                                        <div class="form-group clearfix">
                                            <div class="col-md-6 typeahead-template-wrapper">
                                                    <cat-address  ship-address-id="reviewShippingAddressId{{$index}}" update-selected-address="updateTruckloadDropDownValue(addrssObj,key)"></cat-address>
                                            </div>
            
                                        </div>
                                    </div>
                                </div>
                            <div class="orderContentTab_new">
                                <div class="row orderContentTabLower">
                                    <div class="row productContainer" ng-repeat="(productName,productCount) in entry.productList">
                                        
                                        <div class="col-md-4 col-xs-12 productInfoContainer">
                                            <img class="col-md-6 col-xs-4 productImage" ng-src={{rrc.view.CUVproductImageList[productName]}} alt="{{productName}}"/>
                                           <div class="col-md-6 col-xs-8">
                                                <span class="productName">{{productName}}</span>
                                                <span class="text-upper productQuantity">
                                                        <spring:theme code="truckload.quantity.text" />
                                                </span>
                                                <span class="productCount">{{productCount}}</span>
                                           </div>
                                            
                                        </div>
                                        <div class="col-md-8 col-xs-12 POcontainer">
                                                <div class="row">
                                                        <span class="errorLabel no-pad-left">
                                                            <spring:theme code="truckload.purchase.order.number" />
                                                            <sup>* </sup>
                                                        </span>
                                                    </div>
                                                    <div class="row po-info-message">
                                                             <div class="col-xs-12 no-pad-left">
                                                                     <span class="purchase-text">
                                                                         <spring:theme code="cat.enter.ponumber.message" />
                                                                     </span>
                                                                     <span class="error" ng-if="rrc.productListError[entry.truckloadId][productName].hybrisError">
                                                                         <spring:theme code="cat.ponumber.error.message" /> </span>
                                                                     <span class="error" ng-if="rrc.productListError[entry.truckloadId][productName].repeatError">
                                                                         <spring:theme code="cat.ponumber.unique.message" /> </span>
                                                                     <span class="error" ng-if="rrc.productListError[entry.truckloadId][productName].mandatoryError 
                                                                                                && rrc.userCheckedCertified">
                                                                         <spring:theme code="cat.ponumber.mandatory.message" /> </span>
                                                                 </div>
                                                                 
                                                         </div>
                                                         <div class="validPOCounter" ng-if="
                                                         rrc.returnsSymbolOfstatus(rrc.productListError&&rrc.productListError[entry.truckloadId][productName],
                                                                                    productCount)==='warning'
                                                         ||rrc.productListError[entry.truckloadId][productName].POsFilled>0">
                                                               <span
                                                               ng-class="rrc.returnsSymbolOfstatus(rrc.productListError[entry.truckloadId][productName],productCount)"></span> 
                                                               {{rrc.productListError[entry.truckloadId][productName].POsFilled||0}} out of {{productCount}} added
                                                         </div>
                                                        
                                                         <div class="row inputPOContainer no-padding" ng-init="rrc.initiateTruckLoadPOobj({ref:entry,action:'initializeRef'})">
                                                                <div class="col-xs-6 col-md-3 no-padding"
                                                                    ng-repeat="inputData in rrc.filterPO(entry.range,productCount,productName)">
                                                                    <span class="pull-left POIndexnumber">{{$index+1}}.</span> 
                                                                    <input ng-init="rrc.initiateTruckLoadPOobj({ref:entry,
                                                                                                                action:'initializeInput',
                                                                                                                inputId:inputData.id,
                                                                                                                productCode:productName
                                                                                                                })"
                                                                         cat-capitalize ng-model="inputData.value" class="inputPO" type="text" restrict-model-input="^[A-Za-z0-9]{0,12}$"
                                                                           not-allow-to="[]" ng-trim="false" id="{{inputData.id}}" maxlength="12"
                                                                        ng-blur="rrc.validateTruckLoadPo(inputData, inputData.id-1 , key);
                                                                                 rrc.POStateObj[entry.truckloadId].inputs[inputData.id]=inputData.value"
                                                                        placeholder="Enter PO #" ng-class="{hasError: (!inputData.value&&rrc.userCheckedCertified)||inputData.isInvalid||inputData.hybrisValidateError}"
                                                                    />
                                                                </div>
                                                            </div>
                                            </div>
                                        
                                            
                                    </div>
                                    
                                </div>
                              
                            </div>
                        </div>    
                            <div class="row truckload-unit-price">
                                <div class="row">
                                    <div class="col-xs-8 col-sm-9 label">
                                        <spring:theme code="truckload.review.order.total.truckload.text" />
                                    </div>
                                    <div class="col-xs-4 col-sm-3 value">
                                        {{rrc.view.truckload.totalTruckloadCount}}
                                    </div>
                                    <div class="col-xs-8 col-sm-9 label">
                                        <spring:theme code="truckload.review.order.total.units.text" />
                                    </div>
                                    <div class="col-xs-4 col-sm-3 value">
                                        {{rrc.view.truckload.totalCartQuantity}}
                                    </div>
                                    <div class="col-xs-8 col-sm-9 label">
                                        <spring:theme code="truckload.review.order.total.price.text" />
                                    </div>
                                    <div class="col-xs-4 col-sm-3 value">
                                        <span>$</span>{{rrc.view.truckload.totalCartPrice | number}}
                                    </div>
                                </div>    
                            </div>
                        <!-- order Content Tab below -->

                    </div>

                </div>
            </div>

            <div class="row" id="submitOrderHold">
                <div class="indicateMsg col-md-12">
                    <spring:theme code="cat.indicates.required.fileds.message" />
                </div>
                <div class="form-group clearfix">
                    <div class="col-xs-12 col-md-10 checkBoxSection">
                        <div class="row">
                            <input type="checkbox" name="shippingCheckbox" id="certify" value="true" 
                            ng-model="rrc.view.userCertified" ng-click="rrc.view.userCertified?rrc.triggerValidations():''">
                            <label for="certify" ng-class="!!rrc.view.formSubmited && !rrc.view.userCertified ? 'highlighted-box': ''">
                                <span aria-hidden="true" ng-class="rrc.view.userCertified?'tickFont':''"></span>
                            </label>
                            <label class="checkboxlabel">
                                <spring:theme code="cat.necessary.approval.message" />
                            </label>
                        </div>
                        <div class="row disclimer-error-message" ng-show="!!rrc.view.formSubmited && !rrc.view.userCertified">
                            <spring:theme code="cat.select.submit.order.checkbox" />
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-2 text-right">
                                <div class="col-xs-12 col-md-12" id="submitorderbuttonHold">
                                        <input type="button" class="truckLoad-submit-btn" id="submit" value="<spring:theme code="im.order.submit.button" />" 
                                        ng-class="!rrc.view.userCertified?'disabled':''"
                                            ng-disabled="!rrc.view.userCertified" ng-click="rrc.submitOrderTruckload()"
                                        />
                                    </div>
                        </div>
                    
                    
                 
                    
                </div>
            </div>
        </form:form>
    </div>
    <div ng-if="rrc.view.truckload.isEmptyPage" class="empty-cart">
            <div class="col-md-12 empty-cart-text">
                <!--<spring:theme code="cat.cart.empty.message"/>-->
               <spring:theme code="truckload.review.order.cart.empty"/>
            </div>
            <div class="col-md-12">
                <button class="btn update-product-list add-products-cart-btn" ng-click="rrc.backToHome();"><spring:theme code="truckload.review.add.produts"/></button>
            </div>
        </div>
    </div>
</div>
