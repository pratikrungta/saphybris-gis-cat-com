<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="actionNameKey" required="true" type="java.lang.String"%>
<%@ attribute name="action" required="true" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:set var="hideDescription" value="checkout.login.loginAndCheckout" />

<div class="login-page__headline text-center">
	<spring:theme code="cat.search.portal.text" />
</div>

<!-- <c:if test="${actionNameKey ne hideDescription}">
	<div class="text-center">
		<p>
			<spring:theme code="login.description" />
		</p>
	</div>
</c:if> -->

<div class="login-form">
<form:form action="${action}" method="post" commandName="loginForm">
	<div>
	<c:if test="${not empty message}">
		<span class="has-error"> <spring:theme code="${message}" />
		</span>
	</c:if>	
	

	  <div class="form-group">
      <input id="j_username" name="j_username" class=" form-control" type="text" placeholder="Email">
      <span class="input-icon glyphicon glyphicon-user"></span>
    </div>

    <div class="form-group">
      <input id="j_password" name="j_password" class="form-control" type="password" value="" autocomplete="off" placeholder="Password">
      <span class="input-icon glyphicon glyphicon-lock"></span>
    </div>
<!-- 	
	 	<formElement:formInputBox idKey="j_username" labelKey="login.email"
			path="j_username" mandatory="true"  placeholder="Email"/>
		<i class="input-icon fa fa-user-o" aria-hidden="true">icon</i>
		<formElement:formPasswordBox idKey="j_password"
			labelKey="login.password" path="j_password" inputCSS="form-control"
			mandatory="true"/>
			<i class="input-icon fa fa-lock" aria-hidden="true">lock</i> -->
	

		<ycommerce:testId code="loginAndCheckoutButton">
			<button type="submit" class="btn btn-primary btn-block">
				<spring:theme code="${actionNameKey}" />
			</button>
		</ycommerce:testId>

		<div class="forgotten-password text-center">
				<ycommerce:testId code="login_forgotPassword_link">
					<a href="#" data-link="<c:url value='/login/pw/request'/>" class="js-password-forgotten" data-cbox-title="<spring:theme code="forgottenPwd.title"/>">
						<spring:theme code="login.link.forgottenPwd" />
					</a>
				</ycommerce:testId>
			</div>

	
	<c:if test="${expressCheckoutAllowed}">
		<button type="submit" class="btn btn-default btn-block expressCheckoutButton"><spring:theme code="text.expresscheckout.header" /></button>
		<input id="expressCheckoutCheckbox" name="expressCheckoutEnabled" type="checkbox" class="form left doExpressCheckout display-none" />
	</c:if>
</div>
</form:form>
</div>

