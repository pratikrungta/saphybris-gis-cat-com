<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="screenXs" value="480px" scope="session"/>
<c:set var="screenSm" value="901px" scope="session"/>
<c:set var="screenMd" value="901px" scope="session"/>
<c:set var="screenLg" value="1200px" scope="session"/>
 
<c:set var="screenXsMin" value="480px" scope="session"/>
<c:set var="screenSmMin" value="901px" scope="session"/>
<c:set var="screenMdMin" value="901px" scope="session"/>
<c:set var="screenLgMin" value="1200px" scope="session"/>
 
<c:set var="screenXsMax" value="900px" scope="session"/>
<c:set var="screenSmMax" value="900px" scope="session"/>
<c:set var="screenMdMax" value="1599px" scope="session"/>