<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="headline">
	<spring:theme code="text.account.profile" text="Profile"/>
</div>

<div class="headline">
	<spring:theme code="text.account.profile.dealerInformation" text="Dealer Information"/>
</div>
<table class="account-profile-data">
	<tr>
		<td><spring:theme code="dealerInformation.firstName" text="First name"/>: </td>
		<td><spring:theme code="dealerInformation.lastName" text="Last name"/>: </td>
	</tr>
	<tr>
		<td>${fn:escapeXml(customerData.firstName)}</td>
		<td>${fn:escapeXml(customerData.lastName)}</td>
	</tr>
	<tr>
		<td><spring:theme code="dealerInformation.dealerCode" text="Dealer Code"/>: </td>
		<td><spring:theme code="dealerInformation.dealerName" text="Dealer name"/>: </td>
	</tr>
	<tr>
		<td>${fn:escapeXml(unit.uid)}</td>
		<td>${fn:escapeXml(unit.name)}</td>
	</tr>
	<tr>
		<td><spring:theme code="dealerInformation.description" text="Dealer description"/>: </td>
	<tr>
	<tr>
		<td></td>
	</tr>
</table>

<div class="headline">
	<spring:theme code="text.account.profile.userInformation" text="User Information"/>
</div>
<table class="account-profile-data">
	<tr>
    	<td><spring:theme code="userInformation.email" text="Email"/>: </td>
    	<td><spring:theme code="userInformation.password" text="Password"/>: </td>
  	</tr>
  	<tr>
		<td>${fn:escapeXml(customerData.displayUid)}</td>
		<td>*******</td>
	</tr>
	<tr>
		<td><spring:theme code="userInformation.shippingAddress" text="Shipping Address"/></td>
	</tr>
	<c:forEach items="${unit.addresses}" var="address">
	<c:if test="${(address.shippingAddress) eq true}">
	<tr>
    	<td><spring:theme code="userInformation.address.line1" text=""/>: </td>
    	<td><spring:theme code="userInformation.address.line2" text=""/>: </td>
  	</tr>
  	<tr>
		<td>${fn:escapeXml(address.line1)}</td>
		<td>${fn:escapeXml(address.line2)}</td>
	</tr>
	<tr>
    	<td><spring:theme code="userInformation.address.city" text=""/>: </td>
    	<td><spring:theme code="userInformation.address.state" text=""/>: </td>
  	</tr>
  	<tr>
		<td>${fn:escapeXml(address.town)}</td>
		<td>${fn:escapeXml(address.region)}</td>
	</tr>
	<tr>
    	<td><spring:theme code="userInformation.address.country" text=""/>: </td>
    	<td><spring:theme code="userInformation.address.postalCode" text=""/>: </td>
  	</tr>
  	<tr>
		<td>${fn:escapeXml(address.country.name)}</td>
		<td>${fn:escapeXml(address.postalCode)}</td>
	</tr>
	</c:if>
	</c:forEach>
	<tr>
		<td><spring:theme code="userInformation.billingAddress" text="Billing Address"/></td>
	</tr>
	<c:forEach items="${unit.addresses}" var="address">
	<c:if test="${(address.billingAddress) eq true}">
	<tr>
    	<td><spring:theme code="userInformation.address.line1" text=""/>: </td>
    	<td><spring:theme code="userInformation.address.line2" text=""/>: </td>
  	</tr>
  	<tr>
		<td>${fn:escapeXml(address.line1)}</td>
		<td>${fn:escapeXml(address.line2)}</td>
	</tr>
	<tr>
    	<td><spring:theme code="userInformation.address.city" text=""/>: </td>
    	<td><spring:theme code="userInformation.address.state" text=""/>: </td>
  	</tr>
  	<tr>
		<td>${fn:escapeXml(address.town)}</td>
		<td>${fn:escapeXml(address.region)}</td>
	</tr>
	<tr>
    	<td><spring:theme code="userInformation.address.country" text=""/>: </td>
    	<td><spring:theme code="userInformation.address.postalCode" text=""/>: </td>
  	</tr>
  	<tr>
		<td>${fn:escapeXml(address.country.name)}</td>
		<td>${fn:escapeXml(address.postalCode)}</td>
	</tr>
	</c:if>
	</c:forEach>
</table>



<%-- <a class="button" href="update-password"><spring:theme code="text.account.profile.changePassword" text="Change password"/></a>
<a class="button" href="update-profile"><spring:theme code="text.account.profile.updatePersonalDetails" text="Update personal details"/></a>
<a class="button" href="update-email"><spring:theme code="text.account.profile.updateEmail" text="Update email"/></a> --%>
