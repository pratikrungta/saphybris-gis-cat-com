<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>

<spring:htmlEscape defaultHtmlEscape="true" />

<c:url value="${url}" var="addToCartUrl"/>
<product:addToCartTitle/>

<form method="post" id="addToCartForm" class="add_to_cart_form">
	<input type="hidden" maxlength="3" size="1" id="shoppingQty" name="qty" class="qty js-qty-selector-input">

	<c:set var="buttonType"><spring:theme code="checkout.multi.hostedOrderPostPage.button.submit"/></c:set>
	<ycommerce:testId code="addToCartButton">
	<c:if test="${isInventoryManager}"><div class="lineBreakAddToCartIM visible-xs"></div></c:if>
	<c:if test="${userType eq 'SP'}"><div class="lineBreakAddToCartSP visible-xs"></div></c:if>
	<div class="row no-gutters reorderContent cartAddSection reorderCartSection" 
		 ng-init="setStockLevel('${stocklevelForDealerWarehouse}')">
							<c:if test="${isInventoryManager}">	
								<div class="pdtName col-md-12 col-lg-12 bold pdtName-mb hidden-xs">${product.name}</div>
								<div class="pdtName col-md-12 col-lg-12 bold pdtName-mb visible-xs">${product.name}<span class="reodQtyText"><spring:theme code="cat.pdp.reorder.quantity"/></span></div>
								<div class="col-md-5 col-xs-6 text-uppercase pdtName config-name hidden-xs"><spring:theme code="cat.AddToCart.Reorder.Quantity"/></div>
								<div class="col-md-5 col-xs-6 text-uppercase pdtName config-name qtyValue visible-xs"><spring:theme code="cat.mob.AddToCart.Reorder.Quantity"/></div>
							</c:if>	
							
							<c:if test="${userType eq 'SP'}">
								<div class="col-md-5 col-xs-6 text-uppercase pdtName config-name"><spring:theme code="cat.AddToCart.Add.Quantity"/></div>
							</c:if>
											
							<div class="col-md-3 col-xs-6 opertions">
								<div class="input-params row">
									<span class="col-xs-4 decrement" aria-hidden="true" ng-click="decreaseQuantity()" 
									ng-class="!!(input.customConfig<=0)?'disabled':''"></span> 
									<input ng-readonly="cartInputDisable" id="cartVal" type="text" size="1" ng-model="input.customConfig" restrict-to="[0-9]" ng-blur="validateQuantity()" class="col-xs-4 cartAddInputBox" autocomplete="off"/>
									<span class=" col-xs-4 increment" aria-hidden="true" ng-click="cartInputDisable || increaseQuantity()"
									ng-class="!!(input.customConfig>=maxQuantity) || cartInputDisable ?'disabled':''"></span>
								</div>
								</div>
							<div class="col-md-4 hidden-xs">
								<button type="button" id="cartAdd" class="btn js-add-to-cart addToCartPageBtn" ng-if="!laneWiseData.fbcMustSelectFlag" 
								ng-disabled="!canReorder || isRedirecting" ng-click="addToCart($event,${isUTVCart},'${cartType}')" disabled="disabled"><spring:theme code="cat.AddToCart.cart"/></button>
							</div>
							<div class="col-md-4 hidden-xs" ng-if="laneWiseData.fbcMustSelectFlag">
								<button type="button" id="cartAdd" class="btn js-add-to-cart addToCartPageBtn" 
								ng-click="showSelectOptionsPopup(laneWiseData.primaryImageUrl,laneWiseData.dcaCode)" ng-disabled="input.customConfig == '00'"><spring:theme code="cat.pdp.fbc.select.options"/></button>
							</div>
					<div class="row no-gutters visible-xs view-cart-section" ng-if="!laneWiseData.fbcMustSelectFlag">
							<div ng-show="userTypeConfig === 'IM' || userTypeConfig === 'UTV'" class="pdtName col-md-12 col-lg-12 bold text-uppercase"><spring:theme code="cat.AddToCart.Reorder.Quantity"/></div>
							<div ng-show="userTypeConfig === 'SP'" class="pdtName col-md-12 col-lg-12 bold text-uppercase"><spring:theme code="utv.order.quantity"/></div>
							<span class="mb-PDP-units" ng-bind="input.customConfig+' units'"></span>
							<button type="button" id="cartAdd" 
									class="btn js-add-to-cart col-xs-6 addToCartPageBtn view-cart-btn" 
									ng-disabled="!canReorder || isRedirecting" ng-click="addToCart($event,${isUTVCart},'${cartType}')" disabled="disabled"><spring:theme code="cat.AddToCart.cart"/></button>
					</div>

					<button type="button" 
					class="btn  col-xs-12 selectOptions visible-xs" ng-if="laneWiseData.fbcMustSelectFlag" 
					ng-click="showSelectOptionsPopup(laneWiseData.primaryImageUrl,laneWiseData.dcaCode)" ng-disabled="input.customConfig == '00'"><spring:theme code="cat.pdp.fbc.select.options"/></button>
	</div>
					
	</div>
	</ycommerce:testId>
</form>

