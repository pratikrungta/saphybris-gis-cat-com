<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div>

	<c:set var="componentContent" value="${component.content}" />
	<c:set var="shippingFactor" value="${shippingFactor}" />
	<c:set var="displayText" value="${fn:replace(componentContent, 'shippingFactor', shippingFactor)}" />
	
	${displayText}
	
</div>
