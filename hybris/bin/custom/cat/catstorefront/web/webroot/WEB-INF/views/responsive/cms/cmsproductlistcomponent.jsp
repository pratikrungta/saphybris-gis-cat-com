<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="product__listing product__list">
	<div ng-show="plp.view.pageLoadingParams.ajaxCallSuccess" ng-if="plp.view.searchResults.length>0" id="grid1" scroll-trigger="plp.scrollBottomReached()" threshold="100">
		<div class="row plp-row-container" ng-repeat="setOfResult in plp.view.searchResultsChunkOf3 track by $index">
		<div class="col-xs-12 item grid-item" ng-class="(plp.view.metadata.isUTVUser)?'col-sm-3':'col-sm-4'" ng-repeat="searchObj in setOfResult">
		<c:set var="categoryName" value="{{searchObj.categoryName[0] | titleCase}}"/>
		 <cat-card 
					lane-type="{{plp.laneType}}"
					lane1="{{searchObj.lane1}}"
					lane2="{{searchObj.lane2}}"
					lane3="{{searchObj.lane3}}"
					user-type='${userType}'
					same-in-responsive="false"
					utv-user="{{plp.view.metadata.isUTVUser}}"
					super-header="{{((!!plp.view.metadata.isUTVUser)?searchObj.salesModelName:((!!searchObj.serialNumber && searchObj.serialNumber!=null && searchObj.serialNumber!='null')? searchObj.serialNumber:searchObj.name)) | toUpperCase}}"
					image="{{searchObj.primaryImageUrl || plp.defaultDownloadImage}}" 
					primary-header="{{((!!plp.view.metadata.isUTVUser)?(searchObj.categoryName[0] | titleCase):((searchObj.primaryOfferingName || searchObj.salesModelName)| toUpperCase)) || '-' }}"
					pdp-link="{{searchObj.url}}" 
					book-marked="{{!!searchObj.favorite}}" 
					product-id-for-communication="{{searchObj.code}}"
					show-fullcard="true"
					engine-model-text = '<spring:theme code="product.engine.model.text"/>'
					engine-model-value = "{{searchObj.featureEngineModel}}"
					net-power-text = '<spring:theme code="product.net.power.text"/>'
					net-power-value = "{{searchObj.featureNetPower}}"
					operating-weight-text = '<spring:theme code="product.operating.weight.text"/>'
					operating-weight-value = "{{searchObj.featureOperatingWeight || '-'}}"
					product-type="{{plp.view.productType}}"
					used-tag-position="bottom"
					product-price="{{searchObj.priceClassification?searchObj.priceClassification+'*': '-'}}"
					utv-price="{{searchObj.price.formattedValue?searchObj.price.formattedValue : '-'}}"
					currency= "{{searchObj.priceClassification?'$':''}}"
					delar-name-text="DEALER"
					delar-name-value="{{searchObj.dealerName || '-'}}"
					branch-location-text="BRANCH LOCATION"
					branch-location-value="{{searchObj.branchLocation || '-'}}"
					hours-used-text="HOURS USED"
					hours-used-value="{{searchObj.hoursUsed || '-'}}"
					manufacturing-year-text="MANUFACTURING YEAR"
					manufacturing-year-value="{{searchObj.manufacturingYear|| '-'}}"
					dealer-certified="{{searchObj.dealerCertified}}"
					fuel-type="{{searchObj.fuelType || '-'}}"
          			max-speed="{{searchObj.maxSpeed || '-'}}"
          			seat-capacity="{{searchObj.seatCapacity || '-'}}"
          			stock-in-transit="{{!!(searchObj.stockInTransit)?searchObj.stockInTransit+' Units':'-'}}"
          			view-intransit="{{plp.view.viewIntransit}}"
          			no-view-intransit="{{plp.view.noViewIntransit}}"
          			dis-continued="{{!searchObj.isReorderable}}"
					>
					</cat-card>

		</div>
	</div>
	</div>
	<div class="empty-results" ng-if="plp.view.searchResults.length == 0 && plp.view.pageLoadingParams.ajaxCallSuccess">
		<div class="empty">
			<div class="message"> {{plp.constantService.noSearchResultsFound}} </div>
		</div>
	</div>
	<div ng-show="!plp.view.pageLoadingParams.ajaxCallSuccess" class="searching-failed">
		<div class="error">
			<div class="message"> {{plp.constantService.searchResultsErrorMessage}} </div>
		</div>
	</div>
</div>
