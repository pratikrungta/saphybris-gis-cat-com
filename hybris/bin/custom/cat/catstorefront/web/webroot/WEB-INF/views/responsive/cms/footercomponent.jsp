<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="footer" tagdir="/WEB-INF/tags/responsive/common/footer"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<div class="cat-container row">
    <!-- <div class="col-xs-6 links">
    <c:forEach items="${navigationNodes}" var="node">
                        <c:if test="${node.visible}">
                        
                            <c:forEach items="${node.links}" step="${component.wrapAfter}"
                                       varStatus="i">
						
                                <div class="footer__nav--container col-xs-12">
                                    <c:if test="${component.wrapAfter > i.index}">
                                        <%-- <div class="title">${fn:escapeXml(node.title)}</div> --%>
                                    </c:if>
                                    	<c:set var="count" value="0"/>
                                        <c:forEach items="${node.links}" var="childlink"
                                                   begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
                                            <cms:component component="${childlink}"
                                                           evaluateRestriction="true" element="div" class="links"/>
                                                           <c:if test="${count lt (fn:length(node.links)-1)}">
                                                         	<c:set var="count" value="${count+1}"/>
                                                         	<div class="links  verticalDivider">|</div>
                                                           </c:if>
                                        </c:forEach>
                                   
                                </div>
                            </c:forEach>
                        </c:if>
                    </c:forEach>
    </div> -->
    <div class="copy-rights">
    <jsp:useBean id="date" class="java.util.Date" />
				<p class="copyright text-center">
                        <spring:theme code="cat.footerPart1.privacyPolicy"></spring:theme>
                        <fmt:formatDate value="${date}" pattern="yyyy"/>
                        <spring:theme code="cat.footerPart2.privacyPolicy"></spring:theme>
                    <!-- Copyright &#9400; Caterpillar
                    <fmt:formatDate value="${date}" pattern="yyyy"/>. All rights reserved. View 
                    <a target="_blank" href="https://www.caterpillar.com/">Privacy Policy</a> here.</p> -->
				</div>
</div>