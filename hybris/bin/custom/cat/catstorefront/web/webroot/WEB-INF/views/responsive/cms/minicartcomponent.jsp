<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:htmlEscape defaultHtmlEscape="true" />

<spring:url value="/cart/miniCart/{/totalDisplay}" var="refreshMiniCartUrl" htmlEscape="false">
	<spring:param name="totalDisplay"  value="${totalDisplay}"/>
</spring:url>
<spring:url value="/cart/rollover/{/componentUid}" var="rolloverPopupUrl" htmlEscape="false">
	<spring:param name="componentUid"  value="${component.uid}"/>
</spring:url>

<c:set var="cartQuantity" value="${quantity}"/>
<c:if test="${empty quantity}">
<c:set var="cartQuantity" value="0"/>
</c:if>
<c:choose>
	<c:when test="${not empty selectedModel and selectedModel ne ''}">
		<c:url value="/cart/catCartPage?productCode=${productCode}&orderQuantity=${cartQuantity}&selectedModel=${selectedModel}" var="cartUrl"/>
	</c:when>
	<c:otherwise>
		<c:url value="/cart/catCartPage?productCode=${productCode}&orderQuantity=${cartQuantity}" var="cartUrl"/>
	</c:otherwise>
</c:choose>
<div class="nav-cart col-xs-2 pull-right">
	<a 	href="${cartUrl}" id="desktop-mini-cart">
		<div class="mini-cart-icon">
			<span class="shoppingCart"> </span> <span class="shoppingCartText"><spring:theme code="header.cart.text" /></span>
			<div class="shoppingCartValue">
				<c:choose>
					<c:when test="${totalItems lt 100}">
						<span class="shoppingCartValueLabel" id="shoppingCartValueLabel">${totalItems}</span>
					</c:when>
					<c:otherwise>
						<span class="shoppingCartValueLabel" id="shoppingCartValueLabel"><spring:theme code="header.cart.limit.display.text" /><span class="cartIconPlus"><spring:theme code="header.cart.plus.text" /></span></span>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</a>
</div>
	

<div class="mini-cart-container js-mini-cart-container"></div>
