<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<c:if test="${not isPdpPage}">
<div id="product-facet" class="hidden-xs" ng-init="setter('${userType}')">
	<div class="cat-search-parameters">
			<div class="params">
			<div class="param-head">
				<span class="filter-text"><spring:theme code="cat.plp.filters"/></span>
				<img class="pull-right visible-xs mobile-close-button" src="/_ui/responsive/cat/images/cross.png" ng-click="plp.closeMobleFilters()"/>
			</div>
			<div class="param-sub-head">
				<span class="text"><spring:theme code="cat.plp.inventoryType"/></span>
			</div>
			<div class="params-sec">
				<div class="params-sec-heading"><spring:theme code="product.refinment.component.product.family"/></div>
					<div class="row params-sec-row">
						<div class="btn-group col-xs-12 params-sec-dropdown" uib-dropdown keyboard-nav=true>
							<button type="button" class="btn col-xs-12 drop-down-button" uib-dropdown-toggle ng-disabled="disabled">
							 <div class="text">{{(plp.view.productFamily.selectedProductName | titleCase) || 'Select'}}</div> 
							<div class="arrow pull-right">
							<!-- <i class="fa fa-angle-down" aria-hidden="true"></i> -->
							<div class="dropdown-arrow"></div>
							</div>
							</button>
							<ul class="dropdown-menu col-xs-12" uib-dropdown-menu role="menu">
								<c:forEach var="category" items="${categories}">
										<c:if test="${category.code ne 'utv'}">
											<li role="menuitem" data-productFamily-code="${category.code}" data-productFamily-name="${category.name}" ng-click="plp.productFamilySelected('${category.name}','${category.name}')"><a href="#">${fn:toLowerCase(category.name)}</a></li>
										</c:if>
								</c:forEach>
							</ul>
						</div>
				  </div>
			</div>
			<div class="params-sec">
				<div class="params-sec-heading"><spring:theme code="product.refinment.component.sales.model"/></div>
				<div class="row params-sec-row">
					<div class="btn-group col-xs-12 params-sec-dropdown" uib-dropdown keyboard-nav=true>
						<button id="sales-model" type="button" class="btn col-xs-12 drop-down-button" uib-dropdown-toggle ng-disabled="disabled" ng-class=" plp.view.salesModel.isDisabled?'disabled':''">
						<div class="text">{{(plp.view.salesModel.selectedModelName) || 'Select'}}</div>
						<div class="arrow pull-right">
							<!-- <i class="fa fa-angle-down" aria-hidden="true"></i> -->
							<div class="dropdown-arrow"></div>
						</div>
						</button>
						<ul class="dropdown-menu col-xs-12" uib-dropdown-menu role="menu" aria-labelledby="sales-model">
							<li role="menuitem" ng-repeat="salesModel in plp.view.salesModel.listOfSalesModel | orderBy: 'name' track by $index" ng-click="plp.salesModelSelected(salesModel.name,salesModel.name)"><a href="#">{{salesModel.name}}</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="params-sec" ng-if="plp.view.isNewProduct">
				<div class="params-sec-heading"><spring:theme code="product.refinment.component.primary.offering"/></div>
				<div class="row params-sec-row">
					<div class="btn-group col-xs-12 params-sec-dropdown" uib-dropdown keyboard-nav=true>
						<button id="primary-offering" type="button" class="btn col-xs-12 drop-down-button" uib-dropdown-toggle ng-disabled="disabled" ng-class=" plp.view.primaryOffering.isDisabled?'disabled':''">
						<div class="text">{{(plp.view.primaryOffering.selectedPrimaryOffering) || 'Select'}}</div>
						<div class="arrow pull-right">
							<!-- <i class="fa fa-angle-down" aria-hidden="true"></i> -->
							<div class="dropdown-arrow"></div>
						</div>
						</button>
						<ul class="dropdown-menu col-xs-12" uib-dropdown-menu role="menu" aria-labelledby="primary-offering">
							<li role="menuitem" ng-repeat="primaryOffering in plp.view.primaryOffering.listOfPrimaryOffering | orderBy: 'name' track by $index" ng-click="plp.primaryOfferingSelected(primaryOffering.code,primaryOffering.name)"><a href="#">{{primaryOffering.name}} ({{primaryOffering.code}})</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="update-btn-container hidden-xs">
				<button class="update-btn btn" ng-disabled="!plp.view.hasValidSearchParams?'disabled':''" ng-click="plp.updateSearchData()"><spring:theme code="cat.plp.update"/> </button>
			</div>
			</div>
	</div>
<%-- 		<div class="cat-location-types" ng-class="(plp.view.branchLocations.ishidden)?'remove-margin-bottom':''" ng-if="plp.view.isNewProduct">
				<div class="filters" data-num="2">
					<section class="filter-desc">
						<div class="location-Type" ng-click="plp.toggleLocationFilter();"><spring:theme code="product.refinment.component.filters.location.type"/>
                        <div class="pull-right">
                        	<img class="expandImage" src="/_ui/responsive/cat/images/minus-icon.png" ng-hide="plp.view.branchLocations.ishidden"/>
                        	<img class="expandImage" src="/_ui/responsive/cat/images/plus-icon.png" ng-show="plp.view.branchLocations.ishidden"/> 
                         </div>
						</div>
						<div ng-hide="plp.view.branchLocations.ishidden">
						<label class="filter-checkgrp row">
							<div class="flit-text" ng-class="!plp.view.location.hasWareHouseUnit?'disabled':''"><spring:theme code="product.refinment.component.filters.location.dealer"/></div>
							<input type="checkbox" class="filter-check" readonly="" ng-model="plp.view.location.isWareHouseChecked" id="wareHouseCheckBox" ng-click="plp.locationTypesModified()" ng-disabled="!plp.view.location.hasWareHouseUnit" ng-class="!plp.view.location.hasWareHouseUnit?'disabled':''"><div class="box-rect"></div>
						</label>
						<label class="filter-checkgrp row">
							<div class="flit-text" ng-class="!plp.view.location.hascatPDCUnit?'disabled':''"><spring:theme code="product.refinment.component.filters.location.pdc"/></div>
							<input type="checkbox" class="filter-check" readonly="" ng-model="plp.view.location.iscatPDCChecked" id="catPDCCheckBox" ng-click="plp.locationTypesModified()" ng-disabled="!plp.view.location.hascatPDCUnit">
							<div class="box-rect"></div>
						</label>
						<label class="filter-checkgrp row">
							<div class="flit-text" ng-class="!plp.view.location.hasManufacturing?'disabled':''"><spring:theme code="product.refinment.component.filters.location.mfu"/></div>
							<input type="checkbox" class="filter-check" readonly="" ng-model="plp.view.location.isManufacturingChecked" ng-click="plp.locationTypesModified()" id="mancufacturingCheckBox" ng-disabled="!plp.view.location.hasManufacturing">
							<div class="box-rect"></div>
						</label>
					   </div>
					</section>
				</div>
		</div>
 --%>		<!-- In transit for New Product    -->
       <!-- <div class="cat-intransitData" ng-class="(plp.view.intransit.ishidden)?'remove-margin-bottom':''" ng-if="plp.view.intransitShow">
       	     <section class="filter-desc">
             <div class="inTransitData-Type" ng-click="plp.toggleIntransitFilter();"><spring:theme code="cat.plp.inTransit"/>
               <div class="pull-right">
               	<img class="expandImage" src="/_ui/responsive/cat/images/minus-icon.png" ng-hide="plp.view.intransit.ishidden"/> 
                <img class="expandImage" src="/_ui/responsive/cat/images/plus-icon.png" ng-show="plp.view.intransit.ishidden"/> 
               </div>
             </div>
			<label class="filter-checkgrp row" ng-hide="plp.view.intransit.ishidden">
				<div class="flit-text"><spring:theme code="cat.plp.inTransitMachines"/></div>
					
				<input type="checkbox" class="filter-check" readonly="" ng-model="plp.view.inTransitFlag" ng-click="plp.intransitDataChecked()">
				<div class="box-rect"></div>
			</label>
			 </section>
	   </div>
	   -->
        <!--   -->
		<div class="branch-locations" ng-if="!plp.view.isNewProduct && plp.view.branchLocations.masterSet.length>0">
		<div class="row">
			<div class="col-xs-12 pointer-style" ng-click="plp.toggleLocationFilter();">
				<span class="header"><spring:theme code="cat.plp.branchLocation"/></span> <img src="/_ui/responsive/cat/images/minus-icon.png" class="pull-right toggle-minus" ng-hide="plp.view.branchLocations.ishidden"> <img src="/_ui/responsive/cat/images/plus-icon.png" class="pull-right toggle-plus" ng-show="plp.view.branchLocations.ishidden">
			</div>
		</div>

		<div class="row" ng-hide="plp.view.branchLocations.ishidden">
			<div class="col-xs-12">
				<input class="search-location" type="text" placeholder="Search your location" ng-model="plp.query" restrict-to="[a-zA-Z ]"/>
				<img class="search-icon" src="/_ui/responsive/cat/images/search-icon-01.png" width=16 height=16>
			</div>
		</div>

		<div class="locations" ng-hide="plp.view.branchLocations.ishidden">
			<div class="row" ng-repeat="obj in plp.view.branchLocations.masterSet| filter: plp.query| limitTo:plp.view.branchLocations.displaySize">
				<div class="col-xs-12">
					<div class="location-group">
						<div class="location-name"> {{obj.text | titleCase}} </div>
						<div class="check-box pull-right">
							<input type="checkbox" name="shippingCheckbox" id="location-{{$index}}" value="true" ng-model="obj.isChecked" ng-change="plp.updateBranchLocation(obj)"> 
							<label for="location-{{$index}}"> <img class="tick-mark" src="/_ui/responsive/cat/images/tick-01.png" aria-hidden="true" width="10" height="10"/>
							</label> 
						</div>
					</div>
				</div>
			</div>

			<div class="expand" ng-show="(plp.view.branchLocations.masterSet| filter: plp.query).length > plp.view.branchLocations.defaultSize">
			<span class="more" ng-show="!!plp.view.branchLocations.viewMore" ng-click="plp.viewAllLocations()"><spring:theme code="cat.plp.view.branchlocations.viewmore"/></span>
			<span class="less" ng-show="!!plp.view.branchLocations.viewLess" ng-click="plp.viewLessLocations()"><spring:theme code="cat.plp.view.branchlocations.viewless"/></span>
			</div>
		</div>
	</div>
	<div class="slider-filters" ng-if="!plp.view.isNewProduct">
			<div class="manufacturing-year slider-container">
				<div class="row">
				<div class="col-xs-12 slider-header" ng-click="plp.toggleYearFilter();" ng-class="(plp.view.year.ishidden)?'remove-margin-bottom':''">
					<span class="header"><spring:theme code="cat.plp.manufacturingYear"/></span><img src="/_ui/responsive/cat/images/minus-icon.png" class="pull-right toggle-minus" ng-hide="plp.view.year.ishidden"> <img src="/_ui/responsive/cat/images/plus-icon.png" class="pull-right toggle-plus" ng-show="plp.view.year.ishidden">
				</div>
				</div>
				<div class="row" ng-hide="plp.view.year.ishidden">
					<div class="col-xs-5">
						<input type="text" placeholder="Min" ng-model="plp.view.year.minValue" restrict-to="[0-9]" ng-blur="plp.view.year.minValue = plp.validateMinInRange(plp.view.year.minValue,plp.view.year.options.floor,plp.view.year.maxValue);plp.sliderChanged()"> 
					</div>
					<div class="col-xs-2">
						<span class="minus"></span>
					</div>
					<div class="col-xs-5">
						<input type="text" placeholder="Max" class="pull-right" ng-model="plp.view.year.maxValue"  restrict-to="[0-9]" ng-blur="plp.view.year.maxValue = plp.validateMaxInRange(plp.view.year.maxValue,plp.view.year.options.ceil,plp.view.year.minValue);plp.sliderChanged()">
					</div>
				</div>
				<div class="row" ng-hide="plp.view.year.ishidden">
					<div class="slider-bar col-xs-12">
						<rzslider
							data-rz-slider-model="plp.view.year.minValue"
							data-rz-slider-high="plp.view.year.maxValue"
							data-rz-slider-options="plp.view.year.options">
						</rzslider>
					</div>
				</div>
			</div>

			<div class="hours-used slider-container">
				<div class="row">
				<div class="col-xs-12 slider-header" ng-click="plp.toggleHoursFilter();" ng-class="(plp.view.hours.ishidden)?'remove-margin-bottom':''">
					<span class="header"><spring:theme code="cat.plp.hoursUsed"/></span><img src="/_ui/responsive/cat/images/minus-icon.png" class="pull-right toggle-minus" ng-hide="plp.view.hours.ishidden"> <img src="/_ui/responsive/cat/images/plus-icon.png" class="pull-right toggle-plus" ng-show="plp.view.hours.ishidden">
				</div>
				</div>
				<div class="row" ng-hide="plp.view.hours.ishidden">
					<div class="col-xs-5">
						<input type="text" placeholder="Min"  ng-model="plp.view.hours.minValue" restrict-to="[0-9]" ng-blur="plp.view.hours.minValue = plp.validateMinInRange(plp.view.hours.minValue,plp.view.hours.options.floor,plp.view.hours.maxValue);plp.sliderChanged()"> 
					</div>
					<div class="col-xs-2">
						<span class="minus"></span>
					</div>
					<div class="col-xs-5">
						<input type="text" placeholder="Max" class="pull-right" ng-model="plp.view.hours.maxValue" restrict-to="[0-9]" ng-blur="plp.view.hours.maxValue = plp.validateMaxInRange(plp.view.hours.maxValue,plp.view.hours.options.ceil,plp.view.hours.minValue);plp.sliderChanged()">
					</div>
				</div>
				<div class="row" ng-hide="plp.view.hours.ishidden">
					<div class="slider-bar col-xs-12">
						<rzslider
							data-rz-slider-model="plp.view.hours.minValue"
							data-rz-slider-high="plp.view.hours.maxValue"
							data-rz-slider-options="plp.view.hours.options">
						</rzslider>
					</div>
				</div>
			</div>

			<div class="price slider-container">
				<div class="row">
				<div class="col-xs-12 slider-header" ng-click="plp.togglePriceFilter();" ng-class="(plp.view.price.ishidden)?'remove-margin-bottom':''">
					<span class="header"><spring:theme code="cat.plp.price"/></span><img src="/_ui/responsive/cat/images/minus-icon.png" class="pull-right toggle-minus" ng-hide="plp.view.price.ishidden"> <img src="/_ui/responsive/cat/images/plus-icon.png" class="pull-right toggle-plus" ng-show="plp.view.price.ishidden">
				</div>
				</div>
				<div class="row" ng-hide="plp.view.price.ishidden">
					<div class="col-xs-5">
						<input type="text" placeholder="Min" ng-model="plp.view.price.minValue" restrict-to="[0-9]" ng-blur="plp.view.price.minValue = plp.validateMinInRange(plp.view.price.minValue,plp.view.price.options.floor,plp.view.price.maxValue);plp.sliderChanged()"> 
					</div>
					<div class="col-xs-2">
						<span class="minus"></span>
					</div>
					<div class="col-xs-5">
						<input type="text" placeholder="Max" class="pull-right" ng-model="plp.view.price.maxValue" restrict-to="[0-9]" ng-blur="plp.view.price.maxValue = plp.validateMaxInRange(plp.view.price.maxValue,plp.view.price.options.ceil,plp.view.price.minValue);plp.sliderChanged()">
					</div>
				</div>
				<div class="row" ng-hide="plp.view.price.ishidden">
					<div class="slider-bar col-xs-12">
						<rzslider
							data-rz-slider-model="plp.view.price.minValue"
							data-rz-slider-high="plp.view.price.maxValue"
							data-rz-slider-options="plp.view.price.options">
						</rzslider>
					</div>
				</div>
			</div>


			<div class="certified">
			</div>
	</div>

	<div class="certified-filter" ng-if="!plp.view.isNewProduct">
		<div class="row">
				<div class="col-xs-12 certified-header" ng-click="plp.toggleCertifiedFilter();" ng-class="(plp.view.certifiedUser.ishidden)?'remove-margin-bottom':''">
					<span class="header"><spring:theme code="cat.plp.certification"/></span><img src="/_ui/responsive/cat/images/minus-icon.png" class="pull-right toggle-minus" ng-hide="plp.view.certifiedUser.ishidden"> <img src="/_ui/responsive/cat/images/plus-icon.png" class="pull-right toggle-plus" ng-show="plp.view.certifiedUser.ishidden">
				</div>
				</div>
		<div class="row" ng-hide="plp.view.certifiedUser.ishidden">
				<div class="col-xs-12">
					<div class="group">
						<div class="name"><spring:theme code="cat.plp.dealerCertified"/></div>
						<div class="check-box pull-right">
							<input type="checkbox"  id="certified" value="true" ng-model="plp.view.certifiedUser.isChecked" ng-change="plp.sliderChanged()"> 
							<label for="certified"> <img class="tick-mark" src="/_ui/responsive/cat/images/tick-01.png" aria-hidden="true" width="10" height="10"/>
							</label> 
						</div>
					</div>
				</div>
			</div>
	</div>

	<div class="update-btn-container-mobile visible-xs">
		<div class="row">
			<div class="col-xs-12">
				<button class="cat-buttons primary-btn update-button-mobile" ng-click="plp.updateSearchDataMobile()"> <spring:theme code="cat.plp.update"/> </button>
			</div>
		</div>
	</div>
</div>
</c:if>
<c:if test="${isPdpPage}">
<div class="pdpLeftRefinement" class="hidden-xs">
<!--  Commented Search Parameters as Confirmed with BA that this header should not come for any condition -->
	<c:if test="${not utilityProducts }">
		<div class="header">
			<div> <spring:theme code="cat.plp.search"/> </div>
			<div> <spring:theme code="cat.plp.parameters"/></div>
		</div>
	</c:if>

		<div class="facet-param">
			<div class="facet-label">
				<spring:theme code="product.refinment.component.product.family" />
			</div>
			<div class="facet-value">
				<c:if test="${not empty product.categories}">
					<c:choose>
						<c:when
							test="${productCondition ne null and productCondition eq 'USED'}">
							<c:if test="${fn:length(product.categories)>1}">
								<c:forEach var="category" items="${product.categories}">
									<c:if test="${category.name ne 'USED'}">
									${fn:toLowerCase(category.name)}
									</c:if>
								</c:forEach>
							</c:if>
							
							<c:if test="${fn:length(product.categories)<=1}">
							${fn:toLowerCase(product.categories[0].name)}
							</c:if>
						</c:when>
						<c:otherwise>
							${fn:toLowerCase(product.categories[0].name)}
						</c:otherwise>
					</c:choose>

				</c:if>
				<c:if test="${fn:length(product.categories)==0}">
				-
				</c:if>
			</div>
		</div>
		<div class="facet-param">
		<div class="facet-label"><spring:theme code="product.refinment.component.sales.model"/></div>
			<div class="facet-value"> ${salesModel}</div>
		</div>
		<c:if test="${not utilityProducts and productCondition ne null and productCondition ne 'USED'}">
		<div class="facet-param">
			<div class="facet-label"><spring:theme code="product.refinment.component.primary.offering"/></div>
			
			<c:set var="baseProductNumber" value="${product.baseProduct }"/>
			<div class="facet-value"> ${primaryOffering} (${baseProductNumber})</div> 
		</div>
	</c:if>
</div>

<c:if test="${isInventoryManager and not utilityProducts }">
	<div class="pdp-tools" ng-if="stockRecommend" ng-cloak>
	<div class="single-tool">
	<div><spring:theme code="cat.plp.stockingRecommendations"/></div>
	<cms:pageSlot position="StockLink" var="link" element="div"
				class="product-list-left-refinements-slot">
		<cms:component component="${link}" element="div" class="yComponentWrapper product-list-left-refinements-component nav-link"/>
	</cms:pageSlot>
	</div>
	</div>
	</c:if>
</c:if>
