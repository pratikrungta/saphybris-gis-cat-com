<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="pdp-tools">
	<c:forEach var="virtualShowRoomLink"
		items="${component.virtualShowroomLinks}">
		<div class="single-tool">
			<c:set var="linkUrl" value="${virtualShowRoomLink.linkRedirectUrl}" />
			<div>${virtualShowRoomLink.linkTitle}</div>
			<a class="nav-link" href="${linkUrl}" target="_blank"> ${virtualShowRoomLink.linkText}</a>
		</div>
	</c:forEach>
</div>
