<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="quote" tagdir="/WEB-INF/tags/responsive/quote" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<!-- CAT ACCESSORIES LIST PAGE -->


<template:page pageTitle="${pageTitle}">
<div class="cat-main-content">
    <div ng-app="cat-app" ng-controller="accessoriesController as actrl" ng-init="actrl.loadTourData('${userEmailId}');" ng-cloak >
        <div class="cat-container accessoriesWrapper  hidden-xs">
            <h2><spring:theme code="utv.accessories" /></h2>
            <div class="accessories-disclaimer"><cms:component uid="accessoriesShipMessageParagraphComponent"/></div>
            <div class="row seleAccLabel">
                <div class="col-md-12">
                    <span><spring:theme code="utv.accessories.show.accessories" /></span>
                </div>
                <div class="btn-group col-md-6 acc-model-dropdown" uib-dropdown keyboard-nav="true">
                    <button type="button" class="btn col-xs-6 drop-down-button" uib-dropdown-toggle ng-disabled="disabled">
                        <div class="text m-text">{{actrl.view.filterCUVModel}}</div> 
                    <div class="arrow pull-right">
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </div>
                    </button>
                    <ul class="dropdown-menu col-md-6" uib-dropdown-menu role="menu">
                        <li role="menuitem" ng-repeat="cuvModel in actrl.view.CUVDropDownData" ng-click="actrl.slectedFilter(cuvModel)"><a>{{cuvModel}}</a></li>
                    </ul>
                </div>
            </div>
            <div class="accContentWrap">
                <div class="row partHeadings">
                    <div class="col-md-3">
                        <h3><spring:theme code="utv.accessories.part.description"/></h3>
                    </div>
                    <div class="col-md-2">
                        <h3><spring:theme code="utv.accessories.part.number"/></h3>
                    </div>
                    <div class="col-md-1 text-right no-padding">
                        <h3><spring:theme code="utv.accessories.list.price"/></h3>
                    </div>
                    <div class="col-md-2 text-center">
                        <h3 class="cuv-model" id="accTourDesk1">
                            <div class="message" uib-popover-html="'<spring:theme code="cat.accessories.cuv.model.popup"/>'" popover-placement="bottom" popover-trigger="'mouseenter'">
                                <spring:theme code="utv.accessories.cuv.model"/>
                            </div>
                        </h3>
                    </div>
                    <div class="col-md-2 text-center">
                        <h3 id="accTourDesk2"><spring:theme code="utv.accessories.quantity"/></h3>    
                    </div>
                    <div class="col-md-2 text-right total-price">
                        <h3><spring:theme code="utv.accessories.total.price"/></h3>    
                    </div>
                </div>

                <!-- Loop here for each category -->
                <div class="accTypeWrap" ng-repeat="(key, val) in actrl.view.accessoriesData track by $index "> 
                    <div class="row acc-tab-head accRecommend" ng-click="actrl.view.accordianOpen[key] = !actrl.view.accordianOpen[key]">
                        <div class="col-md-6 text-left no-padding">
                            <h4>{{key}}</h4>
                        </div>
                        <div class="col-md-6 text-right no-padding">
                            <div class="arrow pull-right">
                                <img src="/_ui/responsive/cat/images/arrow-{{actrl.view.accordianOpen[key] ? 'up' : 'down'}}.png" aria-hidden="true"/>
                                <!--<i class="fa fa-angle-{{actrl.view.accordianOpen[key] ? 'up' : 'down'}}" aria-hidden="true"></i>-->
                            </div>
                        </div>
                    </div>
                    <div class="line-item" ng-repeat="obj in val track by $index" ng-if="actrl.view.accordianOpen[key]">
                        <div class="row acc-tab-details">
                            <div class="col-md-3">
                                <div class="recommendTag pull-left" ng-if="obj.isRecommended"><spring:theme code="utv.accessories.recommended"/> <img src="/_ui/responsive/cat/images/tick@3x.png" /></div>
                                <h4 class="accName no-padding col-md-12">{{obj.name}}</h4>
                                <p class="accCompModel col-md-9 no-padding"><spring:theme code="utv.accessories.compatible.models"/> {{obj.compatibleModels.join(', ')}}</p>
                            </div>
                            <div class="col-md-2">{{obj.code}}</div>
                            <div class="col-md-1 text-right no-padding">{{obj.price.formattedValue}}</div>
                            <div class="col-md-2 text-center">
                                <div class="btn-group col-md-12 acc-model-dropdown" uib-dropdown keyboard-nav="true"  ng-if="obj.compatibleModels.length !== actrl.view.CUVDropDownData.length-1">
                                    <button type="button" class="btn col-md-12 drop-down-button" uib-dropdown-toggle ng-disabled="disabled">
                                        <div class="text m-text">{{obj.lineItems[0].modelNumber}}</div> 
                                    <div class="arrow pull-right">
                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                    </div>
                                    </button>
                                    <ul class="dropdown-menu col-md-11" uib-dropdown-menu role="menu">
                                        <li role="menuitem" ng-repeat="cuvModel in obj.lineItems[0].listModels" ng-click="actrl.updateLineItemCUV(key, obj.index, 0, cuvModel)" ><a>{{cuvModel}}</a></li>
                                    </ul>
                                </div>
                                <div class="btn-group col-md-12 acc-model-dropdown" ng-if="obj.compatibleModels.length === actrl.view.CUVDropDownData.length-1">
                                    <span>
                                        <spring:theme code="utv.accessories.compatible.all.models"/>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-2 text-center accQuantControls">
                                    <div class="input-params">
                                        <span class="d-inline-block" ng-click="actrl.updateQuantity(key, obj.index, 0, 'decrement')"><img class="decrement" ng-class="{'disabled': (actrl.disableQuantityControls(obj, 0) || !obj.lineItems[0].quantity)}" src="/_ui/responsive/cat/images/minus-icon.png"></span>
                                        <input type="text" size=1 ng-model="obj.lineItems[0].quantity" ng-disabled="actrl.disableQuantityControls(obj, 0)" restrict-to="[0-9]" placeholder="0" ng-blur="actrl.updateQuantity(key, obj.index, 0, 'blur')"/> 
                                        <img class="increment" src="/_ui/responsive/cat/images/plus-icon.png"  ng-click="actrl.updateQuantity(key, obj.index, 0, 'increment')">
                                    </div>
                            </div>
                            <div class="col-md-2 text-right totPrice">{{obj.lineItems[0].formattedTotalPrice}}</div>            
                        </div>
                        <div class="row acc-tab-details" ng-repeat="(lineItemIdx, lineItem) in obj.lineItems" ng-if="obj.lineItems.length > 1 && lineItemIdx < obj.lineItems.length-1">
                            <div class="col-md-2 col-md-offset-3">{{obj.code}}</div>
                            <div class="col-md-1 text-right no-padding">{{obj.price.formattedValue}}</div>
                            <div class="col-md-2 text-center">
                                <div class="btn-group col-md-12 acc-model-dropdown" uib-dropdown keyboard-nav="true">
                                    <button type="button" class="btn col-md-12 drop-down-button" uib-dropdown-toggle ng-disabled="disabled">
                                        <div class="text m-text">{{obj.lineItems[lineItemIdx+1].modelNumber}}</div> 
                                    <div class="arrow pull-right">
                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                    </div>
                                    </button>
                                    <ul class="dropdown-menu col-md-11" uib-dropdown-menu role="menu">
                                        <li role="menuitem" ng-repeat="cuvModel in obj.lineItems[lineItemIdx+1].listModels" ng-click="actrl.updateLineItemCUV(key, obj.index, lineItemIdx+1, cuvModel)" ><a>{{cuvModel}}</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-2 text-center accQuantControls">
                                    <div class="input-params">
                                        <span ng-click="actrl.updateQuantity(key, obj.index, lineItemIdx+1, 'decrement')"><img class="decrement" ng-class="{'disabled': (actrl.disableQuantityControls(obj, lineItemIdx+1) || !obj.lineItems[lineItemIdx+1].quantity)}" src="/_ui/responsive/cat/images/minus-icon.png"></span>
                                        <input type="text" size=1 ng-model="obj.lineItems[lineItemIdx+1].quantity" ng-disabled="actrl.disableQuantityControls(obj, lineItemIdx+1)" restrict-to="[0-9]" placeholder="0" ng-blur="actrl.updateQuantity(key, obj.index, lineItemIdx+1, 'blur')"/> 
                                        <img class="increment" src="/_ui/responsive/cat/images/plus-icon.png" ng-click="actrl.updateQuantity(key, obj.index, lineItemIdx+1, 'increment')">
                                    </div>
                            </div>
                            <div class="col-md-2 text-right totPrice">{{obj.lineItems[lineItemIdx+1].formattedTotalPrice}}</div>            
                        </div>
                        <div id="accTourDeskStep3" class="row accAddNew" ng-if="obj.compatibleModels.length === actrl.view.CUVDropDownData.length-1 &&  $parent.$parent.$index == 0 && $parent.$index == 0 && actrl.tourFourthElement">
                            <div id="accTourDesk3" class="col-md-9 col-md-offset-3">
                                <button class="default-button" ng-click="actrl.addLineItem(key, obj.index)" ng-disabled="obj.maxQuantity <= obj.lineItems.length">
                                    <img class="increment" src="/_ui/responsive/cat/images/plus-icon.png"> <spring:theme code="utv.accessories.add"/>
                                </button>
                            </div> 
                        </div>
                        <div class="row accAddNew" ng-if="obj.compatibleModels.length !== actrl.view.CUVDropDownData.length-1">
                            <div id="accTourDesk3" class="col-md-9 col-md-offset-3">
                                <button class="default-button" ng-click="actrl.addLineItem(key, obj.index)" ng-disabled="obj.maxQuantity <= obj.lineItems.length">
                                    <img class="increment" src="/_ui/responsive/cat/images/plus-icon.png"> <spring:theme code="utv.accessories.add"/>
                                </button>
                            </div> 
                        </div>
                    </div>
                </div>
                <div class="total-section">
                    <div class="col-md-2 col-md-offset-6 text-right total-label">
                        <spring:theme code="utv.accessories.total.caps"/>
                    </div>
                    <div class="col-md-2 text-center total-count">
                        <span>{{actrl.view.totalCount}}</span>
                    </div>
                    <div class="col-md-2 text-right totPrice"><span>{{actrl.getPriceFormat(actrl.view.totalPrice)}}</span></div>
                </div>
            </div>
            <div class="row reviewBtn">
                <button id="accTourDesk4" class="btn cat-buttons primary-btn reorder-cart-button pull-right" ng-disabled="!actrl.view.totalCount" ng-click="actrl.reviewOrder('${cartType}');"><spring:theme code="utv.accessories.review.order"/></button>
            </div>
        </div>

        <!-- Accessories Mobile markup -->

        <div class="cat-container accessoriesWrapper-mob visible-xs no-margin">
            <div class="row">
                <h2 class="col-xs-8"><spring:theme code="utv.accessories" /></h2>   
                <div class="col-xs-12 accessories-disclaimer"><cms:component uid="accessoriesShipMessageParagraphComponent"/></div> 
            </div>
            <div class="row seleAccLabel">
                <div class="col-xs-12">
                    <span><spring:theme code="utv.accessories.show.accessories" /></span>
                </div>
                <div class="btn-group col-xs-12 acc-model-dropdown" uib-dropdown keyboard-nav="true">
                    <button type="button" class="btn col-xs-11 drop-down-button" uib-dropdown-toggle ng-disabled="disabled">
                        <div class="text m-text">{{actrl.view.filterCUVModel}}</div> 
                    <div class="arrow pull-right">
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </div>
                    </button>
                    <ul class="dropdown-menu col-xs-11" uib-dropdown-menu role="menu">
                        <li role="menuitem" ng-repeat="cuvModel in actrl.view.CUVDropDownData" ng-click="actrl.slectedFilter(cuvModel)"><a>{{cuvModel}}</a></li>
                    </ul>
                </div>
            </div>
            <div class="accContentWrapMob" ng-repeat="(key, val) in actrl.view.accessoriesData">
                <div class="row acc-tab-head accRecommend" ng-click="actrl.view.accordianOpen[key] = !actrl.view.accordianOpen[key]">
                    <div class="col-xs-8 text-left no-padding">
                        <h4>{{key}}</h4>
                    </div>
                    <div class="col-xs-4 text-right no-padding">
                        <div class="arrow pull-right">
                            <img src="/_ui/responsive/cat/images/arrow-{{actrl.view.accordianOpen[key] ? 'up' : 'down'}}.png" aria-hidden="true"/>
                            <!-- <i class="fa fa-angle-{{actrl.view.accordianOpen[key] ? 'up' : 'down'}}" aria-hidden="true"></i> -->
                        </div>
                    </div>
                </div>
                <div id="accessories{{$index}}" class="row acc-tab-details" ng-repeat="obj in val track by $index" ng-if="actrl.view.accordianOpen[key]">
                    <div class="col-xs-12 partDesc">
                        <h3 class="partDescHeading"><spring:theme code="utv.accessories.part.description"/></h3>
                        <div class="recommendTag pull-left" ng-if="obj.isRecommended"><spring:theme code="utv.accessories.recommended"/> <img src="/_ui/responsive/cat/images/tick@3x.png" /></div>
                        <h4 class="accName no-padding col-xs-12">{{obj.name}}</h4>
                        <p class="accCompModel col-xs-9 no-padding"><spring:theme code="utv.accessories.compatible.models"/> {{obj.compatibleModels.join(', ')}}</p>
                    </div>
                    <div class="col-xs-12 partConfig" ng-repeat="(lineItemIdx, lineItem) in obj.lineItems">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="light-head"><spring:theme code="utv.accessories.part.number"/></div>
                                <div>{{obj.code}}</div>
                            </div>
                            <div class="col-xs-6">
                                <div class="light-head"><spring:theme code="utv.accessories.list.price"/></div>
                                <div>{{obj.price.formattedValue}}</div>
                            </div>
                            <div id="accTourMob1{{$parent.$index}}" class="col-xs-12 partConfigMargin">
                                <div class="light-head">
                                    <div class="cuv-model-mobile" uib-popover-html="'<spring:theme code="cat.accessories.cuv.model.popup"/>'" popover-placement="bottom" popover-trigger="'mouseenter'">
                                        <div class="message">
                                            <spring:theme code="utv.accessories.cuv.model"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-group col-xs-8 acc-model-dropdown no-padding" uib-dropdown keyboard-nav="true" ng-if="obj.compatibleModels.length !== actrl.view.CUVDropDownData.length-1">
                                    <button type="button" class="btn col-xs-8 drop-down-button" uib-dropdown-toggle ng-disabled="disabled">
                                        <div class="text m-text">{{obj.lineItems[lineItemIdx].modelNumber}}</div> 
                                    <div class="arrow pull-right">
                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                    </div>
                                    </button>
                                    <ul class="dropdown-menu col-xs-8" uib-dropdown-menu role="menu">
                                        <li role="menuitem" ng-repeat="cuvModel in obj.lineItems[lineItemIdx].listModels" ng-click="actrl.updateLineItemCUV(key, obj.index, lineItemIdx, cuvModel)" ><a>{{cuvModel}}</a></li>
                                    </ul>
                                </div>
                                <div class="btn-group col-xs-4 acc-model-dropdown no-padding" ng-if="obj.compatibleModels.length === actrl.view.CUVDropDownData.length-1">
                                    <span>
                                        <spring:theme code="utv.accessories.compatible.all.models"/>
                                    </span>
                                </div>
                            </div>
                            <div id="accTourMob2{{$parent.$index}}" class="col-xs-6 accQuantControls">
                                <div class="light-head"><spring:theme code="utv.accessories.quantity"/></div>
                                <div class="input-params">
                                    <span ng-click="actrl.updateQuantity(key, obj.index, lineItemIdx, 'decrement')"><img class="decrement" ng-class="{'disabled': actrl.disableQuantityControls(obj, lineItemIdx) || !obj.lineItems[lineItemIdx].quantity}" src="/_ui/responsive/cat/images/minus-icon.png"></span>
                                    <input type="text" size=1 ng-model="obj.lineItems[lineItemIdx].quantity" ng-disabled="actrl.disableQuantityControls(obj, lineItemIdx)" ng-trim="false" restrict-model-input="^[0-9]+$" placeholder="0" ng-blur="actrl.updateQuantity(key, obj.index, lineItemIdx, 'blur')"/> 
                                    <img class="increment" src="/_ui/responsive/cat/images/plus-icon.png" ng-click="actrl.updateQuantity(key, obj.index, lineItemIdx, 'increment')">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="light-head"><spring:theme code="utv.accessories.total.price"/></div>
                                <div>{{obj.lineItems[lineItemIdx].formattedTotalPrice}}</div>
                            </div>
                        </div>
                    </div>
                    <div id="accTourMobStep3" class="row accAddNew" ng-if="obj.compatibleModels.length === actrl.view.CUVDropDownData.length-1 && $parent.$parent.$index == 0 && $parent.$index == 0 && actrl.tourFourthElement">
                        <div id="accTourMob3" class="col-xs-12">
                            <button class="default-button" ng-click="actrl.addLineItem(key, obj.index)" ng-disabled="obj.maxQuantity <= obj.lineItems.length">
                                <img class="increment" src="/_ui/responsive/cat/images/plus-icon.png" ng-click=""> <spring:theme code="utv.accessories.add"/>
                            </button>
                        </div> 
                    </div>
                    <div class="row accAddNew" ng-if="obj.compatibleModels.length !== actrl.view.CUVDropDownData.length-1">
                        <div id="accTourMob3" class="col-xs-12">
                            <button class="default-button" ng-click="actrl.addLineItem(key, obj.index)" ng-disabled="obj.maxQuantity <= obj.lineItems.length">
                                <img class="increment" src="/_ui/responsive/cat/images/plus-icon.png" ng-click=""> <spring:theme code="utv.accessories.add"/>
                            </button>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="row  view-cart-section no-gutters visible-xs">
            <div class="col-xs-6">
                <span class="col-xs-12 mb-units-added"><spring:theme code="utv.accessories.total"/></span>
                <span class="col-xs-12 mb-total-price">{{actrl.getPriceFormat(actrl.view.totalPrice)}}</span>
                <span class="col-xs-12 mb-units">{{actrl.view.totalCount}} <spring:theme code="utv.accessories.units"/></span>
            </div>
            <button id="accTourMob4" class="view-cart-btn btn col-xs-6" ng-disabled="!actrl.view.totalCount" ng-click="actrl.reviewOrder('${cartType}');"><spring:theme code="utv.accessories.review.order"/></button>
        </div>
        </div>
    </div>
</div>

</template:page>