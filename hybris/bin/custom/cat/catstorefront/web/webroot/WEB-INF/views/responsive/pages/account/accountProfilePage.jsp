<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<template:page pageTitle="${pageTitle}">

	<c:forEach items="${unit.addresses}" var="address">
		<c:if test="${(address.billingAddress) eq true}">
			<c:set var="billingAddress" value="${address}" />
		</c:if>
		<c:if test="${not empty address.publicKey}">
			<c:set var="dealerAddress" value="${address}"/>
    	</c:if>
	</c:forEach>
	


	<div class="main-content row">
		<div class="my-profile cat-container" ng-app="cat-app" ng-controller="profilePageController as ppc" ng-init="ppc.roleFeatureInfoProfile('${isUTVUser}','${isIM}','${isSP}');">
			<div class="page-header-title">
				<div>
					<spring:theme code="cat.profile.title" />
				</div>
			</div>
			<div class="row profile-container">
				<!-- <div class="col-xs-12 col-sm-3 left-content">
					<div class="row">
						<div class="col-xs-12 heading"> 
							<spring:theme code="text.account.profile.header" text="Profile"/> 
						</div>
					</div>

					<div class="row">
					<div class="col-xs-12 user-name"> ${fn:escapeXml(customerData.firstName)} &nbsp; ${fn:escapeXml(customerData.lastName)} </div>
					</div>
					<div class="row">
					<div class="col-xs-12 user-email"> ${fn:escapeXml(customerData.email)}</div> 
					</div>
					<div class="row">
					<c:if test="${customerData.userRole eq 'dealerUTVManagerGrp' }">
					<div class="col-xs-12 user-contact">Inventory Manager</div>
					</c:if>
					<c:if test="${customerData.userRole ne 'dealerUTVManagerGrp' }">
					<div class="col-xs-12 user-contact">${fn:escapeXml(customerData.userRole)}</div>
					</c:if>
					</div> 
					<!- <div class="row">
						<div class="col-xs-12 user-password"> <button class="update-password"> update password</button> </div>
					</div> ->
				</div> -->
				<div class="col-xs-12 right-content">
					<div class="row profile-user-info">
						<!-- <div class="col-xs-1"></div> -->
						<div class="col-xs-12 adjust">
							<span class="user-contact-xs">${fn:escapeXml(customerData.userRole)}</span>
							<span class="user-name"><span>
									${fn:escapeXml(customerData.firstName)}</span> <span>
									${fn:escapeXml(customerData.lastName)} </span></span> <span
								class="user-contact">${fn:escapeXml(customerData.userRole)}</span>
							<span class="user-email pull-right">${fn:escapeXml(customerData.email)}</span>
						</div>
						<!-- <div class="col-xs-1"></div> -->
					</div>
					<div class="row border-top dealer-info">
						<div class="row">
							<div class="col-xs-12 heading">
								<spring:theme code="text.account.profile.dealerInformation"
									text="Dealer Information" />
							</div>
						</div>


						<div class="row">
							<div class="col-xs-12 col-md-6 user-profile-cell">
								<label><spring:theme code="dealerInformation.dealerCode"
										text="Dealer Code" />
									<div class="hidden-xs">:</div></label>
								<div>${fn:escapeXml(unit.uid)}</div>
							</div>
							<div class="col-xs-12 col-md-6  user-profile-cell">
								<label><spring:theme code="dealerInformation.dealerName"
										text="Dealer name" />
									<div class="hidden-xs">:</div></label>
								<div>${fn:toLowerCase(fn:escapeXml(unit.name))}</div>
							</div>
						</div>

						<div class="row contact-fax-xs">
							<div class="col-xs-6 user-profile-cell">
								<div class="row">
									<div class="col-md-12">
											<label> <spring:theme code="dealerInformation.phone"
												text="Contact Phone" />
											<div class="hidden-xs">:</div></label>
										<div>${fn:escapeXml(dealerAddress.phone)}</div>
									</div>
									<div class="col-md-12 no-padding hidden-xs dealer-description">
													<div class="col-xs-12 user-profile-cell">
														<label><spring:theme
																code="dealerInformation.description" text="Dealer description" />
															<div class="hidden-xs">:</div></label>
														<div class="descriptionValue">${fn:toLowerCase(fn:escapeXml(unit.description))}</div>
													</div>
									</div>
								</div>
								
								
							</div>
							<div class="col-xs-6 user-profile-cell">
								<div class="row">
										<label class="pull-left dealer-address-label"><spring:theme code=""
											text="DEALER ADDRESS" />
										<div class="hidden-xs">:</div></label>
									<div class='col-md-8 no-padding'>
											<div class="col-xs-12">${fn:escapeXml(dealerAddress.line1)}</div>
											<div class="col-xs-12">${fn:escapeXml(dealerAddress.line2)}</div>
											<div class="col-xs-12">${fn:escapeXml(dealerAddress.town)}</div>
											<div class="col-xs-12">${fn:escapeXml(dealerAddress.country.name)}</div>
											<div class="col-xs-12">${fn:escapeXml(dealerAddress.region.name)}</div>
											<div class="col-xs-12">${fn:escapeXml(dealerAddress.postalCode)}</div>
									</div>
								</div>
											
								
							</div>
						</div>

						<div class="row visible-xs">
							<div class="col-xs-12 user-profile-cell">
								<label><spring:theme
										code="dealerInformation.description" text="Dealer description" />
									<div class="hidden-xs">:</div></label>
								<div class="descriptionValue">${fn:toLowerCase(fn:escapeXml(unit.description))}</div>
							</div>
						</div>
						<div class="row visible-xs">
								<div class="col-xs-12 user-profile-cell">
									<label> <spring:theme code="dealerInformation.phone"
											text="Contact Phone" />
										<div class="hidden-xs">:</div></label>
									<div>${fn:escapeXml(dealerAddress.phone)}</div>
								</div>
								<div class="col-xs-12 user-profile-cell">
									<label><spring:theme code=""
											text="DEALER ADDRESS" />
										<div class="hidden-xs">:</div></label>		
									<div>${fn:escapeXml(dealerAddress.line1)}</div>
									<div>${fn:escapeXml(dealerAddress.line2)}</div>
									<div>${fn:escapeXml(dealerAddress.town)}</div>
									<div>${fn:escapeXml(dealerAddress.country.name)}</div>
									<div>${fn:escapeXml(dealerAddress.region.name)}</div>
									<div>${fn:escapeXml(dealerAddress.postalCode)}</div>
								</div>
							</div>
					</div>
					
<!-- Commenting out shipping address section as per 2688 -->
<!--  
					<div class="row border-top shipping-info">
						<div class="row">
							<div class="col-xs-12 heading">
								<spring:theme code="userInformation.shippingAddress"
									text="Shipping Address" />
							</div>
						</div> -->
						
						<!-- Single Shippng address start -->
						<!--  
						<c:if test="${not empty unit.listOFDealerShipTos}">
						<c:set var="a" value="1" />
							<c:forEach var="shippingAddress"
								items="${unit.listOFDealerShipTos}">
								<div>ADDRESS ${a}</div>
								<c:set var="a" value="${a+1}" />
								
								<div class="atomic-shipping-address">
								<div class="row">
										<div class="col-xs-12 col-md-6 user-profile-cell">
											<label><spring:theme
													code="" text="Ship to code : " />
												</label>
											<div>
												${fn:toLowerCase(fn:escapeXml(shippingAddress.shipToCode))}</div>
										</div>
										</div>
									<div class="row">
										<div class="col-xs-12 col-md-6 user-profile-cell">
											<label><spring:theme
													code="" text="" />
												</label>
											<div>
												${fn:toLowerCase(fn:escapeXml(shippingAddress.line1))}</div>
										</div>
										<div class="col-xs-6 user-profile-cell">
											<label><spring:theme
													code="" text="" />
												</label>
											<div>
												${fn:toLowerCase(fn:escapeXml(shippingAddress.line2))}</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6 user-profile-cell">
											<label><spring:theme
													code="" text="" />
												 </label>
											<div>
												${fn:toLowerCase(fn:escapeXml(shippingAddress.town))}</div>
										</div>
										<div class="col-xs-6 user-profile-cell">
											<label><spring:theme
													code="" text="" />
												 </label>
											<div>${fn:escapeXml(shippingAddress.region.name)}</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6 user-profile-cell">
											<label><spring:theme
													code="" text="" />
												 </label>
											<div>${fn:escapeXml(shippingAddress.country.name)}</div>
										</div>
										<div class="col-xs-6 user-profile-cell">
											<label><spring:theme
													code="" text="" />
												</label>
											<div>${fn:escapeXml(shippingAddress.postalCode)}</div>
										</div>
									</div>
								</div>
							</c:forEach>
						</c:if> -->
						
						<!-- Single Shippng address end -->
						

					</div>
					<%-- <div class="row border-top billing-info">
					<div class="row">
						<div class="col-xs-12 heading">
							<spring:theme code="userInformation.billingAddress" text="Billing Address"/>
						</div>
						</div>

						<!-- Single Billing address start -->
						<div class="atomic-billing-address">
							<div class="row">
								<div class="col-xs-12 col-md-6  user-profile-cell">
									<label><spring:theme code="userInformation.address.line1" text=""/><div class="hidden-xs">:</div></label> 
									<div> ${fn:escapeXml(billingAddress.line1)}</div>
								</div>
								<div class="col-xs-12 col-md-6  user-profile-cell">
									<label><spring:theme code="userInformation.address.line2" text=""/><div class="hidden-xs">:</div></label> 
									<div> ${fn:escapeXml(billingAddress.line2)} </div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6 user-profile-cell">
									<label><spring:theme code="userInformation.address.city" text=""/><div class="hidden-xs">:</div> </label> 
									<div> ${fn:toLowerCase(fn:escapeXml(billingAddress.town))} </div>
								</div>
								<div class="col-xs-6 user-profile-cell">
									<label><spring:theme code="userInformation.address.state" text=""/><div class="hidden-xs">:</div> </label> 
									<div> ${fn:escapeXml(billingAddress.region.name)}</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-6 user-profile-cell">
									<label><spring:theme code="userInformation.address.country" text=""/><div class="hidden-xs">:</div> </label> 
									<div> ${fn:escapeXml(billingAddress.country.name)}</div>
								</div>
								<div class="col-xs-6 user-profile-cell">
									<label><spring:theme code="userInformation.address.postalCode" text=""/><div class="hidden-xs">:</div> </label> 
									<div> ${fn:escapeXml(billingAddress.postalCode)}</div>
								</div>
							</div>
						</div>
						<!-- Single Billing address end -->
						
					</div> --%>
				</div>
			</div>
		</div>
	</div>
	<%-- <a class="button" href="update-password"><spring:theme code="text.account.profile.changePassword" text="Change password"/></a>
<a class="button" href="update-profile"><spring:theme code="text.account.profile.updatePersonalDetails" text="Update personal details"/></a>
<a class="button" href="update-email"><spring:theme code="text.account.profile.updateEmail" text="Update email"/></a> --%>

</template:page>