<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="quote" tagdir="/WEB-INF/tags/responsive/quote" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>


<template:page pageTitle="${pageTitle}">
<div class="cat-main-content">
    <div class="cat-container" ng-app="cat-app" ng-controller="countyMappingController as cmc" ng-cloak>
        <div class="row county-container hidden-xs">
            <h2 class="col-md-12"><spring:theme code="cat.county.mapping.heading.text" /></h2>
            <div class="col-md-12 pagination-row text-right hidden-xs" ng-show="cmc.view.countiesList && cmc.view.countiesList.length > cmc.view.pagination.pageSize">
                <ul uib-pagination boundary-links="true" total-items="cmc.view.pagination.totalNumberOfResults" ng-model="cmc.view.pagination.currentPage" items-per-page="cmc.view.pagination.pageSize" ng-change="cmc.pageChanged()" previous-text="&laquo;" next-text="&raquo;" first-text="" last-text="" max-size="4"></ul>
            </div>
            <div class="county-content-wrapper cat-container row">
                <div class="col-md-3 county-mapping-left-wrap">
                    <div class="row unhide-section">
                        <h3 class="col-md-12 no-padding text-uppercase"><spring:theme code="cat.county.mapping.branch" /></h3>
                        <span class="col-md-12 no-padding"><spring:theme code="cat.county.mapping.hidden.addresses.text" /></span>
                        <div class="btn-group col-md-12 acc-model-dropdown no-padding" uib-dropdown keyboard-nav="true">
                            <button type="button" class="btn col-md-12 drop-down-button" uib-dropdown-toggle ng-disabled="disabled">
                                <div class="text m-text">{{cmc.view.selectedHiddenAddress || cmc.view.defaultSelectOption}}</div> 
                            <div class="arrow pull-right">
                                <img class="pull-right" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png" src="/_ui/responsive/cat/images/dropdown-arrow.png">
                            </div>
                            </button>
                            <ul class="dropdown-menu col-md-12" uib-dropdown-menu role="menu">
                                <li role="menuitem" ng-click="cmc.onDDChange()">
                                    <a>{{cmc.view.defaultSelectOption}}</a>
                                </li>
                                <li role="menuitem" ng-repeat="hiddenAddress in cmc.view.hiddenShippingAddress track by $index" ng-click="cmc.onDDChange($index)">
                                    <a>
                                        <p>{{hiddenAddress.shipToCode}}, {{hiddenAddress.line1}}, </p>
                                        <p>{{hiddenAddress.town}}, {{hiddenAddress.region.name}}, {{hiddenAddress.country.name}},</p>
                                        <p>{{hiddenAddress.postalCode}}</p>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <button class="btn-primary pull-right text-center" ng-disabled="!cmc.view.selectedHiddenAddress" ng-click="cmc.unHideShipTo()"><spring:theme code="cat.county.mapping.unhide.text" /></button>
                    </div>
                    <div class="county-address-container row">
                        <a href="JavaScript:void(0);" class="county-address pull-left clearfix" ng-repeat="(idx, shipTo) in cmc.view.shippingAddressList track by $index" ng-click="cmc.getCountiesofShipTo(shipTo.id)" ng-class="{'selected': cmc.view.selectedShipTo == shipTo.id}">
                                <h3 class="col-md-6"><spring:theme code="cat.county.mapping.Address" /> {{idx+1}}</h3>
                                <h3 class="col-md-6 text-right hide-text">
                                <button  ng-click="cmc.hideShipTo($event, shipTo.id)"><spring:theme code="cat.county.mapping.hide.text" /></button>
                                </h3>
                                <div class="col-md-12 no-padding">
                                    <div>{{shipTo.shipToCode}}, {{shipTo.line1}}, {{shipTo.town}},</div>
                                    <div>{{shipTo.region.name}}, {{shipTo.country.name}},</div>
                                    <div>{{shipTo.postalCode}}</div>
                                </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-9 county-mapping-result-wrap">
                    <div class="row">
                            <h3 class="col-md-12 no-padding"><spring:theme code="cat.county.mapping.linkedCounties" /> ({{cmc.view.pagination.totalNumberOfResults}})</h3>
                    </div>
                    <div class="row">
                        <table class="county-table">
                            <thead>
                                <th class="col-md-5" ><spring:theme code="cat.county.mapping.countyName" /> <span class="fa" ng-class="{'fa-angle-up': (cmc.view.sortKey !== 'countyName' || (cmc.view.sortKey === 'countyName' && cmc.view.sortOrder == cmc.view.ASC)), 'fa-angle-down': (cmc.view.sortKey === 'countyName' && cmc.view.sortOrder == cmc.view.DESC)}" ng-click="cmc.onSortClick('countyName')"></span></th>

                                <th class="col-md-3"><spring:theme code="cat.county.link.stateCountry" /></th>

                                <th class="col-md-4"><spring:theme code="cat.county.mapping.modifiedDate" /></th>
                            </thead>
                            <tbody>
                                <tr ng-repeat="(countyIndex, county) in cmc.view.countiesList | limitTo: cmc.view.pagination.pageSize : cmc.view.pagination.pageSize*(cmc.view.pagination.currentPage-1)" ng-if="cmc.view.countiesList && cmc.view.countiesList.length">
                                    <td class="col-md-5">{{county.countyName}}</td>
                                    <td class="col-md-3">{{county.sellState.name}},{{county.sellCountry.name}}</td>
                                    <td class="col-md-4">{{county.lastModifiedTime}} <a href="#" class="del-county" ng-click="cmc.delinkCounty(county.id)"><spring:theme code="cat.county.mapping.deleteLowerCase" /></a></td>
                                </tr>
                                
                            </tbody>
                        </table>
                        <div class="no-counties" ng-if="cmc.view.shippingAddressList && cmc.view.shippingAddressList.length && (!cmc.view.countiesList || !cmc.view.countiesList.length)">
                        	<spring:theme code="cat.county.mapping.noCountyErrorMessage" />
                        </div>
                        <div class="no-un-hidden-shiptos" ng-if="!cmc.view.shippingAddressList || !cmc.view.shippingAddressList.length">
                            <span><spring:theme code="cat.county.mapping.addresses.notvisible.message1" /></span>
                            <span><spring:theme code="cat.county.mapping.addresses.notvisible.message2" /></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 pagination-row text-right hidden-xs" ng-show="cmc.view.countiesList && cmc.view.countiesList.length > cmc.view.pagination.pageSize">
                <ul uib-pagination boundary-links="true" total-items="cmc.view.pagination.totalNumberOfResults" ng-model="cmc.view.pagination.currentPage" items-per-page="cmc.view.pagination.pageSize" ng-change="cmc.pageChanged()" previous-text="&laquo;" next-text="&raquo;" first-text="" last-text="" max-size="4"></ul>
            </div>
            <div class="col-md-12 add-county-info text-right hidden-xs pull-right">
                <p><spring:theme code="cat.county.mapping.message1" /></p>
                <p><spring:theme code="cat.county.mapping.message2" /></p>
            </div>
            <div class="col-md-12 add-county-btn">
                <a href="" class="btn-primary pull-right" ng-disabled="!cmc.view.enableAddCounty || !cmc.view.shippingAddressList.length" ng-click="cmc.addCounty()"><spring:theme code="cat.county.mapping.addCounty" /></a>
            </div>
        </div>

        <div class="county-mapping-container-mob visible-xs row">
            <h2 class="col-xs-12"><spring:theme code="cat.county.mapping.heading.text" /></h2>
            <div class="row unhide-section">
                <h2 class="col-xs-12 no-padding text-uppercase"><spring:theme code="cat.county.mapping.branches.text" /></h2>
                <span class="col-xs-12 no-padding"><spring:theme code="cat.county.mapping.hidden.addresses.text" /></span>
                <div class="btn-group col-xs-12 acc-model-dropdown no-padding" uib-dropdown keyboard-nav="true">
                    <button type="button" class="btn col-xs-12 drop-down-button" uib-dropdown-toggle ng-disabled="disabled">
                        <div class="text m-text">{{cmc.view.selectedHiddenAddress || cmc.view.defaultSelectOption}}</div> 
                    <div class="arrow pull-right">
                        <img class="pull-right" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png" src="/_ui/responsive/cat/images/dropdown-arrow.png">
                    </div>
                    </button>
                    <ul class="dropdown-menu col-xs-12" uib-dropdown-menu role="menu">
                        <li role="menuitem" ng-click="cmc.onDDChange()">
                            <a>{{cmc.view.defaultSelectOption}}</a>
                        </li>
                        <li role="menuitem" ng-repeat="hiddenAddress in cmc.view.hiddenShippingAddress track by $index" ng-click="cmc.onDDChange($index)">
                            <a>
                                <p>{{hiddenAddress.shipToCode}}, {{hiddenAddress.line1}}, </p>
                                <p>{{hiddenAddress.town}}, {{hiddenAddress.region.name}}, {{hiddenAddress.country.name}},</p>
                                <p>{{hiddenAddress.postalCode}}</p>
                            </a>
                        </li>
                    </ul>
                </div>
                <button class="btn-primary pull-right text-center" ng-disabled="!cmc.view.selectedHiddenAddress" ng-click="cmc.unHideShipTo()"><spring:theme code="cat.county.mapping.unhide.text" /></button>
            </div>
            <div class="col-xs-12 county-wrap">
                <div class="row text-uppercase linked-branches">
                    <h2 class="col-xs-12"><spring:theme code="cat.county.mapping.linkedBranches" /></h2>
                </div>
                <div class="row address-wrap" ng-repeat="(idx, shipTo) in cmc.view.shippingAddressList track by $index">
                    <div class="col-xs-12 address" ng-click="cmc.getCountiesofShipTo(shipTo.id);">
                        <div class="row">
                            <a class="col-xs-12 pull-left">
                                <h3 class="col-xs-9 address-title"><spring:theme code="cat.county.mapping.Address" /> {{idx+1}}</h3>
                                <img src="/_ui/responsive/cat/images/plus-icon.png" class="col-xs-3 pull-right increment" ng-if="cmc.view.selectedShipTo !== shipTo.id"> </img>
                                <img src="/_ui/responsive/cat/images/rectangle-2.svg" class="col-xs-3 pull-right decrement" ng-if="cmc.view.selectedShipTo === shipTo.id"> </img>
                                <div class="col-xs-12 address-details">
                                    <div>{{shipTo.shipToCode}}, {{shipTo.line1}}, {{shipTo.town}},</div>
                                    <div>{{shipTo.region.name}}, {{shipTo.country.name}},</div>
                                    <div>{{shipTo.postalCode}}</div>
                                </div>
                                <h3 class="col-xs-12 text-right hide-text" ng-click="cmc.hideShipTo($event, shipTo.id)"><spring:theme code="cat.county.mapping.hide.text" /></h3>
                            </a>
                        </div>
                    </div>
                    <div ng-if="cmc.view.selectedShipTo == shipTo.id">
                        <div class="address-related-counties col-xs-12">
                            <div class="row county-details" ng-repeat="(countyIndex, county) in cmc.view.countiesList | limitTo: cmc.view.pagination.pageSize*(cmc.view.pagination.currentPage) : 0" ng-if="cmc.view.countiesList && cmc.view.countiesList.length">
                                <h3 class="col-xs-9 light-text"><spring:theme code="cat.county.mapping.countyName" /></h3>
                                <div class="col-xs-3 text-right delink-county pull-right" ng-click="cmc.delinkCounty(county.id)"><img src="/_ui/responsive/cat/images/fill-47-copy-6.svg"/></div>
                                <div class="col-xs-12 mapping-label">{{county.countyName}}</div>
                                <div class="col-xs-12 light-text mapping-title"><spring:theme code="cat.county.link.stateCountry" /></div>
                                <div class="col-xs-12 mapping-label">{{county.sellState.name}}, {{county.sellCountry.name}}</div>
                                <div class="col-xs-12 light-text mapping-title"><spring:theme code="cat.county.mapping.modifiedDate" /></div>
                                <div class="col-xs-12 mapping-label">{{county.lastModifiedTime}}</div>
                            </div>
                            <div class="row county-details no-counties" ng-if="!cmc.view.countiesList || !cmc.view.countiesList.length">
                                <spring:theme code="cat.county.mapping.noCountyErrorMessage" />
                            </div>
                        </div>
                        <div class="col-xs-12 show-more-btn no-padding" ng-if="cmc.view.pagination.totalNumberOfResults > cmc.view.pagination.pageSize*(cmc.view.pagination.currentPage)">
                            <p href="" class="btn-primary text-center" ng-click="cmc.showMore()"><spring:theme code="cat.county.mapping.show.more.counties" /></p>
                        </div>
                        <div class="col-xs-12 add-county-info">
                            <p><spring:theme code="cat.county.mapping.message1" /><spring:theme code="cat.county.mapping.message2" /></p>
                        </div>
                        <div class="col-xs-12 add-county-btn">
                            <a href="" class="btn-primary pull-right text-center" ng-disabled="!cmc.view.enableAddCounty || !cmc.view.shippingAddressList.length" ng-click="cmc.addCounty()"><spring:theme code="cat.county.mapping.addCounty" /></a>
                        </div>
                    </div>
                </div>
                <div class="row address-wrap address-wrap-no-shiptos" ng-if="!cmc.view.shippingAddressList.length">
                    <div class="no-un-hidden-shiptos">
                        <span><spring:theme code="cat.county.mapping.addresses.notvisible.message1" /></span>
                        <span><spring:theme code="cat.county.mapping.addresses.notvisible.message2" /></span>
                    </div>
                    <div class="col-xs-12 add-county-info">
                        <p><spring:theme code="cat.county.mapping.message1" /><spring:theme code="cat.county.mapping.message2" /></p>
                    </div>
                    <div class="col-xs-12 add-county-btn">
                        <a href="" class="btn-primary pull-right text-center" ng-disabled="!cmc.view.enableAddCounty || !cmc.view.shippingAddressList.length" ng-click="cmc.addCounty()"><spring:theme code="cat.county.mapping.addCounty" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</template:page>