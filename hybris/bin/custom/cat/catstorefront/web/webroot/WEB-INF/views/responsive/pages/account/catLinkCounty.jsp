<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="quote" tagdir="/WEB-INF/tags/responsive/quote" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>


<template:page pageTitle="${pageTitle}">
	<div id="countyListContainer" class="cat-main-content" >
		<div class="cat-container" ng-app="cat-app" ng-controller="countyListController as clc" ng-init="clc.setcounty()" ng-cloak>
		    <div class="row">
		        <div class="col-xs-12 col-md-12 countyHeader"><spring:theme code="cat.county.link.heading.text" /></div>
		    </div>
		    <div class="row main-content">
		        <div class="col-xs-12 col-md-3 branch">
		            <div class="row">
		                <div class="col-md-12 branchName"><spring:theme code="cat.county.link.branch" /></div>
		                <div class="col-md-12 branchDesc">
		                	<div>{{clc.address.shipToCode}}, {{clc.address.line1}}, {{clc.address.town}},</div>
                            <div>{{clc.address.region.name}}, {{clc.address.country.name}},</div>
                            <div>{{clc.address.postalCode}}</div>
		                </div>
		            </div>
		        </div>
		        <div class="col-xs-12 col-md-9 county-list-table">
		            <table>
		                <tr>
		                    <th class="col-xs-6 col-md-6 countyName">
		                        <span><spring:theme code="cat.county.link.countyName" /></span>     
		                    </th>
		                    <th class="col-xs-6 col-md-6">
		                        <spring:theme code="cat.county.link.stateCountry" />
		                    </th>
		                </tr>
		                <tr ng-repeat="county in clc.countyList">
		                    <td class="countyValue">
		                            <div class="col-xs-12 col-md-8 no-padding">
		                                <div class="col-md-12">
		                                    <div class="county-List-checkbox-section no-padding">
		                                        <label class="county-List-checkbox"><span class="countyDetail">{{county.countyName}}</span>
		                                        <input type="checkbox" ng-click="clc.updateList();" ng-model="county.selected">
		                                        <span class="checkmark"></span>
		                                        </label>
		                                    </div> 
		                                </div>
		                            </div>
		                    </td>
		                    <td class="stateValue">{{county.sellState.name}}, {{county.sellCountry.name}}</td>
						</tr>
		            </table>
				</div>
		    </div>
		    <div class="row">
		        <div class="col-xs-12 col-md-12 link-button">
		            <span class="btn cat-buttons primary-btn pull-right col-xs-12 col-md-2" ng-disabled="!clc.updatedCountyList.length" ng-click="clc.sendInfo()"><spring:theme code="cat.county.link.linkButton" /></span>
		        </div>
		    </div>
		</div>
</div>
</template:page>