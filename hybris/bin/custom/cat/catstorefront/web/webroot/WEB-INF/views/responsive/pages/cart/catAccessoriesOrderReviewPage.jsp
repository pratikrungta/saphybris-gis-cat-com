<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>


<template:page pageTitle="${pageTitle}">

 <div class="cat-main-content">
    <div ng-app="cat-app" ng-controller="reviewAccessoriesController as actrl" ng-cloak >
		<review-order-pop-up id="orderErrorMessage"
		alert-header="VALIDAtiON ERRoR" 
		order-number="{{actrl.view.response.orderNumber}}"
		error-info="This PO number already exists. Please enter a new one and then resubmit order."
		error-pos="actrl.view.errorPos"
		type="error"
		product-type="accessories"
		></review-order-pop-up>

        <div class="cat-container review-accessoriesWrapper hidden-xs">
            <h2><spring:theme code="utv.accessories.review.order" /></h2>
            <div class="accContentWrap">
                <div class="row main-partHeadings no-margin">
                    <div class="pull-left">
                        <h3><spring:theme code="utv.accessories.text" /></h3>
                    </div>
                    <div class="pull-right edit-order">
                        <h3 ng-click="actrl.navigateToAccessories()"><spring:theme code="utv.accessories.edit" /></h3>
                    </div>
                </div>
                <div class="row partHeadings no-margin">
                    <div class="col-md-4 no-padding">
                        <h3><spring:theme code="utv.accessories.part.description" /></h3>
                    </div>
                    <div class="col-md-2">
                        <h3><spring:theme code="utv.accessories.part.number" /></h3>
                    </div>
                    <div class="col-md-1 text-right no-padding">
                        <h3><spring:theme code="utv.accessories.list.price" /></h3>
                    </div>
                    <div class="col-md-3 text-center">
                        <h3><spring:theme code="utv.accessories.quantity" /></h3>    
                    </div>
                    <div class="col-md-2 text-right">
                        <h3><spring:theme code="utv.accessories.total.price" /></h3>    
                    </div>
                </div>

                <!-- Loop here for each category -->
                <div class="accTypeWrap" ng-repeat="(key, val) in actrl.view.accessoriesData"> 
                    <div class="row acc-tab-head accRecommend">
                        <div class="col-md-6 text-left no-padding">
                            <h4>{{key}}</h4>
                        </div>
                    </div>
                    <div class="line-item" ng-repeat="(idx, obj) in val track by $index">
                        <div class="row acc-tab-details">
                            <div class="col-md-4">
                                <h4 class="accName no-padding col-md-12">{{obj.product.name}}</h4>
                            </div>
                            <div class="col-md-2">{{obj.product.code}}</div>
                            <div class="col-md-1 text-right no-padding">{{obj.basePrice.formattedValue}}</div>
                            <div class="col-md-3 text-center accQuantControls">
                                    <div class="input-params">
                                        <p type="text" size=1>{{obj.quantity}}</p> 
                                    </div>
                            </div>
                            <div class="col-md-2 text-right totPrice">{{obj.totalPrice.formattedValue}} <img ng-click="actrl.removeLineItem(obj)" src="/_ui/responsive/cat/images/fill-47-copy-6.svg"/></div>
                        </div>
                    </div>
                    <div class="po-details">
						<div class="row orderContentTabHeader">
							<span class="col-xs-12 col-md-3 errorLabel no-pad-left"><spring:theme code="cat.purchase.order.numbers" /><sup> * </sup></span>
						</div>
						<div class="row po-info-message">
							<div class="col-xs-12 no-pad-left">
								<span class="purchase-text">
									<spring:theme code="cat.enter.ponumber.message" />
								</span>
								<span class="error" ng-if="!val.isValid"> <spring:theme code="cat.ponumber.error.message" /> </span>
								<span class="error" ng-if="(actrl.duplicatePONumbers.indexOf(val.poNumber) != -1)"> <spring:theme code="cat.ponumber.unique.message" /> </span>
								<span class="error" ng-if="actrl.view.isUserCheckedCertify && val.poNumber == ''"> <spring:theme code="cat.ponumber.empty.message" /></span>
							</div>
						</div>
                        <input cat-capitalize type="text" class="cat-inputs type1" ng-model="val.poNumber" ng-trim="false" placeholder="Enter PO #"
                        restrict-model-input="^[A-Za-z0-9]{0,12}$" ng-blur="actrl.onPoEnter(key)"
                        not-allow-to="[]" maxlength='12' ng-class="{'error': ((actrl.duplicatePONumbers.indexOf(val.poNumber) != -1) || actrl.view.isUserCheckedCertify && val.poNumber == '' || !val.isValid)}" />
                    </div>
                </div>
                <div class="total-section row no-margin">
                	<div class="col-md-offset-7 total-quantity-section">
                		<div class="col-md-6 text-right total-label">
	                        <spring:theme code="utv.accessories.total.accessories" />
	                    </div>
	                    <div class="col-md-6 text-right total-count">
	                        <span>{{actrl.view.totalQuantity}}</span>
	                    </div>	
                	</div>
                	<div class="col-md-offset-7">
                		<div class="col-md-6 text-right total-label">
	                        <spring:theme code="utv.accessories.total.price" />
	                    </div>
	                    <div class="col-md-6 text-right total-count">
	                        <span>{{actrl.view.totalPrice}}</span>
	                    </div>	
                	</div>
                </div>
            </div>
            <div class="row sectionWrap delearsDetailsHold">
				<div class="col-xs-12 headerHold">
					<spring:theme code="im.reorder.dealer.details" />
				</div>
				<div class="col-xs-12 formelementhold">
					<div class="row dealer-name">
						<div class="col-xs-12 col-md-2 no-padding">
							<p class="lable">
								<spring:theme code="im.reorder.dealer.name" />
							</p>
						</div>
						<div class="col-xs-12 col-md-9 dealer-section">
							<div class="form-group clearfix">
								<div class="col-md-4 no-padding">
									<span class="no-border">${dealerName}</span>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-2 no-padding">
							<p class="lable">
								<spring:theme code="im.reorder.shipping.address" />
							</p>
						</div>
						<div class="col-xs-12 col-md-9 dealer-section">
							<div class="form-group clearfix">
								<div class="col-md-6 no-padding" ng-if="!actrl.isMobile">
									<cat-address ship-address-id="reviewShippingAddressId" update-selected-address="updateDropDownValue(addrssObj)"></cat-address>
								</div>

							</div>
						</div>
						
					</div>
				</div>
			</div>
            <div class="row" id="submitOrderHold">
					<div class="indicateMsg col-md-12"><spring:theme code="cat.indicates.required.fileds.message" /></div>
					<div class="form-group clearfix">
						<div class="col-xs-12 col-md-8 checkBoxSection">
							<div class="row">
							<input type="checkbox" name="shippingCheckbox" id="certify"
								value="true" ng-model="actrl.view.userCertified" ng-click="actrl.triggerValidations()">
								<label
								for="certify" ng-class="!!actrl.view.formSubmited && !actrl.view.userCertified ? 'highlighted-box': ''">
									<span aria-hidden="true" ng-class="actrl.view.userCertified?'tickFont':''"></span>
								</label>
								<label class="checkboxlabel"><spring:theme code="cat.necessary.approval.message" /></label>
							</div>
							<div class="row disclimer-error-message" ng-show="!!actrl.view.formSubmited && !actrl.view.userCertified">
								<spring:theme code="cat.select.submit.order.checkbox" />
							</div>
						</div>
						<div class="col-xs-12 col-md-4" id="submitorderbuttonHold">
							<input type="button" id="submit" value="<spring:theme code='im.order.submit.button'/>" ng-class="(!actrl.view.userCertified || !!actrl.view.processingRequest || actrl.duplicatePONumbers.length || actrl.isPoNumberEmpty() || !actrl.productCompareDropDownLabel.id) ?'disabled':''" ng-disabled="(!actrl.view.userCertified || !!actrl.view.processingRequest || actrl.duplicatePONumbers.length || actrl.isPoNumberEmpty() || !actrl.productCompareDropDownLabel.id)" ng-click="actrl.submitOrder()" />
						</div>
					</div>
			</div>
        </div>
        <!-- Accessories Mobile markup -->

        <div class="cat-container review-accessoriesWrapper-mob visible-xs no-margin">
            <div class="row">
                <h2 class="col-xs-8"><spring:theme code="utv.accessories.review.order" /></h2>    
            </div>
            <div class="mobile-container-wrapper">
            	<div class="row main-partHeadings no-margin">
                    <div class="pull-left">
                        <h3><spring:theme code="utv.accessories.text" /></h3>
                    </div>
                    <div class="pull-right edit-order">
                    	<img ng-click="actrl.navigateToAccessories()" src="/_ui/responsive/cat/images/edit.png" />
                    </div>
                </div>
	            <div class="accContentWrapMob" ng-repeat="(key, val) in actrl.view.accessoriesData">
	                <div class="row acc-tab-head accRecommend">
	                    <div class="col-xs-8 text-left no-padding">
	                        <h4>{{key}}</h4>
	                    </div>
	                </div>
	                <div class="row acc-tab-details" ng-repeat="obj in val track by $index">
	                    <div class="col-xs-12 partDesc">
	                        <h3 class="accName no-padding col-xs-12">{{obj.product.name}} <img class="pull-right" ng-click="actrl.removeLineItem(obj)" src="/_ui/responsive/cat/images/fill-47-copy-6.svg"/></h3>
	                    </div>
	                    <div class="col-xs-12 partConfig">
	                        <div class="row">
	                            <div class="col-xs-6">
	                                <div class="light-head"><spring:theme code="utv.accessories.part.number" /></div>
	                                <div>{{obj.product.code}}</div>
	                            </div>
	                            <div class="col-xs-12 list-price">
	                                <div class="light-head d-inline-block"><spring:theme code="utv.accessories.list.price" /></div>
	                                <div class="pull-right">{{obj.basePrice.formattedValue}}</div>
	                            </div>
	                            <div class="col-xs-12 accQuantControls">
	                                <div class="light-head d-inline-block"><spring:theme code="utv.accessories.quantity" /></div>
	                                <div class="input-params pull-right">
                                        <p type="text" size=1 class="no-margin">{{obj.quantity}}</p> 
                                    </div>
	                            </div>
	                            <div class="col-xs-12 total-list-price">
	                                <div class="light-head d-inline-block"><spring:theme code="utv.accessories.total.price" /></div>
	                                <div class="pull-right">{{obj.totalPrice.formattedValue}}</div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	                <div class="po-details">
						<div class="row orderContentTabHeader">
							<span class="col-xs-12 errorLabel no-pad-left"><spring:theme code="cat.purchase.order.numbers" /><sup> * </sup></span>
						</div>
						<div class="row po-info-message">
							<div class="col-xs-12 no-pad-left">
								<span class="purchase-text">
									<spring:theme code="cat.enter.ponumber.message" />
								</span>
								<span class="error" ng-if="!val.isValid"> <spring:theme code="cat.ponumber.error.message" /> </span>
								<span class="error" ng-if="(actrl.duplicatePONumbers.indexOf(val.poNumber) != -1)"> <spring:theme code="cat.ponumber.unique.message" /> </span>
								<span class="error" ng-if="actrl.view.isUserCheckedCertify  && val.poNumber == ''"> <spring:theme code="cat.ponumber.empty.message" /></span>
							</div>
						</div>
                        <input cat-capitalize type="text" class="cat-inputs type1" ng-model="val.poNumber" ng-trim="false" placeholder="Enter PO #"
                        restrict-model-input="^[A-Za-z0-9]{0,12}$" ng-blur="actrl.onPoEnter(key)"
                        not-allow-to="[]" maxlength='12' ng-class="{'error': ((actrl.duplicatePONumbers.indexOf(val.poNumber) != -1) || (actrl.view.isUserCheckedCertify && val.poNumber == '') || !val.isValid)}" />
                    </div>
	            </div>
                <div class="total-section row">
                	<div class="row total-quantity-section">
                		<div class="col-xs-6 text-left total-label">
	                        <spring:theme code="utv.accessories.total.accessories" />
	                    </div>
	                    <div class="col-xs-6 text-right total-count">
	                        <span>{{actrl.view.totalQuantity}}</span>
	                    </div>	
                	</div>
                	<div class="row total-price-section">
                		<div class="col-xs-6 text-left total-label">
	                        <spring:theme code="utv.accessories.total.price" />
	                    </div>
	                    <div class="col-xs-6 text-right total-count">
	                        <span>{{actrl.view.totalPrice}}</span>
	                    </div>	
                	</div>
                </div>
            </div>
            <div class="mobile-container-wrapper dealer-wrapper">
            	<div class="row sectionWrap delearsDetailsHold">
					<div class="col-xs-12 headerHold">
						<spring:theme code="im.reorder.dealer.details" />
					</div>
					<div class="col-xs-12 formelementhold">
						<div class="row dealer-name">
							<div class="col-xs-12 no-padding">
								<p class="lable">
									<spring:theme code="im.reorder.dealer.name" />
								</p>
							</div>
							<div class="col-xs-12 no-padding dealer-section">
								<div class="form-group clearfix">
									<div class="no-padding">
										<span class="no-border">${dealerName}</span>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 no-padding">
								<p class="lable">
									<spring:theme code="im.reorder.shipping.address" />
								</p>
							</div>
							<div class="col-xs-12 no-padding dealer-section">
								<div class="form-group clearfix">
									<div class="no-padding" ng-if="actrl.isMobile">
											<cat-address ship-address-id="reviewShippingAddressId"  update-selected-address="updateDropDownValue(addrssObj)"></cat-address>
									</div>

								</div>
							</div>
							
						</div>
					</div>
				</div>
            </div>
            <div class="row" id="submitOrderHold">
					<div class="indicateMsg col-xs-12"><spring:theme code="cat.indicates.required.fileds.message" /></div>
					<div class="form-group clearfix">
						<div class="col-xs-12 checkBoxSection">
							<div class="row no-margin">
								<input type="checkbox" name="shippingCheckbox" id="certify"
								value="true" ng-model="actrl.view.userCertified" />
								<label for="certify" ng-class="!!actrl.view.formSubmited && !actrl.view.userCertified ? 'highlighted-box': ''">
									<span aria-hidden="true" ng-class="actrl.view.userCertified?'tickFont':''"></span>
								</label>
								<label class="checkboxlabel"><spring:theme code="cat.necessary.approval.message" /></label>
							</div>
							<div class="row disclimer-error-message" ng-show="!!actrl.view.formSubmited && !actrl.view.userCertified">
								<spring:theme code="cat.select.submit.order.checkbox" />
							</div>
						</div>
						<div class="col-xs-12" id="submitorderbuttonHold">
							<input type="button" id="submit" value="<spring:theme code='im.order.submit.button'/>" ng-class="(!actrl.disableSubmit && actrl.view.userCertified || !!actrl.view.processingRequest || actrl.duplicatePONumbers.length || actrl.isPoNumberEmpty() || !actrl.productCompareDropDownLabel.id)?'disabled':''" ng-disabled="(!actrl.view.userCertified || !!actrl.view.processingRequest || actrl.duplicatePONumbers.length || actrl.isPoNumberEmpty() || !actrl.productCompareDropDownLabel.id)" ng-click="actrl.submitOrder()" />
						</div>
					</div>
			</div>
        </div>
    </div>
</div>
 
</template:page>