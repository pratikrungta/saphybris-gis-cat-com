<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="speckcheckProductCompare" tagdir="/WEB-INF/tags/responsive/speckcheckProductCompare" %>
<%@ taglib prefix="quotationCartPage" tagdir="/WEB-INF/tags/responsive/quotationCartPage" %>
<template:page pageTitle="${pageTitle}">
    <div id="cart-page"  class="cat-row cat-main-content" ng-app="cat-app">
        <quotationCartPage:quotationCartPageComponent/>
    </div>
</template:page>