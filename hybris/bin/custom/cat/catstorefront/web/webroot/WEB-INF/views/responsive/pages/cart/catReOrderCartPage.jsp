<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="quotationCartPage" tagdir="/WEB-INF/tags/responsive/quotationCartPage" %>
<%@ taglib prefix="reorder" tagdir="/WEB-INF/tags/responsive/reorder"%>
<template:page pageTitle="${pageTitle}">
    <div class="row cat-main-content">
        <div id="reorder-cart-container" ng-app="cat-app" ng-cloak>
            <reorder:reorderCartPage/>
        </div>
    </div>
</template:page>