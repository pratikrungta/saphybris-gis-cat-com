<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<template:page pageTitle="${pageTitle}">
	<c:set var="category" value="" />
	<c:forEach var="orderEntry" items="${orderData.entries}">
		<c:forEach var="productCategories"
			items="${orderEntry.product.categories}">
			<c:set var="category" value="${productCategories.code}" />
		</c:forEach>
	</c:forEach>
	<div class="row">
		<div class="titleHeader cat-container"><spring:theme code="im.order.details" /></div> 
		<div class="row cat-container Home hidden-xs">
			<div class="col-xs-12">
				<div class="pull-right">
					<span class="homeIcon"><a href="/"><img
							src="/_ui/responsive/cat/images/fill-300.png"></a></span> <span
						class="backToHomebtn"><a href="/"><spring:theme
								code="back.to.home" /></a></span>
				</div>
			</div>
		</div>
	</div>
	<div class="row order-history-box">
		<div id="ord-container" class="cat-container row" ng-app="cat-app" ng-controller="orderDetailsController as odc" ng-init="odc.roleFeatureInfoOrderDetails('${isUTVUser}','${isIM}');">
			<div class="orderinfo">
				<div class="orderTabHeader">
					<span class="orderTabHeaderText"><spring:theme code="im.order.summary" /></span>
				</div>

				<div class="row no-gutters top-nav">
					<img class="col-xs-3 col-sm-3 orderedPdtImage" src="${imageUrl}"></img>
					<div class="col-xs-offset-1 col-xs-7 visible-xs">
						<div class="col-xs-12 visible-xs prodcategory heading-text camelCase">
							<c:forEach var="orderEntry" items="${orderData.entries}">
								<c:forEach var="productCategories" items="${orderEntry.product.categories}">
									${fn:toLowerCase(productCategories.name)}
								</c:forEach>
							</c:forEach>
						</div>
						<div class="row">
							<div class="col-xs-12 hidden-xs heading-text camelCase">
								<c:forEach var="orderEntry" items="${orderData.entries}">
									<c:forEach var="productCategories" items="${orderEntry.product.categories}">
										${fn:toLowerCase(productCategories.name)}
									</c:forEach>
								</c:forEach>
							</div>
								</div>
								<div class="row">
									<div class="col-xs-12 familyName">
										<c:forEach var="orderEntry" items="${orderData.entries}">
											<c:if test="${category eq 'utv' }">
											<div class="col-xs-12 familyName no-padding-desktop">
												<div class="heading-oDetail">Sales Model</div>
												<div class="value-oDetail">${orderEntry.product.salesModelId}</div>
											</div>
											<div class="col-xs-12 familyName no-padding-desktop">
												<div class="heading-oDetail">Configuration ID</div>
												<div class="value-oDetail">${baseProduct.name}</div>
											</div>
											</c:if>
											<c:if test="${category ne 'utv' }">
												<div class="col-xs-12 familyName no-padding-desktop">
													<div class="heading-oDetail">Sales Model</div>
													<div class="value-oDetail">${orderEntry.product.salesModelId}</div>
												</div>
												<div class="col-xs-12 familyName no-padding-desktop">
													<div class="heading-oDetail">Configuration ID</div>
													<div class="value-oDetail">${orderEntry.product.name}</div>
												</div>
					 						</c:if>
										</c:forEach>
									</div>
								</div>
					</div>
					<div class="col-xs-12 visible-xs">
							<div class=" lane-info-tag-order-history text-center cat-buttons secondary-btn">${orderData.laneTypeData.name}
									<c:if test="${orderData.laneTypeData.code eq 'LANE1' }">
											<span class="dot LANE1-dot"></span>
									</c:if>
									<c:if test="${orderData.laneTypeData.code eq 'LANE2' }">
											<span class="dot LANE2-dot"></span>
									</c:if>
									<c:if test="${orderData.laneTypeData.code eq 'LANE3' }">
											<span class="dot LANE3-dot"></span>
									</c:if>
							</div>
					</div>
					<div class="col-xs-12 col-sm-9">

						<div class="row orderDetailsMSOTab">
							<div class="row">
								<div class="col-xs-6 col-sm-2 heading-text">
									<div class="row">
										<p class="lable hidden-xs col-xs-6 col-sm-12">
											<spring:theme code="im.order.purchaseOrderNumber" />
										</p>
										<p class="lable visible-xs col-xs-6 col-sm-12 serialNumber">
											<spring:theme code="im.order.serial" />
										</p>
										<div class="visible-xs col-xs-6 desc-text">${orderData.serialNumber}</div>
									</div>
								</div>
								<div class="col-xs-6 col-sm-2 heading-text">
									<div class="row">
										<p class="lable col-xs-4 col-sm-12">
											<spring:theme code="im.order.mso" />
										</p>
										<div class="visible-xs col-xs-6 desc-text MSOStatus">
											<c:if test="${not empty orderData.mso}">
												${orderData.mso}
											</c:if>
											<c:if test="${empty orderData.mso and orderData.status ne 'FAILED_SUBMISSION'}">
												<c:choose>
													<c:when test="${orderData.status eq 'SUBMITTED'}">
														<spring:theme code="im.order.orderMSODefault" />
													</c:when>
													<c:otherwise>
														<spring:theme code="text.intuitive.account.orderHistory.mso" />
													</c:otherwise>
												</c:choose>
											</c:if>
										</div>
									</div>
								</div>
								
								<div class="col-xs-6 col-sm-2 heading-text">
									<div class="row">
										<p class="lable col-xs-6 col-sm-12">
											<spring:theme code="im.order.quantity" />
										</p>
										<div class="visible-xs col-xs-6 desc-text">
											<spring:theme code="im.order.configured.quantity" />
											<spring:theme code="im.order.unit" />
										</div>
									</div>		
								</div>
								<div class="col-xs-6 col-sm-2 heading-text">
									<div class="row">
										<p class="lable hidden-xs col-xs-6 col-sm-12">
											<spring:theme code="im.order.serial" />
										</p>
										<p class="lable visible-xs col-xs-4 col-sm-12 PONumber">
											<spring:theme code="im.order.purchaseOrderNumber" />
										</p>
										<div class="visible-xs col-xs-6 desc-text">${orderData.purchaseOrderNumber}</div>
									</div>
								</div>
                                <div class="col-xs-12 col-sm-3 heading-text">
									<div class="row">
										<p class="hidden-xs lable col-xs-4 col-sm-12">
											<spring:theme code="im.order.orderStatus" />
										</p>
										<!-- <div class="visible-xs col-xs-6 desc-text">${orderData.status}</div>-->
									</div>
								</div>


							</div>
							<div class="row hidden-xs">
								<div class="col-xs-2 desc-text">${orderData.purchaseOrderNumber}</div>
								<div class="col-xs-2 desc-text">
								<c:if test="${not empty orderData.mso}">
									${orderData.mso}
								</c:if>
								<c:if test="${empty orderData.mso and orderData.status ne 'FAILED_SUBMISSION'}">
									<c:choose>
										<c:when test="${orderData.status eq 'SUBMITTED'}">
											<spring:theme code="im.order.orderMSODefault" />
										</c:when>
										<c:otherwise>
											<spring:theme code="text.intuitive.account.orderHistory.mso" />
										</c:otherwise>
									</c:choose>
								</c:if>
								</div>
								<div class="col-xs-2 desc-text">
									<spring:theme code="im.order.configured.quantity" />
									<spring:theme code="im.order.unit" />
								</div>
								<div class="col-xs-2 desc-text">${orderData.serialNumber}</div>
                                <div class="col-xs-2 desc-text">
                                        <spring:theme code="${orderData.status}" />
                                </div>
							</div>
						</div>
						<div class="row orderDetailsStatusTab no-gutters">
							<div class="row">

								<div class="col-xs-12 visible-xs col-sm-3 heading-text">
									<div class="row">
										<p class="visible-xs lable col-xs-4 col-sm-12">
											<spring:theme code="im.order.orderStatus" />
										</p>
										<div class="visible-xs col-xs-7 desc-text"><spring:theme code="${orderData.status}" /></div>
									</div>
								</div>
								
								<div class="col-xs-12 col-sm-3 heading-text">
									<div class="row">
										<p class="lable col-xs-4 col-sm-12">
											<spring:theme code="im.order.orderDate" />
										</p>
										<div class="visible-xs col-xs-6 desc-text">
											<fmt:formatDate value="${orderData.created}"
											pattern="MM-dd-yyyy" />
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-3 heading-text">
									<div class="row">
										<p class="lable col-xs-4 col-sm-12">
											<spring:theme code="im.order.inTransitStatus" />
										</p>
										<div class="visible-xs col-xs-6 desc-text">
											<c:choose>
													<c:when test="${empty orderData.inTransitStatus}">
														<spring:theme code="im.order.inTransitStatusDefault" />
													</c:when>
													<c:otherwise>
												${orderData.inTransitStatus}
												</c:otherwise>
											</c:choose>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-3 heading-text">
									<div class="row">
										<p class="lable hidden-xs col-xs-4 col-sm-12">
											<spring:theme code="im.order.orderEstimatedDate" />
										</p>
										<p class="lable visible-xs col-xs-4 col-sm-12">
											<spring:theme code="im.order.mobile.orderEstimatedDate" />
										</p>
										<div class="visible-xs col-xs-6 desc-text">
											<c:choose>
												<c:when test="${empty orderData.estimatedDelivery}">
													<spring:theme code="im.order.estimatedDeliveryDateDefault" />
												</c:when>
												<c:otherwise>
													<fmt:formatDate value="${orderData.estimatedDelivery}"
														pattern="MM-dd-yyyy" />
												</c:otherwise>
											</c:choose>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-3 heading-text">
									<div class="row">
										<p class="lable col-xs-4 col-sm-12">
											<spring:theme code="im.order.orderPromiseDate" />
										</p>
										<div class="visible-xs col-xs-6 desc-text">
											<c:choose>
												<c:when test="${empty orderData.orderPromiseDate}">
													<spring:theme code="im.order.orderPromiseDateDefault" />
												</c:when>
												<c:otherwise>
													<fmt:formatDate value="${orderData.orderPromiseDate}"
														pattern="MM-dd-yyyy" />
												</c:otherwise>
											</c:choose>
										</div>
									</div>
								</div>
							</div>
							<!-- -->

							<div class="row hidden-xs">
								<div class="col-xs-12 col-sm-3 desc-text">
									<fmt:formatDate value="${orderData.created}"
										pattern="MM-dd-yyyy" />
								</div>
								<div class="col-xs-12 col-sm-3 desc-text">
									<c:choose>
										<c:when test="${empty orderData.inTransitStatus}">
											<spring:theme code="im.order.inTransitStatusDefault" />
										</c:when>
										<c:otherwise>
                                    ${orderData.inTransitStatus}
                                </c:otherwise>
									</c:choose>
								</div>
								<div class="col-xs-12 col-sm-3 desc-text">
									<c:choose>
										<c:when test="${empty orderData.estimatedDelivery}">
											<spring:theme code="im.order.estimatedDeliveryDateDefault" />
										</c:when>
										<c:otherwise>
											<fmt:formatDate value="${orderData.estimatedDelivery}"
												pattern="MM-dd-yyyy" />
										</c:otherwise>
									</c:choose>
								</div>
								<div class="col-xs-2 desc-text">
									<c:choose>
										<c:when test="${empty orderData.orderPromiseDate}">
											<spring:theme code="im.order.orderPromiseDateDefault" />
										</c:when>
										<c:otherwise>
											<fmt:formatDate value="${orderData.orderPromiseDate}"
												pattern="MM-dd-yyyy" />
										</c:otherwise>
									</c:choose>
								</div>
							</div>


							<div class="row"></div>

						</div>
						<!-- -->
					</div>
				</div>
				<div class="row">
					<div class="productThumbnail">
						<div class="col-sm-6 hidden-xs">
							<div class="row">
								<div class="col-xs-12 heading-text camelCase no-padding-desktop">
									<c:forEach var="orderEntry" items="${orderData.entries}">
										<c:set var="productCategories"
											value="${orderEntry.product.categories[0]}"/>
                  ${fn:toLowerCase(productCategories.name)}
									</c:forEach>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 familyName no-padding-desktop">
									<div class="no-padding-desktop col-xs-6">
										<c:forEach var="orderEntry" items="${orderData.entries}">
											<c:if test="${category eq 'utv' }">
												<div class="col-xs-6 familyName no-padding-desktop">
													<div class="heading-oDetail">Sales Model</div>
													<div class="value-oDetail">${orderEntry.product.salesModelId}</div>
												</div>
												<div class="col-xs-6 familyName no-padding-desktop">
													<div class="heading-oDetail">Configuration ID</div>
													<div class="value-oDetail">${baseProduct.name}</div>
												</div>
										</c:if>
											<c:if test="${category ne 'utv' }">
												<div class="col-xs-6 familyName no-padding-desktop">
													<div class="heading-oDetail">Sales Model</div>
													<div class="value-oDetail">${orderEntry.product.salesModelId}</div>
												</div>
												<div class="col-xs-6 familyName no-padding-desktop">
													<div class="heading-oDetail">Configuration ID</div>
													<div class="value-oDetail">${orderEntry.product.name}</div>
												</div>
										</c:if>
										</c:forEach>
									</div>	
									<div class="lane-info-tag-order-history text-center col-xs-6 cat-buttons secondary-btn"> ${orderData.laneTypeData.name} 
											<c:if test="${orderData.laneTypeData.code eq 'LANE1' }">
													<span class="dot LANE1-dot"></span>
											</c:if>
											<c:if test="${orderData.laneTypeData.code eq 'LANE2' }">
													<span class="dot LANE2-dot"></span>
											</c:if>
											<c:if test="${orderData.laneTypeData.code eq 'LANE3' }">
													<span class="dot LANE3-dot"></span>
											</c:if>
									</div>
								</div>
							</div>
						</div>
						<c:if test="${not empty orderData.mso}">
							<div class="col-sm-6 col-xs-12 trackingDetailsComponent"
								ng-app="cat-app">
								<div class="trackingDetailsButton pull-right"
									ng-controller="GsnLinkController as glc"
									ng-init="glc.init('${orderData.mso}')">
									<cms:component uid="orderPortalLinkComponent" />
								</div>
							</div>
						</c:if>
					</div>
				</div>
			</div>
			<!--  Dealer  -->
			<div class="dealerinfo">

				<div class="dealerdetailsHeader">
					<span class="dealerdetailsHeaderText"> <spring:theme
							code="im.order.dealerDetails" />
					</span>
				</div>
				<div class="dealerDetailsContent">
					<div class="row dealer-details-dealerName">
						<div class="col-sm-2 col-xs-12">
							<p class="title">
								<spring:theme code="im.order.dealerName" />
							</p>
						</div>
						<div class="col-sm-2 col-xs-12 dealerLabelText">${orderData.b2bCustomerData.unit.name}</div>
					</div>
					<div class="row dealer-details">
						<div class="col-sm-2 col-xs-12">
							<p class="title">
								<spring:theme code="im.order.dealerShippingAddress" />
							</p>
						</div>
						<div class="col-sm-2 col-xs-12 dealerLabelText">

							<div class="row addressDealer camelCase">
								${fn:toLowerCase(orderData.deliveryAddress.shipToCode)}, ${fn:toLowerCase(orderData.deliveryAddress.line1)}</div>
							<div class="row addressDealer camelCase">
								${fn:toLowerCase(orderData.deliveryAddress.town)}</div>
							<div class="row addressDealer camelCase">
								${fn:toLowerCase(orderData.deliveryAddress.region.name)}</div>
							<div class="row addressDealer camelCase">
								${fn:toUpperCase(orderData.deliveryAddress.country.isocode)},
								${fn:toUpperCase(orderData.deliveryAddress.postalCode)}</div>
						</div>
					</div>
					<%-- <div class="row dealer-details">
                                                <div class="col-xs-2">
                                                    <p class="title">
                                                        <spring:theme code="im.order.dealerBillingAddress" />
                                                    </p>
                                                </div>
                                                <div class="col-xs-2 dealerLabelText">
                                                    <div class="row addressDealer camelCase">
                                                        ${fn:toLowerCase(orderData.paymentInfo.billingAddress.line1)}
                                                    </div>
                                                    <div class="row addressDealer">
                                                        ${orderData.paymentInfo.billingAddress.line1}
                                                    </div>
                                                    <div class="row addressDealer camelCase">
                                                       ${fn:toLowerCase(orderData.paymentInfo.billingAddress.town)}
                                                    </div>
                                                    <div class="row addressDealer camelCase">
                                                        ${fn:toLowerCase(orderData.paymentInfo.billingAddress.region.name)}
                                                    </div>
                                                    <div class="row addressDealer camelCase">
                                                        ${fn:toUpperCase(orderData.paymentInfo.billingAddress.country.isocode)}, ${fn:toUpperCase(orderData.paymentInfo.billingAddress.postalCode)}
                                                    </div>
                                                </div>
                                            </div> --%>
					<c:if test="${category ne 'utv' }">
						<div class="row dealer-details">
							<div class="col-xs-12 col-sm-2">
								<p class="title">
									<spring:theme code="im.order.dealerComments" />
								</p>
							</div>
							<div class="col-xs-12 col-sm-6 dealerLabelText">
								<div>
								<c:forEach var="b2bComment" items="${orderData.b2bCommentData}">
                                                        ${b2bComment.comment}
													</c:forEach>
							</div>
								<div class="pull-right">
									<img src=""></img>
								</div>
							</div>

							<!--   <div class="col-xs-6 dealerLabelText">
       Lorem ipsum dolor sit amet, consectetur adipiscing elit. <img src="img/shape.svg" class="Shape">
    </div> -->
						</div>
					</c:if>
				</div>
			</div>

		</div>
	</div>
	</div>
	</div>
</template:page>
