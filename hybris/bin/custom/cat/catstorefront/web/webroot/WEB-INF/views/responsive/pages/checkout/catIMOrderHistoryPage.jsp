<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/desktop/action" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<template:page pageTitle="${pageTitle}">
    <spring:url value="/my-account/order/" var="orderDetailsUrl" htmlEscape="false"/>
    <div class="cat-main-content">
        <div class="cat-row" ng-app="cat-app" ng-controller="orderHistoryController as ohc" ng-init="ohc.init(); ohc.roleFeatureInfoOrderHistory('${isUTVUser}','${isIM}');" ng-cloak>
            <div class="orderHistoryPage row cat-container">
                <div class="order-cat-container col-md-12">
                    <div class="row no-gutters pagination-container top">
                        <div class="col-xs-6">
                            <div class="search-text-header upperCase"><spring:theme code="text.account.orderHistory" /></div>
                        </div>
                        <div class="col-xs-6 visible-xs">
                            <div class="dropdown sort-dropdown pull-right">
                                <button class="btn" type="button" data-toggle="dropdown-menu" ng-click="ohc.view.sortBtn = !ohc.view.sortBtn" >
                                    <i class="fa fa-arrow-up" aria-hidden="true"></i>
                                    <i class="fa fa-arrow-down" aria-hidden="true"></i>
                                    SORT
                                </button>
                                <ul id="dropdownList" class="dropdown-menu" ng-show="ohc.view.sortBtn">
                                    <li ng-click="ohc.onSortClick(key)" ng-class="{'selected': sortObj.isSelected}" ng-repeat="(key, sortObj) in ohc.view.sortMetaData">
                                        <span>{{sortObj.name}}</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row" ng-if="!ohc.hasOrders">
                        <div class="account-section-content col-xs-12 content-empty cat-container">
                            <ycommerce:testId code="orderHistory_noOrders_label">
                                <spring:theme code="text.account.orderHistory.noOrders" />
                            </ycommerce:testId>
                        </div>
                    </div>
                    <div class="account-section-content " ng-if="ohc.hasOrders">
                        <div class="account-orderhistory">
                            <div class="account-orderhistory-pagination row">
                                <%-- <c:if test="${isUTVUser eq true}"> --%>
                                        
                                <%-- </c:if>  --%>
                                <c:if test="${isIM  eq true}">
                                <div class="col-xs-12">
                                            <div class="trackingInfo"><spring:theme code="order.history.tracking.info"/></div>
                                        </div>
                                    <div class="col-xs-12 mobile-search">
                                    	<div class="poInfo"><spring:theme code="order.history.po.info"/></div>
                                        <div class="col-md-9 productFamilyHeaderText">
                                            <div class="col-md-6 searchHeader no-padding pull-left">
                                            	SEARCH FOR PO# OR MSO
                                            </div>
                                            <div class="hidden-xs col-md-3 productFamilyHeader pull-left">
                                            	FILTER BY PRODUCT FAMILY
                                            </div>
                                            <div class="hidden-xs col-md-3 productLaneHeader">
                                            	FILTER BY LANE
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="col-xs-12 col-md-4 search-div">
                                                    <input class="col-xs-12 search-box" placeholder="Enter MSO# or PO#" cat-enter="ohc.searchClick()" ng-model="ohc.view.pagination.search"/>
                                                </div>
                                                <div class= "col-xs-2 col-md-2 searchIconsBox clearfix">
                                                    <div class="col-xs-12 searchBoxIcon pull-left"><img src="/_ui/responsive/cat/images/fill-169.png" ng-click="ohc.searchClick()"/></div>
                                                    <div class="col-xs-12 searchCancelIcon pull-left"><img src="/_ui/responsive/cat/images/cross.png" ng-click="ohc.clearSearch()"/></div>
                                                </div>
                                                <div class="col-xs-12 col-md-3 productFamilyBox">
                                                    <div class="hidden-sm hidden-md hidden-lg productFamilyHeaderMob">FILTER BY PRODUCT FAMILY</div>
                                                    <span class="col-xs-12 dropDownSelection hide_overflow" ng-click="ohc.dropDownSelect($event)">{{ohc.dropDownSelection | initcaps}}</span>
                                                    <div class="col-xs-12 vehiclesDropDownBox" >
                                                        <img class="pull-right order-history-dropdown-arrow" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"  ng-click="ohc.dropDownSelect($event)"/> 
                                                        <ul class="dropdown-menu dropdown-select-product-family dropDownPdtFamily shipping-address-dropdown-menu dropdown-resize" uib-dropdown-menu aria-labelledby="shipping-address-dropdown">
                                                            <li class=" dropDownPdtFamilyItem shipping-address-dropdown-item" ng-repeat="item in ohc.selectedVehicles" 
                                                            ng-click="ohc.selectVehicles(item);">
                                                                <a href="#">
                                                                    <label class="camelCase">{{item | initcaps}}</label>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-3 productFamilyBox">
                                                    <div class="laneTypeBox">
                                                        <div class="hidden-sm hidden-md hidden-lg productFamilyHeaderMob">FILTER BY LANE</div>
                                                        <span class="col-xs-12 dropDownSelection hide_overflow" ng-click="ohc.dropDownLaneSelect($event)">{{ohc.dropDownLaneSelection.name || ohc.dropDownLaneSelection.default | initcaps}}</span>
                                                        <div class="col-xs-12 vehiclesDropDownBox" >
                                                            <img class="pull-right order-history-dropdown-arrow" ng-src="/_ui/responsive/cat/images/dropdown-arrow.png"  ng-click="ohc.dropDownLaneSelect($event)"/> 
                                                            <ul class="dropdown-menu dropdown-select-lane dropDownPdtFamily shipping-address-dropdown-menu dropdown-resize" uib-dropdown-menu aria-labelledby="shipping-address-dropdown">
                                                                <li class=" dropDownPdtFamilyItem shipping-address-dropdown-item" ng-repeat="item in ohc.selectedLanes" 
                                                                ng-click="ohc.selectLane(item);">
                                                                    <a href="#">
                                                                        <label class="camelCase">{{item.name||item.default}}</label>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-md-3 hidden-xs" ng-if="ohc.view.pagination.numberOfPages > 1">
                                                <ul uib-pagination boundary-links="true" total-items="ohc.view.pagination.totalNumberOfResults" ng-model="ohc.view.pagination.currentPage" items-per-page="ohc.view.pagination.pageSize" ng-change="ohc.pageChanged()" class="pagination-sm pull-right" previous-text="&laquo;" next-text="&raquo;" first-text="" last-text="" max-size="3">
                                                </ul>
                                                <!-- <nav:catIMOrderHistoryPagination top="true" msgKey="text.account.orderHistory.page" showCurrentPageInfo="true" hideRefineButton="true" supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}" searchUrl="${searchUrl}"  numberPagesShown="${numberPagesShown}"/> -->
                                            </div>
                                        </div>    
                                    </div>
                                </c:if>
                            </div>
                            <div class="account-overview-table col-xs-12 content-empty" ng-if="ohc.orderHistory.orders.length === 0">
                                <div class="content">
                                    <ycommerce:testId code="orderHistory_noOrders_label">
                                        <spring:theme code="text.account.orderHistory.noResults" />
                                    </ycommerce:testId>
                                </div>
                            </div>
                            <div class="account-overview-table" ng-if="ohc.orderHistory.orders.length !== 0">
                                <table class="orderhistory-list-table responsive-table  hidden-xs">
                                    <thead class="account-orderhistory-table-head responsive-table-head hidden-xs">
                                        <tr>
                                            <th class="col-md-2"><spring:theme code="text.account.orderHistory.po"/></th>
                                            <th class="col-md-2 msoHead">
                                                <div uib-popover-html="'<spring:theme code="text.account.orderHistory.mso.tooltip"/>'" popover-placement="bottom" popover-trigger="'mouseenter'">
                                                    <div class="message">
                                                        <spring:theme code="text.account.orderHistory.mso"/><span class="fa" ng-class="{'fa-angle-up': ohc.view.sortMetaData.mso.isReverseSort, 'fa-angle-down': !ohc.view.sortMetaData.mso.isReverseSort}" ng-click="ohc.onSortClick('mso')"></span>
                                                    </div>
                                                </div>
                                            </th>
                                            <th class="col-md-1 laneHead">Lane</th>

                                            <th class="col-md-2"><spring:theme code="text.account.orderHistory.orderDate"/><span class="fa" ng-class="{'fa-angle-up': ohc.view.sortMetaData.orderDate.isReverseSort, 'fa-angle-down': !ohc.view.sortMetaData.orderDate.isReverseSort}" ng-click="ohc.onSortClick('orderDate')"></span></th>
                                            <th class="col-md-1"><spring:theme code="text.account.orderHistory.orderStatus"/><span class="fa" ng-class="{'fa-angle-up': ohc.view.sortMetaData.status.isReverseSort, 'fa-angle-down': !ohc.view.sortMetaData.status.isReverseSort}" ng-click="ohc.onSortClick('status')"></span></th>
                                            <c:if test="${isUTVUser eq true}">
                                                <th class="col-md-2"><spring:theme code="text.account.orderHistory.shipAfterDate"/>*</th>
                                            </c:if>
                                            <th class="col-md-2"><spring:theme code="text.account.orderHistory.orderPromiseDate"/></th>
                                        </tr>
                                    </thead>
                                    <tr ng-repeat="order in ohc.orderHistory.orders">
                                        <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.orderHistory.po"/></td>
                                        <td class="responsive-table-cell order-number" ng-click="ohc.onOrderClick(order)">
                                            <a href="${orderDetailsUrl}{{order.code}}" target="_self" class="responsive-table-link">
                                                {{order.purchaseOrderNumber || "-"}}
                                            </a>

                                        </td>
                                        <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.orderHistory.mso"/></td>
                                        <td class="responsive-table-cell">{{order.mso || (order.status.code == 'FAILED_SUBMISSION' ? ' ' : (order.status.code == 'SUBMITTED' ? "<spring:theme code="im.order.orderMSODefault" />" : "<spring:theme code="text.intuitive.account.orderHistory.mso"/>"))}}</td>
                                        <td class="laneData">{{order.laneTypeData.name || "-"}}</td>
                                        <td class="hidden-sm hidden-md hidden-lg" ><spring:theme code="text.account.orderHistory.datePlaced"/></td>
                                        <td class="responsive-table-cell">{{ohc.getDateFormat(order.placed)}}</td>
                                        <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.orderHistory.orderStatus"/></td>                                                             
                                        <td class="status toCapitalize">
                                            <div uib-popover-html="'{{ohc.getPopOverText(order.status.code)}}'" popover-placement="top" popover-trigger="'mouseenter'">
                                                <div class="message" ng-class="{'failed': order.status.code == 'FAILED_SUBMISSION'}">
                                                    {{ohc.orderStatusMetaData[order.status.code]}}
                                                </div>
                                            </div>
                                        </td>
                                        <c:if test="${isUTVUser eq true}">
                                            <td class="responsive-table-cell">{{ohc.getUTCDateFormat(order.shipAfterDate)}}</td>
                                        </c:if>
                                        <td class="responsive-table-cell">{{ohc.getDateFormat(order.orderPromiseDate)}}</td>
                                    </tr>
                                </table>
                                <div class="col-xs-12 visible-xs order-history-card">
                                    <div class="mob-label" ng-repeat="order in ohc.orderHistory.orders">
                                        <div class="row">
                                            <div class="col-xs-12 label-head">
                                                <span><spring:theme code="text.account.orderHistory.po"/></span> <a href="${orderDetailsUrl}{{order.code}}" target="_self" class="responsive-table-link">{{order.purchaseOrderNumber || "-"}}</a>
                                            </div>
                                        </div>
                                        
                                        <div class="row quote-date">
                                            <div class="col-xs-6">
                                                <div class="head msoHead">
                                                    <div uib-popover-html="'<spring:theme code="text.account.orderHistory.mso.tooltip"/>'" popover-placement="bottom" popover-trigger="'mouseenter'">
                                                        <div class="message">
                                                            <spring:theme code="text.account.orderHistory.mso"/>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                {{order.mso || (order.status.code == 'FAILED_SUBMISSION' ? ' ' : (order.status.code == 'SUBMITTED' ? "<spring:theme code="im.order.orderMSODefault" />" : "<spring:theme code="text.intuitive.account.orderHistory.mso"/>"))}}
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="head">LANE</div>
                                                {{order.laneTypeData.name.substr(order.laneTypeData.name.length - 1) || "-" }}
                                            </div>
                                        </div>
                                        <div class="row quote-date quote-id-name">
                                            <div class="col-xs-6">
                                                <div class="head"><spring:theme code="text.account.orderHistory.orderDate"/></div>
                                                {{ohc.getDateFormat(order.placed)}}
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="head"><spring:theme code="text.account.orderHistory.shipAfterDate"/>*</div>
                                                {{ohc.getDateFormat(order.shipAfterDate)}}
                                            </div>
                                        </div>
                                        <div class="row quote-date quote-id-name">
                                           <div class="col-xs-6">
                                                <div class="head"><spring:theme code="text.account.orderHistory.orderPromiseDate"/></div>
                                                {{ohc.getDateFormat(order.orderPromiseDate)}}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <hr/>
                                            <div class="col-xs-12 cust-approval">
                                                <div uib-popover-html="'{{ohc.getPopOverText(order.status.code)}}'" popover-placement="top-left" popover-trigger="'mouseenter'">
                                                    <div class="message" ng-class="{'failed': order.status.code == 'FAILED_SUBMISSION'}">
                                                        <span class="dash-border">{{ohc.orderStatusMetaData[order.status.code]}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="visible-xs no-padding history-show-more" ng-if="ohc.orderHistory.orders.length < ohc.view.pagination.totalNumberOfResults" ng-click="ohc.showMoreMobile('mobile')">Show More</div>
                        <div class="row pull-right pagination-bottom account-orderhistory-pagination hidden-xs" ng-if="ohc.view.pagination.numberOfPages > 1">
                            <ul uib-pagination boundary-links="true" total-items="ohc.view.pagination.totalNumberOfResults" ng-model="ohc.view.pagination.currentPage" items-per-page="ohc.view.pagination.pageSize" ng-change="ohc.pageChanged()" class="pagination-sm pull-right" previous-text="&laquo;" next-text="&raquo;" first-text="" last-text="" max-size="3">
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</template:page>
