<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<template:page pageTitle="${pageTitle}">
	<div class="cat-row">
	<div class="cat-container order-review-page cat-main-content" id="CaterpillarPageWrap" ng-app="cat-app"
		ng-controller="reorderReviewController as rrc" ng-cloak
		ng-init="rrc.roleFeatureInfoCUV();rrc.setDefaultBillingAddress(${isUtilityProduct || ''},'${reviewOrderData.billingAddress.line3}', '${reviewOrderData.billingAddress.line1}','${reviewOrderData.billingAddress.region.isocode}', '${reviewOrderData.billingAddress.postalCode}');rrc.setQty('${reviewOrderData.orderedQty}');rrc.setMedataData('${reviewOrderData.salesModelCode}','${reviewOrderData.configModelCode}','${reviewOrderData.configName}','${CSRFToken}');">

		<review-order-pop-up id="orderSuccessMessage"
			alert-header="Your order has been submitted!" 
			ok="DONE"
			
			product-info-text="Retail Configuration"
		
			product-info-value="{{rrc.view.response.configurationId}}"
			quantity-info-text="Quantity"
			quantity-units="{{rrc.view.response.reorderUnits}}"
			quantity="{{rrc.view.response.reorderQty}}"	
			home-page-link={{rrc.view.response.homePageLink}}
			view-order-histroy-link="{{rrc.view.response.orderHistroyLink}}"
			type="success"
			product-type="{{rrc.view.isUtvProduct}}"
			result-po="rrc.view.resultPO"
			></review-order-pop-up>


			<review-order-pop-up id="orderErrorMessage"
			alert-header="VALIDAtiON ERRoR" 
			order-number="{{rrc.view.response.orderNumber}}"
			error-info="This PO number already exists. Please enter a new one and then resubmit order."
			error-pos="rrc.view.errorPos"
			type="error"
			></review-order-pop-up>

		<div class="row">
			<div class="col-xs-12 align-to-left">
				<div class="super-header upperCase"><spring:theme code="cat.review.order.message" /></div>
			</div>
		</div>

		<form:form name="orderReviewForm" id="reviewOrderForm" action="/checkout/submitReOrder"
			onsubmit="return false;" commandName="ReviewOrderForm" method="POST">

							<div class="row sectionWrap orderDetailsTab">
	       <div class="row col-md-12">
			<div class="col-md-12 orderDetails_new">
			 <div class="orderDetailsHeader_new no-border-bottom">
			 	<div class="col-xs-12 headerHold">
			 	   <spring:theme code="im.reorder.details" />
			 	</div>
			 </div>
			 <!-- Order header Tabs -->
			 <div class="orderDetailsHeaderTab_new">
				<div ng-if="false" class="row utv-product">
						<div class="col-xs-6 col-md-3 tabContent">
								<div class="tabContentHeader">
									<spring:theme code="im.reorder.sales.model" /> 
								</div>
								<div class="tabContentText">
										${reviewOrderData.salesModelName}
								</div>
						 </div>
						 <div class="col-xs-6 col-md-3 tabContent">
								<div class="tabContentHeader">
									<c:if test="${isUtilityProduct}">
										<spring:theme code="im.utv.configuration.id" />
									</c:if>
								</div>
								<div class="tabContentText">
										${reviewOrderData.configName}
								</div>
						 </div>
						 <div class="col-xs-6 col-md-3 tabContent">
								<div class="tabContentHeader">
									<c:if test="${isUtilityProduct}"><spring:theme code="utv.order.quantity" /></c:if>
								</div>
								<div class="tabContentText">
									<c:if test="${isUtilityProduct}">${reviewOrderData.orderedQty} Units</c:if>
								</div>
						 </div>
						 <div class="col-xs-6 col-md-3 tabContentLast">
								<div class="tabContentHeader">
									<c:if test="${isUtilityProduct}"><spring:theme code="utv.truckload.quantity" /></c:if>	
								</div>
								<div class="tabContentText row">
								<c:if test="${isUtilityProduct}">
								<fmt:formatNumber type = "number" maxFractionDigits = "0" value = "${reviewOrderData.orderedQty/shippingFactor}" />
								</c:if>	
								</div>
						 </div>

				</div>
				<c:if test="${not isUtilityProduct}">
					<div class="row review-row" ng-repeat="(key,entry) in rrc.view.reviewOrderEntry.salesModelEntries track by $index">
							
							<div class="col-xs-6 col-md-2 tabContent">
									<div class="tabContentHeader">
										<spring:theme code="im.reorder.sales.model" /> 
									</div>
									<div class="tabContentText">
											<div>{{key.split('__')[0]}}</div>
											<div class="hidden-xs lane-info-tag-review text-center col-xs-12 cat-buttons secondary-btn">{{rrc.view.laneType.name}} <span class="dot" ng-class="{'{{rrc.view.laneType.code}}-dot' : rrc.view.laneType.code}"></span></div>
								</div>  
							</div>
							<div class="visible-xs col-xs-6 col-md-2 tabContent">
									<div class="tabContentHeader">
										<div class="lane-info-tag-review text-center col-xs-12 cat-buttons secondary-btn">{{rrc.view.laneType.name}} <span class="dot" ng-class="{'{{rrc.view.laneType.code}}-dot' : rrc.view.laneType.code}"></span></div>
									</div>
							</div>
							<div class="col-xs-12 col-md-2 tabContent">
									<div class="tabContentHeader">
										<spring:theme code="im.reorder.projected.quantity" /> 
									</div>
									<div class="tabContentText">
											{{entry[1].projectedInventory}} <spring:theme code="im.order.units" />

									</div>
							</div>
							<div class="col-xs-4 col-md-2 tabContent">
									<div class="tabContentHeader">
										<spring:theme code="im.reorder.configuration.id" /> 
									</div>
									<div class="tabContentText" ng-repeat="(objKey, data) in entry[0]">
										{{data.product.name}}
										<div class="hidden-data" ng-if="$index > 0"  ng-repeat="poNumber in data.purchaseOrderNumberList">
											hidden
										</div>
									</div>
							</div> 
							<div class="col-xs-4 col-md-2 tabContent">
									<div class="tabContentHeader">
										<spring:theme code="im.reorder.stock.quantity" />
										<!-- <c:if test="${isUtilityProduct}">
											<spring:theme code="im.utv.configuration.id" />
										</c:if>
										<c:if test="${not isUtilityProduct}">
											<spring:theme code="im.reorder.configuration.id" />
										</c:if> -->
									</div>
							<div class="tabContentText" ng-repeat="(objKey, data) in entry[0]">
								{{data.quantity}} <spring:theme code="im.order.units" />
								<div class="hidden-data" ng-if="$index > 0"  ng-repeat="poNumber in data.purchaseOrderNumberList">
									hidden
								</div>
							</div>
									
							</div>
							<div class="col-xs-4 col-md-2 tabContent">
									<div class="tabContentHeader">
										<spring:theme code="im.reorder.purchase.order.number" />
										<!-- <c:if test="${isUtilityProduct}"><spring:theme code="utv.order.quantity" /></c:if>
										<c:if test="${not isUtilityProduct}"><spring:theme code="im.reorder.purchase.order.number" /></c:if> -->
									</div>
									<div class="tabContentText" ng-repeat="(objKey, data) in entry[0]">
										<div ng-repeat="poNumber in data.purchaseOrderNumberList">
											{{poNumber}}
										</div>
									</div>
							</div>
							<div class="col-xs-6 col-md-2 tabContentLast">
									<div class="tabContentHeader">
											<spring:theme code="im.reorder.total.list.price" />
									<!-- <c:if test="${isUtilityProduct}"><spring:theme code="utv.truckload.quantity" /></c:if>
										<c:if test="${not isUtilityProduct}"><spring:theme code="im.reorder.total.list.price" /></c:if>	 -->
									</div>
									<div class="tabContentText row">
										{{ rrc.totalLastPrice(entry[0])}}
									</div>
							</div>
					</div>
				</c:if>
				
				
			 </div>

			<c:if test="${isUtilityProduct}">
			 <div class="row shipping-order" ng-if="false">
				<div class="col-lg-4 col-md-4 col-xs-12 shipping-review">
					<div class="row">
						<div class="col-xs-4 element-1"><spring:theme code="im.utv.order.units" /></div>
						<div class="col-xs-8 element-2"><spring:theme code="im.utv.order.ship.date" /></div>
					</div>
					<div class="shipping-data">
						<c:set var="counter" value="0"></c:set>
						<c:forEach var="shipAfterQtyData" items="${reviewOrderData.shipAfterQuantities}">
							<c:set var="counter" value="${counter + 1}"></c:set>
							<div class="row">						
								<div class="col-xs-4 element-1">${shipAfterQtyData.quantity}</div>
								<div class="col-xs-8 element-2">
									${shipAfterQtyData.shippingWindow.shipAfterName} &nbsp;
									
									<c:if test="${shipAfterQtyData.shippingWindow.shipFromDate.date mod 10 eq 1 and shipAfterQtyData.shippingWindow.shipFromDate.date lt 10}"><c:set var="startDate" value="${shipAfterQtyData.shippingWindow.shipFromDate.date}st"/></c:if>
											<c:if test="${shipAfterQtyData.shippingWindow.shipFromDate.date mod 10 eq 2}"><c:set var="startDate" value="${shipAfterQtyData.shippingWindow.shipFromDate.date}nd"/></c:if>
											<c:if test="${shipAfterQtyData.shippingWindow.shipFromDate.date mod 10 eq 3}"><c:set var="startDate" value="${shipAfterQtyData.shippingWindow.shipFromDate.date}rd"/></c:if>
											<c:if test="${shipAfterQtyData.shippingWindow.shipFromDate.date mod 10 gt 3 or shipAfterQtyData.shippingWindow.shipFromDate.date mod 10 eq 0 or (shipAfterQtyData.shippingWindow.shipFromDate.date gt 10 and shipAfterQtyData.shippingWindow.shipFromDate.date lt 20)}"><c:set var="startDate" value="${shipAfterQtyData.shippingWindow.shipFromDate.date}th"/></c:if>
											
									<c:if test="${shipAfterQtyData.shippingWindow.shipToDate.date mod 10 eq 1}"><c:set var="endDate" value="${shipAfterQtyData.shippingWindow.shipToDate.date}st"/></c:if>
											<c:if test="${shipAfterQtyData.shippingWindow.shipToDate.date mod 10 eq 2}"> <c:set var="endDate" value="${shipAfterQtyData.shippingWindow.shipToDate.date}nd"/></c:if>
											<c:if test="${shipAfterQtyData.shippingWindow.shipToDate.date mod 10 eq 3}"><c:set var="endDate" value="${shipAfterQtyData.shippingWindow.shipToDate.date}rd"/></c:if>
											<c:if test="${shipAfterQtyData.shippingWindow.shipToDate.date mod 10 gt 3 or shipAfterQtyData.shippingWindow.shipToDate.date mod 10 eq 0 or (shipAfterQtyData.shippingWindow.shipToDate.date gt 10 and shipAfterQtyData.shippingWindow.shipToDate.date lt 20)}"><c:set var="endDate" value="${shipAfterQtyData.shippingWindow.shipToDate.date}th"/></c:if>
									
									
									${startDate}&nbsp; ${shipAfterQtyData.shippingWindow.shipAfterName}&nbsp;-&nbsp; ${endDate} &nbsp;${shipAfterQtyData.shippingWindow.shipAfterName}
									
								</div>						
							</div>
						</c:forEach>
					</div>   
				</div>	
			</div> 
			 </c:if>
             <!-- order Content Tab below -->	
                <div class="orderContentTab_new clearfix" ng-if="${isUtilityProduct}">
					<div class="cuv-line-entry clearfix" ng-class="{'cuv-container': $last }" ng-repeat="cuvEntry in rrc.quantityBasedArray track by $index">
						<div class="col-sm-4 line-item" ng-class="{'img-details' : 0 === $index }">
							<div class="col-sm-6 no-padding-mobile">
								<img ng-show="$index === 0" class="truck-img" ng-src="https://{{rrc.cuvTruckImage}}">
								<span ng-if="$index === 0" class="config-id config-id-mb visible-xs">{{rrc.view.response.configurationId.split('__')[0]}}</span>
							</div>
							<div class="config-id hidden-xs" ng-if="$index === 0">{{rrc.view.response.configurationId.split('__')[0]}}</div>
							<c:set var="counter" value="0"></c:set>
							<div class="col-sm-6 info-body" ng-class="{'details-section' : ${counter} !== $index, 'details-section-mb' : ${counter} === $index }">								
								<c:forEach var="shipAfterQtyData" items="${reviewOrderData.shipAfterQuantities}">
									<div ng-if="${counter} === $index" class="row">	
										<c:set var="counter" value="${counter + 1}"></c:set>
										<div class="col-sm-12 col-xs-6 element-1 no-padding-desktop">TRUCK {{$index + 1}}  - SHIP AFTER DATE</div>
										<div class="col-sm-12 col-xs-6 element-2 no-padding-desktop">
											<c:if test="${shipAfterQtyData.shippingWindow.shipFromDate.date mod 10 eq 1 and shipAfterQtyData.shippingWindow.shipFromDate.date lt 10}"><c:set var="startDate" value="${shipAfterQtyData.shippingWindow.shipFromDate.date}st"/></c:if>
													<c:if test="${shipAfterQtyData.shippingWindow.shipFromDate.date mod 10 eq 2}"><c:set var="startDate" value="${shipAfterQtyData.shippingWindow.shipFromDate.date}nd"/></c:if>
													<c:if test="${shipAfterQtyData.shippingWindow.shipFromDate.date mod 10 eq 3}"><c:set var="startDate" value="${shipAfterQtyData.shippingWindow.shipFromDate.date}rd"/></c:if>
													<c:if test="${shipAfterQtyData.shippingWindow.shipFromDate.date mod 10 gt 3 or shipAfterQtyData.shippingWindow.shipFromDate.date mod 10 eq 0 or (shipAfterQtyData.shippingWindow.shipFromDate.date gt 10 and shipAfterQtyData.shippingWindow.shipFromDate.date lt 20)}"><c:set var="startDate" value="${shipAfterQtyData.shippingWindow.shipFromDate.date}th"/></c:if>
													
											<c:if test="${shipAfterQtyData.shippingWindow.shipToDate.date mod 10 eq 1}"><c:set var="endDate" value="${shipAfterQtyData.shippingWindow.shipToDate.date}st"/></c:if>
													<c:if test="${shipAfterQtyData.shippingWindow.shipToDate.date mod 10 eq 2}"> <c:set var="endDate" value="${shipAfterQtyData.shippingWindow.shipToDate.date}nd"/></c:if>
													<c:if test="${shipAfterQtyData.shippingWindow.shipToDate.date mod 10 eq 3}"><c:set var="endDate" value="${shipAfterQtyData.shippingWindow.shipToDate.date}rd"/></c:if>
													<c:if test="${shipAfterQtyData.shippingWindow.shipToDate.date mod 10 gt 3 or shipAfterQtyData.shippingWindow.shipToDate.date mod 10 eq 0 or (shipAfterQtyData.shippingWindow.shipToDate.date gt 10 and shipAfterQtyData.shippingWindow.shipToDate.date lt 20)}"><c:set var="endDate" value="${shipAfterQtyData.shippingWindow.shipToDate.date}th"/></c:if>
											
											
													${startDate}&nbsp; ${shipAfterQtyData.shippingWindow.shipAfterName}&nbsp;-&nbsp; ${endDate} &nbsp;${shipAfterQtyData.shippingWindow.shipAfterName}
											
										</div>
										<div class="col-xs-12 col-xs-6 element-1 no-padding-desktop">QUANTITY</div>
										<div class="col-sm-12 col-xs-6 element-2 no-padding-desktop">${shipAfterQtyData.quantity}</div>					
									</div>
								</c:forEach>
							</div>
						</div>
						<div class="col-sm-8 no-padding-desktop" ng-class="0 !== $index ? 'po-section' : 'po-section-first'">
							<div class="orderContentTabHeader row" ng-class="0 === $index ? 'po-box-align' : ''">
								<span class="col-xs-12 col-md-4 errorLabel no-pad-left"><spring:theme code="cat.purchase.order.numbers" /><sup> * </sup></span>
							</div>
							<div class="row po-info-message">
								<div class="col-xs-12 no-pad-left">
									<span class="purchase-text">
										<spring:theme code="cat.enter.ponumber.message" />
									</span>
									<span class="error" ng-if="cuvEntry.hybrisValidateError"> <spring:theme code="cat.ponumber.error.message" /> </span>
								<span class="error" ng-if="cuvEntry.repeatError"> <spring:theme code="cat.ponumber.unique.message" /> </span>
								<span class="error" ng-if="cuvEntry.madatoryError"> <spring:theme code="cat.ponumber.mandatory.message" /> </span>
								</div>
							</div>
							<div class="message-section" >
								<span ng-if="cuvEntry.filledBoxes !== '' " class="po-count-img" ng-class="rrc.poCountStatusImg(cuvEntry)"></span>
								<span ng-if="cuvEntry.filledBoxes !== '' " class="po-count-msg">{{cuvEntry.filledBoxes}} out of {{cuvEntry.quantity}} added</span>
							</div>
							<div ng-if="$index >= cuvEntry.minQuantity && cuvEntry.maxQuantity > $index" class="row orderContentTabLower col-xs-6 col-md-3" ng-repeat="inputData in rrc.range track by $index">   
								<div class="inputCell">
									<span class="po-no">{{$index - cuvEntry.minQuantity + 1}}.</span><input cat-capitalize class="inputPO po-alignment" type="text" restrict-model-input="^[A-Za-z0-9]{0,12}$" not-allow-to="[]" maxlength='12'  id="{{inputData.id}}"  ng-model="inputData.value" ng-blur="rrc.validate(inputData.value, $index)" 
									placeholder="Enter PO #" ng-class="{hasError: inputData.isInvalid}" ng-trim="false"/>
								</div>
							</div>
						</div>	
					</div>
			</div>

	   </div>

				</div>
				</div>
			<div ng-if="${isUtilityProduct}" class="row cuv-unit-price">
				<div class="row">
					<div class="col-xs-8 col-sm-9 label">
						<spring:theme code="truckload.review.order.total.truckload.text" />
					</div>
					<div class="col-xs-4 col-sm-3 value">
						{{rrc.quantityBasedArray.length}}
					</div>
					<div class="col-xs-8 col-sm-9 label">
						<spring:theme code="truckload.review.order.total.units.text" />
					</div>
					<div class="col-xs-4 col-sm-3 value">
						{{rrc.range.length}}
					</div>
					<div class="col-xs-8 col-sm-9 label">
						<spring:theme code="truckload.review.order.total.price.text" />
					</div>
					<div class="col-xs-4 col-sm-3 value">
						<span>$</span>{{rrc.cuvTotalPrice}}
					</div>
				</div>    
			</div>

			<div class="row sectionWrap delearsDetailsHold">
				<div class="col-xs-12 headerHold">
					<spring:theme code="im.reorder.dealer.details" />
				</div>
				<div class="col-xs-12 formelementhold">
					<div class="row">
						<div class="col-xs-12 col-md-2 no-padding">
							<p class="lable">
								<spring:theme code="im.reorder.dealer.name" />
							</p>
						</div>
						<div class="col-xs-12 col-md-9 dealer-section">
							<div class="form-group clearfix">
								<div class="col-md-4 no-padding">
									<input ng-if="${isUtilityProduct}" type="text" name="dealerName"
										class="form-control no-border" id="dealerName" readonly="readonly"
										value="{{rrc.view.reviewOrderEntry.dealerName | initcaps}}"/>
										<input ng-if="${not isUtilityProduct}" type="text" name="dealerName"
										class="form-control no-border" id="dealerName" readonly="readonly"
										value="{{rrc.view.reviewOrderEntry.dealerName}}"/>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-2 no-padding">
							<p class="lable">
								<spring:theme code="im.reorder.shipping.address" />
							</p>
						</div>
						<div class="col-xs-12 col-md-9 dealer-section">
							<div class="form-group clearfix">
								<div class="col-md-6 no-padding" ng-init="rrc.fetchShippingAddress() && ${isUtilityProduct || ''};" >
										<cat-address  ship-address-id="reviewShippingAddressId" update-selected-address="updateDropDownValue(addrssObj)"></cat-address>
								</div>
							</div>
						</div>
						
						<%-- <div class="col-md-12"> 
							<div class="col-xs-12 col-md-2  no-padding">
								<p class="lable">
									<spring:theme code="im.reorder.billing.address" />
								</p>
							</div>
							<div class="col-md-4 billing-address-section">
								<c:if test="${not empty reviewOrderData.billingAddress.line3}">
									<div> <label class="camelCase">${fn:toLowerCase(reviewOrderData.billingAddress.line3)}</label></div>
								</c:if>
								<c:if test="${not empty reviewOrderData.billingAddress.line1}">
									<div> <label class="camelCase">${fn:toLowerCase(reviewOrderData.billingAddress.line1)}</label></div>
								</c:if>
								<c:if test="${not empty reviewOrderData.billingAddress.town}">
									<div>
										<label class="camelCase">${fn:toLowerCase(reviewOrderData.billingAddress.town)}
								        <c:if test="${not empty reviewOrderData.billingAddress.region.name}">
											, ${fn:toLowerCase(reviewOrderData.billingAddress.region.name)}
										</c:if>
										</label>
									</div>
								</c:if>
								<c:if test="${not empty reviewOrderData.billingAddress.country.isocode}">
									<div>
										<label>${reviewOrderData.billingAddress.country.isocode}
											<c:if test="${not empty reviewOrderData.billingAddress.postalCode}">
												, ${reviewOrderData.billingAddress.postalCode}
											</c:if>
										</label>
									</div>
								</c:if>					
							</div>
							</div> --%>
							<!-- <c:if test="${not isUtilityProduct}">
							<div class="col-md-12 enter-comments-section">
								<div class="col-md-3 comment-label-padding">
									<label class="lable"><spring:theme code="cat.enter.comments.message" /></label>
								</div>
								<div class="col-md-7">
									<input class="comments-input-box cat-inputs type2 commentInput" type="text" ng-model="rrc.userComments" maxlength="40"/>
								</div>
							</div>
							</c:if> -->
							
						</div>
						<c:if test="${not isUtilityProduct}">
							<div class="col-md-12 enter-comments-section">
								<div class="col-md-2 comment-label-padding">
									<label class="lable"><spring:theme code="cat.enter.comments.message" /></label>
								</div>
								<div class="col-md-7">
									<input class="comments-input-box cat-inputs type2 commentInput pull-left" type="text" ng-model="rrc.userComments" maxlength="40" />
									<span class="text-counter text-right" class='has-error'>{{rrc.userComments.length||0}} of 40</span>
								</div>
							</div>
						</c:if>
					</div>
				</div>
				<div class="row" id="submitOrderHold">
					<div class="indicateMsg col-md-12"><spring:theme code="cat.indicates.required.fileds.message" /></div>
					<div class="form-group clearfix">
						<div class="col-xs-12 col-md-8 checkBoxSection">
							<div class="row">
							<input type="checkbox" name="shippingCheckbox" id="certify"
								value="true" ng-model="rrc.view.userCertified" ng-click="rrc.view.userCertified?rrc.triggerValidations():''"> 
								<label
								for="certify" id="certifyLabel" ng-class="!!rrc.view.formSubmited && !rrc.view.userCertified ? 'highlighted-box': ''"> <span
								aria-hidden="true" ng-class="rrc.view.userCertified?'tickFont':''"></span>
							</label> <label class="checkboxlabel"><spring:theme code="cat.necessary.approval.message" /></label>
							</div>
							<div class="row disclimer-error-message" ng-show="!!rrc.view.formSubmited && !rrc.view.userCertified">
								<spring:theme code="cat.select.submit.order.checkbox" />
							</div>
						</div>
						<div  class="col-xs-12 col-md-2">
								<input ng-if="${not isUtilityProduct}" class="edit-order-button" type="button" value="<spring:theme code="im.order.edit.button" />" ng-click="rrc.editOrder()" />
							</div>
						<div class="col-xs-12 col-md-2" id="submitorderbuttonHold">
							<input type="button" id="submit" value="<spring:theme code="im.order.submit.button" />" ng-class="(!(rrc.disableSubmit && rrc.view.userCertified) || !!rrc.view.processingRequest)?'disabled':''" ng-disabled="(!(rrc.disableSubmit && rrc.view.userCertified) || !!rrc.view.processingRequest)" ng-click="rrc.validateLaneToReviewOrder(${isUtilityProduct || ''})" />
						</div>
					</div>
				</div>
				<%-- <div class="row hidden" id="orderConfirmationHold">
					<div class="col-xs-12 col-md-4 conmsghold">
						<div class="row headerhold">
							<div class="col-xs-12">
								<h3><spring:theme code="cat.order.submitted.message" /></h3>
							</div>
						</div>
						<div class="row padding-top">
							<div class="col-xs-8 conlabel">order number</div>
							<div class="col-xs-4 convalue">1234</div>
						</div>
						<div class="row padding-top">
							<div class="col-xs-8 conlabel">Configarion ID</div>
							<div class="col-xs-4 convalue">1234-dfr</div>
						</div>
						<div class="row padding-top">
							<div class="col-xs-8 conlabel">reorder stock quantity</div>
							<div class="col-xs-4 convalue">4</div>
						</div>
						<div class="row conbuttonhold">
							<div class="col-xs-12">
								<button class="orderSubConButton">done</button>
							</div>
						</div>
					</div>

				</div> --%>
		</form:form>
	</div>
	</div>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</template:page>
