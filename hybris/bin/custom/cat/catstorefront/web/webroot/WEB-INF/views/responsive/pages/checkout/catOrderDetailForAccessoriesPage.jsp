<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
 

<template:page pageTitle="${pageTitle}">
        <div id="accessoriesOrder" class="cat-main-container cat-container">
                <div class="row">
                    <div class="titleHeader"><spring:theme code="im.order.details" /></div> 
                    <div class="Home pull-right">
                        <span class="homeIcon">
                            <a href="/">
                                <img src="/_ui/responsive/cat/images/fill-300.png">
                            </a>
                        </span>
                        <span class="backToHomebtn">
                            <a href="/">
                                <spring:theme code="back.to.home" />
                            </a>
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="orderSumarrySectionDesktop hidden-xs col-xs-12">
                                <div class="row">
                                    <div class="col-md-12 ordersummaryHead"><spring:theme code="cat.order.details.order.summary"/></div>
                                </div>
                                    <div class="row fisrtHalf col-md-11">
                                        <div class="col-md-2">
                                            <div class="summaryLabel"><spring:theme code="cat.order.details.po"/></div>
                                            <div class="summaryValue">
                                                <div>${orderData.purchaseOrderForAccessories}</div>
                                            </div>
                                       </div>
                                        <div class="col-md-2">
                                            <div class="summaryLabel"><spring:theme code="cat.order.details.mso"/></div>
                                            <div class="summaryValue">
                                                <div>
                                                    <c:if test="${not empty orderData.mso}">
                                                        ${orderData.mso}
                                                    </c:if>
                                                    <c:if test="${empty orderData.mso and orderData.status ne 'FAILED_SUBMISSION'}">
                                                        <c:choose>
                                                            <c:when test="${orderData.status eq 'SUBMITTED'}">
                                                                <spring:theme code="im.order.orderMSODefault" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <spring:theme code="text.intuitive.account.orderHistory.mso" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:if>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="summaryLabel"><spring:theme code="cat.order.details.order.status"/></div>
                                            <div class="summaryValue">
                                                <div><spring:theme code="${orderData.status}" /></div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="summaryLabel"><spring:theme code="cat.order.details.total.price"/></div>
                                            <div class="summaryValue">${orderData.totalPrice.formattedValue}</div>
                                        </div>
                                    </div>
                                    <div class="row secondHalf col-md-12">
                                            <div class="col-md-2">
                                                <div class="summaryLabel"><spring:theme code="cat.order.details.order.submitted.date"/></div>
                                                <div class="summaryValue">
                                                    <fmt:formatDate value="${orderData.created}" pattern="MM-dd-yyyy" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="summaryLabel"><spring:theme code="cat.order.details.intransit.status"/></div>
                                                <div class="summaryValue">
                                                    <div >
                                                        <c:choose>
                                                            <c:when test="${empty orderData.inTransitStatus}">
                                                                <spring:theme code="im.order.inTransitStatusDefault" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                ${orderData.inTransitStatus}
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="summaryLabel"><spring:theme code="cat.order.details.estimated.delivery"/></div>
                                                <div class="summaryValue">
                                                    <c:choose>
                                                    <c:when test="${empty orderData.estimatedDelivery}">
                                                        <spring:theme code="im.order.estimatedDeliveryDateDefault" />
                                                    </c:when>
                                                    <c:otherwise>
                                                        <fmt:formatDate value="${orderData.estimatedDelivery}"
                                                            pattern="MM-dd-yyyy" />
                                                    </c:otherwise>
                                                    </c:choose>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="summaryLabel"><spring:theme code="cat.order.details.order.promise.date"/></div>
                                                <div class="summaryValue">
                                                    <fmt:formatDate value="${orderData.orderPromiseDate}" pattern="MM-dd-yyyy" />
                                                    <div>
                                                        <c:choose>
                                                            <c:when test="${empty orderData.orderPromiseDate}">
                                                                <spring:theme code="im.order.orderPromiseDateDefault" />
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fmt:formatDate value="${orderData.orderPromiseDate}"
                                                                    pattern="MM-dd-yyyy" />
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </div>
                                                </div>
                                            </div>
                                </div>
                    </div>
                    <div class="orderSumarrySectionMobile visible-xs col-xs-12">
                            <div class="row">
                                <div class="col-xs-12 ordersummaryHead"><spring:theme code="cat.order.details.order.summary"/></div>
                            </div>
                            <div class="orderSummarycontMob col-xs-12">
                                <div class="col-xs-12 firstHalf">
                                        <div class="col-xs-2 summaryLabel"><spring:theme code="cat.order.details.po"/></div>
                                        <div class="col-xs-4 summaryValue">
                                            <div>${orderData.purchaseOrderForAccessories}</div>
                                        </div>
                                        <div class="col-xs-2 summaryLabel msoLabel"><spring:theme code="cat.order.details.mso"/></div>
                                        <div class="col-xs-4 summaryValue">
                                            <div>
                                                <c:if test="${not empty orderData.mso}">
                                                    ${orderData.mso}
                                                </c:if>
                                                <c:if test="${empty orderData.mso and orderData.status ne 'FAILED_SUBMISSION'}">
                                                    <c:choose>
                                                        <c:when test="${orderData.status eq 'SUBMITTED'}">
                                                            <spring:theme code="im.order.orderMSODefault" />
                                                        </c:when>
                                                        <c:otherwise>
                                                            <spring:theme code="text.intuitive.account.orderHistory.mso" />
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:if>
                                            </div>
                                        </div> 
                                    <div class="col-xs-12 PriceOrder">
                                        <div class="totalPriceSection">
                                            <div class="row summaryLabel col-xs-2"><spring:theme code="cat.order.details.total.price"/></div>
                                            <div class="row summaryValue col-xs-4">${orderData.totalPrice.formattedValue}</div>
                                        </div>
                                        <div class="orderStatus">
                                            <div class="row summaryLabel col-xs-2"><spring:theme code="cat.order.details.order.status"/></div>
                                            <div class="row summaryValue col-xs-4">
                                                <div><spring:theme code="${orderData.status}" /></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12  ordersubmitSection">
                                    <div class="col-xs-5 summaryLabel"><spring:theme code="cat.order.details.order.submit.date"/></div>
                                    <div class="col-x-4 summaryValue">
                                        <c:choose>
                                            <c:when test="${empty orderData.created}">
                                                <spring:theme code="im.order.createdDateDefault" />
                                            </c:when>
                                            <c:otherwise>
                                                <fmt:formatDate value="${orderData.created}"
                                                    pattern="MM-dd-yyyy" />
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                                <div class="col-xs-12  inTransitSection">
                                    <div class="col-xs-5 summaryLabel"><spring:theme code="cat.order.details.intransit.status"/></div>
                                    <div class="col-x-4 summaryValue">
                                        <div>
                                            <c:choose>
                                                <c:when test="${empty orderData.inTransitStatus}">
                                                    <spring:theme code="im.order.inTransitStatusDefault" />
                                                </c:when>
                                                <c:otherwise>
                                                    ${orderData.inTransitStatus}
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 estimatedSection">
                                    <div class="col-xs-5 summaryLabel"><spring:theme code="cat.order.details.estimated.delivery"/></div>
                                    <div class="col-x-4 summaryValue">
                                        <div>
                                            <fmt:formatDate value="${orderData.created}" pattern="MM-dd-yyyy" />
                                                    <div>${orderData.estimatedDelivery}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12  promiseSection">
                                    <div class="col-xs-5 summaryLabel"><spring:theme code="cat.order.details.order.promise.date"/></div>
                                    <div class="col-x-4 summaryValue">
                                        <div>
                                            <c:choose>
                                                <c:when test="${empty orderData.orderPromiseDate}">
                                                    <spring:theme code="im.order.orderPromiseDateDefault" />
                                                </c:when>
                                                <c:otherwise>
                                                    <fmt:formatDate value="${orderData.orderPromiseDate}"
                                                        pattern="MM-dd-yyyy" />
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="row">
                    <div class="accessoriesSectionDesktop col-xs-12 hidden-xs">
                            <div class="row">
                                <div class="col-md-12 accessoriesHeading">accessories</div>
                            </div>
                            <div class="row tableHeaders">
                                <div class="col-md-3 partDescriptionLabel"><spring:theme code="cat.order.details.part.description"/></div>
                                <div class="col-md-2 partNumberLabel"><spring:theme code="cat.order.details.part.number"/></div>
                                <div class="col-md-2 salesModelLabel"><spring:theme code="cat.order.details.sales.model"/></div>
                                <div class="col-md-2 listPriceLabel"><spring:theme code="cat.order.details.list.price"/></div>
                                <div class="col-md-1 quantityLabel"><spring:theme code="cat.order.details.quantity"/></div>
                                <div class="col-md-2 totalPriceLabel"><spring:theme code="cat.order.details.total.price"/></div>
                            </div>
                            <c:forEach items="${orderData.compatibleModelOrderGroup}" var="model">
                                <div class="tableDataSection">
                                    <c:forEach items="${model.value}" var="lineItem">
                                        <div>
                                            <div class="row tableValues">
                                                <div class="col-md-3 partDescriptionValue">${lineItem.product.name}</div>
                                                <div class="col-md-2 partNumberValue">${lineItem.product.code}</div>
                                                <div class="col-md-2 salesModelValue">${orderData.compatibleModel}</div>
                                                <div class="col-md-2 listPriceValue">${lineItem.basePrice.formattedValue}</div>
                                                <div class="col-md-1 quantityValue">${lineItem.quantity}</div>
                                                <div class="col-md-2 totalPriceValue">${lineItem.totalPrice.formattedValue}</div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </c:forEach>
                            <div class="row totalSection">
                                <div class="col-md-2 col-md-push-7 totalLabel"><spring:theme code="cat.order.details.total"/></div>
                                <div class="col-md-1 col-md-push-7 qunatitySum">${orderData.totalUnitCount}</div>
                                <div class="col-md-2 col-md-push-7 priceSum">${orderData.totalPrice.formattedValue}</div>
                            </div>
                    </div>
                   <div class="accessoriesSectionMobile visible-xs">
                        <div class="row">
                            <div class="accessoriesHeading col-xs-12">accessories</div>
                        </div>
                        <div class="row">
                            <c:forEach items="${orderData.compatibleModelOrderGroup}" var="model">
                                <div class="lineItemDescription col-xs-12">
                                    <div class="col-xs-12 salesModelName">${orderData.compatibleModel}</div>
                                    <c:forEach items="${model.value}" var="lineItem">
                                        <div class="col-xs-12 lineCards">
                                            <div class="col-xs-12 partDescription">${lineItem.product.name}</div>
                                            <div class="col-xs-12 partnumberlabel"><spring:theme code="cat.order.details.part.number"/></div>
                                            <div class="col-xs-12 partnumberValue">${lineItem.product.code}</div>
                                            <div class="col-xs-12 listPriceSection">
                                                <div class="col-xs-6 listPriceLabel"><spring:theme code="cat.order.details.list.price"/></div>
                                                <div class="col-xs-6 listPriceValue">${lineItem.basePrice.formattedValue}</div>
                                            </div>
                                            <div class="col-xs-12 quantiySection">
                                                <div class="col-xs-6 quantityLabel"><spring:theme code="cat.order.details.quantity"/></div>
                                                <div class="col-xs-6 quantityValue">${lineItem.quantity}</div>
                                            </div>
                                            <div class="totalListPriceSection col-xs-12">
                                                <div class="col-xs-6 totallistPriceLabel"><spring:theme code="cat.order.details.list.price"/></div>
                                                <div class="col-xs-6 totalListPriceVlaue">${lineItem.totalPrice.formattedValue}</div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </c:forEach>
                        </div>
                        <div class="row">
                            <div class="totalSection col-xs-12">
                                <div class="col-xs-12 quantityAndPrice">
                                    <div class="quantity">
                                        <div class="col-xs-6 totalQuantityLabel"><spring:theme code="cat.order.details.quantity"/></div>
                                        <div class="col-xs-6 totalQuantityValue">${orderData.totalUnitCount}</div>
                                    </div>
                                    <div class="total col-xs-12">
                                        <div class="col-xs-6 totalPriceLabel"><spring:theme code="cat.order.details.total.price"/></div>
                                        <div class="col-xs-6 totalPriceValue">${orderData.totalPrice.formattedValue}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                        <div class="dealerDetailsSection col-xs-12">
                                    <div class="row">
                                        <div class="col-md-12 col-xs-12 dealerDetailHeading">dealer details</div>
                                   </div>
                                    <div class="row">
                                        <div class="col-md-2 col-xs-12 dealerNameLable">dealer name</div>
                                        <div class="col-md-2 col-xs-12 dealerNameValue">${orderData.b2bCustomerData.unit.name}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2 col-xs-12 shippingAddressLabel">shipping address *</div>
                                        <div class="col-md-3 col-xs-12 shippingAddressValue">
                                            <div class="addressLine1">${orderData.deliveryAddress.shipToCode}, ${orderData.deliveryAddress.line1}</div>
                                            <div class="addressLine2">${orderData.deliveryAddress.town}</div>
                                            <div class="stateName">${orderData.deliveryAddress.region.name}</div>
                                            <div class="stateZip">${orderData.deliveryAddress.country.isocode}, ${orderData.deliveryAddress.postalCode}</div>
                                        </div>
                                    </div>
                        </div>
                </div>
        </div>
</template:page>