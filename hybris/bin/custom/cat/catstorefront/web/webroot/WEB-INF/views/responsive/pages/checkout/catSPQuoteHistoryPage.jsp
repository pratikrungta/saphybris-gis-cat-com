<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/desktop/action" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<template:page pageTitle="${pageTitle}">
<spring:url value="/my-account/my-quotes/" var="quoteDetailsUrl" htmlEscape="false"/>
<div class="cat-row cat-main-content">
    <div ng-app="cat-app" ng-controller="quoteHistoryController as ohc" ng-init="ohc.init()" ng-cloak>
    <div class="orderHistoryPage row cat-container">
        <div class="order-cat-container col-md-12 col-xs-12">
            <div class="row no-gutters pagination-container top">
                <div class="col-xs-6">
                    <div class="search-text-header upperCase"><spring:theme code="text.account.quoteHistory"/></div>
                </div>
                <div class="col-xs-5 visible-xs pull-right">
                    <div class="dropdown sort-dropdown pull-right">
                        <button class="btn" type="button" data-toggle="dropdown-menu" ng-click="ohc.view.sortBtn = !ohc.view.sortBtn" >
                            <i class="fa fa-arrow-up" aria-hidden="true"></i>
                            <i class="fa fa-arrow-down" aria-hidden="true"></i>
                            <spring:theme code="cat.quote.history.sort.button" />
                        </button>
                        <ul id="dropdownList" class="dropdown-menu" ng-show="ohc.view.sortBtn">
                            <li ng-click="ohc.onSortClick(key)" ng-class="{'selected': sortObj.isSelected}" ng-repeat="(key, sortObj) in ohc.view.sortMetaData"><span>{{sortObj.name}}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            
        <div class="row" ng-if="!ohc.view.hasOrders">
            <div class="account-section-content col-xs-12 content-empty cat-container">
                <ycommerce:testId code="orderHistory_noOrders_label">
                    <spring:theme code="text.account.quoteHistory.noQuotes" />
                </ycommerce:testId>
            </div>
        </div>

    <div class="account-section-content " ng-if="ohc.view.hasOrders">
        <div class="account-orderhistory">
           <!--  <div>
            	<input class="col-xs-3" ng-model="ohc.view.pagination.search"/>
            	<input class="col-xs-3" type="button" value="search" ng-click="ohc.searchClick()"/>
            </div> -->
            <div class="account-orderhistory-pagination row">
                <div class="col-xs-8 col-md-6 mobile-search">
                    <div class="col-xs-8 col-md-8 search-div">
                        <input class="col-xs-12 search-box quote-search" placeholder="Enter customer, quote name or quote#" cat-enter="ohc.searchClick()" ng-model="ohc.view.pagination.search"/>
                    </div>
                    <div class= "col-xs-4 col-md-4 searchIconsBox clearfix">
                        <div class="searchBoxIcon text-center pull-left" ng-click="ohc.searchClick()"><img src="/_ui/responsive/cat/images/fill-169.png"/></div>
                        <div class="searchCancelIcon text-center pull-left" ng-click="ohc.clearSearch()"><img src="/_ui/responsive/cat/images/cross.png"/></div>
                    </div>
                </div>
                <div class="col-xs-4 pull-right col-md-4 pagination-top hidden-xs" ng-if="ohc.view.pagination.numberOfPages > 1">
                    <ul uib-pagination boundary-links="true" total-items="ohc.view.pagination.totalNumberOfResults" ng-model="ohc.view.pagination.currentPage" items-per-page="ohc.view.pagination.pageSize" ng-change="ohc.pageChanged()" class="pagination-sm pull-right" previous-text="&laquo;" next-text="&raquo;" first-text="" last-text="" max-size="3"></ul>
                    <!-- <nav:catIMOrderHistoryPagination top="true" msgKey="text.account.orderHistory.page" showCurrentPageInfo="true" hideRefineButton="true" supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}" searchUrl="${searchUrl}"  numberPagesShown="${numberPagesShown}"/> -->
                </div>
              </div>
            <div class="account-overview-table col-xs-12 content-empty" ng-if="ohc.quoteHistory.orders.length === 0">
                <div class="content">
                    <ycommerce:testId code="orderHistory_noOrders_label">
                        <spring:theme code="text.account.orderHistory.noResults" />
                    </ycommerce:testId>
                </div>
            </div>
            <div class="account-overview-table" ng-if="ohc.quoteHistory.orders.length !== 0" >
                <table class="orderhistory-list-table responsive-table hidden-xs">
                    <thead class="account-orderhistory-table-head responsive-table-head hidden-xs ">
                        <th class="col-md-1"><spring:theme code="text.account.quoteHistory.quoteNumber" /><span class="fa adj-fa" ng-class="{'fa-angle-up': ohc.view.sortMetaData.byCode.isReverseSort, 'fa-angle-down': !ohc.view.sortMetaData.byCode.isReverseSort}" ng-click="ohc.onSortClick('byCode')"></span></th>
                        <th class="col-md-2"><spring:theme code="text.account.quoteHistory.quoteName"/></th>
                        <th class="col-md-2"><spring:theme code="text.account.quoteHistory.customerName"/></th>
                        <th class="col-md-2"><spring:theme code="text.account.quoteHistory.quoteDate"/><span class="fa" ng-class="{'fa-angle-up': ohc.view.sortMetaData.byDate.isReverseSort, 'fa-angle-down': !ohc.view.sortMetaData.byDate.isReverseSort}" ng-click="ohc.onSortClick('byDate')"></span></th>
                        <th class="col-md-2"><spring:theme code="text.account.quoteHistory.dateModified"/><span class="fa adj-fa" ng-class="{'fa-angle-up': ohc.view.sortMetaData.byModifiedDate.isReverseSort, 'fa-angle-down': !ohc.view.sortMetaData.byModifiedDate.isReverseSort}" ng-click="ohc.onSortClick('byModifiedDate')"></span></th>
                        <th class="col-md-3"><spring:theme code="text.account.quoteHistory.quoteStatus"/><span class="fa adj-fa" ng-class="{'fa-angle-up': ohc.view.sortMetaData.byState.isReverseSort, 'fa-angle-down': !ohc.view.sortMetaData.byState.isReverseSort}" ng-click="ohc.onSortClick('byState')"></span></th>
                    </thead>
                    <tr class="responsive-table-item" ng-repeat="quote in ohc.quoteHistory.orders">
                        <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.orderHistory.orderNumber" /></td>
                        <td class="responsive-table-cell order-number" ng-click="ohc.onOrderClick(quote)">
                            <a href="${quoteDetailsUrl}{{quote.code}}" target="_self" class="responsive-table-link">
                                {{quote.code}}
                            </a>
                        </td>
                        <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.orderHistory.quoteName"/></td>
                        <td class="responsive-table-cell col-md-1">{{quote.name || "-"}}</td>
                        <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.orderHistory.customerName"/></td>
                        <td class="responsive-table-cell col-md-2">{{quote.quoteCustomer.name || "-"}}</td>
                        <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.orderHistory.quoteDate"/></td>
                        <td class="responsive-table-cell col-md-2">{{ohc.getDateFormat(quote.quoteCreatedTime)}}</td>
                        <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.quoteHistory.dateModified"/></td>
                        <td class="responsive-table-cell col-md-2">{{ohc.getDateFormat(quote.updatedTime)}}</td>
                        <td class="hidden-sm hidden-md hidden-lg"><spring:theme code="text.account.quoteHistory.quoteStatus"/></td>																
                        <td class="status toCapitalize col-md-3">
							{{ohc.quoteStatusMetaData[quote.state.code]}}
                        </td>
                    </tr>
                </table>
                <div class="col-xs-12 visible-xs">
                    <div class="mob-label" ng-repeat="quote in ohc.quoteHistory.orders">
                        <div class="row">
                            <div class="col-xs-12 label-head">
                                {{quote.quoteCustomer.name || "-"}}
                            </div>
                        </div>
                        <div class="row quote-id-name">
                            <div class="col-xs-6">
                                {{quote.name || "-"}}
                            </div>
                            <div class="col-xs-6">
                                <a href="${quoteDetailsUrl}{{quote.code}}" target="_self" class="responsive-table-link">
                                    {{quote.code}}
                                </a>
                            </div>
                        </div>
                        <div class="row quote-date">
                            <div class="col-xs-6">
                                <div class="head"><spring:theme code="text.account.quoteHistory.quoteDate"/></div>
                                {{ohc.getDateFormat(quote.quoteCreatedTime)}}
                            </div>
                            <div class="col-xs-6">
                                <div class="head"><spring:theme code="text.account.quoteHistory.dateModified"/></div>
                                {{ohc.getDateFormat(quote.updatedTime)}}
                            </div>
                        </div>
                        <div class="row">
                            <hr>
                            <div class="col-xs-12 cust-approval">
                                {{ohc.quoteStatusMetaData[quote.state.code]}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="visible-xs history-show-more quote-sjow-more" ng-if="ohc.quoteHistory.orders.length < ohc.view.pagination.totalNumberOfResults" ng-click="ohc.showMoreMobile('mobile')"><spring:theme code="cat.quote.history.show.more"/></div>
        <div class="account-orderhistory-pagination hidden-xs" ng-if="ohc.view.pagination.numberOfPages > 1">
            <ul uib-pagination boundary-links="true" total-items="ohc.view.pagination.totalNumberOfResults" ng-model="ohc.view.pagination.currentPage" items-per-page="ohc.view.pagination.pageSize" ng-change="ohc.pageChanged()" class="pagination-sm pull-right" previous-text="&laquo;" next-text="&raquo;" first-text="" last-text="" max-size="3"></ul>
            <!-- <nav:catIMOrderHistoryPagination top="false" msgKey="text.account.orderHistory.page" showCurrentPageInfo="true" hideRefineButton="true" supportShowPaged="${isShowPageAllowed}" supportShowAll="${isShowAllAllowed}" searchPageData="${searchPageData}" searchUrl="${searchUrl}"  numberPagesShown="${numberPagesShown}"/> -->
        </div>
    </div>
        </div>
    </div>
</div>
</div>

</template:page>