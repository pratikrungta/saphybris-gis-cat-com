<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>

<template:page pageTitle="${pageTitle}">

	<c:url value="/" var="homePageUrl" />

	<div class="error-page">

		<div class="row errorMsg">
			<div class="col-xs-12">
				<c:choose>
					<c:when
						test="${errorMessage ne null and errorMessage eq 'Product Not Found'}">
						<spring:theme code="cat.error.unapproved.product" />
					</c:when>
					<c:otherwise>
						<spring:theme code="cat.error.page.msg" />
					</c:otherwise>
				</c:choose>

			</div>
		</div>
		<div class="row errorImage">
			<div class="col-xs-12">
				<img src="/_ui/responsive/cat/images/cat.svg">
			</div>
		</div>
		<div class="row errorLinks">
			<div class="col-xs-12">
				<c:choose>
					<c:when
						test="${errorMessage ne null and errorMessage eq 'Product Not Found'}">
						<spring:theme code="cat.error.unapproved.product.msg" />
					</c:when>
					<c:otherwise>
						<spring:theme code="cat.error.page.pageNotFound" />
						<a href="${homePageUrl}"><spring:theme
								code="cat.error.page.homePage" /></a>
						<spring:theme code="cat.error.page.contactHelpDesk" />
					</c:otherwise>
				</c:choose>

			</div>
		</div>

	</div>

</template:page>