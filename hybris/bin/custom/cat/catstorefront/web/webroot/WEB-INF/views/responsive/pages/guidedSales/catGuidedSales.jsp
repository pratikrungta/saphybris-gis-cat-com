
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<template:page pageTitle="${pageTitle}">
<div class="cat-main-content">
	<div class="cat-container guided-sales-container" ng-app="cat-app" ng-controller="guidedSalesController as gsc" ng-cloak>
		<div class="row no-margin">
			<div class="header upperCase">GUIDED SELLING</div>
		</div>
		<div class="main-container" ng-if="!gsc.submitFlag">
			<div class="yourSelectionSection no-padding no-margin" ng-if="gsc.view.previous_question_answer_set.length">
				<div class="row mainHeading text-uppercase">Your Selection</div>
				<div class="row">
					<div class="col-md-3 questionTileSection" ng-repeat="qaRow in gsc.view.previous_question_answer_set">
                    
                    	<div class="tile" ng-click="gsc.showPrevious(qaRow)">
                    		<div class="showQuestion">{{qaRow.questionSet.question}}</div>
                        	<div class="showAnswer">{{qaRow.selectedOption.displayName}}</div>
                    	</div>
                </div>
				</div>
				
			</div>
			<div class="row question-container">
				<div class="question text-uppercase">
						<div class="pull-right">
								<button class="guided-sale-btn" 
								ng-click="gsc.nextQuestion(gsc.view.selected_option.suggestedFamily||'')" ng-class="{'disable': !gsc.view.selected_option.displayName}" 
								ng-disabled="!gsc.view.selected_option.displayName">Next</button>
							</div>
					{{gsc.guidedSalesData.question}}
					<div class="select-option">You must <span>select one option</span></div>
				</div>
				<div class="row options">
					<div class="no-padding option" ng-repeat="(idx, option) in gsc.guidedSalesData.options" ng-click="gsc.onOptionSelect(idx, option)">
						<div class="row cat-card-container">
						    <div class="col-xs-12 header-content">
						        <div class="row image">
						            <div class="col-xs-12">
						                <div class="product-image" ng-if="option.displayName!=='Yes'&& option.displayName!=='No'">
						                    <img ng-src="{{option.image || gsc.defaultDownloadImage}}" cat-fallback-image="">
						                </div>
						            </div>
						        </div>
						    </div>
						    <div class="col-xs-12">
						        <div class="body-content">
						            <div class="row no-margin main-header-row">
						                <div class="col-md-12 no-padding">
											<span ng-if="option == gsc.view.selected_option" class="radio-selected"></span>
											<span ng-if="option != gsc.view.selected_option" class="radio-not-selected"></span>
											<label class="display_answer_label hide_overflow" title="{{option.displayName}}">
												<input type="radio" class="product-radio-btn">
												{{option.displayName}}</label>
						                </div>
						            </div>
						        </div>
						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="finalPageContainer" ng-if="gsc.submitFlag">
			<div class="yourSelectionSection">
				<div class="row mainHeading">Your Selection</div>
				<div class="row">
					<div class="col-md-3 questionTileSection" ng-repeat="qaRow in gsc.view.previous_question_answer_set">
                    	<div class="tile" ng-click="gsc.showPrevious(qaRow)">
                    		<div class="showQuestion">{{qaRow.questionSet.question}}</div>
                        	<div class="showAnswer">{{qaRow.selectedOption.displayName}}</div>
                    	</div>
                    
                </div>
				</div>
				
				<div class="row recomendationSection">
					<div class="heading text-uppercase">Here are the products we recommend</div>
					<div class="text">Based on the information we collected, we think that these machines suit your requirement.</div>
					
				<a class="family-nav-bar" ng-repeat="(familyName, salesModels) in gsc.view.resultsData" href="javascript:void(0);" ng-click="gsc.goToFamilyName('family'+$index)">{{familyName}}</a>
			</div>
			<!-- ng-repeat goes in below div to loop over product families availble -->
		<div class="product-family-container">	
			<div id="family{{$index}}" ng-repeat="(familyName, salesModels) in gsc.view.resultsData">  
				<div class="productFamilySection">
					<div class="productFmaliyHeading row text-capitalize">{{familyName}}</div>
					<div class="productFamilyDesc row">The new {{familyName}}, with the 2017 product update, applies proven technologies systematically and strategically to meet your high expectations for reliability, productivity,  fuel efficiency.
					</div>
				</div>
				<div class="salesModelDisplay">
					<div class="row no-margin salesModelHeading text-uppercase">Sales Models</div>
					<div class="row no-margin cardSection">
						<div class="card-container col-md-3 no-padding" ng-repeat="salesModel in salesModels" ng-click="gsc.navigateToPlp(familyName, salesModel.name)">
							<div class="card">
								<div class="salesModelval">{{salesModel.name}}</div>
								<div class="row salesModelDetails">
									<div class="col-md-6 netPower">
										<div class="netPowerLabel">NET POWER</div>
										<div class="netPowerValue">{{salesModel.featureNetPower}}</div>
									</div>
									<div class="col-md-6 operatingWeight">
										<div class="operatingWeightLabel">OPERATING WEIGHT</div>
										<div class="operatingWeightValue">{{salesModel.featureOperatingWeight}}</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
				
		</div>
		<div class="contactUSSection">
				<div class="row heading">HAVE MORE QUESTIONS ?</div>
				<div class="row mailbox"><input name="mail" placeholder="Enter your question here, and we will contact you"></div>
				<div class="row emailSection">
					<span class="emailval">Email : janedoe@cat.com</span>
					<span class="contactUsButton pull-right">
							<button type="button" onclick="">CONTACT US</button>
					</span>
				</div>
			</div>
	</div>
</div>
</template:page>

