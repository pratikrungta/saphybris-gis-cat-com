<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="landingPage" tagdir="/WEB-INF/tags/responsive/landingPage" %>
<%@ taglib prefix="recentlyViewed" tagdir="/WEB-INF/tags/responsive/recentlyViewed" %>
<%@ taglib prefix="favoriteProducts" tagdir="/WEB-INF/tags/responsive/favoriteProducts" %>
<%@ taglib prefix="productCompare" tagdir="/WEB-INF/tags/responsive/productCompareDropdown" %>

<template:page pageTitle="${pageTitle}">

	<div ng-app="cat-app" class="landingPage cat-main-content cat-row" ng-controller="landingPageController as lpc" ng-init="lpc.roleFeatureInfoHomePage('${isUTVUser}','${isIM}','${isSP}');"> 

	<pop-up id="favoriteLimitReached" alert-header="You have reached the maximum limit of marking a product favorite. Please unmark some favorite products to continue" ok="DONE"></pop-up>
	
		<c:if test="${isIM and lowStockCount gt 0}">
			<landingPage:lowStockAlert />
		</c:if>
		<landingPage:autoComplete />
		<recentlyViewed:recentlyViewedComponent />
		<productCompare:productCompareDropdownComponent />
		<favoriteProducts:favoriteProductsComponent /> 


<!-- 		<div class="spreadChecValues cat-container" ng-controller="demoSpreadCheck as dsc">
				<h1>This is demo code</h1>
					<button ng-click="dsc.landingPage()">Landing Page</button>
					<button ng-click="dsc.comparePage()">Compare page</button>
					<button ng-click="dsc.listPage();">List page</button>
		</div> -->

	</div>
	
</template:page>
