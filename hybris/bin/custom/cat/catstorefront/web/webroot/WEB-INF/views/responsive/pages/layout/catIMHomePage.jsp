<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}">
	<div class="row">
	<div class="ct-container">
    <div class="rowLandingPage">
    <form id="manageOrdersForm" name="manageOrdersForm" method="GET" action="/search/view/">
        <div class="alertBox col-md-10 col-xs-push-1">
            <div class="alert-box-container">
                <div class="row">
                    <div class="col-xs-8 info">
                        <div class="glyphicon-warning-sign"></div>
                        <div><spring:theme code="cat.inventory.alert.text"/></div>
                        <input type="hidden" name="categoryCode" value="hexmi"/>
                        <div><spring:theme code="cat.inventory.alert.message" arguments="${lowStockCount}"/> </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="pull-right">
                            <input class="alertActionBox" id="manageOrders" value='Manage Orders' type='submit'/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
    <div class="rowLandingPage">
        <input type="text" class="searchText" placeholder="Search for a Product family, Sales model or Primary offering" />
        <button class="btn btnSearch" type="button">
            <i class="glyphicon glyphicon-search"></i>
        </button>
    </div>
    <div class="rowLandingPage">
        <div class="col-md-12 recentView">
            <span class="contentHeader"><spring:theme code="cat.im.home.recently.viewed"/></span>
            <div class="carousalContent col-md-12">
                <span class="navArrow pull-left glyphicon glyphicon-menu-left"></span>
                <div class="carousalData col-md-3">
                    <div class="carousalDataTitle">197-JWUX-123</div>
                    <div class="carousalDataImg">
                        <img class="pdtImg"></img>
                    </div>
                    <div class="carousalDataDetails">
                        <span class="pdtData">259D</span>
                        <span class="stockLvl">Stock Levels</span>
                        <span class="minStock">c</span>
                        <span class="minUnits">d</span>
                        <span class="maxStock">e</span>
                        <span class="maxUnits">f</span>
                        <span class="recom">g</span>
                        <span class="recomUnits">h</span>
                        <span class="current">i</span>
                        <span class="currentUnits">k</span>
                    </div>
                    <div class="carousalPageBreaker col-md-3"></div>
                    <div class="carousalAction"></div>
                </div>
                <div class="carousalData col-md-3">
                    hi2
                </div>
                <div class="carousalData col-md-3">
                    hi3
                </div>
                <span class="navArrow pull-right glyphicon glyphicon-menu-right"></span>
            </div>
        </div>
    </div>
    <div class="rowLandingPage">
        <div class="col-md-12 favpdts">
            <span class="contentHeader"><spring:theme code="cat.im.home.favorite.products"/></span>
            <div class="carousalContent col-md-12">
            	<span id="favBack" class="navArrow pull-left glyphicon glyphicon-menu-left"></span>
                <div class="carousalData fav pane col-md-3">
                    hi1
                </div>
                <div class="carousalData fav pane col-md-3">
                    hi2
                </div>
                <div class="carousalData fav pane col-md-3">
                    hi3
                </div>
                 <div class="carousalData fav col-md-3">
                    hi4
                </div>
                <div class="carousalData fav col-md-3">
                    hi5
                </div>
                <div class="carousalData fav col-md-3">
                    hi6
                </div>
                <div class="carousalData fav col-md-3">
                    hi7
                </div>
                <div class="carousalData fav col-md-3">
                    hi8
                </div>
                <span id="favFront" class="navArrow pull-right glyphicon glyphicon-menu-right"></span>
            </div>
        </div>
    </div>
</div>
</div>
<script>
	
    $(document).ready(function(){
    	 console.log(plp.view.searchResults);
          var arrFav=[];
          var cls=document.getElementsByClassName("fav"); 
		  for(var index=0;index<cls.length;index++){
			arrFav.push(cls[index].innerHTML);
		  }
		  $("#manageOrders").click(function(){
			  alert("a");
			  
		  });
		  
		  $("#favBack").click(function(){
              arrFav.push(arrFav.shift());
			  for(var index in arrFav){
        		cls[index].innerHTML=arrFav[index];
              }
		  })
		  $("#favFront").click(function(){
		  	 arrFav.unshift(arrFav.pop());
       		 for(var index in arrFav){
        	    cls[index].innerHTML=arrFav[index];
             }
		  })
    });

</script>
</template:page>
