<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix ="fn" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<template:page pageTitle="${pageTitle}">
    <cms:pageSlot position="Section1" var="feature">
        <cms:component component="${feature}" />
    </cms:pageSlot>
	<div> 


<div class="landingPage cat-container" ng-app="cat-app" ng-controller="landingPageController as lpc" ng-cloak>
    <div class="row landing-dropdown">
      <div class="header col-xs-10 col-xs-offset-1">Select a Product Family</div>
      <div id="product-family" class="btn-group col-xs-10 col-xs-offset-1 cat-dropdown" uib-dropdown is-open="lpc.productFamilyMenu.isOpen">
        <button type="button" class="btn col-xs-12 drop-down-button" uib-dropdown-toggle ng-disabled="disabled" ng-click="!lpc.productFamilyMenu.isOpen" >
          <div class="text">{{lpc.productFamilyMenu.selectedProductName || '-- Select --'}}</div>
           <div class="arrow pull-right"><img class="drop-down" src="/_ui/responsive/cat/images/drop-down.png" /> </div>
        </button>
        <ul class="dropdown-menu col-xs-12" uib-dropdown-menu role="menu" aria-labelledby="product-family">
        <c:forEach var="category" items="${categories}">
          <c:if test="${category.subCategories ne null }">
            <c:forEach var="subCat" items="${category.subCategories}">
              <li role="menuitem" ng-click="lpc.productFamilySelected('${subCat.code}','${subCat.name}')">${subCat.name}</li>
            </c:forEach>
          </c:if>
        </c:forEach>
        </ul>
      </div>
    </div>

    <div class="row landing-dropdown">
      <div class="header col-xs-10 col-xs-offset-1">Select a Sales Model</div>
      <div id="sales-model" class="btn-group col-xs-10 col-xs-offset-1 cat-dropdown" uib-dropdown is-open="lpc.salesModelMenu.isopen">
        <button id="single-button" type="button" class="btn col-xs-12 drop-down-button" uib-dropdown-toggle ng-disabled="disabled" ng-class=" lpc.salesModelMenu.isDisabled?'disabled':''">
          <div class="text">{{lpc.salesModelMenu.selectedModelName || '-- Select --'}}</div>
           <div class="arrow pull-right"><img class="drop-down" src="/_ui/responsive/cat/images/drop-down.png" /></div>
        </button>
        <ul class="dropdown-menu col-xs-12" uib-dropdown-menu role="menu" aria-labelledby="sales-model">
          <li role="menuitem" ng-repeat="salesModel in lpc.salesModelMenu.listOfSalesModel track by $index" ng-click="lpc.salesModelSelected(salesModel.code,salesModel.name)">{{salesModel.name}}</li>
        </ul>
      </div>
    </div>

    <div class="row landing-dropdown">
      <div class="header col-xs-10 col-xs-offset-1">Select a Retail Configuration</div>
      <div id="primary-offering" class="btn-group col-xs-10 col-xs-offset-1 cat-dropdown" uib-dropdown is-open="lpc.primaryOffering.isopen">
        <button id="single-button" type="button" class="btn col-xs-12 drop-down-button" uib-dropdown-toggle ng-disabled="disabled" ng-disabled="lpc.primaryOffering.isDisabled" ng-class=" lpc.primaryOffering.isDisabled?'disabled':''">
          <div class="text">{{lpc.primaryOffering.selectedPrimaryOffering || '-- Select --'}}</div>
           <div class="arrow pull-right"><img class="drop-down" src="/_ui/responsive/cat/images/drop-down.png" /></div>
        </button>
         <input type="hidden" id="selectedPrimaryOffering" value="{{lpc.primaryOffering.selectedPrimaryOffering}}"/>
         <ul class="dropdown-menu col-xs-12" uib-dropdown-menu role="menu" aria-labelledby="single-button">
          <li role="menuitem" ng-repeat="primaryOffering in lpc.primaryOffering.listOfPrimaryOffering track by $index" ng-click="lpc.primaryOfferingSelected(primaryOffering.code,primaryOffering.name)">{{primaryOffering.name}}</li>
        </ul>
      </div>
    </div>

    <div class="row landing-search-button">
      <div class="col-xs-10 col-xs-offset-1">
        <div class="btn-container">
          <button ng-disabled="!lpc.validForm?'disabled':''" onclick="javascript:search();"> SEARCH </button>
        </div>
      </div>  
    </div>
    </div>
    
</div>

<div class="footer-bottom"></div>
<head>
<script type="text/javascript">
function search()
{
	var searchParams=""//+document.getElementById('selectedPrimaryOffering').value;
	window.location="/search/"+searchParams;
}
</script>
</head>
</template:page>
