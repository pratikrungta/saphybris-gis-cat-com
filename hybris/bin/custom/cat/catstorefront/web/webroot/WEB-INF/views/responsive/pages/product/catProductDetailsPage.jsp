<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>

<template:page pageTitle="${pageTitle}">
	<div class="row">
		<div id="catImageModal" class="cat-modal" style="z-index:1000;">
			<span id="close">&times;</span>
			<img class="modal-content" id="catImage">
		</div>
		<product:productDetailsPanel />
	</div>
</template:page>
