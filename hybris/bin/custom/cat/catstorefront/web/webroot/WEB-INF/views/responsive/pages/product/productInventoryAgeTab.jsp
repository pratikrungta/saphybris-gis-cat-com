<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 

<div class="tabhead">
	<a href=""> ${fn:escapeXml(title)}</a> <span class="glyphicon"></span>
</div>
<div class="tabbody inventory-tab">
	<div class="container-lg">
		<div class="row">
			<div class="col-xs-12">
				<div class="tab-container">
					<product:productInventoryAgeTab product="${product}" />
				</div>
			</div>
		</div>
	</div>
</div>
