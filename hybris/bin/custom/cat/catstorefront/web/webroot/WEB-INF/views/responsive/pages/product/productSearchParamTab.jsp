<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>

<div class="tabhead">
	<a href="">  <spring:theme code="cat.plp.search"/> &nbsp; <spring:theme code="cat.plp.parameters"/></a> <span class="glyphicon"></span>
</div>
<div class="tabbody search-params-tab search-params-mobile">
	<div class="container-lg">
		<div class="row">
			<div class="col-xs-12">
				<div class="tab-container">
					<div class="row facet-param">
					<div class="col-xs-6 facet-label">
						<spring:theme code="product.refinment.component.product.family" />
					</div>
					<div class="col-xs-6 facet-value">
							<c:if test="${not empty product.categories}">
								<c:choose>
									<c:when
										test="${productCondition ne null and productCondition eq 'USED'}">
										<c:if test="${fn:length(product.categories)>1}">
											<c:forEach var="category" items="${product.categories}">
												<c:if test="${category.name ne 'USED'}">
												${fn:toLowerCase(category.name)}
												</c:if>
											</c:forEach>
										</c:if>
										
										<c:if test="${fn:length(product.categories)<=1}">
										${fn:toLowerCase(product.categories[0].name)}
										</c:if>
									</c:when>
									<c:otherwise>
										${fn:toLowerCase(product.categories[0].name)}
									</c:otherwise>
								</c:choose>
			
							</c:if>
							<c:if test="${fn:length(product.categories)==0}">
							-
							</c:if>
						</div>


					</div>
					<div class="row facet-param">
							<div class="col-xs-6 facet-label"><spring:theme code="product.refinment.component.sales.model"/></div>
							<div class="col-xs-6 facet-value"> ${salesModel}</div>
					</div>
					<c:if test="${not utilityProducts and productCondition ne null and productCondition ne 'USED'}">
						<div class="row facet-param">
							<div class="col-xs-6 facet-label"><spring:theme code="product.refinment.component.primary.offering"/></div>
							<c:set var="baseProductNumber" value="${product.baseProduct }"/>
							<div class="col-xs-6 facet-value"> ${primaryOffering} (${baseProductNumber})</div> 
						</div>
					</c:if>
				</div>
			</div>
		</div>
	</div>
</div>