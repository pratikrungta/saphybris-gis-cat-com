<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement"
	tagdir="/WEB-INF/tags/responsive/formElement"%>
<%@ taglib prefix="quote" tagdir="/WEB-INF/tags/responsive/quote"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ taglib prefix="previewComponents" tagdir="/WEB-INF/tags/responsive/quote/previewComponents"%>
<%@ taglib prefix="navigationComponents" tagdir="/WEB-INF/tags/responsive/quote/navigationComponents" %>

<template:page pageTitle="${pageTitle}">

	<div id="cart-page" class="cat-main-container" ng-app="cat-app">
		<div class="quotation-page-desktop-component" ng-controller="catQuotePageController as catPage" ng-cloak>
            <navigationComponents:mobileProgressBar />
			<div ng-hide="catPage.screenResizeService.resizeMetaData.isMobileScreen && catPage.quoteMetaData.mobileNav.currentPageNum!=1">
				<previewComponents:quoteQuoteDetailsComponent />
			</div>
			<div ng-hide="catPage.screenResizeService.resizeMetaData.isMobileScreen && catPage.quoteMetaData.mobileNav.currentPageNum!=2">
				<previewComponents:quoteCartDetailsComponent />
			</div>
			<div ng-hide="catPage.screenResizeService.resizeMetaData.isMobileScreen && catPage.quoteMetaData.mobileNav.currentPageNum!=3">
				<previewComponents:quoteCustomerDetailsComponent />
			</div>

			<navigationComponents:mobileNavButtons/>
			<c:url value="/quote/${quoteData.code}/export" var="exportQuoteURL" />
			<c:url value="/quote/${quoteData.code}/edit/?firstTimeEdit=false" var="editQuoteURL" />
			<div class="visible-xs"
				ng-if="catPage.quoteMetaData.mobileNav.currentPageNum===3">
					<div class="quotation-submit-container cat-container row cleaefix" 
						 ng-init="catPage.initButtonState('${enableEditButton}')">
					   <div class="col-xs-12 no-padding clearfix">
						   <c:choose>
							   <c:when test="${enableEditButton}">
							   <div class="col-xs-12 clearfix">
								   <a class="btn update-product-list" href="${editQuoteURL}" target="_self"><spring:theme code="cat.quote.details.editQuote" /></a>
								   <a href="${exportQuoteURL}" target="_blank" class="btn exportWordDoccument"><span class="exportWordDoccumenttext"><spring:theme code="cat.quote.details.exportDocument" /></span></a>
							   </div>
							   </c:when>
							   <c:otherwise>
							   
							   <div class="col-xs-12 clearfix">
									   <c:if test="${isQuoteHasUnapprovedProducts}">
										   <div class="error-message"><spring:theme code="cat.quote.details.editAndExport.unapprovedProduct.errorMsg" /></div>
									   </c:if>
									   <button class="btn update-product-list" ng-disabled="true"><spring:theme code="cat.quote.details.editQuote" /></button>
									   <c:choose>
										   <c:when test="${isQuoteHasUnapprovedProducts}">
											   <a class="btn exportWordDoccument" ng-disabled="true"><span class="exportWordDoccumenttext"><spring:theme code="cat.quote.details.exportDocument" /></span></a>
										   </c:when>
										   <c:otherwise>
											   <a class="btn exportWordDoccument" href="${exportQuoteURL}" target="_blank"><span class="exportWordDoccumenttext"><spring:theme code="cat.quote.details.exportDocument" /></span></a>
										   </c:otherwise>
									   </c:choose>
							   </div>
							   </c:otherwise>
						   </c:choose>
					   </div>
					</div>
					</div>
			
				<div class="row visible-xs no-gutters replicate-container-mb"
					 ng-if="catPage.quoteMetaData.mobileNav.currentPageNum===1">
                <div class="error-message" ng-if="!${enableReplicateButton}"><spring:theme code="cat.quote.details.replicate.unapprovedProduct.errorMsg1" /></div>
                <div class="error-message" ng-if="${enableReplicateButton} && ${isQuoteContainsUnpprovedProducts}"><spring:theme code="cat.quote.details.replicate.unapprovedProduct.errorMsg2" /></div>
                 <button class="cat-buttons replicate-quote-btn replicate-quote-mb-btn" 
                        ng-click="catPage.callChildLostProductPopupFn('${quoteData.code}');"
                        ng-disabled="!${enableReplicateButton}"
                        ng-class="{'primary-btn':(${enableReplicateButton}) ,'secondary-btn':(!${enableReplicateButton}),'disabled': (!${enableReplicateButton})}">
                    <spring:theme code="cat.quote.details.quote.replicate" />
                </button> 
            </div>


		</div>
	</div>
</template:page>