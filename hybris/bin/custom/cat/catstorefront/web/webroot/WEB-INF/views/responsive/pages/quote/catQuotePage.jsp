<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="quote" tagdir="/WEB-INF/tags/responsive/quote" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="quotationQuotePage" tagdir="/WEB-INF/tags/responsive/quotationQuotePage" %>
<%@ taglib prefix="quotationQuoteMobile" tagdir="/WEB-INF/tags/responsive/quotationQuotePage/mobileComponent" %>


<template:page pageTitle="${pageTitle}">

<div id="cart-page"  class="cat-row cat-main-content" ng-app="cat-app">
       <div class="quotation-page-desktop-component" 
       ng-controller="catQuotePageController as catPage">
            <div class="row cat-container">
                <div class="col-xs-12 page-header product-compare-header cat-container"><spring:theme code="cat.quote.main.heading.text" /></div>
            </div>
            <quotationQuoteMobile:quotationMobileProgressBar/>

            <quotationQuotePage:quotationQuoteDetailsComponent/>
            <quotationQuotePage:quotationCartDetailsComponent/>
            <quotationQuotePage:quotationCustomerDetailsComponent/>

            <quotationQuoteMobile:quotationMobileNavButtons/>
            
            <quotationQuotePage:submitQuotationComponent/>
       </div>
 </div>
</template:page>