<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 
<%@ taglib prefix="reorder" tagdir="/WEB-INF/tags/responsive/reorder" %>


<template:page pageTitle="${pageTitle}">
<div class="row cat-main-content">
<div id="im-reorder-container"  class="cat-container" ng-app="cat-app">
<div ng-controller="reorderProductsController as rpc" ng-init="rpc.cartType='${cartType}'" ng-cloak>
        <div class="overlay"  id="manageOrderMobileNavOverlay" data-sidebar-overlay>
            <!-- Loaded dynamically-->
        </div>
		<div data-sidebar id="manageOrderMobileNavMenu">
			<div  id="manageOrdersMobileMenu">
			</div>
          </div>
          

    <div class="row no-gutters header pagination-container top">
            <div class="rm-pddn-rgt pull-right visible-xs">
                    <button id="reorder-mobile-filters" class="pull-right cat-buttons secondary-btn"><img src="/_ui/responsive/cat/images/filter-icon.png"><spring:theme code="product.refinment.component.filters" /></button>
                </div>
        <div class="col-xs-6">
            <div class="search-text-header upperCase"><spring:theme code="im.reorder.search.results.reorder.products" /></div>
        </div>
      
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="hidden-xs col-sm-3"></div>
                    <div class="col-xs-12 col-sm-9">
                        <span class="hidden-xs pull-right" ng-show="(rpc.view.pageLoadingParams.pagedLoaded && (rpc.view.pagination.totalNumberOfResults > rpc.view.pagination.pageSize))">
                                    <ul uib-pagination boundary-links="true" total-items="rpc.view.pagination.totalNumberOfResults" 
                                    ng-model="rpc.view.pagination.currentPage" 
                                    items-per-page="rpc.view.pagination.pageSize" 
                                    ng-change="rpc.pageChanged()" class="pagination-sm pull-right" previous-text="&laquo;" 
                                    next-text="&raquo;" first-text="" last-text="" max-size="3"></ul> 
                            </span>
                        <!-- <p class="configMessage">All the Products are re-ordered at the configuration ID level. Only one Configuration can be re-ordered at a time.</p> -->
                    </div>
                </div>
            </div>
    	</div>
    </div>


    <div class="row no-gutters" ng-cloak>
            <div class="row cat-container page-loader" ng-show="!rpc.view.pageLoadingParams.pagedLoaded">
        <div class="cat-loading-spinner"></div>
    </div>

        <div class="col-xs-3 hidden-xs search-facets" ng-show="rpc.view.pageLoadingParams.pagedLoaded" >
            <reorder:reorderLeftRefinements />
        </div>
        <div class="col-xs-12 col-sm-9 search-results" >
            <reorder:reorderSearchResults/>
            <div class="visible-xs">
                    <a class="show-more-mobile" href="#" 
                    ng-show="rpc.view.searchResults.length < rpc.view.pagination.totalNumberOfResults" 
                    ng-click="rpc.loadMoreMobileResults(1)"><spring:theme code="cat.im.manage.order.reorder.search.results"/></a>
                </div>
        </div>
        
    </div>

        <div class="row no-gutters header pagination-container hidden-xs">
        <div class="col-xs-12"  ng-show="(rpc.view.pageLoadingParams.pagedLoaded && (rpc.view.pagination.totalNumberOfResults > rpc.view.pagination.pageSize))">
            <div class=" pull-right">
            <ul uib-pagination boundary-links="true" total-items="rpc.view.pagination.totalNumberOfResults" ng-model="rpc.view.pagination.currentPage" items-per-page="rpc.view.pagination.pageSize" ng-change="rpc.pageChanged()" class="pagination-sm pull-right" previous-text="&laquo;" next-text="&raquo;" first-text="" last-text="" max-size="3"></ul> 
            </div>
        </div>
        <div class="row pull-right view-cart-section">
            <span><spring:theme code="im.reorder.cart.total.units.added" />: {{rpc.getFormattedText(rpc.view.total)}}</span>
            <button class="view-cart-btn btn" ng-click="rpc.navigateToCart()" ng-disabled="!rpc.view.total"> <spring:theme code="im.reorder.view.cart" />  </button>
        </div>
    </div>
    <div class="row  view-cart-section no-gutters visible-xs">
        <div class="col-xs-6">
                <span class="mb-units-added"><spring:theme code="im.reorder.cart.total.units.added" /></span>
                <span class="mb-units">{{rpc.getFormattedText(rpc.view.total)}} <spring:theme code="im.reorder.search.results.units" /></span>
        </div>
            
            <button class="view-cart-btn btn col-xs-6" ng-click="rpc.navigateToCart()" ng-disabled="!rpc.view.total"> <spring:theme code="im.reorder.view.cart" />  </button>
        </div>

    </div>
</div>
</div>
    <storepickup:pickupStorePopup />

</template:page>
