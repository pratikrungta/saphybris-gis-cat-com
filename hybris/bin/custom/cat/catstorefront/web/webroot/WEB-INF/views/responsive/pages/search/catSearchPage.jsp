<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<template:page pageTitle="${pageTitle}">
<div class="cat-main-content">
<div class="row">
<div id="plp-container"  class="cat-container"  ng-app="cat-app">
<div id="pageLoader" class="page-loader"></div>
<c:set var="favoriteLimitReachedPopUp"><spring:theme code="cat.plp.favoriteProduct.limit.popup"/></c:set>
<c:set var="done"><spring:theme code="cat.plp.done"/></c:set>
<pop-up id="favoriteLimitReached" alert-header="${favoriteLimitReachedPopUp}" ok="${done}"></pop-up>

<div ng-controller="plpController as plp"  ng-init="plp.roleFeatureInfoSearch('${isUTVUser}','${isIM}','${isSP}');plp.setProductFamily('${categoryCode}');plp.setSalesModel('${salesModelId}');plp.setPrimaryOffering('${primaryOfferingId}');plp.userType('${userType}');plp.utilityProducts('${utilityProducts}');plp.init();">
	<div class="row cat-container page-loader" ng-show="!plp.view.pageLoadingParams.pagedLoaded">
		<div class="cat-loading-spinner"></div>
	</div>


	<div class="row  pagination-container top" ng-hide="plp.view.metadata.isUTVUser" ng-cloak>
		<div class="col-xs-6" ng-show="plp.view.pageLoadingParams.pagedLoaded">
			<div class="search-text-header upperCase" ng-show="plp.view.isUsedProduct" ><spring:theme code="cat.plp.usedEquipment"/></div>
			<div class="search-text-header upperCase" ng-hide="plp.view.isUsedProduct" ><spring:theme code="cat.plp.newEquipment"/></div>

			<div class="search-info" ng-show="plp.view.searchResults.length > 0"><spring:theme code="cat.plp.showing"/> {{plp.view.pagination.currentElementDisplay}} <spring:theme code="cat.plp.of"/> {{plp.view.pagination.totalItems}} <spring:theme code="cat.plp.results"/></div>

		</div>
		<div class="col-xs-6 hidden-xs"  ng-show="(plp.view.pageLoadingParams.pagedLoaded && (plp.view.pagination.totalItems > plp.view.pagination.elementsForCurrentPage) && plp.view.searchResults.length > 0)">
			<div class=" pull-right">
			<ul uib-pagination boundary-links="true" total-items="plp.view.pagination.totalItems" ng-model="plp.view.pagination.currentPage" items-per-page="plp.view.pagination.elementsForCurrentPage" ng-change="plp.pageChanged()" class="pagination-sm pull-right" previous-text="&laquo;" next-text="&raquo;" first-text="" last-text="" max-size="3"></ul>
			</div>
		</div>
		<div class="col-xs-6 rm-pddn-rgt visible-xs">
			<button id="plp-mobile-filters" class="pull-right cat-buttons secondary-btn"><img src="/_ui/responsive/cat/images/filter-icon.png"> <spring:theme code="cat.plp.filters.small"/></button>
		</div>
	</div>

	<div class="row no-gutters pagination-container top" ng-hide="!plp.view.metadata.isUTVUser" ng-cloak>
		<div class="col-xs-6">
			<div class="search-text-header upperCase"><spring:theme code="cat.plp.allProducts"/></div>
		</div>
	</div>


	<div class="row  no-gutters plp-list ng-hide" ng-show="plp.view.pageLoadingParams.pagedLoaded" ng-cloak>
		<div class="col-xs-3 hidden-xs" id="plp-facet-container" ng-hide="plp.view.metadata.isUTVUser">
			<cms:pageSlot position="ProductLeftRefinements" var="feature" element="div" class="search-list-page-left-refinements-slot">
				<cms:component component="${feature}" element="div" class="search-list-page-left-refinements-component"/>
			</cms:pageSlot>
		</div>
		<div class="col-xs-12" id="plp-results-container" ng-class="(plp.view.metadata.isUTVUser)?'utv-user col-sm-12':'col-sm-9'">
			<c:if test="${userType eq 'IM' || userType eq 'UTV' }">
				<div class="row" ng-if="!(plp.view.metadata.isUTVUser || plp.view.isUsedProduct)">
					<div class="col-xs-12 lane-button-container hidden-md hidden-lg">
						<button ng-class="{laneEnabled:plp.laneType==='All'}" ng-click="plp.orderingWindowForLanes('All')">All Lanes</button>
						<button ng-class="{laneEnabled:plp.laneType==='Lane1'}"  ng-click="plp.orderingWindowForLanes('Lane1')">1<span class="lane-color lane-1"></span></button>
						<button ng-class="{laneEnabled:plp.laneType==='Lane2'}"  ng-click="plp.orderingWindowForLanes('Lane2')">2 <span class="lane-color lane-2"></span></button>
						<button ng-class="{laneEnabled:plp.laneType==='Lane3'}"  ng-click="plp.orderingWindowForLanes('Lane3')">3 <span class="lane-color lane-3"></span></button>
					</div>
					<div class="col-xs-12 lane-button-container visible-md visible-lg">
						<button ng-class="{laneEnabled:plp.laneType==='All'}" ng-click="plp.orderingWindowForLanes('All')">All Lanes</button>
						<button ng-class="{laneEnabled:plp.laneType==='Lane1'}"  ng-click="plp.orderingWindowForLanes('Lane1')">Lane 1<span class="lane-color lane-1"></span></button>
						<button ng-class="{laneEnabled:plp.laneType==='Lane2'}"  ng-click="plp.orderingWindowForLanes('Lane2')" uib-tooltip="Base Orders" tooltip-placement="top" tooltip-trigger="mouseenter" >Lane 2 <span class="lane-color lane-2"></span></button>
						<button ng-class="{laneEnabled:plp.laneType==='Lane3'}"  ng-click="plp.orderingWindowForLanes('Lane3')" uib-tooltip="Allocated only" tooltip-placement="top" tooltip-trigger="mouseenter" >Lane 3 <span class="lane-color lane-3"></span></button>
					</div>
					<div class="col-xs-12 no-padding" ng-if="plp.laneType!=='Lane1'">
						<div class="lane-window-error-msg" ng-if="plp.laneType==='All'">{{plp.allLanesMsg}}</div>
						<div class="lane-window-error-msg" ng-if="plp.laneType==='Lane2'">{{plp.lane2Msg}}</div>
						<div class="lane-window-error-msg" ng-if="plp.laneType==='Lane3'">{{plp.lane3Msg}}</div>
					</div>
				</div>
			</c:if>
			<cms:pageSlot position="SearchResultsListSlot" var="feature" element="div" class="plp-results">
				<cms:component component="${feature}" element="div"/>
			</cms:pageSlot>
		</div>
	</div>
	<div class="row clearfix no-gutters pagination-container">
		<div class="pull-right col-md-4 text-right hidden-xs" ng-show="(plp.view.pageLoadingParams.pagedLoaded && (plp.view.pagination.totalItems > plp.view.pagination.elementsForCurrentPage) && plp.view.searchResults.length > 0)">
			<ul uib-pagination boundary-links="true" total-items="plp.view.pagination.totalItems" ng-model="plp.view.pagination.currentPage" items-per-page="plp.view.pagination.elementsForCurrentPage" ng-change="plp.pageChanged()" class="pagination-sm pull-right" previous-text="&laquo;" next-text="&raquo;" first-text="" last-text="" max-size="3"></ul>
		</div>
		<div class="additional-charges col-md-5 col-sm-5  col-md-push-3 col-sm-push-3" ng-show="plp.view.pageLoadingParams.pagedLoaded && plp.view.isUsedProduct">
				<spring:theme code="cat.plp.price.usedPLP"/>
		</div>
	</div>
	<div class="visible-xs">
		<div class="show-more-mobile" ng-click="plp.loadMoreMobileResults()" ng-show="plp.view.searchResultsMobile.length < plp.view.pagination.totalItems"><spring:theme code="cat.plp.showMore"/></div>
	</div>
	
	<div class="overlay" id="catSearchMobileNavOverlay" data-sidebar-overlay></div>
		<div data-sidebar id="catPLPMobileNavMenu">
			<div  id="catPLPFilterMenu">
			</div>
		  </div>
	</div>
</div>
</div>
</div>
	<storepickup:pickupStorePopup />
</template:page>