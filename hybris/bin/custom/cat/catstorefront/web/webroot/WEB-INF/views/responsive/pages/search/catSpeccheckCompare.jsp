<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="speckcheckProductCompare" tagdir="/WEB-INF/tags/responsive/speckcheckProductCompare" %>
<template:page pageTitle="${pageTitle}">
    <div ng-app="cat-app">
        <speckcheckProductCompare:speckcheckProductCompareComponent/>
        <speckcheckProductCompare:speckcheckProductCompareMobileComponent/>
    </div>
</template:page>