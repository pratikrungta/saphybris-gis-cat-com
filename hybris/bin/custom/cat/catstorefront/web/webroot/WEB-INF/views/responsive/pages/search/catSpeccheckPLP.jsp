<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="speckcheckPLP" tagdir="/WEB-INF/tags/responsive/speckcheckProductList" %>

<template:page pageTitle="${pageTitle}">
    <div class="cat-row">
        <div id="compare-plp-container" class="cat-main-content" ng-app="cat-app">
            <speckcheckPLP:speckcheckProductListComponent/>
            <speckcheckPLP:speckcheckProductListMobileComponent/>
        </div>
    </div>
</template:page>