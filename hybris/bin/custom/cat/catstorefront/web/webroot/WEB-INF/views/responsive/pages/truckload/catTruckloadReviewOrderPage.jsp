<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 
<%@ taglib prefix="truckload" tagdir="/WEB-INF/tags/responsive/truckload" %>

<template:page pageTitle="${pageTitle}">
    <truckload:truckloadReviewOrderComponent/>
</template:page>
