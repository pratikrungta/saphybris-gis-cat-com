var catApp = angular.module('cat-app', ['rzModule','ui.bootstrap','angularjs-dropdown-multiselect','ngSanitize','ngTouch']);

catApp.run(['catService', '$location','getUrlParamFactory',
function(catService, $location,getUrlParamFactory) {
    getUrlParamFactory.storeUrlParams($location.search());
    catService.selectedProductFamilyCode = $location.search().categoryCode || "";
    catService.selectedSalesModelCode = $location.search().salesModelId || ""
    catService.selectedPrimaryOfferingCode = $location.search().primaryOfferingId || "";
    if (!!$location.search().pdcFlag) {
        catService.plp.isPDCChecked = $.parseJSON($location.search().pdcFlag);
    } else {
        catService.plp.isPDCChecked = false;
    }

    if (!!$location.search().pdcFlag) {
        catService.plp.isMFUChecked = $.parseJSON($location.search().mfuFlag);
    } else {
        catService.plp.isMFUChecked = false;
    }

    if (!!$location.search().pdcFlag) {
        catService.plp.isWarehouseChecked = $.parseJSON($location.search().wsFlag);
    } else {
        catService.plp.isWarehouseChecked = false;
    }

    if (!!$location.search().isUsed && $location.search().isUsed=="true") {
        catService.plp.isUsed = Boolean($.parseJSON($location.search().isUsed));
    } else {
        catService.plp.isUsed = false;
    }

    if(!!$location.search().page && !!Number($location.search().page)){
        catService.plp.currentPage = Number($location.search().page);
    }else{
        catService.plp.currentPage = 0;
    }

}]);

catApp.config(['$locationProvider', '$httpProvider',
    function($locationProvider, $httpProvider) {
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });

        delete $httpProvider.defaults.headers.common["X-Requested-With"]
    }
]);
