catApp.controller('accessoriesController', ['catEndpointService','$uibModal','$scope', 'constantService', '$rootScope', 'httpService', '$window', 'getUrlParamFactory', '$filter','properMessageService', function(catEndpointService,$uibModal,$scope, constantService, $rootScope, httpService, $window, getUrlParamFactory, $filter,properMessageService) {
    var vm = this, urlParams= getUrlParamFactory.getUrlParams();
    vm.tourFourthElement = false;
    Tour.prototype._onScroll = function(){}
    var tourDesktop = new Tour(constantService.accessoriesTourDesktopObject);
    var tourMobile = new Tour(constantService.accessoriesTourMobileObject);
    vm.originalAccessoriesData = null;
    vm.view = {
        accessoriesData: null,
        CUVDropDownData: [],
        selecteModel: 'Select a model',
        select: 'Select',
        accordianOpen: {},
        totalPrice: 0,
        totalCount: 0,
        CUVModels: [],
    };
    vm.view.filterCUVModel = vm.view.selecteModel;

    vm.loadTourData = function(userMailId) {
        constantService.setAccessoriesTourUserId(userMailId);
        constantService.setAccessoriesTourMobileUserId(userMailId);
        if(angular.element(window).width() < 900 && 
          (localStorage.getItem("accessoriesTourDismissMobile"+ userMailId) == null || localStorage.getItem("accessoriesTourDismissMobile"+ userMailId) == 'false') &&
          (localStorage.getItem("accessoriesTourMobileUserId") == null || localStorage.getItem("accessoriesTourMobileUserId") != userMailId)){
            vm.tourFourthElement = true;
            tourMobile.init();
            tourMobile.start();
            tourMobile.restart();
        } else if(angular.element(window).width() > 900 && 
          (localStorage.getItem("accessoriesTourDismiss"+ userMailId) == null || localStorage.getItem("accessoriesTourDismiss"+ userMailId) == 'false') &&
          (localStorage.getItem("accessoriesTourUserId") == null || localStorage.getItem("accessoriesTourUserId") != userMailId)) {
            vm.tourFourthElement = true;
            tourDesktop.init();
            tourDesktop.start();
            tourDesktop.restart();
        }
    }

    vm.init = function(){  
        $(window).unbind('tourClosed').bind('tourClosed', function() {
            vm.tourFourthElement = false;
        });
        if(urlParams.selecteModel){
            vm.view.filterCUVModel = urlParams.selecteModel;
        }
        httpService.get(catEndpointService.getAccessoriesCUVModelsURL).then(function(response){
            vm.view.CUVDropDownData = response.data || [];
            vm.view.CUVDropDownData.unshift(vm.view.selecteModel);
        });
        httpService.get(catEndpointService.getAccessoriesDataURL).then(function(response){
            var i = 0;
            vm.view.accessoriesData = response.data;
            vm.originalAccessoriesData = JSON.parse(JSON.stringify(vm.view.accessoriesData));
            _.map(vm.view.accessoriesData, function(v, k){
                vm.view.accordianOpen[k] = (i == 0) ? true : false;
                i++;
                _.map(v, function(obj, idx){
                    obj.price = obj.price || {
                                value: 0,
                                formattedValue: "$0.00"
                            };
                    if(!obj.lineItems || !obj.lineItems.length){
                        obj.lineItems = [
                            vm.getNewLine(obj)
                        ]
                    } else {
                        obj.lineItems = _.map(obj.lineItems, function(v){
                            vm.refereshCUVModelsDropDown(k, idx);
                            return v;
                        })
                    }
                    
                    obj.index = idx;
                    obj.maxQuantity = obj.compatibleModels.length;
                    vm.view.CUVModels.push(obj.compatibleModels);
                });
            });
            vm.originalAccessoriesData = JSON.parse(JSON.stringify(vm.view.accessoriesData));
            vm.view.CUVModels = _.uniq(_.flatten(vm.view.CUVModels));
            vm.view.CUVModels.unshift(vm.view.selecteModel);
            vm.updateTotals();
            // vm.view.CUVDropDownData = vm.view.CUVModels;
        });
    };

    vm.getNewLine = function(obj){
        var listModels = angular.copy(obj.compatibleModels);
        listModels.unshift(vm.view.select);
        return  {
                    quantity: 0,
                    totalPrice: 0,
                    formattedTotalPrice: "$0.00",
                    modelNumber: vm.view.select,
                    listModels: listModels
                };
    };

    vm.updateLineItemCUV = function(key, index, lineItemIndex, CUVModelSelected){
        vm.view.accessoriesData[key][index].lineItems[lineItemIndex].modelNumber = CUVModelSelected;
        // if (CUVModelSelected == vm.view.select) {
            vm.view.accessoriesData[key][index].lineItems[lineItemIndex].quantity = 0;
            vm.view.accessoriesData[key][index].lineItems[lineItemIndex].totalPrice = 0;
            vm.view.accessoriesData[key][index].lineItems[lineItemIndex].formattedTotalPrice = "$0.00";
        // }
        vm.refereshCUVModelsDropDown(key, index);
        vm.updateTotals();
    };

    vm.refereshCUVModelsDropDown = function(key, index){
        var selectedModels = _.map(vm.view.accessoriesData[key][index].lineItems, function(v){
            return v.modelNumber;
        });
        var diffModels = _.differenceWith(vm.view.accessoriesData[key][index].compatibleModels, selectedModels);
        diffModels.unshift(vm.view.select);
        _.map(vm.view.accessoriesData[key][index].lineItems, function(v, i){
            v.listModels = angular.copy(diffModels);
        });
        vm.originalAccessoriesData[key][index].lineItems = angular.copy(vm.view.accessoriesData[key][index].lineItems);
    };

    // vm.onAccordianClick = function(key){
    //     vm.view.accordianOpen[key] = !vm.view.accordianOpen[key];
    // }

    vm.updateQuantity = function(key, index, lineItemIndex, operation){
        if (vm.disableQuantityControls(vm.view.accessoriesData[key][index],lineItemIndex)) {
            return false;
        }
        vm.view.accessoriesData[key][index].lineItems[lineItemIndex].quantity = Number(vm.view.accessoriesData[key][index].lineItems[lineItemIndex].quantity);
        switch(operation){
            case 'decrement':
                if(vm.view.accessoriesData[key][index].lineItems[lineItemIndex].quantity > 0){
                    vm.view.accessoriesData[key][index].lineItems[lineItemIndex].quantity -= 1;
                }
                break;
            case 'increment':
                vm.view.accessoriesData[key][index].lineItems[lineItemIndex].quantity += 1;
                break;
        }
        if(constantService.maxOrderLimit < vm.view.accessoriesData[key][index].lineItems[lineItemIndex].quantity){
            vm.view.accessoriesData[key][index].lineItems[lineItemIndex].quantity = constantService.maxOrderLimit;
        }
        vm.view.accessoriesData[key][index].lineItems[lineItemIndex].totalPrice = (vm.view.accessoriesData[key][index].lineItems[lineItemIndex].quantity * vm.view.accessoriesData[key][index].price.value) || 0;
        vm.view.accessoriesData[key][index].lineItems[lineItemIndex].formattedTotalPrice = this.getPriceFormat(vm.view.accessoriesData[key][index].lineItems[lineItemIndex].totalPrice);
        vm.originalAccessoriesData[key][index].lineItems = angular.copy(vm.view.accessoriesData[key][index].lineItems);
        vm.updateTotals();
    };

    vm.updateTotals = function(){
        vm.view.totalCount = 0;
        vm.view.totalPrice = 0;
        _.map(vm.view.accessoriesData, function(v, k){
            _.map(v, function(obj, idx){
                _.map(obj.lineItems, function(val, idx){
                    vm.view.totalCount += val.quantity;
                    vm.view.totalPrice += val.totalPrice;
                });
            });
        });
    };

    vm.addLineItem = function(key, index){
        vm.view.accessoriesData[key][index].lineItems.push(vm.getNewLine(vm.view.accessoriesData[key][index]));
        vm.refereshCUVModelsDropDown(key, index);
    };

    vm.getPriceFormat = function(price){
        return "$"+($filter('number')(price, 2));
    };

    vm.slectedFilter = function(filterCUVModel){
        vm.view.filterCUVModel = filterCUVModel;
        if(filterCUVModel == vm.view.selecteModel){
            vm.view.accessoriesData = JSON.parse(JSON.stringify(vm.originalAccessoriesData));
        }else{
            _.map(vm.originalAccessoriesData, function(v, i){
                vm.view.accessoriesData[i] = _.filter(v, function(obj){return obj.compatibleModels.indexOf(filterCUVModel) != -1});
            });
        }
    }

    vm.disableQuantityControls = function(index,lineItemIndex){

        if((index.compatibleModels.length === vm.view.CUVDropDownData.length-1)) {
            return false;
        } else if (!isNaN(lineItemIndex)  && index.lineItems[lineItemIndex].modelNumber !== vm.view.select){
            return false;
        }
        return true;
    }

    vm.showRemovebcpPopup = function(cartType){
        $scope.reviewbcpRemovePopupText = properMessageService.cartLostMessage(cartType);
        vm.removeBcpModalInstance = $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: '/_ui/responsive/cat/templates/removebcpPopup.html',
            scope: $scope,
            size: 'remove-bcp-item',
        });
    }
    
    $scope.closeRemovebcpPopup = function() {
        vm.removeBcpModalInstance.close();
    };
    $scope.deletebcpProducts = function(){
        //ACC.minicart.refreshMinicartCountNum();
        vm.removeBcpModalInstance.close();
        vm.accReviewOrderAct();
    }

    vm.reviewOrder = function(cartType){
        httpService.get(catEndpointService.showPopupAccessoriesURl).then(function(res){
            if(!res.data){
                vm.showRemovebcpPopup(cartType);
            }
            else{
                vm.accReviewOrderAct(); 
            }
        });
    }

    vm.accReviewOrderAct = function() {
        var accRequestObj = [];
        _.map(vm.view.accessoriesData, function(v, k){
            _.map(v, function(i) {
                _.map(i.lineItems, function(obj){
                    if(obj.quantity && (obj.modelNumber !== vm.view.select || i.compatibleModels.length === vm.view.CUVDropDownData.length-1)){
                        accRequestObj.push({
                            "quantity": obj.quantity,
                            "selectedCompatibleModel": (obj.modelNumber == vm.view.select) ? "All Models" : obj.modelNumber,
                            "code": i.code
                        })
                    }
                });
            });
        });
        //httpService.get(catEndpointService.showPopupAccessoriesURl).then(function(res){

            httpService.postJsonData(catEndpointService.setAccessoriesCartData, {"accessories": accRequestObj}).then(function(response) {
                if (response.status == 200) {
                   $window.location.href = (vm.view.filterCUVModel == vm.view.selecteModel) ? '/cart/reviewAccessoriesOrder' : '/cart/reviewAccessoriesOrder?selecteModel='+vm.view.filterCUVModel;
                }
            }, function(error) {
                console.log("caused error: ", error);
            });
        //});
    }

    vm.init();

}]);
