catApp.controller('reviewAccessoriesController', ['catEndpointService', '$scope', 'constantService', '$rootScope', 'httpService', '$window', 'getUrlParamFactory', '$filter','$uibModal','reviewOrderValidation','screenResizedService', function(catEndpointService, $scope, constantService, $rootScope, httpService, $window, getUrlParamFactory, $filter, $uibModal,reviewOrderValidation,screenResizedService) {
    var vm = this, urlParams= getUrlParamFactory.getUrlParams();
    vm.hybrisValidateError = false;
    vm.repeatError = false;
    vm.quantityError = false;
    vm.buttonCheck = true;
    vm.madatoryError = false;
    vm.disableSubmit = false;
    vm.totalLineItems = 0;
    vm.view = {
        accessoriesData: [],
        totalQuantity: 0,
        totalPrice: 0,
        shippingAddress: [],
        processingRequest: false,
    };
    vm.duplicatePONumbers = [];
    vm.reviewButtonClicked = false;
    vm.isMobile=screenResizedService.metaInfo.isMobile;
    vm.triggerValidations=function(){
        reviewOrderValidation.notify(reviewOrderValidation.reviewOrderPageSubscriptionId
            ,function(addressValidationResultArray){
                        var i = addressValidationResultArray.length;
                            while(i--){
                                if(addressValidationResultArray[i].isAddressEmpty||vm.isPoNumberEmpty()){
                                    vm.view.userCertified=false;
                                    vm.view.isUserCheckedCertify=true;
                                    // $('#certifyLabel').addClass('shaky-effect');
                                    // $timeout(function(){
                                    //     $('#certifyLabel').removeClass('shaky-effect');
                                    // },2000);

                                    return;
                                }
                            }
                    });
    }
    vm.init = function(){
        httpService.get(catEndpointService.getAccessoriesReviewOrderDetails).then(function(response){
            vm.buildReviewData(response);
        });
        vm.fetchShippingAddress();
    };

    vm.buildReviewData = function(response){
        vm.totalLineItems = 0;
        vm.view.accessoriesData = response.data.accessories;
        _.map(vm.view.accessoriesData, function(val){
            val.poNumber = "";
            val.isValid = true;
            vm.totalLineItems += val.length; 
        });
        vm.view.totalPrice = response.data.totalPrice;
        vm.view.totalQuantity = response.data.totalQuantity;
    }

    vm.getPriceFormat = function(price){
        return "$"+($filter('number')(price, 2));
    };

    vm.fetchShippingAddress = function() {
        httpService.get(catEndpointService.dealerShippingAddress).then(function(response) {
            if (response.status == 200) {
                vm.view.shippingAddress = response.data.allShippingAddressList || [{}];
                vm.productCompareDropDownLabel = {};
            }
        }, function(error) {

        });
    }
    $scope.updateDropDownValue = function(value) {
        vm.productCompareDropDownLabel = value;
    };

    vm.submitOrder = function(){
        vm.reviewButtonClicked = true;
        vm.triggerValidations();
        if(!vm.view.userCertified){
            return;
        }
        if(vm.duplicatePONumbers.length || vm.isPoNumberEmpty() || !vm.productCompareDropDownLabel.id){
            return false;
        }
        vm.view.processingRequest = true;
        httpService.postJsonData(catEndpointService.validateAccessoriesPONumbers, vm.serializePoNumbersData()).then(function(response) {
            if(response.data.length){
                vm.view.errorPos = response.data[0].purchaseOrderNumberSet;
                vm.view.processingRequest = false;
                if(!response.data[0].duplicatePONumbers){
                    $("#orderErrorMessage").show();
                    _.map(vm.view.accessoriesData, function(val){
                        val.isValid = !vm.view.errorPos.includes(val.poNumber.toUpperCase());
                    });    
                }
            }else {
                window.onbeforeunload = null;
                httpService.postJsonData(catEndpointService.placeOrderForAccessories, vm.serializeAddressInfo()).then(function(response) {
                    $scope.price = response.data.totalPrice.formattedValue;
                    $scope.quantity = response.data.totalUnitCount;
                    $scope.splitOrderCount = response.data.splitOrderCount;     
                    vm.showRemoveModalInstance = $uibModal.open({
                        backdrop: 'static',
                        ariaLabelledBy: 'modal-title',
                        ariaDescribedBy: 'modal-body',
                        templateUrl: '/_ui/responsive/cat/templates/accessoriesOrderSuccessPopUp.html',
                        scope: $scope,
                        size: 'quotation-remove-item',
                    });
                });
            }
        });
    }
    vm.isPoNumberEmpty = function(){
        var poNumber = _.chain(vm.view.accessoriesData)
            .filter(function(accessoriesObj){return accessoriesObj.poNumber != ''})
            .value();
        return (poNumber.length !== (Object.keys(vm.view.accessoriesData)).length);
    }

    vm.serializePoNumbersData = function(){
        return _.map(vm.view.accessoriesData, function(val, key){
            return {
                "salesModelId": key,
                "purchaseOrderNumberSet": [
                    val.poNumber
                ],
                "purchaseOrderErrorSet": {
                    "errorMsg": "",
                    "errorNos": []
                }
            };
        });
    };

    vm.serializeAddressInfo = function(){
        return {
            "selectedShippingAddressPK" : vm.productCompareDropDownLabel.id,
            "comments" : ""
        }
    }

    $scope.popUpClose = function() {
        $window.location.href = '/';
    }
    $scope.viewOrderHistory = function(){
        $window.location.href = '/my-account/cat/orders';
    }

    vm.navigateToAccessories = function(){
        $window.location.href = urlParams.selecteModel ? '/accessories?selecteModel='+urlParams.selecteModel : '/accessories';
    }

    vm.removeLineItem = function (obj) {
        if(vm.totalLineItems == 1){
            vm.showRemovePopup(obj);
        }else {
            $scope.deleteCartProducts(obj);
        }
    }

    vm.showRemovePopup = function(obj) {
        $scope.displayRemoveText = constantService.accessories.lastProductRemove;
        $scope.removeCartProductIndex = obj;
        vm.showRemoveModalInstance = $uibModal.open({
            backdrop: 'static',
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: '/_ui/responsive/cat/templates/quotationRemoveItemPopup.html',
            scope: $scope,
            size: 'quotation-remove-item',
        });
    };

    $scope.deleteCartProducts = function(obj){
        delCartEP = catEndpointService.deleteCartAccessories + "?entryNumber=" +obj.entryNumber;
        httpService.get(delCartEP).then(function(response) {
            ACC.minicart.refreshMinicartCountNum();
            if(response.data.totalQuantity > 0){
                vm.buildReviewData(response);
            } else {
                window.onbeforeunload = null;
                vm.navigateToAccessories();
            }
        }, function(error) {
           console.log(error); 
        });
    };
    
    $scope.close = function() {
        vm.showRemoveModalInstance.close();
    }

    vm.onPoEnter = function(key){
        vm.view.accessoriesData[key].isValid = true;
        vm.duplicatePONumbers = _.chain(vm.view.accessoriesData)
                            .filter(function(accessoriesObj){return accessoriesObj.poNumber != ''})
                            .map(function(accessoriesObj){return accessoriesObj.poNumber})
                            .filter(function (value, index, iteratee) {
                               return _.includes(iteratee, value, index + 1);
                            })
                            .value();
        window.onbeforeunload = function(){
            return "Are you sure , you wanted to navigate out of Page?"
        }
    }
    vm.init();

}]);