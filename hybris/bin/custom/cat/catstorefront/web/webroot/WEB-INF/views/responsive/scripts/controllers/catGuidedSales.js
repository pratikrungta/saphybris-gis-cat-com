catApp.controller('guidedSalesController', [
	'$scope',
	'$window',
	'catEndpointService',
	'httpService',
	'constantService',
	'screenResizedService',
	'$timeout',
	function($scope, $window, catEndpointService, httpService, 
			constantService, screenResizedService,$timeout) {
		var vm = this,
			backgroundBannerMap={
				"Mini Hydraulic Excavators":"url(/_ui/responsive/cat/images/guidedSalesImages/min-excavators-banner.png)",
				"Center-Pivot Backhoe Loader":"url(/_ui/responsive/cat/images/guidedSalesImages/backhoe-banner.png)",
				"Compact Track Loaders":"url(/_ui/responsive/cat/images/guidedSalesImages/compact-banner.png)",
				"Skid steer Loaders":"url(/_ui/responsive/cat/images/guidedSalesImages/skidSteersLoader-banner.png)",
				"Compact Wheel Loaders":"url(/_ui/responsive/cat/images/guidedSalesImages/compact-wheel-loader-banner.png)"
			};
		vm.view = {
			"previous_question":[],
			"previous_question_set":[],
			"previous_question_answer_set":[],
 			"previously_selected":[],
			"selected_option": {},
		};
		vm.guidedSalesData = {}
		vm.salesModelData = {}
		vm.defaultDownloadImage = constantService.defaultDownloadImage;
		vm.submitFlag=false;

		vm.init = function() {
			httpService.get(catEndpointService.guidedSalesUrl).then(function(response) {
				vm.guidedSalesData = response.data.questionSet;
				vm.submitFlag = false;
			}, function(error){
				console.log(error);
        	});
		};

		vm.onOptionSelect = function(idx, option) {
			vm.view.selected_option = option;
		};

		vm.nextQuestion = function(isSubmit) {
			vm.view.previously_selected.push(vm.view.selected_option);
			vm.view.previous_question_set.push(vm.guidedSalesData);
			vm.view.previous_question_answer_set.push({"questionSet":vm.guidedSalesData,
														"selectedOption":vm.view.selected_option
											});
			vm.guidedSalesData = vm.view.selected_option.questionSet;
			if(isSubmit){
				vm.submit();
				return;
			}
			vm.view.selected_option="";
		};

		vm.showPrevious = function(previousQuestionBlock){
			if(previousQuestionBlock){
				vm.submitFlag=false;
				vm.guidedSalesData = previousQuestionBlock.questionSet;
				vm.view.selected_option = previousQuestionBlock.selectedOption;
				vm.view.previous_question_answer_set.splice(
					vm.view.previous_question_answer_set.indexOf(previousQuestionBlock));
				return;
			}
			 vm.guidedSalesData = vm.view.previous_question_set.pop();
			 vm.view.selected_option = vm.view.previously_selected.pop();
			 vm.view.previous_question_answer_set.pop();
		};

		vm.submit = function() {
			vm.submitFlag = true;
			vm.view.resultsData = {};
			var productFamilies = vm.view.selected_option.suggestedFamily.split(",");
			_.each(productFamilies, function(v,i){
				vm.guidedSalesFinalViewUrlCategory = catEndpointService.getSalesModel+'?categoryNameList='+v;
				httpService.get(vm.guidedSalesFinalViewUrlCategory).then(function(response) {
					vm.view.resultsData[response.config.url.split("=")[1]] = response.data;
					$timeout(function(){
						$($(".productFamilySection")[i]).css({'background-image':backgroundBannerMap[v]});
					},500);
				}, function(error){
					console.log(error);
	        	});
			});
		};

		vm.navigateToPlp = function(familyName, salesModelName) {
			$window.location.href = "/search/view/?categoryCode="+familyName+"&salesModelId="+salesModelName;
		}

		vm.goToFamilyName = function(productFamily){
			$('html, body').animate({
				scrollTop: $('#'+productFamily).offset().top
			  }, 500);
		}
		vm.init();
	} 
]);