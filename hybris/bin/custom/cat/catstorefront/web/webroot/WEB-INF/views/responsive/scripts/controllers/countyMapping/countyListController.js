catApp.controller('countyListController', ['httpService', 'catEndpointService', '$scope', 'getUrlParamFactory', '$window', function(httpService, catEndpointService, $scope, getUrlParamFactory, $window) {

    var vm = this, urlParams= getUrlParamFactory.getUrlParams();

    vm.selectedShipTo = urlParams.shipToAddressPk;
    vm.countyList = [];
    vm.updatedCountyList = [];
    vm.address = {};

    vm.setcounty = function(){
        httpService.get(catEndpointService.countyListUrl+"?shipToPk="+vm.selectedShipTo).then(function(response){
            vm.address = response.data.addressData;
            vm.countyList = response.data.countyData;
            angular.forEach(vm.countyList,function(object,index){
                object.selected = false;
            });
        });
    }

    vm.updateList = function(){
        vm.updatedCountyList = _.chain(vm.countyList)
                                .filter(function(v){return v.selected})
                                .map(function(v){return v.id})
                                .value();
    }

    vm.sendInfo =function(){
        if(vm.updatedCountyList.length){
            var postData = {
                countyIdList: vm.updatedCountyList,
                shipToPk: vm.selectedShipTo
            }
            httpService.postJsonData(catEndpointService.linkCounty, postData).then(function(response) {
                $window.location.href = '/my-account/getCountyMapping?shipToAddressPk='+vm.selectedShipTo;
            });
        }
    }

}]);

