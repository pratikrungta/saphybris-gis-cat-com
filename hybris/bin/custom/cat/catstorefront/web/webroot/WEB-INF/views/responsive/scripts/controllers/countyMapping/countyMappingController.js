catApp.controller('countyMappingController', ['catEndpointService', '$scope', 'constantService', '$rootScope', 'httpService', '$window', 'getUrlParamFactory', '$uibModal', function(catEndpointService, $scope, constantService, $rootScope, httpService, $window, getUrlParamFactory, $uibModal) {

    var vm = this, urlParams= getUrlParamFactory.getUrlParams();
    vm.isFirstTime = true;
    vm.view = {
        pagination: {
            totalNumberOfResults: 0,
            pageSize: 5,
            currentPage: 1
        },
        sortKey: 'countyName',
        sortOrder: 'ASC',
        ASC: 'ASC',
        DESC: 'DESC',
        selectedShipTo: '',
        defaultSelectOption: 'Select address',
        selectedHiddenAddress: '',
        selectedHiddenAddressIndex: ''
    };

    vm.init = function(){
        httpService.get(catEndpointService.getShiptoAddress+'?sortParameter=town').then(function(response){
            vm.setShippingData(response);
        });
    };

    vm.setShippingData = function(response){
        vm.view.shippingAddressList = response.data.shippingAddressList;
        vm.view.hiddenShippingAddress = response.data.hiddenShippingAddressList;
        if(!vm.view.hiddenShippingAddress || !vm.view.hiddenShippingAddress.length){
            vm.view.selectedHiddenAddress = "";
            vm.view.selectedHiddenAddressIndex = "";
        }
        if(!vm.view.selectedShipTo && vm.view.shippingAddressList && vm.view.shippingAddressList.length){
            vm.view.selectedShipTo = vm.isFirstTime ? (urlParams.shipToAddressPk || vm.view.shippingAddressList[0].id) : vm.view.shippingAddressList[0].id;
            vm.getCountiesofShipTo();
        } else if(!vm.view.shippingAddressList || !vm.view.shippingAddressList.length){
            vm.setCountyListData({countyData:[]});
        }
        vm.isFirstTime = false;
    }

    vm.getCountiesofShipTo = function(selectedShipTo){
        if(vm.view.selectedShipTo == selectedShipTo){
            return false;
        }
        vm.view.selectedShipTo = selectedShipTo || vm.view.selectedShipTo;
        var queryObj = {
            shipToPk: vm.view.selectedShipTo,
            sortParameter: vm.view.sortKey,
            sortOrder: vm.view.sortOrder
        }
        httpService.get(catEndpointService.getCountiesofShipTo+vm.getQueryString(queryObj)).then(function(countiesResponse){
            vm.setCountyListData(countiesResponse.data);
        });
    }

    vm.delinkCounty = function(delinkCountyId){
        httpService.get(catEndpointService.unlinkCounty+"?countyPk="+delinkCountyId).then(function(countiesResponse){
            vm.setCountyListData(countiesResponse.data);
        });
    }

    vm.setCountyListData = function(response){
        vm.view.countiesList = response.countyData;
        vm.view.pagination.totalNumberOfResults = vm.view.countiesList.length;
        vm.view.enableAddCounty = response.enableAddCounty;
        vm.view.pagination.pageSize = response.pageSize || vm.view.pagination.pageSize;
    }

    vm.getQueryString = function(paramsObj){
        var queryString = '';
        _.map(paramsObj, function(v, k){
            queryString += (k + "=" + v) + "&";
        });
        return (queryString ? ("?" + queryString.slice(0, -1)) : queryString);
    }

    vm.onSortClick = function(sortKey){
        if(vm.view.sortKey !== sortKey){
            vm.view.sortOrder = vm.view.DESC;
        }else{
            vm.view.sortOrder = (vm.view.sortOrder == vm.view.ASC ? vm.view.DESC : vm.view.ASC);
        }
        vm.view.sortKey = sortKey || vm.view.sortKey;
        vm.getCountiesofShipTo();
    }

    vm.pageChanged = function(){

    }

    vm.showMore = function(){
        vm.view.pagination.currentPage += 1;
    }

    vm.addCounty = function(){
        if(vm.view.enableAddCounty && vm.view.shippingAddressList.length){
            $window.location.href = "/my-account/linkCounties?shipToAddressPk="+vm.view.selectedShipTo;
        }
    }

    vm.hideShipTo = function(event, hideShipTo){
        event.stopPropagation();
        event.preventDefault();
        if(vm.view.selectedShipTo == hideShipTo){
            vm.emptySelectedShipTo = true;
            if(vm.view.countiesList.length){
                vm.showPopUp();
            }else{
                vm.hideShipToAddress(hideShipTo);
            }
        }else {
            var queryObj = {
                shipToPk: hideShipTo,
                sortParameter: vm.view.sortKey,
                sortOrder: vm.view.sortOrder
            }
            httpService.get(catEndpointService.getCountiesofShipTo+vm.getQueryString(queryObj)).then(function(countiesResponse){
                if(countiesResponse.data && countiesResponse.data.countyData && countiesResponse.data.countyData.length){
                    vm.showPopUp(hideShipTo);
                } else {
                    vm.hideShipToAddress(hideShipTo);
                }
            });
        }
        
    }

    vm.showPopUp = function(hideShipTo){
        hideShipTo = hideShipTo || vm.view.selectedShipTo;
        $scope.displayRemoveText = constantService.countyMapping.hideAddress;
        $scope.removeCartProductIndex = hideShipTo;
        vm.showRemoveModalInstance = $uibModal.open({
            backdrop: 'static',
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: '/_ui/responsive/cat/templates/quotationRemoveItemPopup.html',
            scope: $scope,
            size: 'quotation-remove-item',
        });
    }

    $scope.close = function() {
        if(vm.showRemoveModalInstance){
            vm.showRemoveModalInstance.close();
        }
    }

    vm.hideShipToAddress = function(hideShipTo){
        if(vm.emptySelectedShipTo){
            vm.view.selectedShipTo = "";
            vm.emptySelectedShipTo = false;
        }
        $scope.close();
        httpService.get(catEndpointService.hideShipTo+"?shipToPk="+hideShipTo).then(function(response){
            vm.setShippingData(response);
            vm.view.enableAddCounty = response.data.enableAddCounty;
        });
    }

    $scope.deleteCartProducts = vm.hideShipToAddress;

    vm.unHideShipTo = function(){
        if(vm.view.selectedHiddenAddressIndex || vm.view.selectedHiddenAddressIndex == 0){
            var hideShipTo = vm.view.hiddenShippingAddress[vm.view.selectedHiddenAddressIndex].id;
            httpService.get(catEndpointService.unHideShipTo+"?shipToPk="+hideShipTo).then(function(response){
                vm.setShippingData(response);
                vm.view.enableAddCounty = response.data.enableAddCounty;
                vm.view.selectedHiddenAddressIndex = '';
                vm.view.selectedHiddenAddress = '';
            });
        }
    }

    vm.onDDChange = function(index){
        if(index || index == 0){
            vm.view.selectedHiddenAddress = vm.view.hiddenShippingAddress[index].shipToCode + "," + vm.view.hiddenShippingAddress[index].line1 + "," + vm.view.hiddenShippingAddress[index].town + "," + (vm.view.hiddenShippingAddress[index].region ? vm.view.hiddenShippingAddress[index].region.name : '') + "," + (vm.view.hiddenShippingAddress[index].country ? vm.view.hiddenShippingAddress[index].country.name : '') + "," + vm.view.hiddenShippingAddress[index].postalCode;
            vm.view.selectedHiddenAddressIndex = index;
        }else {
            vm.view.selectedHiddenAddress = "";
            vm.view.selectedHiddenAddressIndex = "";
        }
    }

    vm.init();

}]);