catApp.controller('favoriteProductsController', ['$scope', 'catService', '$rootScope','$timeout', function($scope, catService, $rootScope,$timeout) {

    var vm = this;
    vm.catService = catService;
    vm.view = {
        usedProducts: false,
        newProducts: true,
        showFavoriteProductsOf: "new",
        hasFavoriteUsedProducts: false,
        hasFavoriteNewProducts: false,
        favoriteUsedProductsAddedOnFly: [],
        favoriteNewProductsAddedOnFly: [],
        favoriteUsedProducts: [],
        favoriteNewProducts: []
    };


    $rootScope.$on('bookmark-toggled', function(event, args) {
        $scope.message = args;
        if (!!args.productIdForCommunication && !!args.bookMarked) {
            if (!!args.isNewProduct && vm.view.favoriteNewProducts.indexOf(args.productIdForCommunication) < 0) {
                vm.view.favoriteNewProducts.push(args.productIdForCommunication);
                vm.view.favoriteNewProductsAddedOnFly.unshift({
                    image: args.image,
                    primaryHeader: args.primaryHeader,
                    pdpLink: args.pdpLink,
                    bookMarked: args.bookMarked,
                    productIdForCommunication: args.productIdForCommunication,
                    isNewProduct: args.newProductcard,
                    disContinued: args.disContinued
                });
                if(!!vm.view.favoriteNewProducts && vm.view.favoriteNewProducts.length >0){
                  vm.view.hasFavoriteNewProducts = true; 
                }
            } else if (!args.isNewProduct && vm.view.favoriteUsedProducts.indexOf(args.productIdForCommunication) < 0) {
                vm.view.favoriteUsedProducts.push(args.productIdForCommunication);
                vm.view.favoriteUsedProductsAddedOnFly.unshift({
                    image: args.image,
                    primaryHeader: args.primaryHeader,
                    pdpLink: args.pdpLink,
                    bookMarked: args.bookMarked,
                    productIdForCommunication: args.productIdForCommunication,
                    isNewProduct: args.newProductcard,
                    disContinued: args.disContinued
                });
                if(!!vm.view.favoriteUsedProducts && vm.view.favoriteUsedProducts.length >0){
                  vm.view.hasFavoriteUsedProducts = true; 
                }
            }
            event.preventDefault();
            return;
        }

        if (!!args.productIdForCommunication && !args.bookMarked) {
            if (!!args.isNewProduct && vm.view.favoriteNewProducts.indexOf(args.productIdForCommunication) !==-1) {
                vm.view.favoriteNewProducts.splice(
                    vm.view.favoriteNewProducts.indexOf(args.productIdForCommunication),
                    1);
                    $timeout(function(){
                        vm.view.favoriteNewProductsAddedOnFly
                        = vm.view.favoriteNewProductsAddedOnFly.filter(function(v,i){
                            if(v.productIdForCommunication === args.productIdForCommunication){   
                                return false;
                            }
                            return true;
                        })
                    })
                       
                
                if(!!vm.view.favoriteNewProducts && vm.view.favoriteNewProducts.length === 0){
                  vm.view.hasFavoriteNewProducts = false; 
                }
                return;
            }  
            if (!args.isNewProduct && vm.view.favoriteUsedProducts.indexOf(args.productIdForCommunication) !==-1) {
                vm.view.favoriteUsedProducts.splice(
                    vm.view.favoriteUsedProducts.indexOf(args.productIdForCommunication),
                1);
                vm.view.favoriteUsedProductsAddedOnFly
                                    = vm.view.favoriteUsedProductsAddedOnFly.filter(function(v,i){
                                        if(v.productIdForCommunication === args.productIdForCommunication){
                                            return false;
                                        }
                                        return true;
                                    })

                if(!!vm.view.favoriteUsedProducts && vm.view.favoriteUsedProducts.length === 0){
                  vm.view.hasFavoriteUsedProducts = false; 
                }
                return;
            }
            event.preventDefault();
            return;
        }
        
       
    });

    vm.addNewFavorite = function(obj) {
        vm.view.favoriteNewProducts.push("" + obj.productIdForCommunication);

        vm.view.favoriteNewProductsAddedOnFly.unshift({
            image: obj.image,
            primaryHeader: obj.primaryHeader,
            pdpLink: obj.pdpLink,
            bookMarked: true,
            productIdForCommunication: obj.productIdForCommunication,
            isNewProduct: true,
            disContinued: obj.disContinued
        });
    }

    vm.addUsedFavorite = function(obj) {
        vm.view.favoriteUsedProducts.push("" + obj.productIdForCommunication);

        vm.view.favoriteUsedProductsAddedOnFly.unshift({
            image: obj.image,
            primaryHeader: obj.primaryHeader,
            pdpLink: obj.pdpLink,
            bookMarked: true,
            productIdForCommunication: obj.productIdForCommunication,
            isNewProduct: false,
            disContinued: false
        });
    }

    vm.hasUsedFavoriteProducts = function(hasProducts) {
        if (!!hasProducts && hasProducts == "true") {
            vm.view.hasFavoriteUsedProducts = true;
        }
    }

    vm.hasNewFavoriteProducts = function(hasProducts) {
        if (!!hasProducts && hasProducts == "true") {
            vm.view.hasFavoriteNewProducts = true;
        }
    }

    $scope.$watch(angular.bind(vm, function() {
        return vm.view.showFavoriteProductsOf;
    }), function(value) {
        if (value == "new") {
            vm.view.newProducts = true;
            vm.view.usedProducts = false;
        } else {
            vm.view.newProducts = false;
            vm.view.usedProducts = true;
        }
    });
}]);