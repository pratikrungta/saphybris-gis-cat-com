catApp.controller('fbcMustSelectController', ['httpService', 'catEndpointService', '$scope', '$location', '$window', '$compile', 'constantService', '$timeout', 'catService', '$uibModal', 'getUrlParamFactory', function(httpService, catEndpointService, $scope, $location, $window, $compile, constantService, $timeout, catService, $uibModal, getUrlParamFactory) {
    var vm = this;
    vm.defaultDownloadImage = constantService.defaultDownloadImage;
    vm.fbcFeaturesList = {};
    vm.chunkedProductCardsList = [];
    $scope.configDetailsData = [];
    $scope.featuresData = [];
    vm.selectionsList = [];
    vm.isDataLoaded = false;
    vm.showReviewParts = false;
    vm.totalParts = 0;
    vm.totalListPrice = 0;
    vm.addToCartDisabled=false;
    $scope.isOpen = false;
    vm.userSelection = {
        "disableNextBtn" : true,
        "selectedUserData" : {}
    };
    vm.fbcError = false;
    vm.mustSelectHeading = ACC.localization.fbcMustSelectHeading;
    vm.reviewPartsHeading = ACC.localization.fbcReviewPartsHeading;
    vm.reviewPartsFeatureCode = ACC.localization.fbcReviewPartsFeatureCode;
    vm.reviewPartsReferenceNo = ACC.localization.fbcReviewPartsReferenceNo;
    vm.reviewPartsQuantity = ACC.localization.fbcReviewPartsQuantity;
    vm.reviewPartsPartDesc = ACC.localization.fbcReviewPartsPartDesc;
    vm.reviewPartsAI = ACC.localization.fbcReviewPartsAssemblyIndicator;
    vm.reviewPartsLN = ACC.localization.fbcReviewPartsLN;
    vm.reviewPartsPriceEach = ACC.localization.fbcReviewPartsPriceEach;
    vm.reviewPartsTotalPrice = ACC.localization.fbcReviewPartsTotalPrice;
    vm.reviewPartsTotalParts = ACC.localization.fbcReviewPartsTotalParts;
    vm.reviewPartsTotalListPrice = ACC.localization.fbcReviewPartsTotalListPrice;
    vm.reviewPartsAddToCart = ACC.localization.fbcReviewPartsAddToCart;
    vm.fbcErrorText = ACC.localization.fbcErrorText;

    $scope.viewInfoMobilePopup = function(cardData){
        $scope.cardData = cardData;
        if($scope.cardData.extendedDescription !== '' && !vm.infoPopoverInstance){
            vm.infoPopoverInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: '/_ui/responsive/cat/templates/fbcMustSelectPopoverMobile.html',
                scope: $scope,
                size: 'fbc-popover-mobile-modal',
            });
        }
    }

    vm.navigateToHome = function(){
        $window.location.href = '/';
    }
    vm.populateFBCData = function(){
        var urlParams= getUrlParamFactory.getUrlParams();
        vm.configId = urlParams.productCode;
        vm.productFamily = urlParams.prodFamily;
        vm.salesModel = urlParams.salesModel;
        vm.selectedLane = urlParams.Lane.toUpperCase();
        vm.dca = urlParams.dcaCode;
        vm.selectedQty = urlParams.qty;
        vm.fromPage = urlParams.fromPage;
        vm.imageURL = urlParams.primaryImageUrl;
        var fbcURL = catEndpointService.fbcMustSelectFeaturesPageLoadEndPoint+'?dca='+vm.dca+'&laneCode='+vm.selectedLane;
        httpService.get(fbcURL).then(function(response) {
            var response = JSON.parse(response.data);
            if(response.hasOwnProperty('edhError')){
                vm.fbcError = true;
            } else if(!!response.featureSet) {
                vm.fbcFeaturesList = response.featureSet;
                vm.chunkedProductCardsList = _.chunk(vm.fbcFeaturesList.features, 4);
                vm.isDataLoaded = true;
            } else {
                vm.showReviewParts = true;
                vm.isDataLoaded = true;
                vm.selectionsList = response.selections;
                angular.forEach(vm.selectionsList,function(selection,index){
                    vm.totalParts = vm.totalParts + selection.quantity;
                    vm.totalListPrice = vm.totalListPrice + (selection.quantity * selection.listPrice);
                });
            }
        });
    }
    vm.loadFreshFBCProducts = function(){
        var featureCode = vm.userSelection.selectedUserData.featureCode;
        var productCode = (vm.selectedLane == 'lane1') ? vm.dca : vm.configId;
        var nextBtnURL = catEndpointService.fbcMustSelectFeaturesNextBtnEndPoint+'?featureCode='+featureCode+'&productCode='+productCode;
        vm.userSelection.disableNextBtn = true;
        httpService.get(nextBtnURL).then(function(response) {
            var response = JSON.parse(response.data);
            if(response.hasOwnProperty('edhError')){
                vm.fbcError = true;
            } else if(!!response.featureSet) {
                vm.fbcFeaturesList = response.featureSet;
                vm.chunkedProductCardsList = _.chunk(vm.fbcFeaturesList.features, 4);
                vm.isDataLoaded = true;
            } else {
                vm.showReviewParts = true;
                vm.selectionsList = response.selections;
                angular.forEach(vm.selectionsList,function(selection,index){
                    vm.totalParts = vm.totalParts + selection.quantity;
                    vm.totalListPrice = vm.totalListPrice + (selection.quantity * selection.listPrice);
                });
            }
        });
    }
    $scope.selectFBCProduct = function(featureCode){
        vm.userSelection.disableNextBtn = false;
        vm.userSelection.selectedUserData = {
            "featureCode" : featureCode
        }
    }
    $scope.closeInfoPopover = function(){
        vm.infoPopoverInstance.close();
        vm.infoPopoverInstance = null;
    }
    vm.redirectToCartPage = function(){
    	vm.addToCartDisabled=true;
        httpService.postJsonData(catEndpointService.addNewProductUrl,{configurable:true,laneType:vm.selectedLane,productCodePost:vm.configId,qty:vm.selectedQty}).then(
            function(response){
                $window.location.href = (response && response.data)?response.data.redirectUrl:catEndpointService.reorderSubmitPageEndPoint;
            },function(){
                $window.location.href = "/error";
            }
        );
    }

}]);
