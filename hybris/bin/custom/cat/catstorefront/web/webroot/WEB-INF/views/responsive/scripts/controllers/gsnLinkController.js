catApp.controller('GsnLinkController', ['httpService', 'catEndpointService', '$scope', '$location', '$window', '$compile', 'constantService', '$timeout', 'catService', function(httpService, catEndpointService, $scope, $location, $window, $compile, constantService, $timeout, catService) {
    var vm = this;
    vm.init = function(msoNumber){
        var staticGsnURL = $(".trackingDetailsButton a").attr("href");
        var gsnLinkURL = staticGsnURL.replace(/msoNumVar/g, msoNumber);
        $(".trackingDetailsButton a").attr("href",gsnLinkURL);
    }
}]);
