catApp.controller('landingPageController', ['httpService', 'catEndpointService','specCheckHttpService', '$scope','$uibModal', function(httpService, catEndpointService, specCheckHttpService,$scope,$uibModal) {
  var vm = this;

  vm.roleFeatureInfoHomePage = function(isUTVUser,isIM,isSP){
    if (isUTVUser && isIM){
      ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.cuvUserEventName,ACC.gaHelper.homepageEvent);
    }
    else if(!isUTVUser&& isIM ){
      ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.bcpUserEventName,ACC.gaHelper.homepageEvent);
    }
    else if(isSP){
      ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.spUserEventName,ACC.gaHelper.homepageEvent);
    }
  }


  vm.clearCartForLaneProdcuts = function(){
        httpService.get(catEndpointService.getCartStatusForCurrentOrderWindow).then(function(response) {
          if(response.data.isCartCleared){
            $scope.cartForProducts = response.data;
            vm.clearCartLaneModalInstance = $uibModal.open({
              backdrop: 'static',
              ariaLabelledBy: 'modal-title',
              ariaDescribedBy: 'modal-body',
              templateUrl: '/_ui/responsive/cat/templates/clearCartPopup.html',
              scope:$scope,
              size: 'clear-cart-popup'
            });
          }
        }, function(error) {

        });
  }

  $scope.dismissclearCartPopup = function() {
      vm.clearCartLaneModalInstance.dismiss("cancel");
      ACC.minicart.refreshMinicartCountNum();
  };

  vm.clearCartForLaneProdcuts();
}]);