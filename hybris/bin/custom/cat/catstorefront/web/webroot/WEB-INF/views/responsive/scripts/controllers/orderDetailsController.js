catApp.controller('orderDetailsController', ['httpService', 'catEndpointService','specCheckHttpService', '$scope','$uibModal', function(httpService, catEndpointService, specCheckHttpService,$scope,$uibModal) {
    var vm = this;
    vm.roleFeatureInfoOrderDetails = function(isUTVUser,isIM){
        if (isUTVUser && isIM){
          ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.cuvUserEventName,ACC.gaHelper.orderDetailSEvent);
        }
        else if(!isUTVUser&& isIM ){
          ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.bcpUserEventName,ACC.gaHelper.orderDetailSEvent);
        }
      }
}]);