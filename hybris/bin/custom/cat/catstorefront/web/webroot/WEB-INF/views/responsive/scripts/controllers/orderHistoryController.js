catApp.filter('showPendingSubmission',function(){
	return function(input){
		console.log(input);
		if(input === null){
			return "-";
		} else if(input === "PENDING_APPROVAL") {
			return "Pending Submission";
		} else {
			return input;
		}
	}
});
catApp.filter('initcaps', function() {
    return function(input){
      if(input.indexOf(' ') !== -1){
        var inputPieces,
            i;
  
        input = input.toLowerCase();
        inputPieces = input.split(' ');
  
        for(i = 0; i < inputPieces.length; i++){
          inputPieces[i] = capitalizeString(inputPieces[i]);
        }
  
        return inputPieces.toString().replace(/,/g, ' ');
      }
      else {
        input = input.toLowerCase();
        return capitalizeString(input);
      }
  
      function capitalizeString(inputString){
        return inputString.substring(0,1).toUpperCase() + inputString.substring(1);
      }
    };
  });
catApp.controller('orderHistoryController', ['httpService', 'catEndpointService', '$scope', '$location','$window', function(httpService, catEndpointService, $scope, $location, $window) {
    var vm = this, _selectProductFamily_ = "All";
    vm.orderHistory = {};
    vm.isFirstLoad = true;
    vm.hasOrders = true;
    vm.dropDownSelection=_selectProductFamily_;
    vm.dropDownLaneSelection={default:_selectProductFamily_};
    vm.dropDownSelectionProductFamily="";
    vm.selectedVehicles=[];
    vm.selectedLanes=[];
    vm.view = {
        sortBtn: false,
        sortMetaData: {
            mso:{
                isSelected: false,
                name: "mso",
                canSort: true,
                isReverseSort: false,
                key:"byMSO"
            },
            orderDate:{
                isSelected: true,
                name: "order date",
                canSort: true,
                isReverseSort: false,
                key:"byDate"
            },
            status:{
                isSelected: false,
                name: "status",
                canSort: true,
                isReverseSort: false,
                key:"byOrderStatus"
            }
        },
        currentPage: '',
        pagination: {
        	
        }
    }


    vm.convertToLowerCase = function(text) {
        return text.toLowerCase();
    }

    vm.setSortedKey = function(sortedKey){
        sortedKey = sortedKey || '';
        var isAsc = (sortedKey.indexOf('Asc') != -1);
        sortedKey = sortedKey.replace('Asc', '');
        for(var k in vm.view.sortMetaData) {
           if(vm.view.sortMetaData[k].key == sortedKey){
                vm.view.sortMetaData[k].isReverseSort = isAsc;
            }
        }
    }
   
    vm.onSortClick = function(key){
        vm.view.sortBtn = !vm.view.sortBtn;
        var query = vm.view.sortMetaData[key].key + (vm.view.sortMetaData[key].isReverseSort ? '' : 'Asc');
        for(var i in vm.view.sortMetaData){
            if(i == key){
                vm.view.sortMetaData[key].isSelected = true;
                vm.view.sortMetaData[key].isReverseSort = !vm.view.sortMetaData[key].isReverseSort;
            }else{
                vm.view.sortMetaData[i].isSelected = false;
                vm.view.sortMetaData[i].isReverseSort = false;
            }
        }
        if(!vm.dropDownSelection.match(_selectProductFamily_)){
        	vm.dropDownSelectionProductFamily=vm.dropDownSelection.toUpperCase();
        }

        // $window.location.href = '/my-account/orders?sort='+query+'&show=Page#';
        httpService.get(catEndpointService.getOrders+"?page=0&sort="+query +"&search="+vm.view.pagination.search+"&laneType="+(vm.dropDownLaneSelection.code||'')+"&productFamilySearch="+vm.dropDownSelectionProductFamily).then(function(response) {
            if (response.status == 200) {
                setDomData(response.data);
            }
        }, function(error) {});
    }

    vm.showMoreMobile = function(param){
        httpService.get(catEndpointService.getOrders+"?page="+ (vm.view.pagination.currentPage) +"&sort="+vm.orderHistory.sortKey +"&search="+vm.view.pagination.search).then(function(response) {
           if (response.status == 200) {
               setDomData(response.data, param);
           }
       }, function(error) {});
    }

    vm.pageChanged = function(){
	 	httpService.get(catEndpointService.getOrders+"?page="+ (vm.view.pagination.currentPage-1) +"&sort="+vm.orderHistory.sortKey +"&search="+vm.view.pagination.search+"&laneType="+(vm.dropDownLaneSelection.code||'')+"&productFamilySearch="+(vm.dropDownSelection.toLowerCase() == _selectProductFamily_.toLowerCase() ? "" : vm.dropDownSelection)).then(function(response) {
            if (response.status == 200) {
                setDomData(response.data);
            }
        }, function(error) {});
    }
    vm.roleFeatureInfoOrderHistory = function(isUTVUser,isIM){
        if (isUTVUser && isIM){
            ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.cuvUserEventName,ACC.gaHelper.orderHistoryEvent);
          }
          else if(!isUTVUser&& isIM ){
            ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.bcpUserEventName,ACC.gaHelper.orderHistoryEvent);
          }
    }
    vm.init = function(){
        httpService.get(catEndpointService.getOrders).then(function(response) {
            if (response.status == 200) {
                setDomData(response.data);
               vm.setSelectedVehicles();
               vm.setLaneTypes();
            }
        }, function(error) {});
        var everywhere = angular.element(window.document);
        everywhere.bind('click', function(event){
            if(angular.element(".dropdown-select-product-family").is(':visible')){
                vm.dropDownSelect();
            }
            if(angular.element(".dropdown-select-lane").is(':visible')){
                vm.dropDownLaneSelect();
            }
        });
    }
    vm.dropDownSelect=function($event){
        if($event){
            $event.stopPropagation();
        }
        $(".dropdown-select-product-family").toggle();
        $(".dropdown-select-lane").hide();
    }
    vm.dropDownLaneSelect=function($event){
        if($event){
            $event.stopPropagation();
        }
        $(".dropdown-select-lane").toggle();
        $(".dropdown-select-product-family").hide();
    }
   
	vm.setSelectedVehicles = function(){
		httpService.get(catEndpointService.getProductFamilies).then(function(response) {
            if (response.status == 200) {
                for(var index=0; index<response.data.length; index++){
            	     vm.selectedVehicles.push(response.data[index]);
                }
                if(vm.selectedVehicles.length){
                    vm.selectedVehicles.unshift(_selectProductFamily_);
                }
            	
            }
        }, function(error) {});
    }
    vm.setLaneTypes = function(){
		httpService.get(catEndpointService.getLaneTypes).then(function(response) {
            if (response.status == 200) {
                for(var index=0; index<response.data.length; index++){
            	     vm.selectedLanes.push(response.data[index]);
                }
                if(vm.selectedLanes.length){
                    vm.selectedLanes.unshift({default:_selectProductFamily_});
                }
            }
        }, function(error) {});
    }
    vm.selectLane = function(laneItem, isClear){
        vm.dropDownLaneSelection = laneItem;
		vm.searchClick();
        if(!isClear){
            vm.dropDownLaneSelect();
        }
	}
	vm.selectVehicles = function(arg, isClear){
        vm.dropDownSelection = arg;
		vm.searchClick();
        if(!isClear){
            vm.dropDownSelect();
        }
	}
	vm.searchClick = function(){
		
		httpService.get(catEndpointService.getOrders+"?sort="+vm.orderHistory.sortKey+"&search="+vm.view.pagination.search+"&laneType="+(vm.dropDownLaneSelection.code||'')+"&productFamilySearch="+(vm.dropDownSelection.toLowerCase() == _selectProductFamily_.toLowerCase() ? "" : vm.dropDownSelection)).then(function(response) {
            if (response.status == 200) {
                setDomData(response.data);
            }
        }, function(error) {});
		
	}
    
	vm.clearSearch = function(){
        vm.view.pagination.search = "";
        if(vm.dropDownSelection){
            vm.selectVehicles(vm.dropDownSelection, true);
        }else{
            vm.searchClick();
        }
    }

    function setDomData(response, param){
        vm.orderStatusMetaData = response.orderStatusMetaData;
        response =  response.orderList;
        if(vm.isFirstLoad){
            vm.isFirstLoad = false;
            vm.hasOrders = (response.results.length !== 0);
        }
        if(param == 'mobile') {
            vm.orderHistory.orders = vm.orderHistory.orders.concat(response.results);
        } else {
            vm.orderHistory.orders = response.results;
        }    
        var searchKey = vm.view.pagination.search;
        vm.view.pagination = response.pagination;
        vm.view.sorts = response.sorts;
        vm.view.pagination.currentPage = (vm.view.pagination.currentPage+1);
        vm.view.pagination.search = (searchKey ? searchKey : "");
        vm.orderHistory.sortKey = response.pagination.sort;
    }

    vm.getPopOverText = function(code){
        return vm.orderStatusMetaData[code+"_MESSAGE"];
    }
    
    vm.getDateFormat = function(_date){ // "MM/dd/yyyy"
        if(_date){
            var date = new Date(_date);
            //_date = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
            _date = vm.getTwoDigits(date.getMonth() + 1) + "-" + vm.getTwoDigits(date.getDate()) + "-" + date.getFullYear(); 
        }else {
            _date = '-';
        }
        return _date;
    }
    
    vm.getUTCDateFormat = function(_date){ // "MM/dd/yyyy"
        if(_date){
            var date = new Date(_date);
            //_date = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
            _date = vm.getTwoDigits(date.getUTCMonth() + 1) + "-" + vm.getTwoDigits(date.getUTCDate()) + "-" + date.getUTCFullYear(); 
        }else {
            _date = '-';
        }
        return _date;
    }
    vm.getTwoDigits = function(val){
        return ("00"+val).slice(-2);
    } 

}]);
