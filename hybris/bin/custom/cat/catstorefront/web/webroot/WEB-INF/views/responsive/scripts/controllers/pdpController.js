catApp.controller('pdpController', [
	'$scope',
	'$window',
	'catEndpointService',
	'httpService',
	'modalPopUpFactory',
	'constantService',
	'screenResizedService',
	'properMessageService',
	'$uibModal',
	function($scope, $window, catEndpointService, 
		httpService, modalPopUpFactory, constantService, screenResizedService,properMessageService,$uibModal) {
		var maxRowLimitDesktop = 9, maxRowLimitMobile = 5;
		$scope.isRedirecting = false;
		$scope.maxRowLimit = maxRowLimitDesktop;
		$scope.partCustom = true;
		$scope.attachmentCustom = true;
		$scope.view = {
			laneButtons : [
				{
					buttonKey : ACC.localization.Lane1,
					// dotColor : '#aa1225',
					isSelectedKey : 'lane1',
					actualLaneObj : ''
				},
				{
					buttonKey : ACC.localization.Lane2,
					// dotColor : '#ff7941',
					isSelectedKey : 'lane2',
					actualLaneObj : ''
				},
				{
					buttonKey : ACC.localization.Lane3,
					// dotColor : '#3d7100',
					isSelectedKey : 'lane3',
					actualLaneObj : ''
				}
			],
			laneBtnSelectedData: {
				btnKey: '',
				laneBtnObj: ''
			}
		}
		$scope.pageLoadFlag = false;
		$scope.input = {};
		
		screenResizedService.updateValuesAfterMetaChange = function(){
			$scope.maxRowLimit = screenResizedService.metaInfo.isMobile ? maxRowLimitMobile : maxRowLimitDesktop;
		}
		screenResizedService.updateValuesAfterMetaChange();
		
		var isToOpenCartLostPopup = function(isUTVprod,isUTVCart){
			var a=isUTVprod,b=isUTVCart;
			if(typeof a!=='boolean'||typeof b!=='boolean'){
				return false;
			}
			return !a&&b||a&&!b;//XOR(isUTVprod,isUTVCart)
		}
		$scope.backToReorderInitiated = function(){
			if(!$scope.isBackToReorderClicked){
				window.history.back();
				$scope.isBackToReorderClicked = true;
			}
		}
		$scope.roleFeatureInfoProductDetails = function(isUTVUser,isIM,isSP){
			if (isUTVUser && isIM){
				ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.cuvUserEventName,ACC.gaHelper.productDetailsViewEvent);
			  }
			  else if(!isUTVUser&& isIM ){
				ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.bcpUserEventName,ACC.gaHelper.productDetailsViewEvent);
			  }
			  else if(isSP){
				ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.spUserEventName,ACC.gaHelper.productDetailsViewEvent);
			  }
		}

		$scope.laneBtnClick = function(btnObj) {
			$scope.view.laneBtnSelectedData.btnKey = btnObj.isSelectedKey;
			$scope.view.laneBtnSelectedData.laneBtnObj = btnObj.actualLaneObj;
			$scope.input.customConfig = 00;
			$scope.cartInputDisable = btnObj.actualLaneObj ? btnObj.actualLaneObj.addToCartDisabled : true;
			$scope.validateQuantity();
		}

		$scope.viewCoachingTools = function(){
			httpService.get(catEndpointService.getCoachingTools+"?productCode="+$("#productCodePostId").val()).then(function(response){
				if(!$scope.viewCoachingPopup){
					$scope.coachingTools = response.data;//.concat(response.data);
					$scope.viewCoachingPopup = modalPopUpFactory.open({
						backdrop: 'static',
						ariaLabelledBy: 'modal-title',
						ariaDescribedBy: 'modal-body',
						templateUrl: '/_ui/responsive/cat/templates/coachingToolsPopUp.html',
						size: 'md',
						openedClass: 'coaching-tools-modal',
						scope: $scope,
					});
				}
	        });
		}
		 $scope.dismissCoachingToolsPopUp = function(){
			 modalPopUpFactory.close($scope.viewCoachingPopup);
			 $scope.viewCoachingPopup = null;
		 }
		 $scope.getEllipsedText = function(text){ // TODO: Logic to be added if ellipses is need in middle of the text.
		 	var _length = text ? text.length : 0;
		 	if(_length > 20){
		 		text = text.slice(0, 9) + " ... " + text.slice(_length-10);
		 	}
		 	return text;
		 }
		$scope.isFromReorderPLP = function(){
			return /(reorder\/viewManageOrders)/.test(document.referrer);
		}
		$scope.userTypeConfig = "";
		$scope.input.customConfig = "01";
		$scope.isUtvOrTruckloadFlow = "";
		$scope.reorder = function() {

		}

		$scope.setter = function(arg,isUTVprod, productCode, salesModel, productCondition, laneType) {
			$scope.userTypeConfig = arg;
			$scope.isUTVprod=isUTVprod;
			$scope.init(productCode, salesModel, productCondition);
			$scope.productCode = productCode;
			$scope.salesModel = salesModel;
			$scope.laneTypeFromPlp = laneType.toLowerCase();
		}
		$scope.setStockLevel = function(args,imAddToCartFlag,imCurrentStockFlag){
			args = parseInt(args);
			$scope.maxQuantity =  constantService.maxOrderLimit; //(args && args >= 32) ? 32 : args;
			$scope.imAddToCartFlag = imAddToCartFlag;
			$scope.imCurrentStockFlag = imCurrentStockFlag;
			$scope.input.customConfig = 1;
			if($scope.userTypeConfig.toUpperCase() === constantService.userTypeIM || $scope.userTypeConfig.toUpperCase()===constantService.userTypeUTV){
				$scope.input.customConfig = 0;
			}
			$scope.validateQuantity();
		}
		$scope.init = function(productCode, salesModel, productCondition) {
			switch ($scope.userTypeConfig) {
			case "IM":
				$scope.stockRecommend = false;
				$scope.coachingTool = true;
				if(productCondition === 'New') {
					$scope.initializeLaneButtonObj(productCode, salesModel);
				}
				break;
			case "UTV":
				$scope.stockRecommend = false;
				$scope.coachingTool = true;
				if(!$scope.isUTVprod && productCondition === 'New') {
					$scope.initializeLaneButtonObj(productCode, salesModel);
				}
				break;
			case "SP":
				$scope.stockRecommend = true;
				$scope.coachingTool = false;
				break;

			default:
				$scope.stockRecommend = false;
				$scope.coachingTool = false;
			}
			$scope.validateQuantity();
		};

		$scope.initializeLaneButtonObj = function(productCode, salesModel) {
			httpService.get(catEndpointService.getLaneSpecificData+"?productCode="+ productCode + '&salesModel=' + salesModel).then(function(response){
				if(response.status === 200) {
					$scope.pageLoadFlag = true;
					$scope.laneWiseData = response.data;
					$scope.cartLaneType = $scope.laneWiseData.cartLaneType;
					_.map($scope.view.laneButtons, function(laneObj){
						if($scope.view.laneBtnSelectedData.btnKey == '' && $scope.laneWiseData[laneObj.isSelectedKey] !== null) {
							$scope.view.laneBtnSelectedData.btnKey = $scope.laneTypeFromPlp==='all'?laneObj.isSelectedKey:$scope.laneTypeFromPlp;
							$scope.view.laneBtnSelectedData.laneBtnObj = $scope.laneWiseData[$scope.laneTypeFromPlp==='all'?laneObj.isSelectedKey:$scope.laneTypeFromPlp];
							$scope.cartInputDisable = $scope.view.laneBtnSelectedData.laneBtnObj.addToCartDisabled;
						}
						laneObj.actualLaneObj = $scope.laneWiseData[laneObj.isSelectedKey];
					});
				}
			});
		};

		$scope.getIsFav = function(code, isBookMarked) {
			$scope.bookMarked = isBookMarked ? true : false;
			$scope.productIdForCommunication = code;
		};

		$scope.toggleBookMark = function() {
			httpService.get(
					catEndpointService.toggleFavoriteIcon
							+ "?productCodePost="
							+ $scope.productIdForCommunication).then(
					function(response) {
						if (response.status == 200) {
							if (!!response.data) {
								if (!!response.data.addSuccess) {
									$scope.bookMarked = true;
								} else if (!!response.data.removeSuccess) {
									$scope.bookMarked = false;
								} else {
									$("#favoriteLimitReached").show();
								}
							}
						}
					}, function(error) {

					});
		}

		$scope.validateQuantity = function() {
			if($scope.laneWiseData && $scope.view.laneBtnSelectedData.btnKey &&
				$scope.laneWiseData[$scope.view.laneBtnSelectedData.btnKey]){
				switch($scope.view.laneBtnSelectedData.btnKey){
		            case "lane2":
		            $scope.maxQuantity = $scope.laneWiseData[$scope.view.laneBtnSelectedData.btnKey].committedCount;
		            break;
		            case "lane3":
		            $scope.maxQuantity = $scope.laneWiseData[$scope.view.laneBtnSelectedData.btnKey].allocationCount;
		            break;
		            default:
		            $scope.maxQuantity = constantService.maxOrderLimit;
		        }
			}
			if($scope.maxQuantity < Number($scope.input.customConfig)){
				$scope.input.customConfig = $scope.maxQuantity;
			}
			if(($scope.input.customConfig.length > 2)||($scope.input.customConfig > $scope.maxQuantity)){
				   if($scope.input.customConfig > $scope.maxQuantity){
						$scope.input.customConfig = $scope.maxQuantity;
					}
					$scope.input.customConfig= $scope.input.customConfig.substr($scope.input.customConfig.length-2,$scope.input.customConfig.length);
					$scope.input.customConfig= parseInt($scope.input.customConfig);
			}
			
			if ($scope.input.customConfig == 0 || !$scope.input.customConfig) {
				$scope.input.customConfig="00";
				$scope.canReorder = false;
			} else if (($scope.imAddToCartFlag != null && $scope.imAddToCartFlag == 'false') || ($scope.imCurrentStockFlag != null && $scope.imCurrentStockFlag == 'false')) {
				$scope.canReorder = false;
			} else {
				if($scope.input.customConfig<10){
					$scope.input.customConfig="0"+parseFloat($scope.input.customConfig);
				}
				if($scope.input.customConfig > $scope.maxQuantity){
					$scope.canReorder = false;
				}
				else{
					$scope.wholeNumberError = false;
					$scope.canReorder = true;
				}
			}

		}

		$scope.decreaseQuantity = function() {
			$scope.input.customConfig=parseInt($scope.input.customConfig);
			if ($scope.input.customConfig > 0) {
				$scope.input.customConfig--;
			} else {
				$scope.input.customConfig = 0;
			}
			$scope.validateQuantity();
		}

		$scope.increaseQuantity = function() {
			$scope.input.customConfig=parseInt($scope.input.customConfig);
			if ($scope.input.customConfig >= constantService.maxOrderLimit) {
				$scope.input.customConfig = constantService.maxOrderLimit;
			} else {
				$scope.input.customConfig++;
			}
			$scope.validateQuantity();
		}

		$scope.navigateToReorder = function(arg1, arg2, saleModelCode) {
			reorderId = arg1;
			customOrder = $scope.input.customConfig;
		var currentStock = arg2;
			$window.location.href = "/checkout/review/" + reorderId
					+ "?reOrderProductQuantity=" + customOrder
					+ "&salesModelCode=" + saleModelCode;
		}

		 $scope.dismissCartLostPopup = function(){
			modalPopUpFactory.close($scope.cartLostPopup);
		 }
		 $scope.showSelectOptionsPopup = function(primaryImageUrl,dcaCode){
			$scope.fbcPrimaryImageUrl = primaryImageUrl;
			$scope.dcaCode = dcaCode;
			$scope.cartLostMessage = "Your current cart contains "+$scope.cartLaneType+" products, which would be lost. Do you want to continue?";
			if($scope.cartLaneType != '' && $scope.cartLaneType != $scope.view.laneBtnSelectedData.btnKey.toUpperCase() && $scope.cartLaneType != null  ){
				$scope.showSelectOptionsPopupInstance = $uibModal.open({
					ariaLabelledBy: 'modal-title',
					ariaDescribedBy: 'modal-body',
					templateUrl: '/_ui/responsive/cat/templates/fbcAddToCartPopup.html',
					scope: $scope,
					size: 'add-to-cart-redirect-modal',
				});
			} else {
				$scope.navigateToMustSelect();
			}
		}
		$scope.closeSelectOptionsPopup = function(){
			$scope.showSelectOptionsPopupInstance.close();
		}
		$scope.navigateToMustSelect = function(){
			$scope.prodFamily = window.location.href.split("/")[5];
			$window.location.href = "/fbc/?productCode="+$scope.productCode+"&salesModel="+$scope.salesModel+"&Lane="+$scope.view.laneBtnSelectedData.btnKey+"&prodFamily="+$scope.prodFamily+"&primaryImageUrl="+$scope.fbcPrimaryImageUrl+"&dcaCode="+$scope.dcaCode+"&qty="+$scope.input.customConfig+"&fromPage=Configurations";
		}
		 $scope.addToCart = function(event,isUTVCart,cartType){
			if(isToOpenCartLostPopup($scope.isUTVprod,isUTVCart)){
				$scope.cartLostMessage = properMessageService.cartLostMessage(cartType);
				$scope.cartLostPopup = modalPopUpFactory.open({
					backdrop: 'static',
					ariaLabelledBy: 'modal-title',
					ariaDescribedBy: 'modal-body',
					templateUrl: '/_ui/responsive/cat/templates/cartLostAlertPopup.html',
					size: 'md',
					scope: $scope,
				  });
				  return false;
			}else if($scope.cartLaneType && $scope.cartLaneType.toLowerCase() != $scope.view.laneBtnSelectedData.btnKey){
	            $scope.cartLostMessage = "Your current cart contains "+$scope.cartLaneType+" products, which would be lost. Do you want to continue?"
	            $scope.cartLostPopup = modalPopUpFactory.open({
	                backdrop: 'static',
	                ariaLabelledBy: 'modal-title',
	                ariaDescribedBy: 'modal-body',
	                templateUrl: '/_ui/responsive/cat/templates/clearLaneCartPopup.html',
	                size: 'md',
	                scope: $scope,
	            });
	            return;
	        }
			if(event){
				redirectToCartPage();
			}
			return true;
			
		 }
		 function redirectToCartPage(){
		 	if($scope.isRedirecting){
		 		return false;
		 	}
		 	$scope.isRedirecting = true;
			 httpService.postJsonData(catEndpointService.addNewProductUrl, $scope.requestBodyObj()).then(
					function(response){
						$window.location.href = (response && response.data)?response.data.redirectUrl:"/error";
					},
					function(){
						$window.location.href = "/error";
					}
				);
		 }
		 $scope.requestBodyObj = function() {
				if($scope.userTypeConfig == "IM" || ($scope.userTypeConfig == "UTV" && !$scope.isUTV)) {
					return {
						productCodePost:$("#productCodePostId").val(),
						qty:$scope.input.customConfig,
						laneType: ($scope.view.laneBtnSelectedData.btnKey == $scope.cartLaneType.toLowerCase() ? "" : $scope.view.laneBtnSelectedData.btnKey)
					 }
				} else {
					return {
						productCodePost:$("#productCodePostId").val(),
						qty:$scope.input.customConfig,
					}
				}
		 };
		 $scope.truckloadClearCart= function(data_obj){
			 httpService.get(catEndpointService.clearCartURL).then(function(response){
				if($scope.isUtvOrTruckloadFlow === constantService.truckloadFlow) {
					$scope.navigateToTruckLoad(data_obj.productCode,data_obj.defaultValue,null);
				} else {
					$scope.navigateToUTVRevieworder(data_obj.productCode,data_obj.defaultValue,null);
				}
			});
		 }
		 
		 $scope.clearCart = function(){
			var data_obj = modalPopUpFactory.memoiz('get','reorderData');

			 if(data_obj){
				modalPopUpFactory.close($scope.cartLostPopup);
				$scope.truckloadClearCart(data_obj)
				modalPopUpFactory.memoiz('remove','reorderData');
				return;
			 }
				
			 redirectToCartPage();
		 }
		 $scope.navigateToTruckLoad = function(arg1, arg2, isCartNotEmpty,cartType) {
			$scope.isUtvOrTruckloadFlow = constantService.truckloadFlow;
			if(isCartNotEmpty && cartType != 'emptyCart'){
				$scope.cartLostMessage = properMessageService.cartLostMessage(cartType);
				$scope.cartLostPopup = modalPopUpFactory.open({
					backdrop: 'static',
					ariaLabelledBy: 'modal-title',
					ariaDescribedBy: 'modal-body',
					templateUrl: '/_ui/responsive/cat/templates/cartLostAlertPopup.html',
					size: 'md',
					scope: $scope,
				  });
				modalPopUpFactory.memoiz('add','reorderData',{'productCode':arg1,'defaultValue':arg2});
				return;
			}
			var productCode=arg1;
			$window.location.href = catEndpointService.truckloadCheckoutUrl 
									+ "?productCode="+productCode;
		 }
		 $scope.navigateToUTVRevieworder = function(arg1,arg2,isCartNotEmpty,cartType){
			$scope.isUtvOrTruckloadFlow = constantService.utvFlow;
			if(isCartNotEmpty && cartType != 'emptyCart'){
				$scope.cartLostMessage = properMessageService.cartLostMessage(cartType); 
				$scope.cartLostPopup = modalPopUpFactory.open({
					backdrop: 'static',
					ariaLabelledBy: 'modal-title',
					ariaDescribedBy: 'modal-body',
					templateUrl: '/_ui/responsive/cat/templates/cartLostAlertPopup.html',
					size: 'md',
					scope: $scope,
				  });
				modalPopUpFactory.memoiz('add','reorderData',{'productCode':arg1,'defaultValue':arg2});
				return;
			}
			var productCode=arg1;
			var qty=[];
			var shipToDates=[];
			$("input[name=qty]").each(function() {
				qty.push($(this).val());
			});
	
			$("input[name=shipToDates]").each(function() {
				shipToDates.push($(this).val());
			});
			$window.location.href = "/cart/add?productCodePost="+productCode+"&quantities="+qty+"&shipToDates="+shipToDates;
		}
		// $scope.init();

		$scope.setConfigLengths = function(partLength, attachmentLength){
			$scope.partLength = partLength;
			$scope.attachmentLength = attachmentLength;
		}

        $scope.partReadMore = function() {
		$scope.partCustom = !$scope.partCustom;
	};

	$scope.attachmentReadMore = function() {
		$scope.attachmentCustom = !$scope.attachmentCustom;
	};

	} ]);
