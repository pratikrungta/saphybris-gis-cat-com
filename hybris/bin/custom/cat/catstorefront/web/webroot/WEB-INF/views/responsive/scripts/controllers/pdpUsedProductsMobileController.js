catApp.controller('usedProductsPDPMobileController',['$scope','$window', '$location','$timeout','catEndpointService', 'httpService', 'constantService' ,function($scope,$window,$location,$timeout, catEndpointService, httpService, constantService) {
    var vm = this;
    vm.defaultDownloadImage = constantService.defaultDownloadImage;
    vm.noOfPagesInMobileView = 9;
    vm.productCompareDropDownLabel = '';
    vm.pagination = {
        currentPage : 1,
        elementsForCurrentPage : 9
    };
    vm.accordionData =[ 
        {
           label :'BRANCH LOCATION',
           open : true
        },
        {
            label :'MANUFACTURING YEAR',
            open : true
        },
        {
            label :'HOURS USED',
            open : true
        },
        {
            label :'PRICE',
            open : true
        },
        {
            label :'CERTIFICATION',
            open : true
        }
    ];
    vm.items = [1,2,3,4,5,6,7,8,9,10];
    $scope.slider = {
        minValue: 10,
        maxValue: 90,
        options: {
            floor: 0,
            ceil: 100,
            step: 1,
            noSwitching: true
        }
    };
    vm.productListToDisplay = vm.items.slice(0,vm.pagination.elementsForCurrentPage);
    httpService.get(catEndpointService.productFamilyEnpoint).then(function(response){
        var urlValue = "";
        if(!!$location.search().categoryName){
          urlValue = $location.search().categoryName;
        }
		if(response.status == 200){
            vm.productCategories = response.data;

            if(!!urlValue){
                vm.productCategories.forEach(function(el,index){
                    if(el.name == urlValue){
                    vm.productCompareDropDownLabel = vm.productCategories[index];
                    }
                });
            }else{
            vm.productCompareDropDownLabel = vm.productCategories[0];
            }
		}
 	}, function(error){

    });
    vm.loadMoreProducts = function(){
        vm.noOfPagesInMobileView = vm.noOfPagesInMobileView + vm.pagination.elementsForCurrentPage;
        var noOfProducts = (vm.noOfPagesInMobileView < vm.items.length) ? vm.noOfPagesInMobileView : vm.items.length;
        vm.productListToDisplay = vm.items.slice(0,noOfProducts);
    }
    vm.updateDropDownValue = function(value){
        vm.productCompareDropDownLabel = value;
    };
    vm.toggleSidebar = function(e){
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    }
}]); 