catApp.controller('plpController', ['httpService', 'catEndpointService', '$scope', '$location', '$window', '$compile', 'constantService', '$timeout', 'catService', '$rootScope','screenResizeService', 'getUrlParamFactory', function(httpService, catEndpointService, $scope, $location, $window, $compile, constantService, $timeout, catService, $rootScope, screenResizeService, getUrlParamFactory) {
    
    var vm = this;
    vm.isPaginationReset = false;
    vm.defaultDownloadImage = constantService.defaultDownloadImage;
    vm.isMobile = false;
    var angularWindow = angular.element($window)
        , urlParams= getUrlParamFactory.getUrlParams();
    var windowWidth = angularWindow.width();
    angularWindow.bind('resize', function () {
        vm.updateMobileMetaData();
        //console.log('Resized your browser'+angularWindow.width());
    });

    vm.mobileMeta = {
        isMobile:false
    }
    
    vm.roleFeatureInfoSearch = function(isUTVUser,isIM,isSP){
        if (isUTVUser && isIM){
            ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.cuvUserEventName,ACC.gaHelper.searchEvent);
          }
          else if(!isUTVUser&& isIM ){
            ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.bcpUserEventName,ACC.gaHelper.searchEvent);
          }
          else if(isSP){
            ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.spUserEventName,ACC.gaHelper.searchEvent);
          }
    }
    vm.laneType= urlParams.laneType || "All";    

    vm.updateMobileMetaData = function(){
        if(angularWindow.width() <= parseInt(screenSmMax)){
            vm.mobileMeta.isMobile = true;
            vm.refreshSlider();
        }else{
            vm.mobileMeta.isMobile = false;
        }
        
        var groupCount = 3;
        if(vm.view.metadata.isUTVUser) {
            groupCount = 4;
        }
        if(vm.view.searchResults.length > 0 && !(angularWindow.width() <= parseInt(screenSmMax))){
            vm.view.searchResultsChunkOf3 = vm.view.searchResults.chunk(groupCount);  
        }else if(vm.view.searchResultsMobile.length > 0 && angularWindow.width() <= parseInt(screenSmMax)){
            vm.view.searchResultsChunkOf3 = vm.view.searchResultsMobile.chunk(groupCount);
        }
    }

    vm.view = {
        metadata: {
            isUTVUser: false,
            userType:'',
            isutilityProducts: false,
            canUpdateUsedProducts: true
        },
        productFamily: {
            isDisabled: false,
            isOpen: false,
            selectedProductName: "",
            selectedCode: ""
        },
        salesModel: {
            isDisabled: true,
            selectedModelName: "",
            selectedCode: "",
            listOfSalesModel: []
        },
        primaryOffering: {
            isDisabled: true,
            selectedPrimaryOffering: "",
            selectedCode: "",
            listOfPrimaryOffering: []
        },
        location: {
            isWareHouseChecked: false,
            iscatPDCChecked: false,
            isManufacturingChecked: false,
            hasWareHouseUnit: false,
            hascatPDCUnit: false,
            hasManufacturing: false
        },
        intransit:{
            ishidden: false,
        },
        searchResults: [],
        searchResultsDesktop: [],
        searchResultsMobile: [],
        searchResultsChunkOf3: [],
        searchResultsMetaData: "",
        hasValidSearchParams: false,
        pageLoadingParams: {
            searchResultsLoaded: false,
            salesModelLoaded: false,
            primaryOfferingLoaded: false,
            pagedLoaded: false,
            ajaxCallSuccess: true
        },
        pagination: {
            currentPage: 1,
            totalPages: 10,
            displayGroups: 3,
            elementsForCurrentPage: 8,
            startIndex: 1,
            totalItems: 100,
            hybrisPageNumber: 0,
            clickedOnPagination: false,
            currentElementDisplay: 1
        },
        intransitShow: false,
        viewIntransit: false,
        noViewIntransit: true,
        inTransitFlag : false,
        stockInTransit : 0,
        initialLoad: true,
        primaryOfferingForCards: "",
        productType: "new",
        isNewProduct: true,
        isUsedProduct: false,
        branchLocations: {
            inputValue: "",
            ishidden: false,
            masterSet: [],
            selectedSet: [],
            displaySize: 5,
            defaultSize: 5,
            fullSize: 100,
            maxSize: 10,
            viewMore: true,
            viewLess: false
        },
        year: {
            ishidden: false,
            minValue: 0,
            maxValue: 0,
            options: {
                floor: 0,
                ceil: 0,
                step: 1,
                onEnd: function() {
                    vm.sliderChanged();
                }
            }
        },
        hours: {
            ishidden: false,
            minValue: 0,
            maxValue: 0,
            options: {
                floor: 0,
                ceil: 0,
                step: 1,
                onEnd: function() {
                    vm.sliderChanged();
                }
            }
        },
        price: {
            ishidden: false,
            minValue: 0,
            maxValue: 0,
            options: {
                floor: 0,
                ceil: 0,
                step: 1,
                onEnd: function() {
                    vm.sliderChanged();
                }
            }
        },
        certifiedUser: {
            ishidden: false,
            isChecked: false
        },
        usedFiltersForCommunication: {
            selectedBranchLocation: [],
            manufacturingYearMin: "",
            manufacturingYearMax: "",
            hoursUsedMin: "",
            hoursUsedMax: "",
            priceMin: "",
            priceMax: "",
            defaultFacetValue: true,
            dealerCertified: false,
            backwardNavigationFlag: false
        },
        selectedLocations: "",
        selectedLocationsBkp: ""
    }

    vm.myChangeListener = function(sliderId) {
        //console.log(sliderId, 'has changed with ', vm.slider.value);
    }

    vm.toggleCertifiedFilter = function() {
        vm.view.certifiedUser.ishidden = !vm.view.certifiedUser.ishidden;
    }

    vm.toggleYearFilter = function() {
        vm.view.year.ishidden = !vm.view.year.ishidden;
    }
    vm.togglePriceFilter = function() {
        vm.view.price.ishidden = !vm.view.price.ishidden;
    }
    vm.toggleHoursFilter = function() {
        vm.view.hours.ishidden = !vm.view.hours.ishidden;
    }
    vm.toggleLocationFilter = function() {
        vm.view.branchLocations.ishidden = !vm.view.branchLocations.ishidden;
    }
    vm.toggleIntransitFilter = function() {
        vm.view.intransit.ishidden = !vm.view.intransit.ishidden;
    }
    vm.constantService = constantService;

    vm.updatePageLoadingParams = function() {
        //vm.view.pageLoadingParams.pagedLoaded = (vm.view.pageLoadingParams.searchResultsLoaded && vm.view.pageLoadingParams.salesModelLoaded && vm.view.pageLoadingParams.primaryOfferingLoaded);
        vm.view.pageLoadingParams.pagedLoaded = (vm.view.pageLoadingParams.searchResultsLoaded);
    }

    vm.setSearchParams = function(productFamily, salesModel, primaryOffering) {}


    vm.productFamilySelected = function(code, name) {
        vm.view.productFamily.selectedCode = code;
        vm.view.productFamily.selectedProductName = name;

        vm.view.salesModel.isDisabled = true;
        vm.view.primaryOffering.isDisabled = true;
        vm.view.hasValidSearchParams = false;


        vm.view.salesModel.selectedModelName = "";
        vm.view.primaryOffering.selectedPrimaryOffering = "";

        vm.view.primaryOffering.selectedCode = "";
        vm.view.salesModel.selectedCode = "";
        vm.view.productFamily.isOpen = false;



        httpService.get(catEndpointService.productFamilyEnpoint + "?isUsed="+vm.view.isUsedProduct+"&categoryCode=" + vm.view.productFamily.selectedCode).then(function(response) {
            if (response.status == 200) {
                vm.view.salesModel.listOfSalesModel = response.data;
                vm.view.salesModel.isDisabled = false;
            }
        }, function(error) {});
    },

    vm.salesModelSelected = function(code, name) {
        vm.view.salesModel.selectedCode = code;
        vm.view.salesModel.selectedModelName = name;
        vm.view.primaryOffering.selectedPrimaryOffering = "";
        vm.view.primaryOffering.selectedCode = "";

        vm.view.hasValidSearchParams = true;
        vm.view.primaryOffering.isDisabled = false;

        httpService.get(catEndpointService.salesModelEndpoint + "?isUsed="+vm.view.isUsedProduct+"&productCode=" + vm.view.salesModel.selectedCode).then(function(response) {
            if (response.status == 200) {
                vm.view.primaryOffering.listOfPrimaryOffering = response.data;
                vm.view.primaryOffering.isDisabled = false;
            }
        }, function(error) {});
    }

    vm.primaryOfferingSelected = function(code, name) {
        vm.view.hasValidSearchParams = true;
        vm.view.primaryOffering.isDisabled = false;
        vm.view.primaryOffering.selectedCode = name;
        vm.view.primaryOffering.selectedPrimaryOffering = name + " (" + code + ")";
    }

    vm.updateSearchData = function() {
        vm.view.usedFiltersForCommunication.defaultFacetValue = true;
        vm.view.certifiedUser.isChecked = false;
        vm.view.branchLocations.masterSet = [];
        vm.view.pageLoadingParams.searchResultsLoaded = false;
        vm.isPaginationReset = true;
        vm.updatePageLoadingParams();
        vm.resetLocationTypes();
        vm.retriveSearchData(); 
        vm.view.searchResultsMobile = [];
        vm.query= "";
        ACC.plp.hideSidebar();
    };

    vm.updateSearchDataMobile = function(){
        vm.view.usedFiltersForCommunication.defaultFacetValue = false;
        vm.view.pageLoadingParams.searchResultsLoaded = false;
        vm.updatePageLoadingParams();
        vm.retriveSearchData(); 
        vm.view.searchResultsMobile = [];
        ACC.plp.hideSidebar();
    };

    vm.makeLowerCase = function(string) {
        return angular.lowercase(string);
    };

    vm.serializeFormData = function() {
        var anyOfCheckBoxChecked = (vm.view.location.isWareHouseChecked || vm.view.location.iscatPDCChecked || vm.view.location.isManufacturingChecked);
        var show = 'Page';

        var inWarehouseFlag = (anyOfCheckBoxChecked) ? vm.view.location.isWareHouseChecked : true;
        var inStockPDCFlag = (anyOfCheckBoxChecked) ? vm.view.location.iscatPDCChecked : true;
        var inStockMFUFlag = (anyOfCheckBoxChecked) ? vm.view.location.isManufacturingChecked : true;
        var selectedBranchLocation=[];
        var arrayOfSelectedLocations = vm.view.selectedLocations.split("||");
        arrayOfSelectedLocations.pop();
        var stateObj = ""
        arrayOfSelectedLocations.forEach(function(value) {
            stateObj = stateObj + "&selectedBranchLocation=" + value;
            selectedBranchLocation.push(value);
        });
         var masterData = {
            "q":'',
            "category": vm.makeLowerCase(vm.view.productFamily.selectedCode),
            "salesModelId": vm.makeLowerCase(vm.view.salesModel.selectedCode),
            "primaryOfferingId": vm.makeLowerCase(vm.view.primaryOffering.selectedCode),
            "page": vm.isPaginationReset ? 0 : vm.view.pagination.hybrisPageNumber,
            "show": show,
            "isUsed": vm.view.isUsedProduct,
            "manufacturingYearMin": vm.view.year.minValue,
            "manufacturingYearMax": vm.view.year.maxValue,
            "hoursUsedMin": vm.view.hours.minValue,
            "hoursUsedMax": vm.view.hours.maxValue,
            "priceMin": vm.view.price.minValue,
            "priceMax": vm.view.price.maxValue,
            "defaultFacetValue": vm.view.usedFiltersForCommunication.defaultFacetValue,
            "dealerCertified": vm.view.certifiedUser.isChecked,
            "inTransitFlag": vm.view.inTransitFlag,
            "selectedBranchLocation":_.compact(selectedBranchLocation),
            "backwardNavigationFlag": vm.view.usedFiltersForCommunication.backwardNavigationFlag,
            "laneType":vm.laneType
        }
        //As the AJAX converted to a post, returning master data
        //var searchParam = $.param(masterData) + stateObj;
        //return searchParam;
        return masterData;
    }

    vm.updateBranchLocation = function(obj) {
        if (!!obj && !!obj.text && !!obj.isChecked) {
            vm.view.selectedLocations = vm.view.selectedLocations + obj.text + "||";
            vm.view.selectedLocationsBkp = vm.view.selectedLocationsBkp + obj.text + ",";
        } else {
            vm.view.selectedLocations = vm.view.selectedLocations.replace(obj.text + "||", "");
            vm.view.selectedLocationsBkp = vm.view.selectedLocationsBkp.replace(obj.text + ",", "");
        }
        vm.sliderChanged();
    }
    vm.retriveSearchData = function() {
        vm.backupSearchData();
        if (!!urlParams.isUsed && urlParams.isUsed=="true" && vm.view.metadata.canUpdateUsedProducts) {
            vm.resetUsedProduct();
        }
          var serialisedFormData = vm.serializeFormData();
          serialisedFormData.isUsed=vm.view.isUsedProduct;
        // var url = catEndpointService.searchResultsEndPoint + "?isUsed="+vm.view.isUsedProduct+"&" + serialisedFormData;
        
        vm.view.primaryOfferingForCards = vm.view.productFamily.selectedProductName;
        httpService.get(catEndpointService.orderingWindowForLanes).then(function(response){
            if(response.status === 200){
                vm.lane2Msg = lane2MsgCreation(response.data.lane2);
                vm.lane3Msg = lane3MsgCreation(response.data.lane3);
                vm.allLanesMsg = allLanesMsg(response.data);
            }
        });
        angular.element("#pageLoader").show();
        httpService.postJsonData(catEndpointService.searchResultsEndPoint,serialisedFormData).then(
            function(response) {
            angular.element("#pageLoader").hide();
            if (response.status == 200) {
                vm.isPaginationReset = false;
               
                vm.view.pageLoadingParams.searchResultsLoaded = true;
                vm.view.pageLoadingParams.ajaxCallSuccess = true;

                vm.updatePageLoadingParams();
                vm.view.searchResults = [];
                response.data.results.forEach(function(item) {
                    item.primaryOfferingForCards = vm.view.productFamily.selectedProductName;
                    vm.view.searchResults.push(item);
                    vm.view.searchResultsMobile.push(item);
                });

                var groupCount = 3;
                if(vm.view.metadata.isUTVUser) {
                    groupCount = 4;
                }
                if(vm.view.searchResults.length > 0 && !(angularWindow.width() <= parseInt(screenSmMax))){
                    vm.view.searchResultsChunkOf3 = vm.view.searchResults.chunk(groupCount);  
                }else if(angularWindow.width() <= parseInt(screenSmMax)){
                    vm.view.searchResultsChunkOf3 = vm.view.searchResultsMobile.chunk(groupCount);
                }

                vm.view.searchResultsMetaData = response.data.pagination;
                vm.view.pagination.totalItems = response.data.pagination.totalNumberOfResults;
                vm.view.pagination.elementsForCurrentPage = response.data.pagination.pageSize;
                vm.view.pagination.currentPage = response.data.pagination.currentPage+1;
                vm.view.pagination.currentElementDisplay = (vm.view.pagination.elementsForCurrentPage * vm.view.pagination.currentPage);
                if (vm.view.pagination.currentElementDisplay >= vm.view.pagination.totalItems) {
                    vm.view.pagination.currentElementDisplay = vm.view.pagination.totalItems;
                }

                vm.view.location.hasWareHouseUnit = (!!response.data.warehouseMap) ? $.parseJSON(response.data.warehouseMap.inDealerWS) :
                    true;
                if (!vm.view.location.hasWareHouseUnit) {
                    vm.view.location.isWareHouseChecked = false;
                }
                vm.view.location.hascatPDCUnit = (!!response.data.warehouseMap) ? $.parseJSON(response.data.warehouseMap.inPDC) :
                    true;
                if (!vm.view.location.hascatPDCUnit) {
                    vm.view.location.iscatPDCChecked = false;
                }
                vm.view.location.hasManufacturing = (!!response.data.warehouseMap) ? $.parseJSON(response.data.warehouseMap.inMFU) :
                    true;
                if (!vm.view.location.hasManufacturing) {
                    vm.view.location.isManufacturingChecked = false;
                }

                if (vm.view.usedFiltersForCommunication.defaultFacetValue || vm.view.usedFiltersForCommunication.backwardNavigationFlag) {
                    vm.view.usedFiltersForCommunication.defaultFacetValue = false;
                    if (!!response.data.remainingUsedFacetMap && !!response.data.remainingUsedFacetMap.BRANCH_LOCATION && response.data.remainingUsedFacetMap.BRANCH_LOCATION.length >0) {
                        response.data.remainingUsedFacetMap.BRANCH_LOCATION.forEach(function(value, index) {
                            var obj = {
                                id: index,
                                text: value,
                                isChecked: false
                            };
                            vm.view.branchLocations.masterSet.push(obj);
                        });

                        vm.view.branchLocations.fullSize = vm.view.branchLocations.masterSet.length;
                    }



                    if (!!response.data.usedFacetMap && !!response.data.usedFacetMap.HOURS_USED_MIN && !!Number(response.data.usedFacetMap.HOURS_USED_MIN)) {
                        vm.view.hours.minValue = Math.ceil(Number(response.data.usedFacetMap.HOURS_USED_MIN));
                        vm.view.hours.options.floor = Math.ceil(Number(response.data.usedFacetMap.HOURS_USED_MIN));

                    }
                    if (!!response.data.usedFacetMap && response.data.usedFacetMap.HOURS_USED_MAX) {
                        vm.view.hours.maxValue = Math.ceil(Number(response.data.usedFacetMap.HOURS_USED_MAX));
                        vm.view.hours.options.ceil = Math.ceil(Number(response.data.usedFacetMap.HOURS_USED_MAX));

                    }
                    if (!!response.data.usedFacetMap && response.data.usedFacetMap.MANUFACTURING_YEAR_MIN) {
                        vm.view.year.minValue = Math.ceil(Number(response.data.usedFacetMap.MANUFACTURING_YEAR_MIN));
                        vm.view.year.options.floor = Math.ceil(Number(response.data.usedFacetMap.MANUFACTURING_YEAR_MIN));

                    }
                    if (!!response.data.usedFacetMap && response.data.usedFacetMap.MANUFACTURING_YEAR_MAX) {
                        vm.view.year.maxValue = Math.ceil(Number(response.data.usedFacetMap.MANUFACTURING_YEAR_MAX));
                        vm.view.year.options.ceil = Math.ceil(Number(response.data.usedFacetMap.MANUFACTURING_YEAR_MAX));

                    }
                    if (!!response.data.usedFacetMap && response.data.usedFacetMap.PRICE_MIN) {
                        vm.view.price.minValue = Math.ceil(Number(response.data.usedFacetMap.PRICE_MIN));
                        vm.view.price.options.floor = Math.ceil(Number(response.data.usedFacetMap.PRICE_MIN));

                    }
                    if (!!response.data.usedFacetMap && response.data.usedFacetMap.PRICE_MAX) {
                        vm.view.price.maxValue = Math.ceil(Number(response.data.usedFacetMap.PRICE_MAX));
                        vm.view.price.options.ceil = Math.ceil(Number(response.data.usedFacetMap.PRICE_MAX));
                    }

                    vm.refreshSlider();
                }

                if (vm.view.searchResults.length == 0) {
                    // vm.view.searchResults();
                    vm.view.pageLoadingParams.searchResultsLoaded = true;
                    vm.view.pageLoadingParams.ajaxCallSuccess = true;
                }
                if (!!urlParams.isUsed && urlParams.isUsed=="true" && vm.view.metadata.canUpdateUsedProducts) {
                    vm.view.metadata.canUpdateUsedProducts = false;
                    vm.setBranchLocations();
                    vm.resetUsedProduct();
                }
             
            } else {
                vm.view.pageLoadingParams.ajaxCallSuccess = false;
                vm.view.pageLoadingParams.searchResultsLoaded = true;
                vm.updatePageLoadingParams();
            }
            vm.bkpQueryParams();
            vm.view.usedFiltersForCommunication.backwardNavigationFlag = false;
        }, function(error) {
            vm.view.pageLoadingParams.ajaxCallSuccess = false;
            vm.view.pageLoadingParams.searchResultsLoaded = true;
            vm.updatePageLoadingParams();
        });
        

    };
    var lane2MsgCreation =function(lane2Obj){
        if(lane2Obj.orderWindowStatus==='open'){
            if(lane2Obj.leftDays>1){
                return "Ordering window for Base Orders - Lane 2 products closes in "+ lane2Obj.leftDays +" days." 
            }
            return "Ordering window for Base Orders - Lane 2 products closes today.";
        }
        if(lane2Obj.orderWindowStatus==='closed'){
            if(lane2Obj.leftDays>1){
                return "Ordering window for Base Orders - Lane 2 products opens in "+ lane2Obj.leftDays +" days." ;
            }
            return "Ordering window for Base Orders - Lane 2 products opens in "+ lane2Obj.leftDays +" day." ;
        }
    };
    var lane3MsgCreation =function(lane3Obj){
        if(lane3Obj.orderWindowStatus==='open'){
            if(lane3Obj.leftDays>1){
                return "Ordering window for Allocation Only - Lane 3 products closes in "+ lane3Obj.leftDays +" days." 
            }
            return "Ordering window for Allocation Only - Lane 3 products closes today.";
        }
        if(lane3Obj.orderWindowStatus==='closed'){
            if(lane3Obj.leftDays>1){
                return "Ordering window for Allocation Only - Lane 3 products opens in "+ lane3Obj.leftDays +" days." ;
            }
            return "Ordering window for Allocation Only - Lane 3 products opens in "+ lane3Obj.leftDays +" day." ;
        }
    };
    
    var allLanesMsg = function(laneObj){
        if(laneObj.lane2.orderWindowStatus==='open'){
           return lane2MsgCreation(laneObj.lane2);
        } else if (laneObj.lane3.orderWindowStatus==='open') {
            return lane3MsgCreation(laneObj.lane3);
        } else if (laneObj.lane2.orderWindowStatus==='closed' && laneObj.lane3.orderWindowStatus==='closed') {
            if(laneObj.lane2.leftDays < laneObj.lane3.leftDays) {
                if(laneObj.lane2.leftDays > 1) {
                    return "Ordering window for Base Orders - Lane 2 products opens in "+ laneObj.lane2.leftDays +" days." ;
                } else {    
                    return "Ordering window for Base Orders - Lane 2 products opens in "+ laneObj.lane2.leftDays +" day." ;
                }
            } else {
                if(laneObj.lane3.leftDays > 1) {
                    return "Ordering window for Allocation Only - Lane 3 products opens in "+ laneObj.lane3.leftDays +" days." ;
                } else {    
                    return "Ordering window for Allocation Only - Lane 3 products opens in "+ laneObj.lane3.leftDays +" day." ;
                }
            }
        }
    }

    vm.loadMoreMobileResults = function(){
        vm.view.pagination.currentPage = vm.view.pagination.currentPage+1;
        if ((vm.view.pagination.currentPage - 1) > -1) {
            vm.view.pagination.hybrisPageNumber = (vm.view.pagination.currentPage - 1);
        } 
        vm.retriveSearchData();
    }

    vm.redirect = function(url) {
        url += "?category=" + catService.selectedProductFamilyCode + "&salesModelId=" + catService.selectedSalesModelCode + "&primaryOfferingId=" + catService.selectedPrimaryOfferingCode + "&pdcFlag=" + catService.plp.isPDCChecked + "&mfuFlag=" + catService.plp.isMFUChecked + "&wsFlag=" + catService.plp.isWarehouseChecked + "&page=" + catService.plp.currentPage + "&show=page";
        if (catService.plp.isUsed) {
            url += "&branchLocation=" + vm.view.selectedLocationsBkp + "&manufacturingYearMin=" + vm.view.year.minValue + "&manufacturingYearMax=" + vm.view.year.maxValue + "&hoursUsedMin=" + vm.view.hours.minValue + "&hoursUsedMax=" + vm.view.hours.maxValue + "&priceMin=" + vm.view.price.minValue + "&priceMax=" + vm.view.price.maxValue + "&dealerCertified=" +  vm.view.certifiedUser.isChecked + "&isUsed=" + catService.plp.isUsed;
        }
        $window.location.href = url;
    }

    vm.bkpQueryParams = function(){
        catService.queryParams.hasBkpData = true;
        catService.queryParams.bkp= "?category=" + catService.selectedProductFamilyCode + "&salesModelId=" + catService.selectedSalesModelCode + "&primaryOfferingId=" + catService.selectedPrimaryOfferingCode + "&pdcFlag=" + catService.plp.isPDCChecked + "&mfuFlag=" + catService.plp.isMFUChecked + "&wsFlag=" + catService.plp.isWarehouseChecked + "&page=" + catService.plp.currentPage + "&show=page";
        if (catService.plp.isUsed) {
            catService.queryParams.bkp= catService.queryParams.bkp+"&branchLocation=" + vm.view.selectedLocationsBkp + "&manufacturingYearMin=" + vm.view.year.minValue + "&manufacturingYearMax=" + vm.view.year.maxValue + "&hoursUsedMin=" + vm.view.hours.minValue + "&hoursUsedMax=" + vm.view.hours.maxValue + "&priceMin=" + vm.view.price.minValue + "&priceMax=" + vm.view.price.maxValue + "&dealerCertified=" +  vm.view.certifiedUser.isChecked + "&isUsed=" + catService.plp.isUsed;
        }
    }
    vm.orderingWindowForLanes = function(laneType){
        vm.view.searchResultsMobile = [];
        vm.laneType = laneType;
        vm.retriveSearchData();
    }
    vm.viewMore = function($event) {
        $($event.target).parents(".item-desc").children(".item-details").show();
        $($event.target).hide();

    }
    vm.viewLess = function($event) {
        $($event.target).parents(".item-desc").children(".item-details").hide();
        $($event.target).parents(".item-desc").find(".view-more").show();
    }

    vm.setProductFamily = function(code) {
        if (!!code) {
            vm.view.productFamily.selectedProductName = vm.view.productFamily.selectedCode = code;
        }
    }

    vm.setSalesModel = function(code) {
        if (!!code) {
            vm.view.salesModel.selectedModelName = vm.view.salesModel.selectedCode = code;
        }
    }

    vm.setPrimaryOffering = function(code) {
        if (!!code) {
            vm.view.primaryOffering.selectedPrimaryOffering = vm.view.primaryOffering.selectedCode = code;
        }
    }

    vm.userType = function(userType){
        if(!!userType && userType == catService.userTypeUTV){
            vm.view.metadata.userType = catService.userTypeUTV;
            vm.isUtilityProducts();
        }
        if(!(!!urlParams.isUsed && urlParams.isUsed=="true")){
            vm.view.intransitShow=true;
            vm.view.viewIntransit=true;
            vm.view.noViewIntransit=false;
        }
    }

    vm.utilityProducts = function(utilityProducts){
      if(!!utilityProducts && utilityProducts == 'true'){
            vm.view.metadata.isutilityProducts = true;
            vm.isUtilityProducts();
        }  
    }

    vm.isUtilityProducts = function(){
        vm.view.metadata.isUTVUser = (!!vm.view.metadata.isutilityProducts && !!(vm.view.metadata.userType == 'UTV'));
    }

    vm.setBranchLocations = function(){
        if (!!urlParams.isUsed && urlParams.isUsed=="true") {
            if(!!urlParams.branchLocation){
                vm.view.selectedLocationsBkp = urlParams.branchLocation.split(',');
                // vm.view.selectedLocationsBkp.pop();
                for(var i = 0; i < vm.view.branchLocations.masterSet.length; i++){
                    if(vm.view.selectedLocationsBkp.indexOf(vm.view.branchLocations.masterSet[i].text) != -1){
                        vm.view.branchLocations.masterSet[i].isChecked = true;
                    }
                }
                vm.view.selectedLocationsBkp = urlParams.branchLocation + ',';
            }
        }
    }
    vm.resetUsedProduct = function(){
        if (!!urlParams.isUsed && urlParams.isUsed=="true") {
            if(!!urlParams.branchLocation){
                vm.view.selectedLocations = urlParams.branchLocation.replace(/,/g, '||');
                vm.view.selectedLocations += '||';
            }
            if(!!urlParams.manufacturingYearMin){
                vm.view.year.minValue = Math.ceil(Number(urlParams.manufacturingYearMin));
            }
            if(!!urlParams.manufacturingYearMax){
                vm.view.year.maxValue = Math.ceil(Number(urlParams.manufacturingYearMax));
            }
            if(!!urlParams.hoursUsedMin){
                vm.view.hours.minValue = Math.ceil(Number(urlParams.hoursUsedMin));
            }
            if(!!urlParams.hoursUsedMax){
                vm.view.hours.maxValue = Math.ceil(Number(urlParams.hoursUsedMax));
            }
            if(!!urlParams.priceMin){
                vm.view.price.minValue = Math.ceil(Number(urlParams.priceMin));
            }
            if(!!urlParams.priceMax){
                vm.view.price.maxValue = Math.ceil(Number(urlParams.priceMax));
            }
            if(!!urlParams.dealerCertified){
                vm.view.certifiedUser.isChecked = urlParams.dealerCertified;
            }
            if(!!urlParams.defaultFacetValue){
                vm.view.usedFiltersForCommunication.defaultFacetValue = (urlParams.defaultFacetValue != "false");
            }
            if(!!urlParams.backwardNavigationFlag){
                vm.view.usedFiltersForCommunication.backwardNavigationFlag = (urlParams.backwardNavigationFlag != "false");
            }
        }
    }

    vm.init = function() {
        vm.updateSearchBackupData();
        vm.retriveSearchData();
        vm.updateMobileMetaData();
        if (!!vm.view.productFamily.selectedProductName) {
            httpService.get(catEndpointService.productFamilyEnpoint + "?isUsed="+vm.view.isUsedProduct+"&categoryCode=" + vm.view.productFamily.selectedProductName).then(function(response) {
                vm.view.pageLoadingParams.salesModelLoaded = true;
                vm.updatePageLoadingParams();
                if (response.status == 200) {
                    vm.view.salesModel.listOfSalesModel = response.data;
                    (response.data).forEach(function(el) {
                        if (!!el && (el.code == vm.view.salesModel.selectedCode)) {
                            vm.view.salesModel.selectedModelName = el.name;
                        }
                    });
                    vm.view.salesModel.isDisabled = false;
                }
            }, function(error) {
                vm.view.pageLoadingParams.salesModelLoaded = true;
                vm.updatePageLoadingParams();
            });
        } else {
            vm.view.pageLoadingParams.salesModelLoaded = true;
            vm.updatePageLoadingParams();
        }
        if (!!vm.view.salesModel.selectedModelName) {
            httpService.get(catEndpointService.salesModelEndpoint + "?isUsed="+vm.view.isUsedProduct+"&productCode=" + vm.view.salesModel.selectedModelName).then(function(response) {
                vm.view.pageLoadingParams.primaryOfferingLoaded = true;
                vm.updatePageLoadingParams();
                if (response.status == 200) {
                    vm.view.primaryOffering.listOfPrimaryOffering = response.data;
                    vm.view.primaryOffering.isDisabled = false;

                    (response.data).forEach(function(el) {
                        if (!!el && (el.code == vm.view.primaryOffering.selectedCode)) {
                            vm.view.primaryOffering.selectedPrimaryOffering = el.name + " (" + el.code + ")";
                        }
                    });
                    vm.view.hasValidSearchParams = true;
                }
            }, function(error) {
                vm.view.pageLoadingParams.primaryOfferingLoaded = true;
                vm.updatePageLoadingParams();
            });
        } else {
            vm.view.pageLoadingParams.primaryOfferingLoaded = true;
            vm.updatePageLoadingParams();
        }


    }

    vm.backupSearchData = function() {
        var anyOfCheckBoxChecked = (vm.view.location.isWareHouseChecked || vm.view.location.iscatPDCChecked || vm.view.location.isManufacturingChecked);
        catService.selectedProductFamilyCode = vm.view.productFamily.selectedCode;
        catService.selectedSalesModelCode = vm.view.salesModel.selectedCode;
        catService.selectedPrimaryOfferingCode = vm.view.primaryOffering.selectedCode;
        catService.plp.isPDCChecked = (anyOfCheckBoxChecked) ? vm.view.location.iscatPDCChecked : false;
        catService.plp.isMFUChecked = (anyOfCheckBoxChecked) ? vm.view.location.isManufacturingChecked : false;
        catService.plp.isWarehouseChecked = (anyOfCheckBoxChecked) ? vm.view.location.isWareHouseChecked : false;
        catService.plp.currentPage = vm.view.pagination.hybrisPageNumber;

    }

    vm.updateSearchBackupData = function() {
        var anyOfCheckBoxChecked = (catService.plp.isPDCChecked || catService.plp.isMFUChecked || catService.plp.isWarehouseChecked);
        vm.view.location.isWareHouseChecked = (anyOfCheckBoxChecked) ? catService.plp.isWarehouseChecked : false;
        vm.view.location.iscatPDCChecked = (anyOfCheckBoxChecked) ? catService.plp.isPDCChecked : false;
        vm.view.location.isManufacturingChecked = (anyOfCheckBoxChecked) ? catService.plp.isMFUChecked : false;
        vm.view.pagination.hybrisPageNumber = catService.plp.currentPage;
        vm.view.pagination.currentPage = (catService.plp.currentPage + 1);
        if (catService.plp.isUsed) {
            vm.view.productType = "used";
            vm.view.isNewProduct = false;
            vm.view.isUsedProduct = true;
        }

    }
    vm.intransitDataChecked =function(){

        vm.view.pageLoadingParams.searchResultsLoaded = false;
        if(!vm.mobileMeta.isMobile){
            vm.retriveSearchData();
        }
    }
    vm.locationTypesModified = function(event) {
        vm.view.pageLoadingParams.searchResultsLoaded = false;
        if(!vm.mobileMeta.isMobile){
            vm.retriveSearchData();
        }
    }

    vm.cloneMobileElement = function() {

        var iE1 = angular.element(document.querySelector('#product-facet'));
        var iE2 = angular.element(document.querySelector('#nav-mobile-selection'));
        iE2.append($compile(iE1.clone())($scope));
    }

    vm.pageChanged = function() {
        vm.view.pagination.hybrisPageNumber = 0;
        if ((vm.view.pagination.currentPage - 1) > -1) {
            vm.view.pagination.hybrisPageNumber = (vm.view.pagination.currentPage - 1);
        }

        vm.view.pagination.clickedOnPagination = true;
        vm.view.pageLoadingParams.searchResultsLoaded = false;
        vm.updatePageLoadingParams();
        vm.retriveSearchData();
    }

    vm.scrollTop = function() {}

    vm.applyMasonary = function() {
        $timeout(function() {
            $('#grid').masonry({
                itemSelector: '.item',
                columnWidth: ".item"
            });
            $('#grid').imagesLoaded(function() {
                $('#grid').masonry({
                    itemSelector: '.item',
                    columnWidth: ".item"
                });
            });
        }, 0);
    }

    vm.sliderChanged = function() {
        vm.view.pageLoadingParams.searchResultsLoaded = false;
        if(!vm.mobileMeta.isMobile){
            vm.retriveSearchData();
        }
    }

    vm.resetLocationTypes = function() {
        vm.view.location.isWareHouseChecked = false;
        vm.view.location.iscatPDCChecked = false;
        vm.view.location.isManufacturingChecked = false;

        vm.view.location.hasWareHouseUnit = false;
        vm.view.location.hascatPDCUnit = false;
        vm.view.location.hasManufacturing = false;
    }

    vm.viewAllLocations = function(){
        vm.view.branchLocations.displaySize = vm.view.branchLocations.fullSize;
        vm.view.branchLocations.viewMore = false;
        vm.view.branchLocations.viewLess = true;


    }

    vm.viewLessLocations = function(){
        vm.view.branchLocations.displaySize = vm.view.branchLocations.defaultSize;
        vm.view.branchLocations.viewMore = true;
         vm.view.branchLocations.viewLess = false;

    }

    vm.refreshSlider = function() {

        $timeout(function() {
            $scope.$broadcast('rzSliderForceRender');
            $scope.$broadcast('reCalcViewDimensions');
        }, 100);
    };

    $window.plpSliderRefreshFunction = vm.refreshSlider;

    vm.validateMinInRange = function(value, min, max){
        if(isNaN(value)){
            return min;
        }else{
            var curentValue = Number(value);
            var minValue = Number(min);
            var maxValue = Number(max);

            if(curentValue >= maxValue)
            {
                return min;
            }else if(curentValue <= min){
                return min;
            }
            else{
                return value;
            }
        }
    }

    vm.validateMaxInRange = function(value, max, min){
        if(isNaN(value)){
            return max;
        }else{
            var curentValue = Number(value);
            var minValue = Number(min);
            var maxValue = Number(max);

            if(curentValue >= maxValue)
            {
                return max;
            }else if(curentValue <= minValue){
                return max;
            }
            else{
                return value;
            }
        }
    }

    vm.closeMobleFilters = function(){
        ACC.plp.hideSidebar();
    }

    vm.refreshSlider();

}]);
