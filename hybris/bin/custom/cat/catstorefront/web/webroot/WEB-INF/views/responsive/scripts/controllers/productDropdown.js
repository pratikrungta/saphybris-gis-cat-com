catApp.filter('capitalize', function() { 
    return function(input) { 
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : ''; 
    } 
});
catApp.controller('productDropDownController', ['httpService', 'catEndpointService', '$scope', '$location', '$window', '$compile', 'constantService', '$timeout', 'catService', function(httpService, catEndpointService, $scope, $location, $window, $compile, constantService, $timeout, catService) {
    var vm = this;
    vm.dropdownLabel = 'Select';
    vm.productCompareDropDownLabel = {
        'name' : vm.dropdownLabel
    };
    vm.updateDropDownValue = function(value){
        vm.productCompareDropDownLabel = value;
    };
    vm.fetchProductCategories = function(){
    	httpService.get(catEndpointService.productFamilyEnpointComparePLP).then(function(response){
 			//console.log(response);
			if(response.status == 200){
				vm.productCategories = response.data;
			}
 		}, function(error){

 		});
    };
    vm.navigateToConfigureScreen = function(){
     if(vm.productCompareDropDownLabel.name !== vm.dropdownLabel){
            $window.location.href = "/speccheck/viewSpeccheckPLP/?categoryName="+vm.productCompareDropDownLabel.name;
        }
    };
}]);
