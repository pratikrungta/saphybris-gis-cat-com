catApp.controller('profilePageController', ['httpService', 'catEndpointService','specCheckHttpService', '$scope','$uibModal', function(httpService, catEndpointService, specCheckHttpService,$scope,$uibModal) {
    var vm = this;
    vm.roleFeatureInfoProfile = function(isUTVUser,isIM,isSP){
        if (isUTVUser && isIM){
          ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.cuvUserEventName,ACC.gaHelper.viewProfileEvent);
        }
        else if(!isUTVUser&& isIM ){
          ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.bcpUserEventName,ACC.gaHelper.viewProfileEvent);
        }
        else if(isSP){
          ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.spUserEventName,ACC.gaHelper.viewProfileEvent);
        }
      }
}]);