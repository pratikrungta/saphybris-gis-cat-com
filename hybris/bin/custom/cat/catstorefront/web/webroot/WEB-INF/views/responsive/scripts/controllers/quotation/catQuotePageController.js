catApp.controller('catQuotePageController',['$scope','quoteService','$timeout','screenResizeService',function($scope,
    quoteService,$timeout,screenResizeService){
    var catPage = this;
     $scope.watchPages = quoteService.quoteInfo;
     catPage.quoteMetaData = quoteService.quoteInfo.metaData;
     catPage.screenResizeService = screenResizeService;
     
     catPage.callChildLostProductPopupFn = function(quoteCode){
        $scope.$broadcast('showLostProductPopup',quoteCode);
     }
     catPage.initButtonState=function(btnState){
        $scope.$broadcast('initButtonState',btnState);
     }
     catPage.beforeUnloadFunction = function(event) {
        event.returnValue = "Are you sure , you wanted to navigate out of Page?";
        
    }

    catPage.addBeforeUnloadEvent = function() {
    		window.addEventListener("beforeunload", catPage.beforeUnloadFunction);  
    }

    catPage.removeBeforeUnloadEvent = function() {
        window.removeEventListener("beforeunload", catPage.beforeUnloadFunction);
        window.removeEventListener('load',function(){
        });
    }

    window.addEventListener('load',function(){
        $timeout(function(){catPage.watch()},500);
    });

    $scope.$on('submittedQuote',function(e){
        catPage.removeBeforeUnloadEvent();
        e.preventDefault();
    });

    catPage.watch = function(){
    	
        var unregister = $scope.$watch('watchPages', function(n,o) {
        	
        		 if(!angular.equals(n,o)){
        		    	if(window.location.href.indexOf("/edit?firstTimeEdit") != -1)
        		     	{
        				 catPage.addBeforeUnloadEvent();
        		     	}
                     unregister();
                 }
           
          },true);
     	
    };
}])