catApp.controller('quotationCartController', ['httpService', 'catEndpointService', '$scope', '$location', '$window', '$compile', 'constantService', '$timeout', '$uibModal', 'catService', function(httpService, catEndpointService, $scope, $location, $window, $compile, constantService, $timeout, $uibModal, catService) {
    var vm = this;
    vm.ajaxSuccess = true;
    var emptyCart = 1;
    vm.getFormattedText = constantService.getTwoDigitFormattedText;
    vm.defaultDownloadImage = constantService.defaultDownloadImage;

    vm.roleFeatureInfoQoute = function(){
        ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.spUserEventName,ACC.gaHelper.spQoutationEventName);
    }

    vm.showRemoveItemPopup = function(index) {
        $scope.displayRemoveText = (vm.cartProducts.entries.length === 1) ? constantService.quote.lastProductRemove : constantService.quote.productRemove;
        $scope.removeCartProductIndex = index;
        vm.modalInstance = $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: '/_ui/responsive/cat/templates/quotationRemoveItemPopup.html',
            scope: $scope,
            size: 'quotation-remove-item',
        });
    };
    $scope.close = function() {
        vm.modalInstance.dismiss("cancel");
    };
    $scope.deleteCartProducts = function(index) {
        var entryNumber = vm.cartProducts.entries[index].entryNumber;
        var cartDeleteURL = catEndpointService.quotationCartPageDeleteEndPoint + '?entryNumber=' + entryNumber;
        vm.modalInstance.dismiss("cancel");
        vm.ajaxSuccess = false;
        httpService.get(cartDeleteURL).then(function(response) {
            vm.cartProducts = response.data;
            if (vm.cartProducts.entries.length === 0) {
                emptyCart = 1;
                vm.isEmptyCart = emptyCart;
            } else {
                emptyCart = 0;
                vm.isEmptyCart = emptyCart;
            }
            vm.ajaxSuccess = true;
            ACC.minicart.refreshMinicartCountNum();
        }, function(error) {

        });
    };
    vm.updateCartProducts = function(index, operation) {
        var qty = $(".cart-product-row").eq(index).find(".quantity .quantity-box").val();
        var entryNumber = vm.cartProducts.entries[index].entryNumber;
        if (qty !== "") {
            qty = Number(qty); 
            if (operation === "increment")  {   qty = parseInt(qty) + 1;  } else if (operation === "decrement" && qty !== 1) {   qty = parseInt(qty) - 1;  } 
            var cartUpdateURL = catEndpointService.quotationCartPageUpdateEndPoint + '?entryNumber=' + entryNumber + '&qty=' + qty;
            vm.ajaxSuccess = false;
            if (qty > 0){
                httpService.get(cartUpdateURL).then(function(response) {
                    vm.cartProducts = response.data;
                    if (vm.cartProducts.entries.length === 0) {
                        emptyCart = 1;
                        vm.isEmptyCart = emptyCart;
                    } else {
                        emptyCart = 0;
                        vm.isEmptyCart = emptyCart;
                    }
                    vm.ajaxSuccess = true;
                    ACC.minicart.refreshMinicartCountNum();
                }, function(error) {

                });
            }
        }
        else{
          $(".cart-product-row").eq(index).find(".quantity .quantity-box").val("1");
        }
    };

    vm.loadCartProducts =   function(mediaPrefix) {
        vm.mediaPrefix = mediaPrefix;
        httpService.get(catEndpointService.quotationCartPageEndPoint).then(function(response) {    
            vm.cartProducts = response.data;      
            if (!vm.cartProducts.entries || vm.cartProducts.entries.length === 0) {
                emptyCart = 1;
                vm.isEmptyCart = emptyCart;
            } else  {
                emptyCart = 0;
                vm.isEmptyCart = emptyCart;
            }
        },  function(error) {     });
    }


    vm.navigateToHome = function() {
        $window.location.href = constantService.homeURL;
    };
    vm.navigateToQuote = function() {
        $window.location.href = constantService.quoteURL;
    };

    vm.Prevent = function(event) {
        if (event.target.value == '0') {
            event.target.value = '1';
        }
    }
}]);
