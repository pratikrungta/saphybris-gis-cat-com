catApp.controller('quotationCustomerDetailsController', ['httpService', 'catEndpointService', '$scope', '$location', '$window', '$compile', 'constantService', '$timeout', '$uibModal', 'catService', 'quoteService', function(httpService, catEndpointService, $scope, $location, $window, $compile, constantService, $timeout, $uibModal, catService, quoteService) {
    var vm = this;
    vm.view = {}
    vm.view.customerData = quoteService.quoteInfo.customerData;
    vm.view.quoteMetaData = quoteService.quoteInfo.metaData;
    vm.enableEditButton = "true";
    vm.isReplicate = '';
    vm.isNameUpdated = false;

    vm.showLostProductPopup = function(quoteCode){
    	vm.editQuoteCode = quoteCode;
    	var editQuoteURL = '/quote/'+quoteCode+'/editCheck?removeSessionCart=false';
		vm.callReplicateQuoteAPI(editQuoteURL);
        
    };
    vm.updateReplicateBool = function(isReplicate){
        vm.isReplicate = isReplicate;
    };

    vm.watchCallStack = function(){
        $scope.$watch(function() {
            return vm.view.customerData.customerName.value;
        }, function(newVal, oldVal) {
            if(newVal != oldVal && vm.isReplicate && !vm.isNameUpdated){
                vm.isNameUpdated = true;
                vm.onCustomerNameSelect(vm.view.customerData.customerName.value);
            }
        }, true);

        $scope.$watch(function() {
            return vm.view.customerData.shippingAddress;
        }, function(newVal, oldVal) {
            if(newVal != oldVal && vm.view.customerData.isAddressSame){
                vm.view.customerData.billingAddress = vm.view.customerData.shippingAddress;
            }
        }, true);
    };



    window.onbeforeunload = function() {
     	var clearCartUrl = '/quote/removeSessionCart';
     	// if(window.location.href.indexOf("/edit?firstTimeEdit") != -1)
     	// {
     	// 	httpService.delete(clearCartUrl).then(function(response){
      //    		// Nothing to do with the response. Just clearing the cart in the above request.
      //    	});
     	// }
     }
    
    vm.beforeUnloadFunction = function(event) {
        event.returnValue = "Are you sure , you wanted to navigate out of Page?";
    }

    vm.addBeforeUnloadEvent = function() {
        window.addEventListener("beforeunload", vm.beforeUnloadFunction);
    }

    vm.removeBeforeUnloadEvent = function() {
        window.removeEventListener("beforeunload", vm.beforeUnloadFunction);
      //  window.removeEventListener("unload", vm.onbeforeunload);
        window.removeEventListener('load',function(){
        });
    }
    
    $scope.$on('submittedQuote',function(e){
        vm.removeBeforeUnloadEvent();
        e.preventDefault();
    });
    
    vm.watch = function(){
        var unregister = $scope.$watch('watchPages', function(n,o) {
            if(!angular.equals(n,o)){
                vm.addBeforeUnloadEvent();
                unregister();
            }
          },true);
    };
    
    vm.callReplicateQuoteAPI = function(url)
    {
	    httpService.get(url).then(function(response){
			vm.quoteResponse = response.data;
			$scope.quoteStockPopupResponse = response.data;
			$scope.close();
			// console.log(vm.quoteResponse);
			if (!vm.quoteResponse.status && vm.quoteResponse.cartExists)
			{
				vm.LostProductModalInstance =  $uibModal.open({
		            ariaLabelledBy: 'modal-title',
		            ariaDescribedBy: 'modal-body',
		            templateUrl: '/_ui/responsive/cat/templates/editQuoteMoveConfirmation.html',
		            scope:$scope,
		            size: 'quotation-remove-item',
		        });
			}
			else if(!vm.quoteResponse.status && vm.quoteResponse.stockNotExists){

				vm.LostProductModalInstance =  $uibModal.open({
		            ariaLabelledBy: 'modal-title',
		            ariaDescribedBy: 'modal-body',
		            templateUrl: '/_ui/responsive/cat/templates/cannotReplicateQuote.html',
		            scope:$scope,
		            size: 'quotation-remove-item',
		        });
			}
			else if(!vm.quoteResponse.status && !vm.quoteResponse.stockNotExists){
				
								vm.LostProductModalInstance =  $uibModal.open({
									ariaLabelledBy: 'modal-title',
									ariaDescribedBy: 'modal-body',
									templateUrl: '/_ui/responsive/cat/templates/existingQuoteOutofStock.html',
									scope:$scope,
									size: 'quotation-remove-item',
								});
							}
			else
			{
				if (vm.quoteResponse.quoteCode != null)
				{
					console.log("vm.quoteResponse.quoteCode: ", vm.quoteResponse.quoteCode);
					$window.location.href = '/quote/'+vm.quoteResponse.quoteCode+'/edit?firstTimeEdit=false&replicateQuote=false';
				} else
				{
				$window.location.href = '/quote/'+vm.editQuoteCode+'/edit?firstTimeEdit=false&replicateQuote=false';
				}
			}
	    }, function(error){
	    });
    }
    $scope.$on('initButtonState',function(event,data){
        vm.initButtonState(data);
    })
    vm.initButtonState = function(value){
        vm.enableEditButton = "true";
    }

    vm.editQuote = function(url){
        $window.location.href = url;
    }

    vm.populateCustomerNameBasedOnSuggestion = function(customerName) {
        return httpService.get(catEndpointService.quoteCustomerNameSuggestionEndPoint + customerName + '/').then(function(response) {
            return response.data;
        }, function(error) {});
    }

    vm.populateCustomerEmailBasedOnSuggestion = function(customerEmail) {
        return httpService.get(catEndpointService.quoteCustomerEmailSuggestionEndPoint + encodeURIComponent(customerEmail) + '/').then(function(response) {
            return response.data;
        }, function(error) {});
    }

    vm.shippingCountryChanged = function(obj){
        vm.view.customerData.shippingAddress.countryList.selectedCountry = obj;
        vm.view.customerData.shippingAddress.regionList.selectedRegion = "";
        if(!!vm.view.customerData.shippingAddress.countryList.selectedCountry){
            quoteService.updateShippingRegionList();
        }
    }

    vm.billingCountryChanged = function(obj){
        vm.view.customerData.billingAddress.countryList.selectedCountry = obj;
        vm.view.customerData.billingAddress.regionList.selectedRegion = "";
        if(!!vm.view.customerData.billingAddress.countryList.selectedCountry){
            quoteService.updateBillingRegionList();
        }
    }

    vm.shippingStateChanged = function(obj){
        vm.view.customerData.shippingAddress.regionList.selectedRegion = obj;
    }

    vm.billingStateChanged = function(obj){
        vm.view.customerData.billingAddress.regionList.selectedRegion = obj;
    }


    $scope.closePopup = function() {
        vm.modalInstance.dismiss("cancel");
    };




    $scope.navigateToQuoteHistory = function() {
        $window.location.href = constantService.quoteHistoryURL;
    }

    vm.onCustomerNameSelect = function($item, $model, $label) { 
        httpService.get(catEndpointService.quoteCustomerDataBasedOnNameSuggestionEndPoint + $item).then(function(response) {  
            if(!!response.data) {
                setCustomerDetails(response);
                vm.isExistingCustomerDisabled=true;
            }
            
            
        }, function(error) {    });  
    }
    
    function setCustomerDetails(response) {
    	vm.customerNameData = response.data;  
    	vm.view.customerData.isAddressSame = vm.customerNameData.shippingSameAsBilling;
    	vm.view.customerData.uid = vm.customerNameData.uid;
        vm.view.customerData.customerEmail.value = vm.customerNameData.email;
        vm.view.customerData.customerName.value = vm.customerNameData.name;
        vm.view.quoteMetaData.shippngAddressId = vm.customerNameData.defaultShippingAddress.id;
        vm.view.quoteMetaData.billingAddressId = vm.customerNameData.defaultBillingAddress.id;

        vm.view.customerData.shippingAddress.line1.value = vm.customerNameData.defaultShippingAddress.line1;
        vm.view.customerData.shippingAddress.line2.value = vm.customerNameData.defaultShippingAddress.line2;
        vm.view.customerData.shippingAddress.postalCode.value = vm.customerNameData.defaultShippingAddress.postalCode;
        vm.view.customerData.shippingAddress.town.value = vm.customerNameData.defaultShippingAddress.town;
        vm.view.customerData.shippingAddress.countryList.selectedCountry = vm.customerNameData.defaultShippingAddress.country;
        quoteService.updateShippingRegionList();
        vm.view.customerData.shippingAddress.regionList.selectedRegion = vm.customerNameData.defaultShippingAddress.region;
        
        if(vm.view.customerData.isAddressSame){
        	vm.view.customerData.billingAddress = vm.view.customerData.shippingAddress;
        }else{
        	vm.view.customerData.billingAddress.line1.value = vm.customerNameData.defaultBillingAddress.line1;
            vm.view.customerData.billingAddress.line2.value = vm.customerNameData.defaultBillingAddress.line2;
            vm.view.customerData.billingAddress.postalCode.value = vm.customerNameData.defaultBillingAddress.postalCode;
            vm.view.customerData.billingAddress.town.value = vm.customerNameData.defaultBillingAddress.town;
            vm.view.customerData.billingAddress.countryList.selectedCountry = vm.customerNameData.defaultBillingAddress.country;
            quoteService.updateBillingRegionList();
            vm.view.customerData.billingAddress.regionList.selectedRegion = vm.customerNameData.defaultBillingAddress.region;

        }
    }

    vm.onCustomerEmailSelect = function($item, $model, $label) {  
        httpService.get(catEndpointService.quoteCustomerDataBasedOnEmailSuggestionEndPoint + encodeURIComponent($item) + '/').then(function(response) {  
            setCustomerDetails(response);
            vm.isExistingCustomerDisabled=true;
        }, function(error) {    });  
    }

    vm.isAddressSameChanged = function() {
        if (!!vm.view.customerData.isAddressSame) {
            vm.view.customerData.billingAddress = vm.view.customerData.shippingAddress;
        } else {
           vm.view.customerData.billingAddress = angular.copy(vm.view.customerData.billingAddress);
        }
    }

    vm.submitQuote = function(){
        //quoteService.submitQuote();
            vm.modalInstance =  $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: '/_ui/responsive/cat/templates/quotationSubmitQuotePopup.html',
                scope:$scope,
                size: 'quotation-submit-quote',
            });
    }

    vm.resetCustomerDetails = function(){
        vm.isExistingCustomerDisabled=false;
        quoteService.resetCustomerInformation(); 
        vm.isExistingCustomerDisabled=false;
    }
    vm.clearExistingCustomerForm = function(){
        vm.isExistingCustomerDisabled=false;
        vm.resetCustomerDetails();
    }
	
	vm.enableEditButton = function(enableEditButton){
    	vm.enableEditButton = enableEditButton;
	}
	
	 $scope.navigateToEditQuotePage = function(){
			var replicateQuoteURLLocal = '/quote/'+vm.editQuoteCode+'/edit?firstTimeEdit=false&removeSessionCart=true';
			window.location=replicateQuoteURLLocal;
		}
	 
	$scope.close = function(){
		if(vm.LostProductModalInstance){
			vm.LostProductModalInstance.dismiss("cancel");
			vm.LostProductModalInstance = null;
		}
    };
    
    vm.init = function(){
        vm.watchCallStack();
    };

    vm.init();

}]);