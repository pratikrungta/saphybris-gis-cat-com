catApp.controller('QuotationMobileController', ['httpService', 'catEndpointService', '$scope','$location', '$window', '$compile', 'constantService', '$timeout', '$uibModal','catService', function(httpService, catEndpointService, $scope,$location, $window, $compile, constantService, $timeout,$uibModal, catService) {
    var vm = this;
    vm.stepName = "CART DETAILS";
    var now = new Date(),
    max = new Date(now.getFullYear() + 100, now.getMonth(), now.getDate());
    vm.datePickerSettings = {
        theme: 'ios',
        display: 'bottom',
        max: max,
        onInit: function (event, inst) {
            $(".mbsc-sc-itm div").remove();
        },
        onChange: function (event, inst) {
            $(".mbsc-sc-itm div").remove();
        },
        setText:'SAVE',
        cancelText:'CANCEL',
        dateFormat : "mm-dd-yy",
        height:40
    };
    vm.swipeView = function(event){
        $timeout(function(){
            var current_fs, next_fs, previous_fs;
            var left, opacity, scale;
            var animating;
            if(animating) return false;
            animating = true;
            current_fs = $("fieldset:visible");
            next_fs = $(current_fs).next();
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
            //show the next fieldset
            next_fs.show(); 
           if($(current_fs).attr("data-section") !== 'CUSTOMER DETAILS' && $(event.target).text() !== 'SUBMIT QUOTE'){
                if($(event.target).text() === "CUSTOMER DETAILS"){
                    $(event.target).html("SUBMIT QUOTE");
                    vm.stepName = "SUBMIT QUOTE";
                } else {
                    $(event.target).html("CUSTOMER DETAILS");
                    vm.stepName = "CUSTOMER DETAILS";
                }    
                //hide the current fieldset with style
                current_fs.animate({opacity: 0}, {
                    step: function(now, mx) {
                        //as the opacity of current_fs reduces to 0 - stored in "now"
                        //1. scale current_fs down to 80%
                        scale = 1 - (1 - now) * 0.2;
                        //2. bring next_fs from the right(50%)
                        left = (now * 50)+"%";
                        //3. increase opacity of next_fs to 1 as it moves in
                        opacity = 1 - now;
                        current_fs.css({
                            'transform': 'scale('+scale+')',
                            'position': 'absolute'
                        });
                        next_fs.css({'left': left, 'opacity': opacity});
                    }, 
                    duration: 500, 
                    complete: function(){
                        current_fs.hide("slide", { direction: "left" });
                        animating = false;
                    }, 
                    //this comes from the custom easing plugin
                    easing: 'easeOutSine'
                });
            }
            $scope.$apply();
        });
    }
        
        
}]);