catApp.controller('quotationMobileNavController', ['quoteService','$timeout',function(quoteService,$timeout){
    var vm = this;
    vm.buttonsText = ["QUOTE DETAILS","CART DETAILS","CUSTOMER DETAILS"];
    vm.buttonsMetaInfo = quoteService.quoteInfo.metaData.mobileNav;
       
    vm.previousPage = function(){
        if(quoteService.quoteInfo.metaData.mobileNav.currentPageNum > 1){
            quoteService.quoteInfo.metaData.mobileNav.currentPageNum--;
        }
        vm.updateInfo();
    }
    vm.nextPage = function(){
        if(quoteService.quoteInfo.metaData.mobileNav.currentPageNum <3){
            quoteService.quoteInfo.metaData.mobileNav.currentPageNum++;
        }
        vm.updateInfo();
    }

    vm.updateInfo = function(){
        if(quoteService.quoteInfo.metaData.mobileNav.currentPageNum == 1){
            quoteService.quoteInfo.metaData.mobileNav.showNext = true;
            quoteService.quoteInfo.metaData.mobileNav.showPrevious = false;
            quoteService.quoteInfo.metaData.mobileNav.previousButtonText = "QUOTE DETAILS";
            quoteService.quoteInfo.metaData.mobileNav.nextButtonText = "CART DETAILS";
        }
        if(quoteService.quoteInfo.metaData.mobileNav.currentPageNum == 2){
            quoteService.quoteInfo.metaData.mobileNav.showNext = true;  
            quoteService.quoteInfo.metaData.mobileNav.showPrevious = true;
            quoteService.quoteInfo.metaData.mobileNav.previousButtonText = "QUOTE DETAILS";
            quoteService.quoteInfo.metaData.mobileNav.nextButtonText = "CUSTOMER DETAILS";
        }
        if(quoteService.quoteInfo.metaData.mobileNav.currentPageNum == 3){
            quoteService.quoteInfo.metaData.mobileNav.showNext = false;
            quoteService.quoteInfo.metaData.mobileNav.showPrevious = true; 
            quoteService.quoteInfo.metaData.mobileNav.previousButtonText = "CART DETAILS";
            quoteService.quoteInfo.metaData.mobileNav.nextButtonText = "QUOTE DETAILS";
        }
    }

    $timeout(function(){
        vm.updateInfo();
    },0);

}]);

