catApp.controller('quotationQuoteDetailsController', ['httpService', 'catEndpointService', '$scope', '$location', '$window', '$compile', 'constantService', '$timeout', '$uibModal', 'catService', 'quoteService','$timeout' ,function(httpService, catEndpointService, $scope, $location, $window, $compile, constantService, $timeout, $uibModal, catService,  quoteService, $timeout) {
    var vm = this;

    vm.view = {};
    vm.view.quote = quoteService.quoteInfo.quoteDetails;
    vm.view.quoteMetaData = quoteService.quoteInfo.metaData;


    vm.init = function(expirationDate, quoteCode, quoteName, isFirstTimeEdit, isReplicateQuote) {
        vm.view.quote.name.value = quoteName;
        vm.view.quote.number.value = quoteCode;
        if(isFirstTimeEdit == "false" || isReplicateQuote=="true"){
             vm.view.quoteMetaData.editQuote = (isFirstTimeEdit == "false");
            vm.view.quoteMetaData.replicateQuote = (isReplicateQuote=="true");
            quoteService.retriveQuoteDataOnInit();
        }
        vm.updateExpiryData(expirationDate);
    };

    vm.updateExpiryData = function(expirationDate) {
        var expirationDateArr = expirationDate.split(" ");
        // var todayDateArr = (new Date()).split(" ");
        var expirationDate = new Date(parseInt(expirationDateArr[5]), vm.getMonth(expirationDateArr[1]), parseInt(expirationDateArr[2]));
        var currentDate = new Date(); //new Date(parseInt(todayDateArr[3]), vm.getMonth(todayDateArr[1]), parseInt(expirationDateArr[2]));
        vm.view.quote.validTillQuoteEdit.value = expirationDate;//.setDate(expirationDate.getDate()+30);
        vm.view.quote.validTill = vm.view.quote.validTillQuoteEdit//expirationDate.setDate(expirationDate.getDate());

        vm.view.quote.metaData.validTillDateOptions.minDate = currentDate;
        vm.view.quote.metaData.validTillQuoteEditDateOptions.minDate = currentDate;
        vm.view.quote.metaData.submittedToCustomerDateOptions.minDate = currentDate;//.setDate(currentDate.getDate()-365);
        vm.view.quote.metaData.customerResponseDateOptions.minDate = currentDate;//.setDate(currentDate.getDate()-365);
    };

    vm.checkQuotesFunc = function(data) {
        vm.view.quote.name.value = data.replace(/['"]+/g, '');
    }

    vm.getMonth = function(monthStr) { 
        return new Date(monthStr + '-1-01').getMonth()
    }
    vm.setQuoteStatus = function(statusObj){
      vm.view.quote.status.selected = statusObj;
    }

    vm.setQuoteLostStatus = function(statusObj){
        vm.view.quote.reasonForQuoteLost.selected = statusObj;
    }

    vm.isFirstTimeEdit = function(isFirstTimeEdit){
        debugger;
    }
}]);
