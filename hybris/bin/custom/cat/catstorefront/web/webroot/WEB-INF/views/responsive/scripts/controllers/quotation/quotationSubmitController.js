catApp.controller('quotationSubmitController', ['$timeout','quoteService','constantService','$uibModal',
'$uibModalStack','$scope', '$window','modalPopUpFactory', 
function($timeout,quoteService,constantService, $uibModal,$uibModalStack, $scope, $window,modalPopUpFactory) {
    var vm = this;
    vm.view = {};
    $scope.view = {
    		quoteCode:"",
    		quoteName:"",
    		quoteCustomer:"",
    		status:false,
    		errorMessage:null
    };

    vm.view.quoteMetaData = quoteService.quoteInfo.metaData;
    
    $scope.navigateToQuoteHistory = function(){
    	$window.location.href = '/my-account/my-quotes/view';
    }
    
    $scope.navigateToHome = function(){
    	$window.location.href = '/';
    }

    $scope.view.submitToDSU = quoteService.quoteInfo.submitToDSU;
    vm.view.quoteMetaData = quoteService.quoteInfo.metaData;

    vm.submitQuote = function(){
       quoteService.validateFrom();
       if(quoteService.isFormValid()){
       	quoteService.submitQuote().then(function(response){
          if(response.data && response.data.status){
            var responseData = response.data;
            $scope.view = responseData;
            $scope.userExists = false;
            quoteService.quoteInfo.submitResponse = responseData;
            $scope.$emit('submittedQuote','stopWatching');
            vm.openPopUp();
            ACC.gaHelper.hitPageView(ACC.gaHelper.quoteSubmitPageViewUrl);
          }else{
            $scope.userExists = response.data.userExists;
            $scope.navigateUrl = '/cart/catCartPage';
            $scope.errorMessage = 'Oops Something went wrong. Please retry from the beginning';
            if($scope.userExists){
              $scope.navigateUrl = '';
              $scope.errorMessage = 'The current user exists. Please create a new user and submit the Quote.';  
            }
            vm.openErrorPopUp();
          }
       	},function(error){
       	});
       }
    }

    $scope.submitToDSUChnaged = function(){
      quoteService.quoteInfo.submitToDSU = $scope.view.submitToDSU;
      quoteService.quoteInfo.submitToDSU.ajaxInprogress = true;
      quoteService.submitToDSU().then(function(response){
        quoteService.quoteInfo.submitToDSU.ajaxInprogress = false;
      },function(error){
        quoteService.quoteInfo.submitToDSU.ajaxInprogress = false;
      });
    }

	vm.items = ['item1', 'item2', 'item3'];
  $scope.closePopup = function(){
    //vm.modalInstance.dismiss("cancel");
    modalPopUpFactory.close(vm.modalInstance);
  };
  $scope.navigateToQuoteDetails = function(){
	 try{
		 if($scope.view.quoteCode){
			  $window.location.href = '/my-account/my-quotes/'+$scope.view.quoteCode;
		  } 
	 }catch(e){
		 //some error at quoteCode
	 } 
  };

  $scope.retryNavigation = function(){
    if($scope.navigateUrl){
      $window.location.href = $scope.navigateUrl;
    }else{
      $scope.closePopup();
    }
  }

  vm.openPopUp = function () {
    var t = getPopUp();
    if(/\?(?:firstTimeEdit=false)/.test($window.location.href)){
    	t.templateUrl = getTemplateUrl('quotationEditQuotePopup.html');
    }
    //vm.modalInstance = $uibModal.open(t);
    vm.modalInstance = modalPopUpFactory.open(t);
  };

  vm.openErrorPopUp = function () {
    var t = getPopUp('quotationSubmitQuoteErrorPopup.html');
    //vm.modalInstance = $uibModal.open(t);
    vm.modalInstance = modalPopUpFactory.open(t);
  };

  function getTemplateUrl(template){
    return '/_ui/responsive/cat/templates/' + template;
  };

  function getPopUp(templateUrl) {

    templateUrl = getTemplateUrl(templateUrl || 'quotationSubmitQuotePopup.html');
    //var parentElem = angular.element(".quotation-submit-container");
    var modalSize = (templateUrl == constantService.quotationSubmitPopupURL) ? 'submit-quote' : 'md';
      return {
        backdrop: 'static',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: templateUrl,
        size: modalSize,
        scope: $scope,
        resolve: {
          items: function () {
            return vm.items;
          }
        }
      };
  }
}]);
