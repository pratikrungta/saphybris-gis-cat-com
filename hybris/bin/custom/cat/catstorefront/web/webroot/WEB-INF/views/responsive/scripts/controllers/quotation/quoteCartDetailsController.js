catApp.controller('quoteCartDetailsController', ['httpService', 'catEndpointService', '$scope','$location', 
'$window', '$compile', 'constantService', '$timeout', '$uibModal',
'catService', 'quoteService','EppHelperFactory','CsaHelperFactory', function(httpService, catEndpointService, 
    $scope,$location, $window, $compile, constantService, 
    $timeout,$uibModal, catService, quoteService,EppHelperFactory,CsaHelperFactory) {
    var vm = this;
    $scope._selectText = "Select";
    vm.selectLabel = 'Select';
    vm.ajaxSuccess = true;
    vm.discountRadio = "ABSOLUTE";
    vm.isPopUpOpen = false;
    vm.view = {
        cartDetails: quoteService.quoteInfo.cartDetails,
        quoteMetaData: quoteService.quoteInfo.metaData,
        vetData: null
    };
    var defaultEppData = {
                            quantity: 0,
                            basePrice: {
                                value: 0
                            },
                            totalPrice: {
                                formattedValue: "$0.00"
                            }
                        };
    var defaultCSAData = {
                            configurationInfos: [{ // for hours
                                                    configurationValue: 0
                                                },
                                                { // for dropdown
                                                    configurationValue: vm.selectLabel
                                                },
                                                { // for year
                                                    configurationValue: 0
                                                },
                                                ],
                            quantity: 0,
                            basePrice: {
                                value: 0
                            },
                            totalPrice: {
                                formattedValue: "$0.00"
                            }
                        };
                        
    $scope.reserveMachineDetails = quoteService.quoteInfo.reserveMachineDetails;
    vm.eppOptionsArray = [];
    vm.csaOptionsArray = [];
    $scope.customEppCsaOptionArray = [];

    vm.getFormattedText = constantService.getTwoDigitFormattedText;

    if(!!$location.search().replicateQuote){
        vm.replicateQuote=$location.search().replicateQuote;
    }
    
    vm.totalListPrice = 0;

    vm.setCartDataOnRefresh = function(_data){
        if(vm.selectedProdIndexOfVet){
            vm.cartProducts.entries[vm.selectedProdIndexOfVet].isVetEditing = false;
            vm.selectedProdIndexOfVet = null;
        }
        quoteService.quoteInfo.catQuoteSerialNumbersData = quoteService.quoteInfo.catQuoteSerialNumbersData || []; 
        if(vm.cartProducts){
            for(var i =0 ; i < _data.entries.length; i++){
                _data.entries[i].endDate = vm.cartProducts.entries[i].endDate;
                _data.entries[i].reservedSerialNos = vm.cartProducts.entries[i].reservedSerialNos;
                _data.entries[i].reservedFlag = vm.cartProducts.entries[i].reservedFlag;
                if(!_data.entries[i].discount){
                  _data.entries[i].discount = {
                    maxQuantity : null,
                    minQuantity : null,
                    value: 0,
                    formattedValue: "$0.00"
                  }
                }
                _data.entries[i].eppOptions = vm.cartProducts.entries[i].eppOptions;
                _data.entries[i].csaOptions = vm.cartProducts.entries[i].csaOptions;

                // Used to retain the data that is already been selected for Epp
                _data.entries[i].eppLabel = vm.cartProducts.entries[i].eppLabel;
                _data.entries[i].csaLabel = vm.cartProducts.entries[i].csaLabel;
                _data.entries[i].isCollapsed = ((_data.entries[i].eppLabel != vm.selectLabel) || (_data.entries[i].csaLabel != vm.selectLabel));
                if(_data.entries[i].entries){
                    _data.entries[i].entries = vm.sortEppCsaEntries(_data.entries[i].entries);
                }else{
                    _data.entries[i].entries = vm.cartProducts.entries[i].entries;
                }
                _data.entries[i].entries[0].eppApplyEnable = false;
                _data.entries[i].entries[1].csaApplyEnable = false;
                _.each(_data.entries[i].valueAnalysis, function(v){
                    v.isEditing = false;
                });
                _data.entries[i].showAddVET = false;
                _data.entries[i].isVetCollapsed = (_data.entries[i].valueAnalysis && _data.entries[i].valueAnalysis.length != 0);
            }
        }else { 
            _.each(_data.entries, function(val) {
                val.isVetCollapsed = (val.valueAnalysis && val.valueAnalysis.length != 0);
            });
        }
        vm.cartProducts = _data;
        vm.totalListPrice = ((parseFloat(vm.cartProducts.totalDiscounts.value) + parseFloat(vm.cartProducts.totalPrice.value)).toFixed(2));
        quoteService.quoteInfo.cartEntries = vm.cartProducts.entries;
        quoteService.quoteInfo.cartDetails.totalPrice = vm.cartProducts.totalPrice.formattedValue;
    }

    vm.serializeFormData = function(index, discountType, discountRate) {
        return {
            "entryNumber": vm.cartProducts.entries[index].entryNumber,
            "discountRate": discountRate,
            "discountType": discountType,
            "CSRFToken": ACC.config.CSRFToken,
        }
    }
    
    vm.calcDiscount = function(index,discountType, value){
        var postData = vm.serializeFormData(index, discountType, value);
        httpService.post("/quote/"+vm.quotationCode+catEndpointService.quotePageCalculateDiscountEndPoint, $.param(postData))
        .then(function(response) {
            if (response.status == 200) {
                vm.setCartDataOnRefresh(response.data);
            }
        }, function(error) {
        	if(error.status == 403 && error.data.code){
        		vm.setCartDataOnRefresh(error.data);
        	}
        });
    }

    vm.toggleDiscountCalc = function(discountType){
        vm.discountRadio = discountType;
        httpService.get('/quote/'+vm.quotationCode+catEndpointService.quotationCartRefreshEndPoint).then(function(response) {
            if (response.status == 200) {
                vm.setCartDataOnRefresh(response.data);
            }
        }, function(error) {
        });
    }


    vm.updateEppCsaCounter = function(index, mode, isCSA){
        var idx = isCSA ? 1 : 0;
        var product = vm.cartProducts.entries[index].entries[idx];
        if(product){
            if(mode === 'increment' && (vm.cartProducts.entries[index].quantity > product.quantity)) {
                product.quantity += 1;
                vm.checkEppCsaChange(isCSA, index);
            } else if(mode === 'decrement' && product.quantity > 1) {
                 product.quantity -= 1;
                vm.checkEppCsaChange(isCSA, index);
            } else if(mode === 'blur' && (vm.cartProducts.entries[index].quantity <= product.quantity)) {
                product.quantity = vm.cartProducts.entries[index].quantity;
                vm.checkEppCsaChange(isCSA, index);
            }
            vm.cartProducts.entries[index].entries[idx] = product;
        }
    }

    vm.applyEppCsaFilters = function(postData,cartIndex){
        var EppCsaEndPointURL = catEndpointService.quoteEppCsaApplyEndPoint;
         postData.configVariantId=vm.cartProducts.entries[cartIndex].product.code
        if(postData.entryNumber){
           EppCsaEndPointURL = EppCsaEndPointURL + '?entryNumber=' + postData.entryNumber;
        }
        httpService.postJsonData(EppCsaEndPointURL, postData).then(function(response) {
            if (response.status == 200) {
                vm.setCartDataOnRefresh(response.data);
            }
        }, function(error) {
            
        });
    }

    // vm.serializeEPPCSAData = function(product,index) {
    //     return {
    //         'quantity' : product.quantity,
    //         'eppOption': vm.cartProducts.entries[index].eppLabel,
    //         'basePrice' : product.basePrice.value,
    //         'configVariantId' : vm.cartProducts.entries[index].product.code
    //     }
    // }

    // vm.serializeCSAData = function(product, index){
    //     return {
    //         "quantity" : product.quantity,
    //         "csaOption" : vm.cartProducts.entries[index].csaLabel,
    //         "csaHours" : product.configurationInfos[0].configurationValue,
    //         "csaYear" : product.configurationInfos[2].configurationValue,
    //         "basePrice": product.basePrice.value,
    //         "configVariantId" : vm.cartProducts.entries[index].product.code
    //     }

    // }

    vm.clearEppCsaFilters = function(item,heplerItem,itemContainer){
        var cartDeleteURL = '';
        /**
         * If it has no entry number it is a new item.
         * 
        */
        if(item.entryNumber){
            cartDeleteURL = catEndpointService.quotationCartPageDeleteEndPoint + '?entryNumber=' +(item.entryNumber);
            httpService.get(cartDeleteURL).then(function(response) {
                if(typeof response.data == 'object'){
                    vm.setCartDataOnRefresh(response.data);
                }
            }, function(error) {
            });
            return;
        }
        heplerItem.isNewBtnDisable  = !!0;
        
        if(item.hasOwnProperty('eppOption')){
            heplerItem.eppItems.pop();
            itemContainer.isEppCollapsed=!itemContainer.isEppCollapsed;
            return;
        }
        if(item.hasOwnProperty('csaOption')){
            heplerItem.csaItems.pop();
            itemContainer.isCsaCollapsed=!itemContainer.isCsaCollapsed;
            return;
        }
        
       
    }

    vm.updateEppDropdown = function(index,options){
        vm.cartProducts.entries[index].eppLabel = options;
    }

    vm.updateCsaDropdown = function(index,options){
        vm.checkEppCsaChange(true, index);
        vm.cartProducts.entries[index].csaLabel = options;
    }

    vm.checkErrorForCart = function(errorData, qty){
        return (errorData != null && errorData.maxQty != null && errorData.maxQty > 0 && qty > errorData.maxQty);
    }
    var showEPPCSAWarningMessage = function(msgCode){
        $scope.ErrMsg=msgCode;
        vm.clearEppCsaAlert = $uibModal.open({
        backdrop: 'static',
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        templateUrl: '/_ui/responsive/cat/templates/showEppCsaClearPopup.html',
        scope: $scope,
        size: 'reserve-machine-popup'
    });
    };
    vm.cartDecrement=function(cartProduct){
        cartProduct.quantity>1?cartProduct.quantity-=1:'';
        return;
    };
    vm.cartIncrement=function(cartProduct){
        cartProduct.quantity+=1;
        return;
    };
    /**
     * 
     * @param {Object} context-The epp,csa,respective cart Object with cartIndex
     * @param {Number} context.cartProd
     * @param {Object} context.eppObj
     * @param {Object} context.csaObj
     * 
     */
    vm.updateCartProducts = function(context){
       var msgCode='',
           eppOverFlow=context.eppObj.totalEppQty>context.cartProd.quantity,
           csaOverFlow=context.csaObj.totalCsaQty>context.cartProd.quantity;

           if(context.cartProd.quantity<1){
            context.cartProd.quantity = context.eppObj.productQty;
            return;
           }
           if(!(EppHelperFactory.AreAllEppsApplied(context.eppObj.eppItems)
                &&CsaHelperFactory.AreAllCsasApplied(context.csaObj.csaItems))){
            context.cartProd.quantity = context.eppObj.productQty;
            vm.isPopUpOpen = true;
            if(vm.isPopUpOpen) {
                showEPPCSAWarningMessage(ACC.localization.EPPCSAUnsaved);
            }
            return;
           }

           if(eppOverFlow&&csaOverFlow){
                 msgCode = ACC.localization.EPPCSAOverflow;   
           }else if(eppOverFlow){
                msgCode = ACC.localization.EPPOverflow;
           }else if(csaOverFlow){
                msgCode = ACC.localization.CSAOverflow;
           }else{
                $scope.updateCartQuantityChange(context.cartProd.entryNumber,
                                            parseInt(context.cartProd.quantity,10));
           }
           
           if(msgCode){
            context.cartProd.quantity = context.eppObj.productQty;
            showEPPCSAWarningMessage(msgCode);
        }

    };


    $scope.updateCartQuantityChange = function(entryNumber, qty){
        vm.ajaxSuccess = false;
        httpService.get(catEndpointService.quotationCartPageUpdateEndPoint+'?entryNumber='+entryNumber+'&qty='+qty)
        .then(
            function(response){
                vm.ajaxSuccess = true;
                vm.setCartDataOnRefresh(response.data);
                ACC.minicart.refreshMinicartCountNum();
            }, function(error){
         
            }
        );
    }
    $scope.closeClearEppCsaAlert = function(){
        vm.isPopUpOpen = false;
        vm.clearEppCsaAlert.dismiss('cancel');
    }
        
    vm.loadCartProducts = function(mediaPrefix, quotationCode, firstTimeEdit, isReplicateQuote){
        vm.mediaPrefix = mediaPrefix;
        vm.quotationCode = quotationCode;
        vm.isEditQuote = (firstTimeEdit == 'false');
        vm.isReplicateQuote = (isReplicateQuote == 'true');
        httpService.get(catEndpointService.quotePageCartDetailsEndPoint).then(function(response){
            if(vm.isEditQuote){
                response.data.entries = response.data.quoteData.entries;
            }
            vm.setCartDataOnRefresh(response.data);
            vm.discountRadio = vm.cartProducts.quoteDiscountsType;
            $.each(vm.cartProducts.entries,function(index,value){
                if(value.serialNumbers && value.serialNumbers.length){
                    vm.cartProducts.entries[index].reservedFlag = true;
                    vm.cartProducts.entries[index].reservedSerialNos = value.serialNumbers;
                    // vm.cartProducts.entries[index].endDate = value.endDate;
                }

                if(!value.discount){
                  vm.cartProducts.entries[index].discount = {
                    maxQuantity : null,
                    minQuantity : null,
                    value: 0,
                    formattedValue: "$0.00"
                  }
                }
                $scope.customEppCsaOptionArray.push({
                    'customEppOption' : '',
                    'customCsaOption':''
                });
            })
            //vm.view.discount_value = response.data.totalDiscounts.value;
            vm.populateEPPCSAOptionsDropdown('EPP');
            vm.populateEPPCSAOptionsDropdown('CSA');            
            
        }, function(error){
            
        });
    };
    vm.cartProductInitialDataSetUp=function(cartProduct){
         
        var data={
            eppHelper:{
                productQty:cartProduct.quantity,
                
            },
            csaHelper:{
                productQty:cartProduct.quantity,
            }
        };
        /**
         * Initializing EPP Data.
         */
        data.eppHelper.totalEppQty=0;
        
        data.eppHelper.eppItems = cartProduct.eppEntries&&cartProduct.eppEntries.map(
            function(eppItem,index){
                data.eppHelper.totalEppQty+=eppItem&&eppItem.quantity||0;
                return EppHelperFactory.register(eppItem);   
            }
        )||[];
        if(data.eppHelper.totalEppQty===data.eppHelper.productQty){
            data.eppHelper.isNewBtnDisable = !!1;
          }else{
            data.eppHelper.isNewBtnDisable = !!0;
          }
        /**
         * Initializing CSA Data.
         */
        data.csaHelper.totalCsaQty=0;
        data.csaHelper.csaItems = cartProduct.csaEntries&&cartProduct.csaEntries.map(
            function(csaItem,index){
                data.csaHelper.totalCsaQty+=csaItem&&csaItem.quantity||0;
                return CsaHelperFactory.register(csaItem);   
            }
        )||[];
        if(data.csaHelper.totalCsaQty===data.csaHelper.productQty){
            data.csaHelper.isNewBtnDisable = !!1;
          }else{
            data.csaHelper.isNewBtnDisable = !!0;
          }
        return data;
    }

    vm.populateEPPCSAOptionsDropdown = function(type){
        httpService.get(catEndpointService.quoteEppCsaOptionsEndPoint + '?type='+type)
        .then(function(response) {
            if(type == 'EPP'){
                vm.eppOptionsArray = response.data.options;
            }else {
                vm.csaOptionsArray = response.data.options;
            }
            if(vm.eppOptionsArray.length && vm.csaOptionsArray.length){
                vm.checkEppOptions();
            }
        }, function(error) {

        });
    }

    vm.sortEppCsaEntries = function(entries){
        if(entries.length > 1){
            if(entries[0].isEPP){
                return entries;
            }else {
                return [entries[1], entries[0]];
            }    
        }else {
            if(entries[0].isEPP){
                return [entries[0], angular.copy(defaultCSAData)];
            }else {
                return [angular.copy(defaultEppData), entries[0]];
            }
        }
        
    }
    vm.checkEppOptions = function(){
        angular.forEach(vm.cartProducts.entries,function(value, index){
            vm.cartProducts.entries[index].eppOptions = angular.copy(vm.eppOptionsArray);
            vm.cartProducts.entries[index].csaOptions = angular.copy(vm.csaOptionsArray);
            if(!vm.cartProducts.entries[index].entries){
                vm.cartProducts.entries[index].entries = [angular.copy(defaultEppData), angular.copy(defaultCSAData)];
                vm.cartProducts.entries[index].eppLabel = vm.selectLabel;
                vm.cartProducts.entries[index].csaLabel = vm.selectLabel;
            }else{
                vm.cartProducts.entries[index].entries = vm.sortEppCsaEntries(vm.cartProducts.entries[index].entries); // this is to keep epp in zero position
                angular.forEach(value.entries,function(object, configIndex){
                    if(object.isEPP)
                    {
                        if(vm.eppOptionsArray.indexOf(object.configurationInfos[0].configurationValue) == -1){
                            vm.cartProducts.entries[index].eppOptions.push(object.configurationInfos[0].configurationValue);
                        }
                        vm.cartProducts.entries[index].eppLabel = object.configurationInfos[0].configurationValue;
                    }
                    if(object.isCSA)
                    {
                        if(vm.csaOptionsArray.indexOf(object.configurationInfos[1].configurationValue) == -1){
                            vm.cartProducts.entries[index].csaOptions.push(object.configurationInfos[1].configurationValue);
                        }
                        vm.cartProducts.entries[index].csaLabel = object.configurationInfos[1].configurationValue;
                    }
                });
            }
            vm.cartProducts.entries[index].isCollapsed = ((vm.cartProducts.entries[index].eppLabel != vm.selectLabel) || (vm.cartProducts.entries[index].csaLabel != vm.selectLabel));
        })
    }

    vm.showReservePopup = function(index){
        var reserveMachineURL = catEndpointService.quotePageReserveMachineEndPoint+'?productCode='+vm.cartProducts.entries[index].product.code;
        vm.cartProducts.entries[index].reservedSerialNos = vm.cartProducts.entries[index].reservedSerialNos || [];
        var qty = vm.cartProducts.entries[index].quantity;
        httpService.get(reserveMachineURL).then(function(response){
            if(vm.showReserveModalInstance){
                $scope.closeReservePopup();
            }
            if(!(response.data.catReserveQuoteData && response.data.catReserveQuoteData.length) && !vm.cartProducts.entries[index].serialNumbers){
                return false;
            }
            if(response.data.catReserveQuoteData && response.data.catReserveQuoteData[0] && response.data.catReserveQuoteData[0].endDate 
                && typeof(response.data.catReserveQuoteData[0].endDate) != 'object' && response.data.catReserveQuoteData[0].endDate.indexOf('/') == -1){
                response.data.catReserveQuoteData[0].endDate = response.data.catReserveQuoteData[0].endDate.replace(/-/g, '\/');
            }
            if(vm.cartProducts.entries[index].endDate && typeof(vm.cartProducts.entries[index].endDate) != 'object' && vm.cartProducts.entries[index].endDate.indexOf('/') == -1){
                vm.cartProducts.entries[index].endDate = vm.cartProducts.entries[index].endDate.replace(/-/g, '\/');
            }
            $scope.reservePopupResponse = {
                lineItems : vm.cartProducts.entries[index].quantity,
                endDate : (vm.cartProducts.entries[index].endDate ? new Date(vm.cartProducts.entries[index].endDate) : response.data.catReserveQuoteData[0].endDate),
                configurationID : vm.cartProducts.entries[index].product.name,
                lineIndex : index,
                reservedSerialNos : angular.copy(vm.cartProducts.entries[index].reservedSerialNos),
                serialNosList : []
            };
            if(response.data.catReserveQuoteData && response.data.catReserveQuoteData[0] && response.data.catReserveQuoteData[0].serialNumbers){
                $scope.serialNumbers = response.data.catReserveQuoteData[0].serialNumbers;
                if(vm.cartProducts.entries[index].serialNumbers){
                    $scope.serialNumbers = $scope.serialNumbers.concat(vm.cartProducts.entries[index].serialNumbers);
                }
            }else if (vm.cartProducts.entries[index].serialNumbers) {
                $scope.serialNumbers = vm.cartProducts.entries[index].serialNumbers
            }else {
                $scope.serialNumbers = [];
            }
            // $scope.serialNumbers = response.data.catReserveQuoteData[0] ? response.data.catReserveQuoteData[0].serialNumbers : (vm.cartProducts.entries[index].serialNumbers ? vm.cartProducts.entries[index].serialNumbers : []);
            qty = $scope.serialNumbers.length >= qty ? qty : $scope.serialNumbers.length;
            $scope.serialNumbersCopy = angular.copy($scope.serialNumbers);
            $scope.serialNumbers = $scope.serialNumbers.filter(function(value) { 
                return vm.cartProducts.entries[index].reservedSerialNos.indexOf(value) == -1;
            });
            $scope.endDateSelected = false;
            if($scope.reservePopupResponse.endDate){
                $scope.endDateSelected = true;
            }
            $scope.serialNumberSelected = (vm.cartProducts.entries[index].reservedSerialNos.length != 0);
            vm.shouldReserveBtnInPopUpEnable();
            for(var i = 0; i < qty; i++){
                if(vm.cartProducts.entries[index].reservedSerialNos.length < qty){
                    $scope.reservePopupResponse.reservedSerialNos.push($scope._selectText);
                }

                $scope.reservePopupResponse.serialNosList.push({
                    "SerialNumbers" : $scope.serialNumbers
                });
            }
            vm.showReserveModalInstance = $uibModal.open({
                backdrop: 'static',
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: '/_ui/responsive/cat/templates/reserveMachinePopup.html',
                scope:$scope,
                size: 'reserve-machine-popup'
            });
        }, function(error){
     
        });
    };

    $scope.updateSerialNosSelection = function(arrayIndex,serialNo,rowIndex){
        // vm.cartProducts.entries[rowIndex].reservedSerialNos[arrayIndex] = serialNo;
        $scope.reservePopupResponse.reservedSerialNos[arrayIndex] = serialNo;
        $scope.serialNumberSelected = true;
        angular.forEach($scope.reservePopupResponse.serialNosList,function(value,listIndex){
            value["SerialNumbers"] = $scope.serialNumbersCopy.filter(function(value, index, array){
                return ($scope.reservePopupResponse.reservedSerialNos.indexOf(value) === -1);
            })
        });
        vm.shouldReserveBtnInPopUpEnable();
    };

    $scope.setDefaultValue = function(arrayIndex) {
        $scope.reservePopupResponse.reservedSerialNos[arrayIndex] = $scope._selectText;
        angular.forEach($scope.reservePopupResponse.serialNosList,function(value,listIndex){
            value["SerialNumbers"] = $scope.serialNumbersCopy.filter(function(value, index, array){
                return ($scope.reservePopupResponse.reservedSerialNos.indexOf(value) === -1);
            })
        });
    }

    $scope.onDateSelected = function(){
        if($scope.reservePopupResponse.endDate){
            $scope.endDateSelected = true;
            vm.shouldReserveBtnInPopUpEnable();
        }
        // $scope.reserveMachineDetails.reserveEndDate.popupOpend = !$scope.reserveMachineDetails.reserveEndDate.popupOpend;
    }

    vm.shouldReserveBtnInPopUpEnable = function() {
        $scope.reserveBtnEnable = !($scope.endDateSelected && $scope.serialNumberSelected);
    }

    $scope.reserveMachine = function(lineIndex, isFromClose){
        if(isFromClose){
            vm.cartProducts.entries[lineIndex].reservedSerialNos = vm.cartProducts.entries[lineIndex].reservedSerialNos.filter(function(val){return val != $scope._selectText});
        }else{
            vm.cartProducts.entries[lineIndex].reservedSerialNos = $scope.reservePopupResponse.reservedSerialNos.filter(function(val){return val != $scope._selectText});
            vm.cartProducts.entries[lineIndex].endDate = $scope.reservePopupResponse.endDate;
        }

        vm.cartProducts.entries[lineIndex].reservedFlag = (vm.cartProducts.entries[lineIndex].reservedSerialNos.length != 0);
        quoteService.quoteInfo.cartEntries = vm.cartProducts.entries;
        $scope.closeReservePopup();
    };

    vm.getDateFormat = function(_date){ // "MM/dd/yyyy"
        if(_date){
            if(typeof(_date) == "object" || _date.indexOf('/') != -1){
                var date = new Date(_date);
                //_date = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
                _date = vm.getTwoDigits(date.getMonth() + 1) + "-" + vm.getTwoDigits(date.getDate()) + "-" + date.getFullYear(); 
            }
        }else {
            _date = '-';
        }
        return _date;
    }

    vm.getTwoDigits = function(val){
        return ("00"+val).slice(-2);
    }

    vm.showRemovePopup = function(index) {
        $scope.isRedirect = (vm.cartProducts.entries.length === 1);
        $scope.displayRemoveText = $scope.isRedirect ? constantService.quote.lastProductRemove : constantService.quote.productRemove;
        $scope.removeCartProductIndex = index;
        vm.showRemoveModalInstance = $uibModal.open({
            backdrop: 'static',
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: '/_ui/responsive/cat/templates/quotationRemoveItemPopup.html',
            scope: $scope,
            size: 'quotation-remove-item',
        });
    };

    vm.showEppCsaPopup = function(option,lineIndex,item) {
        $scope.option = option;
        $scope.item = item;
        $scope.lineIndex = lineIndex;
        vm.eppCsaModalInstance = $uibModal.open({
            backdrop: 'static',
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: '/_ui/responsive/cat/templates/quotationEppCsaPopup.html',
            scope: $scope,
            size: 'quotation-remove-item',
        });
    };

    $scope.close = function() {
        vm.showRemoveModalInstance.dismiss("cancel");
    };

    $scope.closeReservePopup = function(lineIndex) {
        if(lineIndex != undefined){
            $scope.reserveMachine(lineIndex, true);
        }else{
            vm.showReserveModalInstance.dismiss("cancel");
        }
    };

    $scope.closeEppCsaPopup = function() {
        vm.eppCsaModalInstance.dismiss("cancel");
    };

    $scope.addCustomEppCsaOption = function(index,option,item){
        if(option === 'EPP'){
            item.eppOption = $scope.customEppCsaOptionArray[index].customEppOption;
            item.isEppDisabled=!!0;
            //vm.updateEppDropdown(index, $scope.customEppCsaOptionArray[index].customEppOption);
            if(vm.cartProducts.entries[index].eppOptions.length > vm.eppOptionsArray.length){
                vm.cartProducts.entries[index].eppOptions[vm.eppOptionsArray.length] = $scope.customEppCsaOptionArray[index].customEppOption;
            } else {
                vm.cartProducts.entries[index].eppOptions.push($scope.customEppCsaOptionArray[index].customEppOption);
            }
        }else{
            item.csaOption = $scope.customEppCsaOptionArray[index].customCsaOption;
            item.isCsaDisabled=!!0;
            //vm.updateCsaDropdown(index, $scope.customEppCsaOptionArray[index].customCsaOption);
            if(vm.cartProducts.entries[index].csaOptions.length > vm.csaOptionsArray.length){
                vm.cartProducts.entries[index].csaOptions[vm.csaOptionsArray.length] = $scope.customEppCsaOptionArray[index].customCsaOption;
            } else {
                vm.cartProducts.entries[index].csaOptions.push($scope.customEppCsaOptionArray[index].customCsaOption);
            }
        }
        $scope.closeEppCsaPopup();
    };

    vm.showVetLitePopUp = function(productIndex){
        vm.selectedProdIndexOfVet = productIndex;
        vm.vetLiteModalInstance = $uibModal.open({
            backdrop: 'static',
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: '/_ui/responsive/cat/templates/vetLitePopup.html',
            scope:$scope,
            size: 'vet-lite-popup'
        });
    }

    $scope.vetContinueLink = function() {
        vm.cartProducts.entries[vm.selectedProdIndexOfVet].isVetEditing = true;
        window.open(vm.cartProducts.entries[vm.selectedProdIndexOfVet].baseVETLink, '_blank');
        if(!vm.cartProducts.entries[vm.selectedProdIndexOfVet].valueAnalysis){
            vm.cartProducts.entries[vm.selectedProdIndexOfVet].valueAnalysis = [];
        }
        vm.cartProducts.entries[vm.selectedProdIndexOfVet].vetData = {
                            vetProduct: "",
                            vetLink: ""
                        };
        $scope.dismissVetLitePopup();
    }

    $scope.dismissVetLitePopup = function() {
        vm.vetLiteModalInstance.dismiss("cancel");
    };

    vm.addValueAnalysis = function(productIndex){
        var url = vm.cartProducts.entries[productIndex].vetData.vetLink;
        if(vm.hasDuplicateVetUrl(url, productIndex)){
            vm.cartProducts.entries[productIndex].vetData.hasError = true;
        }else{
            var catQuoteVETForm = {
                        entryNumber: vm.cartProducts.entries[productIndex].entryNumber, // always needed
                        vetLink: vm.cartProducts.entries[productIndex].vetData.vetLink,       // send only for add or remove
                        vetProduct: vm.cartProducts.entries[productIndex].vetData.vetProduct // send only for add or remove
                        // catQuoteVETDatas: vm.cartProducts.entries[productIndex].valueAnalysis // send only for edit
                    };

            var url = '/quote/'+vm.quotationCode+catEndpointService.quotePageAddValueAnalysisEndPoint;
            httpService.postJsonData(url, catQuoteVETForm).then(function(response){
                vm.setCartDataOnRefresh(response.data);
            }, function(error){
                
            });
        }
    }

    vm.editValueAnalysis = function(productIndex, vetIndex){
        vm.cartProducts.entries[productIndex].valueAnalysis[vetIndex].isEditing = true;
        vm.cartProducts.entries[productIndex].isVetEditing = true;
        vm.cartProducts.entries[productIndex].currentEditingVet = angular.copy(vm.cartProducts.entries[productIndex].valueAnalysis[vetIndex]);
    }

    vm.updateValueAnalysis = function(productIndex, vetIndex){
        var url = vm.cartProducts.entries[productIndex].valueAnalysis[vetIndex].vetLink;
        if(vm.hasDuplicateVetUrl(url, productIndex, vetIndex)){
            vm.cartProducts.entries[productIndex].valueAnalysis[vetIndex].hasError = true;
        }else{
            var catQuoteVETForm = {
                        entryNumber: vm.cartProducts.entries[productIndex].entryNumber, // always needed
                        // vetLink: vm.cartProducts.entries[productIndex].valueAnalysis[vetIndex].vetLink, // send only for add or remove
                        // vetProduct: vm.cartProducts.entries[productIndex].valueAnalysis[vetIndex].vetProduct // send only for add or remove
                        catQuoteVETDatas: vm.cartProducts.entries[productIndex].valueAnalysis // send only for edit
                    };

            var url = '/quote/'+vm.quotationCode+catEndpointService.quotePageEditSaveValueAnalysisEndPoint;
            httpService.postJsonData(url, catQuoteVETForm).then(function(response){
                vm.setCartDataOnRefresh(response.data);
            }, function(error){
                
            });
        }
    }

    vm.removeValueAnalysis = function(productIndex, vetIndex){
        var catQuoteVETForm = {
                    entryNumber: vm.cartProducts.entries[productIndex].entryNumber, // always needed
                    vetLink: vm.cartProducts.entries[productIndex].valueAnalysis[vetIndex].vetLink, // send only for add or remove
                    vetProduct: vm.cartProducts.entries[productIndex].valueAnalysis[vetIndex].vetProduct // send only for add or remove
                    // catQuoteVETDatas: vm.cartProducts.entries[productIndex].valueAnalysis // send only for edit
                };

        var url = '/quote/'+vm.quotationCode+catEndpointService.quotePageRemoveValueAnalysisEndPoint;
        httpService.postJsonData(url, catQuoteVETForm).then(function(response){
            vm.setCartDataOnRefresh(response.data);
        }, function(error){
            
        });
    }

    vm.cancelValueAnalysis = function(productIndex, vetIndex) {
        if(!vetIndex && vetIndex != 0){
            vm.cartProducts.entries[productIndex].vetData = null;
        }else {
            vm.cartProducts.entries[productIndex].valueAnalysis[vetIndex] = vm.cartProducts.entries[productIndex].currentEditingVet;
            vm.cartProducts.entries[productIndex].valueAnalysis[vetIndex].isEditing = false;
            // delete vm.cartProducts.entries[productIndex].currentEditingVet;
        }
        vm.cartProducts.entries[productIndex].isVetEditing = false;
    }

    vm.addMoreValueAnalysis = function(productIndex){
        vm.cartProducts.entries[productIndex].showAddVET = true;
    }

    vm.hasDuplicateVetUrl = function(url, productIndex, vetIndex){
        var filteredData = _.filter(vm.cartProducts.entries[productIndex].valueAnalysis, function(v, i){
            if((vetIndex || vetIndex == 0) && vetIndex == i){
                return false;
            }
            return (v.vetLink == url);
        });
        return (filteredData.length != 0);
    }

    $scope.deleteCartProducts = function(index) {
        var cartDeleteURL = catEndpointService.quotationCartPageDeleteEndPoint + '?entryNumber=' + vm.cartProducts.entries[index].entryNumber;
        vm.showRemoveModalInstance.dismiss("cancel");
        vm.ajaxSuccess = false;
        httpService.get(cartDeleteURL).then(function(response) {
            if($scope.isRedirect){
                $scope.$emit('submittedQuote','stopWatching');
                $window.location.href = "/";
            }else{
                vm.ajaxSuccess = true;
                vm.cartProducts.entries.splice(index, 1);
                vm.setCartDataOnRefresh(response.data);
                ACC.minicart.refreshMinicartCountNum();
            }
        }, function(error) {

        });
    };

    vm.Prevent = function(event,rowIndex) {
        if (event.target.value == '0') {
            if(event.target.name == 'EppQty'){
                vm.cartProducts.entries[rowIndex].entries[0].quantity = 1;
            }else if(event.target.name == 'CsaQty'){
                vm.cartProducts.entries[rowIndex].entries[1].quantity = 1;
            }
            event.target.value = 1;
            vm.checkEppCsaChange(event, rowIndex, true);
        }
    }

    vm.reinitializeEPPCSA = function(event,rowIndex, isPreventCall){
        var target = event.target.name;
        if(event.target.value == ''){
            switch(target){
                case "CsaHrs":
                if(vm.cartProducts.entries[rowIndex].entries != undefined && vm.cartProducts.entries[rowIndex].entries != null){
                    vm.cartProducts.entries[rowIndex].entries[1].configurationInfos[0].configurationValue = 0;
                } else {
                    event.target.value = '0';
                }
                break;
                case "CsaYrs":
                if(vm.cartProducts.entries[rowIndex].entries != undefined && vm.cartProducts.entries[rowIndex].entries != null){
                    vm.cartProducts.entries[rowIndex].entries[1].configurationInfos[2].configurationValue = 0;
                } else {
                    event.target.value = '0';
                }
                break;
                case "CsaPrice":
                if(vm.cartProducts.entries[rowIndex].entries != undefined && vm.cartProducts.entries[rowIndex].entries != null){
                    vm.cartProducts.entries[rowIndex].entries[1].basePrice.value = 0;
                } else {
                    event.target.value = '0';
                }
                break;
                case "CsaQty":
                if(vm.cartProducts.entries[rowIndex].entries != undefined && vm.cartProducts.entries[rowIndex].entries != null){
                    vm.cartProducts.entries[rowIndex].entries[1].quantity = 0;
                } else {
                    event.target.value = '1';
                }
                break;
                case "EppPrice":
                if(vm.cartProducts.entries[rowIndex].entries != undefined && vm.cartProducts.entries[rowIndex].entries != null){
                    vm.cartProducts.entries[rowIndex].entries[0].basePrice.value = 0;
                } else {
                    event.target.value = '0';
                }
                break;
                case "EppQty":
                if(vm.cartProducts.entries[rowIndex].entries != undefined && vm.cartProducts.entries[rowIndex].entries != null){
                    vm.cartProducts.entries[rowIndex].entries[0].quantity = 0;
                } else {
                    event.target.value = '1';
                }
                break;
            }
        }
    }

    vm.addNewEppOption = function(eppItems,eppHelper){
            eppHelper.isNewBtnDisable = !!1;          
            eppItems.push(EppHelperFactory.register());
    }
    vm.addNewCsaOption = function(csaItems,csaHelper){
        csaHelper.isNewBtnDisable = !!1;
        csaItems.push(CsaHelperFactory.register());
    }
    vm.checkEppCsaChange = function(isCsa, rowIndex){
        if(isCsa){
            vm.cartProducts.entries[rowIndex].entries[1].csaApplyEnable = true;
        }else{
            vm.cartProducts.entries[rowIndex].entries[0].eppApplyEnable = true;
        }
    }

    vm.clearCsaWatch = function(rowIndex){
        if(vm.cartProducts.entries[rowIndex].csaWatch){
            vm.cartProducts.entries[rowIndex].csaWatch();
        }
    }

    vm.clearEppWatch = function(rowIndex){
        if(vm.cartProducts.entries[rowIndex].eppWatch){
            vm.cartProducts.entries[rowIndex].eppWatch();
        }
    }

    vm.hasEppOrCsa = function(rowIndex){
        return (vm.cartProducts.entries[rowIndex].entries[1].csaApplyEnable ||
            vm.cartProducts.entries[rowIndex].entries[0].eppApplyEnable || (vm.cartProducts.entries[rowIndex].eppLabel != vm.selectLabel) || 
            (vm.cartProducts.entries[rowIndex].csaLabel != vm.selectLabel));
    }

    function updateCsaEppDetailsChange(newVal, oldVal, rowIndex, isCSA, isPreventCall) {
        if (isCSA && !vm.cartProducts.entries[rowIndex].entries[1].csaApplyEnable) {
            vm.cartProducts.entries[rowIndex].entries[1].csaApplyEnable =  (isPreventCall || (newVal != oldVal));
        }else if(!isCSA && !vm.cartProducts.entries[rowIndex].entries[0].eppApplyEnable){
            vm.cartProducts.entries[rowIndex].entries[0].eppApplyEnable =  (isPreventCall || (newVal != oldVal));
        }
    }

    vm.isApplyEnable = function(productIndex, isApply){
        var bool = (vm.cartProducts.entries[productIndex].eppLabel  &&vm.cartProducts.entries[productIndex].eppLabel !== vm.selectLabel && 
                vm.cartProducts.entries[productIndex].entries && vm.cartProducts.entries[productIndex].entries[0] &&
                Number(vm.cartProducts.entries[productIndex].entries[0].quantity) && Number(vm.cartProducts.entries[productIndex].entries[0].basePrice.value));
        if(isApply){
             return (bool && !!vm.cartProducts.entries[productIndex].entries[0].eppApplyEnable);
        }
        return bool;
    }

    vm.isCSAApplyEnable = function(productIndex, isApply){
        var bool = (vm.cartProducts.entries[productIndex].csaLabel && vm.cartProducts.entries[productIndex].csaLabel !== vm.selectLabel &&
                vm.cartProducts.entries[productIndex].entries && vm.cartProducts.entries[productIndex].entries[1] &&
                Number(vm.cartProducts.entries[productIndex].entries[1].quantity) && Number(vm.cartProducts.entries[productIndex].entries[1].basePrice.value) &&
                Number(vm.cartProducts.entries[productIndex].entries[1].configurationInfos[0].configurationValue) && 
                Number(vm.cartProducts.entries[productIndex].entries[1].configurationInfos[2].configurationValue));
        if(isApply){
            return (bool && !!vm.cartProducts.entries[productIndex].entries[1].csaApplyEnable);
        }
        return bool;
    }
}]);
