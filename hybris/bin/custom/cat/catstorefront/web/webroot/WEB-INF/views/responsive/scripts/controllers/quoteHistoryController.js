catApp.filter('showPendingSubmission',function(){
	return function(input){
		//console.log(input);
		if(input === null){
			return "-";
		} else if(input === "PENDING_APPROVAL") {
			return "Pending Submission";
		} else {
			return input;
		}
	}
});
catApp.controller('quoteHistoryController', ['httpService', 'catEndpointService', '$scope', '$location','$window', function(httpService, catEndpointService, $scope, $location, $window) {
    var vm = this;
    vm.quoteHistory = {};
    vm.view = {
	    isFirstLoad: true,
        hasOrders: true,
        sortBtn: false,
        sortMetaData: {
            byCode:{
                canSort: true,
                isSelected: false,
                isReverseSort: false,
                key:"byCode",
                name: "quote number"
            },
            byDate:{
                canSort: false,
                isSelected: true,
                isReverseSort: false,
                key:"byDate",
                name: "submit date"
            },
            byModifiedDate:{
                canSort: false,
                isSelected: false,
                isReverseSort: false,
                key:"byModifiedDate",
                name: "date modified"
            },
            byState:{
                canSort: true,
                isSelected: false,
                isReverseSort: false,
                key:"byState",
                name: "status"
            }
        },
        currentPage: '',
        pagination: {
        }
    }


    vm.convertToLowerCase = function(text) {
        return text.toLowerCase();
    }

    vm.setSortedKey = function(sortedKey){
        sortedKey = sortedKey || '';
        var isAsc = (sortedKey.indexOf('Asc') != -1);
        sortedKey = sortedKey.replace('Asc', '');
        for(var k in vm.view.sortMetaData) {
           if(vm.view.sortMetaData[k].key == sortedKey){
                vm.view.sortMetaData[k].isReverseSort = isAsc;
            }
        }
    }

    vm.onSortClick = function(key){
        vm.view.sortBtn = !vm.view.sortBtn;
        var query = vm.view.sortMetaData[key].key + (vm.view.sortMetaData[key].isReverseSort ? '' : 'Asc');
        for(var i in vm.view.sortMetaData){
            if(i == key){
                vm.view.sortMetaData[key].isSelected = true;
                vm.view.sortMetaData[key].isReverseSort = !vm.view.sortMetaData[key].isReverseSort;
            }else{
                vm.view.sortMetaData[i].isSelected = false;
                vm.view.sortMetaData[i].isReverseSort = false;
            }
        }
        httpService.get(catEndpointService.getQuotes+"?page=0&sort="+query +"&search="+vm.view.pagination.search).then(function(response) { //+"&search="+vm.view.pagination.search
            if (response.status == 200) {
                setDomData(response.data);
            }
        }, function(error) {});
    }

    vm.pageChanged = function(){
	 	httpService.get(catEndpointService.getQuotes+"?page="+ (vm.view.pagination.currentPage-1) +"&sort="+vm.quoteHistory.sortKey +"&search="+vm.view.pagination.search).then(function(response) { // +"&search="+vm.view.pagination.search
            if (response.status == 200) {
                setDomData(response.data);
            }
        }, function(error) {});
    }

    vm.showMoreMobile = function(param){
        httpService.get(catEndpointService.getQuotes+"?page="+ (vm.view.pagination.currentPage) +"&sort="+vm.quoteHistory.sortKey +"&search="+vm.view.pagination.search).then(function(response) {
           if (response.status == 200) {
               setDomData(response.data, param);
           }
       }, function(error) {});
    }

    vm.init = function(){
        httpService.get(catEndpointService.getQuotes).then(function(response) {
            if (response.status == 200) {
                setDomData(response.data);
            }
        }, function(error) {});
    }
	
	vm.searchClick = function(){
		httpService.get(catEndpointService.getQuotes+"?sort="+vm.quoteHistory.sortKey +"&search="+vm.view.pagination.search).then(function(response) { //+"&search="+vm.view.pagination.search
            if (response.status == 200) {
                setDomData(response.data);
            }
        }, function(error) {});
    }
    
    vm.clearSearch = function(){
        vm.view.pagination.search = "";
        vm.searchClick();
    }
	
    function setDomData(response, param){
    	vm.quoteStatusMetaData = response.quoteStatusMetaData;
        response =  response.quoteList;
        if(vm.view.isFirstLoad){
            vm.view.isFirstLoad = false;
            vm.view.hasOrders = (response.results.length !== 0);
        }
        if(param == 'mobile') {
            vm.quoteHistory.orders = vm.quoteHistory.orders.concat(response.results);
        } else {
            vm.quoteHistory.orders = response.results;
        }
        var searchKey = vm.view.pagination.search;
        vm.view.pagination = response.pagination;
        vm.view.sorts = response.sorts;
        vm.view.pagination.currentPage = (vm.view.pagination.currentPage+1);
        vm.view.pagination.search = (searchKey ? searchKey : "");
        vm.quoteHistory.sortKey = response.pagination.sort;
    }
    
    vm.getDateFormat = function(_date){ // "MM/dd/yyyy"
        if(_date){
            var date = new Date(_date);
            //_date = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
            _date = vm.getTwoDigits(date.getMonth() + 1) + "-" + vm.getTwoDigits(date.getDate()) + "-" + date.getFullYear(); 
        }else {
            _date = '-';
        }
        return _date;
    }

    vm.getTwoDigits = function(val){
        return ("00"+val).slice(-2);
    } 

}]);