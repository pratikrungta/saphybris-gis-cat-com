catApp.controller('reorderCartController', ['$rootScope', '$scope', 'catEndpointService', 'httpService', 'reorderCartService', '$window','constantService', '$timeout', 'modalPopUpFactory', '$uibModal', 'getUrlParamFactory',function($rootScope, $scope, catEndpointService, httpService, reorderCartService, $window,constantService,$timeout,modalPopUpFactory,$uibModal, getUrlParamFactory) {
    var vm = this, urlParams = getUrlParamFactory.getUrlParams();
    vm.view = {};
    vm.allowOnBeforeUnload = false;
    vm.view.reorderCartData = [];
    vm.view.poNumberCountArray = [];
    vm.view.poNumberValuesArray = [];
    vm.view.isEmptyCart = false;
    vm.view.duplicatePOError = false;
    vm.view.lengthExceedingPOError = false;
    vm.view.specialCharsPOError = false;
    vm.view.emptyPOError = false;
    vm.view.hybrisPoErrorArr = [];
    vm.laneErrors = {};

    $scope.watchPages = vm.view;
    $scope.cartSaveConfirmationMsg = "Your Cart will be Saved.";

    vm.loadReorderCart = function(){
        httpService.get(catEndpointService.reorderCartProductEndPoint).then(function(response) {
            vm.view.cartDataFromResponse = response.data;
            if(vm.view.cartDataFromResponse.entries != undefined && vm.view.cartDataFromResponse.entries != null && vm.view.cartDataFromResponse.entries.length){
                var returnedObject = reorderCartService.createReorderCartData(vm.view.cartDataFromResponse);
                vm.view.reorderCartData = returnedObject.reorderCartData;
                vm.view.poNumberCountArray = returnedObject.poNumberCountArray;
                vm.view.poNumberValuesArray = returnedObject.poNumberValuesArray;
                if(urlParams.isFromReviewOrder){
                    vm.validateLaneToReviewOrder(true);
                }
            } else {
                vm.view.isEmptyCart = true;
                window.onbeforeunload = null;
            }
        }, function(error){
        });
    };
    vm.roleFeatureInfoBCP = function(isUTVUser){
        if(isUTVUser){
            ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.cuvUserEventName,ACC.gaHelper.bcpOrderEventName);
        }
        else{
            ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.bcpUserEventName,ACC.gaHelper.bcpOrderEventName);
        }
    }
    vm.backToReorderInitiated = function(){
        if(!vm.isBackToReorderClicked){
            window.history.back();
            vm.isBackToReorderClicked=true;
        }
    }
    vm.backToReorder = function(){
        return /(reorder\/viewManageOrders)/.test(document.referrer);
    };
    vm.backToReorderPage = function(){
        $window.location.href = '/reorder/viewManageOrders';
    };
    $scope.$on('checkEmptyCart', function(evt, data) {
        vm.view.isEmptyCart = data;
        window.onbeforeunload = null;
    });
    $scope.$on('updatedCartResponse', function(evt, data) {
        vm.view.cartDataFromResponse = data.obj;
        if(data.updateFlag) {
            vm.validateLaneToReviewOrder(true);
        }
    });
    $scope.$on('duplicatePOError', function(evt, data) {
        (data > 0) ? vm.view.duplicatePOError = true : vm.view.duplicatePOError = false;
    });
    $scope.$on('lengthExceedingPOError', function(evt, data) {
        (data > 0) ? vm.view.lengthExceedingPOError = true : vm.view.lengthExceedingPOError = false;
    });
    $scope.$on('specialCharsPOError', function(evt, data) {
        (data > 0) ? vm.view.specialCharsPOError = true : vm.view.specialCharsPOError = false;
    });
    $scope.$on('poNumberValuesArray', function(evt, data) {
        vm.view.poNumberValuesArray = data;
    });
    $scope.$on('broadcastTextboxFocus', function(evt, data) {
        vm.allowOnBeforeUnload = true;
    });
    $scope.$on('updateReorderCartData', function(evt, data) {
        vm.view.reorderCartData = data;
    });

    $scope.serializeRequestBody = function(poEntryData) {
        var requestBody = [];
        var index = 0;
        
        angular.forEach(poEntryData, function(value, salesModelId){
            var obj = {
                'salesModelId' : salesModelId,
                'purchaseOrderNumberSet' : [],
                'purchaseOrderNumberConfigIdMap': {}
            };
            angular.forEach(value, function(data, key){
                obj['purchaseOrderNumberSet'] = obj['purchaseOrderNumberSet'].concat(data.poNumberValuesArray.purchaseOrderNumberSet);
                obj['purchaseOrderNumberConfigIdMap'][data.product.code+"__"+data.entryNumber] = data.poNumberValuesArray.purchaseOrderNumberSet;
            });
            requestBody[index] = obj;
            index++;
        });
        return requestBody;
    }

    vm.verifyHybrisError = function(response){
        if(response){
            vm.view.hybrisPoErrorArr = _.map(response, function(errorObject){
                return errorObject.purchaseOrderNumberSet;
            });
            vm.view.hybrisPoErrorArr = _.flatten(vm.view.hybrisPoErrorArr);
            $rootScope.$broadcast('updateHybrisPoErrorsArr',vm.view.hybrisPoErrorArr);
        }
    }

    vm.validateLaneToReviewOrder = function(isOnLoadCall){
        vm.laneErrors = {};
        httpService.get(catEndpointService.reOrderLaneValidation).then(function(response) {
            if(response.data && (Object.keys(response.data)).length){
                _.each(response.data, function(val, key) {
                    _.each(val, function(errorObj) {
                        _.map(vm.view.reorderCartData, function(v, k) {
                            vm.view.reorderCartData[k] = _.map(v, function(configObj) {
                                if(configObj.entryNumber == key){
                                    if(errorObj.errorMessage.indexOf("exhausted") != -1 && !vm.laneErrors[k]){
                                        vm.laneErrors[k] = errorObj.errorMessage;
                                    }else {
                                        configObj.disContinuedError = errorObj.errorMessage;
                                    }
                                }
                                return configObj;
                            })
                        })
                    })
                })
            }else if(!isOnLoadCall){
                vm.submitToReviewOrder();
            }
        }, function(error) {   
        });
    };

    vm.submitToReviewOrder = function(){
        vm.view.emptyPOError = vm.validatePOAndRedirect();
        if(!vm.view.emptyPOError && !vm.view.duplicatePOError && !vm.view.lengthExceedingPOError && !vm.view.specialCharsPOError){
            httpService.postJsonData(catEndpointService.reorderSavePurchaseOrderNumbersEndPoint,$scope.serializeRequestBody(vm.view.reorderCartData)).then(function(response) {
                if (response.status == 200) {
                    window.onbeforeunload = null;
                    if(response.data.length == 0){
                        $window.location.href = '/cart/reviewReOrder';
                    } else {
                        vm.verifyHybrisError(response.data);
                    }   
                }
            }, function(error) {   
            });
        }
    };

    window.onbeforeunload = function(){
        if(vm.allowOnBeforeUnload) {
            return true;
        } else {
            return void (0);
        }
    }
    
    vm.validatePOAndRedirect = function(){
        var emptyPONumber = false;
        var spclCharRegx = /[^a-zA-Z0-9]/;
        angular.forEach(vm.view.reorderCartData,function(objValue,key){
            angular.forEach(objValue,function(val,parentIndex){
                if(val.poNumberValuesArray && val.poNumberValuesArray.purchaseOrderNumberSet){
                    angular.forEach(val.poNumberValuesArray.purchaseOrderNumberSet,function(poNumber,childIndex){
                        if(poNumber == ''){
                            val.poNumberValuesArray.purchaseOrderErrorSet.errorNos = [''];
                            val.poNumberValuesArray.purchaseOrderErrorSet.errorMsg = "PO Number Cannot be Empty.";
                            emptyPONumber = true;
                            return false;
                        } else if(spclCharRegx.test(poNumber)) {
                            val.poNumberValuesArray.purchaseOrderErrorSet.errorNos[childIndex] = poNumber;
                            val.poNumberValuesArray.purchaseOrderErrorSet.errorMsg = "PO number cannot contain special characters.";
                            emptyPONumber = true;
                            return false;
                        } else if (poNumber.length > 12) {
                            val.poNumberValuesArray.purchaseOrderErrorSet.errorNos[childIndex] = poNumber;
                            val.poNumberValuesArray.purchaseOrderErrorSet.errorMsg = "PO number cannot exceed 12 characters.";
                            emptyPONumber = true;
                            return false;
                        } 
                    })
                }
            })
        })
        return emptyPONumber;
    }
}]);
