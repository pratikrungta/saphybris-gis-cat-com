catApp.controller('reorderProductsController', ['$scope', 'catEndpointService', 'httpService', 
'$window','constantService', '$timeout','modalPopUpFactory','getUrlParamFactory','properMessageService','$anchorScroll','$uibModal',
function($scope, catEndpointService, httpService, $window,
    constantService,$timeout,modalPopUpFactory,getUrlParamFactory,properMessageService, $anchorScroll,$uibModal) {
    $scope.informatonMesage = "/_ui/responsive/cat/templates/inventoryBarGraphInfoTool.html";
    var vm = this;
    vm.defaultDownloadImage = constantService.defaultDownloadImage;
    vm.originalSearchResults = [];
    vm.cartLaneType = "";
    vm.update_button = false;
    vm.view = {
        total: 0,
        showingConfig: "",
        currentOpenedConfig: null,
        productFamily: {
            isDisabled: false,
            isOpen: false,
            selectedProductName: "",
            selectedCode: ""
        },
        salesModel: {
            isDisabled: true,
            selectedModelName: "",
            selectedCode: "",
            listOfSalesModel: []
        },
        searchResults: [],
        searchDesktop:[],
        searchMobile:[],
        searchResultsMetaData: "",
        hasValidSearchParams: false,
        pageLoadingParams: {
            searchResultsLoaded: false,
            salesModelLoaded: false,
            primaryOfferingLoaded: false,
            pagedLoaded: false,
            ajaxCallSuccess: true
        },
        pagination: {
            currentPage: 0,
            pageSize:2,
            totalNumberOfResults: 3
        },
        initialLoad: true,
        primaryOfferingForCards: "",
        searchResultsMobile:[],
        laneButtons : [
            {
                buttonKey : ACC.localization.plp1,
                // dotColor : '#e96979',
                isSelectedKey : 'lane1'
            },
            {
                buttonKey : ACC.localization.plp2,
                // dotColor : '#f5a623',
                isSelectedKey : 'lane2'
            },
            {
                buttonKey : ACC.localization.plp3,
                // dotColor : '#8b572a',
                isSelectedKey : 'lane3'
            }
        ],
        laneBtnSelected: 'lane1'
    };
    vm.mobileMeta = { isMobile: false };

    var angularWindow = angular.element($window);

    vm.getFormattedText = constantService.getTwoDigitFormattedText;

    angularWindow.bind('resize', function () {
        vm.updateMobileMetaData();
        //console.log('Resized your browser'+angularWindow.width());
    });
    
    vm.updateMobileMetaData = function(){
        if(angularWindow.width() <= parseInt(screenSmMax)){
            vm.mobileMeta.isMobile = true;
        }else{
            vm.mobileMeta.isMobile = false;
        }

        if(vm.mobileMeta.isMobile){
            vm.view.searchResultsMobile = vm.view.searchMobile;
        }else {
            vm.view.searchResults = vm.view.searchDesktop;
        }
        if(!$scope.$$phase) {
          $scope.$apply();
        }
    }

    vm.init = function(){
        vm.updateMobileMetaData();
        vm.getReorderData();
    }

    vm.laneBtnClick = function(laneBtn, searchObj, mobileFlag) {
      if(mobileFlag !== undefined && searchObj.laneBtnSelected == laneBtn.isSelectedKey) {
        searchObj.laneBtnSelected = '';
      } else {
        searchObj.laneBtnSelected = laneBtn.isSelectedKey;
        vm.view.laneBtnSelected = searchObj.laneBtnSelected;
      }
    }

    vm.navigateToPdp = function(url){
        if(!!url){
            history.replaceState(null,
            null,
            '?'+'page='+vm.view.pagination.currentPage+
            '&PrdselectedCode='+vm.view.productFamily.selectedCode+
            '&'+'productFamily='+vm.view.productFamily.selectedProductName+'&salesSelectedCode='+vm.view.salesModel.selectedCode+
            '&salesModel='+vm.view.salesModel.selectedModelName);
            $window.location.href=getUrlParamFactory.formParams({'page':vm.view.pagination.currentPage,'url':url});
        }
    }

    vm.updatePageLoadingParams = function() {
        vm.view.pageLoadingParams.pagedLoaded = true; //vm.view.pageLoadingParams.searchResultsLoaded;
    }
    vm.closeMoblieFilters = function(){
        ACC.reorder.hideSidebar();
    }
    vm.loadMoreMobileResults = function(incrementer){
        vm.view.pagination.currentPage = parseInt(vm.view.pagination.currentPage,10)+(incrementer||0);
        vm.view.searchResults = vm.view.searchMobile.slice(0, vm.view.pagination.currentPage*vm.view.pagination.pageSize);
    }

    vm.getReorderData = function(){
        angular.element("#pageLoader").show();
        httpService.get(catEndpointService.reorderSearchResults).then(function(response) {
            if(vm.view.initialLoad){
                var prdFamilyName = getUrlParamFactory.getUrlParams().productFamily,
                    prdFamilyCode = getUrlParamFactory.getUrlParams().PrdselectedCode,
                    salesModelCode = getUrlParamFactory.getUrlParams().salesSelectedCode,
                    salesModelName = getUrlParamFactory.getUrlParams().salesSelectedCode;
            }
            var pageNo = getUrlParamFactory.getUrlParams().page || vm.view.pagination.currentPage;
            if(response.status == 200){
                vm.view.pagination = response.data.pagination;
                vm.view.pagination.currentPage = pageNo||1;
                //response.data = tempData;
                vm.originalSearchResults = vm.getDataFormat(response.data.results);
                vm.updatePageLoadingParams();
                vm.cartLaneType = response.data.cartLaneType;
                if(!angular.equals(getUrlParamFactory.getUrlParams(),{}) && prdFamilyName.length && salesModelName.length){
                    vm.productFamilySelected(prdFamilyCode||'',prdFamilyName||'');
                    vm.salesModelSelected(salesModelCode||'',salesModelName||'');
                    vm.updateSearchData();
                }else{
                    vm.pageChanged();
                }
                if(!vm.view.initialLoad && vm.update_button){
                    vm.updateSearchData(vm.view.pagination.currentPage);
                }
                vm.view.initialLoad = false;
                angular.element("#pageLoader").hide();
            }
        }, function(error) {
            // Error scenarios to be handled
        })
    }

    vm.pageChanged = function(){
        var startIndex = (vm.view.pagination.currentPage-1)*vm.view.pagination.pageSize;
        vm.view.searchDesktop = vm.originalSearchResults.slice(startIndex, startIndex+vm.view.pagination.pageSize);
        vm.view.searchMobile = vm.originalSearchResults;
        // Used to close the opened tabs while moving using pagination
        if(vm.view.currentOpenedConfig !== null){
            vm.originalSearchResults[vm.view.currentOpenedConfig].showConfiguration = false;
            vm.view.currentOpenedConfig = null;
        }

        if(vm.view.isMobile){
            vm.loadMoreMobileResults();
        }else{
            vm.view.searchResults = vm.view.searchDesktop;
        }
    }

    vm.getDataFormat = function(results){
        return _.map(results, function(salesModelItem, index){
          !vm.mobileMeta.isMobile ? salesModelItem.laneBtnSelected = 'lane1' : salesModelItem.laneBtnSelected = '';
                    _.map(salesModelItem.configProductsList, function(configVariant){
                        configVariant.totalAddedItem = 00;
                        _.map(configVariant.configVariantList, function(configItem){
                            
                            if(configItem.orderedQty && !vm.view.initialLoad) {
                                configVariant.totalAddedItem = vm.getFormattedText(parseInt(configVariant.totalAddedItem) + configItem.orderedQty);
                                configItem.isAddedToCart = true;
                                configItem.customConfig = vm.getFormattedText(configItem.orderedQty);
                            } else {
                                if(!configVariant.totalAddedItem) {
                                    configVariant.totalAddedItem = vm.getFormattedText(0);
                                }
                                // else{
                                //     configVariant.totalAddedItem = vm.getFormattedText(parseInt(configVariant.totalAddedItem) + configItem.orderedQty);
                                // }
                                configItem.isAddedToCart = false;
                                configItem.customConfig = vm.getFormattedText(0);
                            }
                            vm.view.total += configItem.orderedQty;
                            configItem.wholeNumberError = false;
                            configItem.maximumQuantityError = false;
                            configItem.canReorder = false;
                            configItem.maximumStockLevel = !!Number(salesModelItem.maximumStockLevel)?Number(salesModelItem.maximumStockLevel):0;
                            configItem.currentStockLevel = !!Number(salesModelItem.currentStockLevel)?Number(salesModelItem.currentStockLevel):0;
                            // configItem.allowedReorderStockLevel = configItem.maximumStockLevel;
                            configItem.allowedReorderStockLevel =  (configItem.maximumStockLevel <= constantService.maxOrderLimit) ? configItem.maximumStockLevel : constantService.maxOrderLimit
                            if(configItem.maximumStockLevel > configItem.currentStockLevel){
                                configItem.allowedReorderStockLevel = configItem.maximumStockLevel-configItem.currentStockLevel;
                            }
                            return configItem;
                        }); 
                    });
                    salesModelItem.showConfiguration = false;

                    salesModelItem.index = index;
                    return salesModelItem;
                });
    }
    $scope.maintainIsUTVCart = function(isUTVCart){
        $scope.isUTVCart=isUTVCart;
    }
    $scope.dismissCartLostPopup = function(){
        modalPopUpFactory.close($scope.cartLostPopup);
     }
     $scope.clearCart = function(){
         var data = modalPopUpFactory.memoiz('get','reorderData');
         $scope.isUTVCart=null;
         vm.cartLaneType = "";
        vm.addToCart(data.salIndex, data.configIdKey, data.configIndex);
        $scope.dismissCartLostPopup();
     }
     vm.showSelectOptionsPopup = function(configObj,searchObj){
        $scope.cartLostMessage = "Your current cart contains "+vm.cartLaneType+" products, which would be lost. Do you want to continue?";
        $scope.fbcConfigObj = configObj;
        $scope.searchObjStoreFbc = searchObj;
        if(vm.cartLaneType != '' && vm.cartLaneType != searchObj.laneBtnSelected.toUpperCase() && vm.cartLaneType != null  ){
            vm.showSelectOptionsPopupInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: '/_ui/responsive/cat/templates/fbcAddToCartPopup.html',
                scope: $scope,
                size: 'add-to-cart-redirect-modal',
            });
        } else {
            $scope.navigateToMustSelect();
        }
    }
    $scope.closeSelectOptionsPopup = function(){
        vm.showSelectOptionsPopupInstance.close();
    }
     $scope.navigateToMustSelect = function(){
        $window.location.href = "/fbc/?productCode="+$scope.fbcConfigObj.code+"&salesModel="+$scope.fbcConfigObj.salesModelId+"&Lane="+$scope.searchObjStoreFbc.laneBtnSelected+"&prodFamily="+$scope.fbcConfigObj.categories[0].name+"&primaryImageUrl="+$scope.fbcConfigObj.primaryImageUrl+"&dcaCode="+$scope.fbcConfigObj.baseProduct+"&qty="+$scope.fbcConfigObj.customConfig+"&fromPage=Reorder+Products";
     }
    vm.addToCart = function(salesModelIndex, configIdKey, configObjIndex,isUTVCart,cartType){
        $scope.isUTVCart = isUTVCart;
        if($scope.isUTVCart && vm.cartType !='cartProductIsBCP'){
            $scope.cartLostMessage = properMessageService.cartLostMessage(vm.cartType);
            $scope.cartLostPopup = modalPopUpFactory.open({
                	backdrop: 'static',
                	ariaLabelledBy: 'modal-title',
                	ariaDescribedBy: 'modal-body',
                	templateUrl: '/_ui/responsive/cat/templates/cartLostAlertPopup.html',
                	size: 'md',
                	scope: $scope,
                  });
                  modalPopUpFactory.memoiz('add','reorderData',
                  {'salIndex':salesModelIndex, 'configIdKey':configIdKey, 'configIndex':configObjIndex});

            return;
        }else if(vm.cartLaneType && vm.cartLaneType.toLowerCase() != configIdKey){
            $scope.cartLostMessage = "Your current cart contains "+vm.cartLaneType+" products, which would be lost. Do you want to continue?"
            $scope.cartLostPopup = modalPopUpFactory.open({
                    backdrop: 'static',
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: '/_ui/responsive/cat/templates/clearLaneCartPopup.html',
                    size: 'md',
                    scope: $scope,
                  });
                  modalPopUpFactory.memoiz('add','reorderData',
                  {'salIndex':salesModelIndex, 'configIdKey':configIdKey, 'configIndex':configObjIndex});

            return;
        }
        vm.cartType = 'cartProductIsBCP';
        var configObj = vm.view.searchResults[salesModelIndex].configProductsList[configIdKey].configVariantList[configObjIndex];
        configObj.isAddedToCart = true;
        var laneType = (vm.cartLaneType.toLowerCase() == configIdKey) ? "" : configIdKey;
        httpService.get(catEndpointService.reOrderAddToCartEndPoint+'/'+configObj.code+'/?reorderProductQuantity='+configObj.customConfig+'&laneType='+laneType).then(function(response) {
            if(response.status == 200 && response.data && response.data.success){
                if(laneType){
                    vm.view.total = 0;
                    vm.getReorderData();
                }else{
                    vm.cartLaneType = response.data.cartLaneType;
                    configObj.customConfig = Number(configObj.customConfig);
                    vm.view.searchResults[salesModelIndex].total += configObj.customConfig;
                    var convertToNo = parseInt(vm.view.searchResults[salesModelIndex].configProductsList[configIdKey].totalAddedItem);
                    vm.view.searchResults[salesModelIndex].configProductsList[configIdKey].totalAddedItem = vm.getFormattedText(configObj.customConfig + convertToNo) ;
                    vm.view.total += configObj.customConfig;
                    configObj.customConfig = vm.getFormattedText(configObj.customConfig);
                    vm.view.searchResults[salesModelIndex].configProductsList[configIdKey].configVariantList[configObjIndex] = configObj;
                    vm.originalSearchResults[vm.view.searchResults[salesModelIndex].index] = vm.view.searchResults[salesModelIndex];
                }
                ACC.minicart.refreshMinicartCountNum();
            }
        }, function(error) {
            // Error scenarios to be handled
            configObj.isAddedToCart = false;
        });
    }



    vm.retriveConfigValues = function(index){
        if(vm.view.currentOpenedConfig !== null && vm.view.currentOpenedConfig !== index){
            // if(vm.view.currentOpenedConfig > index) {
            //     vm.view.currentOpenedConfig = index;
            // }
            vm.view.searchResults[vm.view.currentOpenedConfig].showConfiguration = false;
            vm.originalSearchResults[vm.view.searchResults[vm.view.currentOpenedConfig].index].showConfiguration = false;
        }
        vm.view.searchResults[index].showConfiguration = !vm.view.searchResults[index].showConfiguration;
        vm.originalSearchResults[vm.view.searchResults[index].index].showConfiguration = vm.view.searchResults[index].showConfiguration;
        vm.view.currentOpenedConfig = index;
        $anchorScroll("plpProductId" + index);
    }

    vm.validateQuantity = function(pindex, configKey, index, eventType){
        var laneObj = vm.view.searchResults[pindex].configProductsList[configKey];
        var configObj = vm.view.searchResults[pindex].configProductsList[configKey].configVariantList[index];
        if((configObj.isAddedToCart && !configObj.fbcMustSelectFlag) || (laneObj.orderWindowStatus && laneObj.orderWindowStatus == 'closed')){
            return false;
        }
        configObj.customConfig = Number(configObj.customConfig);
        switch(configKey){
            case "lane2":
            configObj.allowedReorderStockLevel = laneObj.committedCount;
            break;
            case "lane3":
            configObj.allowedReorderStockLevel = laneObj.allocationCount;
            break;
            default:
            configObj.allowedReorderStockLevel = constantService.maxOrderLimit;
        }
        switch(eventType){
            case 'decrement':
                configObj.customConfig = configObj.customConfig > 0 ? (configObj.customConfig-1) : 0;
                break;
            case 'increment':
                configObj.customConfig = configObj.customConfig < configObj.allowedReorderStockLevel ? (configObj.customConfig+1) : configObj.customConfig;
                break;
            case 'change':
                // nothing to do in this case
        }
        // vm.view.searchResults[pindex].total = vm.getSalesModelAddedProductsTotal(vm.view.searchResults[pindex].configProducts);

        if(configObj.customConfig == 0 || !configObj.customConfig || configObj.allowedReorderStockLevel < configObj.customConfig || !configObj.isReorderable){
            configObj.canReorder = false;
        }else{
            configObj.canReorder = true;
        }
        if(configObj.allowedReorderStockLevel < configObj.customConfig){
            configObj.customConfig = configObj.allowedReorderStockLevel;
        }
        configObj.customConfig = vm.getFormattedText(configObj.customConfig);
        vm.view.searchResults[pindex].configProductsList[configKey].configVariantList[index] = configObj;
        vm.originalSearchResults[vm.view.searchResults[pindex].index] = vm.view.searchResults[pindex];
        // console.log(vm.originalSearchResults);
    }


    vm.getSalesModelAddedProductsTotal = function(configs){
        var total = 0;
        _.each(configs, function(configObj){
            total += Number(configObj.customConfig);
        });
        return total;
    }

    vm.productFamilySelected = function(code, name) {
        vm.view.productFamily.selectedCode = code;
        vm.view.productFamily.selectedProductName = name;

        vm.view.salesModel.isDisabled = true;
        vm.view.hasValidSearchParams = false;


        vm.view.salesModel.selectedModelName = "";

        vm.view.salesModel.selectedCode = "";
        vm.view.productFamily.isOpen = false;


        httpService.get(catEndpointService.lowStokProductFamilyEnpoint + "?categoryCode=" + vm.view.productFamily.selectedCode).then(function(response) {
            if (response.status == 200) {
                vm.view.salesModel.listOfSalesModel = response.data;
                vm.view.salesModel.isDisabled = false;
            }
        }, function(error) {});
    }

    vm.salesModelSelected = function(code, name) {
        vm.view.salesModel.selectedCode = code;
        vm.view.salesModel.selectedModelName = name;
        vm.view.hasValidSearchParams = true;
    }

    vm.updateSearchData = function(pageNo){
        vm.update_button = true;
        vm.view.pagination.currentPage = pageNo || 1;
        vm.view.searchDesktop = _.filter(vm.originalSearchResults, function(v) { return v.code == vm.view.salesModel.selectedCode });
        vm.view.searchMobile = vm.view.searchDesktop;
        vm.view.pagination.totalNumberOfResults = vm.view.searchDesktop.length;
        if(vm.view.isMobile){
            vm.loadMoreMobileResults();
        }else{
            var startIndex = (vm.view.pagination.currentPage-1)*vm.view.pagination.pageSize;
            vm.view.searchResults = vm.view.searchDesktop.slice(startIndex, startIndex+vm.view.pagination.pageSize);
        }
        
        vm.closeMoblieFilters();
    }

    vm.navigateToCart = function(){
        // httpService.get(catEndpointService.reOrderViewToCartEndPoint).then(function(response) {
        // }, function(error) {
        //     // Error scenarios to be handled
        // })
        history.replaceState(null,
        null,
        '?page='+vm.view.pagination.currentPage+
        '&PrdselectedCode='+vm.view.productFamily.selectedCode+
        '&productFamily='+vm.view.productFamily.selectedProductName+'&salesSelectedCode='+vm.view.salesModel.selectedCode+
        '&salesModel='+vm.view.salesModel.selectedModelName);
        //$window.location.href=getUrlParamFactory.formParams({'page':vm.view.pagination.currentPage,'url':url});
        $window.location.href = catEndpointService.reOrderViewToCartEndPoint;
    }

    vm.init();

}]);
