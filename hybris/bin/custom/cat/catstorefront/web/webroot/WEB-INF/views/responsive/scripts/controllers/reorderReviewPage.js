catApp.controller('reorderReviewController', ['$scope', 'catEndpointService', 'constantService', 'httpService', '$window','$location', '$filter', '$uibModal','reviewOrderValidation','$timeout','modalPopUpFactory','screenResizedService', '$q', function($scope, catEndpointService, constantService, httpService, $window, $location, $filter, $uibModal,reviewOrderValidation,$timeout,modalPopUpFactory,screenResizedService,$q) {
    var vm = this;

    vm.arr = [];
    vm.button = "Update";
    vm.range = [];
    vm.cuvTotalPrice = 0;
    vm.cuvTruckImage = "";
    vm.quantityBasedArray = [];
    vm.quantity = 1;
    vm.hybrisValidateError = false;
    vm.repeatError = false;
    vm.quantityError = false;
    vm.buttonCheck = true;
    vm.madatoryError = false;
    vm.view = {
        "purchaseOrder": "",
        "orderQty": "",
        "salesModel": "",
        "configurationID": "",
        "billingAddressCountry": "",
        "shippingAddressCountry": "",
        "userCertified": false,
        "CSRFToken": "",
        "isUtvProduct": false,
        "billingAndShippingAddressAreSame": false,
        "shippingAddress": [],
        "billingAddress": {
            "buildingNo": "",
            "streetName": "",
            "stateId": "",
            "zipCode": "",
            "country": ""
        },
        "bkpBillingAddress": {
            "buildingNo": "",
            "streetName": "",
            "stateId": "",
            "zipCode": ""
        },
        "response": {
            "orderNumber": "",
            "configurationId": "",
            "reorderUnits": "",
            "homePageLink": "/",
            "orderHistroyLink": "/my-account/cat/orders",
            "splitOrderCount": ''
        },
        "truckload": {
            "isEmptyPage":false,
            "Entries": "",
            "shipToAfterDates": "",
            "cuvProductsList": "",
            "totalCartQuantity": "",
            "totalCartPrice": "",
            "totalTruckloadCount": ""
        },
        "isFormValid": true,
        "isUTV": false,
        "errorPos": [],
        "resultPO": [],
        "dataModified": false,
        "processingRequest": false,
        "formSubmited": false,
        "reviewOrderEntry": "",
        'CUVproductImageList':{}
    };
    /**
     * Object structure for productListError
     * template structure
     * {truckLoadId:{
     *  CUV:{
     *  repeatError:boolean,
     *  hybrisError:boolean,
     *  mandatoryError:boolean,
     *  POsFilled:Number
     * }
     * }
     * }
     */
    vm.isMobile=screenResizedService.metaInfo.isMobile;
    vm.productListError={};

    vm.roleFeatureInfoCUV = function(){
           ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.cuvUserEventName,ACC.gaHelper.cuvOrderEventName);
    }

    var productCodeToSalesModelIdMap={};
    var validatinAndMappingErrorStatus =function(entry,truckloadId){

        vm.view.truckload.Entries[entry].range.forEach(function(inputPO,index){
            var productListErrorContainer={}; 
            if(!vm.productListError[truckloadId][inputPO.productCode]){
                vm.productListError[truckloadId][inputPO.productCode]={
                    repeatError:false,
                    hybrisError:false,
                    mandatoryError:false,
                    POsFilled:0,
                    POArray:[]
                }
                
            }
            productListErrorContainer=vm.productListError[truckloadId][inputPO.productCode];
            if(!inputPO.value){
                productListErrorContainer.mandatoryError=true;
            }else{
                productListErrorContainer.POsFilled+=1;
                productListErrorContainer.POArray.push(inputPO.value);
            }

            if(inputPO.hybrisValidateError){
                productListErrorContainer.hybrisError=true;
                productListErrorContainer.POsFilled-=1;
            }
           
            if(inputPO.repeatError&&vm.view.truckload.Entries[entry].repeatError){
                productListErrorContainer.repeatError=true;
                productListErrorContainer.POsFilled-=1;
            }

        });
    }
    var resetErrorStatus = function(truckloadId){
        for(var cuvProduct in vm.productListError[truckloadId]){
            if(vm.productListError[truckloadId].hasOwnProperty(cuvProduct)){
                vm.productListError[truckloadId][cuvProduct].repeatError=false;
                vm.productListError[truckloadId][cuvProduct].hybrisError=false;
                vm.productListError[truckloadId][cuvProduct].mandatoryError=false;
                vm.productListError[truckloadId][cuvProduct].POsFilled=0;
            }
    }
    }
    var validateTruckLoadProductListErrors = function(){
        for(var entry in vm.view.truckload.Entries){
            if(vm.view.truckload.Entries.hasOwnProperty(entry)){
                var truckloadId=vm.view.truckload.Entries[entry].truckloadId;
                vm.productListError[truckloadId]={};

                if(!angular.equals({},vm.productListError[truckloadId])){
                    resetErrorStatus(truckloadId);
                }
                validatinAndMappingErrorStatus(entry,truckloadId);
               
            }
            
        }
        console.log(vm.productListError);
        };


    vm.POStateObj={};
    /**
     * @param {Object} context- The context of the PO initialisation
     * @param {Object} context.ref - reference of thet truckLoadConfig component
     * @param {String} context.action - Differentiating the initalisation of PO Object and input value
     * @param {Object} context.input - Object which holds input Id as key and value with input's value
     * @param {String} context.productName- ProductCode
     * @param {String} context.inputId- Id of the input of that particular reference
     */

    vm.initiateTruckLoadPOobj=function(context){
        if(context.action==='initializeRef'){
            vm.POStateObj[context.ref.truckloadId]={ref:context.ref,inputs:{}};
            return;
        }
        vm.POStateObj[context.ref.truckloadId].inputs[context.inputId]=context.ref.range[parseInt(context.inputId,10)-1].value||'';
        context.ref.range.forEach(function(PO,index){
            if(PO.id===context.inputId){
                context.ref.range[index].productCode=context.productCode;
            }
        });
        
    }
    vm.filterPO=function(POArray,limit,productName){
        return POArray.filter(function(PO,index){
            return (!PO.productCode||PO.productCode===productName)?true:false;
        }).slice(0,limit);
    };
    vm.returnsSymbolOfstatus= function(CUVProductStatus,productCount){
        if(!CUVProductStatus){
            return '';
        }
        var status = CUVProductStatus.hybrisError
                    ||CUVProductStatus.repeatError
                    ||(CUVProductStatus.mandatoryError&&vm.userCheckedCertified)
        if(status){
            return 'warning';
        }
        return CUVProductStatus.POsFilled===productCount?'tick':'caution-info';
    };
    var validateTruckLoadPOFields = function(){
        var truckLoadComponent;
        var validateInputPOs = function(inputs,truckLoadComponent){
                var inputPO;
                for(inputPO in inputs){
                    if(inputs.hasOwnProperty(inputPO)){
                        if(!inputs[inputPO]||inputs[inputPO].toString().length===0){
                            vm.POStateObj[truckLoadComponent].ref.madatoryError=true;
                            vm.view.userCertified=false;
                            validateTruckLoadProductListErrors();
                            return;
                        }
                        
                    }
                }
                
                vm.POStateObj[truckLoadComponent].ref.madatoryError=false;
        }
        for(truckLoadComponent in vm.POStateObj){
                if(vm.POStateObj.hasOwnProperty(truckLoadComponent)){
                    validateInputPOs(vm.POStateObj[truckLoadComponent].inputs,truckLoadComponent);
                }
            }
            
            return;
    };
    var validateCUVPOFields = function(){
            var inputPO,isAnyPOFieldEmpty;
            for(inputPO in vm.range){
                    if(vm.range.hasOwnProperty(inputPO)){
                        if(!vm.range[inputPO].value){
                            isAnyPOFieldEmpty=true;
                            vm.range[inputPO].isInvalid=true;
                        }else{
                            vm.range[inputPO].isInvalid=false;
                        }
                    }
            }
            vm.madatoryError=isAnyPOFieldEmpty?true :false;
            vm.madatoryError ? vm.quantityBasedArray.map(function(v){
                v.madatoryError = true;
            }) : null;
    };
    var triggerPOValidations = function(){
        if(!angular.equals({},vm.POStateObj)){
            validateTruckLoadPOFields();
            return;
        } 
        //Differentiating CUV normal reorder validation   
        if(vm.range&&vm.range.length>0){
            validateCUVPOFields();
            return;
        } 
    };
    vm.triggerValidations=function(){
        vm.userCheckedCertified=true;
       reviewOrderValidation.notify(reviewOrderValidation.reviewOrderPageSubscriptionId
            ,function(addressValidationResultArray){
                        var i = addressValidationResultArray.length;
                        triggerPOValidations();
                            while(i--){
                                if(addressValidationResultArray[i].isAddressEmpty){
                                    vm.view.userCertified=false;
                                    return;
                                }
                            }
                    });
    }
    vm.serializeFormData = function(param) {
        var purchaseOrder = [],
        key="",
        address= null;
        if(param == 'truckload') {
            key = "shippingAddressList";
            address = vm.view.truckload.shippingAddressPk.join(",");
            angular.forEach(vm.view.truckload.Entries, function(val){
                purchaseOrder = purchaseOrder.concat(val.range.map(function(val1) { return val1.value.toString() }));
            });
        } else {
            key = "selectedShippingAddressPK";
            address = vm.productCompareDropDownLabel.id;
            purchaseOrder = vm.range.map(function(val) { return val.value.toString() });
        }
        var returnvalue = {
            "purchaseOrderList": purchaseOrder.join(","), //JSON.stringify(purchaseOrder),
            "salesModelCode": vm.view.salesModel,
            "configModelCode": vm.view.configurationID,
            "orderedQty": vm.quantity,
            "shippingAddress": vm.shippingAddressString,
            "comments": vm.userComments,
            "identical": vm.view.billingAndShippingAddressAreSame,
            "billingLine1": vm.view.billingAddress.buildingNo,
            "billingStreet": vm.view.billingAddress.streetName,
            "billingState": vm.view.billingAddress.stateId,
            "billingZip": vm.view.billingAddress.zipCode,
            "billingCountry": vm.view.billingAddress.country,
            "acceptance": vm.view.userCertified,
            "CSRFToken": ACC.config.CSRFToken,
        }
        returnvalue[key]= address;
        return returnvalue;
    }
    vm.serializeFormDataForImProduct = function() {
        return {
            "selectedShippingAddressPK": vm.productCompareDropDownLabel.id,
            "comments": vm.userComments,
            "CSRFToken": ACC.config.CSRFToken
        }
    }

    vm.setQty = function(qty) {
        vm.view.orderQty = qty;
    }

    vm.setMedataData = function(salesModel, configurationID, configurationName, CSRFToken) {
        vm.view.salesModel = salesModel;
        vm.view.configurationID = configurationID;
        vm.view.response.configurationId = configurationName;
        vm.view.CSRFToken = CSRFToken;
    }

    vm.fetchShippingAddress = function() {
        httpService.get(catEndpointService.dealerShippingAddress).then(function(response) {
            if (response.status == 200) {
                vm.view.shippingAddress = response.data.allShippingAddressList;
                vm.productCompareDropDownLabel = vm.view.shippingAddress.length > 0 ? vm.view.shippingAddress[0] : null;
            }
        }, function(error) {

        });
    }

    vm.setDefaultBillingAddress = function(isUtvProduct, buildingNo, streetName, stateId, zipCode) {
        var tempCount = 0;
        vm.cuvTotalPrice = 0;
        var entryObj = {
            totalQuantity: 0,
            quantity: 0,
            minQuantity: 0,
            maxQuantity: 0,
            filledBoxes: "",
            repeatError: false,
            hybrisValidateError: false,
            madatoryError: false
        }
        vm.view.billingAddress.buildingNo = buildingNo;
        vm.view.billingAddress.streetName = streetName;
        vm.view.billingAddress.stateId = stateId;
        vm.view.billingAddress.zipCode = zipCode;
        vm.view.isUtvProduct = isUtvProduct;
        if(!vm.view.isUtvProduct) {
            vm.disableSubmit = true;
        }
        httpService.get(catEndpointService.reviewReOrderData).then(function(response) {
            if (response.status == 200) {
                vm.view.reviewOrderEntry = response.data;
                angular.forEach(vm.view.reviewOrderEntry.salesModelEntries, function(data, key){
                    angular.forEach(data[0], function(value, index){
                        tempCount += value.quantity;
                        vm.cuvTruckImage = (value.product.images && value.product.images[0]) ? value.product.images[0].url : "";
                        vm.cuvTotalPrice += value.totalPrice.value;
                      
                        entryObj = {
                            totalQuantity: tempCount,
                            quantity: value.quantity,
                            minQuantity: 0 + entryObj.totalQuantity,
                            maxQuantity: value.quantity + entryObj.totalQuantity,
                            filledBoxes: "",
                            repeatError: false,
                            hybrisValidateError: false,
                            madatoryError: false
                        }
                        
                        vm.quantityBasedArray.push(entryObj);
                    });
                });
                vm.setQuantity(tempCount);
                vm.cuvTotalPrice = vm.cuvTotalPrice.toFixed(2);
                vm.view.shippingAddress = response.data.shippingAddressList;
                vm.view.laneType = response.data.laneTypeData;
                vm.productCompareDropDownLabel = vm.view.shippingAddress[0];
            }
        }, function(error) {

        });

    }
    
    vm.editOrder = function() {
        $window.location.href = "/cart/reOrderCart";
    }

    vm.totalLastPrice = function(reviewOrderObj) {
        var totalPrice = 0;
        for (var i=0; i < reviewOrderObj.length; i++) {
            totalPrice += reviewOrderObj[i].totalPrice.value;
        }
        return reviewOrderObj[0].totalPrice.formattedValue.slice(0,1) + totalPrice.toFixed(2);
    }

    vm.billingAndShippingInSync = function() {
        if (!!vm.view.billingAndShippingAddressAreSame) {
            vm.view.bkpBillingAddress.buildingNo = vm.view.billingAddress.buildingNo;
            vm.view.bkpBillingAddress.streetName = vm.view.billingAddress.streetName;
            vm.view.bkpBillingAddress.stateId = vm.view.billingAddress.stateId;
            vm.view.bkpBillingAddress.zipCode = vm.view.billingAddress.zipCode;
            vm.view.billingAddress.buildingNo = vm.productCompareDropDownLabel.line3;
            vm.view.billingAddress.streetName = vm.productCompareDropDownLabel.line1;
            vm.view.billingAddress.stateId = vm.productCompareDropDownLabel.region.name;
            vm.view.billingAddress.zipCode = vm.productCompareDropDownLabel.postalCode;
        } else {
            vm.view.billingAddress.buildingNo = vm.view.bkpBillingAddress.buildingNo;
            vm.view.billingAddress.streetName = vm.view.bkpBillingAddress.streetName;
            vm.view.billingAddress.stateId = vm.view.bkpBillingAddress.stateId;
            vm.view.billingAddress.zipCode = vm.view.bkpBillingAddress.zipCode;

        }
    }

    vm.shippingBuildingNoChange = function() {
        if (!!vm.view.billingAndShippingAddressAreSame) {
            vm.view.billingAddress.buildingNo = vm.productCompareDropDownLabel.line3;
        }
    }

    vm.shippingStreetChange = function() {
        if (!!vm.view.billingAndShippingAddressAreSame) {
            vm.view.billingAddress.streetName = vm.productCompareDropDownLabel.line1;
        }
    }

    vm.shippingStateChange = function() {
        if (!!vm.view.billingAndShippingAddressAreSame) {
            vm.view.billingAddress.stateId = vm.productCompareDropDownLabel.region.name;
        }
    }

    vm.shippingZipcodeChange = function() {
        if (!!vm.view.billingAndShippingAddressAreSame) {
            vm.view.billingAddress.zipCode = vm.productCompareDropDownLabel.postalCode;
        }
    }

    $scope.editOrder = function() {
        if(vm.somethingWrongPopUp){
            vm.somethingWrongPopUp.dismiss("cancel");
        } else if( vm.snopOrderLimitPopupInstance) {
            vm.snopOrderLimitPopupInstance.dismiss("cancel");
        }
        $window.location.href = '/cart/reOrderCart?isFromReviewOrder=true';
    }

    vm.validateLaneToReviewOrder = function(isUtvProduct){
        if(isUtvProduct){
            vm.submitOrder(isUtvProduct);
            return false;
        }
        httpService.get(catEndpointService.reOrderLaneValidation).then(function(response) {
            if(response.data && (Object.keys(response.data)).length){
                vm.somethingWrongPopUp = $uibModal.open({
                  backdrop: 'static',
                  ariaLabelledBy: 'modal-title',
                  ariaDescribedBy: 'modal-body',
                  templateUrl: '/_ui/responsive/cat/templates/somethingWrong.html',
                  scope: $scope,
                  size: 'clear-cart-popup'
                });
            }else {
                vm.submitOrder(isUtvProduct);
            }
        }, function(error) {   
        });
    };

    vm.checkSnopOrderableLimit = function(reviewOrderUrl,serialisedFormData){
        var showSnopOrderablePopup = false;
        $scope.snopConnectionError = false;
        $scope.snopNullResponseError = false;
        $scope.snopOrderablePopupData = []; 
        httpService.get(catEndpointService.snopOrderableLimitEndPoint).then(function(response) {
            if(response.status == 200 && !!response.data){
                angular.forEach(response.data,function(object,index){
                    if(object['SUCCESS'] == false){
                        $scope.snopOrderablePopupData.push(object);
                        showSnopOrderablePopup = true;
                    }
                });
                $scope.snopConnectionError = response.data[0].isConnectionError;
                $scope.snopNullResponseError = response.data[0].isResponseNull;
                if(showSnopOrderablePopup){
                    vm.snopOrderLimitPopupInstance = $uibModal.open({
                        backdrop: 'static',
                        ariaLabelledBy: 'modal-title',
                        ariaDescribedBy: 'modal-body',
                        templateUrl: '/_ui/responsive/cat/templates/snopOrderableLimitPopup.html',
                        scope: $scope,
                        size: 'snop-orderable-popup'
                      });
                } else {
                    httpService.post(reviewOrderUrl, $.param(serialisedFormData)).then(function(response) {
                        if (response.status == 200) {
                            vm.view.processingRequest = false;
                            var purchaseOrderCheck = JSON.parse(response.data.purchaseOrderCheck);
                            if(purchaseOrderCheck.purchaseOrderPresent) {
                                var purchaseOrderCheck = JSON.parse(response.data.purchaseOrderCheck),quantity=0;
                                vm.view.resultPO = response.data.resultPO;
                                var orderDataList = JSON.parse(response.data.orderDataList);
                                for(var key in  response.data.resultPO ) { 
                                    quantity+= response.data.resultPO[key];
                                }
                                vm.view.response.reorderQty = quantity;
                                vm.removeBeforeUnloadEvent();
                                $("#orderSuccessMessage").show();
                                ACC.gaHelper.hitPageView(ACC.gaHelper.bcpPageViewUrl);
                                ACC.gaHelper.pushingOrderToGA(
                                    orderDataList.entries.map(function(entry,index){
                                        return ACC.gaHelper.formProductsArrayForGA(
                                                        entry.product.categories[0].name,
                                                        entry.quantity,
                                                        entry.product.name);
                                    }),
                                    orderDataList.code,
                                   ACC.gaHelper.bcpEventName,
                                   ACC.gaHelper.bcpOrderEventName
                                );
                            } else {
                                vm.view.errorPos = purchaseOrderCheck.listOfErrorPO;
                                vm.hybrisValidateError = true;
                                vm.disableSubmit = false;
                                $("#orderErrorMessage").show();
                            }
                        }
                    }, function(error) {
                        vm.view.processingRequest = false;
                        $("#orderErrorMessage").show();
                    });
                }
            }
        });
    }

    vm.submitOrder = function(isUtvProduct) {
        var noEmptyField = true;
        vm.triggerValidations();
        //If validations are failed then user certified will be un checked
        if(!vm.view.userCertified){
            return;
        }
        vm.view.formSubmited = true;
        if(isUtvProduct) {
            for (var index = 0; index < vm.range.length; index++) {
                if ((vm.range[index].value == null) || (vm.range[index].value.length == 0)) {
                    noEmptyField = false;
                    vm.view.isFormValid = false;
                    vm.madatoryError = true;
                } else if (vm.range[index].isInvalid == true) {
                    noEmptyField = false;
                    vm.view.isFormValid = false;
                    vm.madatoryError = false;
                }
                if (!vm.view.userCertified) {
                    noEmptyField = false;
                }
            }
        } else {
            !vm.view.userCertified ? 
                noEmptyField = false : null;
        }

        if (noEmptyField) {
            vm.madatoryError = false;
            var bool = true;
            vm.returnShippingAddress();
            if (bool) {
                if(isUtvProduct) {
                    var serialisedFormData = vm.serializeFormData();
                    var reviewOrderUrl = catEndpointService.reviewOrderConfirmation;
                } else {
                    var serialisedFormData = vm.serializeFormDataForImProduct();
                    var reviewOrderUrl = catEndpointService.checkoutPlaceOrderIm;
                }
                vm.view.processingRequest = true;
                if(isUtvProduct) {
                    vm.utvProductApiCall(reviewOrderUrl, serialisedFormData);
                } else {
                    vm.checkSnopOrderableLimit(reviewOrderUrl,serialisedFormData);
                }
            }
        } else {
            vm.hybrisValidateError = false;
            vm.repeatError = false;

        }
    }

    vm.utvProductApiCall = function(reviewOrderUrl, serialisedFormData, param) {
        if(param === undefined) {
            $scope.savePoNumberContinue().then(function(response){
                    vm.cuvProductSaveSubmit(reviewOrderUrl, serialisedFormData, param);
            });
        } else {
            saveOrderAPICall(function(){
                vm.cuvProductSaveSubmit(reviewOrderUrl, serialisedFormData, param);
            });
        }
    }

    vm.cuvProductSaveSubmit = function(reviewOrderUrl, serialisedFormData, param) {
        httpService.post(reviewOrderUrl, $.param(serialisedFormData)).then(function(response) {
            if (response.status == 200) {
                vm.view.processingRequest = false;
                vm.view.response.orderNumber = response.data.orderNumber;
                if(!!response.data && response.data.orderDataList){
                    vm.view.response.splitOrderCount = (JSON.parse(response.data.orderDataList)).splitOrderCount;
                }
                //vm.view.response.configurationId =  response.data.orderProduct;
                var purchaseOrderCheck = JSON.parse(response.data.purchaseOrderCheck);
                if (purchaseOrderCheck.purchaseOrderPresent) {
                    var orderDataList = JSON.parse(response.data.orderDataList),quantity=0;
                    orderDataList.entries.map(
                        function(v,i){
                            quantity+=parseInt(v.quantity);
                        }
                    );
                    vm.view.response.reorderUnits = quantity + (quantity === 1 ? " Unit" : " Units");
                    vm.view.response.reorderQty = quantity;
                    vm.removeBeforeUnloadEvent();
                    $("#orderSuccessMessage").show();
                    ACC.gaHelper.pushingOrderToGA(
                        orderDataList.entries.map(function(entry,index){
                            return ACC.gaHelper.formProductsArrayForGA(
                                entry.product.categories[0].name,
                                entry.quantity,
                                entry.product.name);
                        }),
                        orderDataList.code,
                        ACC.gaHelper.bcpEventName,
                        param == 'truckload'?ACC.gaHelper.bcpEventName:ACC.gaHelper.bcpEventName
                    );
                    if(!angular.equals(vm.POStateObj,{})){
                        ACC.gaHelper.hitPageView(ACC.gaHelper.truckLoadUTVPageViewUrl);
                    }else{
                        ACC.gaHelper.hitPageView(ACC.gaHelper.utvPageViewUrl);
                    }
                    
                } else {
                    vm.view.errorPos = purchaseOrderCheck.listOfErrorPO;
                    if(param == 'truckload') {
                        vm.hlighLightErrorTruckload(purchaseOrderCheck.listOfErrorPO, purchaseOrderCheck.duplicatePONumbers);
                    } else {
                        hlighLightErrors(purchaseOrderCheck.listOfErrorPO, purchaseOrderCheck.duplicatePONumbers);
                        //vm.hybrisValidateError = true;
                    }
                    vm.disableSubmit = false;
                    purchaseOrderCheck.duplicatePONumbers ? "" : $("#orderErrorMessage").show();
                }
                validateTruckLoadProductListErrors();
            }
        }, function(error) {
            vm.view.processingRequest = false;
            $("#orderErrorMessage").show();
        });
    }

    vm.hlighLightErrorTruckload = function(errorPos, duplicateFlag) {
        if (errorPos) {
            angular.forEach(errorPos, function(val, index){
                angular.forEach(vm.view.truckload.Entries, function(entry, key){
                    if(duplicateFlag) {
                        vm.duplicatePoHighlightAfterApi(errorPos, index, entry);
                    } else {
                        vm.intermediatePoHighlight(errorPos, index, entry);
                    }
                });
            });
        }
    }

    vm.duplicatePoHighlightAfterApi = function(errorPos, index, entry) {
        var i = -1;
        while ((i = entry.arr.indexOf(errorPos[index], i+1)) != -1){
            vm.intermediatePoHighlight(errorPos, index, entry, i);
        }
    }

    vm.intermediatePoHighlight = function(errorPos, index, entry, idx) {
        if(idx == undefined){
            var idx = entry.arr.indexOf(errorPos[index]);
            if(idx >= 0) {
                entry.range[idx].isInvalid = true;
                entry.range[idx].hybrisValidateError = true;
                entry.hybrisValidateError = true;
            }
        } else {
            entry.range[idx].isInvalid = true;
            entry.repeatError = true;
            entry.range[idx].repeatError = true;
        }
    }

    /*order details tab */
    vm.setQuantity = function(arg) {
        vm.quantity = parseInt(arg);
        vm.initPage();
    }
    vm.initPage = function() {
        vm.view.isUtvProduct ? vm.disableSubmit = false : vm.disableSubmit = true;

        if (vm.arr.length > vm.quantity) {
            vm.arr = vm.arr.splice(0, vm.quantity);
            vm.range = vm.range.splice(0, vm.quantity);
        } else {
            for (var index = vm.arr.length; index < vm.quantity; index++) {
                vm.range.push({
                    "id": (index + 1),
                    "value":  null, 
                    "hybrisValidation": "",
                    "isInvalid": false, 
                    "hybrisFlag": false 
                });
                vm.arr.push(
                    null
                );
            }
        }

        try {
            if (!!ACC.config.cat.userType && ACC.config.cat.userType == "UTV") {
                vm.view.isUTV = true;
            }
        } catch (e) {

        }
    };


    vm.update = function() {
        if (vm.button == "Update") {
            vm.button = "Save";
            vm.updateShow = "true";
        } else {
            if ((vm.quantity != 0) && (vm.quantity <32)){
                vm.button = "Update";
                vm.updateShow = "false";
                vm.initPage();
            }
        }
    };
    vm.increment = function() {
        if (vm.quantity.length == 0) {       
            vm.quantity = 1;     
        }    
        else {       
            vm.quantity = parseInt(vm.quantity);      
            vm.quantity += 1;     
        }
        vm.validate()
    }
    vm.decrement = function() {
        if (vm.quantity.length == 0) {       
            vm.quantity = 0;     
        }    
        else {       
            vm.quantity = parseInt(vm.quantity);        
            if (vm.quantity > 1) {
                vm.quantity -= 1;
            }
        }
        vm.validate()
    }

    vm.beforeUnloadFunction = function(event) {
        event.returnValue = "Are you sure , you wanted to navigate out of Page?";
    }

    vm.addBeforeUnloadEvent = function() {
        window.addEventListener("beforeunload", vm.beforeUnloadFunction);
    }

    vm.removeBeforeUnloadEvent = function() {
        window.removeEventListener("beforeunload", vm.beforeUnloadFunction);
    }
    vm.poCountStatusImg = function(cuvEntry) {
        if(cuvEntry.filledBoxes === cuvEntry.quantity) {
            return 'po-count-img-full';
        } else if(cuvEntry.hybrisValidateError || cuvEntry.madatoryError || cuvEntry.repeatError ) {
            return 'po-count-img-empty';
        } else if(!cuvEntry.hybrisValidateError || !cuvEntry.madatoryError || !cuvEntry.repeatError ){
            return 'po-count-img-not-full';
        }
    }
    vm.serializeRequestBody = function(poEntryData) {
        var requestBody = [];
        var index = 0;
        angular.forEach(poEntryData, function(value, idx){
        	var truckloadId="";
        	if(!!vm.view.reviewOrderEntry.salesModelEntries[vm.view.response.configurationId+"__"+idx] && !!vm.view.reviewOrderEntry.salesModelEntries[vm.view.response.configurationId+"__"+idx][0])
        	{
            truckloadId=vm.view.reviewOrderEntry.salesModelEntries[vm.view.response.configurationId+"__"+idx][0][0].truckloadId;
        	
        	var obj = {
                'salesModelId' : vm.view.response.configurationId+"__"+truckloadId,
                'purchaseOrderNumberSet' : vm.arr.slice(value.minQuantity, value.maxQuantity),
                'purchaseOrderNumberConfigIdMap': {}
            };
            obj['purchaseOrderNumberConfigIdMap'][vm.view.reviewOrderEntry.salesModelEntries[vm.view.response.configurationId+"__"+idx][0][0].product.code + '__' + vm.view.reviewOrderEntry.salesModelEntries[vm.view.response.configurationId+"__"+idx][0][0].truckloadId] = vm.arr.slice(value.minQuantity, value.maxQuantity);
            requestBody[index] = obj;
            index++;
        	}
        });
        return requestBody;
    }
    $scope.closeConfirmationPopup = function(){
        modalPopUpFactory.close(vm.savePopup);
    }
    $scope.savePoNumberContinue = function() {
        var deferred = $q.defer();
        vm.view.errorPos = [];
        httpService.postJsonData(catEndpointService.reorderSavePurchaseOrderNumbersEndPoint,
                vm.serializeRequestBody(vm.quantityBasedArray)).then(function(response) {
            if (response.status == 200) {
                deferred.resolve(response);
            }
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }
    vm.getQuantityBasedArray = function(index) {
        return vm.quantityBasedArray.filter(function(data){
                    if((data.minQuantity <= index) && (index <= data.maxQuantity)) {
                        return data;
                    }
                });
    }
    vm.checkForInvalidKeysHybris = function(tempPoBoxes) {
        return !tempPoBoxes.every(function(v){
            return v.hybrisFlag === false;
        });
    }
    vm.checkForInvalidKeys = function(tempPoBoxes) {
        return !tempPoBoxes.every(function(v){
            return (v.isInvalid === false && v.hybrisFlag === false) || (v.isInvalid === true && v.hybrisFlag === true);
        });
    }
    vm.uniqueArray = function(arrArg, tempPoBoxes) {
        return tempPoBoxes.filter(function(val){
            return !!val.value && !val.isInvalid && !val.hybrisFlag;
        }).length;
    };
    vm.getIndexInRangeGroup = function(index) {
        vm.getQuantityBasedArray(index).map(function(val){
            var tempPoBoxes = vm.range.slice(val.minQuantity, val.maxQuantity);
            val.repeatError  = vm.checkForInvalidKeys(tempPoBoxes);
            val.hybrisValidateError  = vm.checkForInvalidKeysHybris(tempPoBoxes);
            if(!tempPoBoxes.every(function(v){
                return v.value === null
            })) {
                val.filledBoxes = vm.uniqueArray(vm.arr.slice(val.minQuantity, val.maxQuantity), tempPoBoxes);
            }
        });
    }
    vm.getIndexInRangeGroupForHybrisError = function(index, isHybrisErrFlag) {
        vm.getQuantityBasedArray(index).map(function(val){
            var tempPoBoxes = vm.range.slice(val.minQuantity, val.maxQuantity);
            val.hybrisValidateError  = vm.checkForInvalidKeysHybris(tempPoBoxes);
            val.filledBoxes = tempPoBoxes.filter(function(v){
                return !(vm.view.errorPos.indexOf(v) > -1) && !v.isInvalid;
            }).length;
        });
    }

    vm.validate = function(arg, id) {
        if (!vm.view.dataModified) {
            vm.view.dataModified = true;
            vm.addBeforeUnloadEvent();
        }
        if(arguments.length === 0){
            return;
        }
        vm.madatoryError = false;
        vm.getQuantityBasedArray(parseInt(id) + 1)[0].madatoryError = false;
        var bool = false,
            boolHybris = false,
            isEmpty = false;
        vm.range[id].value = arg;
        vm.arr[id] = arg;

        for (var index in vm.range) {
            if (!isNaN(index)) {
                if (!vm.range[index].value) {
                    isEmpty = true;
                }
                if (vm.range[index].value && (vm.view.errorPos.indexOf(vm.range[index].value) != -1 || (vm.arr.indexOf(vm.range[index].value) != vm.arr.lastIndexOf(vm.range[index].value)))) {
                    vm.range[index].isInvalid = true;
                    if (!(vm.view.errorPos.indexOf(vm.range[index].value) != -1)) {
                        bool = true;
                        vm.range[index].hybrisFlag = false;
                        vm.getIndexInRangeGroup(parseInt(index) + 1);
                    } else {
                        vm.range[index].hybrisFlag = true;
                        vm.getIndexInRangeGroupForHybrisError(parseInt(index) + 1, true);
                        boolHybris = true;
                    }
                } else {
                    if(!vm.getQuantityBasedArray(parseInt(id) + 1)[0].madatoryError) {
                        vm.range[index].isInvalid = false;
                    }
                    vm.range[index].hybrisFlag = false;
                    vm.getIndexInRangeGroup(parseInt(index) + 1);
                }
            }
        }

        vm.hybrisValidateError = boolHybris;
        vm.repeatError = bool;
        vm.disableSubmit = (!bool && !isEmpty);
        if (vm.repeatError) {
            vm.hybrisValidateError = false;
        }
        if (vm.hybrisValidateError) {
            vm.repeatError = false;
        }
    };

    function hlighLightErrors(errorPos, duplicateFlag) {
        if (errorPos) {
            for (var i in errorPos) {
                if(duplicateFlag) {
                    vm.duplicatePoHighlightAfterApiUtvFlow(errorPos, i, vm.arr);
                } else {
                    vm.intermediatePoHighlightUtvFlow(errorPos, i, vm.arr);
                }
            }
        } else {
            for (var i in vm.range) {
                if (!vm.range[i].value) {
                    vm.range[i].isInvalid = true;
                }
            }
        }

    }

    vm.duplicatePoHighlightAfterApiUtvFlow = function(errorPos, index, entry) {
        var i = -1;
        while ((i = entry.indexOf(errorPos[index], i+1)) != -1){
            vm.intermediatePoHighlightUtvFlow(errorPos, index, vm.range, i);
        }
    }

    vm.intermediatePoHighlightUtvFlow = function(errorPos, index, entry, idx) {
        var indx = idx;
        if(indx == undefined){
            indx = entry.indexOf(errorPos[index]);
            if(indx >= 0) {
                vm.range[indx].isInvalid = true;
                vm.range[indx].hybrisFlag = true;
                vm.getIndexInRangeGroupForHybrisError(parseInt(indx) + 1, true);
                vm.hybrisValidateError = true;
            }
        } else {
            entry[indx].isInvalid = true;
            vm.repeatError = true;
        }
    }

    vm.removeError = function() {
        if (document.getElementsByClassName('hasError').length == 0) {
            vm.hybrisValidateError = false;
            vm.repeatError = false;
        }
    };

    vm.quantityCheck = function(arg) {
        if(arg == "0"){
            vm.quantityError = true;
            vm.buttonCheck = false;
            vm.quantity = 1;
        }
        if (arg.match(".")) {
            vm.quantityError = true;
            vm.buttonCheck = false;
        } else {
            vm.quantityError = false;
            vm.buttonCheck = true;
        }
        vm.validate();
    };

    vm.editTruckload = function(){
        if(!!$location.search().productCode){
            var productCode = $location.search().productCode;
            $window.location.href = "/truckload/configureTruckload/?productCode=" + productCode;
        }
    }

    vm.showRemoveTruckloadPopup = function(truckloadId,truckIndex){
        if(Object.getOwnPropertyNames(vm.view.truckload.Entries).length == 1){
            $scope.reviewTruckloadRemovePopupText = 'Your current configuration will be lost. Would you like to continue and configure your truckload again?';
        } else {
            $scope.reviewTruckloadRemovePopupText = 'Are you sure you want to remove this truckload?';
        }
        $scope.currentTruckloadId =  truckloadId;
        $scope.currentTruckIndex = truckIndex;
        vm.removeTruckloadModalInstance = $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: '/_ui/responsive/cat/templates/reviewTruckloadRemoveItemPopup.html',
            scope: $scope,
            size: 'review-truckload-remove-item',
        });
    }

    $scope.closeRemoveTruckloadPopup = function() {
        vm.removeTruckloadModalInstance.close();
    };

    $scope.deleteTruckload = function(truckloadId,truckloadIndex){
        var deleteTruckURL = catEndpointService.truckloadDeleteURL+'?truckloadId='+truckloadId;
        httpService.get(deleteTruckURL).then(function(response) {
            delete vm.POStateObj[truckloadId];
            reviewOrderValidation.unSubscribe(reviewOrderValidation.reviewOrderPageSubscriptionId,truckloadIndex);
            vm.view.truckload.totalCartPrice = vm.view.truckload.totalCartPrice - vm.view.truckload.Entries[truckloadIndex].truckload_price;
            if(vm.view.truckload.Entries[truckloadIndex].repeatError || vm.view.truckload.Entries[truckloadIndex].hybrisValidateError) {
                angular.forEach(vm.view.truckload.Entries[truckloadIndex].arr, function(val, key){
                    if(!!val) {
                        vm.poNoCheckBtwTruckEntries(truckloadIndex, key, val);
                    }    
                })
            }
            delete vm.view.truckload.Entries[truckloadIndex];
            vm.view.truckload.totalCartQuantity = response.data.totalCartQuantity;                
            vm.view.truckload.totalTruckloadCount = Object.getOwnPropertyNames(vm.view.truckload.Entries).length;
            vm.view.truckload.isEmptyPage = (vm.view.truckload.totalCartQuantity == 0) ? true : false;
            if(vm.view.truckload.isEmptyPage){
                vm.removeBeforeUnloadEvent();
            }
            ACC.minicart.refreshMinicartCountNum();
            vm.removeTruckloadModalInstance.close();
        });
    }


    vm.returnShippingAddress = function() {
        var shippingAddressLabel = '';
        if (vm.productCompareDropDownLabel.line3) {
            shippingAddressLabel = shippingAddressLabel + vm.productCompareDropDownLabel.line3;
        }
        if (vm.productCompareDropDownLabel.line1) {
            shippingAddressLabel = shippingAddressLabel + ',' + vm.productCompareDropDownLabel.line1;
        }
        if (vm.productCompareDropDownLabel.town) {
            shippingAddressLabel = shippingAddressLabel + ',' + vm.productCompareDropDownLabel.town;
        }
        if (vm.productCompareDropDownLabel.region && vm.productCompareDropDownLabel.region.name) {
            shippingAddressLabel = shippingAddressLabel + ',' + vm.productCompareDropDownLabel.region.name;
        }
        if (vm.productCompareDropDownLabel.country && vm.productCompareDropDownLabel.country.isocode) {
            shippingAddressLabel = shippingAddressLabel + ',' + vm.productCompareDropDownLabel.country.isocode;
        }
        if (vm.productCompareDropDownLabel.postalCode) {
            shippingAddressLabel = shippingAddressLabel + ',' + vm.productCompareDropDownLabel.postalCode;
        }
        vm.shippingAddressString = shippingAddressLabel;
    }

    vm.shippingAddressString = "";
    $scope.updateDropDownValue = function(value) {
        vm.productCompareDropDownLabel = value;
    };
    
    $scope.updateTruckloadDropDownValue = function(addrssObj, parentIndex) {
        vm.view.truckload.shippingAddressPk[parentIndex] = addrssObj.id;
        vm.addBeforeUnloadEvent();
    };

    //Truckload Review order functionality
    vm.validateTruckLoadPo = function(inputObj, id, parentIndex) {
        var arg = inputObj.value;
        if (!vm.view.dataModified) {
            vm.view.dataModified = true;
            vm.addBeforeUnloadEvent();
        }
        if(arguments.length===0){
            return;
        }
        vm.madatoryError = false;
        var bool = false,
            boolHybris = false,
            isEmpty = false;
        if(!!vm.view.truckload.Entries[parentIndex].range[id].oldValue) {
            vm.poNoCheckBtwTruckEntries(parentIndex, id, vm.view.truckload.Entries[parentIndex].range[id].oldValue);
        }
        vm.view.truckload.Entries[parentIndex].range[id].value = arg;
        vm.view.truckload.Entries[parentIndex].range[id].oldValue = arg;
        vm.view.truckload.Entries[parentIndex].arr[id] = arg;

        for (var index in vm.view.truckload.Entries[parentIndex].range) {
            if (!isNaN(index)) {
                if (!vm.view.truckload.Entries[parentIndex].range[index].value) {
                    isEmpty = true;
                }
                if (vm.validationCheck(parentIndex, index)) {
                    vm.view.truckload.Entries[parentIndex].range[index].isInvalid = true;
                    if (!(vm.view.errorPos.indexOf(vm.view.truckload.Entries[parentIndex].range[index].value) != -1)) {
                        bool = true;
                        vm.view.truckload.Entries[parentIndex].range[index].hybrisValidateError=false;
                        vm.view.truckload.Entries[parentIndex].range[index].repeatError=true;
                    } else {
                        boolHybris = true;
                        vm.view.truckload.Entries[parentIndex].range[index].hybrisValidateError=true;
                        vm.view.truckload.Entries[parentIndex].range[index].repeatError=false;
                    }
                } else {
                    vm.view.truckload.Entries[parentIndex].range[index].isInvalid = false;
                    vm.view.truckload.Entries[parentIndex].range[index].hybrisValidateError=false;
                    vm.view.truckload.Entries[parentIndex].range[index].repeatError=false;
                }
            }
        }

        vm.view.truckload.Entries[parentIndex].hybrisValidateError = boolHybris;
        vm.view.truckload.Entries[parentIndex].repeatError = bool;
        vm.disableSubmit = (!bool && !isEmpty);
        if (vm.view.truckload.Entries[parentIndex].repeatError) {
            vm.view.truckload.Entries[parentIndex].hybrisValidateError = false;
        }
        if (vm.view.truckload.Entries[parentIndex].hybrisValidateError) {
            vm.view.truckload.Entries[parentIndex].repeatError = false;
            
        }
        validateTruckLoadProductListErrors();
    };
    vm.validationCheck = function (parentIndex, index) {
        if(vm.view.truckload.Entries[parentIndex].range[index].value &&
            (vm.view.errorPos.indexOf(vm.view.truckload.Entries[parentIndex].range[index].value) != -1 ||
            vm.poNoCheckBtwTruckEntries(parentIndex, index) ||
            (vm.view.truckload.Entries[parentIndex].arr.indexOf(vm.view.truckload.Entries[parentIndex].range[index].value) != vm.view.truckload.Entries[parentIndex].arr.lastIndexOf(vm.view.truckload.Entries[parentIndex].range[index].value)))) {
            return true;
        } else {
            return false;
        }
    }

    vm.poNoCheckBtwTruckEntries = function(parentIndex, index, inputText) {
        var enteredText = "";
        var similarPoCount = 0;
        if(!!inputText) {
            enteredText = inputText;
            angular.forEach(vm.view.truckload.Entries, function(val, key){
                var temp = val.arr.filter(function(data){
                    return data == inputText;
                })
                similarPoCount += temp.length;
            });
        
        } else {
            enteredText = vm.view.truckload.Entries[parentIndex].range[index].value;
        }
        var bool = false;
        angular.forEach(vm.view.truckload.Entries, function(val, key){
            if(val.arr.indexOf(enteredText) > -1 && parentIndex != key ) {
                bool = !(!!inputText && similarPoCount < 3);
                var entryIndex = val.arr.indexOf(enteredText);
                vm.view.truckload.Entries[parseInt(key)].range[entryIndex].isInvalid = bool;
                vm.view.truckload.Entries[parentIndex].range[index].isInvalid = bool;
                vm.view.truckload.Entries[parentIndex].repeatError = bool;
                vm.view.truckload.Entries[key].repeatError = bool;
            }
        });
        return bool;
    }
    vm.getShipAfterDateFormat = function(dateObj){
        var retainedDate = $filter('date')(dateObj, 'EEE MMM dd HH:mm:ss Z yyyy');
        var dateReturnObj = {};
        angular.forEach(vm.view.truckload.shipToAfterDates,function(dateLabel,dateCode){
            var currentDate = $filter('date')(dateCode, 'EEE MMM dd HH:mm:ss Z yyyy');
            if(retainedDate == currentDate){
                dateReturnObj.dateCode = dateCode;
                dateReturnObj.dateLabel = dateLabel;
            }
        });
        return dateReturnObj;
    }

     vm.backToHome = function(){
        $window.location.href = constantService.homeURL;
    }

    vm.submitOrderTruckload = function() {
        vm.triggerValidations();
        if(vm.view.userCertified===false){
            return;
        }
        vm.returnShippingAddress();
        var serialisedFormData = vm.serializeFormData('truckload');
        var reviewOrderUrl = catEndpointService.reviewOrderConfirmation;
        vm.view.processingRequest = true;
        vm.utvProductApiCall(reviewOrderUrl, serialisedFormData, 'truckload');
    };
    var serializeTruckLoadSaveRequest = function(){
       var result=[];
            for(var truckload in vm.productListError){
                if(vm.productListError.hasOwnProperty(truckload)){
                    var resObj={};
                    resObj.mixMatchTruckloadId=truckload;
                    resObj.purchaseOrderNumberSet=[];
                    resObj.truckloadPurchaseOrderNumberConfigIdMap={};
                    resObj.truckloadPurchaseOrderNumberConfigIdMap[truckload]={};
                for(var productName in vm.productListError[truckload]){
                    if(vm.productListError[truckload].hasOwnProperty(productName)){
                        var productCode = productCodeToSalesModelIdMap[productName];
                        resObj.truckloadPurchaseOrderNumberConfigIdMap[truckload][productCode+"__"+truckload]=[];
                        angular.copy(vm.productListError[truckload][productName].POArray,resObj.truckloadPurchaseOrderNumberConfigIdMap[truckload][productCode+"__"+truckload]);
                        resObj.purchaseOrderNumberSet=resObj.purchaseOrderNumberSet.concat(vm.productListError[truckload][productName].POArray);
                    }
                }
                    result.push(resObj);
                }
                
            }
                return result;
        };
       
        var saveOrderAPICall = function(cbk){
            vm.view.errorPos = [];
            httpService.postJsonData(catEndpointService.truckloadSaveURL,
                        serializeTruckLoadSaveRequest()).then(function(response) {
                            if (response.status == 200) {
                                window.onbeforeunload = null;
                                if(response.data != null){
                                    cbk?cbk():'';
                                }
                            }
                        }, function(error) {   
                        });
        }

    var  assignCUVproductImagesAndCUVproductCodeMapping = function(productsList){
        productsList.forEach(function(CUVproduct,index){
            vm.view.CUVproductImageList[CUVproduct.name]=CUVproduct.images[0].url;
            productCodeToSalesModelIdMap[CUVproduct.code] = CUVproduct.name;
            productCodeToSalesModelIdMap[CUVproduct.name] = CUVproduct.code;

        });
    }
    vm.loadTruckloadReviewOrderData = function() {
        httpService.get(catEndpointService.truckloadDataURL).then(function(response) {
            if((response.data && response.data.truckloadEntries) || response.data.status === 200) {
                vm.view.truckload.Entries = response.data.truckloadEntries;
                vm.view.truckload.shipToAfterDates = response.data.shipToAfterDates;
                vm.view.truckload.cuvProductsList =  response.data.cuvProductsList;
                vm.view.truckload.totalCartQuantity = response.data.totalCartQuantity; 
                vm.view.shippingAddressConcatenated = response.data.concatenatedShippingAddressList;
                vm.view.shippingAddress=response.data.shippingAddressList;
                vm.view.dealerName = response.data.dealerName;
                vm.productCompareDropDownLabel=vm.view.shippingAddress.length > 0 ? vm.view.shippingAddress[0] : null;
                vm.view.truckload.repeatError = false;
                vm.view.truckload.hybrisValidateError = false;
                vm.view.truckload.madatoryError = false;
                vm.view.truckload.shippingAddressPk = [];               
                vm.view.truckload.totalCartPrice = response.data.totalCartPrice.toFixed(2);
                vm.view.truckload.totalTruckloadCount = Object.getOwnPropertyNames(vm.view.truckload.Entries).length;
                assignCUVproductImagesAndCUVproductCodeMapping(vm.view.truckload.cuvProductsList);
                angular.forEach(vm.view.truckload.Entries, function(entry, entryIndex){
                    vm.view.truckload.Entries[entryIndex].dateRange =  vm.getShipAfterDateFormat(entry.shipAfterDate);
                    vm.view.truckload.Entries[entryIndex].totalCount = 0;
                    vm.view.truckload.shippingAddressPk[entryIndex] = vm.view.shippingAddress[0].id;
                    angular.forEach(entry.productList, function(obj){
                        vm.view.truckload.Entries[entryIndex].totalCount += obj;
                    })
                    vm.view.truckload.Entries[entryIndex].range = [];
                    vm.view.truckload.Entries[entryIndex].arr = [];
                    for (var index = 0; index < vm.view.truckload.Entries[entryIndex].totalCount; index++) {
                        vm.view.truckload.Entries[entryIndex].range.push({ "id": (index + 1), "hybrisValidation": "", "isInvalid": false, "oldValue": "" });
                        vm.view.truckload.Entries[entryIndex].arr.push("");
                    }
                    
                })
                
            } else {
                vm.view.truckload.isEmptyPage = true;
                vm.removeBeforeUnloadEvent();
            }
        });
    }
}]);
