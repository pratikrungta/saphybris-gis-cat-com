catApp.controller('replicateQuoteDetailsController', ['httpService', 'catEndpointService', '$scope','$location', '$window', '$compile', 'constantService', '$timeout', '$uibModal','catService', 'quoteService', function(httpService, catEndpointService, $scope,$location, $window, $compile, constantService, $timeout,$uibModal, catService, quoteService) {
    var vm = this;
	vm.LostProductModalInstance = null;
	vm.view = {
		enableReplicateButton: true,
		hasSomeUnapprovedProducts:false
	};

    $scope.defaultDownloadImage = constantService.defaultDownloadImage;
	
	$scope.$on('showLostProductPopup',function(event,data){
		vm.showLostProductPopup(data);
	});
	vm.showLostProductPopup = function(quoteCode){
    	vm.replicatedQuoteCode = quoteCode;
		var replicateQuoteURL = '/quote/'+quoteCode+'/replicate?removeSessionCart=false';
		//var replicateQuoteURL =  '/_ui/responsive/cat/mockjsons/replicate.json';
    	vm.callReplicateQuoteAPI(replicateQuoteURL);
        
    };
    vm.callReplicateQuoteAPI = function(url)
    {
	    httpService.get(url).then(function(response){
			vm.quoteResponse = response.data;
			$scope.quoteStockPopupResponse = response.data;
			$scope.close();
			// console.log(vm.quoteResponse);
			if (!vm.quoteResponse.status && vm.quoteResponse.cartExists)
			{
				vm.LostProductModalInstance =  $uibModal.open({
		            ariaLabelledBy: 'modal-title',
		            ariaDescribedBy: 'modal-body',
		            templateUrl: '/_ui/responsive/cat/templates/replicateQuoteNonEmptyCartPopup.html',
		            scope:$scope,
		            size: 'quotation-remove-item',
		        });
			}
			else if(!vm.quoteResponse.status && vm.quoteResponse.stockNotExists){

				vm.LostProductModalInstance =  $uibModal.open({
		            ariaLabelledBy: 'modal-title',
		            ariaDescribedBy: 'modal-body',
		            templateUrl: '/_ui/responsive/cat/templates/cannotReplicateQuote.html',
		            scope:$scope,
		            size: 'quotation-remove-item',
		        });
			}
			else if(!vm.quoteResponse.status && !vm.quoteResponse.stockNotExists){
				
								vm.LostProductModalInstance =  $uibModal.open({
									ariaLabelledBy: 'modal-title',
									ariaDescribedBy: 'modal-body',
									templateUrl: '/_ui/responsive/cat/templates/existingQuoteOutofStock.html',
									scope:$scope,
									size: 'quotation-remove-item',
								});
							}
			else
			{
				if (vm.quoteResponse.quoteCode != null)
				{
					console.log("vm.quoteResponse.quoteCode: ", vm.quoteResponse.quoteCode);
					$window.location.href = '/quote/'+vm.quoteResponse.quoteCode+'/edit?firstTimeEdit=true&replicateQuote=true';
				} else
				{
				$window.location.href = '/quote/'+vm.replicatedQuoteCode+'/edit?firstTimeEdit=true&replicateQuote=true';
				}
			}
	    }, function(error){
	    });
    }
    $scope.navigateToEditQuotePage = function(){
		var replicateQuoteURLLocal = '/quote/'+vm.replicatedQuoteCode+'/replicate?removeSessionCart=true';
		vm.callReplicateQuoteAPI(replicateQuoteURLLocal);
	}
	$scope.allowToReplicateQuote = function(){
		var allowReplicateQuoteURL = '/quote/'+vm.replicatedQuoteCode+'/replicate?allowReplicate=true';
		vm.callReplicateQuoteAPI(allowReplicateQuoteURL);
	}
	$scope.close = function(){
		if(vm.LostProductModalInstance){
			vm.LostProductModalInstance.dismiss("cancel");
			vm.LostProductModalInstance = null;
		}
	} 
	vm.setReplicateParam = function(enableReplicateButton, hasSomeUnapprovedProducts){
		if(!!enableReplicateButton && enableReplicateButton=="false"){
			vm.view.enableReplicateButton = false;
		}
		if(!!hasSomeUnapprovedProducts && hasSomeUnapprovedProducts=="true"){
			vm.view.hasSomeUnapprovedProducts = true;
		}
	}

}]);
