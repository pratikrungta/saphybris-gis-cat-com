catApp.filter('initcaps', function() {
    return function(input){
       if(!input && input!==0){
        return;
       }
      if(input.indexOf(' ') !== -1){
        var inputPieces,
            i;
  
        input = input.toLowerCase();
        inputPieces = input.split(' ');
  
        for(i = 0; i < inputPieces.length; i++){
          inputPieces[i] = capitalizeString(inputPieces[i]);
        }
  
        return inputPieces.toString().replace(/,/g, ' ');
      }
      else {
        input = input.toLowerCase();
        return capitalizeString(input);
      }
  
      function capitalizeString(inputString){
        return inputString.substring(0,1).toUpperCase() + inputString.substring(1);
      }
    };
  });
catApp.filter('isNotArray', function() {
    return function (input) {
      return (Array.isArray(input) === false);
    };
  });

  catApp.filter('isArray', function() {
    return function (input) {
      return Array.isArray(input);
    };
  });  

  catApp.filter('noSpace', function() {
    return function (input) {
        if(input==="" || input === null){
            return "-"
        }else if(typeof(input) === 'object'){
            return input.Filename;
        }else{
            return input;
        }
    };
  }); 
catApp.controller('productCompareController', ['httpService', 'catEndpointService', '$scope', '$location', '$window', '$compile', 'constantService', '$timeout', 'catService', function(httpService, catEndpointService, $scope,$location, $window, $compile, constantService, $timeout, catService) {
	$scope.isUTVProduct=false;
    $scope.selectCompetitorView = true;
    $scope.selectCompetitorViewMobile = true;
    $scope.ajaxSuccess = true;
    $scope.dropdown1Open = false;
    $scope.dropdown2Open = false;
    $scope.dropdown3Open = false;
    $scope.scrollableView = false;
    $scope.scrollableViewMobile = false;
    $scope.dropdownLabel = 'Select';
    $scope.defaultDownloadImage = constantService.defaultDownloadImage;
    $scope.productCompareDropDownLabel1 = $scope.dropdownLabel;
    $scope.productCompareDropDownLabel2 = $scope.dropdownLabel;
    $scope.productCompareDropDownLabel3 = $scope.dropdownLabel;
    $scope.competitor1 = "";
    $scope.competitor2 = "";
    $scope.competitor3 = "";
    $scope.uom = 2;
    $scope.activeAccordion = catService.getActiveAccordion();
    $scope.scrollY = $(window).attr("scrollY");
    $scope.visibleSlideArray = [0,1];
    $scope.showExpandArrow = function(event){
        $(event.target).toggleClass("expanded");
    }
    $scope.updateAccordionIndex = function(index){
        catService.setActiveAccordion(index);
        $.each($scope.specsAccordionData,function(index,value){
            value.AccordionOpen = false;
        });
        ACC.productCompare.productComparisonScroll();
    };
    $scope.resetView = function(){
        $scope.selectCompetitorView = true;
        $scope.selectCompetitorViewMobile = true;
        $scope.scrollableView = false;
        $scope.scrollableViewMobile = false;
        catService.setActiveAccordion(0);
        $scope.productCompareDropDownLabel1 = $scope.dropdownLabel;
        $scope.productCompareDropDownLabel2 = $scope.dropdownLabel;
        $scope.productCompareDropDownLabel3 = $scope.dropdownLabel;
	    $scope.competitor1 = "";
        $scope.competitor2 = "";
        $scope.competitor3 = "";
        $scope.machineId1 = "";
        $scope.machineId2 = "";
        $scope.machineId3 = "";
    }
    $scope.toggleUnits = function(event){
      if($(event.target).text() === 'US'){
        $scope.uom = 2;
      } else {
        $scope.uom = 1;  
      }
      if(!$scope.selectCompetitorView || !$scope.selectCompetitorViewMobile){
        var competitorString = '';
        var competitorStringforUTV = '';
        var dropdown1 = ($scope.productCompareDropDownLabel1 === $scope.dropdownLabel)? '':$scope.machineId1;
        var dropdown2 =($scope.productCompareDropDownLabel2 === $scope.dropdownLabel)? '': $scope.machineId2;
        var dropdown3 = ($scope.productCompareDropDownLabel3 === $scope.dropdownLabel)? '': $scope.machineId3;
        if(dropdown1 !== ""){
            competitorString = "," + $scope.machineId1;
            competitorStringforUTV = "," + $scope.productCompareDropDownLabel1;
        }
        if(dropdown2 !== ""){
            competitorString = competitorString + "," +$scope.machineId2;
            competitorStringforUTV = competitorStringforUTV + "," + $scope.productCompareDropDownLabel2;
        }
        if(dropdown3 !== ""){
            competitorString = competitorString + "," +$scope.machineId3;
            competitorStringforUTV = competitorStringforUTV +"," + $scope.productCompareDropDownLabel3;
        }
        $scope.selectedCompetitors = $scope.machineId + competitorString;
        $scope.selectedCompetitorsForUTV = $scope.salesModel + competitorStringforUTV;
          $scope.ajaxSuccess = false;
	//$scope.compareURL = "/_ui/responsive/cat/mockjsons/productCompare.json";
          if(!!$location.search().categoryName && $location.search().categoryName==='UTILITY VEHICLE'){
        	  $scope.isUTVProduct=true;
        	  $scope.compareURL = '/speccheck/productCompareUsingHybris?salesModels='+$scope.selectedCompetitorsForUTV+'&uom='+$scope.uom;
          } else {
        	  $scope.compareURL = '/speccheck/callSpeccheckApi?handler=compare&mids='+$scope.selectedCompetitors+'&uom='+$scope.uom;
          }
        httpService.get($scope.compareURL).then(function(response){
            if(response.status == 200){
                $scope.machineArray = [];
                $scope.productData = response.data;
		$scope.ajaxSuccess = true;
                $scope.arrengeCompetitorAccordionData();
                if($scope.productData.length){
                    $scope.selectCompetitorView = false;
                    $scope.selectCompetitorViewMobile = false;
                } else {
                    $scope.selectCompetitorView = true;
                    $scope.selectCompetitorViewMobile = true;
                }
            }
         }, function(error){

        });
      }
    }
    $scope.setMetaData = function(machineId,salesModel,imageURL,mediaPrefix){
        if(decodeURI(location.search).indexOf("UTILITY VEHICLE") > -1) {
            $scope.machineId = salesModel;
        } else {
            $scope.machineId = machineId;
        }
    	$scope.salesModel = salesModel;
        $scope.imageURL = imageURL;
        $scope.mediaPrefix = mediaPrefix;
        $scope.competitorURL = '/speccheck/competitorProducts?salesModel='+$scope.salesModel;
        $scope.ajaxSuccess = false;
        httpService.get($scope.competitorURL).then(function(response){
            if(response.status == 200){
            	$scope.productCategories = response.data;
                $scope.ajaxSuccess = true;
            }
         }, function(error){
    
         });
    };

    $scope.updateDropDownValue = function(event,category){
        if($(event.target).parent().hasClass("dropdown1") && $scope.productCompareDropDownLabel2 !== category.Model && $scope.productCompareDropDownLabel3 !== category.Model){
            if($scope.machineId === category.Machine_ID) {
                return true;
            }
            $scope.productCompareDropDownLabel1 = category.Model;
            $scope.machineId1=category.Machine_ID;
            $scope.competitor1 = category.Manufacturer;
        }else if($(event.target).parent().hasClass("dropdown2") && $scope.productCompareDropDownLabel1 !== category.Model && $scope.productCompareDropDownLabel3 !== category.Model){
            if($scope.machineId === category.Machine_ID) {
                return true;
            }
            $scope.productCompareDropDownLabel2 = category.Model;
            $scope.machineId2=category.Machine_ID;
            $scope.competitor2 = category.Manufacturer;
        }else if($scope.productCompareDropDownLabel1 !== category.Model && $scope.productCompareDropDownLabel2 !== category.Model){
            if($scope.machineId === category.Machine_ID) {
                return true;
            }
            $scope.productCompareDropDownLabel3 = category.Model;
            $scope.machineId3=category.Machine_ID;
            $scope.competitor3 = category.Manufacturer;
        }
    }
    $scope.dropdownToggled = function(dropdown){
        if(dropdown === "first"){
            $scope.dropdown1Open = !$scope.dropdown1Open;
        }else if(dropdown === "second"){
            $scope.dropdown2Open = !$scope.dropdown2Open;
        }else{
            $scope.dropdown3Open = !$scope.dropdown3Open;
        }
        
    }
    $scope.toggleCompareView = function(event){
        if($scope.selectCompetitorView || $scope.selectCompetitorViewMobile){
            var competitorString = '';
            var competitorStringforUTV = '';
            var dropdown1 = ($scope.productCompareDropDownLabel1 === $scope.dropdownLabel)? '':$scope.machineId1;
            var dropdown2 =($scope.productCompareDropDownLabel2 === $scope.dropdownLabel)? '': $scope.machineId2;
            var dropdown3 = ($scope.productCompareDropDownLabel3 === $scope.dropdownLabel)? '': $scope.machineId3;
            if(dropdown1 !== ""){
            	competitorString = "," + $scope.machineId1;
            	competitorStringforUTV = "," + $scope.productCompareDropDownLabel1;
            }
            if(dropdown2 !== ""){
            	competitorString = competitorString + "," +$scope.machineId2;
            	competitorStringforUTV = competitorStringforUTV + "," + $scope.productCompareDropDownLabel2;
            }
            if(dropdown3 !== ""){
            	competitorString = competitorString + "," +$scope.machineId3;
            	competitorStringforUTV = competitorStringforUTV +"," + $scope.productCompareDropDownLabel3;
            }
            $scope.selectedCompetitors = $scope.machineId + competitorString;
            $scope.selectedCompetitorsForUTV = $scope.salesModel + competitorStringforUTV;
            $scope.ajaxSuccess = false;
            if(!!$location.search().categoryName && $location.search().categoryName==='UTILITY VEHICLE'){
          	  $scope.compareURL = '/speccheck/productCompareUsingHybris?salesModels='+$scope.selectedCompetitorsForUTV+'&uom='+$scope.uom;
          	  $scope.isUTVProduct=true;
            } else {
            	$scope.compareURL = '/speccheck/callSpeccheckApi?handler=compare&mids='+$scope.selectedCompetitors+'&uom='+$scope.uom;
            }
            httpService.get($scope.compareURL).then(function(response){
                if(response.status == 200){
                    $scope.machineArray = [];
                    $scope.productData = response.data;
		    $scope.ajaxSuccess = true;
                    $scope.arrengeCompetitorAccordionData();
                    if($scope.productData.length) {
                        $scope.selectCompetitorView = false;
                        $scope.selectCompetitorViewMobile = false;
                    } else {
                        $scope.selectCompetitorView = true;
                        $scope.selectCompetitorViewMobile = true;
                    }
                }
             }, function(error){
    
            });
        } else {
            $scope.selectCompetitorView = true;
            $scope.selectCompetitorViewMobile = true;
        }
    }

    $scope.arrengeCompetitorAccordionData = function(){
        $scope.specsAccordionResponse = $scope.productData.slice(1);
        $scope.machinesData = $scope.specsAccordionResponse.filter(function(data){
            return data.DataType === "M";
        });
        $scope.specsAccordionResponse = $scope.specsAccordionResponse.filter(function(data){
            return data.DataType !== "M";
        });
        var HeaderPresentArray = $scope.specsAccordionResponse.filter(function(data){
            return data.DataType === "H";
        });
        $scope.specsAccordionData = (HeaderPresentArray.length) ? $scope.formatSpecsResponse($scope.specsAccordionResponse) : $scope.populateDefaultResponse($scope.specsAccordionResponse);
        angular.forEach($scope.machinesData[0].Data,function(columns,index){
            angular.forEach(columns,function(data,i){
                var selectedPhoto;
                angular.forEach(data,function(value,j){
                    if(value.Content_Type === ".jpg" || value.Content_Type === "image/jpeg"){
                        selectedPhoto = value;
                        return false;
                    }
                });
                $scope.machineArray.push(selectedPhoto);
            });
        });
    }


    $scope.populateDefaultResponse = function(specsResponse){
        var headerArray = [];
        $scope.activeAccordion = catService.getActiveAccordion();
        headerArray.push({
            "AccordionHeader" : {'Label':'Others'},
            "AccordionContent" : [],
            "AccordionOpen" : true
        });
        angular.forEach(headerArray,function(object,headerIndex){
            angular.forEach(specsResponse,function(value,index){
                    object["AccordionContent"].push(value);
            });
        });
        return headerArray;
    };

    $scope.formatSpecsResponse = function(specsResponse){
        var headerArray = [];
        $scope.activeAccordion = catService.getActiveAccordion();
        angular.forEach(specsResponse,function(object,index){
            if(object.DataType === "H"){
                headerArray.push({
                    "AccordionHeader" : object,
                    "ObjectIndex" : index,
                    "AccordionContent" : [],
                    "AccordionOpen" : false
                });
            }
        });
        
        angular.forEach(headerArray,function(object,headerIndex){
            if(headerIndex !== headerArray.length-1){
                var currentIndex = object.ObjectIndex;
                var nextIndex = headerArray[headerIndex+1].ObjectIndex || currentIndex ;
                angular.forEach(specsResponse,function(value,index){
                    if(value.hasOwnProperty('Data') && index > currentIndex && index < nextIndex){
                         for(var i=0; i < 4; i++){
                            if(value.Data[i] === undefined){ 
                                value.Data[i] = {};
                                value.Data[i]["Column "+i] = "";
                            }
                        }
                        object["AccordionContent"].push(value);
                    }
                });
            }else{
                var currentIndex = object.ObjectIndex;
                angular.forEach(specsResponse,function(value,index){
                    if(value.hasOwnProperty('Data') && index > currentIndex){
                        for(var i=0; i < 4; i++){
                            if(value.Data[i] === undefined){
                                value.Data[i] = {};
                                value.Data[i]["Column "+i] = "";
                            }
                        }
                        object["AccordionContent"].push(value);
                    }
                });
            }
        });
        for(var i=0;i<headerArray.length;i++){
            if($scope.activeAccordion == i){
                headerArray[i].AccordionOpen = true;
            } else {
                headerArray[i].AccordionOpen =  false;
            }
        }
        return headerArray;
    };
    function sticky_relocate() {
        if(!$scope.selectCompetitorView && ($(window).width() > parseInt(screenSmMax))){
            $timeout(function(){
                var window_top = $(window).scrollTop();
                var div_top = $('#sticky-anchor').offset().top;
                $('#sticky-header').width($(".product-compare-accordion").width());
                if (window_top > (div_top - 10)) {
                    $scope.scrollableView = true;
                    $scope.scrollY = $(window).attr("scrollY");
                    $('#sticky-header').addClass('stick');
                    $('#sticky-anchor').height($('#sticky-header').outerHeight());
                } else {
                    $scope.scrollableView = false;
                    $scope.scrollY = $(window).attr("scrollY");
                    $('#sticky-header').removeClass('stick');
                    $('#sticky-anchor').height(0);
                }
                $scope.$apply();
            });
        }
    }
function sticky_relocate_mobile() {
        if(!$scope.selectCompetitorViewMobile && ($(window).width() <= parseInt(screenSmMax))){
            $timeout(function(){
                var window_top = $(window).scrollTop();
                var div_top = $('#sticky-anchor-mobile').offset().top;
                if (window_top > (div_top - 10)) {
                    if(!$scope.scrollableViewMobile)
                    $scope.scrollY = $(window).attr("scrollY");
                    $scope.scrollableViewMobile = true;
                    $('#sticky-header-mobile').width($(".product-compare-mobile-view .product-compare-accordion").width());
                    $('#sticky-header-mobile').addClass('stick');
                    $('#sticky-anchor-mobile').height($('#sticky-header-mobile').outerHeight());
                } else {
                    if($scope.scrollableViewMobile)
                    $scope.scrollY = $(window).attr("scrollY");
                    $scope.scrollableViewMobile = false;
                    $('#sticky-header-mobile').css('width', '100%');
                    $('#sticky-header-mobile').removeClass('stick');
                    $('#sticky-anchor-mobile').height(0);
                }
                $scope.$apply();
            });
        }
    }

    $(window).resize(function(){
        $(window).scroll();
    });

    $(window).scroll(function(){
        scrollOnResize();
    });

    $(".slider").on("afterChange",function(event,slick,currentSlide,nextSlide){
        $timeout(function(){
            $scope.visibleSlideArray = [currentSlide,currentSlide+1];
            $scope.$apply();
        })
    });

    $(function(){
        scrollOnResize();
    });

    function scrollOnResize(){
        if($(window).width() <= parseInt(screenSmMax)){
            sticky_relocate_mobile();
         }else{
            sticky_relocate();
         }
    } 
   
 

}]);
