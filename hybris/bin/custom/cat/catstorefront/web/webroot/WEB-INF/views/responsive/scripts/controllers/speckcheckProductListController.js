catApp.filter('capitalize', function() {
  return function(input) {
    return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
  }
});
catApp.controller('speckcheckProductListController', ['httpService', 'catEndpointService', '$scope', '$location', '$window', '$compile', 'constantService', '$timeout', 'catService','screenResizedService', function(httpService, catEndpointService, $scope, $location, $window, $compile, constantService, $timeout, catService,screenResizedService) {
  var vm = this;
  vm.defaultDownloadImage = constantService.defaultDownloadImage;
  vm.productCompareDropDownLabel = '';
  vm.showProductName = '';
  vm.selectedProductCode = '';
  vm.ajaxSuccess = true;
  vm.showSideBar = false;
  vm.isMobile = screenResizedService.metaInfo.isMobile;
  vm.pagination = {
      currentPage : 1,
      elementsForCurrentPage : 9
  };
  vm.noOfPagesInMobileView = 9;
  vm.roleFeatureInfoCompare = function(isUTVUser,isIM,isSP){
    if (isUTVUser && isIM){
      ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.cuvUserEventName,ACC.gaHelper.comparissionEvent);
    }
    else if(!isUTVUser&& isIM ){
      ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.bcpUserEventName,ACC.gaHelper.comparissionEvent);
    }
    else if(isSP){
      ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.spUserEventName,ACC.gaHelper.comparissionEvent);
    }
  }
  vm.pageChanged = function(){
          vm.upperLimit = vm.pagination.currentPage*vm.pagination.elementsForCurrentPage,
          vm.lowerLimit = (vm.pagination.currentPage-1)*vm.pagination.elementsForCurrentPage;
      
    }
  vm.updateDropDownValue = function(value){
      vm.productCompareDropDownLabel = value;
  };
  
  vm.fetchProductCategories = function(){
	  vm.ajaxSuccess = false;
	  httpService.get(catEndpointService.productFamilyEnpointComparePLP).then(function(response){
      var urlValue = "";
      if(!!$location.search().categoryName){
        urlValue = $location.search().categoryName;
      }
    if(response.status == 200){
      vm.productCategories = response.data;
      vm.ajaxSuccess = true;
      if(!!urlValue){
        vm.productCategories.forEach(function(el,index){
          if(el.name == urlValue){
            vm.productCompareDropDownLabel = vm.productCategories[index];
          }
        });
      }else{
        vm.productCompareDropDownLabel = vm.productCategories[0];
      }
      
      vm.updateProductsList();
    }
   }, function(error){

      });
  };

  vm.updateProductsList = function(){
      //vm.updateProductsListURL = '/search/selectCategory?categoryCode='+vm.productCompareDropDownLabel.name;
      vm.ajaxSuccess = false; 
      httpService.get(catEndpointService.productCompareSearchResults+'?categoryCode='+vm.productCompareDropDownLabel.name).then(function(response){
        if(response.status == 200){
          vm.productList = response.data;
          vm.ajaxSuccess = true;
          vm.pagination.totalItems = vm.productList.length;
          vm.productListToDisplay = vm.productList.slice(0,vm.pagination.elementsForCurrentPage);
          vm.upperLimit = vm.pagination.elementsForCurrentPage;
          vm.lowerLimit = 0;
          vm.productListToDisplayMobile =  vm.productListToDisplay;
          vm.productListToDisplay = vm.productListToDisplay.chunk(3);
          vm.showProductName = vm.productCompareDropDownLabel.name;
          vm.showSideBar = (vm.showSideBar == true) ? false : vm.showSideBar;
          vm.noOfPagesInMobileView = 9;
        }
      }, function(error){

     });
  }

  vm.init = function(){
    vm.fetchProductCategories();
  }

  vm.toggleSidebar = function(e){
    e.preventDefault();
    vm.showSideBar = !vm.showSideBar;
  }

  vm.loadMoreProducts = function(){
    vm.noOfPagesInMobileView = vm.noOfPagesInMobileView + vm.pagination.elementsForCurrentPage;
    var noOfProducts = (vm.noOfPagesInMobileView < vm.productList.length) ? vm.noOfPagesInMobileView : vm.productList.length;
    vm.productListToDisplayMobile = vm.productList.slice(0,noOfProducts);
  }

  vm.init();

}]);