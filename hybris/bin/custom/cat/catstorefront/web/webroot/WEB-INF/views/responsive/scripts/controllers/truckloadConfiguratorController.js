catApp.controller('truckloadConfiguratorController', ['$scope', '$filter', '$timeout', 'catEndpointService', 'httpService', 'constantService', '$window', 'getUrlParamFactory', '$anchorScroll', '$uibModal', function($scope, $filter, $timeout, catEndpointService, httpService, constantService, $window, getUrlParamFactory, $anchorScroll, $uibModal) {
    var vm = this;
    Tour.prototype._onScroll = function(){}
    var tourDesktop = new Tour(constantService.truckloadTourDesktopObject);
    var tourMobile = new Tour(constantService.truckloadTourMobileObject);
    vm.lineItemArray = [];
    vm.isIE = false || !!document.documentMode;
    vm.isMobileSafari = /iP(ad|hone|od).+Version\/[\d\.]+.*Safari/i.test(navigator.userAgent);
    vm.navigateToReviewOrder = false;
    vm.allowOnBeforeUnload = false;
    vm.lineItemObj = {
        "cuvProductsArray" : [],
        "cuvImagesArray":[],
        "addToTruckDisable" : true,
        "shipAfterDateLabel" : "Select a Ship After date",
        "shipAfterDateCode" : null,
        "shipAfterDateError" : false, 
        "isAddedToTruck" : false,
        "addNewLineItem":false,
        "truckloadId": null,
        "maxCUVQtyReached": false,
        "addToTruckSuccessMsg":false,
        "truckButtonType": 'ADD',
        "addToTruckResponse" : {
            prodQtyMap : {},
            success : false,
        },
        "totalUnits":0,
        "productNotAvailableMsg": "",
        "isAvailable": true
    };
    vm.lineItemArray.push(angular.copy(vm.lineItemObj));
    vm.totalTruckloadUnits = 0;
    vm.shipAfterDatesArray = [];
    vm.cuvProductsList = [];
    vm.truckloadConfigMatrix = [];
    vm.defaultDownloadImage = constantService.defaultDownloadImage;
    
    vm.truckloadConfigOptionsHeader = ACC.localization.truckloadConfigOptionsHeader;
    vm.truckloadConfigSelectionHeader = ACC.localization.truckloadConfigSelectionHeader;
    vm.truckloadConfigSetOneSeaterHeading = ACC.localization.truckloadConfigSetOneSeaterHeading;
    vm.truckloadConfigSetOneSeatCount = ACC.localization.truckloadConfigSetOneSeatCount;
    vm.truckloadConfigSetOneOptionsHeading = ACC.localization.truckloadConfigSetOneOptionsHeading;
    vm.truckloadConfigSetOneOption1 = ACC.localization.truckloadConfigSetOneOption1;
    vm.truckloadConfigSetOneOption2 = ACC.localization.truckloadConfigSetOneOption2;
    vm.truckloadConfigSetOneOption3 = ACC.localization.truckloadConfigSetOneOption3;
    vm.truckloadConfigSetTwoSeaterHeading = ACC.localization.truckloadConfigSetTwoSeaterHeading;
    vm.truckloadConfigSetTwoSeatCount = ACC.localization.truckloadConfigSetTwoSeatCount;
    vm.truckloadConfigSetTwoOptionsHeading = ACC.localization.truckloadConfigSetTwoOptionsHeading;
    vm.truckloadConfigSetTwoOption1 = ACC.localization.truckloadConfigSetTwoOption1;
    vm.truckloadConfigSetTwoOption2 = ACC.localization.truckloadConfigSetTwoOption2;
    vm.truckloadConfigSetTwoOption3 = ACC.localization.truckloadConfigSetTwoOption3;

    vm.roleFeatureinfoTruckLoad = function(){
        ACC.gaHelper.roleFeatureInfo(ACC.gaHelper.cuvUserEventName,ACC.gaHelper.cuvTruckLoadOrderEventName);
    }
    vm.loadTruckLoadData = function(userMailId){
        constantService.settruckloadTourUserId(userMailId);
        constantService.settruckloadTourMobileUserId(userMailId);
        httpService.get(catEndpointService.truckloadDataURL).then(function(response) {
            if (response.data.truckloadEntries && Object.getOwnPropertyNames(response.data.truckloadEntries).length > 0){
                vm.populateDefaultLineItemsOnPageRefresh(response.data.truckloadEntries);
            }
            vm.shipAfterDatesArray = response.data.shipToAfterDates;
            vm.cuvProductsList = response.data.cuvProductsList;
            vm.truckloadConfigMatrix = response.data.possibleTruckloadCombinationList;
            vm.updateLineItemWithCUVProducts(vm.cuvProductsList);
            if (response.data.truckloadEntries && Object.getOwnPropertyNames(response.data.truckloadEntries).length > 0){
                vm.populateLineItemWithRetainedData(response.data.truckloadEntries);
            }
            if(response.data.totalCartQuantity){
                vm.totalTruckloadUnits = response.data.totalCartQuantity;
            }
        });
        if(angular.element(window).width() < 900 && 
          (localStorage.getItem("truckloadTourDismissMobile") == null || localStorage.getItem("truckloadTourDismissMobile") == 'false') &&
          (localStorage.getItem("truckloadTourMobileUserId") == null || localStorage.getItem("truckloadTourMobileUserId") != userMailId)){
            tourMobile.init();
            tourMobile.start();
            tourMobile.restart();
        } else if(angular.element(window).width() > 900 && 
          (localStorage.getItem("truckloadTourDismiss") == null || localStorage.getItem("truckloadTourDismiss") == 'false') &&
          (localStorage.getItem("truckloadTourUserId") == null || localStorage.getItem("truckloadTourUserId") != userMailId)) {
            tourDesktop.init();
            tourDesktop.start();
            tourDesktop.restart();
        }
    }
    vm.populateDefaultLineItemsOnPageRefresh = function(truckloadEntries){
        vm.lineItemArray.length = 0;
        angular.forEach(truckloadEntries,function(truckloadMap,truckIndex){
            vm.lineItemArray.push(angular.copy(vm.lineItemObj));
            vm.lineItemArray[truckIndex].addToTruckResponse.success = true;
            !!truckloadMap.truckloadId ? vm.lineItemArray[truckIndex].truckButtonType = 'UPDATE' : '';
        });
    }
    vm.mobileSafariPopup = function(){
        vm.mobileSafariPopupInstance = $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: '/_ui/responsive/cat/templates/mobileSafariPopup.html',
            scope: $scope,
            size: 'mobile-safari-navigate-away',
        });
    }
    $scope.mobileSafariPopupCancel = function(){
        vm.mobileSafariPopupInstance.close();
    }
    $scope.mobileSafariPopupOk = function(){
        var productCode = getUrlParamFactory.getUrlParams().productCode;
        vm.navigateToReviewOrder = true;
        vm.mobileSafariPopupInstance.close();
        $window.location.href = "/truckload/reviewTruckload/?productCode=" + productCode;
    }

    vm.populateLineItemWithRetainedData = function(truckloadEntries){
        angular.forEach(truckloadEntries,function(truckloadMap,truckIndex){
            var totalUnits = 0;
            for(var key in truckloadMap){
                switch(key){
                    case 'truckloadId' :
                        vm.lineItemArray[truckIndex].truckloadId = truckloadMap[key];
                        break;
                    case 'shipAfterDate':
                        var dateObj = vm.populateShipAfterDatesFromRetainedData(new Date(truckloadMap[key]));
                        vm.lineItemArray[truckIndex].shipAfterDateLabel = dateObj.dateLabel;
                        vm.lineItemArray[truckIndex].shipAfterDateCode = $filter('date')(dateObj.dateCode, 'EEE MMM dd HH:mm:ss Z yyyy');
                        vm.getProductByDates(vm.lineItemArray[truckIndex].shipAfterDateCode, truckIndex, false);
                        break;
                    case 'truckloadImageUrl':
                            vm.lineItemArray[truckIndex].truckImgURL = truckloadMap[key];
                        break;
                    default :
                        angular.forEach(vm.lineItemArray[truckIndex].cuvProductsArray,function(cuvProduct,cuvIndex){
                            if(key == cuvProduct.productNames){
                                cuvProduct.quantities = truckloadMap[key];
                                totalUnits = totalUnits + truckloadMap[key];
                                vm.lineItemArray[truckIndex].addToTruckResponse.prodQtyMap[key] = truckloadMap[key];
                            }
                            vm.setCUVProductImageBasedOnUserSelection(truckIndex,cuvIndex);
                        });
                }
            }
            vm.lineItemArray[truckIndex].totalUnits = totalUnits;
            vm.lineItemArray[truckIndex].isAddedToTruck = true;
            vm.lineItemArray[truckIndex].addNewLineItem = true;
            vm.lineItemArray[truckIndex].addToTruckDisable = true;
        });
    }
    vm.populateShipAfterDatesFromRetainedData = function(dateObj){
        var retainedDate = $filter('date')(dateObj, 'EEE MMM dd HH:mm:ss Z yyyy');
        var dateReturnObj = {};
        angular.forEach(vm.shipAfterDatesArray,function(dateLabel,dateCode){
            var currentDate = $filter('date')(dateCode, 'EEE MMM dd HH:mm:ss Z yyyy');
            if(retainedDate == currentDate){
                dateReturnObj.dateCode = dateCode;
                dateReturnObj.dateLabel = dateLabel;
            }
        });
        return dateReturnObj;
    }
    vm.setShipAfterDate = function(dateCode,dateObj,truckIndex){
        vm.lineItemArray[truckIndex].addToTruckDisable = (vm.lineItemArray[truckIndex].shipAfterDateLabel != dateObj && !vm.ifAnyNonZero(vm.lineItemArray[truckIndex].cuvProductsArray));
        vm.lineItemArray[truckIndex].shipAfterDateLabel = dateObj;
        vm.lineItemArray[truckIndex].shipAfterDateCode = $filter('date')(dateCode, 'EEE MMM dd HH:mm:ss Z yyyy');
        vm.lineItemArray[truckIndex].shipAfterDateError = false;
        vm.getProductByDates(vm.lineItemArray[truckIndex].shipAfterDateCode, truckIndex, true);
        vm.allowOnBeforeUnload = true;
    }

    vm.getProductByDates = function(date, truckIndex, flag) {
        var dateFormat = {
            "shipAfterDate" : date,
            "CSRFToken": ACC.config.CSRFToken
        }
        httpService.post(catEndpointService.getProductForDateUrl, $.param(dateFormat)).then(function(response) {
            if(response.data.unavailableProductsList.length) {
                vm.lineItemArray[truckIndex].productNotAvailableMsg = response.data.productInformationMsg;
                angular.forEach(response.data.unavailableProductsList, function(val, key){
                    var missingProduct = vm.lineItemArray[truckIndex].cuvProductsArray.filter(function(data, index){	
                        return data.productNames == val.name
                    })
                    missingProduct[0].isAvailable =  false;
                    if(flag) {
                        vm.lineItemArray[truckIndex].totalUnits -= missingProduct[0].quantities;
                        vm.lineItemArray[truckIndex].totalUnits === 0 ? vm.lineItemArray[truckIndex].addToTruckDisable = true : null;
                            missingProduct[0].seatCapacity === "5" ?
                            vm.removeFiveSeater(truckIndex, missingProduct) :
                            vm.lineItemArray[truckIndex].cuvImagesArray.splice(vm.lineItemArray[truckIndex].cuvImagesArray.indexOf('cuvImage seater2 col-xs-3 col-md-3'),missingProduct[0].quantities);
                        missingProduct[0].quantities = 0;
                        !!vm.lineItemArray[truckIndex].addToTruckResponse.errorMessage ? 
                            delete vm.lineItemArray[truckIndex].addToTruckResponse.errorMessage : 
                            null;
                    }
                });
                vm.lineItemArray[truckIndex].totalUnits < 6 ? vm.lineItemArray[truckIndex].maxCUVQtyReached = false : null;
                angular.forEach(vm.lineItemArray[truckIndex].cuvProductsArray, function(data, key){
                    data.isAvailable ? data.correctCombination = true : null;
                })
            } else {
                vm.lineItemArray[truckIndex].productNotAvailableMsg = response.data.productInformationMsg;
                angular.forEach($scope.tcc.lineItemArray[truckIndex].cuvProductsArray, function(obj, key){
                    obj.isAvailable = true;
                })
            }
        }, function(error) {
            console.log(error);
        });
    }

    vm.removeFiveSeater = function(truckIndex, missingProduct) {
        if(vm.lineItemArray[truckIndex].cuvImagesArray.indexOf('cuvImage seater5 col-xs-3 col-md-3') !== -1) {
            vm.lineItemArray[truckIndex].cuvImagesArray.splice(vm.lineItemArray[truckIndex].cuvImagesArray.indexOf('cuvImage seater5 col-xs-3 col-md-3'),missingProduct[0].quantities)
        } else {
            vm.lineItemArray[truckIndex].cuvImagesArray.splice(vm.lineItemArray[truckIndex].cuvImagesArray.indexOf('cuvImage seater5 col-xs-4 col-md-4'),missingProduct[0].quantities)
        }
    }

    vm.updateLineItemWithCUVProducts = function(cuvList){
        angular.forEach(vm.lineItemArray,function(lineItem,truckIndex){
            angular.forEach(cuvList,function(cuvProduct,cuvIndex){
                lineItem.cuvProductsArray.push({
                    "productNames" : cuvProduct.name,
                    "productCodes" : cuvProduct.code,
                    "quantities" : 0,
                    "seatCapacity" : cuvProduct.seatCapacity,
                    "correctCombination" : true,
                    "isAvailable": true
                });
            });
        });
    }
    vm.enableDisableAddToTruckBtn = function(truckIndex,cuvIndex){
        var qtyVal = vm.lineItemArray[truckIndex].cuvProductsArray[cuvIndex].quantities;
        !vm.lineItemArray[truckIndex].maxCUVQtyReached ? vm.lineItemArray[truckIndex].isAddedToTruck = false : null;
        if(qtyVal == ''){
            vm.lineItemArray[truckIndex].cuvProductsArray[cuvIndex].quantities = 0;
        } else if (qtyVal > Number(vm.cuvProductsList[cuvIndex].shippingFactor)) {
            vm.lineItemArray[truckIndex].cuvProductsArray[cuvIndex].quantities = (vm.lineItemArray[truckIndex].cuvProductsArray[cuvIndex].quantities != vm.cuvProductsList[cuvIndex].shippingFactor) ? 0 : Number(vm.cuvProductsList[cuvIndex].shippingFactor);
        }
        vm.setCUVProductImageBasedOnUserSelection(truckIndex,cuvIndex);
        vm.lineItemArray[truckIndex].addToTruckDisable = !vm.ifAnyNonZero(vm.lineItemArray[truckIndex].cuvProductsArray) || !vm.lineItemArray[truckIndex].cuvProductsArray[cuvIndex].correctCombination || (vm.lineItemArray[truckIndex].isAddedToTruck && vm.lineItemArray[truckIndex].maxCUVQtyReached);
    }
    vm.changeTruckLoadQty = function(operator,truckIndex,cuvIndex){
            var QtyVal = vm.lineItemArray[truckIndex].cuvProductsArray[cuvIndex].quantities;
            !vm.lineItemArray[truckIndex].maxCUVQtyReached ? vm.lineItemArray[truckIndex].isAddedToTruck = false : null;
            if(operator == "+" && (QtyVal < Number(vm.cuvProductsList[cuvIndex].shippingFactor)) && vm.lineItemArray[truckIndex].cuvProductsArray[cuvIndex].correctCombination && !vm.lineItemArray[truckIndex].maxCUVQtyReached){     
                vm.lineItemArray[truckIndex].cuvProductsArray[cuvIndex].quantities = Number(vm.lineItemArray[truckIndex].cuvProductsArray[cuvIndex].quantities) + 1;
            } else if (operator == "-" && (QtyVal > 0)) {
                vm.lineItemArray[truckIndex].cuvProductsArray[cuvIndex].quantities = Number(vm.lineItemArray[truckIndex].cuvProductsArray[cuvIndex].quantities) - 1;   
            }
            vm.setCUVProductImageBasedOnUserSelection(truckIndex,cuvIndex);
            vm.lineItemArray[truckIndex].addToTruckDisable = !vm.ifAnyNonZero(vm.lineItemArray[truckIndex].cuvProductsArray) || !vm.lineItemArray[truckIndex].cuvProductsArray[cuvIndex].correctCombination || (vm.lineItemArray[truckIndex].isAddedToTruck && vm.lineItemArray[truckIndex].maxCUVQtyReached);
            vm.allowOnBeforeUnload = true;
            if(vm.lineItemArray[truckIndex].cuvProductsArray[cuvIndex].quantities == 0){
                vm.allowOnBeforeUnload = false;
            }
    }

    vm.setCUVProductImageBasedOnUserSelection = function(truckIndex,cuvIndex){
        var seatCapacity = vm.cuvProductsList[cuvIndex].seatCapacity;
        var QtyVal = vm.lineItemArray[truckIndex].cuvProductsArray[cuvIndex].quantities;
        var cuvProductsArray = vm.lineItemArray[truckIndex].cuvProductsArray;
        var imageArray = vm.lineItemArray[truckIndex].cuvImagesArray;
        var fiveSeaterTotal = vm.calculateFiveSeaters(cuvProductsArray);
        var twoSeaterTotal =  vm.calculateTwoSeaters(cuvProductsArray);
        vm.lineItemArray[truckIndex].totalUnits = parseInt(fiveSeaterTotal) + parseInt(twoSeaterTotal);
        var correctCombination = vm.checkTruckloadMatrix(fiveSeaterTotal,twoSeaterTotal);
        cuvProductsArray[cuvIndex].correctCombination = correctCombination;
        if(correctCombination){
            imageArray = [];
            for(var i=0;i < twoSeaterTotal;i++){
                imageArray.push('cuvImage seater2 col-xs-3 col-md-3');
            }
            for(var i=0;i < fiveSeaterTotal;i++){
                (twoSeaterTotal == 0 || twoSeaterTotal == 4) ? imageArray.push('cuvImage seater5 col-xs-4 col-md-4') : imageArray.push('cuvImage seater5 col-xs-3 col-md-3');
            }
            vm.lineItemArray[truckIndex].cuvImagesArray = imageArray;
            vm.lineItemArray[truckIndex].maxCUVQtyReached = false;
            _.map(cuvProductsArray,function(item,index){
                item.correctCombination = true;
            });
            _.map(vm.cuvProductsList,function(item,index){
                if(item.shippingFactor == fiveSeaterTotal && item.seatCapacity == 5 ||
                   item.shippingFactor == twoSeaterTotal && item.seatCapacity == 2){
                    vm.lineItemArray[truckIndex].maxCUVQtyReached = true;
                }
            });
            delete vm.lineItemArray[truckIndex].addToTruckResponse.errorMessage;

        } else {
            _.map(cuvProductsArray,function(item,index){
                item.correctCombination = false;
            });
            vm.lineItemArray[truckIndex].addToTruckResponse.errorMessage = 'GREATER';
        }
    }
    vm.calculateFiveSeaters = function(cuvProductsArray){
        var fiveSeaters = 0;
        angular.forEach(cuvProductsArray,function(cuvObject,index){
            if(cuvObject.seatCapacity == '5'){
                fiveSeaters = parseInt(fiveSeaters) + parseInt(cuvObject.quantities);
            }
        });
        return fiveSeaters;
    }
    vm.calculateTwoSeaters = function(cuvProductsArray){
        var twoSeaters = 0;
        angular.forEach(cuvProductsArray,function(cuvObject,index){
            if(cuvObject.seatCapacity == '2'){
                twoSeaters = parseInt(twoSeaters) + parseInt(cuvObject.quantities);
            }
        });
        return twoSeaters;
    }

    vm.checkTruckloadMatrix = function(fiveSeaterTotal,twoSeaterTotal){
        var combinationFlag = false;
        for(var i = vm.truckloadConfigMatrix.length-1;i >= 0;i--){
            if (vm.truckloadConfigMatrix[i].fiveSeaterCapacity == fiveSeaterTotal &&
                vm.truckloadConfigMatrix[i].twoSeaterCapacity >= twoSeaterTotal){
                combinationFlag = true;
                break;
            }
        }
        return combinationFlag;
    }
    vm.addTruckLoadLineItem = function(){
        vm.lineItemArray.push(angular.copy(vm.lineItemObj));
        vm.updateLineItemWithCUVProducts(vm.cuvProductsList);
    }
    vm.removeTruckloadLineItem = function(truckIndex){
            if(vm.lineItemArray[truckIndex].truckloadId != null){
                var deleteTruckURL = catEndpointService.truckloadDeleteURL+'?truckloadId='+vm.lineItemArray[truckIndex].truckloadId;
                httpService.get(deleteTruckURL).then(function(response) {
                    vm.totalTruckloadUnits = response.data.totalCartQuantity;
                    vm.lineItemArray.splice(truckIndex, 1);
                    ACC.minicart.refreshMinicartCountNum(); 
                });
            } else {
                vm.lineItemArray.splice(truckIndex, 1);
            }
    }

    vm.addProductsToTruck = function(truckIndex){
        if(vm.lineItemArray[truckIndex].shipAfterDateLabel == 'Select a Ship After date'){
            vm.lineItemArray[truckIndex].shipAfterDateError = true;
            $anchorScroll("truckloadForm" + truckIndex);
        } else {
            $.ajax({
                type:'POST',
                url: '/truckload/validateTruckload',
                data: $("#truckloadForm"+truckIndex).find("input").serialize(),
                success: function(data) {
                    vm.lineItemArray[truckIndex].addToTruckResponse = data;
                    if(data.hasOwnProperty('success')){
                        vm.lineItemArray[truckIndex].isAddedToTruck = true;
                        vm.lineItemArray[truckIndex].addToTruckDisable = true;
                        vm.lineItemArray[truckIndex].addNewLineItem = true;
                        vm.lineItemArray[truckIndex].truckloadId = data.truckloadId;
                        vm.lineItemArray[truckIndex].addToTruckSuccessMsg = true;
                        vm.lineItemArray[truckIndex].totalUnits = data.totalQuantity;
                        vm.lineItemArray[truckIndex].truckImgURL = data.truckloadImageUrl;
                        vm.totalTruckloadUnits = data.totalCartQuantity;
                        vm.lineItemArray[truckIndex].truckButtonType = 'UPDATE';
                        ACC.minicart.refreshMinicartCountNum();
                    } else {
                        vm.lineItemArray[truckIndex].addNewLineItem = (data.hasOwnProperty('errorMessage')) ?  false : true;
                    }
                    $scope.$apply();
                }
            });
            $timeout(function(){
                vm.lineItemArray[truckIndex].addToTruckSuccessMsg = false;
            },3000);
        }
    }
    vm.updateTruckProducts = function(truckIndex){
        var updateTruckURL = catEndpointService.truckloadUpdateURL;
        $.ajax({
            method :'POST',
            url: updateTruckURL,
            data: $("#truckloadForm"+truckIndex).find("input").serialize(),
            success: function(data) {
                vm.lineItemArray[truckIndex].addToTruckResponse = data;
                if(data.hasOwnProperty('success')){
                    vm.lineItemArray[truckIndex].isAddedToTruck = true;
                    vm.lineItemArray[truckIndex].addToTruckDisable = true;
                    vm.lineItemArray[truckIndex].addNewLineItem = true;
                    vm.lineItemArray[truckIndex].truckloadId = data.truckloadId;
                    vm.lineItemArray[truckIndex].addToTruckSuccessMsg = true;
                    vm.lineItemArray[truckIndex].totalUnits = data.totalQuantity;
                    vm.lineItemArray[truckIndex].truckImgURL = data.truckloadImageUrl;
                    vm.totalTruckloadUnits = data.totalCartQuantity;
                    vm.lineItemArray[truckIndex].truckButtonType = 'UPDATE';
                    ACC.minicart.refreshMinicartCountNum();
                    $timeout(function(){
                        vm.lineItemArray[truckIndex].addToTruckSuccessMsg = false;
                    },3000);
                } else {
                    vm.lineItemArray[truckIndex].addNewLineItem = (data.hasOwnProperty('errorMessage')) ?  false : true;
                }
                $scope.$apply();
            }
        });
    }
    vm.ifAnyNonZero = function(array){
        for(var i = 0; i < array.length; ++i) {
          if(array[i].quantities !== 0) {
            return true;
          }
        }
        return false;
    }
    window.onbeforeunload = function(){
        if(vm.allowOnBeforeUnload) {
            return true;
        } 
    }
    // vm.beforeUnloadFunction = function(event) {
    //     event.returnValue = "Are you sure , you wanted to navigate out of Page?";
    //     vm.removeBeforeUnloadEvent();    
    // }
    // vm.removeBeforeUnloadEvent = function() {
    //     window.removeEventListener("beforeunload", vm.beforeUnloadFunction);
    // }
    vm.truckloadNavigatetoReviewOrder = function() {
        var productCode = getUrlParamFactory.getUrlParams().productCode;
        var isAllTruckloadAdded = false;
        var newLineItem = vm.lineItemArray.filter(function(obj){
            return obj.truckloadId === null;
        })
        if(newLineItem.length > 0) {
            if(vm.isMobileSafari){
                vm.mobileSafariPopup();
            } else {
                vm.navigateToReviewOrder = true;
                // window.addEventListener("beforeunload", vm.beforeUnloadFunction);
            }
        } else {
            vm.lineItemArray.map(function(lineItem){
                if(!isAllTruckloadAdded && !lineItem.addToTruckDisable) {
                    isAllTruckloadAdded = true;
                    if(vm.isMobileSafari){
                        vm.mobileSafariPopup();
                    } else {
                        vm.navigateToReviewOrder = true;
                        // window.addEventListener("beforeunload", vm.beforeUnloadFunction);
                    }
                } else {
                   vm.navigateToReviewOrder = true;
                }
            });
        }   
        if(vm.navigateToReviewOrder) {
            vm.allowOnBeforeUnload = false;
            $window.location.href = "/truckload/reviewTruckload/?productCode=" + productCode;
        }
    }
}]);
