catApp.controller('utvPDPPlaceOrderController',['$uibModal','$uibModalStack','$scope','$window', 'catEndpointService', 'httpService',function($uibModal,$uibModalStack,$scope,$window, catEndpointService, httpService) {

var vm = this;

vm.view = {
    reorderMonths: [],
    canreorder: false,
    informatonMesage: "utvInformationMessage.html",
	    unitTotalValue: 0
}

vm.updateMonths=function(id, defaultValue, min, max, increment){
  vm.view.unitTotalValue += defaultValue;  
  vm.view.reorderMonths.push({
    'id': id,
    'defaultValue': defaultValue,
    'min': min,
    'max': max,
    'increment': increment,
    'value': defaultValue,
    'cannotDecrement': false,
    'cannnotIncrement': false
  });
}

vm.canOrder = function(){
    var reorder = false;
    vm.view.reorderMonths.forEach(function(obj){
        if(obj.value == 0){
            reorder = reorder || false;
        }else{
            reorder = true;  
        }
    });
    vm.view.canreorder = reorder;
}

vm.items = ['item1', 'item2', 'item3'];


  vm.showMoreDetails = function () {
    var parentElem = angular.element(".utv-order-container");

    var modalInstance = $uibModal.open({
      backdrop:'static',
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'myModalContent.html',
      controller: 'ModalInstanceCtrl',
      controllerAs: 'upo',
      size: 'md',
      appendTo: parentElem,
      resolve: {
        items: function () {
          return vm.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      vm.selected = selectedItem;
    }, function () {
      
    });
  };

vm.increment = function(index){
    if(!!vm.view.reorderMonths[index].max && vm.view.reorderMonths[index].max>0 && (vm.view.reorderMonths[index].value+vm.view.reorderMonths[index].increment) > vm.view.reorderMonths[index].max){
        vm.view.reorderMonths[index].cannnotIncrement = true;
    }else{
        vm.view.reorderMonths[index].cannnotIncrement = false;
        vm.view.reorderMonths[index].cannotDecrement = false;
        vm.view.reorderMonths[index].value = vm.view.reorderMonths[index].value+vm.view.reorderMonths[index].increment;
        vm.view.unitTotalValue += vm.view.reorderMonths[index].increment;
    }
    vm.canOrder();
}

vm.decrement = function(index){
    if((vm.view.reorderMonths[index].value-vm.view.reorderMonths[index].increment) < 0){
      vm.view.reorderMonths[index].cannotDecrement = true;
    }else{
       vm.view.reorderMonths[index].cannnotIncrement = false;
       vm.view.reorderMonths[index].cannotDecrement = false;
       vm.view.reorderMonths[index].value = vm.view.reorderMonths[index].value-vm.view.reorderMonths[index].increment; 
       vm.view.unitTotalValue -= vm.view.reorderMonths[index].increment;
    }
    if(vm.view.reorderMonths[index].value == 0){
        vm.view.reorderMonths[index].cannotDecrement = true;   
    }
    vm.canOrder();
}

vm.init = function(){
    if($("#reorder-months-block").height() > 0){
        $("#reorder-button-block").height($("#reorder-months-block").height());
    }
/*    if($(".utv-headding-row").height() > 0){
        $(".utv-price").height($(".utv-headding").height());
    }*/
}

vm.init();

}]); 
