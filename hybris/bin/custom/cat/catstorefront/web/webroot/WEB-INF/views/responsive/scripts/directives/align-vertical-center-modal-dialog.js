catApp.directive("alignVerticalCenterModalDialog", ['$timeout',function($timeout){
    return {
        restrict: "A",
        link: function (scope, element, attrs) {
        	 $timeout(function(){
                 angular.element(angular.element('.modal-dialog')[0]).addClass('edit-pop-up-wrapper clearfix');
                 //angular.element(angular.element("[ng-app=cat-app]")[0]).addClass('pop-up-blur');
                 if(attrs.alignVerticalCenterModalDialog){
                    angular.element(angular.element('.modal-dialog')[0]).addClass(attrs.alignVerticalCenterModalDialog+'');
                 }
        	 		}
        		 );
        }
    };
}]);