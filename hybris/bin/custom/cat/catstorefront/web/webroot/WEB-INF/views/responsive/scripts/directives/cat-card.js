catApp.directive('catCard', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            laneType:'@',
            lane1:'@',
            lane2:'@',
            lane3:'@',
            sameInResponsive: '@',
            superHeader: '@',
            primaryHeader: '@',
            image: '@',
            favoriteMarked: '@',
            productIdForCommunication: '@',
            pdpLink: '@',
            showFullcard: '@',
            engineModelText: '@',
            engineModelValue: '@',
            netPowerText: '@',
            netPowerValue: '@',
            operatingWeightText:'@',
            operatingWeightValue:'@',
            productPrice: '@',
            currency: '@',
            delarNameText: '@',
            delarNameValue: '@',
            branchLocationText: '@',
            branchLocationValue:'@',
            hoursUsedText: '@',
            hoursUsedValue: '@',
            manufacturingYearText: '@',
            manufacturingYearValue: '@',
            utvUser: '@',
            fuelType: '@',
            maxSpeed: '@',
            seatCapacity: '@',
            hideBookMarked: '@',
            utvPrice: '@',
            usedTagPosition: '@',
            stockInTransit: '@',
            viewIntransit: '@',
            noViewIntransit: '@',
            disContinued: '@',
            relation:'@',
            userType:'@'
        },
        templateUrl: '/_ui/responsive/cat/templates/cardTemplate.html',
        compile: function(tElem, attrs) {
            //do optional DOM transformation here
            return function(scope, elem, attrs) {
                if(!!attrs.relation){
                    scope.relation = attrs.relation; 
                }
                if(!!attrs.laneType){
                    scope.laneType = attrs.laneType;
                }
                if(!!attrs.lane1){
                    scope.lane1 = attrs.lane1;
                }
                if(!!attrs.lane2){
                    scope.lane2 = attrs.lane2;
                }
                if(!!attrs.lane3){
                    scope.lane3 = attrs.lane3;
                }
                if(!!attrs.userType){
                    scope.isLaneDotsApplied = (attrs.userType === 'SP')?false:true;
                }
                scope.showFullcard = false;
                if (!!attrs.showFullcard && attrs.showFullcard == "true") {
                    scope.showFullcard = true;
                }
                scope.bookMarked = false;
                if (!!attrs.bookMarked && attrs.bookMarked == "true") {
                    scope.bookMarked = true;
                }
                scope.disContinued = 'false';
                if (!!attrs.disContinued && attrs.disContinued == "true") {
                    scope.disContinued = 'true';
                }
                // if(scope.disContinued){
                //     scope.bookMarked = false;
                // }
                scope.dealerCertified = false;
                if (!!attrs.dealerCertified && attrs.dealerCertified=="true") {
                    scope.dealerCertified = true;
                }
                if (!!attrs.productType && attrs.productType == "used") {
                    scope.newProductcard = false;
                    scope.usedProductCard = true;
                }else{
                    scope.newProductcard = true;
                    scope.usedProductCard = false;
                }
                if(!!attrs.utvUser && attrs.utvUser == 'true'){
                    scope.newProductcard = false;
                    scope.hideBookMarked = true;
                    scope.dealerCertified = false;
                    scope.utvCard = true;
                }
                if(!!attrs.image){
                    scope.newImage = attrs.image;
                }
                if(!!attrs.sameInResponsive && attrs.sameInResponsive == "false"){
                    scope.showMobileTemplate = true;
                }else{
                    scope.showMobileTemplate = false;
                }

                if(!!attrs.productIdForCommunication){
                    scope.formId = attrs.productIdForCommunication.replace(/[.]/g, '-');
                }else{
                    scope.formId = "";
                }
            };
        },
        controller: ['$scope', '$window', 'catEndpointService', 'httpService', '$rootScope', 'catImageFinder','$timeout', 'constantService', 'catService' , function($scope, $window, catEndpointService, httpService, $rootScope, catImageFinder,$timeout, constantService, catService) {
            var listener_a,listener_b;
            $scope.defaultDownloadImage = constantService.defaultDownloadImage;
            var formNameAndValueForAnInputTag = function(queryParamString){
                    var params = queryParamString.split('=');
                    return {
                        name:params[0],
                        value:params[1]
                    }
            }
            var formHiddenInputTags=function(queryString){
                var splittedQueryString = (queryString||'') && queryString.replace(/\?/,'').split('&'),
                    result=[],
                    i=splittedQueryString.length;
                    
                    while(i--){
                      result.push(formNameAndValueForAnInputTag(splittedQueryString[i]));
                    }
                    return result;
            };
               $scope.PDPrequestBody=formHiddenInputTags(catService.queryParams.bkp);
               $scope.PDPrequestBody.push({name:'CSRFToken',value:ACC.config.CSRFToken});
               $scope.PDPrequestBody.push({name:'laneType',value:$scope.laneType});
            $scope.navigateToPDP = function() {
                $scope.PDPrequestBody=[];
                if (!!$scope.pdpLink) {
                    angular.element('#'+$scope.formId).submit();
                }
            }
            $scope.decreaseQuantity = function() {
                if ($scope.reorderqty > 0) {
                    $scope.reorderqty = $scope.reorderqty - 1;
                } else {
                    $scope.reorderqty = 0;
                }
            }
            $scope.increaseQuantity = function() {
                $scope.reorderqty = $scope.reorderqty + 1;
            }
            $scope.resetInputValue = function() {
                if (!$scope.reorderqty) {
                    $scope.reorderqty = $scope.defaultValue;
                }
            }
            $scope.toggleBookMark = function() {
                
                // if($scope.disContinued=="false"){
                   httpService.get(catEndpointService.toggleFavoriteIcon + "?productCodePost=" + $scope.productIdForCommunication).then(function(response) {
                    if (response.status == 200) {
                        if (!!response.data) {
                            if (!!response.data.addSuccess) {
                                $scope.bookMarked = true;
                            } else if (!!response.data.removeSuccess) {
                                $scope.bookMarked = false;
                            } else {
                                $("#favoriteLimitReached").show();
                            }
                        }
                        listener_b = $rootScope.$emit('bookmark-toggled', { 
                            image: $scope.newImage,
                            primaryHeader: $scope.primaryHeader,
                            pdpLink: $scope.pdpLink,
                            bookMarked: $scope.bookMarked,
                            productIdForCommunication: $scope.productIdForCommunication,
                            isNewProduct: $scope.newProductcard,
                            disContinued: $scope.disContinued
                        });
                        if($scope.relation && $scope.relation.toLowerCase()==='slave'){
                            $scope.bookMarked = true;
                        }
                       
                        
                    }
                }, function(error) {


                

                })
                // }
            };


            $scope.init = function(){
                catImageFinder.isImage($scope.image).then(function(result) {
                    if(!result){
                        $scope.newImage = "";
                    }
                });
            }

            $scope.init();
            $scope.$on('destroy',function(){
                listener_a();
                listener_b();
            })
                listener_a =  $rootScope.$on('bookmark-toggled', function (event, data) {
                    if(data.productIdForCommunication == $scope.productIdForCommunication 
                        && $scope.relation && $scope.relation.toLowerCase()==='master'){
                        $scope.bookMarked = data.bookMarked;
                        event.stopPropagation();
                    }
                });

        }]
    };
});

