catApp.directive('catAddress', function() {
    return {
        restrict: 'E',
        replace:true,
        templateUrl: '/_ui/responsive/cat/templates/catAddressTemplate.html',
        scope: {
            updateSelectedAddress:'&',
            shipAddressId:'@'
        },
        compile: function(tElem, attrs) {
            return function(scope, elem, attrs) {
            
            };
        },
        controller: ['$scope', 'catEndpointService', 'httpService','$timeout' ,'reviewOrderValidation',
            function($scope, catEndpointService, httpService,$timeout,reviewOrderValidation) {
            
            var addressValidation = {
                    validateAddressField:function(){
                    if($scope.selectedAddress&&$scope.isAddressValid){
                        $scope.isAddressEmpty = false;
                        return false;
                    }
                    $scope.isAddressEmpty = true;
                    return true;
                    
                    }
                },
                isDropDownSelected='',
                focusShippingAddressField=function(callback){
                    $timeout(function(){
                        angular.element("#"+$scope.shipAddressId).focus();
                        callback?callback():'';
                    },500);
                };
           
            $scope.shippingAddressErrMsg=ACC.localization.shippingAddressErrMsg;
            $scope.shippingAddress=[];
            $scope.resultsNotFound=ACC.localization.noResultsFound;
          
            angular.element('body').on('click',function(){
                isDropDownSelected=false;
            });
            $scope.userModifiedField=function(){
                $scope.isAddressValid=false;
            };
            $scope.showAddressesList=function(){
                if(isDropDownSelected){
                    isDropDownSelected=false;
                    return;
                }
                focusShippingAddressField(function(){
                    isDropDownSelected=true;
                });
            };
            $scope.filterCondition = function(conditions){
                return ($scope.selectedAddress&&$scope.selectedAddress.length>conditions.triggerFrom)?$scope.selectedAddress:'';
            };
          
            var getFormattedAddress=function(listObj){
                var addressLine1='',addressLine2='',addressLine3='';
                    addressLine1 = (listObj.shipToCode?listObj.shipToCode+', ':'')
                                    +(listObj.line3?listObj.line3.titleCase()+', ':'')
                                    +(listObj.line1?listObj.line1.titleCase()+', ':'');
                    addressLine2 =  (listObj.town?listObj.town.titleCase()+', ':'')
                                    +(listObj.region?listObj.region.name.titleCase()+', ':'');
                    addressLine3 =  (listObj.country?listObj.country.isocode+', ':'')
                                    +(listObj.postalCode||'');

                return addressLine1+'\n'+addressLine2+'\n'+addressLine3;
            }
           
            $scope.autosuggestionList = function(){
                
                httpService.get(catEndpointService.dealerShippingAddress).then(function(response) {
                    if (response.status == 200) {
                        $scope.shippingAddress = response.data.allShippingAddressList.map(function(listObj,index){
                                listObj.formattedAddress = getFormattedAddress(listObj);
                                
                                return listObj;
                        });
                       
                    }
                }, function(error) {
        
                });
                
            }
            $scope.clearAddress=function(){
                $scope.selectedAddress='';
                focusShippingAddressField();
                 
            }
            $scope.init = function() {
                $scope.autosuggestionList();
                
                reviewOrderValidation.subscribe(reviewOrderValidation.reviewOrderPageSubscriptionId,
                    addressValidation,'validateAddressField');
            }
            $scope.onSelect = function(selectedObj){
                $scope.selectedAddress=selectedObj.formattedAddress;
                $scope.isAddressValid=true;
                $scope.updateSelectedAddress({addrssObj:selectedObj});
            }
            $scope.init();
        }],

    }
});