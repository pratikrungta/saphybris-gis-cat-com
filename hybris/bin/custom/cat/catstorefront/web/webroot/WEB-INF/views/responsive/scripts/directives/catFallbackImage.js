catApp.directive('catFallbackImage', ['constantService', function(constantService) {
    return {
      link: function(scope, element, attrs) {
        element.bind('error abort', function() {
            if(attrs.src != constantService.defaultDownloadImage){
                attrs.$set('src', constantService.defaultDownloadImage);
            }
        });
      }
    }
}]);