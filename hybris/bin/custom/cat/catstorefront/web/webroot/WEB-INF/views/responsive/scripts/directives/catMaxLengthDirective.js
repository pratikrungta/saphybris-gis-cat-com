catApp.directive('catMaxLength', [function() {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var maxLength = Number(attrs.catMaxLength);
            element[0].addEventListener('keypress', function(event) {
                if (event.target.value.length >= maxLength) {
                    event.preventDefault();
                }
            });
        }
    }
}]);