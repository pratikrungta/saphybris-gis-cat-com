catApp.directive('catSearch', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: '/_ui/responsive/cat/templates/typeahead.html',
        scope: {
            plpLink: '@',
            id: '@',
            placeholderUsedProducts: '@',
            placeholderNewProducts: '@',
            placeholderForMobile: '@'
        },
        compile: function(tElem, attrs) {
            return function(scope, elem, attrs) {
                if (!!attrs.defaultValue && attrs.defaultValue == 'usedProductPLP') {
                    scope.usedProducts = true;
                    scope.placeholder = attrs.placeholderUsedProducts;
                }else if (!!attrs.defaultValue && attrs.defaultValue == "newProductPLP") {
                    scope.newProducts = true;
                    scope.placeholder = attrs.placeholderNewProducts;
                }
                scope.init();
            };
        },
        controller: ['$scope', 'catEndpointService', 'httpService', 'filterFilter', '$window', '$timeout', 'catService', '$filter','screenResizedService', function($scope, catEndpointService, httpService, filterFilter, $window, $timeout, catService, $filter, screenResizedService) {
            $scope.view = {
                searchResults: [],
                searchResultsBkp: [],
                inputDirty: false,
                emptyResults: false,
                codeSuggester: 'Serial Number',
                serialNumber: 'Serial Number',
                productFamily: 'PRODUCT FAMILY',
                salesModel: 'SALES MODEL',
                baseModel: 'BASE MODEL',
                typeaHeadDropDownTemplate: catEndpointService.typeaheadDropdownHtml,
                categoryCode: '',
                primaryOfferingId: '',
                salesModelId: '',
                inputBoxIsEmpty: true,
                inputValue: "",
                screenResizedService: screenResizedService
            };

            $scope.makeLowerCase = function(string) {
                return angular.lowercase(string);
            };

            $scope.getCatResults = function(search) {
                $scope.view.inputValue = search;
                if (!!search && search.length >= 2) {
                    return httpService.get(catEndpointService.typeaheadSearchEndPoint + "?text=" + $scope.makeLowerCase(search) + "&isUsed=" + !!$scope.usedProducts).then(
                    function(response) {
                        $scope.view.searchResults = [];
                        if (!!response.data && response.data.productFamilyNameSuggester && response.data.productFamilyNameSuggester.length > 0) {
                            (response.data.productFamilyNameSuggester).forEach(function(obj, index) {
                                if (index < 5) {
                                    var searchObject = {
                                        firstInGroup: false,
                                        displayValue: "",
                                        group: "",
                                        categoryCode: "",
                                        salesModelId: "",
                                        primaryOfferingId: ""
                                    }
                                    if (index == 0) {
                                        searchObject.firstInGroup = true;
                                    } else {
                                        searchObject.firstInGroup = false;
                                    }
                                    searchObject.displayValue = !!obj ? $filter("titleCase")(obj) : "";
                                    searchObject.categoryCode = !!obj ? obj : "";
                                    searchObject.type = "productFamilyNameSuggester";
                                    searchObject.group = $scope.view.productFamily;
                                    $scope.view.searchResults.push(searchObject);
                                }
                            });
                            var lastobj = $scope.view.searchResults.pop();
                            lastobj.lastObject = true;
                            $scope.view.searchResults.push(lastobj);
                        } else {
                            if (!!response.data && response.data.salesModelNameSuggester && response.data.salesModelNameSuggester.length > 0) {
                                (response.data.salesModelNameSuggester).forEach(function(obj, index) {
                                    if (index < 5) {
                                        var searchObject = {
                                            firstInGroup: false,
                                            displayValue: "",
                                            group: "",
                                            categoryCode: "",
                                            salesModelId: "",
                                            primaryOfferingId: ""
                                        };
                                        if (index == 0) {
                                            searchObject.firstInGroup = true;
                                        } else {
                                            searchObject.firstInGroup = false;
                                        }
                                        searchObject.displayValue = !!obj ? $filter("toUpperCase")(obj) : "";
                                        searchObject.salesModelId = !!obj ? obj : "";
                                        searchObject.type = "salesModelNameSuggester";
                                        searchObject.group = $scope.view.salesModel;
                                        $scope.view.searchResults.push(searchObject);
                                    }
                                });
                                var lastobj = $scope.view.searchResults.pop();
                                lastobj.lastObject = true;
                                $scope.view.searchResults.push(lastobj);
                            }
                            if (!!response.data && response.data.baseModelNameSuggester && response.data.baseModelNameSuggester.length > 0) {
                                (response.data.baseModelNameSuggester).forEach(function(obj, index) {
                                    if (index < 5) {
                                        var searchObject = {
                                            firstInGroup: false,
                                            displayValue: "",
                                            group: "",
                                            categoryCode: "",
                                            salesModelId: "",
                                            primaryOfferingId: ""
                                        };
                                        if (index == 0) {
                                            searchObject.firstInGroup = true;
                                        } else {
                                            searchObject.firstInGroup = false;
                                        }
                                        searchObject.displayValue = !!obj ? $filter("toUpperCase")(obj) : "";
                                        searchObject.primaryOfferingId = !!obj ? obj : "";
                                        searchObject.type = "baseModelNameSuggester";
                                        searchObject.group = $scope.view.baseModel;
                                        $scope.view.searchResults.push(searchObject);
                                    }
                                });
                                var lastobj = $scope.view.searchResults.pop();
                                lastobj.lastObject = true;
                                $scope.view.searchResults.push(lastobj);
                            }
                        }
                        if(!!response.data && response.data.codeSuggester && response.data.codeSuggester.length > 0){
                            (response.data.codeSuggester).forEach(function(obj, index) {
                                if (index < 5) {
                                    var searchObject = {
                                        firstInGroup: false,
                                        displayValue: "",
                                        group: "",
                                        categoryCode: "",
                                        salesModelId: "",
                                        primaryOfferingId: ""
                                    }
                                    if (index == 0) {
                                        searchObject.firstInGroup = true;
                                    } else {
                                        searchObject.firstInGroup = false;
                                    }
                                    searchObject.displayValue = !!obj ? $filter("toUpperCase")(obj) : "";
                                    searchObject.codeSuggester = !!obj ? obj : "";
                                    searchObject.type = "codeSuggester";
                                    searchObject.group = $scope.view.codeSuggester;
                                    $scope.view.searchResults.push(searchObject);
                                }
                            });
                            var lastobj = $scope.view.searchResults.pop();
                            lastobj.lastObject = true;
                            $scope.view.searchResults.push(lastobj);
                        }
                        if(!!response.data && response.data.serialNumberSuggester && response.data.serialNumberSuggester.length > 0){
                            (response.data.serialNumberSuggester).forEach(function(obj, index) {
                                if (index < 5) {
                                    var searchObject = {
                                        firstInGroup: false,
                                        displayValue: "",
                                        group: "",
                                        categoryCode: "",
                                        salesModelId: "",
                                        primaryOfferingId: ""
                                    }
                                    if (index == 0) {
                                        searchObject.firstInGroup = true;
                                    } else {
                                        searchObject.firstInGroup = false;
                                    }
                                    searchObject.displayValue = !!obj ?$filter("toUpperCase")(obj) : "";
                                    searchObject.serialNumberSuggester = !!obj ? obj : "";
                                    searchObject.type = "serialNumberSuggester";
                                    searchObject.group = $scope.view.serialNumber;
                                    $scope.view.searchResults.push(searchObject);
                                }
                            });
                            var lastobj = $scope.view.searchResults.pop();
                            lastobj.lastObject = true;
                            $scope.view.searchResults.push(lastobj);
                        }
                        if (!$scope.view.searchResults || (!!$scope.view.searchResults && $scope.view.searchResults.length == 0)) {
                            var obj = {};
                            obj.firstInGroup = true;
                            obj.group = "No Results Found";
                            obj.noResultsFound = true;
                            obj.noResultsFoundClass = "no-results";
                            $scope.view.searchResults = [];
                            $scope.view.searchResults.push({
                                "firstInGroup": true,
                                "group": "No search results.",
                                "noResultsFound": true,
                                "noResultsFoundClass": "no-results"
                            });
                        }
                        $scope.view.searchResultsBkp = $scope.view.searchResults;
                        return $scope.view.searchResults;
                    }, 
                    function(error) {
                        $scope.view.searchResults = [];
                        var obj = {};
                        obj.firstInGroup = true;
                        obj.group = "No search results.";
                        obj.noResultsFound = true;
                        obj.noResultsFoundClass = "no-results";
                        $scope.view.searchResults.push({
                            "firstInGroup": true,
                            "group": "No Results Found",
                            "noResultsFound": true,
                            "noResultsFoundClass": "no-results"
                        });

                        $scope.view.searchResultsBkp = $scope.view.searchResults;
                        return $scope.view.searchResults;
                    });
                }
            };


            $scope.onSelect = function($item, $model, $label) {
                var bkp = $scope.view.searchResults;
                if (!!$item && !$item.noResultsFound) {
                    var categoryCode = "";
                    var salesModelId = "";
                    var primaryOfferingId = "";
                    if (!!$model) {
                        categoryCode = $model.categoryCode;
                        if (!!$model.salesModelId) {
                            salesModelId = $model.salesModelId;
                        }
                        if (!!$model.primaryOfferingId) {
                            primaryOfferingId = $model.primaryOfferingId;
                        }
                    }
                    if($model.type == "codeSuggester"){
                        $window.location.href = catEndpointService.pdpPageLink+"/"+$model.codeSuggester+"?searchKeywordCaptured="+$scope.view.inputValue;
                    }else if($model.type == "serialNumberSuggester"){
                        $window.location.href = catEndpointService.plpPageLink+"/serialNumberSearch/?serialNumber="+$model.serialNumberSuggester+"&searchKeywordCaptured="+$scope.view.inputValue;
                    }else{
                        $window.location.href = $scope.plpLink + "?categoryCode=" + categoryCode + "&salesModelId=" + salesModelId + "&primaryOfferingId=" + primaryOfferingId + "&isUsed=" + !!$scope.usedProducts+"&searchKeywordCaptured="+$scope.view.inputValue;
                    }
                }else{
                    //$scope.view.searchResults = $scope.view.searchResults.searchResultsBkp;
                    $scope.selectedSearchItem = $scope.view.inputValue;
                    return false;
                }
            }

            $scope.toggleSearchOption = function(isNewSearch) {
                $scope.newProducts = !isNewSearch;
                $scope.usedProducts = isNewSearch;
                $scope.selectedSearchItem = null;
                $scope.updateService();
            }

            $scope.updateService = function() {
                if ($scope.newProducts) {
                    catService.userWantsNewProducts();
                    $scope.placeholder = $scope.placeholderNewProducts;
                } else {
                    catService.userWantsUsedProducts();
                    $scope.placeholder = $scope.placeholderUsedProducts;
                }
            }

            $scope.init = function() {
                $scope.updateService();
            }



            $scope.$watch('typeaheadIsOpen', function(newVal, oldVal) {
                if (newVal == oldVal) {
                    return;
                }
                if (newVal) {
                    $timeout(function() {
                        var searchField = angular.element.find('input' + "#" + $scope.id);
                        var width = searchField[0].offsetWidth;
                        var top = searchField[0].offsetTop;
                        var inputHeight = searchField[0].offsetHeight;
                        var dropdown = angular.element.find('.dropdown-menu');
                        angular.element(dropdown[0]).css({ 'width': (width + 'px'), 'top': ((top + inputHeight) + 'px') });
                    }, 0)
                }
            });

            $scope.clearInput = function() {
                $scope.selectedSearchItem = null;
            }

            $scope.$watch("selectedSearchItem", function(val) {
                if (!!val) {
                    $scope.view.inputBoxIsEmpty = false;
                } else {
                    $scope.view.inputBoxIsEmpty = true;
                }
            });
        }],

    }
});