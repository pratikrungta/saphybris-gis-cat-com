catApp.directive('fbcProductCard', function() {
    return {
        restrict: 'EA',
        replace: true,
        templateUrl: '/_ui/responsive/cat/templates/fbcMustSelectProductCard.html',
        compile: function(tElem, attrs) {
            //do optional DOM transformation here
            return function($scope, $elem, $attrs) {
                $scope.cardData = JSON.parse($attrs.data);
                $scope.popupTemplate = '/_ui/responsive/cat/templates/fbcMustSelectPopover.html';
            };
        },
        controller: ['$scope', '$window', 'catEndpointService', 'httpService', 'catImageFinder', function($scope, $window, catEndpointService, httpService, catImageFinder) {
        }]
    };
});