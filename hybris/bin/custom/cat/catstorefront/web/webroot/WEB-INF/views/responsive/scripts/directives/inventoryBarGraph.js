catApp.directive('inventoryBarGraph', function() {
    return {
        restrict: 'E',
        templateUrl: '/_ui/responsive/cat/templates/inventoryBarGraph.html',
        scope: {
            graph: '=' 
        },
        controller: ['$scope',
            function($scope) {
                $scope.barPointerData = "";
                $scope.barColor = "";
                $scope.minimum = ACC.localization.minimum;
                $scope.recommended = ACC.localization.recommended;
                $scope.current = ACC.localization.current;
                $scope.informatonMesage = ACC.localization.LaneMsg;
                $scope.graphProjectedData = function() {
                    $scope.temp = $scope.graph.current;
                    if($scope.graph.current > $scope.graph.recommended) {
                        $scope.temp = $scope.graph.recommended;
                    }
                    if($scope.graph.current < 0) {
                        $scope.temp = 0;
                    } 
                    $scope.barPointerData = ($scope.temp/$scope.graph.recommended) * 100;
                }
            }],
    }
});