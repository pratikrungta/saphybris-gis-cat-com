catApp.directive('pdpUsedProductCard', function() {
    return {
        restrict: 'EA',
        replace: true,
        scope: {
            headerCode : '@',
            salesModel : '@',
            image : '@',
            price : '@',
            dealerName : '@',
            branchLocation : '@',
            hoursUsed : '@',
            manufacturingYear : '@',
            bookMarked:'@'
        },
        templateUrl: '/_ui/responsive/cat/templates/pdpUsedProductCard.html',
        compile: function(tElem, attrs) {
            //do optional DOM transformation here
            return function($scope, $elem, $attrs) {
                $scope.headerCode = $attrs.headerCode;
                $scope.salesModel = $attrs.salesModel;
                if(!!attrs.image){
                    var imageArray = (attrs.image).split("?"); 
                    $scope.image = imageArray[0]+"?wid=103&hei=72";
                } 
                $scope.bookMarked = $attrs.bookMarked
                $scope.price = $attrs.price;
                $scope.dealerName = $attrs.dealerName;
                $scope.branchLocation = $attrs.branchLocation;
                $scope.hoursUsed = $attrs.hoursUsed;
                $scope.manufacturingYear = $attrs.manufacturingYear;
                $scope.bookMarked = $attrs.bookMarked;
            };
        },
        controller: ['$scope', '$window', 'catEndpointService', 'httpService', 'constantService', function($scope, $window, catEndpointService, httpService, constantService) {
            $scope.defaultDownloadImage = constantService.defaultDownloadImage;
            $scope.toggleBookMarkIcon = function() {
                ($scope.bookMarked === 'true') ? ($scope.bookMarked = 'false') : ($scope.bookMarked = 'true');
            }
        }]
    };
});