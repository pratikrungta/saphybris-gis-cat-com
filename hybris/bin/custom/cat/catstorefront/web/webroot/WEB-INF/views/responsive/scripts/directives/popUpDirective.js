catApp.directive('popUp',function(){
	return {
		restrict: 'E',
        replace: true,
        scope: {
        	alertHeader: '@',
            alertContent: '@',
            alertButtonContent: '@',
            id: '@',
            ok:'@'
        },
        templateUrl: '/_ui/responsive/cat/templates/popUpTemplate.html',
        controller: ['$scope', '$window', function($scope, $window) {
            $scope.popUpClose = function() {
                $('#'+$scope.id).hide();
            }
        }]
    };
});