catApp.directive('productCompareCardMobile', function() {
    return {
        restrict: 'EA',
        replace: true,
        scope: {
            headerCode : '@',
            productName : '@',
            image : '@',
            productDesc : '@',
            engineModel : '@',
            netPower : '@',
            operatingWeight : '@',
            price : '@',
            fuelType : '@',
            maxSpeed : '@',
            seatingCapacity : '@',
            bookMarked : '@'
        },
        templateUrl: '/_ui/responsive/cat/templates/productCompareCardMobile.html',
        compile: function(tElem, attrs) {
            //do optional DOM transformation here
            return function($scope, $elem, $attrs) {
                $scope.headerCode = $attrs.headerCode;
                $scope.productName = $attrs.productName;
                if(!!attrs.image){
                    var imageArray = (attrs.image).split("?"); 
                    $scope.image = imageArray[0]+"?wid=220&hei=162";
                } 
                $scope.productDesc = $attrs.productDesc;
                $scope.engineModel = $attrs.engineModel;
                $scope.netPower = $attrs.netPower;
                $scope.operatingWeight = $attrs.operatingWeight;
                $scope.price = $attrs.price;
                $scope.fuelType = $attrs.fuelType;
                $scope.maxSpeed = $attrs.maxSpeed;
                $scope.seatingCapacity = $attrs.seatingCapacity;
                $scope.bookMarked = $attrs.bookMarked;
            };
        },
        controller: ['$scope', '$window', 'catEndpointService', 'httpService', function($scope, $window, catEndpointService, httpService) {
            $scope.toggleBookMarkIcon = function() {
                $scope.bookMarked = ($scope.bookMarked === 'true') ? 'false' : 'true';
            }
            $scope.navigateToProductCompare = function(headerCode){
                $window.location.href = "/speccheck/viewSpeccheckCompare/?salesModel="+headerCode+"&categoryName="+$scope.productName;
            };
        }]
    };
});