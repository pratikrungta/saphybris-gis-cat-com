catApp.directive('productCompareCard', function() {
    return {
        restrict: 'EA',
        replace: true,
        scope: {
            headerCode : '@',
            productName : '@',
            image : '@',
            productDesc : '@',
            engineModel : '@',
            netPower : '@',
            operatingWeight : '@',
            price : '@',
            fuelType : '@',
            maxSpeed : '@',
            seatingCapacity : '@',
            bookMarked : '@'
        },
        templateUrl: '/_ui/responsive/cat/templates/productCompareCard.html',
        compile: function(tElem, attrs) {
            //do optional DOM transformation here
            return function($scope, $elem, $attrs) {
                $scope.headerCode = $attrs.headerCode;
                $scope.productName = $attrs.productName;
                if(!!$attrs.image){
                    $scope.newImage = $attrs.image;
                } 
                $scope.productDesc = $attrs.productDesc;
                $scope.engineModel = $attrs.engineModel;
                $scope.netPower = $attrs.netPower;
                $scope.operatingWeight = $attrs.operatingWeight;
                $scope.price = $attrs.price;
                $scope.fuelType = $attrs.fuelType;
                $scope.maxSpeed = $attrs.maxSpeed;
                $scope.seatingCapacity = $attrs.seatingCapacity;
                $scope.bookMarked = $attrs.bookMarked;
            };
        },
        controller: ['$scope', '$window', 'catEndpointService', 'httpService', 'catImageFinder', function($scope, $window, catEndpointService, httpService, catImageFinder) {
            $scope.toggleBookMarkIcon = function() {
                ($scope.bookMarked === 'true') ? ($scope.bookMarked = 'false') : ($scope.bookMarked = 'true');
            }
            $scope.navigateToProductCompare = function(headerCode){
                $window.location.href = "/speccheck/viewSpeccheckCompare/?salesModel="+headerCode+"&categoryName="+$scope.productName;
            };
            $scope.init = function(){
                catImageFinder.isImage($scope.image).then(function(result) {
                    if(!result){
                        $scope.newImage = "";
                    }
                });
            }
            $scope.init();
        }]
    };
});