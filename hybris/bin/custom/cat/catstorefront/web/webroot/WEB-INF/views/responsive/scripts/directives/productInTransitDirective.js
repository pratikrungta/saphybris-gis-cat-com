catApp.directive('productInTransitDirective',['catEndpointService','httpService',function(catEndpointService,httpService) {
    return {
        restrict:'A',
        templateUrl:'/_ui/responsive/cat/templates/productInTransitDirective.html',
        scope:{
        	dealerCode: '@',
            productCode: '@',
            tabName:'@'
        },
        compile: function(tElem, attrs){
        	return function(scope,elem,attrs){
        		if(!!attrs.dealerCode){
        			scope.dealerCode = attrs.dealerCode;
        		}
				if(!!attrs.productCode){
					scope.productCode = attrs.productCode;
                }
                if(!!attrs.tabName){
					scope.tabName = attrs.tabName;
				}
        	}
        },
        controller: ['$scope','pdpTabService','constantService', function($scope,pdpTabService,constantService){
            
            angular.element(angular.element(".tabs")[0]).on('click',
            function(event){
                var tabName = pdpTabService.getTabName(angular.element(event.target).text());
                if(tabName===$scope.tabName){
                    $scope.loadData();
                    }
               
                }
            )

            $scope.EDHData = [];
            $scope.inTransitLink = constantService.pdpPage.inTransitLink;
            $scope.loadData = function(){
                $scope.isloaderOn=true;
                $scope.serialLabel = ACC.localization.serialNumberLabel;
                $scope.estimatedDeliveryDateLabel = ACC.localization.estimatedDeliveryDateLabel;
                pdpTabService.loadUpdatedData(
                    {
                     url:catEndpointService.edhIntransitDetails,
                     method:'post',
                     dealerCode:$scope.dealerCode, 
                     productCode:$scope.productCode
                    }).then(
                    function(res){
                        $scope.isloaderOn=false;
                        res?(angular.copy(res,$scope.EDHData)):'';
                    },function(res){
                        $scope.errObj=res;
                        $scope.isloaderOn=false
                    }
                );  
            }
            $scope.loadData();
        }]
    }
}])