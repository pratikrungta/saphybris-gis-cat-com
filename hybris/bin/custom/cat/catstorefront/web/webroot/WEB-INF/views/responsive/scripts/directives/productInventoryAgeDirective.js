catApp.directive('productInventoryAgeDirective',['httpService','catEndpointService',function(httpService,catEndpointService) {
     return {
        restrict:'A',
        templateUrl:'/_ui/responsive/cat/templates/productInventoryAge.html',
        scope:{
        	dealerCode: '@',
            productCode: '@',
            tabName:'@'
        },
        compile: function(tElem, attrs){
        	return function(scope,elem,attrs){
        		if(!!attrs.dealerCode){
        			scope.dealerCode = attrs.dealerCode;
        		}
				if(!!attrs.productCode){
					scope.productCode = attrs.productCode;
                }
                if(!!attrs.tabName){
					scope.tabName = attrs.tabName;
				}
        	}
        },
        controller: ['$scope','pdpTabService','constantService', function($scope,pdpTabService,constantService){
            
            angular.element(angular.element(".tabs")[0]).on('click',
            function(event){
                var tabName = pdpTabService.getTabName(angular.element(event.target).text());
                if(tabName===$scope.tabName){
                    $scope.loadData();
                    }
               
                }
            );

            $scope.EDHData =  [];
            $scope.errMsg = ACC.localization.inventoryAgeError;
            $scope.loadData = function(){
                $scope.isloaderOn=true;
                $scope.serialLabel = ACC.localization.serialNumberLabel;
                $scope.inventoryAgeLabel = ACC.localization.inventoryAgeLabel;
                pdpTabService.loadUpdatedData(
                    {
                        url:catEndpointService.edhIntransitDetails,
                        method:'post',
                        dealerCode:$scope.dealerCode, 
                        productCode:$scope.productCode}).then(
                    function(res){
                        $scope.isloaderOn=false;
                        res?(angular.copy(res,$scope.EDHData)):'';
                    },function(res){
                        $scope.errObj=res;
                        $scope.isloaderOn=false;
                    }
                );
            }
            $scope.loadData();
        }]
    }
}])