
catApp.filter('unique', function() {
    return function (arr, field) {
        var o = {}, i, l = arr.length, r = [];
        for(i=0; i<l;i+=1) {
          o[arr[i][field]] = arr[i];
        }
        for(i in o) {
          r.push(o[i]);
        }
        return r;
    };
})
catApp.directive('productInventoryCountDirective',['httpService','catEndpointService',function(httpService,catEndpointService) {
    return {
       restrict:'A',
       templateUrl:'/_ui/responsive/cat/templates/productInventoryCount.html',
       scope:{
           dealerCode: '@',
           productCode: '@',
           tabName:'@'
       },
       compile: function(tElem, attrs){
           return function(scope,elem,attrs){
               if(!!attrs.dealerCode){
                   scope.dealerCode = attrs.dealerCode;
               }
               if(!!attrs.productCode){
                   scope.productCode = attrs.productCode;
               }
               if(!!attrs.tabName){
                   scope.tabName = attrs.tabName;
               }
           }
       },
       controller: ['$scope','$filter','$window','pdpTabService','constantService', function($scope,$filter,$window,pdpTabService,constantService){
           
           angular.element(angular.element(".tabs")[0]).on('click',
           function(event){
               var tabName = pdpTabService.getTabName(angular.element(event.target).text());
               if(tabName === $scope.tabName){
                    $scope.loadData();
                    angular.element($window).resize();
                   }
               }
           );
           $scope.locationDropDownModel = [];
           $scope.locationDropDownSettings = { 
               idProperty: 'id',
               template : '<label class="dropdown-value pull-left" for="checkbox{{$index}}">{{option.label}}</label>'+
                            '<label class="billing-address-checkbox pull-right">'+
                            '<input type="checkbox" id="checkbox{{$index}}" ng-checked="isChecked(option)">'+
                            '<span class="checkmark"></span>'+
                          '</label>',
               smartButtonTextProvider: function(selectionArray) { 
                    if(selectionArray.length == $scope.locationDropDownData.length){
                        return 'All'
                    } else {
                        return selectionArray.length + ' Selected';
                    }
                },
                showCheckAll : false,
                showUncheckAll : false 
           };
           $scope.noOfRowsToDisplay = 3;
           $scope.reverse = false;
           $scope.uncheckAllSelected = false;
           $scope.stockData =  [];
           $scope.rowsToDisplay = [];
           $scope.rowsToDisplayFiltered = [];
           $scope.totalDealerRows = [];
           $scope.locationDropDownData = [];
           $scope.locationDropDownDataCopy = [];
           $scope.locationDropDownModelCopy = [];
           $scope.OutOfStockMsg = ACC.localization.pdpMessageOutOfStock;
           $scope.addressLabel=ACC.localization.inventoryAddressText;
           $scope.quantityLabel=ACC.localization.inventoryQuantityText;
           $scope.$watchCollection('stockData', function(stockDataArray) {
            if(stockDataArray.length){
                var DLRArray = [];
                var PDCArray = [];
                var MFRArray = [];
                angular.forEach($scope.stockData,function(rowObject,index){
                    if(rowObject.type == 'DLR' && !!rowObject.address){
                        DLRArray.push(rowObject);
                        if(!!rowObject.children)
                        DLRArray = DLRArray.concat(rowObject.children);
                    } else if (rowObject.type == 'PDC' && !!rowObject.address) {
                        PDCArray.push(rowObject);
                    } else if(rowObject.type == 'MFR' && !!rowObject.address) {
                        MFRArray.push(rowObject);
                    }
                });
                $scope.totalDealerRows = (DLRArray.concat(PDCArray)).concat(MFRArray);
                $scope.rowsToDisplay = $scope.totalDealerRows;
                $scope.rowsToDisplayCopy = angular.copy($scope.rowsToDisplay);
                angular.forEach($scope.totalDealerRows,function(stockObject,index){
                    if(stockObject.type == 'DLR'){
                        $scope.locationDropDownData.push({
                            'id' : stockObject.partyId + '-' + stockObject.address.city + '-' + stockObject.address.streetAddress,
                            'label' : stockObject.address.city + '-' + stockObject.address.streetAddress
                        });
                    }  else if(stockObject.type == 'PDC') {
                        $scope.locationDropDownData.push({
                            'id' : stockObject.partyId + '-' + stockObject.address.city + '-' + stockObject.address.streetAddress,
                            'label' : 'PDC'
                        });
                    } else if(stockObject.type == 'MFR') {
                        $scope.locationDropDownData.push({
                            'id' : stockObject.partyId + '-' + stockObject.address.city + '-' + stockObject.address.streetAddress,
                            'label' : 'Factory'
                        });
                    }
                });
                $scope.locationDropDownData = $filter('unique')($scope.locationDropDownData,"label");
                $scope.locationDropDownDataCopy = angular.copy($scope.locationDropDownData);
                $scope.locationDropDownModel = angular.copy($scope.locationDropDownData);
                $scope.locationDropDownModelCopy = angular.copy($scope.locationDropDownData);
                $scope.maxDropdownModelLength = $scope.locationDropDownDataCopy.length;
            }
           });
          
           $scope.loadData = function(){
               $scope.isloaderOn=true;
               $scope.serialLabel = ACC.localization.serialNumberLabel;
               $scope.inventoryAgeLabel = ACC.localization.inventoryAgeLabel;
               pdpTabService.loadUpdatedData(
                {
                   url:catEndpointService.inventoryCountData.replace('$partyId',$scope.dealerCode)
                   .replace('$configId',$scope.productCode),
                       method:'get'
                    }).then(
                   function(res){
                       $scope.isloaderOn=false;
                       res?(angular.copy(res,$scope.stockData)):'';
                       $scope.stockData = JSON.parse($scope.stockData);
                   },function(res){
                       $scope.errObj=res;
                       $scope.isloaderOn=false;
                   }
               );
           }
           $scope.dropdownEvents = {
                    onItemSelect: function(item) {
                        $scope.locationDropDownModelCopy = angular.copy($scope.locationDropDownModel);
                        $scope.filterDropdownData();
                    },
                    onItemDeselect: function(item) {
                        $scope.locationDropDownModelCopy = angular.copy($scope.locationDropDownModel);
                        $scope.filterDropdownData();
                    },
                    onSelectAll : function(){
                        $scope.locationDropDownModelCopy = angular.copy($scope.locationDropDownDataCopy);
                        if($scope.uncheckAllSelected && $scope.searchText != undefined && $scope.searchText.length){
                            $scope.uncheckAllSelected = false;
                            $scope.rowsToDisplay = $scope.rowsToDisplayCopy;
                        } else if ($scope.searchText == undefined || !$scope.searchText.length) {
                            $scope.uncheckAllSelected = false;
                            $scope.rowsToDisplay = $scope.totalDealerRows;
                        }
                    },
                    onDeselectAll : function(){
                        $scope.locationDropDownModelCopy = [];
                        if(!$scope.uncheckAllSelected && $scope.searchText != undefined && $scope.searchText.length){
                            $scope.rowsToDisplayCopy = angular.copy($scope.rowsToDisplay);
                            $scope.uncheckAllSelected = true;
                            $scope.rowsToDisplay = [];
                        }  else if ($scope.searchText == undefined || !$scope.searchText.length) {
                            $scope.uncheckAllSelected = true;
                            $scope.rowsToDisplay = [];
                        }
                    }
            };
            $scope.dealerSearch = function(tableRow){
                    var address = tableRow.address.streetAddress.toLowerCase();
                    var city = tableRow.address.city.toLowerCase();
                    var dealerCode = tableRow.partyId.toLowerCase();
                    var searchText = $scope.searchText || '';
                    var searchResult = !!((address.indexOf(searchText.toLowerCase()) !== -1 || 
                    city.indexOf(searchText.toLowerCase() || '') !== -1 ||
                    dealerCode.indexOf(searchText.toLowerCase()|| '') !== -1
                    ));
                    return searchResult;
            }

            $scope.$watch('searchText',function(value){
                $scope.locationDropDownData = angular.copy($scope.locationDropDownDataCopy);
                if(value != undefined){
                    $scope.locationDropDownModel = ($scope.locationDropDownModel.length == $scope.maxDropdownModelLength) ? angular.copy($scope.locationDropDownDataCopy) : angular.copy($scope.locationDropDownModelCopy);
                    $scope.locationDropDownData = $scope.locationDropDownData.filter(function(item){
                        var item = item.id.toLowerCase();
                        var searchString = value.toLowerCase();
                        return (item.indexOf(searchString) !== -1);
                    });
                    $scope.locationDropDownModel = $scope.locationDropDownModel.filter(function(item){
                        var item = item.id.toLowerCase();
                        var searchString = value.toLowerCase();
                        return (item.indexOf(searchString) !== -1);
                    });
                }
                $scope.filterDropdownData();
            })

           $scope.filterDropdownData = function(){
                $scope.rowsToDisplay = $scope.totalDealerRows;
                var filterArray = [];
                angular.forEach($scope.locationDropDownModel,function(object,index){
                    var dlrCode = object.id.split("-")[0];
                    var city = object.id.split("-")[1];
                    var streetAddress = object.id.split("-")[2];
                    var filterString = (dlrCode + '-' + city + '-' + streetAddress).toLowerCase();
                    filterArray.push(filterString);
                });
                $scope.rowsToDisplay = $scope.rowsToDisplay.filter(function(item) {
                    var searchString = (item.partyId + '-' + item.address.city + '-' + item.address.streetAddress).toLowerCase();
                    return filterArray.indexOf(searchString) !== -1;
                });
                $scope.uncheckAllSelected = ($scope.rowsToDisplay.length) ? false : true;
           }
           $scope.showMoreData = function(){
                $scope.noOfRowsToDisplay = $scope.totalDealerRows.length;
                $scope.filterDropdownData();
           }
           $scope.showLessData = function(){
                $scope.noOfRowsToDisplay = 3;
                $scope.filterDropdownData();
           }
           $scope.sortData = function(){
                $scope.reverse = !$scope.reverse;
                $scope.rowsToDisplay = $filter('orderBy')($scope.rowsToDisplay,'count',$scope.reverse);
           }
           $scope.loadData();
       }]
   }
}])
