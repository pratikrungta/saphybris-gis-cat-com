catApp.directive('reorderCartTableDirective',['$rootScope', function($rootScope){
	return {
		restrict: 'E',
        scope: {
        	reorderCartData: '=',
            poNumberValuesArray : '=',
            poNumberCountArray : '=',
            laneType: '=',
            laneErrors: '='
        },
        templateUrl: '/_ui/responsive/cat/templates/reorderCartTableDirective.html',
        compile: function(tElem, attrs) {
           return function($scope, $elem, $attrs){
           }
        },
        controller: ['$scope', '$rootScope', '$window', 'catEndpointService', 'httpService', 'reorderCartService', 'constantService',function($scope, $rootScope, $window, catEndpointService, httpService, reorderCartService, constantService) {
            $scope.defaultDownloadImage = constantService.defaultDownloadImage;
            $scope.totalPONosArray = [];
            $scope.hybrisPoErrorsArr = [];
            $scope.hybrisPoErrorsMsg = "";
            $scope.$watch('reorderCartData', function(value) {
                if(value.length){
                    $scope.reorderCartData = value;
                }
            });
            $scope.$watch('poNumberValuesArray', function(value) {
                if(value.length){
                    $scope.poNumberValuesArray = value;
                    $rootScope.$broadcast('poNumberValuesArray', $scope.poNumberValuesArray);
                }
            });
            $scope.$watch('poNumberCountArray', function(value) {
                if(value.length){
                    $scope.poNumberCountArray = value;
                }
            });
            $rootScope.$on("updateHybrisPoErrorsArr", function(event, errorPOs){
                $scope.hybrisPoErrorsArr = errorPOs;
                $scope.duplicateErrorPONumbers();
                // console.log("$scope.reorderCartData: ", $scope.reorderCartData);
                // console.log("$scope.hybrisPoErrorsArr: ", $scope.hybrisPoErrorsArr);
            });

            $scope.Prevent = function(event) {
                if (event.target.value == '0') {
                    event.target.value = '1';
                }
            }
            $scope.getFormattedText = constantService.getTwoDigitFormattedText;

            $scope.showOutOfCount = function(configData) {
                return (configData.poNumberValuesArray.purchaseOrderErrorSet.errorMsg
                                || configData.poNumberValuesArray.filledPoCount != 0);
            }

            $scope.getInfoIconClass = function(configData) {
               if(configData.poNumberValuesArray.purchaseOrderErrorSet.errorMsg){ 
                    return 'po-count-img-empty'
                }else if(configData.poNumberValuesArray.filledPoCount === configData.poNumberValuesArray.purchaseOrderNumberSet.length){
                    return 'po-count-img-full';
                }else{
                    return 'po-count-img-not-full'
                }
            }

            $scope.updateReorderCartData = function(operation,event,entryNo, currentQuantity){
                var qty = '';
                if(!$(event.target).val()){
                    event.target.value = '1';
                }
                if($(event.target).hasClass("quantity-box")){
                    qty = $(event.target).val();
                } else {
                    qty = $(event.target).siblings(".quantity-box").val();
                }
                if(qty != ''){
                    qty = Number(qty);
                    if (operation === "increment")  { 
                        qty = Number(qty) + 1;  
                    } else if (operation === "decrement" && qty !== 1) {  
                        qty = Number(qty) - 1;
                    } else if(operation === "") {
                        qty = Number(qty);
                    }
                    // if(qty > constantService.maxOrderLimit){ // to keep the restriction on cart page
                    //     qty = constantService.maxOrderLimit;
                    // }
                    var updateReorderCartURL = catEndpointService.reorderUpdateCartProductEndPoint + '?entryNumber=' + entryNo + '&qty=' + qty;
                    angular.element("#pageLoader").show();
                    httpService.get(updateReorderCartURL).then(function(response) {
                        angular.element("#pageLoader").hide();
                        $scope.cartDataFromResponse = response.data;
                        if($scope.cartDataFromResponse.entries != undefined && $scope.cartDataFromResponse.entries != null && $scope.cartDataFromResponse.entries.length){
                            var returnedObject = reorderCartService.createReorderCartData($scope.cartDataFromResponse, operation, $scope.reorderCartData, entryNo, currentQuantity);
                            $scope.reorderCartData = returnedObject.reorderCartData;
                            $scope.poNumberCountArray = returnedObject.poNumberCountArray;
                            $scope.poNumberValuesArray = $scope.updatePONumberValues();
                            $rootScope.$broadcast('updatedCartResponse', {obj: $scope.cartDataFromResponse,updateFlag: false});
                            $rootScope.$broadcast('poNumberValuesArray', $scope.poNumberValuesArray);
                        } else {
                            
                            $scope.isEmptyCart = true;
                            $rootScope.$broadcast('checkEmptyCart', $scope.isEmptyCart);
                        }
                        ACC.minicart.refreshMinicartCountNum();
                        $scope.duplicateErrorPONumbers();
                    }, function(error){
                    });
                }
            };

            $scope.deleteReorderCartData = function(entryNo){
                var deleteReorderCartURL = catEndpointService.reorderDeleteCartProductEndPoint + '?entryNumber=' + entryNo;
                httpService.get(deleteReorderCartURL).then(function(response) {
                    $scope.cartDataFromResponse = response.data;
                    if($scope.cartDataFromResponse.entries != undefined && $scope.cartDataFromResponse.entries != null && $scope.cartDataFromResponse.entries.length){
                        var returnedObject = reorderCartService.createReorderCartData($scope.cartDataFromResponse);
                        var currentSalesModels = Object.keys($scope.reorderCartData), updatedSalesModels = Object.keys(returnedObject.reorderCartData);

                        if(currentSalesModels.length != updatedSalesModels.length){
                            var deletedSaleModel = currentSalesModels.filter(function(i) {return updatedSalesModels.indexOf(i) < 0;})
                            if(deletedSaleModel && deletedSaleModel.length){
                                $scope.poNumberValuesArray.splice(currentSalesModels.indexOf(deletedSaleModel[0]), 1);
                            }
                        }
                        $scope.reorderCartData = updatePreviousPOs($scope.reorderCartData, returnedObject.reorderCartData);
                        $scope.duplicateErrorPONumbers();
                        $scope.poNumberCountArray = returnedObject.poNumberCountArray;
                        $scope.poNumberValuesArray = $scope.updatePONumberValues();
                        $rootScope.$broadcast('updatedCartResponse', {obj: $scope.cartDataFromResponse,updateFlag: true});
                        $rootScope.$broadcast('poNumberValuesArray', $scope.poNumberValuesArray);
                    } else {
                        $scope.isEmptyCart = true;
                        $rootScope.$broadcast('checkEmptyCart', $scope.isEmptyCart);
                    }
                    ACC.minicart.refreshMinicartCountNum();
                }, function(error){
                });
            };

            var updatePreviousPOs = function(prevCartData, currentCartData) {
                _.each(currentCartData, function(v, k){
                    _.each(v, function(val, idx){
                        val.poNumberValuesArray.purchaseOrderNumberSet = _.map(val.poNumberValuesArray.purchaseOrderNumberSet, function(poNum, index){
                            return prevCartData[k][idx].poNumberValuesArray.purchaseOrderNumberSet[index];
                        });
                    })
                });
                return currentCartData;
            };

            $scope.calculateErrorPONumbers = function(poNumber){
                if(poNumber === "") {
                    return false;
                }
                var count = 0;
                count = $scope.totalPONosArray.filter(function(val, key){
                    return (val === poNumber);
                });
                return (count.length > 1) || ($scope.hybrisPoErrorsArr.indexOf(poNumber) != -1);
                
                //var poNumber = $scope.poNumberValuesArray[parentIndex].purchaseOrderNumberSet[childIndex];
                //return $scope.poNumberValuesArray[parentIndex].purchaseOrderErrorSet.errorNos.indexOf(poNumber) > -1;
            }

            $scope.broadcastTextBoxFocus = function(){
                $rootScope.$broadcast('broadcastTextboxFocus','textbox focused');
            }
        
            $scope.duplicateErrorPONumbers = function(poNumber, entryData){
                $scope.totalPONosArray = [];
                angular.forEach($scope.reorderCartData, function(value, pIndex){
                    angular.forEach(value, function(data, key){
                        $scope.totalPONosArray = $scope.totalPONosArray.concat(data.poNumberValuesArray.purchaseOrderNumberSet);
                    })
                });
                var duplicateArray = $scope.checkDuplicateArrayEntries(_.compact($scope.totalPONosArray));
                var poLengthExceedingArray = $scope.checkPONumberLength(_.uniq($scope.totalPONosArray));
                var specialCharPOArray = $scope.checkSpecialCharPO(_.uniq($scope.totalPONosArray));
                _.each($scope.reorderCartData, function(v){
                    _.each(v, function(val){
                        var  dupList = getIndexsOfFirstArrayValuesInSecond(val.poNumberValuesArray.purchaseOrderNumberSet, duplicateArray);
                        var  lengthList = getIndexsOfFirstArrayValuesInSecond(val.poNumberValuesArray.purchaseOrderNumberSet, poLengthExceedingArray);
                        var  specialCharList = getIndexsOfFirstArrayValuesInSecond(val.poNumberValuesArray.purchaseOrderNumberSet, specialCharPOArray);
                        var  hybrisErrList = getIndexsOfFirstArrayValuesInSecond(val.poNumberValuesArray.purchaseOrderNumberSet, $scope.hybrisPoErrorsArr);
                        
                        val.poNumberValuesArray.purchaseOrderErrorSet.errorMsg = [];
                        if(dupList.length){
                            val.poNumberValuesArray.purchaseOrderErrorSet.errorMsg.push("Please enter a unique PO number.");
                        }
                        if(lengthList.length){
                            val.poNumberValuesArray.purchaseOrderErrorSet.errorMsg.push("PO number cannot exceed 12 characters.");
                        }
                        if(specialCharList.length){
                            val.poNumberValuesArray.purchaseOrderErrorSet.errorMsg.push("PO number cannot contain special characters.");
                        }
                        if(hybrisErrList.length){
                            val.poNumberValuesArray.purchaseOrderErrorSet.errorMsg.push("This PO number is already used, please enter a different PO number to proceed.");
                        }

                        val.poNumberValuesArray.purchaseOrderErrorSet.errorMsg = val.poNumberValuesArray.purchaseOrderErrorSet.errorMsg.join(" ");

                        var errIndex = dupList.concat(lengthList, specialCharList, hybrisErrList);
                        val.poNumberValuesArray.purchaseOrderErrorSet.errorNos = errIndex;

                        val.poNumberValuesArray.filledPoCount = val.poNumberValuesArray.purchaseOrderNumberSet.length - (errIndex.length + getEmptyPosCount(val.poNumberValuesArray.purchaseOrderNumberSet));
                        val.poNumberValuesArray.filledPoCount = val.poNumberValuesArray.filledPoCount < 0 ? 0 : val.poNumberValuesArray.filledPoCount;
                    });
                });
                $rootScope.$broadcast('duplicatePOError', duplicateArray.length);
                $rootScope.$broadcast('lengthExceedingPOError', poLengthExceedingArray.length);
                $rootScope.$broadcast('specialCharsPOError', specialCharPOArray.length);
                $rootScope.$broadcast('updateReorderCartData', $scope.reorderCartData);
            }

            function getEmptyPosCount(arr) {
                return arr ? (arr.filter(function(num) { return num == ""; })).length : 0;
            }
            function getIndexsOfFirstArrayValuesInSecond(arr1, arr2) {
                if(!arr1.length || !arr2.length){
                    return [];
                }

                return _.chain(arr1)
                        .map(function(val, idx){ 
                            return  arr2.indexOf(val) != -1 ? idx : null;
                        })
                        .filter(function(val) {
                            return (val || val == 0);
                        })
                        .value();
            }
        
            $scope.checkDuplicateArrayEntries = function(arr){
                return _.chain(arr)
                        .filter(function(v){
                            return (arr.indexOf(v) !== arr.lastIndexOf(v));
                        })
                        .uniq()
                        .value();
            }
            
            $scope.updatePONumberValues = function(){
                angular.forEach($scope.poNumberValuesArray,function(value,parentIndex){
                    var diff = (value.purchaseOrderNumberSet.length - $scope.poNumberCountArray[parentIndex].poNumbersCount);
                    if(diff === Math.abs(diff)){
                        value.purchaseOrderNumberSet.splice($scope.poNumberCountArray[parentIndex].poNumbersCount, diff)
                    } else {
                        diff = Math.abs(diff);
                        for(var i = 0; i < diff; i++){
                            value.purchaseOrderNumberSet.push("");    
                        }
                        // value.purchaseOrderNumberSet = value.purchaseOrderNumberSet.concat(Array(Math.abs(diff)).fill("")); // can use new Array(Math.abs(diff)).join(".").split(".")
                    }
                });
                return $scope.poNumberValuesArray;
            }

            $scope.checkPONumberLength = function(arr){
                return _.chain(arr)
                        .filter(function(poNumber){
                            return (poNumber.length > 12);
                        })
                        .uniq()
                        .value();
            }

            $scope.checkSpecialCharPO = function(arr){
                return _.chain(arr)
                        .filter(function(poNumber){
                            return /[^a-zA-Z0-9]/.test(poNumber);
                        })
                        .uniq()
                        .value();
            }
        
            $scope.getPONumberArray = function(num) {
                return new Array(num);   
            };  

            $scope.arrayContainsAnotherArray = function(arr1, arr2){
                for(var i = 0; i < arr1.length; i++){
                  if(arr2.indexOf(arr1[i]) === -1)
                     return false;
                }
                return true;
            }
        }]
    };
}]);
