catApp.directive('reorderCartRow', function() {
    return {
        restrict: 'EA',
        replace: true,
        scope: {
            
        },
        templateUrl: '/_ui/responsive/cat/templates/reorderCartRow.html',
        compile: function(tElem, attrs) {
            //do optional DOM transformation here
            return function($scope, $elem, $attrs) {
            };
        },
        controller: ['$scope', '$window', 'catEndpointService', 'httpService', 'catImageFinder', function($scope, $window, catEndpointService, httpService, catImageFinder) {
            
        }]
    };
});