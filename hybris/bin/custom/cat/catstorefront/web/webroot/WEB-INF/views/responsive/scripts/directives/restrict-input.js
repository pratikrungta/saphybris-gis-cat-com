catApp.directive('notAllowTo', [function() {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) { 
            element.bind('keypress', function (event) {
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                if (key.match(attrs.notAllowTo)) {
                    event.preventDefault();
                }
            });
        }
    }
}]);
