catApp.directive('restrictModelInput', ["$window","$timeout",function($window,$timeout) {
    return {
        require : '?ngModel',
        link :{
            post:function(scope, element, attrs, ngModelCtrl) {
              var regex = new RegExp(attrs.restrictModelInput),
                  ref_value='',
                  isQualifiedValidations = function(input){
                        if(attrs.max+''){
                             if(/\$/.test(attrs.max)){
                                 attrs.max = (attrs.max.replace(/\$/g,'').replace(/^0/,'').replace(/\,/g,''));
                                 attrs.max *= parseFloat(attrs.quant);
                            }
                            if(/\%/.test(attrs.max)){
                                attrs.max = attrs.max.replace(/\%/,'');
                            }
                            if (parseFloat(attrs.max,10)<parseFloat(input)){
                                return false;
                            }
                        }
                    return true;
              };

            if (!ngModelCtrl) {
                return;
            }
            
            ngModelCtrl.$parsers.push(function(value) {
                var temp="";
                if (angular.isUndefined(value)) {
                     value = "";
                }
                    temp = value;
                    if(regex.test(temp) && isQualifiedValidations(temp)){
                        ref_value = temp;
                    }else{
                        temp = ref_value;
                    }
                    if(value===""){
                        ref_value = "";temp="";
                    }

                if (value !== temp) {
                    ngModelCtrl.$setViewValue(temp);
                    ngModelCtrl.$render();
                }
                return temp;
            });
        }}
    };
}])