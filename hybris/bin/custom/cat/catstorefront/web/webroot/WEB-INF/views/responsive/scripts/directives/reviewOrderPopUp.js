catApp.directive('reviewOrderPopUp',function(){
	return {
		restrict: 'E',
        replace: true,
        scope: {
        	alertHeader: '@',
            alertButtonContent: '@',
            id: '@',
            ok:'@',
            productInfoText: '@',
            productInfoValue: '@',
            homePageLink: '@',
            viewOrderHistroyLink: '@',
            orderNumber: '@',
            configurationId: '@',
            quantity: '@',
            quantityUnits: '@',
            type: '@',
            errorInfo: '@',
            errorPos: '=',
            productType: '@',
            resultPo: '=',
            isTruckloadSubmitSuccess : '@',
            totalTruckload : '@',
            totalTruckloadUnits : '@',
            totalTruckloadPrice : '@',
            splitOrderCount: '@'
        }, 
        templateUrl: '/_ui/responsive/cat/templates/reorderPopUpTemplate.html',
        compile: function(tElem, attrs) {
            return function(scope, elem, attrs) {
                if(!!attrs.isTruckloadSubmitSuccess){
                    scope.isTruckloadSubmitSuccess = attrs.isTruckloadSubmitSuccess;
                }
                if(!!attrs.totalTruckload){
                    scope.totalTruckload = attrs.totalTruckload;
                }
                if(!!attrs.totalTruckloadUnits){
                    scope.totalTruckloadUnits = attrs.totalTruckloadUnits;
                }
                if(!!attrs.totalTruckloadPrice){
                    scope.totalTruckloadPrice = attrs.totalTruckloadPrice;
                }
                if(!!attrs.splitOrderCount) {
                    scope.splitOrderCount = attrs.splitOrderCount;
                }
            };
        },
        controller: ['$scope', '$window', function($scope, $window) {
            $scope.closeCurrentPopUp = function(){
                $("#"+$scope.id).hide();
            }
            $scope.popUpClose = function() {
                $window.location.href = $scope.homePageLink;
            }
            $scope.viewOrderHistory = function(){
                $window.location.href = $scope.viewOrderHistroyLink;
            }
        }]
    };
});
