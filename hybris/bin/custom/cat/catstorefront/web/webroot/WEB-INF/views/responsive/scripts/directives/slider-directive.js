catApp.directive('slider', function() {
    return {
        restrict: 'EA',
        transclude: true,
        replace: true,
        scope: {},
        compile: function(tElem, attrs) {
            //var $ele = angular.element(tElem);
            //$ele.prepend(angular.element(''));
            //$ele.append(angular.element(''));
            return {
                pre: function(scope, elem, attrs, nullCtrl, transcludeFn) {
                    transcludeFn(scope, function(clone) {
                        elem.append(clone);
                    });
                },
                post: function(scope, element, attributes, controller, transcludeFn) {
                    var id = element.attr("id");
                    if (!!id) {
                        $("#" + id).addClass("custom-slider");
                        $("#" + id).owlCarousel({
                            navigation: true,
                            items: 3,
                            navigationText: ["<img class='left-arrow' src='/_ui/responsive/cat/images/left-arrow.png'>", "<img class='right-arrow' src='/_ui/responsive/cat/images/right-arrow.png'>"],
                            itemsDesktop: [1199, 3],
                            itemsDesktopSmall: [979, 3],
                            itemsTablet: [768, 2],
                            itemsTabletSmall: false,
                            itemsMobile: [479, 2],
                            slideSpeed: 800,
                            rewindSpeed: 1000,
                            rewindNav: false,
                            scrollPerPage: true
                        });
                    }
                }
            }
        }
    };
});