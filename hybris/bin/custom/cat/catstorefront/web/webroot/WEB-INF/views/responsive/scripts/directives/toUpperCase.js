catApp.filter('toUpperCase', function() {
    return function(input) {
      input = input || '';
      return input.replace(/\w\S*/g, function(txt){return txt.substr(0).toUpperCase()});
    };
  })