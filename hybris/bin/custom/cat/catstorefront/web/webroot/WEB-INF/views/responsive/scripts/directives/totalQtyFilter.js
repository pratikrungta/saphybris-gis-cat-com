catApp.filter('totalQuantityFilter',function(){
	return function(input){
        var total = 0;
        angular.forEach(input,function(value,index){
            total = total + value;
        });
        return total;
	}
});