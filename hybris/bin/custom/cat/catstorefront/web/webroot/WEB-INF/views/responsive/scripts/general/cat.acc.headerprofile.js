ACC.catHeaderProfile = {
	_autoload: [
		"bindProfileDropDown"
    ],
	bindProfileDropDown: function () {

		var width = $("#profile_drop_down_icon").width()+36,
			ShowingLowStockMsg=function(e){
				$($(".low-stock-order-container")[0]).toggleClass("fade-move-up");
				e.stopPropagation();
			},
			hideLowStockMsg = function(e){
				$($(".low-stock-order-container")[0]).addClass("fade-move-up");
			};
		$("#myDropdown").width(width);
		$("#profile_drop_down_icon").click(function(e){
			$("#myDropdown").toggleClass("show");
			e.stopPropagation();
		});
		$("body").click(function(){
			$("#myDropdown").removeClass("show");
			$($(".low-stock-order-container")[0]).addClass("fade-move-up");

		});
		$($(".bell-counter")[0]).unbind("click",
		ShowingLowStockMsg).bind('click',ShowingLowStockMsg);
		
		//handled for the click on reorder button
		$($(".manage-m-order")[0]).unbind("click",
		hideLowStockMsg).bind("click",hideLowStockMsg);
		
	}
}