ACC.catNavigation = {
    _autoload: ["bindToggleOffcanvas", "constructMenu", "desktopMenu","openCookiePopUp"],
    openCookiePopUp:function(){
        if(!$.cookie('DealersPrivacyAccepted')){
            $("#cookie-popUp").show();
        }
        
    },
    makeCookiesAccepted:function(){
        $.cookie('DealersPrivacyAccepted',true,{path:'/',secure:true});
        $("#cookie-popUp").hide();
    },
    showSidebar: function() {
        ACC.catNavigation.sidebar().show('slide', { direction: 'left' }, 300);
        ACC.catNavigation.overlay().show(0, function() {
            ACC.catNavigation.overlay().fadeTo('500', 0.5);
            $("body").addClass("modal-open");
        });
    },
    hideSidebar: function() {
        ACC.catNavigation.sidebar().hide('slide', { direction: 'left' }, 200, function() {
            ACC.catNavigation.overlay().hide();
            $("body").removeClass("modal-open");
        });
    },
    sidebar: function() {
        return $('#catMobileNavMenu[data-sidebar]');
    },
    overlay: function() {
        return $('#catMobileNavOverlay[data-sidebar-overlay]');
    },
    toggleMobileMenu: function() {
        if (ACC.catNavigation.sidebar().is(':visible')) {
            ACC.catNavigation.hideSidebar();
        } else {
            ACC.catNavigation.showSidebar();
        }
        return false;
    },
    constructMenu: function() {

        var userDiv = $("<div>", { id: "catNavMenuprofile" });
        var oUserInfo = $(".nav__right ul li.logged_in");
        if (oUserInfo && oUserInfo.length === 1) {
            var sUserBtn = oUserInfo[0].innerHTML
            userDiv.html(sUserBtn);
            $("#catNavMenuContainer").append(userDiv);
            $($("#catNavMenuContainer .edit-profile-mobile")[0]).removeClass("hide-important");
        }

        var navDiv = $("<div>", { id: "navigationLinks" });
        var ulDiv = $("<ul>", { id: "mobileNavMenu" });

        var desktopAnalyticsInfo = $("#top-analytics a[title]");
        if (desktopAnalyticsInfo && desktopAnalyticsInfo.length >= 1) {
            var topAnalytics = desktopAnalyticsInfo[0];
            ulDiv.append('<li class="analyticsLink"></li>');
            ulDiv.children(".analyticsLink").html($("#top-analytics a[title]").clone(true));
        }

        var desktopGNSInfo = $("#top-gns-link a[title]");
        if (desktopGNSInfo && desktopGNSInfo.length >= 1) {
            var topGns = desktopGNSInfo[0];
            ulDiv.append('<li class="gnsLink"></li>');
            ulDiv.children(".gnsLink").html($("#top-gns-link a[title]").clone(true));
        }

        var desktopNavInfo = $(".desktop-nav-menu.nav__links--products");
        if (desktopNavInfo && desktopNavInfo.length >= 1) {

            var mobileNaveMenu = desktopNavInfo[0].innerHTML;
            ulDiv.append(mobileNaveMenu);
        }
        navDiv.append(ulDiv);

        
        $("#catNavMenuContainer").append(navDiv);

        var oCartInfo = $("#desktop-mini-cart");
        if (oCartInfo && oCartInfo.length >= 1) {
            ulDiv.append('<li class="mobile-cart-info"></li>');
            var clonedCart = $($("#desktop-mini-cart")[0]).clone(true);
            var text = $($(clonedCart[0]).find(".shoppingCartText")[0]).text(),
                quant = $($(".shoppingCartValueLabel")[0]).text();
            $(clonedCart).empty();
            $(clonedCart).text(text);
            //following mobile cart span which updates the value on reload.
            $(clonedCart).append('<span id="mobile-cart-quant"></span>');
            ulDiv.children(".mobile-cart-info").html(clonedCart);
            $($('#mobile-cart-quant')[0]).text(quant&&quant!=='0'?' ('+quant+') ':'');
        }

        var oOrderHistory = $(".order-history");
        if (oOrderHistory && oOrderHistory.length >= 1) {
            ulDiv.append('<li class="mobile-order-history"></li>');
            ulDiv.children(".mobile-order-history").html($($(".order-history")[0]).clone(true));
        }

        var oQuoteHistory = $(".quote-history");
        if (oQuoteHistory && oQuoteHistory.length >= 1) {
            ulDiv.append('<li class="mobile-quote-history"></li>');
            ulDiv.children(".mobile-quote-history").html($($(".quote-history")[0]).clone(true));
        }

        var countyMapping = $(".county-mapping");
        if (countyMapping && countyMapping.length >= 1) {
            ulDiv.append('<li class="mobile-order-history mobile-county-mapping"></li>');
            ulDiv.children(".mobile-county-mapping").html($($(".county-mapping")[0]).clone(true));
        }

        var signOutDiv = $("<div>", { id: "logout" });
        var oUserLogout = $(".desktop-logout");
        if (oUserLogout && oUserLogout.length >= 1) {
            signOutDiv.html($($(".desktop-logout")[0]).clone(true));
            $("#catNavMenuContainer").append(signOutDiv);
        }
    },
    bindToggleOffcanvas: function() {
        enquire.register("screen and (max-width:" + screenSmMax + ")", {
            match: function() {
                ACC.catNavigation.sidebar().hide();
                ACC.catNavigation.overlay().hide();
                $("#cat-toggle-menu").unbind("click").bind("click", ACC.catNavigation.toggleMobileMenu);
                $("#catMobileNavOverlay").unbind("click").bind("click", ACC.catNavigation.hideSidebar);
            },
            unmatch: function() {
                ACC.catNavigation.sidebar().hide();
                ACC.catNavigation.overlay().hide();
                $("#cat-toggle-menu").unbind("click");
                $("#catMobileNavOverlay").unbind("click");
                $("body").removeClass("modal-open");
            }
        });
    },
    desktopMenu: function() {
        $(".subCat-box").each(function(index, element) {
            var mobileNavDiv = $("<div>", { class: "col-xs-12 desk-menu desk-menu-equipment" });
            var linksContainer = $("<div>",{ class: "col-xs-3 col-sm-3 border-right"});

            var linksLength = $(this).children("a.nav-link-desktop").length;
            var count = 0;
            if (linksLength >= 1) {
                mobileNavDiv.append(linksContainer.clone().addClass("first"));
            } 
            if (linksLength >= 2) {
                mobileNavDiv.append(linksContainer.clone().addClass("second"));
            } 
            if (linksLength >= 3) {
                mobileNavDiv.append(linksContainer.clone().addClass("third"));
            } 
            if(linksLength >= 4) {
                mobileNavDiv.append(linksContainer.clone().addClass("fourth"));
            }
            if($(this).attr('id') == 'sub-New' && $("#sub-New-attachment").length){
                $(this).prepend('<div id="subHeader" class="row subHeader upperCase" style="width:100%"> <span id="equipment" class="equipment-header"> Equipment </span> <span id="attachment" class="attachment-header"> Features </span> </div>');
            }else {
                $(this).prepend('<div id="subHeader" class="row subHeader upperCase" style="width:100%"> Equipment </div>');
            }
            $(this).children("a.nav-link-desktop").each(function(i, subElement){
               $(this).removeClass("col-xs-3").addClass("col-xs-12");
                if(i%4 == 0){
                    mobileNavDiv.find(".first").append($(this));
                }else if(i%4 == 1){
                    mobileNavDiv.find(".second").append($(this));
                }else if(i%4 == 2){
                    mobileNavDiv.find(".third").append($(this));
                }else if(i%4 == 3){
                    mobileNavDiv.find(".fourth").append($(this));
                }
            });
            $(this).append(mobileNavDiv);
        });

        if($("#sub-New-attachment").length){
            $(".subCat-box-attchment").each(function(index, element) {
                var mobileNavDiv = $("<div>", { class: "col-xs-12 desk-menu desk-menu-attachment" });
                var linksContainer = $("<div>",{ class: "col-xs-3 col-sm-3 border-right"});

                var linksLength = $(this).children("a.nav-link-desktop").length;
                mobileNavDiv.append(linksContainer.clone().addClass("first"));
                
                $(this).children("a.nav-link-desktop").each(function(i, subElement){
                   $(this).removeClass("col-xs-3").addClass("col-xs-12");
                    mobileNavDiv.find(".first").append($(this));
                });
                $(".subCat-box").append(mobileNavDiv);
                $(this).remove();
            });

            $("#equipment").hover(function(){
                ACC.catNavigation.showNavTab('equipment', 'attachment');
            });
 
            $("#attachment").hover(function(){
                ACC.catNavigation.showNavTab('attachment', 'equipment');
            });
 
            $(".main-navigation-container #New, .main-navigation-container #Used").hover(function(){
                ACC.catNavigation.showNavTab('equipment', 'attachment');
            });
        }else {
            $(".desk-menu-equipment").addClass('active').show();
            $('#sub-New-attachment').remove();
        }

    },
    createGroupedArray: function(arrList, chunkSize) {
        var chunk = [], i;
        for (i = 0; i < arrList.length; i += chunkSize) {
            chunk.push(arrList.slice(i, i + chunkSize));
        }
        return chunk;
    },
    showNavTab: function(activeId, inActiveId){
        $("#"+activeId).addClass('active');
        $('#'+inActiveId).removeClass('active');
        $(".desk-menu-"+activeId).addClass('active').show();
        $(".desk-menu-"+inActiveId).removeClass('active').hide();
    }
}