ACC.catPDP = {
    _autoload: [
        "bindSpecificationClickEvent","mobileSupportMaterial","dynamicTabHeightControl","readMoreMobile","tabManipulation","orientationChange"
    ],
    bindSpecificationClickEvent: function() {
        $('.product-classifications .table').each(function(e) {
            $(this).find('tr.units_metrics_metric:visible:odd').css('background', '#f4f4f4');
            $(this).find('tr.units_metrics_us:visible:odd').css('background', '#f4f4f4');
        });
        $(".accessories-overview-link").click(function() {
        	window.location.href="/accessories";
        });
        $(".units-us").click(function() {
            $($(".units-metrics")[0]).removeClass("active");
            $($(".units-us")[0]).addClass("active");

            $($(".units-metrics")[1]).removeClass("active");
            $($(".units-us")[1]).addClass("active");


            $(".units_metrics_us").show();
            $(".units_metrics_metric").hide();
            $(".units_metrics_metric").removeClass("border-color-even");
            $(".units_metrics_us").addClass("border-color-even");
            //$('.units_metrics_metric:visible:odd').css('background', '#f4f4f4');
            //$('.units_metrics_us:visible:odd').css('background', '#f4f4f4');

            $('.product-classifications .table').each(function(e) {
                $(this).find('tr.units_metrics_metric:odd').css('background', '#f4f4f4');
                $(this).find('tr.units_metrics_us:odd').css('background', '#f4f4f4');
            });

        });
        $(".units-metrics").click(function() {
            $($(".units-metrics")[0]).addClass("active");
            $($(".units-us")[0]).removeClass("active");

            $($(".units-metrics")[1]).addClass("active");
            $($(".units-us")[1]).removeClass("active");

            $(".units_metrics_us").hide();
            $(".units_metrics_metric").show();
            $(".units_metrics_metric").addClass("border-color-even");
            $(".units_metrics_us").removeClass("border-color-even");
            $('.product-classifications .table').each(function(e) {
                $(this).find('tr.units_metrics_metric:odd').css('background', '#f4f4f4');
                $(this).find('tr.units_metrics_us:odd').css('background', '#f4f4f4');
            });
        });
        $(".units-us").trigger('click');
    },
    mobileSupportMaterial: function(){
        $(".single-tool").each(function(){
            var name = $(this).data("tool-name");
            var link= $(this).data("navigation-url");
            if (name && link) {
                $("#mobile-support-material").append("<a class='single-tool-mobile col-xs-12 cat-buttons secondary-btn' href='"+link+"'>"+name+"</a>");
            }
        });
    },
    dynamicTabHeightControl:function(){
        var heightOfLongestEl=0;
        if($("[dynamic-tab-height-control]").attr("dynamic-tab-height-control")+''==="true" && window.innerWidth > window.innerHeight){
            $("[dynamic-tab-height-control] ul.tabs-list li").each(function(index,el){
                try{
                    if(heightOfLongestEl < $(el).outerHeight()){
                        heightOfLongestEl = $(el).outerHeight();
                    }
                    $(el).height("inherit");
                }catch(e){
                    //issue in reading the height
                }
                
            });
            $("[dynamic-tab-height-control] .tabs-list").height(heightOfLongestEl);
    }
    },
    readMoreMobile: function(){
        var elem = document.getElementById("pdp-description");

        var descLen = elem ? elem.innerHTML : ' ';

        if(elem && (descLen.length < 350)){
            $("#description-read-more").hide();
        } 
        $("#description-read-more").click(function() {
           var buttonText  = $("#description-read-more").text();
            if (buttonText == "View More") {
              $("#description-read-more").text("View Less");
              $("#pdp-description").removeClass("readMoreClass");
            } else {
              $("#description-read-more").text("View More");
              $("#pdp-description").addClass("readMoreClass");
            }
          });
    },
    tabManipulation: function(){
        $(".right-content .js-tabs >ul").find('li:first').hide();
        $(".right-content .js-tabs >ul >li:nth-child(2)").children("a:first").trigger("click");
    },
    orientationChange: function(){
        var that = this;
        window.addEventListener("resize", function() {
            // Get screen size (inner/outerWidth, inner/outerHeight)
            if(window.innerWidth > window.innerHeight){
                that.dynamicTabHeightControl();
            }

        }, false);
    }
}