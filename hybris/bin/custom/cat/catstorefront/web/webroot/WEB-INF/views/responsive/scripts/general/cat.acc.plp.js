ACC.plp = {
	_autoload: ["constructMobileMenu","bindMobileClickEvents"],
	constructMobileMenu: function(){
		var paramsDiv = $('#product-facet');
        if (paramsDiv && paramsDiv.length === 1) {
            var paramsConent = paramsDiv[0].innerHTML;
            $("#catPLPMobileNavMenu").append(paramsConent);
        }
	},
	bindMobileClickEvents: function(){
		 enquire.register("screen and (max-width:" + screenSmMax + ")", { 
		 	 match: function() { 
		 	 	$("#plp-mobile-filters").unbind("click").bind("click", function(){
					ACC.plp.showSidebar();
					window.plpSliderRefreshFunction();
		 	 		});
		 	 		$("#catSearchMobileNavOverlay").unbind("click").bind("click", function(){
						ACC.plp.hideSidebar();
						window.plpSliderRefreshFunction();
		 	 		});
		 	 },
		 	 unmatch: function() { $(".mobile-content").hide();}
		 });
	},
	showSidebar: function() {
        ACC.plp.sidebar().show('slide', { direction: 'right' }, 400);
        ACC.plp.overlay().show(0, function() {
            ACC.plp.overlay().fadeTo('500', 0.5);
            $("body").addClass("modal-open");
        });
    },
    hideSidebar: function() {
        ACC.plp.sidebar().hide('slide', { direction: 'right' }, 400, function() {
			ACC.plp.overlay().hide();
			$("body").removeClass("modal-open");
        });
    },
	overlay: function() {
        return $('#catSearchMobileNavOverlay[data-sidebar-overlay]');
	},
	sidebar: function() {
        return $('#catPLPMobileNavMenu[data-sidebar]');
    }
}