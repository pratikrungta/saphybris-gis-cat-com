ACC.productCompare = {
    _autoload:[],
    productComparisonScroll : function(){
        var headingTop = $('.panel.panel-default > .panel-collapse.in').offset().top;
        var visibleTop = $(window).scrollTop();
        if (headingTop < visibleTop) {
            $('html,body').animate({
                scrollTop: headingTop - 200
            }, 500);
        }
    }
}