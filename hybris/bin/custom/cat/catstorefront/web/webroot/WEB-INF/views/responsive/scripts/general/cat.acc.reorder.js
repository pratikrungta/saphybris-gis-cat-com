ACC.reorder={
    _autoload:["constructMobileMenu","bindMobileClickEvents"],
    constructMobileMenu:function(){
        var paramsDiv = $('#rpc-facet-container');
        if (paramsDiv && paramsDiv.length === 1) {
            var paramsConent = paramsDiv[0].innerHTML;
            $("#manageOrdersMobileMenu").append(paramsConent);
        }
    },
    bindMobileClickEvents: function(){
        enquire.register("screen and (max-width:" + screenSmMax + ")", { 
             match: function() { 
                 $("#reorder-mobile-filters").unbind("click").bind("click", function(){
                    ACC.reorder.showSidebar();
                   //window.plpSliderRefreshFunction();
                     });
                     $("#manageOrderMobileNavOverlay").unbind("click").bind("click", function(){
                       ACC.reorder.hideSidebar();
                       //window.plpSliderRefreshFunction();
                     });
             },
             unmatch: function() { $(".mobile-content").hide();}
        });
   },
   showSidebar: function() {
    ACC.reorder.sidebar().show('slide', { direction: 'right' }, 400);
    ACC.reorder.overlay().show(0, function() {
        ACC.reorder.overlay().fadeTo('500', 0.5);
        $("body").addClass("modal-open");
    });
},
hideSidebar: function() {
    ACC.reorder.sidebar().hide('slide', { direction: 'right' }, 400, function() {
        ACC.reorder.overlay().hide();
        $("body").removeClass("modal-open");
    });
},
overlay: function() {
    return $('#manageOrderMobileNavOverlay[data-sidebar-overlay]');
},
sidebar: function() {
    return $('#manageOrderMobileNavMenu[data-sidebar]');
}
}