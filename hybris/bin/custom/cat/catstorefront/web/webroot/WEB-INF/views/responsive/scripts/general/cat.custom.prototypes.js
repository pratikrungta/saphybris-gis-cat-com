Array.prototype.chunk = function ( n ) {
    if ( !this.length ) {
        return [];
    }
    return [ this.slice( 0, n ) ].concat( this.slice(n).chunk(n) );
};
String.prototype.titleCase = function(){
    var wordsSplittedBySpaces=this.split(' ');
    return wordsSplittedBySpaces.map(function(word){
        return word.charAt(0).toUpperCase()+word.substr(1).toLowerCase();
    }).join(' ');
        
};