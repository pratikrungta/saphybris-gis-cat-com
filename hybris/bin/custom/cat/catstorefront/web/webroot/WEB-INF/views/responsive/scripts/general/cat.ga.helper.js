ACC.gaHelper=(function(){
        window.dataLayer=window.dataLayer || [];
			dataLayer.push({ 
			'event': 'dealerInfo', 
			'attributes': {  
			'dealerName': $("#GADealerName").text(), 
			'pagePath': document.location.pathname 
            }
        });
    return{
        cuvTruckLoadEventName:'truckLoadOrderPlaced',
        cuvEventName:'CUVOrderPlaced',
        bcpEventName:'BCPOrderPlaced',
        utvPageViewUrl:'/checkout/submitReOrder',
        bcpPageViewUrl:'/checkout/placeReOrder',
        quoteSubmitPageViewUrl:'/quote/submit',
        truckLoadUTVPageViewUrl:'/truckload/checkout/submitReOrder',
        bcpCartPageUrl:'/cart/reOrderCart',
        
        bcpUserEventName:'IM',
        cuvUserEventName:'CUV',
        spUserEventName:'SP',
        bcpOrderEventName:'BCP REORDER',
        cuvOrderEventName:'CUV ORDER',
        cuvTruckLoadOrderEventName:'CUV TRUCKLOAD',
        spQoutationEventName:'QUOTATION',
        comparissionEvent:'PRODUCT COMPARISON',
        searchEvent:'SEARCH',
        homepageEvent:'HOME PAGE',
        viewProfileEvent:'VIEW  PROFILE',
        orderDetailSEvent:'ORDER DETAILS',
        orderHistoryEvent:'ORDER HISTORY',
        productDetailsViewEvent:'PRODUCT DETAILS VIEW',

        hitPageView:function(pageViewUrl){
            ga('send', 'pageview', pageViewUrl);
        },
        roleFeatureInfo:function(userRole,functionality){
            var roleObj={event:'Role-Group-Trigger'},index;
            switch(userRole){
                case 'IM':index=1;break;
                case 'CUV':index=2;break;
                case 'SP':index=3;break;
                default:index=null;
            }
            roleObj['Content-Group-Index'] = index;
            roleObj['Content-Group-Functionality'] = functionality;
            dataLayer.push(roleObj); 
        },
        formProductsArrayForGA:function(category,quantity,name){
            return{
                'category':category,
                'quantity':quantity,
                'name':name
            };
        },
        pushingOrderToGA:function(productsPurchased,orderCode,eventName,affliationCode){
            dataLayer.push({
                'event':eventName,
                'ecommerce':{
                    'purchase':{
                        'actionField':{
                           'id':orderCode,
                           'affiliation':affliationCode
                        },
                        'products':productsPurchased//Array of [{name,quantity,category}]
                    }
                }
            });
    }
    }
    
}());
