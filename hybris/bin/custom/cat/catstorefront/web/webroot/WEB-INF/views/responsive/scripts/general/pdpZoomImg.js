$(function() {

    $(".image-gallery__image").click(function() {
        var imgIndex = $(".image-gallery__image .owl-controls .owl-pagination .owl-page.active").index();
        if(!(imgIndex > -1)){
            imgIndex = 0;
        }
        $("#catImage").attr("src",$(".image-gallery__image .owl-item .item img.lazyOwl")[imgIndex].src);
        $("#catImageModal").show();
    });

    $("#close").click(function() {
        $("#catImageModal").hide();
    });

    $("#catImageModal").click(function() {
        $("#catImageModal").hide();
    });

});