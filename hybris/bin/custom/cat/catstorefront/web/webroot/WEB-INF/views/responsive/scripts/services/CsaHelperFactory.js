catApp.factory('CsaHelperFactory', [function(){
  var csaHelper={};

      csaHelper.AreAllCsasApplied = function(csaItems){
            var i = csaItems.length;
                    while(i--){
                        if(!csaItems[i].isCsaDisabled){
                            return !!0;
                        }
                    }
                    return !!1;
                };
      
      csaHelper.register = function(rawObj){
          return {
              entryNumber:rawObj&&rawObj.entryNumber||'',
              basePrice:rawObj&&rawObj.basePrice.value||0,
              totalPrice:rawObj&&rawObj.totalPrice.formattedValue||"$0.00",
              qty:rawObj&&rawObj.quantity||0,
              old_qty:rawObj&&rawObj.quantity||0,
              csaOption:rawObj&&rawObj.configurationInfos.length
                        &&rawObj.configurationInfos[1].configurationValue||"",
              isCsaDisabled:this.qty>0?!!0:!!1,
              hours:rawObj&&rawObj.configurationInfos.length
                    &&rawObj.configurationInfos[0].configurationValue||0,
              years:rawObj&&rawObj.configurationInfos.length
                    &&rawObj.configurationInfos[2].configurationValue||0,
              increment:function(csaObj){
                  this.qty+=1;
                  this.changeInCsaQty(csaObj);
              },
              decrement:function(csaObj){
                  this.qty>1?(this.qty-=1):'';
                  this.changeInCsaQty(csaObj);
              },
              changeInCsaQty:function(csaObj){
                this.qty = parseInt(this.qty,10)||0;
                csaObj.totalCsaQty+=(this.qty-this.old_qty);

                if(this.isApplyDisabled()&&csaObj.totalEppQty>=csaObj.productQty){
                    this.isCsaDisabled=!!1;
                  }

                  if(this.qty !== this.old_qty){
                    if(csaObj.totalCsaQty>csaObj.productQty){
                        csaObj.totalCsaQty-=(this.qty-this.old_qty);
                          this.qty = this.old_qty;
                      }else{
                            this.old_qty = this.qty;
                            this.isCsaDisabled=!!0;
                      }
                }
                  /**
                   * Below code is to check the add button's disable status
                   * If an item doesn't have the "entry number" it is a new one, based on that disable condition has
                   * been modified.
                   */
                  if(csaObj.totalCsaQty<csaObj.productQty&&csaObj.csaItems[csaObj.csaItems.length-1].entryNumber){
                    csaObj.isNewBtnDisable = !!0;
                  }
                  if(csaObj.totalCsaQty===csaObj.productQty){
                    csaObj.isNewBtnDisable = !!1;
                  }
              },
              isApplyDisabled:function(){
                  if(this.isCsaDisabled){
                      return !!1;
                  }
                  if(!(parseInt(this.basePrice,10)
                        &&parseInt(this.qty,10)
                        &&parseInt(this.hours)&&parseInt(this.years)
                        &&this.csaOption)){
                      return !!1;
                  }
                  return !!0;
              },
              setAttributeWithValue:function(attrObj){
                var i;
                    for(i in attrObj){
                        if(attrObj.hasOwnProperty(i)&&this.hasOwnProperty(i)){
                            this[i] = attrObj[i];
                        }

                    }
              },
              getPostJson:function(){
                  return{
                       "entryNumber":this.entryNumber,
                       "quantity" : this.qty,
                       "csaOption": this.csaOption,
                       "csaHours" : parseFloat(this.hours)+'',
                       "csaYear" : this.years,
                       "basePrice": this.basePrice+''
                  }
              }
          }
      }
  return csaHelper;
}]);