catApp.factory('EppHelperFactory', [function(){
  var eppHelper={};
      eppHelper.AreAllEppsApplied = function(eppItems){
        var i = eppItems.length;
                while(i--){
                    if(!eppItems[i].isEppDisabled){
                        return !!0;
                    }
                }
                return !!1;
      };

      eppHelper.register = function(rawObj){
          return {
              entryNumber:rawObj&&rawObj.entryNumber||'',
              basePrice:rawObj&&rawObj.basePrice.value||0,
              totalPrice:rawObj&&rawObj.totalPrice.formattedValue||"$0.00",
              qty:rawObj&&rawObj.quantity||0,
              old_qty:rawObj&&rawObj.quantity||0,
              eppOption:rawObj&&rawObj.configurationInfos.length
                        &&rawObj.configurationInfos[0].configurationValue||"",
              isEppDisabled:this.qty>0?!!0:!!1,
              increment:function(eppObj){
                  this.qty+=1;
                  this.changeInEppQty(eppObj);
              },
              decrement:function(eppObj){
                  this.qty>1?(this.qty-=1):'';
                  this.changeInEppQty(eppObj);
              },
              changeInEppQty:function(eppObj){
                this.qty = parseInt(this.qty,10)||0;
                eppObj.totalEppQty+=(this.qty-this.old_qty);

                if(this.isApplyDisabled()&&eppObj.totalEppQty>=eppObj.productQty){
                    this.isEppDisabled=!!1;
                  }
                if(this.qty !== this.old_qty){
                    if(eppObj.totalEppQty>eppObj.productQty){
                        eppObj.totalEppQty-=(this.qty-this.old_qty);
                          this.qty = this.old_qty;
                      }else{
                            this.old_qty = this.qty;
                            this.isEppDisabled=!!0;
                      }
                }
               
                  
                  /**
                   * Below code is to check the add button's disable status
                   * If an item doesn't have the entry number it is a new one, based on that disable condition has
                   * been modified.
                   */
                  if(eppObj.totalEppQty<eppObj.productQty&&eppObj.eppItems[eppObj.eppItems.length-1].entryNumber){
                    eppObj.isNewBtnDisable = !!0;
                  }
                  if(eppObj.totalEppQty===eppObj.productQty){
                    eppObj.isNewBtnDisable = !!1;
                  }
                },
              isApplyDisabled:function(){
                  if(this.isEppDisabled||!(parseInt(this.basePrice,10)
                        &&parseInt(this.qty,10)
                        &&this.eppOption)){
                      return !!1;
                  }
                  return !!0;
              },
              setAttributeWithValue:function(attrObj){
                var i;
                    for(i in attrObj){
                        if(attrObj.hasOwnProperty(i)&&this.hasOwnProperty(i)){
                            this[i] = attrObj[i];
                        }

                    }
              },
              getPostJson:function(){
                    return {
                          'entryNumber':this.entryNumber,
                          'quantity' : this.qty,
                         'eppOption': this.eppOption,
                        'basePrice' : parseFloat(this.basePrice)+''
                   }
                }
          }
      }
  return eppHelper;
}]);