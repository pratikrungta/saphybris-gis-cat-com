catApp.factory('catService', [function(){
	 var catData = {
	 				selectedProductFamilyCode: "",
			 		selectedSalesModelCode: "",
			 		selectedPrimaryOfferingCode: "",
			 		userTypeUTV: "UTV",
			 		userTypeSP: "SP",
			 		userTypeIM: "IM",
					activeAccordion : 0,
		 	plp: {
			 		isPDCChecked: false,
			 		isMFUChecked: false,
			 		isWarehouseChecked: false,
			 		currentPage: 0,
			 		isUsed: false

			 },
			 queryParams: {
				 bkp: "",
				 hasBkpData: false
			},
		 	recentlyviwed: {
		 		newProducts: true,
		 		usedProducts: false,
		 	},
		 	typeaHead: {
		 		newProducts: true,
		 		usedProducts: false
		 	},
		 	userWantsNewProducts: function(){
		 		this.recentlyviwed.newProducts = true;
		 		this.recentlyviwed.usedProducts = false;

		 		this.typeaHead.newProducts = true;
		 		this.typeaHead.usedProducts = false;

		 	},
		 	userWantsUsedProducts: function(){
		 		this.recentlyviwed.newProducts = false;
		 		this.recentlyviwed.usedProducts = true;

		 		this.typeaHead.newProducts = true;
		 		this.typeaHead.usedProducts = false;
			},
			setActiveAccordion : function(index){
				this.activeAccordion = index;
			},
			getActiveAccordion : function(){
				return this.activeAccordion;
			}
	 };
	 return catData;
}]);
