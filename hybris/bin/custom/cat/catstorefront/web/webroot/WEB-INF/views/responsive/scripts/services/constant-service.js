catApp.factory('constantService', [function(){
	 var constantData = {
			self : this,
			stopTruckloadTour : false,
			stopTruckloadTourMobile : false, 
			truckloadTourUserId : '',
			truckloadTourMobileUserId : '',
			stopAccessoriesTour : false,
			stopAccessoriesTourMobile : false, 
			accessoriesTourUserId : '',
			accessoriesTourMobileUserId : '',
			settruckloadTourUserId : function(userid){
				self.truckloadTourUserId = userid;
			},
			settruckloadTourMobileUserId : function(userid){
				self.truckloadTourMobileUserId = userid;
			},
			setAccessoriesTourUserId : function(userid){
				self.accessoriesTourUserId = userid;
			},
			setAccessoriesTourMobileUserId : function(userid){
				self.accessoriesTourMobileUserId = userid;
			},
	 		defaultDownloadImage: '/_ui/responsive/cat/images/download.jpg',
	 		globalSorryHeader: 'We\'re sorry',
	 		searchResultsErrorMessage: ' Something went wrong.Thanks for noticing, we are working on this!',
			noSearchResultsFound: 'Sorry, no results for your current search. Please try changing the search attributes.',
			homeURL : '/',
			quoteURL : '/quote/create',
			quoteHistoryURL : '/my-account/my-quotes/view',
			orderHistoryURL: '/my-account/cat/orders',
			quotationSubmitPopupURL : '/_ui/responsive/cat/templates/quotationSubmitQuotePopup.html',
			maxOrderLimit : 99,
			userTypeUTV: "UTV",
			userTypeSP: "SP",
			userTypeIM: "IM",
			truckloadFlow: "truckloadFlow",
			utvFlow: "utvFlow",
			quote: {
				productRemove: 'Are you sure you want to remove this product?',
				lastProductRemove: 'You have only one product in your quote. If you delete this product you will be redirected to the home page. Are you sure you want to delete this item?'
			},
			accessories: {
				lastProductRemove: 'Your current accessory selection will be lost. Do you want to continue and add the accessories again?'
			},
			countyMapping: {
				hideAddress: 'Are you sure you want to hide this county? All of the counties linked to this Ship-to would be unlinked.'
			},
			getTwoDigitFormattedText: function(val){
				if(val < 10){
		        	return ("00" + val).slice(-2);
				}
				return val;
			},
			getTruckloadLineItemObj : function(){
				return {
					"cuvProductsArray" : [],
					"addToTruckDisable" : true,
					"shipAfterDateLabel" : "Select a Ship After date",
					"shipAfterDateCode" : null,
					"shipAfterDateError" : false, 
					"isAddedToTruck" : false,
					"truckloadId": null,
					"addToTruckSuccessMsg":false,
					"addToTruckResponse" : {
						prodQtyMap : {},
						success : false,
					},
					"totalUnits":0
				}
			},
			pdpPage:{
				inTransitLink: 'https://gsnportal.cat.com/wps/myportal/gp/gpLMOrderDetails?lmdtsearch=3&otdtserial=_serialNumVar&otdtserialname=_serialNumVar'
			},
			accessoriesTourDesktopObject : {
				steps: [
				{
				  title: "<p>Hey there!</p><p> Let's walk through the accessory ordering.</p>",
				  content: "Please take the following 3 simple steps to understand how to add accessories to your order.",
				  template : beginTourTemplateString() +
									"<input type='checkbox' name='dissmissPopoverCheckbox' id='dismiss-acc-desk-popover-step0'>"+
									"<label for='dismiss-acc-desk-popover-step0'>"+
										"<span aria-hidden='true'></span>"+
									"</label>"+
									"<label class='checkboxlabel'>Don't show this walkthrough again.</label>"+
								"</div>"+
								beginTourButton() +
							"</div>",
				  orphan:true,
				  onShown : function(tour){
					$(".tour-backdrop.bottom").css({'position':'fixed'});
				  },	
				  onNext : function(){
					$(".tour-backdrop.bottom").css({'position':'absolute'});
					if($("#dismiss-acc-desk-popover-step0").prop("checked")){
						localStorage.setItem('accessoriesTourDismiss'+ self.accessoriesTourUserId,'true');
					} else {
						localStorage.setItem('accessoriesTourDismiss'+ self.accessoriesTourUserId,'false');
					}
				  }
				},
				// {
				//   element: "#accTourDesk1",
				//   title: "<p>Step 1 of 4 </p><p>Select a model</p>",
				//   content: "Select the CUV model for which you are ordering the accessory.",
				//   placement: 'left',
				//   backdropPadding:{
				// 	  right: 50,
				// 	  bottom:180,
				// 	  left: 0
				//   },
				//   template : "<div class='col-xs-12 col-md-12 popover tour truckloadStartPopover acc-step-1'>"+
				// 			"<div class='arrow'></div>"+
				// 			"<span class='end-tour pull-right' data-role='end'></span>"+
				// 			iconTitleString() +
				// 			onlyNextButtonOnPage() +
				// 			"</div>",
				// 			onShown: function(tour) {
				// 				disableBackdropInteraction(tour);
				// 			},
				// 			onHidden: function(tour) {
				// 				enableBackdropInteraction(tour);
				// 			}
				// },
				{
				  element: "#accTourDesk2",
				  title: "<p>Step 1 of 3</p><p>Select quantity</p>",
				  content: "Select a quantity for the accessory.",
				  placement: 'left',
				  backdropPadding:{
					  right:30,
					  bottom:160,
					  left:0
				  },
				  template : "<div class='col-xs-12 col-md-12 popover tour truckloadStartPopover acc-step-2'>"+
							"<div class='arrow'></div>"+
							"<span class='end-tour pull-right' data-role='end'></span>"+
							iconTitleString() +
							onlyNextButtonOnPage() +
							"</div>",
							onShown: function(tour) {
								disableBackdropInteraction(tour);
							},
							onHidden: function(tour) {
								enableBackdropInteraction(tour);
							}
				},
				{
				  element: "#accTourDesk3",
				  title: "<p>Step 2 of 3</p><p>ORDERING FOR ANOTHER CUV</p>",
				  content: "<p>Click on \"+ ADD\" to order the accessory for another CUV.</p>",
				  placement: 'bottom',
				  backdropPadding:{
					  top:30,
					  right:50,
					  bottom:50,
					  left:30
				  },
				  template : "<div class='col-xs-12 col-md-12 popover tour truckloadStartPopover acc-step-3'>"+
							"<div class='arrow'></div>"+
							"<span class='end-tour pull-right' data-role='end'></span>"+
							iconTitleString() +
							nextButtonString() +
							prevButtonString() +
							"</div>",
							onShown: function(tour) {
								disableBackdropInteraction(tour);
							},
							onHidden: function(tour) {
								enableBackdropInteraction(tour);
							},
				  onNext : function(){}
				},
				{
				  element: "#accTourDesk4",
				  title: "<p>Step 3 of 3</p><p>One last step</p>",
				  content: "<p>Click on “Review Order” to review your order.</p>",
				  placement: 'top',
				  backdropPadding:30,
				  template : "<div class='col-xs-12 col-md-12 popover tour truckloadStartPopover acc-step-4'>"+
							"<div class='arrow'></div>"+
							"<span class='end-tour pull-right' data-role='end'></span>"+
							iconTitleString() +
							finishButtonString() +
							prevButtonString() +
							"</div>",
							onShown: function(tour) {
								disableBackdropInteraction(tour);
							},
							onHidden: function(tour) {
								enableBackdropInteraction(tour);
							}	
				}],
				backdrop:true,
				onEnd : function(tour) {
					if(tour._current == 0 && $("#dismiss-acc-desk-popover-step0").prop('checked')){
						localStorage.setItem('accessoriesTourDismiss'+ self.accessoriesTourUserId,'true');
						localStorage.setItem('accessoriesTourUserId',self.accessoriesTourUserId);
					} else if((tour._current == 4 && !self.stopAccessoriesTour)){
						localStorage.setItem('accessoriesTourDismiss' + self.accessoriesTourUserId,'true');
						localStorage.setItem('accessoriesTourUserId',self.accessoriesTourUserId);
					} else {
						localStorage.setItem('accessoriesTourUserId','');
					}
					$(window).trigger('tourClosed');
					$("#accTourDeskStep3").css({'display':'none'});
					window.scrollTo(0,0);
				}
			},
			accessoriesTourMobileObject : {
				steps: [
				{
				  title: "<p>Hey there!</p><p> Let's walk through the accessory ordering.</p>",
				  content: "Please take the following 3 simple steps to understand how to add accessories to your order",
				  template : beginTourTemplateString() +
									"<input type='checkbox' name='dissmissPopoverCheckbox' id='dismiss-acc-popover-step0'>"+
									"<label for='dismiss-acc-popover-step0'>"+
										"<span aria-hidden='true'></span>"+
									"</label>"+
									"<label class='checkboxlabel'>Don't show this walkthrough again.</label>"+
								"</div>"+
								beginTourButton() +
							"</div>",
				  orphan:true,
				  onShown : function(tour){
					$(".tour-backdrop.bottom").css({'position':'fixed'});
				  },	
				  onNext : function(){
					$(".tour-backdrop.bottom").css({'position':'absolute'});
					if($("#dismiss-acc-popover-step0").prop("checked")){
						localStorage.setItem('accessoriesTourDismiss'+ self.accessoriesTourUserId,'true');
					} else {
						localStorage.setItem('accessoriesTourDismiss'+ self.accessoriesTourUserId,'false');
					}
				  }
				},
				// {
				//   element: "#accessories0 #accTourMob10",
				//   title: "<p>Step 1 of 4 </p><p>SELECT A MODEL</p>",
				//   content: "Select the CUV model for which you are ordering the accessory.",
				//   placement: 'bottom',
				//   backdropPadding:{
				// 	  top:10,
				// 	  right:0,
				// 	  bottom:10,
				// 	  left:0
				//   },
				//   template : "<div class='col-xs-12 col-md-12 popover tour truckloadStartPopover step1 acc-step-mb-1'>"+
				// 			"<div class='arrow'></div>"+
				// 			"<span class='end-tour pull-right' data-role='end'></span>"+
				// 			iconTitleString() +
				// 			onlyNextButtonOnPage() +
				// 			"</div>",
				// 			onShown: function(tour) {
				// 				disableBackdropInteraction(tour);
				// 			},
				// 			onHidden: function(tour) {
				// 				enableBackdropInteraction(tour);
				// 			}
				// },
				{
				  element: "#accessories0 #accTourMob20",
				  title: "<p>Step 1 of 3</p><p>SELECT QUANTITY</p>",
				  content: "Select a quantity for the accessory.",
				  placement: 'bottom',
				  backdropPadding:{
					  top:10,
					  right:-40,
					  bottom:20,
					  left: 0
				  },
				  template : "<div class='col-xs-12 col-md-12 popover tour truckloadStartPopover step2 mobile acc-step-mb-2'>"+
							"<div class='arrow'></div>"+
							"<span class='end-tour pull-right' data-role='end'></span>"+
							iconTitleString() +
							onlyNextButtonOnPage() +
							"</div>",
							onShown: function(tour) {
								disableBackdropInteraction(tour);
							},
							onHidden: function(tour) {
								enableBackdropInteraction(tour);
							}
				},
				{
				  element: "#accTourMob3",
				  title: "<p>Step 2 of 3</p><p>ORDERING FOR ANOTHER CUV</p>",
				  content: "<p>Click on \"+ Add\" to order the accessory for another CUV</p>",
				  placement: 'bottom',
				  backdropPadding:{
					  top:0,
					  right:0,
					  bottom:20,
					  left:0
				  },
				  template : "<div class='col-xs-12 col-md-12 popover tour truckloadStartPopover step3 acc-step-mb-3'>"+
							"<div class='arrow'></div>"+
							"<span class='end-tour pull-right' data-role='end'></span>"+
							iconTitleString() +
							nextButtonString() +
							prevButtonString() +
							"</div>",
							onShown: function(tour) {
								disableBackdropInteraction(tour);
							},
							onHidden: function(tour) {
								enableBackdropInteraction(tour);
							},
							onNext : function(tour){
								$(".view-cart-section").css({'position':'absolute'});
							  },
				},
				{
				  element: "#accTourMob4",
				  title: "<p>Step 3 of 3</p><p>One last step</p>",
				  content: "<p>Click on “Review Order” to review your order.</p>",
				  placement: 'top',
				  backdropPadding:{
					  top:15,
					  right:10,
					  bottom:15,
					  left:10
				  },
				  template : "<div class='col-xs-12 col-md-12 popover tour truckloadStartPopover acc-step-mb-4'>"+
							"<div class='arrow'></div>"+
							"<span class='end-tour pull-right' data-role='end'></span>"+
							iconTitleString() +
							finishButtonString() +
							prevButtonString() +
							"</div>",
							onShown: function(tour) {
								disableBackdropInteraction(tour);
							},
							onHidden: function(tour) {
								enableBackdropInteraction(tour);
							},
							onHide : function(){
								$(".view-cart-section").css({'position':'fixed'});
							}	
				},
			],
			backdrop:true,
			onEnd : function(tour){
				if(tour._current == 0 && $("#dismiss-acc-popover-step0").prop('checked')){
					localStorage.setItem('accessoriesTourDismissMobile'+ self.accessoriesTourUserId,'true');
					localStorage.setItem('accessoriesTourMobileUserId',self.accessoriesTourMobileUserId);
				} else if(tour._current == 4 && !self.stopAccessoriesTourMobile){
					localStorage.setItem('accessoriesTourDismissMobile'+ self.accessoriesTourUserId,'true');
					localStorage.setItem('accessoriesTourMobileUserId',self.accessoriesTourMobileUserId);
				} else {
					localStorage.setItem('accessoriesTourMobileUserId','');
				}
				$("#accTourMobStep3").css({'display':'none'});
				window.scrollTo(0,0);
			}
		},
			truckloadTourDesktopObject : {
				steps: [
				{
				  title: "<p>Hey there!</p><p> Let's walk you through the truckload configurator.</p>",
				  content: "Please take a moment and begin the tour following 4 simple steps to understand how a truckload configurator works.",
				  template : beginTourTemplateString() +
									"<input type='checkbox' name='dissmissPopoverCheckbox' id='dismiss-popover-step0'>"+
									"<label for='dismiss-popover-step0'>"+
										"<span aria-hidden='true'></span>"+
									"</label>"+
									"<label class='checkboxlabel'>Don't show this walkthrough again.</label>"+
								"</div>"+
								beginTourButton() +
							"</div>",
				  orphan:true,
				  onShown : function(tour){
					$(".tour-backdrop.bottom").css({'position':'fixed'});
				  },
				  onNext : function(){
					$(".tour-backdrop.bottom").css({'position':'absolute'});
					if($("#dismiss-popover-step0").prop("checked")){
						localStorage.setItem('truckloadTourDismiss','true');
					} else {
						localStorage.setItem('truckloadTourDismiss','false');
					}
				  }
				},
				{
				  element: ".truckload-config-accordion",
				  title: "<p>STEP 1 of 4 </p><p>Truckload Configuration Matrix</p>",
				  content: "CUV’s have to be ordered in a full truckload. This matrix will help you understand the combination of products that can be added to make one full truckload.",
				  placement: 'bottom',
				  backdropPadding:{
					  top:75,
					  right:30,
					  bottom:30,
					  left:30
				  },
				  template : "<div class='col-xs-12 col-md-12 popover tour truckloadStartPopover step1'>"+
							"<div class='arrow'></div>"+
							"<span class='end-tour pull-right' data-role='end' onclick='self.stopTruckloadTour = true;'></span>"+
							iconTitleString() +
							onlyNextButtonOnPage() +
							"</div>",
				  onShown: function(tour) {
					  disableBackdropInteraction(tour);
				  },
				  onHidden: function(tour) {
					  enableBackdropInteraction(tour);
				  }		
				},
				{
				  element: "#truckloadForm0 .product-family-dropdown",
				  title: "<p>STEP 2 of 4</p><p>Ship after date</p>",
				  content: "Select your preferred ship after date.",
				  placement: 'bottom',
				  backdropPadding:{
					  top:60,
					  right:30,
					  bottom:30,
					  left:30
				  },
				  template : "<div class='col-xs-12 col-md-12 popover tour truckloadStartPopover step2'>"+
							"<div class='arrow'></div>"+
							"<span class='end-tour pull-right' data-role='end' onclick='self.stopTruckloadTour = true;'></span>"+
							iconTitleString() +
							nextButtonString() +
							prevButtonString() +
							"</div>",
							onShown: function(tour) {
								disableBackdropInteraction(tour);
							},
							onHidden: function(tour) {
								enableBackdropInteraction(tour);
							}
				},
				{
				  element: "#truckloadForm0 .truckload-product-card:first",
				  title: "<p>STEP 3 of 4</p><p>Add or edit CUV's</p>",
				  content: "<p>Select the CUV quantity by typing or clicking the \"+\" button. You can reduce the quantity by pressing the \"-\" button.</p>",
				  placement: 'right',
				  backdropPadding:{
					  top:30,
					  right:-50,
					  bottom:-30,
					  left:30
				  },
				  template : "<div class='col-xs-12 col-md-12 popover tour truckloadStartPopover step3'>"+
							"<div class='arrow'></div>"+
							"<span class='end-tour pull-right' data-role='end' onclick='self.stopTruckloadTour = true;'></span>"+
							iconTitleString() +
							nextButtonString() +
							prevButtonString() +
							"</div>",
				  onShown: function(tour) {
					  disableBackdropInteraction(tour);
				  },
				  onHidden: function(tour) {
					  enableBackdropInteraction(tour);
				  }
				},
				{
				  element: "#truckloadForm0 .truckload-button-wrapper:last .add-to-truckload-btn",
				  title: "<p>STEP 4 of 4</p><p>One last step</p>",
				  content: "<p>Click on the \"Add to Truck button\" to move CUV's to the truckload.</p><p>Note : \"Add to Truck\" button changes to \"Update Truck\" upon modifying the existing truckload.</p>",
				  placement: 'left',
				  backdropPadding:30,
				  template : "<div class='col-xs-12 col-md-12 popover tour truckloadStartPopover step4'>"+
							"<div class='arrow'></div>"+
							"<span class='end-tour pull-right' data-role='end' onclick='self.stopTruckloadTour = true;'></span>"+
							iconTitleString() +
							finishButtonString() +
							prevButtonString() +
							"</div>",
				  onShown: function(tour) {
					  disableBackdropInteraction(tour);
				  },
				  onHidden: function(tour) {
					  enableBackdropInteraction(tour);
				  }		
				}],
				backdrop:true,
				onEnd : function(tour){
					if(tour._current == 0 && $("#dismiss-popover-step0").prop('checked')){
						localStorage.setItem('truckloadTourDismiss','true');
						localStorage.setItem('truckloadTourUserId',self.truckloadTourUserId);
					} else if((tour._current == 4 && !self.stopTruckloadTour)) {
						localStorage.setItem('truckloadTourDismiss','true');
						localStorage.setItem('truckloadTourUserId',self.truckloadTourUserId);
					} else {
						localStorage.setItem('truckloadTourUserId','');
					}
				}
			},
			truckloadTourMobileObject : {
				steps: [
				{
				  title: "<p>Hey there!</p><p> Let's walk you through the truckload configurator.</p>",
				  content: "Please take a moment and begin the tour following 4 simple steps to understand how a truckload configurator works.",
				  template : beginTourTemplateString() +
									"<input type='checkbox' name='dissmissPopoverCheckbox' id='dismiss-popover-step0Mobile'>"+
									"<label for='dismiss-popover-step0Mobile'>"+
										"<span aria-hidden='true'></span>"+
									"</label>"+
									"<label class='checkboxlabel'>Don't show this walkthrough again.</label>"+
								"</div>"+
								beginTourButton() +
							"</div>",
				  orphan:true,
				  onShown : function(tour){
					$(".tour-backdrop.bottom").css({'position':'fixed'});
				  },
				  onNext : function(){
					$(".tour-backdrop.bottom").css({'position':'absolute'});
					if($("#dismiss-popover-step0Mobile").prop("checked")){
						localStorage.setItem('truckloadTourDismissMobile','true');
					} else {
						localStorage.setItem('truckloadTourDismissMobile','false');
					}
				  }
				},
				{
				  element: ".truckload-config-accordion",
				  title: "<p>STEP 1 of 4 </p><p>Truckload Configuration Matrix</p>",
				  content: "CUV’s have to be ordered in a full truckload. This matrix will help you understand the combination of products that can be added to make one full truckload.",
				  placement: 'bottom',
				  backdropPadding:{
					  top:75,
					  right:30,
					  bottom:30,
					  left:30
				  },
				  template : "<div class='col-xs-12 col-md-12 popover tour truckloadStartPopover step1 mobile'>"+
							"<div class='arrow'></div>"+
							"<span class='end-tour pull-right' data-role='end'></span>"+
							iconTitleString() +
							onlyNextButtonOnPage() +
							"</div>",
				  onShown: function(tour) {
					  disableBackdropInteraction(tour);
				  },
				  onHidden: function(tour) {
					  enableBackdropInteraction(tour);
				  }			
				},
				{
				  element: "#truckloadForm0 .product-family-dropdown",
				  title: "<p>STEP 2 of 4</p><p>Ship after date</p>",
				  content: "Select your preferred ship after date.",
				  placement: 'bottom',
				  backdropPadding:{
					  top:40,
					  right:10,
					  bottom:20,
					  left:10
				  },
				  template : "<div class='col-xs-12 col-md-12 popover tour truckloadStartPopover step2 mobile'>"+
							"<div class='arrow'></div>"+
							"<span class='end-tour pull-right' data-role='end'></span>"+
							iconTitleString() +
							nextButtonString() +
							prevButtonString() +
							"</div>",
				  onShown: function(tour) {
					  disableBackdropInteraction(tour);
				  },
				  onHidden: function(tour) {
					  enableBackdropInteraction(tour);
				  }		
				},
				{
				  element: "#truckloadForm0 .truckload-product-card:first",
				  title: "<p>STEP 3 of 4</p><p>Add or edit CUV's</p>",
				  content: "<p>Select the CUV quantity by typing or clicking the \"+\" button. You can reduce the quantity by pressing the \"-\" button.</p>",
				  placement: 'bottom',
				  backdropPadding:{
					  top:10,
					  right:0,
					  bottom:-10,
					  left:10
				  },
				  template : "<div class='col-xs-12 col-md-12 popover tour truckloadStartPopover step3 mobile'>"+
							"<div class='arrow'></div>"+
							"<span class='end-tour pull-right' data-role='end'></span>"+
							iconTitleString() +
							nextButtonString() +
							prevButtonString() +
							"</div>",
				  onShown: function(tour) {
					  disableBackdropInteraction(tour);
				  },
				  onHidden: function(tour) {
					  enableBackdropInteraction(tour);
				  }		
				},
				{
				  element: "#truckloadForm0 .truckload-button-wrapper:first .add-to-truckload-btn",
				  title: "<p>STEP 4 of 4</p><p>One last step</p>",
				  content: "<p>Click on the \"Add to Truck button\" to move CUV's to the truckload.</p><p>Note : \"Add to Truck\" button changes to \"Update Truck\" upon modifying the existing truckload.</p>",
				  placement: 'bottom',
				  backdropPadding:{
					  top:15,
					  right:10,
					  bottom:15,
					  left:10
				  },
				  template : "<div class='col-xs-12 col-md-12 popover tour truckloadStartPopover step4 mobile'>"+
							"<div class='arrow'></div>"+
							"<span class='end-tour pull-right' data-role='end' onclick='self.stopTruckloadTourMobile = true;'></span>"+
							iconTitleString() +
							finishButtonString() +
							prevButtonString() +
							"</div>",
				  onShown: function(tour) {
					  disableBackdropInteraction(tour);
				  },
				  onHidden: function(tour) {
					  enableBackdropInteraction(tour);
				  }		
				}],
				backdrop:true,
				onEnd : function(tour){
					if(tour._current == 0 && $("#dismiss-popover-step0").prop('checked')){
						localStorage.setItem('truckloadTourDismissMobile','true');
						localStorage.setItem('truckloadTourMobileUserId',self.truckloadTourMobileUserId);
					} else if(tour._current == 4 && !self.stopTruckloadTourMobile){
						localStorage.setItem('truckloadTourDismissMobile','true');
						localStorage.setItem('truckloadTourMobileUserId',self.truckloadTourMobileUserId);
					} else {
						localStorage.setItem('truckloadTourMobileUserId','');
					}
				}
			}
	 };

	function prevButtonString() {
		return (
			"<div class='col-xs-12 col-md-12 popover-navigation prev-container no-padding text-center'>"+
				"<button class='btn update-product-list truckload-step-prev' data-role='prev'>Previous</button>"+
			"</div>"
		);
	}

	function finishButtonString() {
		return (
			"<div class='col-xs-12 col-md-12 popover-navigation no-padding text-center'>"+
				"<button class='btn update-product-list truckload-step-next' data-role='end'>Finish</button>"+
			"</div>"
		);
	}

	function nextButtonString() {
		return (
			"<div class='col-xs-12 col-md-12 popover-navigation no-padding text-center'>"+
				"<button class='btn update-product-list truckload-step-next' data-role='next'>Next</button>"+
			"</div>"
		)
	}

	function beginTourButton() {
		return (
			"<div class='col-xs-12 col-md-12 popover-navigation no-padding text-center'>"+
				"<button class='btn update-product-list truckload-begin-tour' data-role='next'>Begin Tour</button>"+
			"</div>"
		);
	}

	function onlyNextButtonOnPage() {
		return (
			"<div class='col-xs-12 col-md-12 popover-navigation no-padding text-center'>"+
				"<button class='btn update-product-list truckload-begin-tour' data-role='next'>Next</button>"+
			"</div>"
		)
	}

	function iconTitleString() {
		return (
			"<div class='col-xs-12 col-md-12 no-padding'>"+
				"<div class='col-xs-3 col-md-3 configure-icon'></div>"+
				"<div class='col-xs-9 col-md-9 popover-title'></div>"+
			"</div>"+
			"<div class='col-xs-12 col-md-12 popover-content no-padding'></div>"
		)
	}

	function beginTourTemplateString() {
		return (
			"<div class='col-xs-12 col-md-12 popover tour truckloadStartPopover'>"+
			"<span class='end-tour pull-right' data-role='end'></span>"+
			"<div class='col-xs-12 col-md-12 no-padding'>"+
				"<div class='col-xs-3 col-md-3 configure-icon'></div>"+
				"<div class='col-xs-9 col-md-9 popover-title'></div>"+
			"</div>"+
			"<div class='col-xs-12 col-md-12 popover-content no-padding'></div>"+
			"<div class='col-xs-12 col-md-12 popover-dismiss no-padding'>"
		)
	}

	function disableBackdropInteraction(tour){
		var step = tour._options.steps[tour._current];
		/// Overlay a div on the tour contents to prevent clicking
		// Remove any existing overlay divs - accounting for shown event being called on window resize
		$(step.element).children('.tour-step-backdrop-parent').remove();
		// Create overlay div
		var disabledOverlay = $('<div class="tour-step-backdrop-parent"></div>');
		// Insert overlay div before step element in DOM
		step.element.indexOf('acc') >= 0 ? $(step.element).prepend(disabledOverlay) : 
										   $(step.element).before(disabledOverlay);
		var stepElement = $(step.element);
		// Account for step's backdrop padding if any (double it since it surrounds the element)
		var padding = (step.backdropPadding || 0) * 2;
		// Set size of the overlay div to the step element's size and account for rounding by adding 1 pixel
		disabledOverlay.outerHeight(stepElement.outerHeight() + padding + 1).outerWidth(stepElement.outerWidth() + padding + 1);
		// Set position based on element's position and adjust for backdrop padding
		var offset = stepElement.offset();
		var width = stepElement.outerWidth();
		var height = stepElement.outerHeight();
		offset.top = offset.top - (step.backdropPadding || 0);
		offset.left = offset.left - (step.backdropPadding || 0);
		step.backdropPadding.bottom > 0 ? height += step.backdropPadding.bottom : null;
		step.backdropPadding.right > 0 ? width += step.backdropPadding.right : null;
		disabledOverlay.offset(offset);
		disabledOverlay.width(width);
		disabledOverlay.height(height);
		disabledOverlay.css('border', 0);
	}

	function enableBackdropInteraction(tour){
		var step = tour._options.steps[tour._current];
		// Remove overlay div
		step.element.indexOf('acc') >= 0 ? $(step.element).children('.tour-step-backdrop-parent').remove() : 
											$(step.element).siblings('.tour-step-backdrop-parent').remove();	
	}

	 return constantData;
}]);
