catApp.factory('getUrlParamFactory', ['$location',function($location){
    var getParams ={},data={};
    data.params={};
    getParams.storeUrlParams = function(){
        angular.copy($location.search(),data.params);
    }
    getParams.getUrlParams = function(){
        return data.params||{};
    }
    getParams.formParams = function(context){
        var i;
        if(!context.hasOwnProperty('url')){
            return;
        }
        for(i in context){
            if(context.hasOwnProperty(i) && i!=='url'){
                if(context.url.indexOf('?') !== -1){
                    context.url+="&"+i+"="+context[i];
                }else{
                    context.url+='?'+i+"="+context[i];
                }
            }
        }
        return context.url;
    }
    return getParams;
}]);