catApp.service('httpService', ['$http', function($http) {
	var config = {
		headers : {
      'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
    }
	};

  var configForJson = {
    headers : {
      'Content-Type': 'application/json',
       'CSRFToken': ACC.config.CSRFToken
    }
  };

	this.post = function(path, data) {
    return $http.post(path, data, config);
  };

  this.postJsonData = function(path, data) {
    return $http.post(path, data, configForJson);
  };

  this.get = function(path) {
    return $http.get(path);
  };
  
  this.delete = function(path) {
	    return $http.delete(path);
	  };
}]);