catApp.factory('modalPopUpFactory', ['$uibModal',function($uibmodal){
    var modal_object={},
        memoiz_data={};
    modal_object.open=function(m_obj){
        if(typeof m_obj==="object"){
            //angular.element(angular.element("[ng-app=cat-app]")[0]).addClass('pop-up-blur');
            return $uibmodal.open(m_obj);
        }
        return;
    };
    modal_object.memoiz=function(op,key,data){
    	var operation = op.toLowerCase()
        if(!(key&&operation)){
            return;
        }
        if(operation==='add' && data){
            memoiz_data[key]=data;
            return;
        }
        if(operation==='remove'){
            delete memoiz_data[key];
            return;
        }
        if(operation.toLowerCase()==='get'){
            return memoiz_data[key];
        }
    }
    modal_object.close=function(m_obj){
        if(typeof m_obj==="object"){
            //angular.element(angular.element("[ng-app=cat-app]")[0]).removeClass('pop-up-blur');
            m_obj.dismiss("cancel");
            return;
        }
        return;
    };
    return modal_object;
}])