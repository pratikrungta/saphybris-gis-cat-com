catApp.service('pdpTabService', ['$q','httpService','catEndpointService', function($q,httpService, catEndpointService){
        
    this.getTabName = function(tabName){
            return tabName.split(' ').slice(3).join(' ');
        };
        this.loadUpdatedData = function(context){
            var deferred = $q.defer();
            var partyIdList = [], configIdList = [];
            
            var success = function(res){
                var response,errObj={};
                if(!!res && !!res.data){
                    response=(typeof res.data==='string')?JSON.parse(res.data):res.data;
                }
                try{
                    if(response[0].edhError&&response[0].edhError.code=="600"){
                        errObj.oopsMsg = response[0].edhError.description;
                        deferred.reject(errObj);
                        return
                    }
                    if(response[0].edhError){
                        var contactMsg = response[0].edhError.description.split('\n')[1].split(response[0].edhError.mailid);
                            errObj.oopsMsg = response[0].edhError.description.split('\n')[0];
                            errObj.mailId = response[0].edhError.mailid;
                            errObj.contactMsgPreText=contactMsg[0];
                            errObj.contactMsgPostText=contactMsg[1];
                            deferred.reject(errObj);
                        return;
                   } 
                   deferred.resolve(response);
                }catch(e){
                    deferred.reject(false);
                }
            };
            var failure = function(res){
                deferred.reject(res);
            };

            if(context.method.toUpperCase()==='POST'){
                partyIdList.push(context.dealerCode);
                configIdList.push(context.productCode);
                var req = {
                        'partyIdList':partyIdList,
                        'configIdList':configIdList
                        };
                httpService.postJsonData(context.url,req).then(
                    function(res){
                        success(res);
                       
                    },function(res){
                        failure(res);
                    }
               );
            }else{
                httpService.get(context.url).then(
                    function(res){
                        success(res);
                    },function(res){
                        failure(res);
                    }
               );
            }
            
           return deferred.promise;
        };
}])