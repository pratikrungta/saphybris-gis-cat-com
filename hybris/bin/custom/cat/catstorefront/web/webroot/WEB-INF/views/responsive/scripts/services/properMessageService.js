catApp.service('properMessageService',[function(){
    var message;
    this.cartLostMessage = function(cartType){
        switch (cartType){
            case 'cartProductIsBCP' :
            message ='Your current cart contains BCP products, which would be lost. Do you want to continue?';
            break;
            case 'cartProductIsAccessory' :
            message='Your current cart contains Utility Vehicles Accessories, which would be lost. Do you want to continue?';
            break;
            case 'cartProductIsUtv':
            message='Your current cart contains Utility Vehicles, which would be lost. Do you want to continue?';
            break;
            case 'cartProductIsUtvTruck' :
            message='Your current cart contains Truckload of Utility Vehicles, which would be lost. Do you want to continue?';
            break;
            default:
            message='Your current cart would be lost, do you want to continue?';
        }
        return message;            
    }
}]);
