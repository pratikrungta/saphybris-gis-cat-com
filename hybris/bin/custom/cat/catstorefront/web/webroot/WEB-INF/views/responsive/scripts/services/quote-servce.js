catApp.service('quoteService', ['httpService', 'catEndpointService', '$q', '$filter', 'regionService','screenResizeService', function(httpService, catEndpointService, $q, $filter, regionService,screenResizeService) {

    this.quoteInfo = {
        metaData: {
            editQuote: false,
	    replicateQuote: false,
            countryList: {
                dataList: [],
                dataInProgress: true,
                dataRecived: false,
                selectedCountry: {}
            },
            regionList: {
                dataList: [],
                countrySelected: false,
                dataInProgress: true,
                dataRecived: false,
                selectedRegion: {}
            },
            shippngAddressId: null,
            billingAddressId: null,
            dateModified: "",
            mobileMetaData: screenResizeService.resizeMetaData,
            mobileNav: {
                currentPageNum: 1,
                nextButtonText: "",
                previousButtonText: "",
                showPrevious: true,
                showNext: false
            }
        },
        quoteDetails: {
            metaData: {
                dateFormat: 'MM-dd-yyyy',
                validTillDateOptions: {
                    data: '',
                    formatYear: 'yy',
                    startingDay: 0,
                    showWeeks: false,
                    minDate: new Date(),
                    maxDate: new Date(2030, 31, 12)
                },
                validTillQuoteEditDateOptions: {
                    data: '',
                    formatYear: 'yy',
                    startingDay: 0,
                    showWeeks: false,
                    minDate: new Date(),
                    maxDate: new Date(2030, 31, 12)
                },
                submittedToCustomerDateOptions: {
                    data: '',
                    formatYear: 'yy',
                    startingDay: 0,
                    showWeeks: false,
                    minDate: new Date(),
                    maxDate: new Date(2030, 31, 12)
                },
                customerResponseDateOptions: {
                    data: '',
                    formatYear: 'yy',
                    startingDay: 0,
                    showWeeks: false,
                    minDate: new Date(),
                    maxDate: new Date(2030, 31, 12)
                }
            },
            number: {
                value: "",
                isValid: true,
            },
            createdOn: {
                value: "",
                isValid: true,
                popupOpend: false
            },
            name: {
                value: "",
                isValid: true
            },
            validTill: {
                value: "",
                isValid: true,
                popupOpend: false
            },
            validTillQuoteEdit: {
                value: "",
                isValid: true,
                popupOpend: false
            },
            description: {
                value: "",
                isValid: true
            },
            submitedToCustomer: {
                value: "",
                isValid: true,
                popupOpend: false
            },
            customerResponseDate: {
                value: "",
                isValid: true,
                popupOpend: false
            },
            status: {
                list: "",
                selected: "",
                isValid: true
            },
            reasonForQuoteLost: {
                list:"",
                selected: "",
                isValid: true
            }
        },
        cartDetails: {
            CSAOption: {
                checked: true,
                details: ""
            },
            EPPOption: {
                checked: true,
                details: ""
            }
        },
	reserveMachineDetails:{
            reserveEndDate: {
                value: "",
                isValid: true,
                popupOpend: false
            },
            reserveEndDateOptions: {
                data: '',
                formatYear: 'yy',
                startingDay: 0,
                showWeeks: false,
                minDate: new Date(),
                maxDate: new Date(2030, 31, 12)
            },
            dateFormat: 'MM-dd-yyyy'

        },
        customerData: {
            isAddressSame: false,
            customerType: 'old',
            uid: '',
            customerName: {
                value: '',
                isValid: true
            },
            customerEmail: {
                value: '',
                isValid: true
            },
            customerComments: {
                value: '',
                isValid: true
            },
            shippingAddress: {
                line1: {
                    value: '',
                    isValid: true
                },
                line2: {
                    value: '',
                    isValid: true
                },
                town: {
                    value: '',
                    isValid: true
                },
                region: {
                    value: '',
                    isValid: true
                },
                country: {
                    value: '',
                    isValid: true
                },
                postalCode: {
                    valid: '',
                    isValid: true
                },
                countryList: {
                    dataList: [],
                    dataInProgress: true,
                    dataRecived: false,
                    selectedCountry: {},
                    isValid: true
                },
                regionList: {
                    dataList: [],
                    countrySelected: false,
                    dataInProgress: true,
                    dataRecived: false,
                    selectedRegion: {},
                    isValid: true
                }
            },
            billingAddress: {
                line1: {
                    value: '',
                    isValid: true
                },
                line2: {
                    value: '',
                    isValid: true
                },
                town: {
                    value: '',
                    isValid: true
                },
                region: {
                    value: '',
                    isValid: true
                },
                country: {
                    value: '',
                    isValid: true
                },
                postalCode: {
                    valid: '',
                    isValid: true
                },
                countryList: {
                    dataList: [],
                    dataInProgress: true,
                    dataRecived: false,
                    selectedCountry: {},
                    isValid: true
                },
                regionList: {
                    dataList: [],
                    countrySelected: false,
                    dataInProgress: true,
                    dataRecived: false,
                    selectedRegion: {},
                    isValid: true
                }
            },
            countryList: {},
            stateList: {},
            ajaxCallInProgress: false
        },
        formValid: false,
        submitToDSU: {
            value: false,
            ajaxInprogress: false,
            response: {}
        },
        submitResponse: {
            status: true,
            quoteCode: "12334",
            quoteName: "quotename",
            quoteCustomer: "customer name",
            errorMessage: "Error message to be shown to user"
        }
    };

    this.backupCustomerData = {};

    this.retriveDateInCatFormate = function(catDate) {
        var dateArr = catDate.split(" ");
        return new Date(parseInt(dateArr[5]), vm.getMonth(dateArr[1]), parseInt(dateArr[2]));
    };

    this.retriveStautsList = function(){
        var quoteObj = this.quoteInfo;
        _this = this;
        httpService.get(catEndpointService.quoteStatus).then(function(response) { 
            quoteObj.quoteDetails.status.list = response.data;
            _this.retriveQuoteList();
        }, function(error){

        });
    }

    this.retriveQuoteList = function(){
        var quoteObj = this.quoteInfo;
        _this = this;
        httpService.get(catEndpointService.quoteReasonLost).then(function(response) { 
            quoteObj.quoteDetails.reasonForQuoteLost.list = response.data;
            _this.retriveQuoteData();
        }, function(error){
        });
    }

    this.updateCountryList = function() {
        var currentScope = this.quoteInfo;
        (function() {
            regionService.getCountryList().then(function(response) {
                currentScope.metaData.countryList.dataList = response;
                currentScope.metaData.countryList.dataInProgress = false
                currentScope.metaData.countryList.dataRecived = true;

                currentScope.customerData.shippingAddress.countryList.dataList = response;
                currentScope.customerData.shippingAddress.countryList.dataInProgress = false
                currentScope.customerData.shippingAddress.countryList.dataRecived = true;

                currentScope.customerData.billingAddress.countryList.dataList = response;
                currentScope.customerData.billingAddress.countryList.dataInProgress = false
                currentScope.customerData.billingAddress.countryList.dataRecived = true;
            }, function(error) {
                currentScope.metaData.countryList.dataList = [];
                currentScope.metaData.countryList.dataInProgress = true
                currentScope.metaData.countryList.dataRecived = false;
            });
        }(this));
    }

    this.retriveQuoteData = function(){
        var url = '/quote/'+this.quoteInfo.quoteDetails.number.value+'/quoteDetails';
        //var url = '/_ui/responsive/cat/mockjsons/quoteDetails.json';
        var quoteObj = this.quoteInfo;
        var _this = this;
        httpService.get(url).then(function(response) {
        	if(!!response.data.description){
        		quoteObj.quoteDetails.description.value = response.data.description; 
        	}
        	if(!!response.data.state && !!response.data.state.code){
        		quoteObj.quoteDetails.status.selected = (quoteObj.quoteDetails.status.list.filter(function(v){return v.code == response.data.state.code}))[0];
        	}
        	
        	if(!!response.data.quoteLostReason && !!response.data.quoteLostReason.key){
        		quoteObj.quoteDetails.reasonForQuoteLost.selected = (quoteObj.quoteDetails.reasonForQuoteLost.list.filter(function(v){return v.key == response.data.quoteLostReason.key}))[0];
        	}
        	
        	
        	if(!!response.data.submittedToCustomerDate){
        		quoteObj.quoteDetails.submitedToCustomer.value = new Date(response.data.submittedToCustomerDate);
        	}
        	
            if(!!response.data.csaAdditionalServices){
                quoteObj.cartDetails.CSAOption.details =  response.data.csaAdditionalServices;
                quoteObj.cartDetails.CSAOption.checked =  true;
            }

            if(!!response.data.eppAdditionalServices){
                quoteObj.cartDetails.EPPOption.details =  response.data.eppAdditionalServices;
                quoteObj.cartDetails.EPPOption.checked =  true;
            }
           // quoteObj.metaData.dateModified = _this.retriveDateInCatFormate(new Date(response.data.updatedTime));
           quoteObj.metaData.dateModified  = "";
           
           if(response.data.deliveryAddress!=null && response.data.deliveryAddress != '')
           {
        	quoteObj.metaData.shippngAddressId = response.data.deliveryAddress.id;
           }
           
           if(response.data.paymentAddress!=null && response.data.paymentAddress != '')
    	   {
            quoteObj.metaData.billingAddressId = response.data.paymentAddress.id;
    	   }
           
            quoteObj.customerData.customerName.value =  response.data.quoteCustomer.name;
            quoteObj.customerData.customerEmail.value = response.data.customerEmail;
            quoteObj.customerData.uid = response.data.quoteCustomer.uid;
            if(response.data.deliveryAddress!=null && response.data.deliveryAddress != '')
            {
            quoteObj.customerData.shippingAddress.line1.value = response.data.deliveryAddress.line1;
            quoteObj.customerData.shippingAddress.line2.value = response.data.deliveryAddress.line2;
            quoteObj.customerData.shippingAddress.postalCode.value = response.data.deliveryAddress.postalCode;
            quoteObj.customerData.shippingAddress.town.value = response.data.deliveryAddress.town;
            quoteObj.customerData.shippingAddress.countryList.selectedCountry = response.data.deliveryAddress.country;
            quoteObj.customerData.shippingAddress.regionList.selectedRegion = response.data.deliveryAddress.region;
            quoteObj.customerData.isAddressSame = response.data.isShippingSameAsBilling;
            }
            else
            	{
            	 quoteObj.customerData.shippingAddress.line1.value = response.data.quoteCustomer.defaultShippingAddress.line1;
                 quoteObj.customerData.shippingAddress.line2.value = response.data.quoteCustomer.defaultShippingAddress.line2;
                 quoteObj.customerData.shippingAddress.postalCode.value = response.data.quoteCustomer.defaultShippingAddress.postalCode;
                 quoteObj.customerData.shippingAddress.town.value = response.data.quoteCustomer.defaultShippingAddress.town;
                 quoteObj.customerData.shippingAddress.countryList.selectedCountry = response.data.quoteCustomer.defaultShippingAddress.country;
                 quoteObj.customerData.shippingAddress.regionList.selectedRegion = response.data.quoteCustomer.defaultShippingAddress.region;
                 quoteObj.customerData.isAddressSame = response.data.quoteCustomer.shippingSameAsBilling;
            	}
            
            _this.updateShippingRegionList();
            if(response.data.quoteCustomer.isAddressSame){
                quoteObj.customerData.billingAddress = quoteObj.customerData.shippingAddress;
            }else{
            	 if(response.data.paymentAddress!=null && response.data.paymentAddress != '')
          	   {
                quoteObj.customerData.billingAddress.line1.value = response.data.paymentAddress.line1;
                quoteObj.customerData.billingAddress.line2.value = response.data.paymentAddress.line2;
                quoteObj.customerData.billingAddress.postalCode.value = response.data.paymentAddress.postalCode;
                quoteObj.customerData.billingAddress.town.value = response.data.paymentAddress.town;
                quoteObj.customerData.billingAddress.countryList.selectedCountry = response.data.paymentAddress.country;
                quoteObj.customerData.billingAddress.regionList.selectedRegion = response.data.paymentAddress.region;
          	   }
            	 else
            		 {
            		 quoteObj.customerData.billingAddress.line1.value = response.data.quoteCustomer.defaultBillingAddress.line1;
                     quoteObj.customerData.billingAddress.line2.value = response.data.quoteCustomer.defaultBillingAddress.line2;
                     quoteObj.customerData.billingAddress.postalCode.value = response.data.quoteCustomer.defaultBillingAddress.postalCode;
                     quoteObj.customerData.billingAddress.town.value = response.data.quoteCustomer.defaultBillingAddress.town;
                     quoteObj.customerData.billingAddress.countryList.selectedCountry = response.data.quoteCustomer.defaultBillingAddress.country;
                     quoteObj.customerData.billingAddress.regionList.selectedRegion = response.data.quoteCustomer.defaultShippingAddress.region;
            		 }
                _this.updateBillingRegionList();
            }

            quoteObj.customerData.customerComments.value = response.data.quoteComments;
        }, function(error) {
        });
    }

    this.updateShippingRegionList = function() {
        var currentScope = this.quoteInfo;
        if (currentScope.customerData.shippingAddress.countryList.selectedCountry.isocode) {
            regionService.getRegionsList(currentScope.customerData.shippingAddress.countryList.selectedCountry.isocode).then(function(response) {
                currentScope.customerData.shippingAddress.regionList.dataList = response;
                currentScope.customerData.shippingAddress.regionList.dataInProgress = false
                currentScope.customerData.shippingAddress.regionList.dataRecived = true;
            }, function(error) {
                currentScope.customerData.shippingAddress.regionList.dataList = [];
                currentScope.customerData.shippingAddress.regionList.dataInProgress = true
                currentScope.customerData.shippingAddress.regionList.dataRecived = false;
            });
        }
    }

    this.updateBillingRegionList = function() {
        var currentScope = this.quoteInfo;
        if (currentScope.customerData.billingAddress.countryList.selectedCountry.isocode) {
            regionService.getRegionsList(currentScope.customerData.billingAddress.countryList.selectedCountry.isocode).then(function(response) {
                currentScope.customerData.billingAddress.regionList.dataList = response;
                currentScope.customerData.billingAddress.regionList.dataInProgress = false
                currentScope.customerData.billingAddress.regionList.dataRecived = true;
            }, function(error) {
                currentScope.customerData.billingAddress.regionList.dataList = [];
                currentScope.customerData.billingAddress.regionList.dataInProgress = true
                currentScope.customerData.billingAddress.regionList.dataRecived = false;
            });
        }
    }

    this.init = function() {
        angular.copy(this.quoteInfo.customerData, this.backupCustomerData);
        this.updateCountryList();
        
    }

    this.retriveQuoteDataOnInit = function(){
        if(this.quoteInfo.metaData.editQuote || this.quoteInfo.metaData.replicateQuote){
            this.retriveStautsList();
            //this.retriveQuoteList();
            //this.retriveQuoteData();

        }
    }
    this.init();


    this.submitToDSU = function(){
       var deferred = $q.defer();
        var url = "/quote/"+this.quoteInfo.submitResponse.quoteCode+"/updateQuotes?submittedToDSU="+this.quoteInfo.submitToDSU.value;
        var promise = httpService.get(url).then(function(response) {
            deferred.resolve(response);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    this.submitQuote = function() {
        var deferred = $q.defer();
        var url = "/quote/submit";
        var promise = httpService.postJsonData(url, this.serialiseData()).then(function(response) {
            deferred.resolve(response);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    this.serialiseData = function() {
        var dateFormater = function(customDate) {
            if (!!customDate) {
                return $filter('date')(customDate, "yyyy-MM-dd");
            }
        }
        var catQuoteSerialNumbersData = [];

        if(this.quoteInfo.cartEntries!=null && this.quoteInfo.cartEntries!=undefined && this.quoteInfo.cartEntries.length)
    	{
	        for(var i = 0; i < this.quoteInfo.cartEntries.length; i++){
	            if(this.quoteInfo.cartEntries[i].reservedSerialNos && this.quoteInfo.cartEntries[i].reservedSerialNos.length){
	                catQuoteSerialNumbersData.push({
	                    serialNumbers: this.quoteInfo.cartEntries[i].reservedSerialNos,
	                    endDate: new Date(this.quoteInfo.cartEntries[i].endDate),
	                    configVariantProduct: {
	                        code: this.quoteInfo.cartEntries[i].product.code
	                    }
	                });    
	            }
	        };
    	}
        return {
            quoteCode: this.quoteInfo.quoteDetails.number.value,
            name: this.quoteInfo.quoteDetails.name.value,
            description: this.quoteInfo.quoteDetails.description.value,
            expirationTime: this.quoteInfo.quoteDetails.validTill ? dateFormater(this.quoteInfo.quoteDetails.validTill.value) : null,
            customerName: this.quoteInfo.customerData.customerName.value,
            customerUid: this.quoteInfo.customerData.uid,
            customerEmail: this.quoteInfo.customerData.customerEmail.value,
            isShippingSameAsBilling: this.quoteInfo.customerData.isAddressSame,
            eppAdditionalServices: (!!this.quoteInfo.cartDetails.EPPOption.checked) ? this.quoteInfo.cartDetails.EPPOption.details : '',
            csaAdditionalServices: (!!this.quoteInfo.cartDetails.CSAOption.checked) ? this.quoteInfo.cartDetails.CSAOption.details : '',
            existingCustomer: (this.quoteInfo.customerData.customerType == 'old') ? 'true' : 'false',
            defaultShippingAddress: {
                addressId:this.quoteInfo.metaData.shippngAddressId,
                line1: this.quoteInfo.customerData.shippingAddress.line1.value,
                line2: this.quoteInfo.customerData.shippingAddress.line2.value,
                townCity: this.quoteInfo.customerData.shippingAddress.town.value,
                postcode: this.quoteInfo.customerData.shippingAddress.postalCode.value,
                regionIso: this.quoteInfo.customerData.shippingAddress.regionList.selectedRegion.isocode,
                countryIso: this.quoteInfo.customerData.shippingAddress.countryList.selectedCountry.isocode
            },
            defaultBillingAddress: {
            	addressId: this.quoteInfo.metaData.billingAddressId,
                line1: this.quoteInfo.customerData.billingAddress.line1.value,
                line2: this.quoteInfo.customerData.billingAddress.line2.value,
                townCity: this.quoteInfo.customerData.billingAddress.town.value,
                postcode: this.quoteInfo.customerData.billingAddress.postalCode.value,
                regionIso: this.quoteInfo.customerData.billingAddress.regionList.selectedRegion.isocode,
                countryIso: this.quoteInfo.customerData.billingAddress.countryList.selectedCountry.isocode
            },
            quoteComments: this.quoteInfo.customerData.customerComments.value,
            editMode: false,
            quoteLostReason: this.quoteInfo.quoteDetails.reasonForQuoteLost.selected.key,
            submittedToCustomerDate: this.quoteInfo.quoteDetails.submitedToCustomer ? dateFormater(this.quoteInfo.quoteDetails.submitedToCustomer.value) : null,
            quoteState: (this.quoteInfo.quoteDetails.status.selected ? this.quoteInfo.quoteDetails.status.selected.code : null),
            customerResponseDate: this.quoteInfo.quoteDetails.customerResponseData ? dateFormater(this.quoteInfo.quoteDetails.customerResponseData.value) : null,
            catQuoteSerialNumbersData: {catReserveQuoteData: catQuoteSerialNumbersData}
        }
    };

    this.resetCustomerInformation = function() {
        this.backupCustomerData.customerType = this.quoteInfo.customerData.customerType;
        angular.copy(this.backupCustomerData, this.quoteInfo.customerData);
        this.updateCountryList();

    };

    this.updateQuoteDetails = function(quoteObject) {
        this.quoteInfo.quoteDetails = quoteObject;
    }

    this.updateshippingAddress = function(shippingObject) {
        this.quoteInfo.customerDetails.shippingAddress = shippingObject;
    }

    this.updateBillingAddress = function(billingObject) {
        this.quoteInfo.customerDetails.billingAddress = billingObject;
    }

    this.updateCustomerComments = function(customerComments) {
        this.quoteInfo.customerDetails.comments = customerComments;
    }

    this.updateCustomerType = function(customerType) {
        this.quoteInfo.customerDetails.customerType = customerType;
    }


    this.isFormValid = function() {
        return this.quoteInfo.formValid;
    }

    this.validateFrom = function() {
        var quoteDetailsValidity = this.validateQuoteDetails();
        var customerDetailsValidity = this.validateCustomerDetails();
        this.quoteInfo.formValid = (quoteDetailsValidity && customerDetailsValidity);
    }

    this.validateQuoteDetails = function() {
        var isQuoteDetailsValid = true;
        //Validate Quote Name in Quote Details Page
        if(this.quoteInfo.quoteDetails.description.value&&
            this.quoteInfo.quoteDetails.description.value.length>255){
                isQuoteDetailsValid = false;
        }
        if (!!this.quoteInfo.quoteDetails.name.value) {
            this.quoteInfo.quoteDetails.name.isValid = true;
        } else {
            this.quoteInfo.quoteDetails.name.isValid = false;
            isQuoteDetailsValid = false;
        }
        if(this.quoteInfo.cartDetails.CSAOption.details
            && this.quoteInfo.cartDetails.CSAOption.details.length>255){
                isQuoteDetailsValid = false;
        }
        if(this.quoteInfo.cartDetails.EPPOption.details
            && this.quoteInfo.cartDetails.EPPOption.details.length>255){
                isQuoteDetailsValid = false;
        }

        //Validate data in Quote Details Page
        if (!!this.quoteInfo.quoteDetails.validTill.value) {
            this.quoteInfo.quoteDetails.validTill.isValid = true;
        } else {
            this.quoteInfo.quoteDetails.validTill.isValid = false;
            isQuoteDetailsValid = false;
        }

        return isQuoteDetailsValid;
    }

    this.validateCustomerDetails = function() {
        var isCustomerDetailsValid = true;

        if (!!this.quoteInfo.customerData.customerName.value) {
            this.quoteInfo.customerData.customerName.isValid = true;
        } else {
            this.quoteInfo.customerData.customerName.isValid = false;
            isCustomerDetailsValid = false;
        }

        if (!!this.quoteInfo.customerData.customerEmail.value) {
            if(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/.test(this.quoteInfo.customerData.customerEmail.value)){
                this.quoteInfo.customerData.customerEmail.isValid = true;
            }else{
                this.quoteInfo.customerData.customerEmail.isValid = false;
                isCustomerDetailsValid = false;
            }
        } else {
            this.quoteInfo.customerData.customerEmail.isValid = false;
            isCustomerDetailsValid = false;
        }

        if (!!this.quoteInfo.customerData.shippingAddress.line1.value) {
            this.quoteInfo.customerData.shippingAddress.line1.isValid = true;
        } else {
            this.quoteInfo.customerData.shippingAddress.line1.isValid = false;
            isCustomerDetailsValid = false;
        }
        if (!!this.quoteInfo.customerData.shippingAddress.line2.value) {
            this.quoteInfo.customerData.shippingAddress.line2.isValid = true;
        } else {
            this.quoteInfo.customerData.shippingAddress.line2.isValid = false;
            isCustomerDetailsValid = false;
        }
        if (!!this.quoteInfo.customerData.shippingAddress.town.value) {
            this.quoteInfo.customerData.shippingAddress.town.isValid = true;
        } else {
            this.quoteInfo.customerData.shippingAddress.town.isValid = false;
            isCustomerDetailsValid = false;
        }
        if (!!this.quoteInfo.customerData.shippingAddress.regionList.selectedRegion.countryIso) {
            this.quoteInfo.customerData.shippingAddress.regionList.isValid = true;
        } else {
            this.quoteInfo.customerData.shippingAddress.regionList.isValid = false;
            isCustomerDetailsValid = false;
        }
        if (!!this.quoteInfo.customerData.shippingAddress.countryList.selectedCountry.isocode) {
            this.quoteInfo.customerData.shippingAddress.countryList.isValid = true;
        } else {
            this.quoteInfo.customerData.shippingAddress.countryList.isValid = false;
            isCustomerDetailsValid = false;
        }
        if (!!this.quoteInfo.customerData.shippingAddress.postalCode.value) {
            this.quoteInfo.customerData.shippingAddress.postalCode.isValid = true;
        } else {
            this.quoteInfo.customerData.shippingAddress.postalCode.isValid = false;
            isCustomerDetailsValid = false;
        }


        if (!!this.quoteInfo.customerData.billingAddress.line1.value) {
            this.quoteInfo.customerData.billingAddress.line1.isValid = true;
        } else {
            this.quoteInfo.customerData.billingAddress.line1.isValid = false;
            isCustomerDetailsValid = false;
        }
        if (!!this.quoteInfo.customerData.billingAddress.line2.value) {
            this.quoteInfo.customerData.billingAddress.line2.isValid = true;
        } else {
            this.quoteInfo.customerData.billingAddress.line2.isValid = false;
            isCustomerDetailsValid = false;
        }
        if (!!this.quoteInfo.customerData.billingAddress.town.value) {
            this.quoteInfo.customerData.billingAddress.town.isValid = true;
        } else {
            this.quoteInfo.customerData.billingAddress.town.isValid = false;
            isCustomerDetailsValid = false;
        }
        if (!!this.quoteInfo.customerData.billingAddress.regionList.selectedRegion.countryIso) {
            this.quoteInfo.customerData.billingAddress.regionList.isValid = true;
        } else {
            this.quoteInfo.customerData.billingAddress.regionList.isValid = false;
            isCustomerDetailsValid = false;
        }
        if (!!this.quoteInfo.customerData.billingAddress.countryList.selectedCountry.isocode) {
            this.quoteInfo.customerData.billingAddress.countryList.isValid = true;
        } else {
            this.quoteInfo.customerData.billingAddress.countryList.isValid = false;
            isCustomerDetailsValid = false;
        }
        if (!!this.quoteInfo.customerData.billingAddress.postalCode.value) {
            this.quoteInfo.customerData.billingAddress.postalCode.isValid = true;
        } else {
            this.quoteInfo.customerData.billingAddress.postalCode.isValid = false;
            isCustomerDetailsValid = false;
        }



        // if (!!this.quoteInfo.customerData.customerComments.value){
        //     this.quoteInfo.customerData.customerComments.isValid = true;
        // }
        if(!!this.quoteInfo.customerData.customerComments.value
            &&this.quoteInfo.customerData.customerComments.value.length>255){
            isCustomerDetailsValid = false;
        } 
        return isCustomerDetailsValid;

    }
}]);


