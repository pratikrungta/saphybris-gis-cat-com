catApp.service('regionService', ['httpService','$q',  function(httpService, $q){
	 this.countryList = {};
	 this.regionsList = {};
	 this.cityList = {};

	 this.endPoints = {
	 		country:'/countries',
	 		//states: '/quote/countryIso/regions'
	 		//country:'/_ui/responsive/cat/mockjsons/countryList.json',
	 		states: '/countryIso/regions'
	 };

	 this.getCountryList = function(){
	 		var deferred = $q.defer();
	 		var countryObject = this.countryList;
	 		if(Object.keys(this.countryList).length){
	 			deferred.resolve(this.countryList);
	 		}else{
		 			httpService.get(this.endPoints.country).then(function(response) {
		 				if(!!response.data){
		 					countryObject = response.data;
		 				}
	            deferred.resolve(countryObject);
	        }, function(error) {
	            deferred.reject(error);
	        });
	 		}
	 		return deferred.promise;
	 }

	 this.getRegionsList = function(countryCode){
	 		var deferred = $q.defer();
	 		var countryCode = countryCode;
	 		var regionsObject = this.regionsList;
	 		if(this.regionsList[countryCode]){
	 			deferred.resolve(regionsObject[countryCode]);
	 		}else{
	 			  var regionUrl = this.endPoints.states;
	 			  regionUrl = regionUrl.replace("countryIso",countryCode);
		 			httpService.get(regionUrl).then(function(response) {
		 				if(!!response.data){
		 					regionsObject[countryCode] = response.data;
		 				}
	            deferred.resolve(regionsObject[countryCode]);
	        }, function(error) {
	            deferred.reject(error);
	        });
	 		}
	 		return deferred.promise;
	 }

}]);
