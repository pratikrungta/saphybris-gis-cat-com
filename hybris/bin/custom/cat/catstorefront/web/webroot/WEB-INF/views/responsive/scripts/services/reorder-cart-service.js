catApp.service('reorderCartService', ['httpService','$q', 'constantService',  function(httpService, $q, constantService){
    this.createReorderCartData = function(cartData, operationFlag, previousData, entryNo, currentQuantity){
        var poNumberCountArray = [];
        var reorderCartData = [];
        var poNumberValuesArray = [];
        var reorderEntries = cartData.entries.reduce(function (r, a) {
            if(a.product.salesModelId != null){
                r[a.product.salesModelId+"__"+a.entryNumber] = r[a.product.salesModelId] || [];
                r[a.product.salesModelId+"__"+a.entryNumber].push(a);
            }
            return r;
        }, Object.create(null));
        reorderCartData = reorderEntries;
        angular.forEach(reorderCartData,function(object,salesModelId){
            var qty = 0;
            angular.forEach(object,function(value,configDataIndex){
                var maintainSameState = [];
                angular.forEach(previousData, function(value1, key){
                    angular.forEach(value1, function(data, index){
                        if(data.entryNumber === value.entryNumber) {
                            maintainSameState = data.poNumberValuesArray.purchaseOrderNumberSet;
                        }
                    })
                })

                var obj = {
                    'salesModelId' : salesModelId,
                    'purchaseOrderNumberSet' : maintainSameState,
                    'purchaseOrderErrorSet':{
                        'errorMsg':'',
                        'errorNos':[]
                    },
                    'filledPoCount': 0
                };
                value.poNumberValuesArray = angular.copy(obj);
                if(operationFlag === undefined || operationFlag == "") {
                    var diff = (value.quantity - maintainSameState.length);
                    if(diff > 0){
                        diff = Math.abs(diff);
                        for(var length=0;length < diff;length++){
                            value.poNumberValuesArray.purchaseOrderNumberSet.push('');
                        }
                    }else{
                        diff = Math.abs(diff);
                        var spliceIndex = (value.poNumberValuesArray.purchaseOrderNumberSet.length - diff);
                        value.poNumberValuesArray.purchaseOrderNumberSet.splice(spliceIndex);
                    }
                    
                } else if(operationFlag === 'increment' && value.entryNumber  === entryNo) {
                    value.poNumberValuesArray.purchaseOrderNumberSet.push('');
                } else if(operationFlag === 'decrement' && (value.entryNumber  === entryNo && currentQuantity > 1)) {
                    value.poNumberValuesArray.purchaseOrderNumberSet.pop();
                }
                poNumberCountArray.push({
                    'poNumbersCount' : value.quantity
                });
                value.poNumberValuesArray.filledPoCount = value.poNumberValuesArray.purchaseOrderNumberSet.filter(function(data, key){
                                                            return (data !== "");
                                                        }).length;
                poNumberValuesArray.push({
                    'salesModelId' : salesModelId,
                    'purchaseOrderNumberSet' : [],
                    'purchaseOrderErrorSet':{
                        'errorMsg':'',
                        'errorNos':[]
                    }
                });
                if(value.purchaseOrderNumberList != null && value.purchaseOrderNumberList != undefined && value.purchaseOrderNumberList.length && operationFlag == undefined){
                    angular.forEach(value.purchaseOrderNumberList, function(val, idx) {
                        value.poNumberValuesArray.purchaseOrderNumberSet[idx] = val;
                    })
                    value.poNumberValuesArray.filledPoCount = value.purchaseOrderNumberList.length;
                }
                value.quantity = constantService.getTwoDigitFormattedText(value.quantity);
            });
        });
        angular.forEach(poNumberCountArray,function(object,index){
            for(var length=0;length < object.poNumbersCount;length++){
                if(poNumberValuesArray[index]['purchaseOrderNumberSet'][length] == undefined){
                    poNumberValuesArray[index]['purchaseOrderNumberSet'][length] = '';
                }
            }
        });
        return {
            'reorderCartData' : reorderCartData,
            'poNumberCountArray' : poNumberCountArray,
            'poNumberValuesArray' : poNumberValuesArray
        }
    };
}]);