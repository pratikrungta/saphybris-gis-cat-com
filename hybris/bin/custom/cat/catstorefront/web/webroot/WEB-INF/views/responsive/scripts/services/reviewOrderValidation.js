catApp.factory('reviewOrderValidation', [function(){
    var validationObj={},subscribedComponents={};
        validationObj.reviewOrderPageSubscriptionId='reviewPage';

        validationObj.unSubscribe=function(subscriptionId,pos){
            subscribedComponents[subscriptionId].splice(pos,1);
        }
        validationObj.subscribe=function(subscriptionId,ref,fnName){
            if(!subscribedComponents[subscriptionId]||
                subscribedComponents[subscriptionId].length==0){
                subscribedComponents[subscriptionId]=[];
                }
                subscribedComponents[subscriptionId].push({
                                                            subscriptionId:subscriptionId,
                                                            context:ref,
                                                            fnName:fnName
                                                        });

        };
        validationObj.notify=function(subscriptionId,callback){
            var result=[];
            if(subscribedComponents[subscriptionId]){
                subscribedComponents[subscriptionId].map(function(component,index){
                    if(subscriptionId===validationObj.reviewOrderPageSubscriptionId){
                        result.push({isAddressEmpty:component.context[component.fnName]()});
                    }
                    
                })
                callback?callback(result):'';
                return;
            }
        }

    return validationObj;
}])