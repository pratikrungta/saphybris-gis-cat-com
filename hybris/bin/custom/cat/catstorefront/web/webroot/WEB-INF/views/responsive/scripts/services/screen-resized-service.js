catApp.service('screenResizedService', ['$window','$timeout', function($window,$timeout) {
    var _this = this;

	this.metaInfo = {
		isMobile: false
	};
    
    this.angularWindow = angular.element($window);
    this.windowWidth = this.angularWindow.width();
    this.angularWindow.bind('resize', function () {
        $timeout(function(){
            _this.updateMobileMetaData();
        },0);     
    });

    this.updateMobileMetaData = function(){
        if(_this.angularWindow.width() <= parseInt(screenSmMax)){
            _this.metaInfo.isMobile = true;
        }else{
            _this.metaInfo.isMobile = false;
        }
        _this.updateValuesAfterMetaChange();
    };

    this.updateValuesAfterMetaChange = function(){
        // TODO: overwrite this function where the service is imported to get the call on resize
    }

    this.updateMobileMetaData();

}]);