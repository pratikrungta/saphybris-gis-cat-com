catApp.service('screenResizeService', ['$window','$timeout', function($window, $timeout){
    this.resizeMetaData = {
        isMobileScreen: false,
        screenWidth: 1024
    }

    this.angularWindow = angular.element($window);
    this.windowWidth = this.angularWindow.width();
    var _this = this;
    this.angularWindow.bind('resize', function () {
        _this.updateMetaData();
    });

    this.updateMetaData = function(){
        $timeout(function(){
            if(_this.angularWindow.width() <= parseInt(screenSmMax)){
                _this.resizeMetaData.isMobileScreen = true;
            }else{
                _this.resizeMetaData.isMobileScreen = false;
            }
        },0);
    }

    this.updateMetaData();
}]);
