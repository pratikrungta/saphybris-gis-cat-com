var a = {};
catApp.service('specCheckHttpService', ['$http', function($http) {
	var config = {
		headers : {
      'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
    }
  };
  var baseUrl = '//www.speccheck.com/api/';
	this.post = function(path, data, successCallBack, errorCallBack) {
    config.headers = Object.assign({}, config.headers, a);
    //console.log('config: ', config);
    return $http.post(baseUrl+path, data, config).then(function(response){ 
      if(successCallBack){
        successCallBack(response);
      }else{
        //console.log("success with response: ", response);
      }
    }, function(err){
      if(errorCallBack){
        errorCallBack(err);
      }else{
        //console.log('error in the call: ', err);
      }
    });
  };

  this.get = function(path, successCallBack, errorCallBack) {
    config.headers = Object.assign({}, config.headers, a);
    //console.log('config: ', config);
    return $http.get(baseUrl+path, config).then(function(response){ 
      if(successCallBack){
        successCallBack(response);
      }else{
        //console.log("success with response: ", response);
      }
    }, function(err){
      if(errorCallBack){
        errorCallBack(err);
      }else{
        //console.log('error in the call: ', err);
      }
    });
  }
}]);