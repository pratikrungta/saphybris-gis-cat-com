import de.hybris.platform.cronjob.enums.*
import de.hybris.platform.servicelayer.cronjob.PerformResult
import de.hybris.platform.servicelayer.search.*
import de.hybris.platform.servicelayer.model.*
import com.cat.bcp.core.enums.LaneTypeEnum
import com.cat.bcp.core.enums.CompatibilityStatusEnum
  
searchService = spring.getBean("flexibleSearchService")
modelService = spring.getBean("modelService")

catalogVersionQuery=new FlexibleSearchQuery("SELECT {PK} FROM {CatalogVersion} WHERE {version}='Staged' AND {catalog} IN ({{ Select {pk} from {Catalog} where {id} = 'NAProductCatalog' }})");
CatalogVersion_pk= searchService.search(catalogVersionQuery).getResult().get(0);

Config_query = new FlexibleSearchQuery("select {pk} from {ConfigVariantProduct! as config},{category as cat}, {CategoryProductRelation as rel} where {config.pk} not in ({{select {configVariantProduct} from {CompatibilityInfo} where {laneType} in ({{select {pk} from {LaneTypeEnum} where {code}='LANE1'}}) }}) and {cat.code}!='utv' and {cat.pk}={rel.source} and {rel.target}={config.pk} and {config.catalogversion} ='"+CatalogVersion_pk.getPk().toString()+"'");

searchService.search(Config_query).getResult().each {
  
com=modelService.create("CompatibilityInfo")
             com.setConfigVariantProduct(it)
              com.setLaneType(LaneTypeEnum.LANE1)
                com.setCompatibilityStatus(CompatibilityStatusEnum.COMPATIBLE)
              com.setCatalogVersion(CatalogVersion_pk)
             modelService.save(com)

}
println "All records inserted successfully without exceptions"




