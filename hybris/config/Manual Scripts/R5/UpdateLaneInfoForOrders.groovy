import de.hybris.platform.cronjob.enums.*
import de.hybris.platform.servicelayer.cronjob.PerformResult
import de.hybris.platform.servicelayer.search.*
import de.hybris.platform.servicelayer.model.*
import com.cat.bcp.core.enums.LaneTypeEnum 
  
searchService = spring.getBean("flexibleSearchService")
modelService = spring.getBean("modelService")
query = new FlexibleSearchQuery("select DISTINCT {O.PK} from  {  	 order as O  	 JOIN OrderEntry AS OE ON ({OE.Order} = {O.PK})  	 JOIN ConfigVariantProduct AS cvp ON ({OE.product}={cvp.pk})  	 join CATEGORYPRODUCTRELATION  as pcpr on ({pcpr.target}={cvp.pk}) 	 join category as pc on ({pc.pk}={pcpr.source})      } where ({pc.code} !='utv' and {pc.code}!='accessory') and  {O.laneType} is  null");
searchService.search(query).getResult().each {
it.laneType=LaneTypeEnum.LANE1
modelService.save(it)
}

query = new FlexibleSearchQuery("select DISTINCT {O.PK} from { order as O  	 JOIN OrderEntry AS OE ON ({OE.Order} = {O.PK})  	 JOIN ConfigVariantProduct AS cvp ON ({OE.product}={cvp.pk})  	 join CATEGORYPRODUCTRELATION  as pcpr on ({pcpr.target}={cvp.pk}) 	 join category as pc on ({pc.pk}={pcpr.source})  }  where ({pc.code} ='utv' and {pc.code}!='accessory') and {O.laneType} is null");
searchService.search(query).getResult().each {
it.laneType=LaneTypeEnum.LANE3
modelService.save(it)
}

