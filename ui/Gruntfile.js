module.exports = function(grunt) {
  var pkg = grunt.file.readJSON('package.json');
  grunt.initConfig({
    pkg: pkg,
    less: {
      default: {
        options: {
          outputStyle: 'compressed',
          sourceMap: false
        },
        files: {
          '_ui/responsive/cat/css/core.css': 'libs/core/styles/_styles.less'
        }
      }
    },
    watch: {
      less: {
        files: ['libs/core/styles/*.less', 'libs/core/styles/**/*.less'],
        tasks: ['less']
      },
      js: {
        files: ['libs/core/scripts/*.js', 'libs/core/scripts/**/*.js'],
        tasks: ['import_js:files','uglify','import_js:catCoreMin']
      }
    },
    open: {
      delayed: {
        path: 'http://localhost:3000',
        app: 'Google Chrome',
        options: {
          openOn: 'serverListening',
        }
      }
    },
    run: {
      tool: {
        cmd: 'http-server',
      }
    },
    exec: {
      runHttpServer: {
        cmd: 'start cmd.exe /k "\"FOR /F \"tokens=5 delims= \" %P IN (\'netstat -a -n -o ^| findstr :3001\') DO TaskKill.exe /PID %P /T /F\" & http-server -p 3001 -c-1"'
      }
    },
    cssmin : {
    	options: {
        mergeIntoShorthands: false,
        roundingPrecision: -1
      },
      dist: {
        src: '_ui/responsive/cat/css/core.css',
        dest: '_ui/responsive/cat/css/core.min.css'
      }
    },
    import_js: {
      files: {
        expand: true,
        cwd: 'libs/core/scripts/',
        src: ['catScripts.js'],
        dest: 'libs/core/scripts/.temp',
        ext: '.js'
      },
      catCoreMin:{
        expand: true,
        cwd: 'libs/core/scripts/',
        src: ['core.js'],
        dest: '_ui/responsive/cat/js',
        ext: '.min.js'
      }
    },
    uglify: {
        my_target: {
        files: {
          'libs/core/scripts/.temp/catScripts.min.js': ['libs/core/scripts/.temp/catScripts.js']
        }
      }
    },
  });
  
 

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-import-js');
  grunt.loadNpmTasks('grunt-exec');

  grunt.registerTask('server', function() {
    var server = require('myServer');
    server.listen(3000, function(err) {
      if (!err) {
        grunt.log.writeln('Server started');
        grunt.event.emit('serverListening'); // triggers open:delayed
      }
    });
  });


   grunt.registerTask('default', ['less', 'cssmin','import_js:files','uglify','import_js:catCoreMin','exec', 'watch']);


};