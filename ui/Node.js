var drakov = require('drakov');

var argv = {
    sourceFiles: 'mock/**.md',
    serverPort: 3000,
    staticPaths: './**.html',
    stealthmode: false,
    disableCORS: true,
    delay: 100,
    watch: true
};

drakov.run(argv, function(){
});