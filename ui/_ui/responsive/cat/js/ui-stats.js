
catApp.requires =['ui.bootstrap','ui.router'];


catApp.config(['$stateProvider','$urlRouterProvider',function($stateProvider, $urlRouterProvider) { 
	$urlRouterProvider.otherwise('/index.html');
console.log("temp");
	$stateProvider.state('plp', {
						url: '/pages/plp.html',
            views: {		
				      'header': { 
				      	'templateUrl': '/modules/general/header.html'
				       },		
				      'footer': {
				      	'templateUrl':'/modules/general/footer.html'
				      },	
				      'navigation': {
				      	'templateUrl':'/modules/general/navigation.html'
				      },
				      'plpleftrefinement': {
				      	'templateUrl':'/modules/plp/leftRefinement.html'
				      },
				      'plpsearchresults':{
				      	'templateUrl':'/modules/plp/searchResults.html'
				      }
			    }	
        }).state('pdp',{
        		url: '/pages/pdp.html',
        		views: {		
				      'header': { 
				      	'templateUrl': '/modules/general/header.html'
				       },		
				      'footer': {
				      	'templateUrl':'/modules/general/footer.html'
				      },	
				      'navigation': {
				      	'templateUrl':'/modules/general/navigation.html'
				      },
				      'pdpleftrefinement':{
				      	'templateUrl':'/modules/pdp/leftRefinement.html'
				      },
				      'pdpproductdetails':{
				      	'templateUrl':'/modules/pdp/productData.html'
				      }
			    }	
        }).state('landingPage',{
        		url: '/pages/landingPage.html',
        		views: {		
				      'header': { 
				      	'templateUrl': '/modules/general/header.html'
				       },		
				      'footer': {
				      	'templateUrl':'/modules/general/footer.html'
				      },	
				      'navigation': {
				      	'templateUrl':'/modules/general/navigation.html'
				      },
				      'catSearch':{
				      	'templateUrl':'/modules/landingPage/catSearch.html'
				      },
				      'favoriteProducts':{
				      	'templateUrl':'/modules/landingPage/favoriteProducts.html'
				      },
				      'recentlyViewed':{
				      	'templateUrl':'/modules/landingPage/recentlyViewed.html'
				      },
				      'lowStockAlert':{
				      	'templateUrl':'/modules/landingPage/lowStockAlert.html'
				      },
			    }	
        }).state('IMLandingPage',{
        		url: '/pages/IMLandingPage.html',
        		views: {		
				      'header': { 
				      	'templateUrl': '/modules/general/header.html'
				       },		
				      'footer': {
				      	'templateUrl':'/modules/general/footer.html'
				      },	
				      'navigation': {
				      	'templateUrl':'/modules/general/navigation.html'
				      },
				      'catSearch':{
				      	'templateUrl':'/modules/IMLanding/catSearch.html'
				      },
				      'favoriteProducts':{
				      	'templateUrl':'/modules/IMLanding/favoriteProducts.html'
				      },
				      'recentlyViewed':{
				      	'templateUrl':'/modules/IMLanding/recentlyViewed.html'
				      },
				      'lowStockAlert':{
				      	'templateUrl':'/modules/IMLanding/lowStockAlert.html'
				      },
				      'catCompare':{
				      	'templateUrl':'/modules/IMLanding/catPdtCompare.html'
				      }
			    }	
        }).state('IMOrderHistoryPage',{
        		url: '/pages/IMOrderHistoryPage.html',
        		views: {		
				      'header': { 
				      	'templateUrl': '/modules/general/header.html'
				       },		
				      'footer': {
				      	'templateUrl':'/modules/general/footer.html'
				      },	
				      'navigation': {
				      	'templateUrl':'/modules/general/navigation.html'
				      },
				      'orderTable': {
				      	'templateUrl' : '/modules/IMOrderHistory/orderHistoryTable.html'
				      }
			    }	
        }).state('orderSummaryPage',{
        		url: '/pages/orderSummaryPage.html',
        		views: {		
				      'header': { 
				      	'templateUrl': '/modules/general/header.html'
				       },		
				      'footer': {
				      	'templateUrl':'/modules/general/footer.html'
				      },	
				      'navigation': {
				      	'templateUrl':'/modules/general/navigation.html'
				      },
				      'orderDetails': {
				      	'templateUrl' : '/modules/orderSummary/orderDetails.html'
				      },
				      'dealerDetails': {
				      	'templateUrl' : '/modules/orderSummary/dealerDetails.html'
				      }
			    }	
        });
}]);