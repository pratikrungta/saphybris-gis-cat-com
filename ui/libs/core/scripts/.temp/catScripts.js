
var catApp = angular.module('cat-app', ['ui.bootstrap','ngSanitize']);

catApp.run(['catService', '$location', function(catService, $location) {
    catService.selectedProductFamilyCode = $location.search().categoryCode || "";
    catService.selectedSalesModelCode = $location.search().salesModelId || ""
    catService.selectedPrimaryOfferingCode = $location.search().primaryOfferingId || "";

    if (!!$location.search().pdcFlag) {
        catService.plp.isPDCChecked = $.parseJSON($location.search().pdcFlag);
    } else {
        catService.plp.isPDCChecked = false;
    }

    if (!!$location.search().pdcFlag) {
        catService.plp.isMFUChecked = $.parseJSON($location.search().mfuFlag);
    } else {
        catService.plp.isMFUChecked = false;
    }

    if (!!$location.search().pdcFlag) {
        catService.plp.isWarehouseChecked = $.parseJSON($location.search().wsFlag);
    } else {
        catService.plp.isWarehouseChecked = false;
    }

    if(!!$location.search().page && !!Number($location.search().page)){
        catService.plp.currentPage = Number($location.search().page);
    }else{
        catService.plp.currentPage = 0;
    }

}]);

catApp.config(['$locationProvider',
    function($locationProvider) {
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }
]);

catApp.service('httpService', ['$http', function($http) {
	var config = {
		headers : {
      'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
    }
	};
	this.post = function(path, data) {
    return $http.post(path, data, config);
  };
  this.get = function(path) {
    return $http.get(path);
  }
}]);

catApp.factory('catService', [function(){
	 var catData = {
	 				selectedProductFamilyCode: "",
			 		selectedSalesModelCode: "",
			 		selectedPrimaryOfferingCode: "",
		 	plp: {
			 		isPDCChecked: false,
			 		isMFUChecked: false,
			 		isWarehouseChecked: false,
			 		currentPage: 0

		 	}
	 };
	 return catData;
}]);

catApp.factory('catEndpointService', [function(){
	 var endPointsData = {
	 	productFamilyEnpoint: '/_ui/responsive/cat/mockjsons/selectCategory.json',
	 	salesModelEndpoint: '/_ui/responsive/cat/mockjsons/selectSalesModel.json',
	 	searchResultsEndPoint: '/_ui/responsive/cat/mockjsons/searchResults.json',
	 	typeaheadSearchEndPoint: '/_ui/responsive/cat/mockjsons/catSearch.json',

	 	//productFamilyEnpoint: '/search/selectCategory',
	 	//salesModelEndpoint: '/search/selectSalesModel',
	 	//searchResultsEndPoint: '/search/results',
	 	
	 };
	 return endPointsData;
}]);

catApp.factory('constantService', [function(){
	 var constantData = {
	 		globalSorryHeader: 'We\'re sorry',
	 		searchResultsErrorMessage: ' Something went wrong.Thanks for noticing, we are working on this!',
	 		noSearchResultsFound: 'Sorry, no results for your current search.Please try changing the search attributes.',
	 };
	 return constantData;
}]);

catApp.directive('stopEvent', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            element.bind('click', function (e) {
                e.stopPropagation();
            });
        }
    };
 });

        catApp.directive("repeatDone", function(){
            return {
                restrict: "A",
                link: function (scope, element, attrs) {
                    if (scope.$last) {
                        scope.$eval(attrs.repeatDone);
                    }
                }
            };
        });

catApp.filter('titleCase', function() {
    return function(input) {
      input = input || '';
      return input.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    };
  })

catApp.directive('ngCatMasonry', function($timeout) {
    return function(scope, element, attrs) {
        if (scope.$last) {
            $timeout(function() {
                var parent = element.parent();
                var masonry = new Masonry(parent[0], {
                    itemSelector: '.item',
                    isAnimated: true,
                    animationOptions: {
                        duration: 750,
                        easing: 'linear',
                        queue: false
                    },
                    transitionDuration: "0.4s",
                    isResizable: false
                });
            });
        }
    };
})

catApp.directive('catSearch', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: '/libs/core/scripts/templates/typeahead.html',
        scope: true,
        controller: ['$scope', 'catEndpointService', 'httpService', 'filterFilter', function($scope, catEndpointService, httpService, filterFilter) {
            $scope.view = {
                searchResults: [],
                inputDirty: false,
                emptyResults: false,
                productFamily: 'Product Family',
                salesModel: 'Sales Model',
                baseModel: 'Base Model'
            }

            $scope.getCatResults = function(search) {
                httpService.get(catEndpointService.typeaheadSearchEndPoint).then(function(response) {
                  console.log(new Date());
                    $scope.view.searchResults = [];
                    if (!!response.data && response.data.productFamily) {
                        (response.data.productFamily).forEach(function(obj, index) {
                            if (index == 0) {
                                obj.firstInGroup = true;
                           } else {
                                obj.firstInGroup = false;
                            }
                            obj.displayValue = obj.categoryName;
                            obj.group = $scope.view.productFamily;
                            $scope.view.searchResults.push(obj);
                        });
                        var lastobj =$scope.view.searchResults.pop();
                        lastobj.lastObject = true;
                        $scope.view.searchResults.push(lastobj);
                    }
                    if (!!response.data && response.data.salesModels) {
                        (response.data.salesModels).forEach(function(obj, index) {
                            if (index == 0) {
                                obj.firstInGroup = true;
                            } else {
                                obj.firstInGroup = false;
                            }
                            obj.displayValue = obj.salesModelName;
                            obj.group = $scope.view.salesModel;
                            $scope.view.searchResults.push(obj);
                        });
                        var lastobj =$scope.view.searchResults.pop();
                        lastobj.lastObject = true;
                        $scope.view.searchResults.push(lastobj);
                    }
                    if (!!response.data && response.data.baseModels) {
                        (response.data.baseModels).forEach(function(obj, index) {
                            if (index == 0) {
                                obj.firstInGroup = true;
                            } else {
                                obj.firstInGroup = false;
                            }
                            obj.displayValue = obj.primaryOfferingName;
                            obj.group = $scope.view.baseModel;
                            $scope.view.searchResults.push(obj);
                        });
                        var lastobj =$scope.view.searchResults.pop();
                        lastobj.lastObject = true;
                        $scope.view.searchResults.push(lastobj);
                    }
                });
                 console.log(new Date());
                return $scope.view.searchResults;

            };


            $scope.onSelect = function($item, $model, $label) {
                if (!!$model && $model.group){

                }
            }

            $scope.$watch('typeaheadIsOpen', function(newVal, oldVal) {
                if (newVal == oldVal) {
                    return;
                }
                if (newVal) {
                    var searchField = angular.element.find('input');
                    var width = searchField[0].offsetWidth;

                    var dropdown = angular.element.find('.dropdown-menu');
                    angular.element(dropdown[0]).css('width', (width + 'px'));
                }
            });
        }]
      }
    });

catApp.directive('catCard', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            superHeader: '@',
            primaryHeader: '@',
            secondryHeader: '@',
            image: '@',
            minQty: '@',
            minQtyText: '@',
            maxQty: '@',
            maxQtyText: '@',
            recommendedQty: '@',
            recommendedQtyText: '@',
            currentQty: '@',
            currentQtyText: '@',
            qtyMeasurement: '@',
            favoriteMarked: '@',
            cardType: '@',
            productIdForCommunication: '@',
            pdpLink: '@',
            reorderQty: '@'
        },
        templateUrl: '/libs/core/scripts/templates/cardTemplate.html',
        compile: function(tElem, attrs) {
            //do optional DOM transformation here
            return function(scope, elem, attrs) {
                if (!!attrs.cardType) {
                    if (attrs.cardType == 'IM') {
                        scope.imCard = true;
                    } else {
                        scope.spCard = true;
                    }
                }
                scope.canReorder = false;
                if (!!attrs.canReorder && attrs.canReorder == "true") {
                    scope.canReorder = true;
                }
                scope.bookMarked = false;
                if (!!attrs.bookMarked && attrs.bookMarked == "true") {
                    scope.bookMarked = true;
                }
                debugger;
                if (!!attrs.reorderDefaultValue && !!Number(attrs.reorderDefaultValue)) {
                    scope.reorderqty = Number(attrs.reorderDefaultValue);
                    scope.defaultValue =  scope.reorderqty;
                }
            };
        },
        controller: ['$scope', '$window', function($scope, $window) {
            $scope.removeFromBookMark = function() {
                if (!!$scope.productIdForCommunication) {}
            }
            $scope.addFromBookMark = function() {
                if (!!$scope.productIdForCommunication) {

                }
            }
            $scope.navigateToPDP = function() {
                if (!!$scope.pdpLink) {
                    $window.location.href = $scope.pdpLink;
                }
            }
            $scope.decreaseQuantity = function() {
                if ($scope.reorderqty > 0) {
                    $scope.reorderqty = $scope.reorderqty-1;
                }else{
                  $scope.reorderqty = 0;  
                }
            }
            $scope.increaseQuantity = function() {
                $scope.reorderqty = $scope.reorderqty+1;
            }
            $scope.resetInputValue = function(){
                debugger;
                if(!$scope.reorderqty){
                    $scope.reorderqty = $scope.defaultValue;
                }
            }
        }]
    };
});

catApp.directive('slider', function() {
    return {
        restrict: 'EA',
        transclude: true,
        replace: true,
        scope: {},
        compile: function(tElem, attrs) {
            //var $ele = angular.element(tElem);
            //$ele.prepend(angular.element(''));
            //$ele.append(angular.element(''));
            return {
                pre: function(scope, elem, attrs, nullCtrl, transcludeFn) {
                    transcludeFn(scope, function(clone) {
                        elem.append(clone);
                    });
                },
                post: function(scope, element, attributes, controller, transcludeFn) {
                    $("#owl-demo").owlCarousel({
                        navigation: true,
                        items: 3,
                        navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
                        itemsDesktop: [1199, 3],
                        itemsDesktopSmall: [979, 2],
                        itemsTablet: [768, 1],
                        itemsTabletSmall: false,
                        itemsMobile: [479, 1],
                        slideSpeed: 800,
                        rewindSpeed: 1000
                    });
                }
            }
        }
    };
});

catApp.directive('restrictTo', [function() {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var re = RegExp(attrs.restrictTo);
            var exclude = /Backspace|Enter|Tab|Delete|Del|ArrowUp|Up|ArrowDown|Down|ArrowLeft|Left|ArrowRight|Right/;
            var excludeKeyCode = /^(8|13|9|46|37|38|40)$/;

            element[0].addEventListener('keydown', function(event) {
                var keyCode = event.keyCode || event.which;
                if(keyCode >= 96 && keyCode <= 105){
                    keyCode -= 48;
                }
                keyValue = String.fromCharCode(keyCode);
                if ( !excludeKeyCode.test(keyCode) && !re.test(keyValue)) {
                    event.preventDefault();
                }
            });
        }
    }
}]);

catApp.controller('landingPageController', ['httpService', 'catEndpointService', '$scope', function(httpService, catEndpointService, $scope) {

  var vm = this;

  vm.init = function() {
    console.log("Initiated");
  };

  vm.productFamilyMenu = {
    isOpen: false,
    isDisabled: true,
    selectedCode: "",
    selectedProductName: ""
  };

  vm.salesModelMenu = {
    isOpen: false,
    isDisabled: true,
    listOfSalesModel: [],
    selectedCode: "",
    selectedModelName: ""
  };

  vm.primaryOffering = {
    isOpen: false,
    isDisabled: true,
    listOfPrimaryOffering: [],
    selectedCode: "",
    selectedPrimaryOffering: ""
  };

  vm.validForm = false;
  vm.init();

  vm.productFamilyDropdown = function($event) {

    setTimeout(function() {
      if ($("#product-family").hasClass("open")) {
        $("#product-family").removeClass("open");
      } else {
        $("#product-family").addClass("open");
      }
    }, 0);


    vm.productFamilyMenu.isOpen = !vm.productFamilyMenu.isOpen;
    return false;
  }

  vm.productFamilySelected = function(code, name) {
    vm.productFamilyMenu.selectedCode = code;
    vm.productFamilyMenu.selectedProductName = name;

    vm.salesModelMenu.isDisabled = true;
    vm.primaryOffering.isDisabled = true;

    vm.validForm = false;

    vm.salesModelMenu.selectedModelName = "";
    vm.primaryOffering.selectedPrimaryOffering = "";

    vm.primaryOffering.selectedCode= "";
    vm.salesModelMenu.selectedCode = "";

    httpService.get(catEndpointService.productFamilyEnpoint + "?categoryCode=" + vm.productFamilyMenu.selectedCode).then(function(response) {
      if (response.status == 200) {
        vm.salesModelMenu.listOfSalesModel = response.data;
        vm.salesModelMenu.isDisabled = false;
      }
    }, function(error) {});
    $("#product-family").removeClass("open");
  }

  vm.salesModelSelected = function(code, name) {
    vm.salesModelMenu.selectedCode = code;
    vm.salesModelMenu.selectedModelName = name;
    vm.primaryOffering.selectedPrimaryOffering = "";
    vm.primaryOffering.selectedCode= "";
     vm.validForm = false;

    httpService.get(catEndpointService.salesModelEndpoint + "?productCode=" + vm.salesModelMenu.selectedCode).then(function(response) {
      if (response.status == 200) {
        vm.primaryOffering.listOfPrimaryOffering = response.data;
        vm.primaryOffering.isDisabled = false;
      }
    }, function(error) {});
  }

  vm.primaryOfferingSelected = function(code, name) {
  	vm.validForm = true;
    vm.primaryOffering.isDisabled = false;
    vm.primaryOffering.selectedCode = code;
    vm.primaryOffering.selectedPrimaryOffering = name +" ("+code+")";
}

}]);

$(function() {
  $(document).click(function(event) {
    if (!$(event.target).closest('.cat-dropdown').length) {
      $(".cat-dropdown").removeClass("open");
    }
  });
});

catApp.controller('plpController', ['httpService', 'catEndpointService', '$scope', '$location', '$window', '$compile', 'constantService', '$timeout', 'catService', function(httpService, catEndpointService, $scope, $location, $window, $compile, constantService, $timeout, catService) {

    var vm = this;
    vm.view = {
        productFamily: {
            isDisabled: false,
            isOpen: false,
            selectedProductName: "",
            selectedCode: ""
        },
        salesModel: {
            isDisabled: true,
            selectedModelName: "",
            selectedCode: "",
            listOfSalesModel: []
        },
        primaryOffering: {
            isDisabled: true,
            selectedPrimaryOffering: "",
            selectedCode: "",
            listOfPrimaryOffering: []
        },
        location: {
            isWareHouseChecked: false,
            iscatPDCChecked: false,
            isManufacturingChecked: false,
            hasWareHouseUnit: false,
            hascatPDCUnit: false,
            hasManufacturing: false
        },
        searchResults: [],
        searchResultsMetaData: "",
        hasValidSearchParams: false,
        pageLoadingParams: {
            searchResultsLoaded: false,
            salesModelLoaded: false,
            primaryOfferingLoaded: false,
            pagedLoaded: false,
            ajaxCallSuccess: true
        },
        pagination: {
            currentPage: 1,
            totalPages: 10,
            displayGroups: 3,
            elementsForCurrentPage: 8,
            startIndex: 1,
            totalItems: 100,
            hybrisPageNumber: 0,
            clickedOnPagination: false
        },
        initialLoad: true,
        primaryOfferingForCards: ""
    }


    vm.constantService = constantService;

    vm.updatePageLoadingParams = function() {
        vm.view.pageLoadingParams.pagedLoaded = (vm.view.pageLoadingParams.searchResultsLoaded && vm.view.pageLoadingParams.salesModelLoaded && vm.view.pageLoadingParams.primaryOfferingLoaded);
    }

    vm.setSearchParams = function(productFamily, salesModel, primaryOffering) {}


    vm.productFamilySelected = function(code, name) {
            vm.view.productFamily.selectedCode = code;
            vm.view.productFamily.selectedProductName = name;

            vm.view.salesModel.isDisabled = true;
            vm.view.primaryOffering.isDisabled = true;
            vm.view.hasValidSearchParams = false;


            vm.view.salesModel.selectedModelName = "";
            vm.view.primaryOffering.selectedPrimaryOffering = "";

            vm.view.primaryOffering.selectedCode = "";
            vm.view.salesModel.selectedCode = "";
            vm.view.productFamily.isOpen = false;



            httpService.get(catEndpointService.productFamilyEnpoint + "?categoryCode=" + vm.view.productFamily.selectedCode).then(function(response) {
                if (response.status == 200) {
                    vm.view.salesModel.listOfSalesModel = response.data;
                    vm.view.salesModel.isDisabled = false;
                }
            }, function(error) {});
        },

        vm.salesModelSelected = function(code, name) {
            vm.view.salesModel.selectedCode = code;
            vm.view.salesModel.selectedModelName = name;
            vm.view.primaryOffering.selectedPrimaryOffering = "";
            vm.view.primaryOffering.selectedCode = "";

            vm.view.hasValidSearchParams = true;
            vm.view.primaryOffering.isDisabled = false;

            httpService.get(catEndpointService.salesModelEndpoint + "?productCode=" + vm.view.salesModel.selectedCode).then(function(response) {
                if (response.status == 200) {
                    vm.view.primaryOffering.listOfPrimaryOffering = response.data;
                    vm.view.primaryOffering.isDisabled = false;
                }
            }, function(error) {});
        }

    vm.primaryOfferingSelected = function(code, name) {
        vm.view.hasValidSearchParams = true;
        vm.view.primaryOffering.isDisabled = false;
        vm.view.primaryOffering.selectedCode = code;
        vm.view.primaryOffering.selectedPrimaryOffering = name+" ("+ code +")";
    }

    vm.updateSearchData = function() {
        vm.view.pageLoadingParams.searchResultsLoaded = false;
        vm.updatePageLoadingParams();
        vm.resetLocationTypes();
        vm.retriveSearchData(); 
        {
            ACC.plp.overlay().hide();
            $("#nav-mobile-selection").hide();
            $("#nav-mobile-locations").hide();
            $("body").removeClass("modal-open");
        }
    }

    


    vm.retriveSearchData = function() {
        vm.backupSearchData();

        var anyOfCheckBoxChecked = (vm.view.location.isWareHouseChecked || vm.view.location.iscatPDCChecked || vm.view.location.isManufacturingChecked);
        var show='Page';

        var inWarehouseFlag = (anyOfCheckBoxChecked)?vm.view.location.isWareHouseChecked:true;
        var inStockPDCFlag = (anyOfCheckBoxChecked)?vm.view.location.iscatPDCChecked:true;
        var inStockMFUFlag = (anyOfCheckBoxChecked)?vm.view.location.isManufacturingChecked:true;

        var url = catEndpointService.searchResultsEndPoint+"?q=&category="+vm.view.productFamily.selectedCode+"&salesModelId="+vm.view.salesModel.selectedCode+"&primaryOfferingId="+vm.view.primaryOffering.selectedCode+"&pdcFlag="+inStockPDCFlag+"&mfuFlag="+inStockMFUFlag+"&wsFlag="+inWarehouseFlag+"&page="+vm.view.pagination.hybrisPageNumber+"&show="+show;
        vm.view.primaryOfferingForCards = vm.view.productFamily.selectedProductName;
        httpService.get(url).then(function(response) {
            if(response.status == 200){
            vm.view.pageLoadingParams.searchResultsLoaded = true;
            vm.view.pageLoadingParams.ajaxCallSuccess = true;

            vm.updatePageLoadingParams();
            vm.view.searchResults = [];
            response.data.results.forEach(function(item) {
                item.primaryOfferingForCards = vm.view.productFamily.selectedProductName;
                vm.view.searchResults.push(item);
            });
            vm.view.searchResultsMetaData = response.data.pagination;
            vm.view.pagination.totalItems = response.data.pagination.totalNumberOfResults;
            vm.view.pagination.elementsForCurrentPage = response.data.pagination.pageSize;
            vm.view.location.hasWareHouseUnit = (!!response.data.warehouseMap)?$.parseJSON(response.data.warehouseMap.inDealerWS)
            :true;
            if(!vm.view.location.hasWareHouseUnit){
                vm.view.location.isWareHouseChecked = false;
            }
            vm.view.location.hascatPDCUnit = (!!response.data.warehouseMap)?$.parseJSON(response.data.warehouseMap.inPDC)
            :true;
            if(!vm.view.location.hascatPDCUnit){
                vm.view.location.iscatPDCChecked = false;
            }
            vm.view.location.hasManufacturing =  (!!response.data.warehouseMap)?$.parseJSON(response.data.warehouseMap.inMFU)
            :true;
            if(!vm.view.location.hasManufacturing){
                    vm.view.location.isManufacturingChecked = false;
            }
            if (vm.view.searchResults.lenght == 0) {
                vm.view.searchResults();
                vm.view.pageLoadingParams.searchResultsLoaded = true;
                vm.view.pageLoadingParams.ajaxCallSuccess = true;
            }
        }else{
         vm.view.pageLoadingParams.ajaxCallSuccess = false;
            vm.view.pageLoadingParams.searchResultsLoaded = true;
            vm.updatePageLoadingParams();   
        }
        }, function(error) {
            vm.view.pageLoadingParams.ajaxCallSuccess = false;
            vm.view.pageLoadingParams.searchResultsLoaded = true;
            vm.updatePageLoadingParams();
        });
    }

    vm.redirect = function(url) {
        $window.location.href = url+"?category="+catService.selectedProductFamilyCode+"&salesModelId="+catService.selectedSalesModelCode+"&primaryOfferingId="+catService.selectedPrimaryOfferingCode+"&pdcFlag="+catService.plp.isPDCChecked+"&mfuFlag="+catService.plp.isMFUChecked+"&wsFlag="+catService.plp.isWarehouseChecked+"&page="+catService.plp.currentPage+"&show=page";
    }
    vm.viewMore = function($event) {
        $($event.target).parents(".item-desc").children(".item-details").show();
        $($event.target).hide();

    }
    vm.viewLess = function($event) {
        $($event.target).parents(".item-desc").children(".item-details").hide();
        $($event.target).parents(".item-desc").find(".view-more").show();
    }

    vm.setProductFamily = function(code) {
        if (!!code) {
            vm.view.productFamily.selectedCode = code;
        }
    }

    vm.setSalesModel = function(code) {
        if (!!code) {
            vm.view.salesModel.selectedCode = code;
        }
    }

    vm.setPrimaryOffering = function(code) {
        if (!!code) {
            vm.view.primaryOffering.selectedCode = code;
        }
    }


    vm.init = function() {
        vm.updateSearchBackupData();
        vm.retriveSearchData();
        if (!!vm.view.productFamily.selectedCode) {
            var selectedCode = $("li[data-productFamily-code=" + vm.view.productFamily.selectedCode + "]");
            if (!!selectedCode && selectedCode.length > 0) {
                var name = selectedCode.data("productfamily-name");
                vm.view.productFamily.selectedProductName = name;
            }
        }
        if (!!vm.view.productFamily.selectedCode) {
            httpService.get(catEndpointService.productFamilyEnpoint + "?categoryCode=" + vm.view.productFamily.selectedCode).then(function(response) {
                vm.view.pageLoadingParams.salesModelLoaded = true;
                vm.updatePageLoadingParams();
                if (response.status == 200) {
                    vm.view.salesModel.listOfSalesModel = response.data;
                    (response.data).forEach(function(el) {
                        if (!!el && (el.code == vm.view.salesModel.selectedCode)) {
                            vm.view.salesModel.selectedModelName = el.name;
                        }
                    });
                    vm.view.salesModel.isDisabled = false;
                }
            }, function(error) {
                vm.view.pageLoadingParams.salesModelLoaded = true;
                vm.updatePageLoadingParams();
            });
        }
        if (!!vm.view.salesModel.selectedCode) {
            httpService.get(catEndpointService.salesModelEndpoint + "?productCode=" + vm.view.salesModel.selectedCode).then(function(response) {
                vm.view.pageLoadingParams.primaryOfferingLoaded = true;
                vm.updatePageLoadingParams();
                if (response.status == 200) {
                    vm.view.primaryOffering.listOfPrimaryOffering = response.data;
                    vm.view.primaryOffering.isDisabled = false;

                    (response.data).forEach(function(el) {
                        if (!!el && (el.code == vm.view.primaryOffering.selectedCode)) {
                            vm.view.primaryOffering.selectedPrimaryOffering = el.name+" ("+ el.code +")";
                        }
                    });
                    vm.view.hasValidSearchParams = true;
                }
            }, function(error) {
                vm.view.pageLoadingParams.primaryOfferingLoaded = true;
                vm.updatePageLoadingParams();
            });
        } else {
            vm.view.pageLoadingParams.primaryOfferingLoaded = true;
            vm.updatePageLoadingParams();
        }
    }

    vm.backupSearchData = function(){
        var anyOfCheckBoxChecked = (vm.view.location.isWareHouseChecked || vm.view.location.iscatPDCChecked || vm.view.location.isManufacturingChecked);
        catService.selectedProductFamilyCode = vm.view.productFamily.selectedCode;
        catService.selectedSalesModelCode = vm.view.salesModel.selectedCode;
        catService.selectedPrimaryOfferingCode = vm.view.primaryOffering.selectedCode;
        catService.plp.isPDCChecked =(anyOfCheckBoxChecked)?vm.view.location.iscatPDCChecked:false;
        catService.plp.isMFUChecked = (anyOfCheckBoxChecked)?vm.view.location.isManufacturingChecked:false;
        catService.plp.isWarehouseChecked =(anyOfCheckBoxChecked)?vm.view.location.isWareHouseChecked:false;
        catService.plp.currentPage = vm.view.pagination.hybrisPageNumber;

    }

    vm.updateSearchBackupData = function(){
        var anyOfCheckBoxChecked = (catService.plp.isPDCChecked || catService.plp.isMFUChecked || catService.plp.isWarehouseChecked);   
        vm.view.location.isWareHouseChecked=(anyOfCheckBoxChecked)?catService.plp.isWarehouseChecked:false;
        vm.view.location.iscatPDCChecked= (anyOfCheckBoxChecked)?catService.plp.isPDCChecked:false;
        vm.view.location.isManufacturingChecked =(anyOfCheckBoxChecked)?catService.plp.isMFUChecked:false;
        vm.view.pagination.hybrisPageNumber =  catService.plp.currentPage;
        vm.view.pagination.currentPage = (catService.plp.currentPage+1);
    }

    vm.locationTypesModified = function(event) {
        vm.view.pageLoadingParams.searchResultsLoaded = false;
        vm.retriveSearchData();
    }

    vm.cloneMobileElement = function() {

        var iE1 = angular.element(document.querySelector('.params'));
        var iE2 = angular.element(document.querySelector('#nav-mobile-selection'));

        var iE3 = angular.element(document.querySelector('.cat-location-types'));
        var iE4 = angular.element(document.querySelector('#nav-mobile-locations'));

        iE2.append($compile(iE1.clone())($scope));
        iE4.append($compile(iE3.clone())($scope));
    }

    vm.pageChanged = function() {
        vm.view.pagination.hybrisPageNumber = 0;
        if ((vm.view.pagination.currentPage - 1) > -1) {
            vm.view.pagination.hybrisPageNumber = (vm.view.pagination.currentPage - 1);
        }

        vm.view.pagination.clickedOnPagination = true;
        vm.view.pageLoadingParams.searchResultsLoaded = false;
        vm.updatePageLoadingParams();
        vm.retriveSearchData();
    }

    vm.scrollTop = function() {
    }

    vm.applyMasonary = function() {
        $timeout(function() {
            $('#grid').masonry({
                itemSelector: '.item',
                columnWidth: ".item"
            });
            $('#grid').imagesLoaded(function() {
                $('#grid').masonry({
                    itemSelector: '.item',
                    columnWidth: ".item"
                });
            });
        }, 0);
    }

    vm.resetLocationTypes = function(){
        vm.view.location.isWareHouseChecked = false;
        vm.view.location.iscatPDCChecked = false;
        vm.view.location.isManufacturingChecked = false;

        vm.view.location.hasWareHouseUnit = false;
        vm.view.location.hascatPDCUnit = false;
        vm.view.location.hasManufacturing = false;
    }
}]);

ACC.catGlobal = {
	_autoload: [
		"bindHoverForMainNavigation"
    ],
    bindHoverForMainNavigation: function () {
        enquire.register("screen and (min-width:" + screenMdMin + ")", {
             match: function () {
                $(".js-enquire-has-sub").hoverIntent(function (e) { 
                    var offSetLeft = $(".desktop-nav-menu").offset().left;
                     var $subNav = $('#cat-navigation-menu');
                     var height = 0;
                     $("#sub-"+$(this).attr("id")+" .border-right").each(function(index){
                        $(this).height('auto');
                        $(this).children("a").height('auto');
                        if($(this).height() > height){
                            height = $(this).height();
                        }
                     });
                     $("#sub-"+$(this).attr("id")+" .border-right").each(function(index){
                        $(this).height(height);
                     });
                     $("#sub-"+$(this).attr("id")+" .border-right a.nav-link-desktop").each(function(index){
                        $(this).height(height-20);
                     });

                     $subNav.css({
                            "left": offSetLeft
                        });

                     $(this).addClass("active");
                    if($("#sub-"+$(this).attr("id")+" >div").length > 0){
                        $("#sub-"+$(this).attr("id")).addClass("active");
                        $("#cat-navigation-menu").addClass("active");
                    }
                },function(){
                    setTimeout(function(){
                        $(this).removeClass("active");
                        $("#sub-"+$(this).attr("id")).removeClass("active");
                        $("#cat-navigation-menu").removeClass("active");
                    }, 100);
                }); 

                 $(".subCat-box").hoverIntent(function (e) {
                    $("#"+$(this).attr("id").replace("sub-","")).addClass("active");
                 },function(){
                    $("#"+$(this).attr("id").replace("sub-","")).removeClass("active");
                 });
              },
             unmatch: function () {
                 }
        });

         enquire.register("screen and (min-width:" + screenMdMin + ")", { });


       
    },
    constructAnalyticsForMobileVersion: function(){
       

    }
}

ACC.catNavigation = {
    _autoload: ["bindToggleOffcanvas", "constructMenu", "desktopMenu"],
    showSidebar: function() {
        ACC.catNavigation.sidebar().show('slide', { direction: 'left' }, 300);
        ACC.catNavigation.overlay().show(0, function() {
            ACC.catNavigation.overlay().fadeTo('500', 0.5);
            $("body").addClass("modal-open");
        });
    },
    hideSidebar: function() {
        ACC.catNavigation.sidebar().hide('slide', { direction: 'left' }, 200, function() {
            ACC.catNavigation.overlay().hide();
            $("body").removeClass("modal-open");
        });
    },
    sidebar: function() {
        return $('[data-sidebar]');
    },
    overlay: function() {
        return $('#catMobileNavOverlay[data-sidebar-overlay]');
    },
    toggleMobileMenu: function() {
        if (ACC.catNavigation.sidebar().is(':visible')) {
            ACC.catNavigation.hideSidebar();

        } else {
            ACC.catNavigation.showSidebar();
        }
        return false;
    },
    constructMenu: function() {

        var userDiv = $("<div>", { id: "catNavMenuprofile" });
        var oUserInfo = $(".nav__right ul li.logged_in");
        if (oUserInfo && oUserInfo.length === 1) {
            var sUserBtn = oUserInfo[0].innerHTML
            userDiv.html(sUserBtn);
            $("#catNavMenuContainer").append(userDiv);
        }

        var navDiv = $("<div>", { id: "navigationLinks" });
        var ulDiv = $("<ul>", { id: "mobileNavMenu" });
        var desktopAnalyticsInfo = $("#top-analytics a[title]");
        if (desktopAnalyticsInfo && desktopAnalyticsInfo.length >= 1) {
            var topAnalytics = desktopAnalyticsInfo[0].innerHTML;
            ulDiv.prepend("<li>" + topAnalytics + "</li>");
        }

        var desktopNavInfo = $(".desktop-nav-menu.nav__links--products");
        if (desktopNavInfo && desktopNavInfo.length >= 1) {

            var mobileNaveMenu = desktopNavInfo[0].innerHTML;
            ulDiv.append(mobileNaveMenu);
        }
        navDiv.append(ulDiv);
        $("#catNavMenuContainer").append(navDiv);

        var signOutDiv = $("<div>", { id: "logout" });
        var oUserLogout = $(".nav__right ul li.liOffcanvas");
        oUserLogout.removeClass("liOffcanvas");
        if (oUserLogout && oUserLogout.length === 1) {
            var sUserLogout = oUserLogout[0].innerHTML
            signOutDiv.html(sUserLogout);
            $("#catNavMenuContainer").append(signOutDiv);
        }
    },
    bindToggleOffcanvas: function() {
        enquire.register("screen and (max-width:" + screenSmMax + ")", {
            match: function() {
                ACC.catNavigation.sidebar().hide();
                ACC.catNavigation.overlay().hide();
                $("#cat-toggle-menu").unbind("click").bind("click", ACC.catNavigation.toggleMobileMenu);
                $("#catMobileNavOverlay").unbind("click").bind("click", ACC.catNavigation.hideSidebar);
            },
            unmatch: function() {
                ACC.catNavigation.sidebar().hide();
                ACC.catNavigation.overlay().hide();
                $("#cat-toggle-menu").unbind("click");
                $("#catMobileNavOverlay").unbind("click");
                $("body").removeClass("modal-open");
            }
        });
    },
    desktopMenu: function() {
        $(".subCat-box").each(function(index, element) {


            var mobileNavDiv = $("<div>", { class: "col-xs-12 desk-menu" });
            var linksContainer = $("<div>",{ class: "col-xs-3 col-sm-3 border-right"});

            var linksLength = $(this).children("a.nav-link-desktop").length;
            var count = 0;
            if (linksLength >= 1) {
                mobileNavDiv.append(linksContainer.clone().addClass("first"));
            } 
            if (linksLength >= 2) {
                mobileNavDiv.append(linksContainer.clone().addClass("second"));
            } 
            if (linksLength >= 3) {
                mobileNavDiv.append(linksContainer.clone().addClass("third"));
            } 
            if(linksLength >= 4) {
                mobileNavDiv.append(linksContainer.clone().addClass("fourth"));
            }
            $(this).children("a.nav-link-desktop").each(function(i, subElement){
               $(this).removeClass("col-xs-3").addClass("col-xs-12");
                if(i%4 == 0){
                    mobileNavDiv.find(".first").append($(this));
                }else if(i%4 == 1){
                    mobileNavDiv.find(".second").append($(this));
                }else if(i%4 == 2){
                    mobileNavDiv.find(".third").append($(this));
                }else if(i%4 == 3){
                    mobileNavDiv.find(".fourth").append($(this));
                }
            });
            $(this).append(mobileNavDiv);


/*            var chunks = ACC.catNavigation.createGroupedArray($(this).children("div.nav-link-desktop"), 4);
            chunks.forEach(function(element, index){
                element.each(function(i, subElement){
                    subElement.removeClass("col-xs-3").addClass("col-xs-12");
                    if(i == 0){
                        mobileNavDiv.find(".first").append(subElement);
                    }else if(i == 1){
                        mobileNavDiv.find(".second").append(subElement);
                    }else if(i == 2){
                        mobileNavDiv.find(".third").append(subElement);
                    }else if(i == 3){
                        mobileNavDiv.find(".fourth").append(subElement);
                    }
                });
            });*/
        });
    },
    createGroupedArray: function(arrList, chunkSize) {
        var chunk = [], i;
        for (i = 0; i < arrList.length; i += chunkSize) {
            chunk.push(arrList.slice(i, i + chunkSize));
        }
        return chunk;
    }
}

$(function(){
	$(".primary-link").click(function(){
	});

    $(".cat-sub-menu-expand").click(function(){
        if($("#mob-sub-"+$(this).attr("id")).hasClass("active")){
            $("#mob-sub-"+$(this).attr("id")).removeClass("active");
            $("#sub-icon-"+$(this).attr("id")).removeClass("glyphicon-minus").addClass("glyphicon-plus");
        }else{
            $("#mob-sub-"+$(this).attr("id")).addClass("active");
            $("#sub-icon-"+$(this).attr("id")).removeClass("glyphicon-plus").addClass("glyphicon-minus");
        }
    });

//$('.dropdown-toggle').dropdown();

//$(".navigation--bottom  .mobile-navigation  .close-nav");.remove();

});



ACC.plp = {
	_autoload: ["constructMobileMenu","bindMobileClickEvents"],
	constructMobileMenu: function(){
			 var paramsDiv = $(".params");
        if (paramsDiv && paramsDiv.length === 1) {
            var paramsConent = paramsDiv[0].innerHTML
            $("#nav-mobile-selection").append(paramsConent);
        }
        var locationTypesDiv = $(".cat-location-types");
        if (locationTypesDiv && locationTypesDiv.length === 1) {
            var locationTypeConent = locationTypesDiv[0].innerHTML
            $("#nav-mobile-locations").append(locationTypeConent);
        }


	},
	bindMobileClickEvents: function(){
		 enquire.register("screen and (max-width:" + screenSmMax + ")", { 
		 	 match: function() { 

		 	 	$(".cat-mobile-tabs").unbind("click").bind("click", function(){
		 	 		$("#"+$(this).attr("data-target")).siblings().hide();
		 	 		$("#"+$(this).attr("data-target")).slideToggle(function(){
		 	 				if($(this).is(":visible")){
		 	 					ACC.plp.overlay().fadeTo('50', 0.6);
          			$("body").addClass("modal-open");
		 	 				}else{
		 	 					ACC.plp.overlay().hide();
            		$("body").removeClass("modal-open");
		 	 				}
		 	 		});

		 	 		$("#catSearchMobileNavOverlay").unbind("click").bind("click", function(){
		 	 			ACC.plp.overlay().hide();
		 	 			$("#nav-mobile-selection").hide();
		 	 			$("#nav-mobile-locations").hide();
            $("body").removeClass("modal-open");
		 	 		});

		 	 		

		 	 		

		 	 		//'
		 	 	});
		 	 	/*$(".loacate-tab").on("click",function(){
		 	 		$("#nav-mobile-selection").hide();
		 	 		$("#nav-mobile-locations").toggleClass("active");
		 	 		if($("#nav-mobile-locations").hasClass("active")){
		 	 			$("#nav-mobile-locations").hide();
		 	 		}else{
		 	 				$("#nav-mobile-locations").show();
		 	 		}
		 	 	});
		 	 	$(".select-tab").on("click",function(){
		 	 		$("#nav-mobile-locations").hide();
		 	 		$("#nav-mobile-selection").toggleClass("active");
		 	 		if($("#nav-mobile-selection").hasClass("active")){
		 	 			$("#nav-mobile-selection").hide();
		 	 		}else{
		 	 				$("#nav-mobile-selection").show();
		 	 		}
		 	 	});*/
		 	 },
		 	 unmatch: function() { $(".mobile-content").hide();}
		 });
	},
	overlay: function() {
        return $('#catSearchMobileNavOverlay[data-sidebar-overlay]');
    },
}

$(function() {

    $(".image-gallery__image").click(function() {
    	 $("#catImage").attr("src",$(".image-gallery__image .owl-item .item img.lazyOwl")[0].src);
        $("#catImageModal").show();
    });

    $("#close").click(function() {
        $("#catImageModal").hide();
    });

    $("#catImageModal").click(function() {
        $("#catImageModal").hide();
    });

});
