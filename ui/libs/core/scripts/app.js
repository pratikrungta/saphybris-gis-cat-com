var catApp = angular.module('cat-app', ['ui.bootstrap','ngSanitize']);

catApp.run(['catService', '$location', function(catService, $location) {
    catService.selectedProductFamilyCode = $location.search().categoryCode || "";
    catService.selectedSalesModelCode = $location.search().salesModelId || ""
    catService.selectedPrimaryOfferingCode = $location.search().primaryOfferingId || "";

    if (!!$location.search().pdcFlag) {
        catService.plp.isPDCChecked = $.parseJSON($location.search().pdcFlag);
    } else {
        catService.plp.isPDCChecked = false;
    }

    if (!!$location.search().pdcFlag) {
        catService.plp.isMFUChecked = $.parseJSON($location.search().mfuFlag);
    } else {
        catService.plp.isMFUChecked = false;
    }

    if (!!$location.search().pdcFlag) {
        catService.plp.isWarehouseChecked = $.parseJSON($location.search().wsFlag);
    } else {
        catService.plp.isWarehouseChecked = false;
    }

    if(!!$location.search().page && !!Number($location.search().page)){
        catService.plp.currentPage = Number($location.search().page);
    }else{
        catService.plp.currentPage = 0;
    }

}]);

catApp.config(['$locationProvider',
    function($locationProvider) {
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }
]);