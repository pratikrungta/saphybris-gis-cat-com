catApp.controller('landingPageController', ['httpService', 'catEndpointService', '$scope', function(httpService, catEndpointService, $scope) {

  var vm = this;

  vm.init = function() {
    console.log("Initiated");
  };

  vm.productFamilyMenu = {
    isOpen: false,
    isDisabled: true,
    selectedCode: "",
    selectedProductName: ""
  };

  vm.salesModelMenu = {
    isOpen: false,
    isDisabled: true,
    listOfSalesModel: [],
    selectedCode: "",
    selectedModelName: ""
  };

  vm.primaryOffering = {
    isOpen: false,
    isDisabled: true,
    listOfPrimaryOffering: [],
    selectedCode: "",
    selectedPrimaryOffering: ""
  };

  vm.validForm = false;
  vm.init();

  vm.productFamilyDropdown = function($event) {

    setTimeout(function() {
      if ($("#product-family").hasClass("open")) {
        $("#product-family").removeClass("open");
      } else {
        $("#product-family").addClass("open");
      }
    }, 0);


    vm.productFamilyMenu.isOpen = !vm.productFamilyMenu.isOpen;
    return false;
  }

  vm.productFamilySelected = function(code, name) {
    vm.productFamilyMenu.selectedCode = code;
    vm.productFamilyMenu.selectedProductName = name;

    vm.salesModelMenu.isDisabled = true;
    vm.primaryOffering.isDisabled = true;

    vm.validForm = false;

    vm.salesModelMenu.selectedModelName = "";
    vm.primaryOffering.selectedPrimaryOffering = "";

    vm.primaryOffering.selectedCode= "";
    vm.salesModelMenu.selectedCode = "";

    httpService.get(catEndpointService.productFamilyEnpoint + "?categoryCode=" + vm.productFamilyMenu.selectedCode).then(function(response) {
      if (response.status == 200) {
        vm.salesModelMenu.listOfSalesModel = response.data;
        vm.salesModelMenu.isDisabled = false;
      }
    }, function(error) {});
    $("#product-family").removeClass("open");
  }

  vm.salesModelSelected = function(code, name) {
    vm.salesModelMenu.selectedCode = code;
    vm.salesModelMenu.selectedModelName = name;
    vm.primaryOffering.selectedPrimaryOffering = "";
    vm.primaryOffering.selectedCode= "";
     vm.validForm = false;

    httpService.get(catEndpointService.salesModelEndpoint + "?productCode=" + vm.salesModelMenu.selectedCode).then(function(response) {
      if (response.status == 200) {
        vm.primaryOffering.listOfPrimaryOffering = response.data;
        vm.primaryOffering.isDisabled = false;
      }
    }, function(error) {});
  }

  vm.primaryOfferingSelected = function(code, name) {
  	vm.validForm = true;
    vm.primaryOffering.isDisabled = false;
    vm.primaryOffering.selectedCode = code;
    vm.primaryOffering.selectedPrimaryOffering = name +" ("+code+")";
}

}]);

$(function() {
  $(document).click(function(event) {
    if (!$(event.target).closest('.cat-dropdown').length) {
      $(".cat-dropdown").removeClass("open");
    }
  });
});