catApp.controller('plpController', ['httpService', 'catEndpointService', '$scope', '$location', '$window', '$compile', 'constantService', '$timeout', 'catService', function(httpService, catEndpointService, $scope, $location, $window, $compile, constantService, $timeout, catService) {

    var vm = this;
    vm.view = {
        productFamily: {
            isDisabled: false,
            isOpen: false,
            selectedProductName: "",
            selectedCode: ""
        },
        salesModel: {
            isDisabled: true,
            selectedModelName: "",
            selectedCode: "",
            listOfSalesModel: []
        },
        primaryOffering: {
            isDisabled: true,
            selectedPrimaryOffering: "",
            selectedCode: "",
            listOfPrimaryOffering: []
        },
        location: {
            isWareHouseChecked: false,
            iscatPDCChecked: false,
            isManufacturingChecked: false,
            hasWareHouseUnit: false,
            hascatPDCUnit: false,
            hasManufacturing: false
        },
        searchResults: [],
        searchResultsMetaData: "",
        hasValidSearchParams: false,
        pageLoadingParams: {
            searchResultsLoaded: false,
            salesModelLoaded: false,
            primaryOfferingLoaded: false,
            pagedLoaded: false,
            ajaxCallSuccess: true
        },
        pagination: {
            currentPage: 1,
            totalPages: 10,
            displayGroups: 3,
            elementsForCurrentPage: 8,
            startIndex: 1,
            totalItems: 100,
            hybrisPageNumber: 0,
            clickedOnPagination: false
        },
        initialLoad: true,
        primaryOfferingForCards: ""
    }


    vm.constantService = constantService;

    vm.updatePageLoadingParams = function() {
        vm.view.pageLoadingParams.pagedLoaded = (vm.view.pageLoadingParams.searchResultsLoaded && vm.view.pageLoadingParams.salesModelLoaded && vm.view.pageLoadingParams.primaryOfferingLoaded);
    }

    vm.setSearchParams = function(productFamily, salesModel, primaryOffering) {}


    vm.productFamilySelected = function(code, name) {
            vm.view.productFamily.selectedCode = code;
            vm.view.productFamily.selectedProductName = name;

            vm.view.salesModel.isDisabled = true;
            vm.view.primaryOffering.isDisabled = true;
            vm.view.hasValidSearchParams = false;


            vm.view.salesModel.selectedModelName = "";
            vm.view.primaryOffering.selectedPrimaryOffering = "";

            vm.view.primaryOffering.selectedCode = "";
            vm.view.salesModel.selectedCode = "";
            vm.view.productFamily.isOpen = false;



            httpService.get(catEndpointService.productFamilyEnpoint + "?categoryCode=" + vm.view.productFamily.selectedCode).then(function(response) {
                if (response.status == 200) {
                    vm.view.salesModel.listOfSalesModel = response.data;
                    vm.view.salesModel.isDisabled = false;
                }
            }, function(error) {});
        },

        vm.salesModelSelected = function(code, name) {
            vm.view.salesModel.selectedCode = code;
            vm.view.salesModel.selectedModelName = name;
            vm.view.primaryOffering.selectedPrimaryOffering = "";
            vm.view.primaryOffering.selectedCode = "";

            vm.view.hasValidSearchParams = true;
            vm.view.primaryOffering.isDisabled = false;

            httpService.get(catEndpointService.salesModelEndpoint + "?productCode=" + vm.view.salesModel.selectedCode).then(function(response) {
                if (response.status == 200) {
                    vm.view.primaryOffering.listOfPrimaryOffering = response.data;
                    vm.view.primaryOffering.isDisabled = false;
                }
            }, function(error) {});
        }

    vm.primaryOfferingSelected = function(code, name) {
        vm.view.hasValidSearchParams = true;
        vm.view.primaryOffering.isDisabled = false;
        vm.view.primaryOffering.selectedCode = code;
        vm.view.primaryOffering.selectedPrimaryOffering = name+" ("+ code +")";
    }

    vm.updateSearchData = function() {
        vm.view.pageLoadingParams.searchResultsLoaded = false;
        vm.updatePageLoadingParams();
        vm.resetLocationTypes();
        vm.retriveSearchData(); 
        {
            ACC.plp.overlay().hide();
            $("#nav-mobile-selection").hide();
            $("#nav-mobile-locations").hide();
            $("body").removeClass("modal-open");
        }
    }

    


    vm.retriveSearchData = function() {
        vm.backupSearchData();

        var anyOfCheckBoxChecked = (vm.view.location.isWareHouseChecked || vm.view.location.iscatPDCChecked || vm.view.location.isManufacturingChecked);
        var show='Page';

        var inWarehouseFlag = (anyOfCheckBoxChecked)?vm.view.location.isWareHouseChecked:true;
        var inStockPDCFlag = (anyOfCheckBoxChecked)?vm.view.location.iscatPDCChecked:true;
        var inStockMFUFlag = (anyOfCheckBoxChecked)?vm.view.location.isManufacturingChecked:true;

        var url = catEndpointService.searchResultsEndPoint+"?q=&category="+vm.view.productFamily.selectedCode+"&salesModelId="+vm.view.salesModel.selectedCode+"&primaryOfferingId="+vm.view.primaryOffering.selectedCode+"&pdcFlag="+inStockPDCFlag+"&mfuFlag="+inStockMFUFlag+"&wsFlag="+inWarehouseFlag+"&page="+vm.view.pagination.hybrisPageNumber+"&show="+show;
        vm.view.primaryOfferingForCards = vm.view.productFamily.selectedProductName;
        httpService.get(url).then(function(response) {
            if(response.status == 200){
            vm.view.pageLoadingParams.searchResultsLoaded = true;
            vm.view.pageLoadingParams.ajaxCallSuccess = true;

            vm.updatePageLoadingParams();
            vm.view.searchResults = [];
            response.data.results.forEach(function(item) {
                item.primaryOfferingForCards = vm.view.productFamily.selectedProductName;
                vm.view.searchResults.push(item);
            });
            vm.view.searchResultsMetaData = response.data.pagination;
            vm.view.pagination.totalItems = response.data.pagination.totalNumberOfResults;
            vm.view.pagination.elementsForCurrentPage = response.data.pagination.pageSize;
            vm.view.location.hasWareHouseUnit = (!!response.data.warehouseMap)?$.parseJSON(response.data.warehouseMap.inDealerWS)
            :true;
            if(!vm.view.location.hasWareHouseUnit){
                vm.view.location.isWareHouseChecked = false;
            }
            vm.view.location.hascatPDCUnit = (!!response.data.warehouseMap)?$.parseJSON(response.data.warehouseMap.inPDC)
            :true;
            if(!vm.view.location.hascatPDCUnit){
                vm.view.location.iscatPDCChecked = false;
            }
            vm.view.location.hasManufacturing =  (!!response.data.warehouseMap)?$.parseJSON(response.data.warehouseMap.inMFU)
            :true;
            if(!vm.view.location.hasManufacturing){
                    vm.view.location.isManufacturingChecked = false;
            }
            if (vm.view.searchResults.lenght == 0) {
                vm.view.searchResults();
                vm.view.pageLoadingParams.searchResultsLoaded = true;
                vm.view.pageLoadingParams.ajaxCallSuccess = true;
            }
        }else{
         vm.view.pageLoadingParams.ajaxCallSuccess = false;
            vm.view.pageLoadingParams.searchResultsLoaded = true;
            vm.updatePageLoadingParams();   
        }
        }, function(error) {
            vm.view.pageLoadingParams.ajaxCallSuccess = false;
            vm.view.pageLoadingParams.searchResultsLoaded = true;
            vm.updatePageLoadingParams();
        });
    }

    vm.redirect = function(url) {
        $window.location.href = url+"?category="+catService.selectedProductFamilyCode+"&salesModelId="+catService.selectedSalesModelCode+"&primaryOfferingId="+catService.selectedPrimaryOfferingCode+"&pdcFlag="+catService.plp.isPDCChecked+"&mfuFlag="+catService.plp.isMFUChecked+"&wsFlag="+catService.plp.isWarehouseChecked+"&page="+catService.plp.currentPage+"&show=page";
    }
    vm.viewMore = function($event) {
        $($event.target).parents(".item-desc").children(".item-details").show();
        $($event.target).hide();

    }
    vm.viewLess = function($event) {
        $($event.target).parents(".item-desc").children(".item-details").hide();
        $($event.target).parents(".item-desc").find(".view-more").show();
    }

    vm.setProductFamily = function(code) {
        if (!!code) {
            vm.view.productFamily.selectedCode = code;
        }
    }

    vm.setSalesModel = function(code) {
        if (!!code) {
            vm.view.salesModel.selectedCode = code;
        }
    }

    vm.setPrimaryOffering = function(code) {
        if (!!code) {
            vm.view.primaryOffering.selectedCode = code;
        }
    }


    vm.init = function() {
        vm.updateSearchBackupData();
        vm.retriveSearchData();
        if (!!vm.view.productFamily.selectedCode) {
            var selectedCode = $("li[data-productFamily-code=" + vm.view.productFamily.selectedCode + "]");
            if (!!selectedCode && selectedCode.length > 0) {
                var name = selectedCode.data("productfamily-name");
                vm.view.productFamily.selectedProductName = name;
            }
        }
        if (!!vm.view.productFamily.selectedCode) {
            httpService.get(catEndpointService.productFamilyEnpoint + "?categoryCode=" + vm.view.productFamily.selectedCode).then(function(response) {
                vm.view.pageLoadingParams.salesModelLoaded = true;
                vm.updatePageLoadingParams();
                if (response.status == 200) {
                    vm.view.salesModel.listOfSalesModel = response.data;
                    (response.data).forEach(function(el) {
                        if (!!el && (el.code == vm.view.salesModel.selectedCode)) {
                            vm.view.salesModel.selectedModelName = el.name;
                        }
                    });
                    vm.view.salesModel.isDisabled = false;
                }
            }, function(error) {
                vm.view.pageLoadingParams.salesModelLoaded = true;
                vm.updatePageLoadingParams();
            });
        }
        if (!!vm.view.salesModel.selectedCode) {
            httpService.get(catEndpointService.salesModelEndpoint + "?productCode=" + vm.view.salesModel.selectedCode).then(function(response) {
                vm.view.pageLoadingParams.primaryOfferingLoaded = true;
                vm.updatePageLoadingParams();
                if (response.status == 200) {
                    vm.view.primaryOffering.listOfPrimaryOffering = response.data;
                    vm.view.primaryOffering.isDisabled = false;

                    (response.data).forEach(function(el) {
                        if (!!el && (el.code == vm.view.primaryOffering.selectedCode)) {
                            vm.view.primaryOffering.selectedPrimaryOffering = el.name+" ("+ el.code +")";
                        }
                    });
                    vm.view.hasValidSearchParams = true;
                }
            }, function(error) {
                vm.view.pageLoadingParams.primaryOfferingLoaded = true;
                vm.updatePageLoadingParams();
            });
        } else {
            vm.view.pageLoadingParams.primaryOfferingLoaded = true;
            vm.updatePageLoadingParams();
        }
    }

    vm.backupSearchData = function(){
        var anyOfCheckBoxChecked = (vm.view.location.isWareHouseChecked || vm.view.location.iscatPDCChecked || vm.view.location.isManufacturingChecked);
        catService.selectedProductFamilyCode = vm.view.productFamily.selectedCode;
        catService.selectedSalesModelCode = vm.view.salesModel.selectedCode;
        catService.selectedPrimaryOfferingCode = vm.view.primaryOffering.selectedCode;
        catService.plp.isPDCChecked =(anyOfCheckBoxChecked)?vm.view.location.iscatPDCChecked:false;
        catService.plp.isMFUChecked = (anyOfCheckBoxChecked)?vm.view.location.isManufacturingChecked:false;
        catService.plp.isWarehouseChecked =(anyOfCheckBoxChecked)?vm.view.location.isWareHouseChecked:false;
        catService.plp.currentPage = vm.view.pagination.hybrisPageNumber;

    }

    vm.updateSearchBackupData = function(){
        var anyOfCheckBoxChecked = (catService.plp.isPDCChecked || catService.plp.isMFUChecked || catService.plp.isWarehouseChecked);   
        vm.view.location.isWareHouseChecked=(anyOfCheckBoxChecked)?catService.plp.isWarehouseChecked:false;
        vm.view.location.iscatPDCChecked= (anyOfCheckBoxChecked)?catService.plp.isPDCChecked:false;
        vm.view.location.isManufacturingChecked =(anyOfCheckBoxChecked)?catService.plp.isMFUChecked:false;
        vm.view.pagination.hybrisPageNumber =  catService.plp.currentPage;
        vm.view.pagination.currentPage = (catService.plp.currentPage+1);
    }

    vm.locationTypesModified = function(event) {
        vm.view.pageLoadingParams.searchResultsLoaded = false;
        vm.retriveSearchData();
    }

    vm.cloneMobileElement = function() {

        var iE1 = angular.element(document.querySelector('.params'));
        var iE2 = angular.element(document.querySelector('#nav-mobile-selection'));

        var iE3 = angular.element(document.querySelector('.cat-location-types'));
        var iE4 = angular.element(document.querySelector('#nav-mobile-locations'));

        iE2.append($compile(iE1.clone())($scope));
        iE4.append($compile(iE3.clone())($scope));
    }

    vm.pageChanged = function() {
        vm.view.pagination.hybrisPageNumber = 0;
        if ((vm.view.pagination.currentPage - 1) > -1) {
            vm.view.pagination.hybrisPageNumber = (vm.view.pagination.currentPage - 1);
        }

        vm.view.pagination.clickedOnPagination = true;
        vm.view.pageLoadingParams.searchResultsLoaded = false;
        vm.updatePageLoadingParams();
        vm.retriveSearchData();
    }

    vm.scrollTop = function() {
    }

    vm.applyMasonary = function() {
        $timeout(function() {
            $('#grid').masonry({
                itemSelector: '.item',
                columnWidth: ".item"
            });
            $('#grid').imagesLoaded(function() {
                $('#grid').masonry({
                    itemSelector: '.item',
                    columnWidth: ".item"
                });
            });
        }, 0);
    }

    vm.resetLocationTypes = function(){
        vm.view.location.isWareHouseChecked = false;
        vm.view.location.iscatPDCChecked = false;
        vm.view.location.isManufacturingChecked = false;

        vm.view.location.hasWareHouseUnit = false;
        vm.view.location.hascatPDCUnit = false;
        vm.view.location.hasManufacturing = false;
    }
}]);