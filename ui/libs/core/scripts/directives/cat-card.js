catApp.directive('catCard', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            superHeader: '@',
            primaryHeader: '@',
            secondryHeader: '@',
            image: '@',
            minQty: '@',
            minQtyText: '@',
            maxQty: '@',
            maxQtyText: '@',
            recommendedQty: '@',
            recommendedQtyText: '@',
            currentQty: '@',
            currentQtyText: '@',
            qtyMeasurement: '@',
            favoriteMarked: '@',
            cardType: '@',
            productIdForCommunication: '@',
            pdpLink: '@',
            reorderQty: '@'
        },
        templateUrl: '/libs/core/scripts/templates/cardTemplate.html',
        compile: function(tElem, attrs) {
            //do optional DOM transformation here
            return function(scope, elem, attrs) {
                if (!!attrs.cardType) {
                    if (attrs.cardType == 'IM') {
                        scope.imCard = true;
                    } else {
                        scope.spCard = true;
                    }
                }
                scope.canReorder = false;
                if (!!attrs.canReorder && attrs.canReorder == "true") {
                    scope.canReorder = true;
                }
                scope.bookMarked = false;
                if (!!attrs.bookMarked && attrs.bookMarked == "true") {
                    scope.bookMarked = true;
                }
                debugger;
                if (!!attrs.reorderDefaultValue && !!Number(attrs.reorderDefaultValue)) {
                    scope.reorderqty = Number(attrs.reorderDefaultValue);
                    scope.defaultValue =  scope.reorderqty;
                }
            };
        },
        controller: ['$scope', '$window', function($scope, $window) {
            $scope.removeFromBookMark = function() {
                if (!!$scope.productIdForCommunication) {}
            }
            $scope.addFromBookMark = function() {
                if (!!$scope.productIdForCommunication) {

                }
            }
            $scope.navigateToPDP = function() {
                if (!!$scope.pdpLink) {
                    $window.location.href = $scope.pdpLink;
                }
            }
            $scope.decreaseQuantity = function() {
                if ($scope.reorderqty > 0) {
                    $scope.reorderqty = $scope.reorderqty-1;
                }else{
                  $scope.reorderqty = 0;  
                }
            }
            $scope.increaseQuantity = function() {
                $scope.reorderqty = $scope.reorderqty+1;
            }
            $scope.resetInputValue = function(){
                debugger;
                if(!$scope.reorderqty){
                    $scope.reorderqty = $scope.defaultValue;
                }
            }
        }]
    };
});