catApp.directive('catSearch', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: '/libs/core/scripts/templates/typeahead.html',
        scope: true,
        controller: ['$scope', 'catEndpointService', 'httpService', 'filterFilter', function($scope, catEndpointService, httpService, filterFilter) {
            $scope.view = {
                searchResults: [],
                inputDirty: false,
                emptyResults: false,
                productFamily: 'Product Family',
                salesModel: 'Sales Model',
                baseModel: 'Base Model'
            }

            $scope.getCatResults = function(search) {
                httpService.get(catEndpointService.typeaheadSearchEndPoint).then(function(response) {
                  console.log(new Date());
                    $scope.view.searchResults = [];
                    if (!!response.data && response.data.productFamily) {
                        (response.data.productFamily).forEach(function(obj, index) {
                            if (index == 0) {
                                obj.firstInGroup = true;
                           } else {
                                obj.firstInGroup = false;
                            }
                            obj.displayValue = obj.categoryName;
                            obj.group = $scope.view.productFamily;
                            $scope.view.searchResults.push(obj);
                        });
                        var lastobj =$scope.view.searchResults.pop();
                        lastobj.lastObject = true;
                        $scope.view.searchResults.push(lastobj);
                    }
                    if (!!response.data && response.data.salesModels) {
                        (response.data.salesModels).forEach(function(obj, index) {
                            if (index == 0) {
                                obj.firstInGroup = true;
                            } else {
                                obj.firstInGroup = false;
                            }
                            obj.displayValue = obj.salesModelName;
                            obj.group = $scope.view.salesModel;
                            $scope.view.searchResults.push(obj);
                        });
                        var lastobj =$scope.view.searchResults.pop();
                        lastobj.lastObject = true;
                        $scope.view.searchResults.push(lastobj);
                    }
                    if (!!response.data && response.data.baseModels) {
                        (response.data.baseModels).forEach(function(obj, index) {
                            if (index == 0) {
                                obj.firstInGroup = true;
                            } else {
                                obj.firstInGroup = false;
                            }
                            obj.displayValue = obj.primaryOfferingName;
                            obj.group = $scope.view.baseModel;
                            $scope.view.searchResults.push(obj);
                        });
                        var lastobj =$scope.view.searchResults.pop();
                        lastobj.lastObject = true;
                        $scope.view.searchResults.push(lastobj);
                    }
                });
                 console.log(new Date());
                return $scope.view.searchResults;

            };


            $scope.onSelect = function($item, $model, $label) {
                if (!!$model && $model.group){

                }
            }

            $scope.$watch('typeaheadIsOpen', function(newVal, oldVal) {
                if (newVal == oldVal) {
                    return;
                }
                if (newVal) {
                    var searchField = angular.element.find('input');
                    var width = searchField[0].offsetWidth;

                    var dropdown = angular.element.find('.dropdown-menu');
                    angular.element(dropdown[0]).css('width', (width + 'px'));
                }
            });
        }]
      }
    });