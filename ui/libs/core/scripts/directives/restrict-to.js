catApp.directive('restrictTo', [function() {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var re = RegExp(attrs.restrictTo);
            var exclude = /Backspace|Enter|Tab|Delete|Del|ArrowUp|Up|ArrowDown|Down|ArrowLeft|Left|ArrowRight|Right/;
            var excludeKeyCode = /^(8|13|9|46|37|38|40)$/;

            element[0].addEventListener('keydown', function(event) {
                var keyCode = event.keyCode || event.which;
                if(keyCode >= 96 && keyCode <= 105){
                    keyCode -= 48;
                }
                keyValue = String.fromCharCode(keyCode);
                if ( !excludeKeyCode.test(keyCode) && !re.test(keyValue)) {
                    event.preventDefault();
                }
            });
        }
    }
}]);