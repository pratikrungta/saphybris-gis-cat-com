catApp.directive('slider', function() {
    return {
        restrict: 'EA',
        transclude: true,
        replace: true,
        scope: {},
        compile: function(tElem, attrs) {
            //var $ele = angular.element(tElem);
            //$ele.prepend(angular.element(''));
            //$ele.append(angular.element(''));
            return {
                pre: function(scope, elem, attrs, nullCtrl, transcludeFn) {
                    transcludeFn(scope, function(clone) {
                        elem.append(clone);
                    });
                },
                post: function(scope, element, attributes, controller, transcludeFn) {
                    $("#owl-demo").owlCarousel({
                        navigation: true,
                        items: 3,
                        navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
                        itemsDesktop: [1199, 3],
                        itemsDesktopSmall: [979, 2],
                        itemsTablet: [768, 1],
                        itemsTabletSmall: false,
                        itemsMobile: [479, 1],
                        slideSpeed: 800,
                        rewindSpeed: 1000
                    });
                }
            }
        }
    };
});