ACC.catGlobal = {
	_autoload: [
		"bindHoverForMainNavigation"
    ],
    bindHoverForMainNavigation: function () {
        enquire.register("screen and (min-width:" + screenMdMin + ")", {
             match: function () {
                $(".js-enquire-has-sub").hoverIntent(function (e) { 
                    var offSetLeft = $(".desktop-nav-menu").offset().left;
                     var $subNav = $('#cat-navigation-menu');
                     var height = 0;
                     $("#sub-"+$(this).attr("id")+" .border-right").each(function(index){
                        $(this).height('auto');
                        $(this).children("a").height('auto');
                        if($(this).height() > height){
                            height = $(this).height();
                        }
                     });
                     $("#sub-"+$(this).attr("id")+" .border-right").each(function(index){
                        $(this).height(height);
                     });
                     $("#sub-"+$(this).attr("id")+" .border-right a.nav-link-desktop").each(function(index){
                        $(this).height(height-20);
                     });

                     $subNav.css({
                            "left": offSetLeft
                        });

                     $(this).addClass("active");
                    if($("#sub-"+$(this).attr("id")+" >div").length > 0){
                        $("#sub-"+$(this).attr("id")).addClass("active");
                        $("#cat-navigation-menu").addClass("active");
                    }
                },function(){
                    setTimeout(function(){
                        $(this).removeClass("active");
                        $("#sub-"+$(this).attr("id")).removeClass("active");
                        $("#cat-navigation-menu").removeClass("active");
                    }, 100);
                }); 

                 $(".subCat-box").hoverIntent(function (e) {
                    $("#"+$(this).attr("id").replace("sub-","")).addClass("active");
                 },function(){
                    $("#"+$(this).attr("id").replace("sub-","")).removeClass("active");
                 });
              },
             unmatch: function () {
                 }
        });

         enquire.register("screen and (min-width:" + screenMdMin + ")", { });


       
    },
    constructAnalyticsForMobileVersion: function(){
       

    }
}