ACC.catNavigation = {
    _autoload: ["bindToggleOffcanvas", "constructMenu", "desktopMenu"],
    showSidebar: function() {
        ACC.catNavigation.sidebar().show('slide', { direction: 'left' }, 300);
        ACC.catNavigation.overlay().show(0, function() {
            ACC.catNavigation.overlay().fadeTo('500', 0.5);
            $("body").addClass("modal-open");
        });
    },
    hideSidebar: function() {
        ACC.catNavigation.sidebar().hide('slide', { direction: 'left' }, 200, function() {
            ACC.catNavigation.overlay().hide();
            $("body").removeClass("modal-open");
        });
    },
    sidebar: function() {
        return $('[data-sidebar]');
    },
    overlay: function() {
        return $('#catMobileNavOverlay[data-sidebar-overlay]');
    },
    toggleMobileMenu: function() {
        if (ACC.catNavigation.sidebar().is(':visible')) {
            ACC.catNavigation.hideSidebar();

        } else {
            ACC.catNavigation.showSidebar();
        }
        return false;
    },
    constructMenu: function() {

        var userDiv = $("<div>", { id: "catNavMenuprofile" });
        var oUserInfo = $(".nav__right ul li.logged_in");
        if (oUserInfo && oUserInfo.length === 1) {
            var sUserBtn = oUserInfo[0].innerHTML
            userDiv.html(sUserBtn);
            $("#catNavMenuContainer").append(userDiv);
        }

        var navDiv = $("<div>", { id: "navigationLinks" });
        var ulDiv = $("<ul>", { id: "mobileNavMenu" });
        var desktopAnalyticsInfo = $("#top-analytics a[title]");
        if (desktopAnalyticsInfo && desktopAnalyticsInfo.length >= 1) {
            var topAnalytics = desktopAnalyticsInfo[0].innerHTML;
            ulDiv.prepend("<li>" + topAnalytics + "</li>");
        }

        var desktopNavInfo = $(".desktop-nav-menu.nav__links--products");
        if (desktopNavInfo && desktopNavInfo.length >= 1) {

            var mobileNaveMenu = desktopNavInfo[0].innerHTML;
            ulDiv.append(mobileNaveMenu);
        }
        navDiv.append(ulDiv);
        $("#catNavMenuContainer").append(navDiv);

        var signOutDiv = $("<div>", { id: "logout" });
        var oUserLogout = $(".nav__right ul li.liOffcanvas");
        oUserLogout.removeClass("liOffcanvas");
        if (oUserLogout && oUserLogout.length === 1) {
            var sUserLogout = oUserLogout[0].innerHTML
            signOutDiv.html(sUserLogout);
            $("#catNavMenuContainer").append(signOutDiv);
        }
    },
    bindToggleOffcanvas: function() {
        enquire.register("screen and (max-width:" + screenSmMax + ")", {
            match: function() {
                ACC.catNavigation.sidebar().hide();
                ACC.catNavigation.overlay().hide();
                $("#cat-toggle-menu").unbind("click").bind("click", ACC.catNavigation.toggleMobileMenu);
                $("#catMobileNavOverlay").unbind("click").bind("click", ACC.catNavigation.hideSidebar);
            },
            unmatch: function() {
                ACC.catNavigation.sidebar().hide();
                ACC.catNavigation.overlay().hide();
                $("#cat-toggle-menu").unbind("click");
                $("#catMobileNavOverlay").unbind("click");
                $("body").removeClass("modal-open");
            }
        });
    },
    desktopMenu: function() {
        $(".subCat-box").each(function(index, element) {


            var mobileNavDiv = $("<div>", { class: "col-xs-12 desk-menu" });
            var linksContainer = $("<div>",{ class: "col-xs-3 col-sm-3 border-right"});

            var linksLength = $(this).children("a.nav-link-desktop").length;
            var count = 0;
            if (linksLength >= 1) {
                mobileNavDiv.append(linksContainer.clone().addClass("first"));
            } 
            if (linksLength >= 2) {
                mobileNavDiv.append(linksContainer.clone().addClass("second"));
            } 
            if (linksLength >= 3) {
                mobileNavDiv.append(linksContainer.clone().addClass("third"));
            } 
            if(linksLength >= 4) {
                mobileNavDiv.append(linksContainer.clone().addClass("fourth"));
            }
            $(this).children("a.nav-link-desktop").each(function(i, subElement){
               $(this).removeClass("col-xs-3").addClass("col-xs-12");
                if(i%4 == 0){
                    mobileNavDiv.find(".first").append($(this));
                }else if(i%4 == 1){
                    mobileNavDiv.find(".second").append($(this));
                }else if(i%4 == 2){
                    mobileNavDiv.find(".third").append($(this));
                }else if(i%4 == 3){
                    mobileNavDiv.find(".fourth").append($(this));
                }
            });
            $(this).append(mobileNavDiv);


/*            var chunks = ACC.catNavigation.createGroupedArray($(this).children("div.nav-link-desktop"), 4);
            chunks.forEach(function(element, index){
                element.each(function(i, subElement){
                    subElement.removeClass("col-xs-3").addClass("col-xs-12");
                    if(i == 0){
                        mobileNavDiv.find(".first").append(subElement);
                    }else if(i == 1){
                        mobileNavDiv.find(".second").append(subElement);
                    }else if(i == 2){
                        mobileNavDiv.find(".third").append(subElement);
                    }else if(i == 3){
                        mobileNavDiv.find(".fourth").append(subElement);
                    }
                });
            });*/
        });
    },
    createGroupedArray: function(arrList, chunkSize) {
        var chunk = [], i;
        for (i = 0; i < arrList.length; i += chunkSize) {
            chunk.push(arrList.slice(i, i + chunkSize));
        }
        return chunk;
    }
}