ACC.plp = {
	_autoload: ["constructMobileMenu","bindMobileClickEvents"],
	constructMobileMenu: function(){
			 var paramsDiv = $(".params");
        if (paramsDiv && paramsDiv.length === 1) {
            var paramsConent = paramsDiv[0].innerHTML
            $("#nav-mobile-selection").append(paramsConent);
        }
        var locationTypesDiv = $(".cat-location-types");
        if (locationTypesDiv && locationTypesDiv.length === 1) {
            var locationTypeConent = locationTypesDiv[0].innerHTML
            $("#nav-mobile-locations").append(locationTypeConent);
        }


	},
	bindMobileClickEvents: function(){
		 enquire.register("screen and (max-width:" + screenSmMax + ")", { 
		 	 match: function() { 

		 	 	$(".cat-mobile-tabs").unbind("click").bind("click", function(){
		 	 		$("#"+$(this).attr("data-target")).siblings().hide();
		 	 		$("#"+$(this).attr("data-target")).slideToggle(function(){
		 	 				if($(this).is(":visible")){
		 	 					ACC.plp.overlay().fadeTo('50', 0.6);
          			$("body").addClass("modal-open");
		 	 				}else{
		 	 					ACC.plp.overlay().hide();
            		$("body").removeClass("modal-open");
		 	 				}
		 	 		});

		 	 		$("#catSearchMobileNavOverlay").unbind("click").bind("click", function(){
		 	 			ACC.plp.overlay().hide();
		 	 			$("#nav-mobile-selection").hide();
		 	 			$("#nav-mobile-locations").hide();
            $("body").removeClass("modal-open");
		 	 		});

		 	 		

		 	 		

		 	 		//'
		 	 	});
		 	 	/*$(".loacate-tab").on("click",function(){
		 	 		$("#nav-mobile-selection").hide();
		 	 		$("#nav-mobile-locations").toggleClass("active");
		 	 		if($("#nav-mobile-locations").hasClass("active")){
		 	 			$("#nav-mobile-locations").hide();
		 	 		}else{
		 	 				$("#nav-mobile-locations").show();
		 	 		}
		 	 	});
		 	 	$(".select-tab").on("click",function(){
		 	 		$("#nav-mobile-locations").hide();
		 	 		$("#nav-mobile-selection").toggleClass("active");
		 	 		if($("#nav-mobile-selection").hasClass("active")){
		 	 			$("#nav-mobile-selection").hide();
		 	 		}else{
		 	 				$("#nav-mobile-selection").show();
		 	 		}
		 	 	});*/
		 	 },
		 	 unmatch: function() { $(".mobile-content").hide();}
		 });
	},
	overlay: function() {
        return $('#catSearchMobileNavOverlay[data-sidebar-overlay]');
    },
}