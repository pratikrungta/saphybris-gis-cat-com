$(function() {

    $(".image-gallery__image").click(function() {
    	 $("#catImage").attr("src",$(".image-gallery__image .owl-item .item img.lazyOwl")[0].src);
        $("#catImageModal").show();
    });

    $("#close").click(function() {
        $("#catImageModal").hide();
    });

    $("#catImageModal").click(function() {
        $("#catImageModal").hide();
    });

});