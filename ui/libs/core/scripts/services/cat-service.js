catApp.factory('catService', [function(){
	 var catData = {
	 				selectedProductFamilyCode: "",
			 		selectedSalesModelCode: "",
			 		selectedPrimaryOfferingCode: "",
		 	plp: {
			 		isPDCChecked: false,
			 		isMFUChecked: false,
			 		isWarehouseChecked: false,
			 		currentPage: 0

		 	}
	 };
	 return catData;
}]);