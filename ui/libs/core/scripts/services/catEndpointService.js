catApp.factory('catEndpointService', [function(){
	 var endPointsData = {
	 	productFamilyEnpoint: '/_ui/responsive/cat/mockjsons/selectCategory.json',
	 	salesModelEndpoint: '/_ui/responsive/cat/mockjsons/selectSalesModel.json',
	 	searchResultsEndPoint: '/_ui/responsive/cat/mockjsons/searchResults.json',
	 	typeaheadSearchEndPoint: '/_ui/responsive/cat/mockjsons/catSearch.json',

	 	//productFamilyEnpoint: '/search/selectCategory',
	 	//salesModelEndpoint: '/search/selectSalesModel',
	 	//searchResultsEndPoint: '/search/results',
	 	
	 };
	 return endPointsData;
}]);