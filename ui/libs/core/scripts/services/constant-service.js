catApp.factory('constantService', [function(){
	 var constantData = {
	 		globalSorryHeader: 'We\'re sorry',
	 		searchResultsErrorMessage: ' Something went wrong.Thanks for noticing, we are working on this!',
	 		noSearchResultsFound: 'Sorry, no results for your current search.Please try changing the search attributes.',
	 };
	 return constantData;
}]);