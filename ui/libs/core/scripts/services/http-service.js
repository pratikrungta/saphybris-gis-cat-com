catApp.service('httpService', ['$http', function($http) {
	var config = {
		headers : {
      'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
    }
	};
	this.post = function(path, data) {
    return $http.post(path, data, config);
  };
  this.get = function(path) {
    return $http.get(path);
  }
}]);